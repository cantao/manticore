#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MDF file abstraction
#
# @addtogroup MDF
# @author Cantão! (rfcantao@gmail.com)
#

import h5py
import numpy as np
import manticore
from manticore.mdf.utils import Folders, Prefixes
from manticore.mdf.enums import Interpolation, MapType
from manticore.mdf.modeldata import ModelData
from manticore.mdf.partitiondata import PartitionData
from manticore.mdf.fielddata import FieldData
from manticore.geom.standard_geometries import number_of_nodes, geometry_name

logger = manticore.logging.getLogger('_MDF_APIS_')

class ModellingException(Exception):
    """Throw in case of errors on model description."""
    pass


class ConsistencyException(Exception):
    """Throw when trying to modify the data model in an incompatible way."""
    pass


class UnknownFileMode(Exception):
    """Throw when an incompatible or unknown file mode is used."""
    pass


class UnknownFieldType(Exception):
    """Throw when an unknown field type is specified."""
    pass


class File(h5py.File):
    """Representation of a HDF5 file.

    Creates a HDF5 file and the underlying structure in it using the
    model's attributes. The file is left in a state in which relevant data
    (nodes, elements or fields) can be written.

    Attributes:
        __model_data (ModelData): the data binding this mesh to a physical
            model.

        __partition_data (dict): meta description of the underlying
            partitioning scheme. Its a dictionary relating a numeric
            integer key with a PartitionData object.

            .. warning:: Check if we need at least one partition!

        __field_data (dict): solution fields that can be present in the MDF
            file.
    """

    IDS = 'ids'
    COORDS = 'coords'

    def __init__(self, name, mode=None):
        """Constructor for a MDF file object.

        Args:
            name (str): on disk file name.

            mode (str): file creation mode

                +---------+--------------------------------------------------+
                | Flag    | Meaning                                          |
                +=========+==================================================+
                | r       | Readonly, file must exist                        |
                +---------+--------------------------------------------------+
                | w       | Create file, truncate if exists                  |
                +---------+--------------------------------------------------+
                | w- or x | Create file, fail if exists                      |
                +---------+--------------------------------------------------+

            libver (str): HDF5 library version. Defaults to `latest`.
        """
        if mode not in ['r', 'w', 'w-', 'x']:
            raise UnknownFileMode('Unknown file mode: "{}"'.format(mode))

        super().__init__(name, mode=mode, libver='latest')

        self.__model_data = None
        self.__partition_data = dict()
        self.__field_data = dict()
        # We store a mode here that is probably not the same as used by h5py
        self.__mode = mode

    def create(self):
        # If we are creating the file, we start afresh. Note that if the file
        # already exists (mode 'w-' or 'x'), the exception should be thrown in
        # the super().__init__() call.
        if self.__mode in ['w', 'w-', 'x']:
            self.__create_structure()
        else:
            self.__open_structure()

    def set_model_attr(self, description, **kwargs):
        """Sets the model attributes.

        Create a :class:`.ModelData` object with the specified parameters.
        ``**kwargs`` processing is deferred to :class:`.ModelData`.

        Args:
            description (str): succint model description.

            **kwargs (dict): see :class:`.ModelData` for all possible
                parameters.

        Raises:
            RuntimeError: if the user tries to set it again.
        """

        # It uses the property to do that to guarantee that the data model will
        # never be overwritten.
        self.model_data = ModelData(description=description, **kwargs)

    def read_model_attr(self):
        """Reads the model attributes from file.

        Ideally, we should strive to preserve the consistency at all costs, but
        in this case it starts to make things too complex, so we leave this to
        the user.
        """
        model_data_group = self[Folders.MODEL]

        self.__model_data = ModelData()
        self.__model_data.read(model_data_group)

        return self.model_data

    def set_partition_attr(self, idx, name, **kwargs):
        """Creates and inserts a partition attribute object into the list of
        partitions.

        Create a :class:`.PartitionData` object and associate it do key
        `idx` on `__partition_data`.

        Args:
            idx(int): partition index. Mostly for historical purposes. p.name,
            p.description, p.n_nodes, p.boun

            name(string): partition name.

            **kwargs: see :class:`.PartitionData` for more info.
        """
        logger.debug('Creating partition data #{} <- {}'.format(idx, name))

        self.__partition_data[idx] = PartitionData(name, **kwargs)
        self.__model_data.n_parts = len(self.__partition_data)

        return self.__partition_data[idx]

    def read_partition_attr(self):
        """Reads the partition attributes from file.

        See comments on read_model_attr.
        """
        # /model/geometry
        geometry_group = self[Folders.MODEL + '/' + Folders.GEOMETRY]

        for i in range(0, self.__model_data.n_parts):
            logger.debug('Reading partition data #{}'.format(i))
            self.__partition_data[i] = PartitionData.create_from_group(
                geometry_group, i)

        return self.partition_data

    def set_field_attr(self, idx, name, **kwargs):
        """Insert a field attribute object into the list of field.

        Create a :class:`.FieldData` object and associate it do key `idx`
        on `__field_data`.

        Args:
            idx(int): partition index. Mostly for historical purposes.

            **kwargs: see :class:`.FieldData` for more info.
        """
        self.__field_data[idx] = FieldData(name, **kwargs)
        self.__model_data.n_fields = len(self.__field_data)

        return self.__field_data[idx]

    def write_nodes(self, dim, partition_id, coords, indices):
        """Writes a dataset with nodal information.

        Args:
            dim (int): spatial dimension.

            partition_id (int): which partition we are writing to.

            coords (list): the coordinates themselves, as a 1D or 2D NumPy
                array. If 1D, its size must be len(indices)*dim. If 2D it must
                be (len(indices), dim).

            ids (list): indexes of the coordinates as a 1D NumPy array. If
                None, a sequential id list will be used.
        """
        # Flattening the array
        coords = np.ravel(coords)

        n_coords = len(coords)

        # Sanity check
        if n_coords % dim != 0:
            raise ConsistencyException('Incompatible geometry dimension!')

        n_nodes = int(n_coords / dim)

        try:
            partition = self.__partition_data[partition_id]
        except KeyError:  # non-existent partition
            logger.critical('Trying to access a non-existent partition!')
            logger.critical('Nodes were NOT written!')
            return

        # Sanity check
        if n_nodes != partition.n_nodes:
            raise ConsistencyException(
                'Partition {} and ModelData are not synced'.format(
                    partition_id))

        logger.debug('Writing [{}] {}D nodes'.format(n_nodes, dim))

        folder = self.__concatenate_partition(partition_id)
        folder += '/' + Folders.NODES

        nodes_group = self[folder]

        # Dataset for the ids and the coords
        nodes_group.create_dataset(File.IDS, data=indices)
        nodes_group.create_dataset(File.COORDS, data=coords)

    def read_nodes(self, partition_id):
        """Reads a dataset with nodal information.

        A tuple with two 1D NumPy arrays is returned. The first element of the
        tuple is a matrix with the coordinates and the second a vector
        representing the indices.

        Args:
            partition_id (int): which partition we are reading from.

        Returns:
            tuple: Coordinates in matricial form (NELEM x DIM) and indices.
        """
        folder = self.__concatenate_partition(partition_id)
        folder += '/' + Folders.NODES

        nodes_group = self[folder]

        # TODO: dim must be save also in the coordinates info
        dim = 2

        # Reading the datasets for coords and indices
        indices = np.array(nodes_group[File.IDS], dtype=int)
        coords = np.array(nodes_group[File.COORDS])

        # Sanity check
        if len(indices) * dim != len(coords):
            raise ConsistencyException(
                'Indices and coordinates are unsynced '
                'for partition #{}!'.format(partition_id))

        return coords.reshape(-1, dim), indices

    def write_elements(self, partition_id, subdomain_id, p_order, element,
                       conn, indices):
        """Dumps elemental information into the MDF file.

        Args:
            partition_id (int): which partition we are writing to.
            subdomain_id (int): which domain we are referring to.
            p_order (int): order of field interpolation.
            element (BaseGeometry): type of element we are writing.
            conn (array): connectivity information. It can be a single vector
                or a NELEM x DIM matrix.
            indices (array): elements indices
        """
        n_element_nodes = number_of_nodes(element)

        conn = conn.ravel()

        # Sanity check
        if len(conn) % n_element_nodes != 0:
            raise ConsistencyException('Inconsistent connectivity!')

        n_elements = int(len(conn) / n_element_nodes)

        # Sanity check
        if len(indices) != n_elements:
            raise ConsistencyException('Wrong number of indices!')

        # /model/geometry/part_i/domain/sdom_j
        folder = self.__concatenate_partition_and_subdomain(
            partition_id, subdomain_id)
        subdomain_group = self[folder]

        ename = geometry_name(element)
        element_group = subdomain_group.require_group(ename)

        # Dataset names, depending on the kind of interpolation.
        if self.__model_data.interpolation == Interpolation.P_INTERPOL:
            id_name = 'pids_{}'.format(p_order)
            conn_name = 'pconn_{}'.format(p_order)
            attr_name = 'nPElements_{}'.format(p_order)
        else:
            id_name = 'hids'
            conn_name = 'hconn'
            attr_name = 'nHElements'

        element_group.attrs[attr_name] = n_elements
        element_group.create_dataset(id_name, data=indices)
        element_group.create_dataset(conn_name, data=conn)

    def read_elements(self, partition_id, subdomain_id, p_order, element):
        """Reads elemental information from the MDF file.

        Args:
            partition_id (int): which partition we are writing to.
            subdomain_id (int): which domain we are referring to.
            p_order (int): order of field interpolation.
            element (BaseGeometry): type of element we are reading.

        Returns:
            tuple: connectivity information in matrix form (NELEM x NNODES),
                followed by elements indices.
        """
        # /model/geometry/part_i/domain/sdom_j
        folder = self.__concatenate_partition_and_subdomain(
            partition_id, subdomain_id)

        subdomain_group = self[folder]

        ename = geometry_name(element)
        element_group = subdomain_group[ename]

        # Dataset names, depending on the kind of interpolation.
        if self.__model_data.interpolation == Interpolation.P_INTERPOL:
            id_name = 'pids_{}'.format(p_order)
            conn_name = 'pconn_{}'.format(p_order)
            attr_name = 'nPElements_{}'.format(p_order)
        else:
            id_name = 'hids'
            conn_name = 'hconn'
            attr_name = 'nHElements'

        n_elements = element_group.attrs.get(attr_name)

        indices = np.array(element_group[id_name])
        conn = np.array(element_group[conn_name])

        n_element_nodes = number_of_nodes(element)

        # Sanity check
        if n_elements * n_element_nodes != len(conn):
            raise ConsistencyException(
                'Indices and connectivity are unsynced '
                'for partition #{}, subdomain #{}!'.format(
                    partition_id, subdomain_id))

        return conn.reshape(-1, n_element_nodes), indices

    def write_boundary_elements(self, partition_id, boundary_id, element, conn,
                                indices):
        """Dumps elemental boundary information into the MDF file.

        Args:
            partition_id (int): which partition we are writing to.
            boundary_id (int): which boundary we are referring to.
            element (BaseGeometry): type of element we are writing.
            conn (array): connectivity information. It can be a single vector
                or a NELEM x DIM matrix.
            indices (array): elements indices
        """
        n_element_nodes = number_of_nodes(element)

        conn = conn.ravel()

        # Sanity check
        if len(conn) % n_element_nodes != 0:
            raise ConsistencyException('Inconsistent connectivity!')

        n_elements = int(len(conn) / n_element_nodes)

        # Sanity check
        if len(indices) != n_elements:
            raise ConsistencyException('Wrong number of indices!')

        folder = self.__concatenate_partition_and_boundary(
            partition_id, boundary_id)

        ename = geometry_name(element)

        boundary_group = self[folder]
        boundary_group.attrs['n' + ename] = n_elements
        boundary_group.create_dataset(ename + '_ids', data=indices)
        boundary_group.create_dataset(ename, data=conn)

    def read_boundary_elements(self, partition_id, boundary_id, element):
        """Reads elemental boundary information from the MDF file.

        Args:
            partition_id (int): which partition we are writing to.
            boundary_id (int): which boundary we are referring to.
            element (BaseGeometry): type of element we are writing.

        Returns:
            tuple: connectivity information in matrix form (NELEM x NNODES),
                followed by elements indices.
        """
        # /model/geometry/part_i/domain/sdom_j
        folder = self.__concatenate_partition_and_boundary(
            partition_id, boundary_id)

        boundary_group = self[folder]

        ename = geometry_name(element)

        n_elements = boundary_group.attrs.get('n' + ename)
        indices = np.array(boundary_group[ename + '_ids'])
        conn = np.array(boundary_group[ename])

        n_element_nodes = number_of_nodes(element)

        # Sanity check
        if n_elements * n_element_nodes != len(conn):
            raise ConsistencyException(
                'Indices and connectivity are unsynced '
                'for partition #{}, subdomain #{}!'.format(
                    partition_id, boundary_id))

        return conn.reshape(-1, n_element_nodes), indices

    def write_field(self, field_id, partition_id, subdomain_id, p_order,
                    element, field):
        """Writes a field to a MDF file.

        Args:
            field_id (int): which field we are writing to.
            partition_id (int): which partition we are writing to.
            subdomain_id (int): which subdomain we are referring to.
            p_order (int): order of field interpolation.
            element (BaseGeometry): type of element we are writing.
            field (NumPy array): the field itself.
        """
        map_type = self.__field_data[field_id].map_type
        header = self.__generate_field_header(map_type, p_order)

        # Element geometric common name
        gname = geometry_name(element)

        group_name = self.__concatenate_partition_field_subdomain(
            field_id, partition_id, subdomain_id)

        base_group = self.require_group(group_name)

        # Open the specific geometric group. Note that the elemental groups are
        # *not* created during the structural phase.
        element_group = base_group.require_group(gname)
        element_group.attrs['n' + header] = len(field)
        element_group.create_dataset(header, data=field)

    def read_field(self, field_id, partition_id, subdomain_id, p_order,
                   element):
        """Reads a field from a MDF file.

        Args:
            field_id (int): which field we are reading from.
            partition_id (int): which partition we are reading from.
            subdomain_id (int): which subdomain we are referring to.
            p_order (int): order of field interpolation.
            element (BaseGeometry): type of element we are writing.

        Returns:
            A NumPy array with field values.
        """
        map_type = self.__field_data[field_id].map_type
        header = self.__generate_field_header(map_type, p_order)

        # Element geometric common name
        gname = geometry_name(element)

        group_name = self.__concatenate_partition_field_subdomain(
            field_id, partition_id, subdomain_id)

        base_group = self[group_name]
        element_group = base_group[gname]

        return element_group[header][:]

    def find_boundary(self, partition_id, bdr):
        return self.__partition_data[partition_id].boundary_names.index(bdr)

    def query_subdomain_name(self, partition_id, subdomain_id):
        # /model/geometry/part_i/domain/sdom_j
        group_name = self.__concatenate_partition_and_subdomain(
            partition_id, subdomain_id)

        return self[group_name].attrs.get('name')

    def query_element_shapes(self, partition_id, subdomain_id):
        # /model/geometry/part_i/domain/sdom_j
        group_name = self.__concatenate_partition_and_subdomain(
            partition_id, subdomain_id)
        return self[group_name].keys()

    def query_boundary_element_shapes(self, partition_id, boundary):
        boundary_id = self.find_boundary(partition_id, boundary)

        # /model/geometry/part_i/boundary/sdom_j
        group_name = self.__concatenate_partition_and_boundary(
            partition_id, boundary_id)

        return [b for b in self[group_name].keys() if '_' not in b]

    def query_number_elements(self, partition_id, domain_id, shape):
        # /model/geometry/part_i/domain/sdom_j/shape
        group_name = self.__concatenate_partition_and_subdomain(
            partition_id, domain_id) + '/' + shape

        group = self[group_name]

        if self.__model_data.interpolation == Interpolation.P_INTERPOL:
            numels = dict()

            for name, attrs in group.attrs.items():
                split_attr = name.split('_')

                if split_attr[0] == 'nPElements':
                    numels[int(split_attr[1])] = attrs
        else:  # Interpolation.H_INTERPOL
            numels = {0: group.attrs.get('nHElements')}

        return numels

    def query_number_boundary_elements(self, partition_id, boundary_id, shape):
        # /model/geometry/part_i/boundary/sbound_j/shape
        group_name = self.__concatenate_partition_and_boundary(
            partition_id, boundary_id)

        return self[group_name].attrs.get('n' + shape)

    # -------------------------------------------------------------------------
    # -- INTERNAL -------------------------------------------------------------
    # -------------------------------------------------------------------------

    def __create_structure(self):
        """Creates the standard MDF structure."""
        if self.__model_data is None:
            raise RuntimeError('No data model available!')

        # Important step to guarantee the consistency!
        self.__model_data.n_parts = len(self.__partition_data)
        self.__model_data.n_subdomains = len(self.domain_names)

        model_data_group = self.require_group(Folders.MODEL)
        self.__model_data.write(model_data_group)

        if self.__model_data.needs_mesh_storage():
            if not self.__partition_data:
                raise RuntimeError('No partition model available!')

            self.__create_geometry_group(model_data_group)

        if self.__model_data.has_fields():
            if not self.__field_data:
                raise RuntimeError('Can\'t create fields without field data!')

            self.__create_field_group(model_data_group)

    def __open_structure(self):
        """Reads a previously created MDF file structure."""
        model_data_group = self[Folders.MODEL]
        self.__model_data = ModelData.create_from_group(model_data_group)

        if self.__model_data.needs_mesh_storage():
            self.__open_geometry_group(model_data_group)

        if self.__model_data.has_fields():
            self.__open_field_group(model_data_group)

    def __create_geometry_group(self, model_data_group):
        """Creates the whole geometry group of a MDF file.

        Args:
            model_data_group: h5py group that will root the geometry group.
        """
        # A little check of sanity between ModelData and PartitionData notion
        # of "number of partitions".
        assert self.__model_data.n_parts == len(self.__partition_data), \
            'ModelData and PartitionData are in disagreement!'

        # /model/geometry
        geom_group = model_data_group.require_group(Folders.GEOMETRY)

        for i, partition in self.__partition_data.items():
            partition.write(geom_group, i)

    def __open_geometry_group(self, model_data_group):
        """Reads the geometrical information from the geometry group."""
        # /model/geometry
        geom_group = model_data_group[Folders.GEOMETRY]

        for i in range(0, self.__model_data.n_parts):
            logger.debug('Reading partition {}'.format(i))

            # /model/geometry/part_i
            self.__partition_data[i] = PartitionData.create_from_group(
                geom_group, i)

    def __create_field_group(self, model_data_group):
        """Create the field group of a MDF file.

        Args:
            model_data_group: h5py group that will root of the fields group.
        """
        # Sanity check: number of fields declared in __model_data and the
        # actual number of declared fields.
        assert self.__model_data.n_fields == len(self.__field_data), \
            'ModelData and FieldData are in disagreement!'

        # /model/fields
        fields_group = model_data_group.require_group(Folders.FIELDS)

        for k, field in self.__field_data.items():
            # /model/fields/field_k
            field_k_group = field.write(fields_group, k)

            for i, partition in self.__partition_data.items():
                # /model/fields/field_k/part_i
                partition_group = \
                    field_k_group.require_group(Prefixes.PARTITION + str(i))

                # /model/fields/field_k/part_i/nodes
                nodes_group = partition_group.require_group('nodes')

                # /model/fields/field_k/part_i/domain
                domain_group = partition_group.require_group('domain')

                for j in range(self.__model_data.n_subdomains):
                    # /model/fields/field_k/part_i/domain/sdom_j
                    domain_group.require_group(Prefixes.SUBDOMAIN + str(j))

                # /model/fields/field_k/part_i/boundary
                bdr_group = partition_group.require_group('boundary')

                for j, bdr in enumerate(partition.boundary_names):
                    # /model/fields/field_k/part_i/boundary/sbound_j
                    bdr_group.require_group(Prefixes.BOUNDARY + str(j))

    def __open_field_group(self, model_data_group):
        # /model/fields
        fields_group = model_data_group[Folders.FIELDS]

        for i in range(0, self.__model_data.n_fields):
            logger.debug('Reading field {}'.format(i))

            # /model/fields/field_k
            self.__field_data[i] = FieldData.create_from_group(fields_group, i)

    def __concatenate_partition(self, partition_id):
        return '/{}/{}/{}{}'.format(Folders.MODEL, Folders.GEOMETRY,
                                    Prefixes.PARTITION, partition_id)

    def __concatenate_partition_and_subdomain(self, partition_id,
                                              subdomain_id):
        return '/{}/{}/{}{}/{}/{}{}'.format(
            Folders.MODEL, Folders.GEOMETRY, Prefixes.PARTITION, partition_id,
            Folders.DOMAIN, Prefixes.SUBDOMAIN, subdomain_id)

    def __concatenate_partition_and_boundary(self, partition_id, boundary_id):
        return '/{}/{}/{}{}'.format(
            self.__concatenate_partition(partition_id), Folders.BOUNDARY,
            Prefixes.BOUNDARY, boundary_id)

    def __concatenate_partition_field_subdomain(self, field_id, partition_id,
                                                subdomain_id):
        return '/{}/{}/{}{}/{}{}/{}/{}{}'.format(
            Folders.MODEL, Folders.FIELDS, Prefixes.FIELD, field_id,
            Prefixes.PARTITION, partition_id, Folders.DOMAIN,
            Prefixes.SUBDOMAIN, subdomain_id)

    def __generate_field_header(self, map_type, order):
        """Generates the prefix header for a field map."""
        if map_type == MapType.PER_INTP:
            if self.model_data.interpolation == Interpolation.P_INTERPOL:
                return 'pdata_{}'.format(order)
            else:
                return 'hdata'
        elif map_type == MapType.PER_ELEM:
            if self.model_data.interpolation == Interpolation.P_INTERPOL:
                return 'data_{}'.format(order)
            else:
                return 'data'
        elif map_type == MapType.HIERARCHICAL:
            return 'pcoef_{}'.format(order)
        else:
            raise UnknownFieldType('Unknown field type {}!'.format(map_type))

    @property
    def model_data(self):
        """ModelData getter."""
        return self.__model_data

    @model_data.setter
    def model_data(self, model_data):
        """ModelData setter.

            Args:
                model_data (ModelData): new :class:`.ModelData` object. Note
                    that this operation can be performed only once in order to
                    keep the file in sync with data on disk.
        """
        if self.__model_data is not None:
            raise RuntimeError('Can\'t set data model twice!')
        else:
            self.__model_data = model_data

    @property
    def partition_data(self):
        """PartitionData getter."""
        return self.__partition_data

    @partition_data.setter
    def partition_data(self, partition_data):
        """PartitionData setter.

            Args:
                partition_data (PartitionData): new :class:`.PartitionData`
                    object. Note that this operation can be performed only once
                    in order to keep the file in sync with data on disk.
        """
        if bool(self.__partition_data):
            raise RuntimeError('Can\'t set partition data twice!')
        else:
            self.__partition_data = partition_data

    @property
    def field_data(self):
        """FieldData getter."""
        return self.__field_data

    @property
    def domain_names(self):
        """Coalesced domain names."""
        dm = set()

        for k, p in self.__partition_data.items():
            dm.update(p.domain_names)

        return dm


# -- file.py ------------------------------------------------------------------
