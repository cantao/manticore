#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MDF file abstraction
#
# @addtogroup MDF
# @author Cantão! (rfcantao@gmail.com)
#

from enum import Enum


class MeshChange(Enum):
    """Types of mesh change.

        Args:
            STATIC_MESH: the mesh remains the same during the simulation.
            CH_COORDS_ONLY: only nodes coordinates change; connectivity
                            remains intact.
            CH_CONN: Both coordinates and connectivity can change.
    """
    STATIC_MESH = 0
    CH_COORDS_ONLY = 1
    CH_CONN = 2


class Interpolation(Enum):
    """Types of interpolation.

        Args:
            H_INTERPOL: h-type interpolation.
            P_INTERPOL: p-type interpolation.
    """
    H_INTERPOL = 0
    P_INTERPOL = 1


class FieldType(Enum):
    """Types of solution fields.

        The numbers indicate the number of elements needed to store each type.
        By sensible position we mean node, integration point or element.

        Args:
            SCALAR_FIELD: one scalar per sensible position.
            VECTOR_FIELD_2D: two scalars per sensible position.
            VECTOR_FIELD_3D: three scalars per sensible position.
            SYMM_TENSOR_FIELD: six scalars for symmetric tensor.
            TENSOR_FIELD: nine scalars for a full tensor.
    """
    SCALAR_FIELD = 1
    VECTOR_FIELD_2D = 2
    VECTOR_FIELD_3D = 3
    SYMM_TENSOR_FIELD = 6
    TENSOR_FIELD = 9


class MapType(Enum):
    """Possible mappings on an element.

        Args:
            PER_NODE: per-node, including possibly midnodes.
            PER_ELEM: centroid-valued field, finite volume like.
            PER_INTP: field on integration points.
            HIERARCHICAL: polynomial coefficients.
    """
    PER_NODE = 0
    PER_ELEM = 1
    PER_INTP = 2
    HIERARCHICAL = 3


# -- enums.py -----------------------------------------------------------------
