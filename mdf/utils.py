#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Constants used along the MDF implementation
#
# @addtogroup MDF
# @author Cantão! (rfcantao@gmail.com)
#


class Folders:
    """Standard folder names as per the MDF standard."""
    MODEL = "model"
    GEOMETRY = "geometry"
    FIELDS = "fields"
    NODES = "nodes"
    DOMAIN = "domain"
    BOUNDARY = "boundary"


class Prefixes:
    """Standard prefixes as per the MDF standard."""
    PARTITION = "part_"
    FIELD = "field_"
    SUBDOMAIN = "sdom_"
    BOUNDARY = "sbound_"


# -- utils.py -----------------------------------------------------------------
