#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MDF tests
#
# @addtogroup MDF
# @author Cantão! (rfcantao@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#

import unittest
from manticore.mdf.modeldata import ModelData
from manticore.mdf.enums import Interpolation, MeshChange


class ModelDataTestCase(unittest.TestCase):
    def test_defaults(self):
        model_data = ModelData()

        self.assertEqual(model_data.description, '__DEFAULT__')
        self.assertEqual(model_data.n_parts, 1)
        self.assertEqual(model_data.n_subdomains, 1)
        self.assertEqual(model_data.n_fields, 0)
        self.assertAlmostEqual(model_data.time_step, 0.0)
        self.assertAlmostEqual(model_data.time_value, 0.0)
        self.assertEqual(model_data.interpolation, Interpolation.H_INTERPOL)
        self.assertEqual(model_data.mesh_change, MeshChange.STATIC_MESH)


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_modeldata.py --------------------------------------------------------
