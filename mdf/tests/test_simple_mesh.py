#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Test mdf.File methods by writing/reading a simple mesh.
#
# @addtogroup INTEGRATION_TESTS
# @author Cantão! (rfcantao@gmail.com)
#
# $ python3 -m unittest -v manticore/mdf/tests/test_simple_mesh.py

import unittest
import numpy as np
from manticore.mdf.file import File
from manticore.mdf.enums import Interpolation, FieldType, MapType
from manticore.geom.standard_geometries import StandardGeometry


# SM stands for 'SimpleMesh'
class SM(unittest.TestCase):
    # Model
    FILENAME = 'integration_test_simple_mesh.mdf'
    MODEL_DESCR = 'IT SIMPLE MESH'
    MODEL_NFIELDS = 2
    MODEL_TIME_STEP = 1.2
    MODEL_TIME_VALUE = 10.5
    MODEL_INTERP = Interpolation.P_INTERPOL

    # Partitions
    A = {'name': 'A',
         'description': 'Partition A',
         'n_nodes': 3,
         'bdr': ['DIRICHLET', 'NEUMANN'],
         'dom': ['FLUID']}

    B = {'name': 'B',
         'description': 'Partition B',
         'n_nodes': 5,
         'bdr': ['DIRICHLET', 'NEUMANN'],
         'dom': ['FLUID', 'SOLID']}

    COORDS = {A['name']: np.array([[0.0, 1.0],
                                   [1.0, 1.0],
                                   [0.0, 0.0]]),
              B['name']: np.array([[1.0, 1.0],
                                   [2.0, 1.0],
                                   [0.0, 0.0],
                                   [1.0, 0.0],
                                   [2.0, 0.0]])}

    INDICES = {A['name']: np.array([0, 1, 3], dtype=int),
               B['name']: np.array([1, 2, 3, 4, 5], dtype=int)}

    BOUNDARIES = {A['name']:
                  {'DIRICHLET': np.array([[0, 1]], dtype=int),
                   'NEUMANN': np.array([[0, 3]], dtype=int)},
                  B['name']:
                  {'DIRICHLET': np.array([[1, 2], [2, 5]], dtype=int),
                   'NEUMANN': np.array([[3, 4], [4, 5]], dtype=int)}}

    # Incidence (per partition, per geometry)
    TRI = StandardGeometry.TRI3
    QUAD = StandardGeometry.QUAD4
    EDGE = StandardGeometry.EDGE2

    INCIDENCE = {A['name']: {TRI.uint: np.array([[0, 3, 1]], dtype=int),
                             QUAD.uint: []},
                 B['name']: {TRI.uint: np.array([[3, 4, 1]], dtype=int),
                             QUAD.uint: np.array([[4, 5, 2, 1]], dtype=int)}}

    def setUp(self):
        # Creating the mesh
        #
        # 0    1    2        Nodes:             Elements:
        # +----+----+        0    (0.0, 1.0)    0:  0, 3, 1
        # | A /|    |        1    (1.0, 1.0)    1:  3, 4, 1
        # | F/ |    |        2    (2.0, 1.0)    2:  4, 5, 2, 1
        # | /F | S  |        3    (0.0, 0.0)
        # |/ B | B  |        4    (1.0, 0.0)
        # +----+----+        5    (2.0, 0.0)
        # 3    4    5
        #
        # Boundaries:                  Partitions:
        # 0, 1, 2, 5: DIRICHLET        A: element 0
        # 0, 3, 4, 5: NEUMANN          B: elements 1 and 2
        #
        # Subdomains:
        # FLUID: elements 0 and 1
        # SOLID: element 2

        f = File(SM.FILENAME, mode='w')
        f.set_model_attr(SM.MODEL_DESCR, time_step=SM.MODEL_TIME_STEP,
                         time_value=SM.MODEL_TIME_VALUE,
                         interpolation=SM.MODEL_INTERP)

        A = SM.A['name']
        B = SM.B['name']

        # Partitions

        # Using an object partition
        part = f.set_partition_attr(0, A, description=SM.A['description'],
                                    n_nodes=SM.A['n_nodes'])
        part.append_boundaries(SM.A['bdr'])
        part.append_domains(SM.A['dom'])

        # Directly
        f.set_partition_attr(1, B, description=SM.B['description'],
                             n_nodes=SM.B['n_nodes'],
                             boundary_names=SM.B['bdr'],
                             domain_names=SM.B['dom'])

        # Fields
        f.set_field_attr(0, 'Density', description='Mass by volume',
                         field_type=FieldType.SCALAR_FIELD,
                         map_type=MapType.PER_INTP)

        f.set_field_attr(1, 'Energy', description='110V',
                         field_type=FieldType.SCALAR_FIELD,
                         map_type=MapType.PER_ELEM)

        # Structure creation
        f.create()

        # Attention! There should be another layer here pointing to the element
        # type on the boundary. Given that in 2D we have just edges as
        # boundaries, I choosed to simplify matters.

        # Nodes
        f.write_nodes(dim=2, partition_id=0,
                      coords=SM.COORDS[A], indices=SM.INDICES[A])
        f.write_nodes(dim=2, partition_id=1,
                      coords=SM.COORDS[B], indices=SM.INDICES[B])

        # Elements
        f.write_elements(partition_id=0, subdomain_id=0, g_order=1,
                         element=SM.TRI, conn=SM.INCIDENCE[A][SM.TRI.uint],
                         indices=[0])

        f.write_elements(partition_id=1, subdomain_id=0, g_order=1,
                         element=SM.TRI, conn=SM.INCIDENCE[B][SM.TRI.uint],
                         indices=[1])

        f.write_elements(partition_id=1, subdomain_id=1, g_order=1,
                         element=SM.QUAD, conn=SM.INCIDENCE[B][SM.QUAD.uint],
                         indices=[2])

        # Boundaries
        f.write_boundary_elements(
            partition_id=0, boundary_id=0, element=SM.EDGE,
            conn=SM.BOUNDARIES[A]['DIRICHLET'], indices=[0])

        f.write_boundary_elements(
            partition_id=0, boundary_id=1, element=SM.EDGE,
            conn=SM.BOUNDARIES[A]['NEUMANN'], indices=[1])

        f.write_boundary_elements(
            partition_id=1, boundary_id=0, element=SM.EDGE,
            conn=SM.BOUNDARIES[B]['DIRICHLET'], indices=[2, 3])

        f.write_boundary_elements(
            partition_id=1, boundary_id=1, element=SM.EDGE,
            conn=SM.BOUNDARIES[B]['NEUMANN'], indices=[4, 5])

        # Fields
        f.write_field(field_id=0, partition_id=0, subdomain_id=0, g_order=1,
                      element=SM.TRI, field=[1, 2, 3])
        f.write_field(field_id=0, partition_id=1, subdomain_id=0, g_order=1,
                      element=SM.TRI, field=[4])
        f.write_field(field_id=0, partition_id=1, subdomain_id=1, g_order=1,
                      element=SM.QUAD, field=[5])
        f.close()

    def test_reading(self):
        # Opening for reading
        f = File(SM.FILENAME, mode='r')

        # Structure creation
        f.create()

        # Basic MDF attributes
        model_data = f.read_model_attr()

        with self.subTest():
            self.assertEqual(model_data.description, SM.MODEL_DESCR)
            self.assertEqual(model_data.n_parts, 2)
            self.assertEqual(model_data.n_subdomains, 2)
            self.assertEqual(model_data.n_fields, 2)
            self.assertAlmostEqual(model_data.time_step, SM.MODEL_TIME_STEP)
            self.assertAlmostEqual(model_data.time_value, SM.MODEL_TIME_VALUE)
            self.assertEqual(model_data.interpolation, SM.MODEL_INTERP)

        A = SM.A['name']
        B = SM.B['name']

        # Partition attributes
        part_data = f.read_partition_attr()

        with self.subTest():
            self.assertEqual(part_data[0].name, A)
            self.assertEqual(part_data[1].name, B)

            self.assertEqual(part_data[0].description, SM.A['description'])
            self.assertEqual(part_data[1].description, SM.B['description'])

            self.assertEqual(part_data[0].n_nodes, SM.A['n_nodes'])
            self.assertEqual(part_data[1].n_nodes, SM.B['n_nodes'])

            self.assertListEqual(part_data[0].boundary_names, SM.A['bdr'])
            self.assertListEqual(part_data[1].boundary_names, SM.B['bdr'])

            self.assertListEqual(part_data[0].domain_names, SM.A['dom'])
            self.assertListEqual(part_data[1].domain_names, SM.B['dom'])

        # Nodes
        coordsA, idsA = f.read_nodes(partition_id=0)
        coordsB, idsB = f.read_nodes(partition_id=1)

        with self.subTest():
            self.assertListEqual(coordsA.tolist(), SM.COORDS[A].tolist())
            self.assertListEqual(coordsB.tolist(), SM.COORDS[B].tolist())

            self.assertListEqual(idsA.tolist(), SM.INDICES[A].tolist())
            self.assertListEqual(idsB.tolist(), SM.INDICES[B].tolist())

        # Elements
        conn, ids = f.read_elements(partition_id=0, subdomain_id=0,
                                    g_order=1, element=SM.TRI)

        with self.subTest():
            self.assertListEqual(conn.tolist(),
                                 SM.INCIDENCE[A][SM.TRI.uint].tolist())
            self.assertListEqual(ids.tolist(), [0])

        conn, ids = f.read_elements(partition_id=1, subdomain_id=0,
                                    g_order=1, element=SM.TRI)

        with self.subTest():
            self.assertListEqual(conn.tolist(),
                                 SM.INCIDENCE[B][SM.TRI.uint].tolist())
            self.assertListEqual(ids.tolist(), [1])

        conn, ids = f.read_elements(partition_id=1, subdomain_id=1,
                                    g_order=1, element=SM.QUAD)

        with self.subTest():
            self.assertListEqual(conn.tolist(),
                                 SM.INCIDENCE[B][SM.QUAD.uint].tolist())
            self.assertListEqual(ids.tolist(), [2])

        # Boundaries
        conn, ids = f.read_boundary_elements(partition_id=0, boundary_id=0,
                                             element=SM.EDGE)

        with self.subTest():
            self.assertListEqual(conn.tolist(),
                                 SM.BOUNDARIES[A]['DIRICHLET'].tolist())
            self.assertListEqual(ids.tolist(), [0])

        conn, ids = f.read_boundary_elements(partition_id=0, boundary_id=1,
                                             element=SM.EDGE)

        with self.subTest():
            self.assertListEqual(conn.tolist(),
                                 SM.BOUNDARIES[A]['NEUMANN'].tolist())
            self.assertListEqual(ids.tolist(), [1])

        conn, ids = f.read_boundary_elements(partition_id=1, boundary_id=0,
                                             element=SM.EDGE)

        with self.subTest():
            self.assertListEqual(conn.tolist(),
                                 SM.BOUNDARIES[B]['DIRICHLET'].tolist())
            self.assertListEqual(ids.tolist(), [2, 3])

        conn, ids = f.read_boundary_elements(partition_id=1, boundary_id=1,
                                             element=SM.EDGE)

        with self.subTest():
            self.assertListEqual(conn.tolist(),
                                 SM.BOUNDARIES[B]['NEUMANN'].tolist())
            self.assertListEqual(ids.tolist(), [4, 5])

        # Fields
        field1 = f.read_field(field_id=0, partition_id=0, subdomain_id=0,
                              g_order=1, element=SM.TRI)
        field2 = f.read_field(field_id=0, partition_id=1, subdomain_id=0,
                              g_order=1, element=SM.TRI)
        field3 = f.read_field(field_id=0, partition_id=1, subdomain_id=1,
                              g_order=1, element=SM.QUAD)

        with self.subTest():
            self.assertListEqual(field1.tolist(), [1, 2, 3])
            self.assertListEqual(field2.tolist(), [4])
            self.assertListEqual(field3.tolist(), [5])

        f.close()


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_create_structure.py -------------------------------------------------
