#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MDF model data
#
# @addtogroup MDF
# @author Cantão! (rfcantao@gmail.com)
#

from manticore.mdf.enums import MeshChange, Interpolation
from manticore import __version__


class ModelData:
    """Basic description of MDF base data, in memory.

    Notes:
        :class:`.ModelData` does not have a group structure, only attributes.

    Example:
        Declaring a :class:`ModelData` object describing a ``WING MODEL``,
        with 2 partitions and 5 solution fields.

        >>> from manticore.mdf import ModelData
        >>> M = ModelData('WING MODEL', n_parts=2, n_fields=5)
        >>> print(M)
    """
    DATA_MAP = {
        'description': "description",
        'nParts': "n_parts",
        'nSubDomains': "n_subdomains",
        'nFields': "n_fields",
        'timeStep': "time_step",
        'timeValue': "time_value",
        'writingConn': "writing_conn",
        'lastGeomFile': "last_geom_file"
    }

    # Some special meaning guys
    TDF_RELEASE = 'TDFRelease'
    INTERPOLATION = 'interpolation'
    MESH_CHANGE = 'meshChange'

    def __init__(self, **kwargs):
        """Constructor for a ModelData object.

        Attributes:
            n_parts (int): number of partitions (defaults to 1).

            n_subdomains (int): number of n_subdomains (defaults to 1).

            n_fields (int): number of associated data fields. Given that we
                can store only the geometry, it defaults to 0.

        Args:
            description (str): a fragment of text explaining what in the
                MDF file.

            time_step (float): time step used in the simulation. Defaults
                to 0.0

            time_value (float): present time, with respect to the time
                discretization. Defaults to 0.0.

            interpolation (Interpolation): type of interpolation (h-type or
                p-type). Defaults to h-type.

            mesh_change (MeshChange): type of mesh modification we are
                applying: it can be static (the mesh remains fixed), only
                coordinates (nodes are moving without a corresponding
                change on connectivity) or full mesh change (nodes and
                connectivity, for instance h-type refinement). Defaults to
                static meshes.

            writing_conn (bool): True if we are effectivelly writing the
                connectivity information for a mesh. Defaults to True.

            last_geom_file (str): ToDo
        """
        # Historically these parameters were given by the user. Now, they are
        # updated by the mdf.File class as needed, or in the __init__ method,
        # as needed.
        self.n_parts = 0
        self.n_subdomains = 0
        self.n_fields = 0

        params = {
            'description': '__DEFAULT__',
            'n_parts': 0,
            'n_subdomains': 0,
            'n_fields': 0,
            'time_step': 0.0,
            'time_value': 0.0,
            'interpolation': Interpolation.H_INTERPOL,
            'mesh_change': MeshChange.STATIC_MESH,
            'writing_conn': True,
            'last_geom_file': ''
        }

        # Sanity check: avoid mispellings!
        if not set(kwargs.keys()).issubset(params.keys()):
            raise RuntimeError('Please, check your arguments!')

        for param, default in params.items():
            setattr(self, param, kwargs.get(param, default))

    @classmethod
    def create_from_group(cls, group):
        model_data = cls()
        return model_data.read(group)

    def write(self, group):
        """Dumps the model data onto ``group``."""
        self.version = __version__

        # These guys are treated separately because or they come from __init__
        # (case of __version__) or because they are enums.
        group.attrs[ModelData.TDF_RELEASE] = self.version
        group.attrs[ModelData.INTERPOLATION] = self.interpolation.value
        group.attrs[ModelData.MESH_CHANGE] = self.mesh_change.value

        for name, data_name in ModelData.DATA_MAP.items():
            group.attrs[name] = self.__getattribute__(data_name)

    def read(self, group):
        """Reads the model data onto self."""
        # See comments on self.write()
        self.version = group.attrs[ModelData.TDF_RELEASE]
        self.interpolation = Interpolation(
            group.attrs[ModelData.INTERPOLATION])
        self.mesh_change = MeshChange(group.attrs[ModelData.MESH_CHANGE])

        for name, data_name in ModelData.DATA_MAP.items():
            self.__setattr__(data_name, group.attrs[name])

        return self

    def needs_mesh_storage(self):
        """
        Returns:
            bool: True if we need to create the mesh structure, False
            otherwise.
        """
        return not (self.mesh_change == MeshChange.STATIC_MESH and
                    not self.writing_conn)

    def has_fields(self):
        """
        Returns:
            bool: True if we have fields, False otherwise.
        """
        return self.n_fields > 0

    def __str__(self):
        s = 'MDF FILE "{}"\n{}\n'.format(self.description,
                                         '=' * (11 + len(self.description)))
        s += '# of Partitions [{}], Subdomains [{}], Fields[{}]\n'.format(
            self.n_parts, self.n_subdomains, self.n_fields)
        s += 'Time step = {} on T = {}\n'.format(self.time_step,
                                                 self.time_value)
        s += 'Interpolation: {}\n'.format(self.interpolation)
        s += 'Mesh change: {}\n'.format(self.mesh_change)

        return s

# -- modeldata.py -------------------------------------------------------------
