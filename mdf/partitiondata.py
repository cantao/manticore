#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MDF partition data
#
# @addtogroup MDF
# @author Cantão! (rfcantao@gmail.com)
#

from manticore.mdf.utils import Folders, Prefixes


class PartitionData:
    """Basic description of a MDF mesh partition, in memory.

    Example:
        Declaring a :class:`PartitionData` object describing a
        `__PART_2_5` partition with 450 nodes and boundaries `BDR_1` and
        `BDR_DIRICHLET`. We also have have two subdomains `FLUID` and `SOLID`.

        >>> from manticore.mdf import PartitionData
        >>> P = PartitionData('__PART_2_5', n_nodes=450,
                              boundary_names=['BDR_1', 'BDR_DIRICHLET'],
                              domain_names=['FLUID', 'SOLID'])
        >>> print(P)

        .. note:: Typically the user does not need to instantiate directly
            a object from class :class:`PartitionData`; that will be
            responsibility of a :class:`.File` object.

        Args:
            name (str): a name for the partition. At this point no attempt
                is made to preserve uniqueness of names. It defaults to None,
                so the user must set it before writing to the file.

                **HDF5 attribute**

            description (str): a fragment of text explaining what this
                partition is for. Usually partitions don't need a
                description, so it defaults to an empty string.

                **HDF5 attribute**

            n_nodes (int): number of nodes on the partition. Defaults to
                zero.

                **HDF5 attribute**

            boundary_names (list): boundaries belonging to the partition.
                Defaults to []. We can further append boundaries to the
                partition.

                **HDF5 attributes and groups**

            domain_names (list): domains belonging to the partition.
                Defaults to []. We can further append domains to the
                partition.

                **HDF5 attributes and groups**
    """
    DATA_MAP = {
        'name': "name",
        'description': "description",
        'nNodes': "n_nodes"
    }

    # Special meaning guys
    N_SUB_BOUNDARIES = 'nSubBoundaries'
    N_SUB_DOMAINS = 'nSubDomains'

    def __init__(self, name=None, **kwargs):
        params = {
            'description': '',
            'n_nodes': 0,
            'boundary_names': [],
            'domain_names': []
        }

        # Sanity check: avoid mispellings!
        if not set(kwargs.keys()).issubset(params.keys()):
            raise RuntimeError('Please, check your arguments!')

        self.name = name

        for param, default in params.items():
            setattr(self, param, kwargs.get(param, default))

    @classmethod
    def create_from_group(cls, group, idx):
        partition_data = cls()
        return partition_data.read(group, idx)

    def append_boundaries(self, boundaries):
        """Append to the list of boundaries of this partitions.

        Args:
            boundaries (list): list of boundaries to be appended.
        """
        self.boundary_names.extend(boundaries)

    def append_domains(self, domains):
        """Append to the list of domains of this partitions.

        Args:
            domains (list): list of domains to be appended.
        """
        self.domain_names.extend(domains)

    def write(self, group, idx):
        """Dumps the partition data onto `group`.

        The partition group will be created with name 'part_[idx]'.

        Args:
            group: base group we are reading from.
            idx (int): partition index.

        Notes:
            * Usually `group` will be either a geometry group or a field group.
            * Groups are created if needed.
            * The standard MDF format *does not* include the number of
              subdomains; we are adding this as of 2016.11.10.
        """
        # [group]/part_i
        partition_group = group.require_group(Prefixes.PARTITION + str(idx))

        # Writing attributes
        for name, data_name in PartitionData.DATA_MAP.items():
            partition_group.attrs[name] = self.__getattribute__(data_name)

        partition_group.attrs[
            PartitionData.N_SUB_BOUNDARIES] = self.n_boundaries()
        partition_group.attrs[
            PartitionData.N_SUB_DOMAINS] = self.n_subdomains()

        # [group]/part_i/nodes
        partition_group.require_group(Folders.NODES)

        # [group]/part_i/boundary
        boundary_group = partition_group.require_group(Folders.BOUNDARY)

        # [group]/part_i/boundary/sbound_j
        for j, boundary_name in enumerate(self.boundary_names):
            boundary = boundary_group.require_group(Prefixes.BOUNDARY + str(j))
            boundary.attrs['name'] = boundary_name

        # [group]/part_i/domain
        domain_group = partition_group.require_group(Folders.DOMAIN)

        # [group]/part_i/domain/sdom_j
        for j, domain_name in enumerate(self.domain_names):
            domain = domain_group.require_group(Prefixes.SUBDOMAIN + str(j))
            domain.attrs['name'] = domain_name

        return partition_group

    def read(self, group, idx):
        """Read the partition data onto self.

        Args:
            group: base group we are reading from.
            idx (int): partition index.
        """
        # [group]/part_i
        partition_group = group[Prefixes.PARTITION + str(idx)]

        # Reading attributes
        for name, data_name in PartitionData.DATA_MAP.items():
            self.__setattr__(data_name, partition_group.attrs[name])

        # [group]/part_i/boundary
        boundary_group = partition_group[Folders.BOUNDARY]

        # [group]/part_i/boundary/sbound_j
        n_bdr = partition_group.attrs[PartitionData.N_SUB_BOUNDARIES]

        for j in range(0, n_bdr):
            boundary = boundary_group[Prefixes.BOUNDARY + str(j)]
            boundary_name = boundary.attrs['name']
            self.boundary_names.append(boundary_name)

        # [group]/part_i/domain
        domain_group = partition_group[Folders.DOMAIN]

        # [group]/part_i/domain/sdom_j
        n_dom = partition_group.attrs[PartitionData.N_SUB_DOMAINS]

        for j in range(0, n_dom):
            domain = domain_group[Prefixes.SUBDOMAIN + str(j)]
            domain_name = domain.attrs['name']
            self.domain_names.append(domain_name)

        return self

    def n_subdomains(self):
        """
        Returns:
            int: Number of domains on this partition.
        """
        return len(self.domain_names)

    def n_boundaries(self):
        """
        Returns:
            int: Number of boundaries on this partition.
        """
        return len(self.boundary_names)

    def __repr__(self):
        s = 'Partition "{}"\n{}\n'.format(self.name, '=' *
                                          (12 + len(self.name)))
        s += 'Description: "{}"\n'.format(self.description)
        s += 'Number of nodes = {}\n'.format(self.n_nodes)

        for i, b in enumerate(self.boundary_names):
            s += '** Boundary {}: "{}"\n'.format(i, b)

        return s


# -- partitiondata.py ---------------------------------------------------------
