#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MDF field data
#
# @addtogroup MDF
# @author Cantão! (rfcantao@gmail.com)
#

import manticore
from manticore.mdf.enums import FieldType, MapType
from manticore.mdf.utils import Prefixes

logger = manticore.logging.getLogger('_MDF_APIS_')

class FieldData:
    """Basic description of a MDF field, in memory."""

    def __init__(self, name, **kwargs):
        """Constructor for a :class:`FieldData` object.

            Args:
                name (str): a name for the field. At this point no attempt is
                    made to preserve uniqueness of names.

                description (str): a fragment of text explaining what this
                    field really means. Usually fields don't need a
                    description, so it defaults to an empty string.

                field_type (FieldType): FieldType enum, defaults to None.
                    Attempts to save a field with field_type as None raises an
                    exception.

                map_type (MapType): the elemental structure this field is
                    associated to: we can have fields on elements nodes, on the
                    whole element, integration points or in the form of
                    polynomial coefficients. Defaults to None. Attempts to save
                    a field with field_type as None raises an exception.
        """
        params = {'description': '', 'field_type': None, 'map_type': None}

        # Sanity check: avoid mispellings!
        if not set(kwargs.keys()).issubset(params.keys()):
            raise RuntimeError('Please, check your arguments!')

        self.name = name

        for param, default in params.items():
            setattr(self, param, kwargs.get(param, default))
            logger.debug('Created "%s" as "%s"', param, getattr(self, param))

    @classmethod
    def create_from_group(cls, group, idx):
        field_data = cls(name=None)
        return field_data.read(group, idx)

    def write(self, group, idx):
        # [group]/field_i
        field_group = group.require_group(Prefixes.FIELD + str(idx))

        """Dumps the field data onto `group`."""
        field_group.attrs['name'] = self.name
        field_group.attrs['description'] = self.description
        field_group.attrs['type'] = self.field_type.value
        field_group.attrs['mapType'] = self.map_type.value

        logger.debug(
            'Wrote field group "{}" {}'.format(self.name, self.map_type))

        return field_group

    def read(self, group, idx):
        # [group]/field_i
        field_group = group.require_group(Prefixes.FIELD + str(idx))

        """Reads data from field `group`."""
        self.name = field_group.attrs['name']
        self.description = field_group.attrs['description']

        self.field_type = FieldType(field_group.attrs['type'])
        self.map_type = MapType(field_group.attrs['mapType'])

        return self

    def __str__(self):
        s = 'Field "{}"\n{}\n'.format(self.name, '=' * (12 + len(self.name)))
        s += 'Description: "{}"\n'.format(self.description)
        s += 'Type = {}\n'.format(self.field_type)
        s += 'Mapping = {}\n'.format(self.map_type)

        return s


# -- fielddata.py -------------------------------------------------------------
