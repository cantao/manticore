**Manticore** is a Python framework for the solution of PDEs using [Galerkin methods](https://en.wikipedia.org/wiki/Galerkin_method). Our main focus now is on the [Discontinuous Galerkin method](https://en.wikipedia.org/wiki/Discontinuous_Galerkin_method) but we do encourage you to also develop other methods using the Manticore framework. 

Manticore is released under the quite flexible [LGPL](https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License).

# Getting started

* [More detailed information](../../wiki/Home)
* [Introductory presentation](https://drive.google.com/file/d/1SF8IYPLsLy-ruRRpEjZpcbYkNf4SPv-U/view?usp=sharing)
* [Basic tutorial](../../wiki/gstart)

# Extras

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Repo owner or admin