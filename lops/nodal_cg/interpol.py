#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Standard nodal shape functions
#
# @addtogroup LOPS.NODAL_CG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from collections import namedtuple
from manticore.geom.standard_geometries import StandardGeometry
from manticore.geom.standard_geometries import geom, order, number_of_nodes
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions
from manticore.services.datatypes import FlyweightMixin

hasblInterpolationKey = namedtuple('hasblInterpolationKey',
                                   ['type', 'nip', 'face'])


class InterpolationKey(FlyweightMixin):
    """ Key to shape functions data and methods. """

    __slots__ = ['__data']

    def __init__(self, stdgeo, nip, face_id=None):
        """ Constructor.

        Args:
            stdgeo (StandardGeometry) [in]: type of h-element

            nip (int) [in]: total number of integration points where shape
                functions must be evaluated at.

        Attributes:
            __data (hasblInterpolationKey): hashable data.

        """

        self.__data = hasblInterpolationKey(stdgeo, int(nip), face_id)

    @property
    def hasblkey(self):
        return self.__data

    @property
    def type(self):
        return self.__data.type

    @property
    def nip(self):
        return self.__data.nip

    @property
    def face(self):
        return self.__data.face

    @property
    def shape(self):
        return geom(self.__data.type)

    @property
    def order(self):
        return order(self.__data.type) + 1

    @property
    def number_of_functions(self):
        return number_of_nodes(self.__data.type)

    @property
    def restricted_to_face(self):
        return self.__data.face


class InterpolationTable:
    """Table of shape functions values at integration points."""

    __slots__ = ['table', 'table_d', 'shfuncs']

    def __init__(self, key, xi1, xi2):
        """ Constructor of InterpolationTable objects.

        Args:
            key (InterpolationKey):

            xi1, xi2 (numpy.array):

        Attributes:
            table   (list(numpy.array)):

            table_d (list(numpy.array)):

        """

        nip = key.nip
        nm = key.number_of_functions

        assert xi1.shape[0] == nip

        # self.table    = [np.zeros(nm)      for i in range(nip)]
        # self.table_d  = [np.zeros((2,nm))  for i in range(nip)]
        self.table = [None for i in range(nip)]
        self.table_d = [None for i in range(nip)]

        self.__create_table(key.type, xi1, xi2)

    def __create_table(self, stdgeo, xi1, xi2):

        nip = len(self.table)

        self.shfuncs = factory_shape_functions(stdgeo)()

        for ii in range(nip):
            self.table[ii] = np.copy(
                self.shfuncs.get_shape_functions(xi1[ii], xi2[ii]))
            self.table_d[ii] = np.copy(
                self.shfuncs.get_local_derivatives(xi1[ii], xi2[ii]))

    @property
    def max_order(self):
        return self.shfuncs.get_max_order()

    @property
    def jacobian_order(self):
        return 2 * (self.shfuncs.get_max_order() - 1)

    def row(self, ii):
        """Access the ii-th table row: values of all modes at the ii-th integration point

        Args:

            ii (int): row index, [0, number_of_integration_points[

        Returns:

            np.array: the ii-th table row

        """
        return self.table[ii]

    def d_row(self, ii):
        """Access the ii-th derivative table row: values of all modes derivatives at the ii-th integration point

        Args:

            ii (int): row index, [0, number_of_integration_points[

        Returns:

            np.array: the ii-th derivative table row

        """
        return self.table_d[ii]

    def jacobian_matrix(self, coordinates, ii):
        """ Returns the Jacobian matrix at integration point ii.

        Notes:
            * The 'Jacobian matrix' is F^T, being F = grad x(l1,l2).
        """

        dsf = self.d_row(ii)

        assert coordinates.shape[0] == 2
        assert coordinates.shape[1] == dsf.shape[1]

        J = np.zeros((2, 2))

        J[0, 0] = np.dot(coordinates[0, :], dsf[0, :])  # dx1/dl1
        J[0, 1] = np.dot(coordinates[1, :], dsf[0, :])  # dx2/dl1
        J[1, 0] = np.dot(coordinates[0, :], dsf[1, :])  # dx1/dl2
        J[1, 1] = np.dot(coordinates[1, :], dsf[1, :])  # dx2/dl2

        #detJ = np.linalg.det(J)
        detJ = J[0, 0] * J[1, 1] - J[0, 1] * J[1, 0]  # 3X faster

        return J, detJ

    def det_jacobian(self, coordinates, detJ):

        assert coordinates.shape[0] == 2
        assert coordinates.shape[1] == self.d_row(0).shape[1]

        nip = len(self.table)

        assert nip == detJ.shape[0]

        for ii in range(nip):
            dsf = self.d_row(ii)

            JM00 = np.dot(coordinates[0, :], dsf[0, :])
            JM01 = np.dot(coordinates[1, :], dsf[0, :])
            JM10 = np.dot(coordinates[0, :], dsf[1, :])
            JM11 = np.dot(coordinates[1, :], dsf[1, :])

            detJ[ii] = JM00 * JM11 - JM01 * JM10

    def local_to_global(self, coordinates, x, y):

        assert coordinates.shape[0] == 2
        assert coordinates.shape[1] == self.row(0).shape[0]

        nip = len(self.table)

        assert nip == x.shape[0]
        assert nip == y.shape[0]

        for ii in range(nip):
            sf = self.row(ii)

            x[ii] = np.dot(coordinates[0, :], sf)
            y[ii] = np.dot(coordinates[1, :], sf)

    # def inv_jacobian_matrix(self, coordinates, ii):

    #     J, detJ = self.jacobian_matrix(coordinates, ii)
    #     IJ = np.linalg.inv(J)

    #     return IJ, detJ

    def inv_jacobian_matrix(self, coordinates, invJ, detJ):
        """Computes the Jacobian matrix inverse at all integration points.

        Args:

            coordinates (numpy.array) [in]

            invJ (list(numpy.array))  [out] List of 2x2 numpy.array's

            detJ (numpy.array)        [out]
        """

        assert coordinates.shape[0] == 2
        assert coordinates.shape[1] == self.d_row(0).shape[1]

        nip = len(self.table)

        assert nip == detJ.shape[0]
        assert nip == len(invJ)

        for ii in range(nip):
            dsf = self.d_row(ii)

            JM00 = np.dot(coordinates[0, :], dsf[0, :])
            JM01 = np.dot(coordinates[1, :], dsf[0, :])
            JM10 = np.dot(coordinates[0, :], dsf[1, :])
            JM11 = np.dot(coordinates[1, :], dsf[1, :])

            detJac = JM00 * JM11 - JM01 * JM10

            detJ[ii] = detJac

            invJ[ii][0, 0] = JM11 / detJac  # dl1/dx1
            invJ[ii][1, 1] = JM00 / detJac  # dl2/dx2
            invJ[ii][0, 1] = -JM01 / detJac  # dl2/dx1
            invJ[ii][1, 0] = -JM10 / detJac  # dl1/dx2

    def centroid(self, coordinates):
        xi1, xi2 = self.shfuncs.centroid()
        sf = self.shfuncs.get_shape_functions(xi1, xi2)
        return np.array([np.dot(coordinates[0, :], sf),
                         np.dot(coordinates[1, :], sf)])

    def char_length(self, coordinates):

        # Element centroid
        xi1, xi2 = self.shfuncs.centroid()
        sf = self.shfuncs.get_shape_functions(xi1, xi2)
        c = np.array([np.dot(coordinates[0, :], sf),
                      np.dot(coordinates[1, :], sf)])

        #
        fc = self.shfuncs.face_centroids()
        n  = len(fc)
        d  = np.zeros(n)

        for ii in range(n):
            sf = self.shfuncs.get_shape_functions(fc[ii][0], fc[ii][1])
            cf = np.array([np.dot(coordinates[0, :], sf),
                           np.dot(coordinates[1, :], sf)])
            d[ii] = np.linalg.norm(c-cf)

        return d.min()
