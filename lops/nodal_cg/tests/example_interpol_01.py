#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/nodal_cg/tests/example_interpol_01.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.nodal_cg.interpol import InterpolationKey
from manticore.lops.nodal_cg.interpol import InterpolationTable
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.gauss import GaussKey, JacobiGaussQuadratures
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.geomfactors import factory_colapsed_to_std_maps
from manticore.lops.modal_dg.geomfactors import factory_std_to_area_maps
from manticore.geom.standard_geometries import StandardGeometry


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example Interpolation Table 01")

    M = 10
    N = 2 * M + 1

    k1 = GaussKey(GaussPointsType.GaussLegendre, N)
    k2 = GaussKey(GaussPointsType.GaussJacobi10, N)
    GaussIWP = JacobiGaussQuadratures()

    mapp1 = factory_colapsed_to_std_maps(StdRegionType.DG_Triangle)
    mapp2 = factory_std_to_area_maps(StdRegionType.DG_Triangle)

    px1 = GaussIWP.ips(k1)
    px2 = GaussIWP.ips(k2)
    px1, px2 = np.meshgrid(px1, px2)
    px1_s, px2_s = mapp1.eval(np.ravel(px1), np.ravel(px2))
    px1_a, px2_a = mapp2.eval(px1_s, px2_s)

    ik = InterpolationKey(StandardGeometry.TRI10, N * N)

    logger.debug("Number of shape functions = %s" % (ik.number_of_functions))
    logger.debug("Number of integration points = %s\n" % (ik.nip))

    sftable = InterpolationTable(ik, px1_a, px2_a)

    ip = 0
    logger.debug("ip[%s] = (%s, %s)" % (ip, px1_s[ip], px2_s[ip]))
    logger.debug("Functions at ip[%s] = \n%s\n" % (ip, sftable.row(ip)))

    ip = 20
    logger.debug("ip[%s] = (%s, %s)" % (ip, px1_s[ip], px2_s[ip]))
    logger.debug("Functions at ip[%s] = \n%s\n" % (ip, sftable.row(ip)))

    ip = 440
    logger.debug("ip[%s] = (%s, %s)" % (ip, px1_s[ip], px2_s[ip]))
    logger.debug("Functions at ip[%s] = \n%s\n" % (ip, sftable.row(ip)))

    ip = 220
    logger.debug("ip[%s] = (%s, %s)" % (ip, px1_s[ip], px2_s[ip]))
    logger.debug("Functions at ip[%s] = \n%s\n" % (ip, sftable.row(ip)))

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
