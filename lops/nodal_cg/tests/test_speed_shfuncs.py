#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Executar (no diretorio acima do manticore):
# $ python3 -mtimeit -s 'import manticore.lops.nodal_cg.tests.test_speed_shfuncs as shf' 'shf.f1()'
# e
# python3 -mtimeit -s 'import manticore.lops.nodal_cg.tests.test_speed_shfuncs as shf' 'shf.f2()'

from sys import float_info
from manticore.lops.nodal_cg.shfuncs import l4_0, l4_1, l4_2, l4_3, l4_4, l4
from manticore.lops.nodal_cg.shfuncs import dl4_0, dl4_1, dl4_2, dl4_3, dl4_4, dl4
import numpy as np


def f1():
    L = [l4_0, l4_1, l4_2, l4_3, l4_4]
    dL = [dl4_0, dl4_1, dl4_2, dl4_3, dl4_4]

    n1d = len(L)

    size = 100
    x = np.linspace(-1, 1, size)
    H = np.zeros((n1d, size))
    dH = np.zeros((n1d, size))

    for i in range(n1d):
        H[i, :] = L[i](x)

    for i in range(n1d):
        dH[i, :] = dL[i](x)


def f2():
    L = [l4_0, l4_1, l4_2, l4_3, l4_4]
    dL = [dl4_0, dl4_1, dl4_2, dl4_3, dl4_4]

    n1d = len(L)

    size = 100
    x = np.linspace(-1, 1, size)
    H = np.zeros((n1d, size))
    dH = np.zeros((n1d, size))

    l4(x, H)
    dl4(x, dH)
