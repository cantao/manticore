#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Standard nodal shape functions
#
# @addtogroup LOPS.NODAL_CG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore.services.const as const
from manticore.geom.standard_geometries import StandardGeometry

# One dimensional Langrage polinomials on [-1, 1]:


# Degree 1
# Nodes 0(x = -1.0), 1(x = 1.0)
# 0-----------1
def l1_0(x):
    return 0.5 * (1.0 - x)


def l1_1(x):
    return 0.5 * (1.0 + x)


def l1(x, L):
    L[0, :] = -x
    L[1, :] = x
    L += 1.0
    L *= 0.5


def dl1_0(x):
    return -0.5


def dl1_1(x):
    return 0.5


def dl1(x, dL):
    dL[0, :] = -0.5
    dL[1, :] = 0.5


# Degree 2:
# Nodes 0(x = -1.0), 1(x = 1.0), 2(x = 0.0)
# 0-----2-----1
#
def l2_0(x):
    return 0.5 * x * (x - 1.0)


def l2_1(x):
    return 0.5 * x * (x + 1.0)


def l2_2(x):
    return (1.0 - x) * (x + 1.0)


def l2(x, L):
    #
    L[0, :] = x
    L[0, :] -= 1.0
    L[1, :] = x
    L[1, :] += 1.0
    #
    L[2, :] = -L[0, :] * L[1, :]
    #
    L[0, :] *= x
    L[0, :] *= 0.5
    L[1, :] *= x
    L[1, :] *= 0.5


def dl2_0(x):
    return x - 0.5


def dl2_1(x):
    return x + 0.5


def dl2_2(x):
    return -2.0 * x


def dl2(x, dL):
    dL[0, :] = x
    dL[0, :] -= 0.5
    dL[1, :] = x
    dL[1, :] += 0.5
    dL[2, :] = x
    dL[2, :] *= -2.0


# Degree 3:
# Nodes 0(x = -1.0), 1(x = 1.0), 2(x = -1/3), 3(x = 1/3)
# 0---2---3---1
#
def l3_0(x):
    return -0.5625 * (x - 1.0) * (x - 1. / 3.) * (x + 1. / 3.)


def l3_1(x):
    return 0.5625 * (x - 1. / 3.) * (x + 1. / 3.) * (x + 1.0)


def l3_2(x):
    return 1.6875 * (x - 1.0) * (x - 1. / 3.) * (x + 1.0)


def l3_3(x):
    return -1.6875 * (x - 1.0) * (x + 1. / 3.) * (x + 1.0)


def l3(x, L):
    n = L.shape[1]
    a = np.zeros(n)
    b = np.ones(n)

    a += x
    a += 1. / 3.  # a == x+1/3
    b += x  # b == x+1

    L[0, :] = x
    L[0, :] -= 1.0  # L[0,:] == x-1
    L[1, :] = x
    L[1, :] -= 1. / 3.  # L[1,:] == x-1/3
    L[3, :] = L[0, :]  # L[3,:] == x-1
    L[0, :] *= L[1, :]  # L[0,:] == (x-1)*(x-1/3)
    L[2, :] = L[0, :]  # L[2,:] == (x-1)*(x-1/3)
    #
    L[0, :] *= a  # L[0,:] == (x-1)*(x-1/3)*(x+1/3)
    L[0, :] *= -0.5625  # L[0,:] == -0.5625*(x-1)*(x-1/3)*(x+1/3)
    L[1, :] *= a  # L[1,:] == (x-1/3)*(x+1/3)
    L[1, :] *= b  # L[1,:] == (x-1/3)*(x+1/3)*(x+1)
    L[1, :] *= 0.5625  # L[1,:] == 0.5625*(x-1/3)*(x+1/3)*(x+1)
    L[2, :] *= b  # L[2,:] == (x-1)*(x-1/3)*(x+1)
    L[2, :] *= 1.6875  # L[2,:] == 1.6875*(x-1)*(x-1/3)*(x+1)
    L[3, :] *= a  # L[3,:] == (x-1)*(x+1/3)
    L[3, :] *= b  # L[3,:] == (x-1)*(x+1/3)*(x+1)
    L[3, :] *= -1.6875  # L[3,:] == -1.6875*(x-1)*(x+1/3)*(x+1)


def dl3_0(x):
    return -1.6875 * x**2 + 1.125 * x + 0.0625


def dl3_1(x):
    return 1.6875 * x**2 + 1.125 * x - 0.0625


def dl3_2(x):
    return 5.0625 * x**2 - 1.125 * x - 1.6875


def dl3_3(x):
    return -5.0625 * x**2 - 1.125 * x + 1.6875


def dl3(x, dL):
    n = dL.shape[1]
    a = np.zeros(n)
    b = np.zeros(n)
    a += x
    a *= x
    b += x
    b *= 1.125

    dL[0, :] = a
    dL[0, :] *= -1.6875
    dL[0, :] += b
    dL[0, :] += 0.0625
    dL[1, :] = a
    dL[1, :] *= 1.6875
    dL[1, :] += b
    dL[1, :] -= 0.0625
    dL[2, :] = a
    dL[2, :] *= 5.0625
    dL[2, :] -= b
    dL[2, :] -= 1.6875
    dL[3, :] = a
    dL[3, :] *= -5.0625
    dL[3, :] -= b
    dL[3, :] += 1.6875


# Degree 4:
# Nodes 0(x = -1.0), 1(x = 1.0), 2(x = -0.5), 3(x = 0.0), 4(x = 0.5)
# 0--2--3--4--1
#
def l4_0(x):
    return (2. / 3.) * x * (x - 1.0) * (x - 0.5) * (x + 0.5)


def l4_1(x):
    return (2. / 3.) * x * (x - 0.5) * (x + 0.5) * (x + 1.0)


def l4_2(x):
    return -(8. / 3.) * x * (x - 1.0) * (x - 0.5) * (x + 1.0)


def l4_3(x):
    return 4.0 * (x - 1.0) * (x - 0.5) * (x + 0.5) * (x + 1.0)


def l4_4(x):
    return -(8. / 3.) * x * (x - 1.0) * (x + 0.5) * (x + 1.0)


def l4(x, L):
    n = L.shape[1]
    a = np.zeros(n)
    b = np.zeros(n)
    c = np.zeros(n)
    d = np.ones(n)

    a += x
    a -= 1.0  # a == (x - 1.0)
    b += x
    b -= 0.5  # b == (x - 0.5)
    c += x
    c += 0.5  # c == (x + 0.5)
    d += x  # d == (x + 1.0)

    L[0, :] = x
    L[0, :] *= 2. / 3.
    L[0, :] *= b
    L[0, :] *= c
    L[1, :] = L[0, :]
    L[0, :] *= a
    L[1, :] *= d
    L[2, :] = x
    L[2, :] *= -8. / 3.
    L[2, :] *= a
    L[2, :] *= d
    L[4, :] = L[2, :]
    L[2, :] *= b
    L[4, :] *= c
    L[3, :] = a
    L[3, :] *= b
    L[3, :] *= c
    L[3, :] *= d
    L[3, :] *= 4.


def dl4_0(x):
    return (8. / 3.) * x**3 - 2.0 * x**2 - (1. / 3.) * x + 1. / 6.


def dl4_1(x):
    return (8. / 3.) * x**3 + 2.0 * x**2 - (1. / 3.) * x - 1. / 6.


def dl4_2(x):
    return -(32. / 3.) * x**3 + 4.0 * x**2 + (16. / 3.) * x - 4. / 3.


def dl4_3(x):
    return x * (16.0 * x**2 - 10.0)


def dl4_4(x):
    return -(32. / 3.) * x**3 - 4.0 * x**2 + (16. / 3.) * x + 4. / 3.


def dl4(x, dL):
    n = dL.shape[1]
    a = np.zeros(n)
    b = np.zeros(n)
    c = np.zeros(n)
    a += x
    a *= x
    a *= x
    a *= 8. / 3.  # a == (8./3.)*x**3
    b += x
    b *= x
    b *= 2.  # b == 2.0*x**2
    c += x
    c *= 1. / 3.  # c == (1./3.)*x

    dL[0, :] = a
    dL[0, :] -= b
    dL[0, :] -= c
    dL[0, :] += 1. / 6.
    dL[1, :] = a
    dL[1, :] += b
    dL[1, :] -= c
    dL[1, :] -= 1. / 6.

    a *= -4.  # a == -(32./3.)*x**3
    b *= 2.  # b == 4.0*x**2
    c *= 16.  # c == (16./3.)*x

    dL[2, :] = a
    dL[2, :] += b
    dL[2, :] += c
    dL[2, :] -= 4. / 3.
    dL[4, :] = a
    dL[4, :] -= b
    dL[4, :] += c
    dL[4, :] += 4. / 3.

    b *= 4.  # b == 16.*x**2

    dL[3, :] = b
    dL[3, :] -= 10.0
    dL[3, :] *= x


#-------------------------------------------------------------------------------


def factory_shape_functions(mesh_entity_type):

    UU = const.INVALID_RESULT

    class BaseShapeFunctions:

        __slots__ = ['h', 'dh', 'nsf', 'order', 'fnodes']

        def __init__(self):
            self.nsf = 1
            self.order = 0
            self.fnodes = []

        def set_storage(self):
            self.h = np.zeros(self.nsf)
            self.dh = np.zeros((2, self.nsf))

        def number_shape_functions(self):
            return self.nsf

        def get_order(self):
            return self.order

        def get_max_order(self):
            return self.order

        def jacobian_order(self):
            return 2 * (self.get_max_order() - 1)

        def get_shape_functions(self, xi1, xi2):

            self.eval_functions(xi1, xi2)

            return self.h

        def get_local_derivatives(self, xi1, xi2):

            self.eval_derivatives(xi1, xi2)

            return self.dh

        def get_face_nodes(self, face):
            return self.fnodes[face]

        def centroid(self):
            """For triangles. 

            This method is substituted by BaseQuadShapeFunctions for
            quadrangles.
            """
            return 1./3., 1./3.

        def face_centroids(self):
            """For triangles. 

            This method is substituted by BaseQuadShapeFunctions for
            quadrangles.
            """
            return [(0.5,0.5), (0.5,0.), (0.,0.5)]

        def face_vertices(self):
            """For triangles. 

            This method is substituted by BaseQuadShapeFunctions for
            quadrangles.
            """
            return [(0, 1), (2, 0), (1, 2)]

    class BaseQuadShapeFunctions(BaseShapeFunctions):

        __slots__ = ['n1d', 'Lxi1', 'Lxi2', 'dLxi1', 'dLxi2', 'L', 'dL']

        def __init__(self):
            BaseShapeFunctions.__init__(self)
            self.L = []
            self.dL = []

        def set_storage(self):
            self.n1d = len(self.L)
            self.nsf = self.n1d * self.n1d
            self.Lxi1 = np.zeros(self.n1d)
            self.Lxi2 = np.zeros(self.n1d)
            self.dLxi1 = np.zeros(self.n1d)
            self.dLxi2 = np.zeros(self.n1d)

            BaseShapeFunctions.set_storage(self)

        def get_max_order(self):
            return 2 * self.order

        def eval_1d_functions(self, xi1, xi2):

            for i in range(self.n1d):
                self.Lxi1[i] = self.L[i](xi1)
                self.Lxi2[i] = self.L[i](xi2)

        def eval_1d_derivatives(self, xi1, xi2):

            for i in range(self.n1d):
                self.dLxi1[i] = self.dL[i](xi1)
                self.dLxi2[i] = self.dL[i](xi2)

        def centroid(self):
            return 0., 0.

        def face_centroids(self):
            return [(0.,-1.), (0.,1.), (-1.,0.), (1.,0.)]

        def face_vertices(self):
            return [(0, 1), (2, 3), (3, 0), (1, 2)]

    if mesh_entity_type == StandardGeometry.QUAD4:

        class ShapeFunctions(BaseQuadShapeFunctions):
            """
            Lagrange shape functions for the linear quadrangle:

            3-----------2
            |           |
            |           |
            |           |
            |           |
            0-----------1

            -1 <= xi1, xi2 <= +1

            """

            __slots__ = []

            def __init__(self):

                BaseQuadShapeFunctions.__init__(self)

                self.L = [l1_0, l1_1]
                self.dL = [dl1_0, dl1_1]

                self.order = 1

                self.fnodes = [
                    [0, 1],  # face 0
                    [2, 3],  # face 1
                    [3, 0],  # face 2
                    [1, 2]
                ]  # face 3

                BaseQuadShapeFunctions.set_storage(self)

            def eval_functions(self, xi1, xi2):

                self.eval_1d_functions(xi1, xi2)

                self.h[0] = self.Lxi1[0] * self.Lxi2[0]
                self.h[1] = self.Lxi1[1] * self.Lxi2[0]
                self.h[2] = self.Lxi1[1] * self.Lxi2[1]
                self.h[3] = self.Lxi1[0] * self.Lxi2[1]

            def eval_derivatives(self, xi1, xi2):

                self.eval_1d_functions(xi1, xi2)
                self.eval_1d_derivatives(xi1, xi2)

                # Derivatives relative to xi1
                self.dh[0, 0] = self.dLxi1[0] * self.Lxi2[0]
                self.dh[0, 1] = self.dLxi1[1] * self.Lxi2[0]
                self.dh[0, 2] = self.dLxi1[1] * self.Lxi2[1]
                self.dh[0, 3] = self.dLxi1[0] * self.Lxi2[1]

                # Derivatives relative to xi2
                self.dh[1, 0] = self.Lxi1[0] * self.dLxi2[0]
                self.dh[1, 1] = self.Lxi1[1] * self.dLxi2[0]
                self.dh[1, 2] = self.Lxi1[1] * self.dLxi2[1]
                self.dh[1, 3] = self.Lxi1[0] * self.dLxi2[1]

        ## end of factory_shape_functions.ShapeFunctions<StandardGeometry.QUAD4>

        return ShapeFunctions

    elif mesh_entity_type == StandardGeometry.QUAD9:

        class ShapeFunctions(BaseQuadShapeFunctions):
            """
            Lagrange shape functions for the quadratic quadrangle:

            3-----6-----2
            |           |
            |           |
            7     8     5
            |           |
            |           |
            0-----4-----1

            -1 <= xi1, xi2 <= +1

            """

            __slots__ = []

            def __init__(self):

                BaseQuadShapeFunctions.__init__(self)

                self.L = [l2_0, l2_1, l2_2]
                self.dL = [dl2_0, dl2_1, dl2_2]

                self.order = 2

                self.fnodes = [
                    [0, 4, 1],  # face 0
                    [2, 6, 3],  # face 1
                    [3, 7, 0],  # face 2
                    [1, 5, 2]
                ]  # face 3

                BaseQuadShapeFunctions.set_storage(self)

            def eval_functions(self, xi1, xi2):

                self.eval_1d_functions(xi1, xi2)

                # Vertices
                self.h[0] = self.Lxi1[0] * self.Lxi2[0]
                self.h[1] = self.Lxi1[1] * self.Lxi2[0]
                self.h[2] = self.Lxi1[1] * self.Lxi2[1]
                self.h[3] = self.Lxi1[0] * self.Lxi2[1]

                # Faces
                self.h[4] = self.Lxi1[2] * self.Lxi2[0]
                self.h[5] = self.Lxi1[1] * self.Lxi2[2]
                self.h[6] = self.Lxi1[2] * self.Lxi2[1]
                self.h[7] = self.Lxi1[0] * self.Lxi2[2]

                # Interior
                self.h[8] = self.Lxi1[2] * self.Lxi2[2]

            def eval_derivatives(self, xi1, xi2):

                self.eval_1d_functions(xi1, xi2)
                self.eval_1d_derivatives(xi1, xi2)

                # Vertices
                # Derivatives relative to xi1
                self.dh[0, 0] = self.dLxi1[0] * self.Lxi2[0]
                self.dh[0, 1] = self.dLxi1[1] * self.Lxi2[0]
                self.dh[0, 2] = self.dLxi1[1] * self.Lxi2[1]
                self.dh[0, 3] = self.dLxi1[0] * self.Lxi2[1]

                # Derivatives relative to xi2
                self.dh[1, 0] = self.Lxi1[0] * self.dLxi2[0]
                self.dh[1, 1] = self.Lxi1[1] * self.dLxi2[0]
                self.dh[1, 2] = self.Lxi1[1] * self.dLxi2[1]
                self.dh[1, 3] = self.Lxi1[0] * self.dLxi2[1]

                # Faces
                # Derivatives relative to xi1
                self.dh[0, 4] = self.dLxi1[2] * self.Lxi2[0]
                self.dh[0, 5] = self.dLxi1[1] * self.Lxi2[2]
                self.dh[0, 6] = self.dLxi1[2] * self.Lxi2[1]
                self.dh[0, 7] = self.dLxi1[0] * self.Lxi2[2]

                # Derivatives relative to xi2
                self.dh[1, 4] = self.Lxi1[2] * self.dLxi2[0]
                self.dh[1, 5] = self.Lxi1[1] * self.dLxi2[2]
                self.dh[1, 6] = self.Lxi1[2] * self.dLxi2[1]
                self.dh[1, 7] = self.Lxi1[0] * self.dLxi2[2]

                # Interior
                self.dh[0, 8] = self.dLxi1[2] * self.Lxi2[2]
                self.dh[1, 8] = self.Lxi1[2] * self.dLxi2[2]

        ## end of factory_shape_functions.ShapeFunctions<StandardGeometry.QUAD9>

        return ShapeFunctions

    elif mesh_entity_type == StandardGeometry.QUAD16:

        class ShapeFunctions(BaseQuadShapeFunctions):
            """
            Lagrange shape functions for the cubic quadrangle:

            3---9---8---2
            |           |
           10  15   14  7
            |           |
           11  12   13  6
            |           |
            0---4---5---1

            -1 <= xi1, xi2 <= +1

            """

            __slots__ = []

            def __init__(self):

                BaseQuadShapeFunctions.__init__(self)

                self.L = [l3_0, l3_1, l3_2, l3_3]
                self.dL = [dl3_0, dl3_1, dl3_2, dl3_3]

                self.order = 3

                self.fnodes = [
                    [0, 4, 5, 1],  # face 0
                    [2, 8, 9, 3],  # face 1
                    [3, 10, 11, 0],  # face 2
                    [1, 6, 7, 2]
                ]  # face 3

                BaseQuadShapeFunctions.set_storage(self)

            def eval_functions(self, xi1, xi2):

                self.eval_1d_functions(xi1, xi2)

                # Vertices
                self.h[0] = self.Lxi1[0] * self.Lxi2[0]
                self.h[1] = self.Lxi1[1] * self.Lxi2[0]
                self.h[2] = self.Lxi1[1] * self.Lxi2[1]
                self.h[3] = self.Lxi1[0] * self.Lxi2[1]

                # Faces
                self.h[4] = self.Lxi1[2] * self.Lxi2[0]
                self.h[5] = self.Lxi1[3] * self.Lxi2[0]
                self.h[6] = self.Lxi1[1] * self.Lxi2[2]
                self.h[7] = self.Lxi1[1] * self.Lxi2[3]
                self.h[8] = self.Lxi1[3] * self.Lxi2[1]
                self.h[9] = self.Lxi1[2] * self.Lxi2[1]
                self.h[10] = self.Lxi1[0] * self.Lxi2[3]
                self.h[11] = self.Lxi1[0] * self.Lxi2[2]

                # Interior
                self.h[12] = self.Lxi1[2] * self.Lxi2[2]
                self.h[13] = self.Lxi1[3] * self.Lxi2[2]
                self.h[14] = self.Lxi1[3] * self.Lxi2[3]
                self.h[15] = self.Lxi1[2] * self.Lxi2[3]

            def eval_derivatives(self, xi1, xi2):

                self.eval_1d_functions(xi1, xi2)
                self.eval_1d_derivatives(xi1, xi2)

                # Vertices
                # Derivatives relative to xi1
                self.dh[0, 0] = self.dLxi1[0] * self.Lxi2[0]
                self.dh[0, 1] = self.dLxi1[1] * self.Lxi2[0]
                self.dh[0, 2] = self.dLxi1[1] * self.Lxi2[1]
                self.dh[0, 3] = self.dLxi1[0] * self.Lxi2[1]

                # Derivatives relative to xi2
                self.dh[1, 0] = self.Lxi1[0] * self.dLxi2[0]
                self.dh[1, 1] = self.Lxi1[1] * self.dLxi2[0]
                self.dh[1, 2] = self.Lxi1[1] * self.dLxi2[1]
                self.dh[1, 3] = self.Lxi1[0] * self.dLxi2[1]

                # Faces
                # Derivatives relative to xi1
                self.dh[0, 4] = self.dLxi1[2] * self.Lxi2[0]
                self.dh[0, 5] = self.dLxi1[3] * self.Lxi2[0]
                self.dh[0, 6] = self.dLxi1[1] * self.Lxi2[2]
                self.dh[0, 7] = self.dLxi1[1] * self.Lxi2[3]
                self.dh[0, 8] = self.dLxi1[3] * self.Lxi2[1]
                self.dh[0, 9] = self.dLxi1[2] * self.Lxi2[1]
                self.dh[0, 10] = self.dLxi1[0] * self.Lxi2[3]
                self.dh[0, 11] = self.dLxi1[0] * self.Lxi2[2]

                # Derivatives relative to xi2
                self.dh[1, 4] = self.Lxi1[2] * self.dLxi2[0]
                self.dh[1, 5] = self.Lxi1[3] * self.dLxi2[0]
                self.dh[1, 6] = self.Lxi1[1] * self.dLxi2[2]
                self.dh[1, 7] = self.Lxi1[1] * self.dLxi2[3]
                self.dh[1, 8] = self.Lxi1[3] * self.dLxi2[1]
                self.dh[1, 9] = self.Lxi1[2] * self.dLxi2[1]
                self.dh[1, 10] = self.Lxi1[0] * self.dLxi2[3]
                self.dh[1, 11] = self.Lxi1[0] * self.dLxi2[2]

                # Interior
                # Derivatives relative to xi1
                self.dh[0, 12] = self.dLxi1[2] * self.Lxi2[2]
                self.dh[0, 13] = self.dLxi1[3] * self.Lxi2[2]
                self.dh[0, 14] = self.dLxi1[3] * self.Lxi2[3]
                self.dh[0, 15] = self.dLxi1[2] * self.Lxi2[3]

                # Derivatives relative to xi2
                self.dh[1, 12] = self.Lxi1[2] * self.dLxi2[2]
                self.dh[1, 13] = self.Lxi1[3] * self.dLxi2[2]
                self.dh[1, 14] = self.Lxi1[3] * self.dLxi2[3]
                self.dh[1, 15] = self.Lxi1[2] * self.dLxi2[3]

        ## end of
        ## factory_shape_functions.ShapeFunctions<StandardGeometry.QUAD16>

        return ShapeFunctions

    elif mesh_entity_type == StandardGeometry.QUAD25:

        class ShapeFunctions(BaseQuadShapeFunctions):
            """
            Lagrange shape functions for the quartic quadrangle:

            3--12--11--10--2
            |              |
           13  19  22  18  9
           14  23  24  21  8
           15  16  20  17  7
            |              |
            0---4---5---6--1

            -1 <= xi1, xi2 <= +1

            """
            __slots__ = []

            def __init__(self):

                BaseQuadShapeFunctions.__init__(self)

                self.L = [l4_0, l4_1, l4_2, l4_3, l4_4]
                self.dL = [dl4_0, dl4_1, dl4_2, dl4_3, dl4_4]

                self.order = 4

                self.fnodes = [
                    [0, 4, 5, 6, 1],  # face 0
                    [2, 10, 11, 12, 3],  # face 1
                    [3, 13, 14, 15, 0],  # face 2
                    [1, 7, 8, 9, 2]
                ]  # face 3

                BaseQuadShapeFunctions.set_storage(self)

            def eval_functions(self, xi1, xi2):

                self.eval_1d_functions(xi1, xi2)

                # Vertices
                self.h[0] = self.Lxi1[0] * self.Lxi2[0]
                self.h[1] = self.Lxi1[1] * self.Lxi2[0]
                self.h[2] = self.Lxi1[1] * self.Lxi2[1]
                self.h[3] = self.Lxi1[0] * self.Lxi2[1]

                # Faces
                self.h[4] = self.Lxi1[2] * self.Lxi2[0]
                self.h[5] = self.Lxi1[3] * self.Lxi2[0]
                self.h[6] = self.Lxi1[4] * self.Lxi2[0]
                self.h[7] = self.Lxi1[1] * self.Lxi2[2]
                self.h[8] = self.Lxi1[1] * self.Lxi2[3]
                self.h[9] = self.Lxi1[1] * self.Lxi2[4]
                self.h[10] = self.Lxi1[4] * self.Lxi2[1]
                self.h[11] = self.Lxi1[3] * self.Lxi2[1]
                self.h[12] = self.Lxi1[2] * self.Lxi2[1]
                self.h[13] = self.Lxi1[0] * self.Lxi2[4]
                self.h[14] = self.Lxi1[0] * self.Lxi2[3]
                self.h[15] = self.Lxi1[0] * self.Lxi2[2]

                # Interior
                self.h[16] = self.Lxi1[2] * self.Lxi2[2]
                self.h[17] = self.Lxi1[4] * self.Lxi2[2]
                self.h[18] = self.Lxi1[4] * self.Lxi2[4]
                self.h[19] = self.Lxi1[2] * self.Lxi2[4]

                self.h[20] = self.Lxi1[3] * self.Lxi2[2]
                self.h[21] = self.Lxi1[4] * self.Lxi2[3]
                self.h[22] = self.Lxi1[3] * self.Lxi2[4]
                self.h[23] = self.Lxi1[2] * self.Lxi2[3]

                self.h[24] = self.Lxi1[3] * self.Lxi2[3]

            def eval_derivatives(self, xi1, xi2):

                self.eval_1d_functions(xi1, xi2)
                self.eval_1d_derivatives(xi1, xi2)

                # Vertices
                # Derivatives relative to xi1
                self.dh[0, 0] = self.dLxi1[0] * self.Lxi2[0]
                self.dh[0, 1] = self.dLxi1[1] * self.Lxi2[0]
                self.dh[0, 2] = self.dLxi1[1] * self.Lxi2[1]
                self.dh[0, 3] = self.dLxi1[0] * self.Lxi2[1]

                # Derivatives relative to xi2
                self.dh[1, 0] = self.Lxi1[0] * self.dLxi2[0]
                self.dh[1, 1] = self.Lxi1[1] * self.dLxi2[0]
                self.dh[1, 2] = self.Lxi1[1] * self.dLxi2[1]
                self.dh[1, 3] = self.Lxi1[0] * self.dLxi2[1]

                # Faces
                self.dh[0, 4] = self.dLxi1[2] * self.Lxi2[0]
                self.dh[0, 5] = self.dLxi1[3] * self.Lxi2[0]
                self.dh[0, 6] = self.dLxi1[4] * self.Lxi2[0]
                self.dh[0, 7] = self.dLxi1[1] * self.Lxi2[2]
                self.dh[0, 8] = self.dLxi1[1] * self.Lxi2[3]
                self.dh[0, 9] = self.dLxi1[1] * self.Lxi2[4]
                self.dh[0, 10] = self.dLxi1[4] * self.Lxi2[1]
                self.dh[0, 11] = self.dLxi1[3] * self.Lxi2[1]
                self.dh[0, 12] = self.dLxi1[2] * self.Lxi2[1]
                self.dh[0, 13] = self.dLxi1[0] * self.Lxi2[4]
                self.dh[0, 14] = self.dLxi1[0] * self.Lxi2[3]
                self.dh[0, 15] = self.dLxi1[0] * self.Lxi2[2]

                self.dh[1, 4] = self.Lxi1[2] * self.dLxi2[0]
                self.dh[1, 5] = self.Lxi1[3] * self.dLxi2[0]
                self.dh[1, 6] = self.Lxi1[4] * self.dLxi2[0]
                self.dh[1, 7] = self.Lxi1[1] * self.dLxi2[2]
                self.dh[1, 8] = self.Lxi1[1] * self.dLxi2[3]
                self.dh[1, 9] = self.Lxi1[1] * self.dLxi2[4]
                self.dh[1, 10] = self.Lxi1[4] * self.dLxi2[1]
                self.dh[1, 11] = self.Lxi1[3] * self.dLxi2[1]
                self.dh[1, 12] = self.Lxi1[2] * self.dLxi2[1]
                self.dh[1, 13] = self.Lxi1[0] * self.dLxi2[4]
                self.dh[1, 14] = self.Lxi1[0] * self.dLxi2[3]
                self.dh[1, 15] = self.Lxi1[0] * self.dLxi2[2]

                # Interior
                self.dh[0, 16] = self.dLxi1[2] * self.Lxi2[2]
                self.dh[0, 17] = self.dLxi1[4] * self.Lxi2[2]
                self.dh[0, 18] = self.dLxi1[4] * self.Lxi2[4]
                self.dh[0, 19] = self.dLxi1[2] * self.Lxi2[4]
                self.dh[0, 20] = self.dLxi1[3] * self.Lxi2[2]
                self.dh[0, 21] = self.dLxi1[4] * self.Lxi2[3]
                self.dh[0, 22] = self.dLxi1[3] * self.Lxi2[4]
                self.dh[0, 23] = self.dLxi1[2] * self.Lxi2[3]
                self.dh[0, 24] = self.dLxi1[3] * self.Lxi2[3]

                self.dh[1, 16] = self.Lxi1[2] * self.dLxi2[2]
                self.dh[1, 17] = self.Lxi1[4] * self.dLxi2[2]
                self.dh[1, 18] = self.Lxi1[4] * self.dLxi2[4]
                self.dh[1, 19] = self.Lxi1[2] * self.dLxi2[4]
                self.dh[1, 20] = self.Lxi1[3] * self.dLxi2[2]
                self.dh[1, 21] = self.Lxi1[4] * self.dLxi2[3]
                self.dh[1, 22] = self.Lxi1[3] * self.dLxi2[4]
                self.dh[1, 23] = self.Lxi1[2] * self.dLxi2[3]
                self.dh[1, 24] = self.Lxi1[3] * self.dLxi2[3]

        ## end of
        ## factory_shape_functions.ShapeFunctions<StandardGeometry.QUAD25>

        return ShapeFunctions

    elif mesh_entity_type == StandardGeometry.TRI3:

        class ShapeFunctions(BaseShapeFunctions):
            """
            Shape functions for the linear triangle:

            2
            |\
            |   \
            |      \
            |         \
            0-----------1

            0 <= L1, L2 <= 1

            node 0: (1, 0)
            node 1: (0, 1)
            node 2: (0, 0)

            """

            __slots__ = []

            def __init__(self):

                BaseShapeFunctions.__init__(self)
                self.nsf = 3
                self.order = 1
                self.fnodes = [
                    [0, 1],  # face 0
                    [2, 0],  # face 1
                    [1, 2],  # face 2
                    [UU, UU]
                ]  # face 3

                BaseShapeFunctions.set_storage(self)

            def eval_functions(self, L1, L2):

                self.h[0] = L1
                self.h[1] = L2
                self.h[2] = 1.0 - L1 - L2

            def eval_derivatives(self, L1, L2):
                # Derivatives relative to L1
                self.dh[0, 0] = 1.
                self.dh[0, 1] = 0.
                self.dh[0, 2] = -1.

                # Derivatives relative to L2
                self.dh[1, 0] = 0.
                self.dh[1, 1] = 1.
                self.dh[1, 2] = -1.

        ## end of factory_shape_functions.ShapeFunctions<StandardGeometry.TRI3>

        return ShapeFunctions

    elif mesh_entity_type == StandardGeometry.TRI6:

        class ShapeFunctions(BaseShapeFunctions):
            """
            Shape functions for the quadratic triangle:

            2
            |\
            |   \
            5    4
            |      \
            |         \
            0-----3-----1

            0 <= L1, L2 <= 1

            node 0: (1, 0)
            node 1: (0, 1)
            node 2: (0, 0)
            node 3: (1/2, 1/2)
            node 4: (  0, 1/2)
            node 5: (1/2,   0)

            """

            __slots__ = []

            def __init__(self):

                BaseShapeFunctions.__init__(self)
                self.nsf = 6
                self.order = 2
                self.fnodes = [
                    [0, 3, 1],  # face 0
                    [2, 5, 0],  # face 1
                    [1, 4, 2],  # face 2
                    [UU, UU, UU]
                ]  # face 3

                BaseShapeFunctions.set_storage(self)

            def eval_functions(self, L1, L2):

                L3 = 1.0 - L1 - L2

                # Vertices
                self.h[0] = L1 * (2.0 * L1 - 1.0)
                self.h[1] = L2 * (2.0 * L2 - 1.0)
                self.h[2] = L3 * (2.0 * L3 - 1.0)

                # Faces
                self.h[3] = 4.0 * L1 * L2
                self.h[4] = 4.0 * L2 * L3
                self.h[5] = 4.0 * L1 * L3

            def eval_derivatives(self, L1, L2):
                # Derivatives relative to L1
                self.dh[0, 0] = 4.0 * L1 - 1.0
                self.dh[0, 1] = 0.
                self.dh[0, 2] = 4.0 * L1 + 4.0 * L2 - 3.0
                self.dh[0, 3] = 4.0 * L2
                self.dh[0, 4] = -4.0 * L2
                self.dh[0, 5] = -8.0 * L1 - 4.0 * L2 + 4.0

                # Derivatives relative to L2
                self.dh[1, 0] = 0.
                self.dh[1, 1] = 4.0 * L2 - 1.0
                self.dh[1, 2] = 4.0 * L1 + 4.0 * L2 - 3.0
                self.dh[1, 3] = 4.0 * L1
                self.dh[1, 4] = -4.0 * L1 - 8.0 * L2 + 4.0
                self.dh[1, 5] = -4.0 * L1

        ## end of factory_shape_functions.ShapeFunctions<StandardGeometry.TRI6>

        return ShapeFunctions

    elif mesh_entity_type == StandardGeometry.TRI10:

        class ShapeFunctions(BaseShapeFunctions):
            """
            Shape functions for the cubic triangle:

            2
            |\
            |   \
            7    6
            |      \
            8   9   5
            |         \
            0---3---4---1

            0 <= L1, L2 <= 1

            node 0: (1, 0)
            node 1: (0, 1)
            node 2: (0, 0)
            node 3: (2/3, 1/3)
            node 4: (1/3, 2/3)
            node 5: (  0, 2/3)
            node 6: (  0, 1/3)
            node 7: (1/3,   0)
            node 8: (2/3,   0)
            node 9: (1/3, 1/3)

            """

            __slots__ = []

            def __init__(self):

                BaseShapeFunctions.__init__(self)
                self.nsf = 10
                self.order = 3
                self.fnodes = [
                    [0, 3, 4, 1],  # face 0
                    [2, 7, 8, 0],  # face 1
                    [1, 5, 6, 2],  # face 2
                    [UU, UU, UU, UU]
                ]  # face 3
                BaseShapeFunctions.set_storage(self)

            def eval_functions(self, L1, L2):

                L3 = 1.0 - L1 - L2

                # Vertices
                self.h[0] = 0.5 * L1 * (3.0 * L1 - 1.0) * (3.0 * L1 - 2.0)
                self.h[1] = 0.5 * L2 * (3.0 * L2 - 1.0) * (3.0 * L2 - 2.0)
                self.h[2] = 0.5 * L3 * (3.0 * L3 - 1.0) * (3.0 * L3 - 2.0)

                # Faces
                self.h[3] = 4.5 * L1 * L2 * (3.0 * L1 - 1.0)
                self.h[4] = 4.5 * L2 * L1 * (3.0 * L2 - 1.0)
                self.h[5] = 4.5 * L2 * L3 * (3.0 * L2 - 1.0)
                self.h[6] = 4.5 * L3 * L2 * (3.0 * L3 - 1.0)
                self.h[7] = 4.5 * L3 * L1 * (3.0 * L3 - 1.0)
                self.h[8] = 4.5 * L1 * L3 * (3.0 * L1 - 1.0)

                # Interior
                self.h[9] = 27.0 * L1 * L2 * L3

            def eval_derivatives(self, L1, L2):
                # Derivatives relative to L1
                self.dh[0, 0] = 13.5 * L1**2 - 9.0 * L1 + 1.0
                self.dh[0, 1] = 0.
                self.dh[0, 2] = (-13.5 * L1**2 - 27.0 * L1 * L2 + 18.0 * L1 -
                                 13.5 * L2**2 + 18.0 * L2 - 5.5)
                self.dh[0, 3] = L2 * (27.0 * L1 - 4.5)
                self.dh[0, 4] = L2 * (13.5 * L2 - 4.5)
                self.dh[0, 5] = L2 * (-13.5 * L2 + 4.5)
                self.dh[0, 6] = L2 * (27.0 * L1 + 27.0 * L2 - 22.5)
                self.dh[0, 7] = (40.5 * L1**2 + 54.0 * L1 * L2 - 45.0 * L1 +
                                 13.5 * L2**2 - 22.5 * L2 + 9.0)
                self.dh[0, 8] = (-40.5 * L1**2 - 27.0 * L1 * L2 + 36.0 * L1 +
                                 4.5 * L2 - 4.5)
                self.dh[0, 9] = L2 * (-54.0 * L1 - 27.0 * L2 + 27.0)

                # Derivatives relative to L2
                self.dh[1, 0] = 0.
                self.dh[1, 1] = 13.5 * L2**2 - 9.0 * L2 + 1.0
                self.dh[1, 2] = (-13.5 * L1**2 - 27.0 * L1 * L2 + 18.0 * L1 -
                                 13.5 * L2**2 + 18.0 * L2 - 5.5)
                self.dh[1, 3] = L1 * (13.5 * L1 - 4.5)
                self.dh[1, 4] = L1 * (27.0 * L2 - 4.5)
                self.dh[1, 5] = (-27.0 * L1 * L2 + 4.5 * L1 - 40.5 * L2**2 +
                                 36.0 * L2 - 4.5)
                self.dh[1, 6] = (13.5 * L1**2 + 54.0 * L1 * L2 - 22.5 * L1 +
                                 40.5 * L2**2 - 45.0 * L2 + 9.0)
                self.dh[1, 7] = L1 * (27.0 * L1 + 27.0 * L2 - 22.5)
                self.dh[1, 8] = L1 * (-13.5 * L1 + 4.5)
                self.dh[1, 9] = L1 * (-27.0 * L1 - 54.0 * L2 + 27.0)

        ## end of factory_shape_functions.ShapeFunctions<StandardGeometry.TRI10>

        return ShapeFunctions

    elif mesh_entity_type == StandardGeometry.TRI15:

        class ShapeFunctions(BaseShapeFunctions):
            """
            Shape functions for the quartic triangle:

             2
            |  \
            |    \
            9     8
            10 14   7
            11 12 13  6
            |          \
            0--3--4--5--1

            0 <= L1, L2 <= 1

            node  0: (1, 0)
            node  1: (0, 1)
            node  2: (0, 0)
            node  3: (3/4, 1/4)
            node  4: (2/4, 2/4)
            node  5: (1/4, 3/4)
            node  6: (  0, 3/4)
            node  7: (  0, 2/4)
            node  8: (  0, 1/4)
            node  9: (1/4,   0)
            node 10: (2/4,   0)
            node 11: (3/4,   0)
            node 12: (2/4, 1/4)
            node 13: (1/4, 2/4)
            node 14: (1/4, 1/4)

            """
            __slots__ = []

            def __init__(self):

                BaseShapeFunctions.__init__(self)
                self.nsf = 15
                self.order = 4
                self.fnodes = [
                    [0, 3, 4, 5, 1],  # face 0
                    [2, 9, 10, 11, 0],  # face 1
                    [1, 6, 7, 8, 2],  # face 2
                    [UU, UU, UU, UU, UU]
                ]  # face 3
                BaseShapeFunctions.set_storage(self)

            def eval_functions(self, L1, L2):

                L3 = 1.0 - L1 - L2

                # Vertices
                self.h[0] = L1 * (4.0 * L1 - 1.0) * (2.0 * L1 - 1.0) * (
                    4.0 * L1 - 3.0) / 3.0
                self.h[1] = L2 * (4.0 * L2 - 1.0) * (2.0 * L2 - 1.0) * (
                    4.0 * L2 - 3.0) / 3.0
                self.h[2] = L3 * (4.0 * L3 - 1.0) * (2.0 * L3 - 1.0) * (
                    4.0 * L3 - 3.0) / 3.0

                # Faces
                self.h[3] = 16.0 * L1 * L2 * (4.0 * L1 - 1.0) * (
                    2.0 * L1 - 1.0) / 3.0
                self.h[4] = 4.0 * L1 * L2 * (4.0 * L1 - 1.0) * (4.0 * L2 - 1.0)
                self.h[5] = 16.0 * L1 * L2 * (4.0 * L2 - 1.0) * (
                    2.0 * L2 - 1.0) / 3.0
                self.h[6] = 16.0 * L2 * L3 * (4.0 * L2 - 1.0) * (
                    2.0 * L2 - 1.0) / 3.0
                self.h[7] = 4.0 * L2 * L3 * (4.0 * L2 - 1.0) * (4.0 * L3 - 1.0)
                self.h[8] = 16.0 * L2 * L3 * (4.0 * L3 - 1.0) * (
                    2.0 * L3 - 1.0) / 3.0
                self.h[9] = 16.0 * L3 * L1 * (4.0 * L3 - 1.0) * (
                    2.0 * L3 - 1.0) / 3.0
                self.h[10] = 4.0 * L3 * L1 * (4.0 * L3 - 1.0) * (
                    4.0 * L1 - 1.0)
                self.h[11] = 16.0 * L3 * L1 * (4.0 * L1 - 1.0) * (
                    2.0 * L1 - 1.0) / 3.0

                # Interior
                self.h[12] = 32.0 * L1 * L2 * L3 * (4.0 * L1 - 1.0)
                self.h[13] = 32.0 * L1 * L2 * L3 * (4.0 * L2 - 1.0)
                self.h[14] = 32.0 * L1 * L2 * L3 * (4.0 * L3 - 1.0)

            def eval_derivatives(self, L1, L2):
                # Derivatives relative to L1
                self.dh[0, 0] = 128. * (
                    L1**3) / 3. - 48. * L1**2 + 44. * L1 / 3. - 1.
                self.dh[0, 1] = 0.
                self.dh[0, 2] = (
                    128. * (L1**3) / 3. + 128. *
                    (L1**2) * L2 - 80. * L1**2 + 128. * L1 * L2**2 -
                    160. * L1 * L2 + 140. * L1 / 3. + 128. *
                    (L2**3) / 3. - 80. * L2**2 + 140. * L2 / 3. - 25. / 3.)
                self.dh[0, 3] = L2 * (128. * L1**2 - 64. * L1 + 16. / 3.)
                self.dh[0, 4] = L2 * (32. * L1 - 4.) * (4. * L2 - 1.)
                self.dh[0, 5] = L2 * (128. *
                                      (L2**2) / 3. - 32. * L2 + 16. / 3.)
                self.dh[0, 6] = L2 * (-128. *
                                      (L2**2) / 3. + 32. * L2 - 16. / 3.)
                self.dh[0, 7] = L2 * (4. * L2 - 1.) * (
                    32. * L1 + 32. * L2 - 28.)
                self.dh[0, 8] = L2 * (
                    -128. * L1**2 - 256. * L1 * L2 + 192. * L1 - 128. * L2**2 +
                    192. * L2 - 208. / 3.)
                self.dh[0, 9] = (
                    -512. * (L1**3) / 3. - 384. * L1**2 * L2 + 288. * L1**2 -
                    256. * L1 * L2**2 + 384. * L1 * L2 - 416. * L1 / 3. -
                    128. * (L2**3) / 3. + 96. * L2**2 - 208. * L2 / 3. + 16.)
                self.dh[0, 10] = (
                    256. * L1**3 + 384. * L1**2 * L2 - 384. * L1**2 +
                    128. * L1 * L2**2 - 288. * L1 * L2 + 152. * L1 -
                    16. * L2**2 + 28. * L2 - 12.)
                self.dh[0, 11] = (
                    -512. * (L1**3) / 3. - 128. * L1**2 * L2 + 224. * L1**2 +
                    64. * L1 * L2 - 224. * L1 / 3 - 16. * L2 / 3. + 16. / 3.)
                self.dh[0, 12] = L2 * (-384. * L1**2 - 256. * L1 * L2 +
                                       320. * L1 + 32. * L2 - 32.)
                self.dh[0, 13] = -32. * L2 * (4. * L2 - 1.) * (
                    2. * L1 + L2 - 1.)
                self.dh[0, 14] = L2 * (384. * L1**2 + 512. * L1 * L2 - 448. *
                                       L1 + 128. * L2**2 - 224. * L2 + 96.)

                # Derivatives relative to L2
                self.dh[1, 0] = 0.
                self.dh[1, 1] = 128. * (
                    L2**3) / 3. - 48. * L2**2 + 44. * L2 / 3. - 1.
                self.dh[1, 2] = (
                    128. * (L1**3) / 3. + 128. *
                    (L1**2) * L2 - 80. * L1**2 + 128. * L1 * L2**2 -
                    160. * L1 * L2 + 140. * L1 / 3. + 128. *
                    (L2**3) / 3. - 80. * L2**2 + 140. * L2 / 3. - 25. / 3.)
                self.dh[1, 3] = L1 * (128. *
                                      (L1**2) / 3. - 32. * L1 + 16. / 3.)
                self.dh[1, 4] = L1 * (4. * L1 - 1.) * (32. * L2 - 4.)
                self.dh[1, 5] = L1 * (128. * L2**2 - 64. * L2 + 16. / 3.)
                self.dh[1, 6] = (
                    -128. * L1 * L2**2 + 64. * L1 * L2 - 16. * L1 / 3. - 512. *
                    (L2**3) / 3. + 224. * L2**2 - 224. * L2 / 3. + 16. / 3.)
                self.dh[1, 7] = (128. *
                                 (L1**2) * L2 - 16. * L1**2 + 384. * L1 * L2**2
                                 - 288. * L1 * L2 + 28. * L1 + 256. * L2**3 -
                                 384. * L2**2 + 152. * L2 - 12.)
                self.dh[1, 8] = (
                    -128. * (L1**3) / 3. - 256. * L1**2 * L2 + 96. * L1**2 -
                    384. * L1 * L2**2 + 384. * L1 * L2 - 208. * L1 / 3. -
                    512. * (L2**3) / 3. + 288. * L2**2 - 416. * L2 / 3. + 16.)
                self.dh[1, 9] = L1 * (
                    -128. * L1**2 - 256. * L1 * L2 + 192. * L1 - 128. * L2**2 +
                    192. * L2 - 208. / 3.)
                self.dh[1, 10] = L1 * (4. * L1 - 1.) * (
                    32. * L1 + 32. * L2 - 28.)
                self.dh[1, 11] = L1 * (-128. *
                                       (L1**2) / 3. + 32. * L1 - 16. / 3.)
                self.dh[1, 12] = -32. * L1 * (4. * L1 - 1.) * (
                    L1 + 2. * L2 - 1.)
                self.dh[1, 13] = L1 * (-256. * L1 * L2 + 32. * L1 -
                                       384. * L2**2 + 320. * L2 - 32.)
                self.dh[1, 14] = L1 * (128. * L1**2 + 512. * L1 * L2 - 224. *
                                       L1 + 384. * L2**2 - 448. * L2 + 96.)

        ## end of factory_shape_functions.ShapeFunctions<StandardGeometry.TRI15>

        return ShapeFunctions

    else:
        raise AssertionError("Incorrect StandardGeometry!")


#-------------------------------------------------------------------------------
