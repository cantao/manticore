#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Operations over computational units of a mesh.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

from abc import ABCMeta, abstractmethod
from manticore.lops.modal_dg.dgtypes import StdRegionType, RegionAdapter

class EntityOperator(metaclass=ABCMeta):

    contexts = [
            (StdRegionType.DG_Quadrangle, RegionAdapter.WADG),
            (StdRegionType.DG_Quadrangle, RegionAdapter.RWADG),
            (StdRegionType.DG_Triangle, RegionAdapter.WADG),
            (StdRegionType.DG_Triangle, RegionAdapter.RWADG)]

    def __init__(self): pass
        
    @abstractmethod
    def __call__(self, e): pass

    @abstractmethod
    def container_operator(self, group, key, container): pass


def foreach_entity_in_mesh(cm, op):

    for g in cm: # for each ExpansionGroup (group==subdomain)
        for c in g: # for each EntityContainer

            # Set up related to group and container data
            op.container_operator(g, c[0], c[1])

            for context in EntityOperator.contexts: # for each context
                elist = c[1].get_container(context[0], context[1])

                for e in elist: # for each ExpansionEntity
                    op(e)

def foreach_entity_in_subdomain(g, op):
        
    for c in g: # for each EntityContainer

        # Set up related to group and container data
        op.container_operator(g, c[0], c[1])

        for context in EntityOperator.contexts: # for each context
            elist = c[1].get_container(context[0], context[1])

            for e in elist: # for each ExpansionEntity
                op(e)
