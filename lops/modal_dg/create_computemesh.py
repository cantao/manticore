#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#

import manticore
from array import array
import numpy as np
from manticore.mesh.umesh import (GroupDimensionIterator, SequentialIterator,
                                  ConceptualGroup)
from manticore.geom.standard_geometries import StandardGeometry
from manticore.lops.modal_dg.computemesh import ComputeMesh
from manticore.lops.modal_dg.dgtypes import EntityRole, GaussPointsType
from manticore.lops.modal_dg.gauss import standard_nip_rule
from manticore.lops.modal_dg.entity import ExpansionEntityFactory
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions
from manticore.geom.standard_geometries import dim, number_of_edges

logger = manticore.logging.getLogger('LOPS_MD_DG')


def find_local_number(edge, area, n=1):
    """Finds the local edge number connecting it to an area."""
    nedges = number_of_edges(area.geom_type)
    adj0 = edge.get_adjacency(0)
    vertices = array('L',
                     [adj0[0].neighbour.get_id(), adj0[n].neighbour.get_id()])

    faces = [area.get_face_vertices(i) for i in range(nedges)]

    if vertices not in faces:
        vertices.reverse()

    #logger.debug('V = {}, F = {}'.format(vertices, faces))

    return faces.index(vertices)


def create_computemesh(m):
    """Creates a ComputeMesh object from an UnstructuredMesh one.

    Args:
        m (UnstructuredMesh): the unstructured mesh geometric representation.

    Returns:
        A ComputeMesh object.
    """
    cm = ComputeMesh()

    #
    # Step 1: group creation and element insertion
    #
    logger.debug('Iterating through 2D elements.')

    groups_2d = GroupDimensionIterator(m.groups, 2)

    for group in groups_2d:
        group_id = group.get_id()
        group_name = m.get_group_name(group_id)
        group_order = group.get_order()

        logger.debug('Processing group {}: {} [o: {}]'.format(
            group_id, group_name, group_order))

        gid = cm.create_subdomain(group_name, EntityRole.PHYSICAL)

        # Now we iterate over the containers on the group
        for (std_geo, me_containers) in group:
            # If the container has something
            if not me_containers.empty() and dim(std_geo) == 2:
                shape_factory = factory_shape_functions(std_geo)()

                # This number of integration points is related to the
                # compounded order of the nonlinear operator we are integrating
                # in a weak DG formulation of the Navier-Stokes equation for
                # compressible flow.
                nip = 2 * group_order + standard_nip_rule(
                    GaussPointsType.GaussLegendre,
                    shape_factory.jacobian_order())

                key, cnt = cm.create_expansion(gid, group_order, nip)

                me_iterator = SequentialIterator(me_containers)

                # Finally, we're going to iterate through entities
                for entity in me_iterator:
                    # Obsidian entity
                    e = ExpansionEntityFactory.make(std_geo)
                    e.ID = entity.get_id()
                    e.seqID = entity.get_seq_id()
                    e.gID = gid

                    # Insert the element into the computemesh
                    e = cm.insert(gid, key, e)

                    vertices = entity.get_adjacency(0)

                    e.set_vertices(
                        np.array([v.neighbour.ID for v in vertices]))
                    e.set_coeff(
                        np.transpose(
                            np.array(
                                [v.neighbour.coordinates for v in vertices])))

    #
    # Step 2: ghost creation
    #
    logger.debug('Boundary conditions assessment.')

    groups_1d = GroupDimensionIterator(m.groups, 1)
    active_bdr = [ConceptualGroup.Type.BOUNDARY, ConceptualGroup.Type.PARALLEL]

    for group in groups_1d:
        group_type = group.get_type()

        if group_type not in active_bdr:
            continue

        role = (EntityRole.GHOST if group_type == ConceptualGroup.Type.BOUNDARY
                else ConceptualGroup.Type.PARALLEL)

        group_id = group.get_id()
        group_name = m.get_group_name(group_id)

        logger.debug('Processing bdr {}: {}'.format(group_id, group_name))

        gid = cm.create_subdomain(group_name, role)

        # Now we iterate over the containers on the group
        for (std_geo, me_containers) in group:
            # If the container has something
            if not me_containers.empty() and dim(std_geo) == 1:
                me_iterator = SequentialIterator(me_containers)

                ghost_expansion_created = False

                # Finally, we're going to iterate through entities
                for entity in me_iterator:
                    # The physical neighbour
                    n0 = cm.get_entity(
                        entity.get_adjacency(2)[0].neighbour.get_id())

                    if not ghost_expansion_created:
                        # Ghost/parallel expansion
                        key, cnt = cm.create_expansion(gid, 0, n0.key.n1d)
                        ghost_expansion_created = True

                    e_quad = ExpansionEntityFactory.make(
                        StandardGeometry.QUAD4)
                    e_quad.ID = entity.get_id()
                    e_quad.seqID = entity.get_seq_id()
                    e_quad.gID = gid
                    
                    e = cm.insert(gid, key, e_quad)

                    local_number = find_local_number(entity, n0)
                    e.insert_neighbour(0, n0, local_number)

    #
    # Step 3: all face types to fix 2D adjacencies on the compute mesh
    #

    logger.debug('Connecting 2D elements.')

    groups_1d = GroupDimensionIterator(m.groups, 1)

    externally_generated_bdr = [ConceptualGroup.Type.BOUNDARY,
                                    ConceptualGroup.Type.INTERFACE]

    for group in groups_1d:
        group_type = group.get_type()
        group_id = group.get_id()
        group_name = m.get_group_name(group_id)

        logger.debug('Processing bdr {}: {}, {}'.format(
            group_id, group_name, group_type))

        # The incidence of an edge comming from an external mesh
        # generator is different from the incidence of an internal
        # generated edge.
        if group_type == ConceptualGroup.Type.INTERNAL:
            n = -1
        elif group_type in externally_generated_bdr:
            n = 1
        else:
            RuntimeError('Undefined edge type of incidence!')

        # Now we iterate over the containers on the group
        for (std_geo, me_containers) in group:
            # If the container has something
            if not me_containers.empty() and dim(std_geo) == 1:
                me_iterator = SequentialIterator(me_containers)

                # Finally, we're going to iterate through entities
                for entity in me_iterator:
                    adj = entity.get_adjacency(2)

                    # The physical neighbour
                    n0 = cm.get_entity(adj[0].neighbour.get_id())
                    n0_local_number = find_local_number(entity, n0, n)

                    if len(adj) > 1:
                        # The other physical neighbour
                        n1 = cm.get_entity(adj[1].neighbour.get_id())
                        n1_local_number = find_local_number(entity, n1, n)
                    else:
                        # The ghost
                        n1 = cm.get_entity(entity.get_id())
                        n1_local_number = 0  # It's a ghost

                    n0.insert_neighbour(n0_local_number, n1, n1_local_number)
                    n1.insert_neighbour(n1_local_number, n0, n0_local_number)
    return cm


# -- create_computemesh.py ----------------------------------------------------
