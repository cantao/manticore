#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# ExpansioKey factory.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

#
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.prinfunc import PrincipalFunctions
from manticore.lops.modal_dg.faceprinfunc import FacePrincipalFunctions
from manticore.lops.modal_dg.gauss import (CollocationDiffMat,
                                           JacobiGaussQuadratures)
from manticore.lops.modal_dg.gauss_strm import StreamlinedGaussWeights


class ExpansionKeyFactory:

    gaussWIP = JacobiGaussQuadratures()
    PFuncs = PrincipalFunctions(gaussWIP)
    FPFuncs = FacePrincipalFunctions(gaussWIP)
    CDiffMat = CollocationDiffMat(gaussWIP)
    SGaussW = StreamlinedGaussWeights(gaussWIP)

    def make(p_order, nip):
        key = ExpansionKey.get_instance(p_order, nip)

        key.set_gauss_quadrature(ExpansionKeyFactory.gaussWIP)
        key.set_strm_gauss_quadrature(ExpansionKeyFactory.SGaussW)
        key.set_diff_matrix(ExpansionKeyFactory.CDiffMat)
        key.set_principal_functions(ExpansionKeyFactory.PFuncs)
        key.set_face_principal_functions(ExpansionKeyFactory.FPFuncs)

        return key


# end of manticore.lops.modal_dg.entity.ExpansionKeyFactory
