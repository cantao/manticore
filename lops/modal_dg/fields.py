#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Fields abstractions.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore.services.const as const
from manticore.services.mconsts import CTEs
from manticore.services.datatypes import bidict
from manticore.services.datatypes import FlyweightMixin
from manticore.lops.modal_dg.dgtypes import FieldType


class LocalField:
    """Local storage of fields."""

    __slots__ = ['u']

    def __init__(self, num_vars, num_phys, num_tr=0):
        """ Constructor of LocalFields objects.

        Args:
            num_vars (int): Number of variables
            num_phys (int): Number of physical coefficients
            num_tr   (int): Number of transformed (modal) coefficients

        Attributes:
            u (list(list(np.array))): [[ph: [np.array], ... [np.array]],
                                       [tr: [np.array], ... [np.array]]]

        """

        self.u = [None for i in range(FieldType.size())]

        self.initialize(num_vars, num_phys, num_tr)

    def initialize(self, num_vars, num_phys, num_tr=0):
        """ Initializer

        Args:
            num_vars (int): Number of variables
            num_phys (int): Number of physical coefficients
            num_tr   (int): Number of transformed coefficients

        """

        self.u[FieldType.ph] = [np.zeros(num_phys) for i in range(num_vars)]

        if num_tr > 0:
            self.u[FieldType.tr] = [np.zeros(num_tr) for i in range(num_vars)]

    def get(self, field_type, idx):

        return self.u[field_type][idx]

    def size(self, field_type):

        return self.u[field_type][0].shape[0]

    def copy(self, field_type, field):
        """Copies the contents of a field.u[field_type].

        Notes:

        * Assumes field.u[field_type] has the same size and shape of
            self.u[field_type].

        """

        assert len(self.u[field_type]) == len(field.u[field_type])
        assert self.size(field_type) == field.size(field_type)

        for (su, fu) in zip(self.u[field_type], field.u[field_type]):
            np.copyto(su, fu)  # su <- fu


class FieldVarList(FlyweightMixin):
    """ List of variables services.fieldvariables.FieldVariable.

    Notes:

        * Variables and respective storage index are kept in a
          bidirectional map.

        * This classe inherities a flyweight design pattern. The
          shared storage pool can be accessed through the
          super.get_instance method.

    """

    def __init__(self, *args):

        self._index = 0
        self._vars = bidict()

        for v in args:
            self._insert(v)

    def _insert(self, v):

        if v not in self._vars:
            self._vars[v] = self._index
            self._index += 1

    def __call__(self, v):
        return self._vars.get(v, const.INVALID_RESULT)

    def right(self, var_idx):
        if var_idx in self._vars.inverse:
            return self._vars.inverse[var_idx][0]
        else:
            return None

    def size(self):
        return len(self._vars)

    def get(self):
        return list(self._vars.keys())
