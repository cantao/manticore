#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Mixins for 2D operations on standard regions.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.expantools import ModeLoopLimit


def factory_number_of_ip(classRegion):
    """Factory for binding element's shape information (classRegion) into
    a mixin class that provides 'number of integration points'
    information according to a ExpansionKey object.

    See manticore.lops.modal_dg.dgtypes.factory_class_region

    """

    class NumberOfIp(classRegion):
        __slots__ = ['n1d', 'n2d']

        def __init__(self, key):
            """Constructor of NumberOfIp objects.

            Args:
                key (ExpansionKey)
            """
            classRegion.__init__(self)
            self.n1d = key.n1d
            self.n2d = key.n2d

        def set_expansion_key(self, key):
            self.n1d = key.n1d
            self.n2d = key.n2d

    # end of factory_number_of_ip.NumberOfIp

    return NumberOfIp


# end of manticore.lops.modal_dg.stdmixins2d.factory_number_of_ip


def factory_number_of_modes(classBase):
    """Factory of mixin classes that provide interpolation order and
        number of modes P and Q according to StdRegionType value and
        ExpansionKey object.

    classBase must provide StdRegionType information.

    """

    class NumberOfModes(classBase):
        __slots__ = ['o']

        def __init__(self, key):
            """Constructor of NumberOfModes objects.

            Args:
                key (ExpansionKey)
            """

            classBase.__init__(self, key)
            self.o = key.order

        def set_expansion_key(self, key):
            classBase.set_expansion_key(self, key)
            self.o = key.order

        def P(self):
            return ModeLoopLimit.P(self.G, self.o)

        def Q(self, p):
            return ModeLoopLimit.Q(self.G, self.o, p)

    # end of factory_number_of_modes.NumberOfModes

    return NumberOfModes


# end of manticore.lops.modal_dg.stdmixins2d.factory_number_of_modes


def factory_wp_mixin(classBase):
    """Factory of mixin classes that provide integration points and
        weights according to StdRegionType value and ExpansionKey
        object.

    classBase must provide StdRegionType information.

    """

    class WPMixin(classBase):
        __slots__ = ['W1', 'P1', 'W2', 'P2']

        def __init__(self, key):
            """Constructor of WPMixin objects.

            Args:
                key (ExpansionKey)
            """

            classBase.__init__(self, key)
            self.W1, self.P1 = key.wp(self.G, 1)
            self.W2, self.P2 = key.wp(self.G, 2)

        def set_expansion_key(self, key):
            classBase.set_expansion_key(self, key)
            self.W1, self.P1 = key.wp(self.G, 1)
            self.W2, self.P2 = key.wp(self.G, 2)

    # end of factory_wp_mixin.WPMixin

    return WPMixin


# end of manticore.lops.modal_dg.stdmixins2d.factory_wp_mixin


def factory_sw_mixin(classBase):
    """Factory of mixin classes that provide the streamlined version of
        integration weights according to StdRegionType value and
        ExpansionKey object. classBase must provide StdRegionType
        information.

    """

    class SWMixin(classBase):
        __slots__ = ['W']

        def __init__(self, key):
            """Constructor of SWMixin objects.

            Args:
                key (ExpansionKey)
            """

            classBase.__init__(self, key)
            self.W = key.sw(self.G)

        def set_expansion_key(self, key):
            classBase.set_expansion_key(self, key)
            self.W = key.sw(self.G)

    # end of factory_sw_mixin.SWMixin

    return SWMixin


# end of manticore.lops.modal_dg.stdmixins2d.factory_sw_mixin


def factory_collocation_mixin(classBase):
    class CollocationMixin(classBase):
        __slots__ = ['D1', 'D2']

        def __init__(self, key):
            """Constructor of CollocationMixin objects.

            Args:
                key (ExpansionKey)
            """
            classBase.__init__(self, key)
            self.D1 = key.D(self.G, 1)
            self.D2 = key.D(self.G, 2)

        def set_expansion_key(self, key):
            classBase.set_expansion_key(self, key)
            self.D1 = key.D(self.G, 1)
            self.D2 = key.D(self.G, 2)

    # end of factory_collocation_mixin.CollocationMixin

    return CollocationMixin


# end of manticore.lops.modal_dg.stdmixins2d.factory_collocation_mixin


def factory_princ_func_selector(region_type):

    if region_type == StdRegionType.DG_Quadrangle:

        class PrincipalFuncSelector:
            def PF1(key):
                return key.psi_a()

            def PF2(key):
                return key.psi_a()

        return PrincipalFuncSelector

    elif region_type == StdRegionType.DG_Triangle:

        class PrincipalFuncSelector:
            def PF1(key):
                return key.psi_a()

            def PF2(key):
                return key.psi_b()

        return PrincipalFuncSelector

    else:
        raise AssertionError("Incorrect region_type!")


# end of manticore.lops.modal_dg.stdmixins2d.factory_princ_func_selector


def factory_princ_func_mixin(classBase):
    """Factory of mixin classes that provides access to the tables of
        principal functions (evaluated at the integration points)
        according to StdRegionType value and ExpansionKey object.

    classBase must provide StdRegionType information.

    See manticore.lops.modal_dg.stdmixins2d.factory_princ_func_selector

    """

    class PrincipalFuncMixin(classBase):
        __slots__ = ['PF1', 'PF2']

        def __init__(self, key):
            """Constructor of PrincipalFuncMixin objects.

            Args:
                key (ExpansionKey)
            """

            classBase.__init__(self, key)
            PrincipalFuncSelector = factory_princ_func_selector(self.G)
            self.PF1 = key.prin_funcs(PrincipalFuncSelector.PF1(key))
            self.PF2 = key.prin_funcs(PrincipalFuncSelector.PF2(key))

        def set_expansion_key(self, key):
            classBase.set_expansion_key(self, key)
            PrincipalFuncSelector = factory_princ_func_selector(self.G)
            self.PF1 = key.prin_funcs(PrincipalFuncSelector.PF1(key))
            self.PF2 = key.prin_funcs(PrincipalFuncSelector.PF2(key))

    return PrincipalFuncMixin


# end of manticore.lops.modal_dg.stdmixins2d.factory_princ_func_mixin
