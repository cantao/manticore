#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Operations on faces of standard 2D regions.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore.lops.modal_dg.stdmixins2d as _2d_mx
import manticore.lops.modal_dg.stdmixins1d as _1d_mx
from manticore.lops.modal_dg.dgtypes import StdRegionType, factory_class_region
from manticore.lops.modal_dg.faceprinfunc import FacePrincipalFunctionKey
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.expantools import ExpansionSize


def factory_stdbackward1d(classRegion):
    """Factory of classes that perform backward tranformation.

    Backward transformation: reconstruction from coefficient space to
    physical space on two dimensions according to element's shape
    information (classRegion) and an ExpansionKey object.

    Notes:
        * See manticore.lops.modal_dg.dgtypes.factory_class_region
        * See manticore.lops.modal_dg.stdops2d.py

    """

    # Using stdmixins2d factories to built a class that provides
    # access to principal functions, number of modes and number of
    # integration points.
    Parent = _1d_mx.factory_princ_func_mixin(
        _2d_mx.factory_number_of_modes(
            _1d_mx.factory_number_of_ip(classRegion)))

    class StdBackwardBase(Parent):
        """Base class for all specializations of StdBackward."""

        __slots__ = ['nm', 'Fp', 'Psi']

        def __init__(self, key):
            """Constructor of StdBackwardBase objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.nm = self.o + 1  # Number of modes on
            # non-colapsed directions.

            self.Fp = np.zeros(self.nm)  # Auxiliar storage
            self.Psi = np.zeros(self.nm)  # Auxiliar storage

    # end of factory_stdbackward1d.StdBackwardBase

    # Specializations
    region = classRegion()

    if region.G == StdRegionType.DG_Quadrangle:

        class StdBackward(StdBackwardBase):
            """Backward transformation for StdRegionType.DG_Quadrangle regions.

            """
            __slots__ = []

            def __init__(self, key):
                """Constructor of StdBackward objects.

                Args:

                    key (ExpansionKey): required for the
                        initialization of Parent.
                """
                StdBackwardBase.__init__(self, key)

            def at_ips(self, face_id, C, Ph):
                """Backward transformation at all integration points.

                This operation is executed considering only element's
                data indepepndently from neighbourhood relationships.

                Args:

                    face_id (int) [in]: local face index.

                    C (Numpy.array) [in]: real-valued (P*Q, 1) array
                        (P = Q = number_of_1d_modes, P*Q = number_of_2d_modes)
                        containing the coeficients of the polynomial expansion
                        \sum_p\sum_q C_{pq}\phi_p\phi_q

                    Ph (Numpy.array) [out]: real-valued (nip, 1)
                        array containing the physical values of the
                        interpolation at the integration points ordered
                        as Phi[ii+jj*nip], ii=0,..., nip in xi_1,
                        jj=0,..., nip in xi_2. 

                        Note: nip must be >= the number of integration points 
                        on face face_id as defined by the argument "key" in 
                        self.__init__(self, key).

                """

                psi_x = self.PF1[face_id]
                psi_y = self.PF2[face_id]

                nx = self.n_coord[0][face_id]
                ny = self.n_coord[1][face_id]

                nip = nx * ny

                #
                # This engine supports non-homogeneous p-order. This
                # way, reconstruction of values on faces may be
                # required on a number of integration points higher
                # than that defined by the argument "key" in
                # self.__init__(self, key). In this case, the number
                # of integration points and the values of the
                # principal functions on those points must be updated locally.
                #
                if Ph.shape[0] > nip:
                    #
                    # Be careful when using ExpansionKey to get integration
                    # points on faces: the second argument of the constructor
                    # is the one-dimensional number of integration points for
                    # numerical integration INSIDE the element. The number of
                    # integration points generated on faces by ExpansionKey is
                    # the second argument of the constructor PLUS ONE. Thus,
                    # if you know the number of integration points on face (as
                    # here) you must SUBTRACT ONE from it to get the correct
                    # integration points on faces.
                    #
                    k = ExpansionKeyFactory.make(self.o, Ph.shape[0]-1)
                    nx = k.n1d_on_face(self.G, face_id, 1)
                    ny = k.n1d_on_face(self.G, face_id, 2)
                    
                    k1 = k.psi_on_face(self.G, face_id, 1)
                    k2 = k.psi_on_face(self.G, face_id, 2)
                    psi_x = k.prin_funcs(k1)
                    psi_y = k.prin_funcs(k2)

                else:
                    assert Ph.shape[0] == nip

                nm = self.nm
                Cpq = np.reshape(C, (nm, nm))  # This reshape returns a view.
                phys = 0

                for jj in range(ny):
                    np.dot(Cpq, psi_y.row(jj), self.Fp)

                    for ii in range(nx):
                        Ph[phys] = np.dot(self.Fp, psi_x.row(ii))
                        phys += 1

            def at_ips_as_passive(self, actV, actF, actNIP, pasF, C, Ph):

                nx = self.n_coord[0][pasF]
                ny = self.n_coord[1][pasF]

                nmax = max(actNIP, max(nx, ny))

                kf = FacePrincipalFunctionKey.get_instance(actV, actF,
                                              StdRegionType.DG_Quadrangle,
                                              pasF, self.o, nmax)

                fpf = self.FPFs(kf)

                M = fpf.nmodes

                assert C.shape[0] == M

                # Trying to avoid realocation
                if Ph.shape[0] == nmax:
                    Ph.fill(0.0)
                else:
                    Ph = np.zeros(nmax)

                for ii in range(M):
                    Ph += C[ii] * fpf.column(ii)

                return Ph

            # end of factory_stdbackward1d.StdBackward

        return StdBackward

    elif region.G == StdRegionType.DG_Triangle:

        class StdBackward(StdBackwardBase):
            """Backward transformation for StdRegionType.DG_Triangle regions.

            """
            __slots__ = []

            def __init__(self, key):
                """Constructor of StdBackward objects.

                Args:

                    key (ExpansionKey): required for the
                        initialization of Parent.
                """
                StdBackwardBase.__init__(self, key)

            def at_ips(self, face_id, C, Ph):
                """Backward transformation at all integration points.

                This operation is executed considering only element's
                data indepepndently from neighbourhood relationships.

                Args:

                    face_id (int) [in]: local face index.

                    C (Numpy.array) [in]: real-valued (number_of_modes, 1) array
                        containing the coeficients of the polynomial expansion
                        \sum_p\sum_q C_{pq}\phi_p\phi_q

                    Ph (Numpy.array) [out]: real-valued (nip, 1)
                        array containing the physical values of the
                        interpolation at the integration poits ordered
                        as Phi[ii+jj*nip], ii=0,..., nip in xi_1,
                        jj=0,..., nip in xi_2.

                """

                psi_x = self.PF1[face_id]
                psi_y = self.PF2[face_id]

                nx = self.n_coord[0][face_id]
                ny = self.n_coord[1][face_id]

                nip = nx * ny

                #
                # This engine supports non-homogeneous p-order. This
                # way, reconstruction of values on faces may be
                # required on a number of integration points higher
                # than that defined by the argument "key" in
                # self.__init__(self, key). In this case, the number
                # of integration points and the values of the
                # principal functions on those points must be updated locally.
                #
                if Ph.shape[0] > nip:

                    #
                    # Be careful when using ExpansionKey to get integration
                    # points on faces: the second argument of the constructor
                    # is the one-dimensional number of integration points for
                    # numerical integration INSIDE the element. The number of
                    # integration points generated on faces by ExpansionKey is
                    # the second argument of the constructor PLUS ONE. Thus,
                    # if you know the number of integration points on face (as
                    # here) you must SUBTRACT ONE from it to get the correct
                    # integration points on faces.
                    #
                    k = ExpansionKeyFactory.make(self.o, Ph.shape[0]-1)
                    nx = k.n1d_on_face(self.G, face_id, 1)
                    ny = k.n1d_on_face(self.G, face_id, 2)
                    
                    k1 = k.psi_on_face(self.G, face_id, 1)
                    k2 = k.psi_on_face(self.G, face_id, 2)
                    psi_x = k.prin_funcs(k1)
                    psi_y = k.prin_funcs(k2)

                else:
                    assert Ph.shape[0] == nip

                oP1 = self.nm
                phys = 0

                for jj in range(ny):
                    psi = psi_y.row(jj)
                    mode = 0

                    for pp in range(oP1):
                        self.Fp[pp] = np.dot(C[mode:mode + oP1 - pp],
                                             psi[mode:mode + oP1 - pp])
                        mode += oP1 - pp

                    for ii in range(nx):
                        Ph[phys] = np.dot(self.Fp, psi_x.row(ii))
                        phys += 1

            def at_ips_as_passive(self, actV, actF, actNIP, pasF, C, Ph):

                nx = self.n_coord[0][pasF]
                ny = self.n_coord[1][pasF]

                nmax = max(actNIP, max(nx, ny))

                kf = FacePrincipalFunctionKey.get_instance(
                    actV, actF, StdRegionType.DG_Triangle, pasF, self.o, nmax)

                fpf = self.FPFs(kf)

                M = fpf.nmodes

                assert C.shape[0] == M

                # Trying to avoid realocation
                if Ph.shape[0] == nmax:
                    Ph.fill(0.0)
                else:
                    Ph = np.zeros(nmax)

                for ii in range(M):
                    Ph += C[ii] * fpf.column(ii)

                return Ph

            # end of factory_stdbackward2d.StdBackward

        return StdBackward

    else:
        raise AssertionError("Incorrect region_type!")


# end of manticore.lops.modal_dg.stdops1d.factory_stdbackward1d


def factory_stdintegrator1d(classRegion):

    # Using stdmixins1d factories to built a class that provides
    # access to principal functions, number of modes and number of
    # integration points.
    Parent = _1d_mx.factory_wp_mixin(_1d_mx.factory_number_of_ip(classRegion))

    class StdIntegrator(Parent):

        __slots__ = []

        def __init__(self, key):
            """Constructor of StdIntegrator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

        def __call__(self, face_id, Ph):
            """

            Args:

                face_id (int) [in]: local face index.

                Ph (numpy.array) [in]: values of a scalar function at the
                    integration points (in streamlined ordering; see
                    gauss_strm.StreamlinedGaussWeights).

            Returns:

                double

            """

            # I must tinhk better about this: the broadcasting
            # features of Numpy may be useful for treating elements
            # with constant Jacobians.
            assert Ph.shape[0] == self.W[face_id].shape[0]

            return np.dot(Ph, self.W[face_id])

    # end of factory_stdintegrator1d.StdIntegrator

    return StdIntegrator


# end of manticore.lops.modal_dg.stdops1d.factory_stdintegrator1d


def factory_stdinnerproduct1d(classRegion):
    """Factory of classes that evaluates the inner product of the physical
    represented values with every basis function.

    Notes:
        * See K&S, page 173-176
    """

    # Using stdmixins1d factories to built a class that provides
    # access to principal functions, number of modes and number of
    # integration points.
    Parent = _1d_mx.factory_princ_func_mixin(
        _2d_mx.factory_number_of_modes(
            _1d_mx.factory_number_of_ip(classRegion)))

    class StdInnerProductBase(Parent):
        """Base class for all specializations of StdInnerProduct."""

        __slots__ = ['nm', 'op']

        def __init__(self, key):
            """Constructor of StdInnerProductBase objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.nm = ExpansionSize.get(self.G, self.o)
            self.op = factory_stdintegrator1d(classRegion)(key)

    # Specializations
    region = classRegion()

    if region.G == StdRegionType.DG_Quadrangle:

        class StdInnerProduct(StdInnerProductBase):

            __slots__ = []

            def __init__(self, key):
                """Constructor of StdInnerProduct objects for
                StdRegionType.DG_Quadrangle.

                Args:
                    key (ExpansionKey): required for the initialization
                        of Parent.
                """
                StdInnerProductBase.__init__(self, key)

            def __call__(self, face_id, Ph, Ipq):
                """Executes inner product of the physical represented values
                with every basis function.

                Args:

                    face_id (int) [in]: local face index.

                    Ph (numpy.array) [in]: values of a scalar function at the
                        integration points (in streamlined ordering; see
                        gauss_strm.StreamlinedGaussWeights).

                    Iqp (numpy.array) [out]: each element of this array is the
                        the inner product of Ph wiht a basis function.

                """

                assert Ipq.shape[0] == self.nm

                Ps1 = self.PF1[face_id]
                Ps2 = self.PF2[face_id]

                n1 = self.n_coord[0][face_id]
                n2 = self.n_coord[1][face_id]

                nip = n1 * n2

                #
                # This engine supports non-homogeneous p-order. This
                # way, reconstruction of values on faces may be
                # required on a number of integration points higher
                # than that defined by the argument "key" in
                # self.__init__(self, key). In this case, the number
                # of integration points and the values of the
                # principal functions on those points must be updated locally.
                #
                if Ph.shape[0] > nip:
                    #
                    # Be careful when using ExpansionKey to get integration
                    # points on faces: the second argument of the constructor
                    # is the one-dimensional number of integration points for
                    # numerical integration INSIDE the element. The number of
                    # integration points generated on faces by ExpansionKey is
                    # the second argument of the constructor PLUS ONE. Thus,
                    # if you know the number of integration points on face (as
                    # here) you must SUBTRACT ONE from it to get the correct
                    # integration points on faces.
                    #
                    k = ExpansionKeyFactory.make(self.o, Ph.shape[0]-1)
                    n1 = k.n1d_on_face(self.G, face_id, 1)
                    n2 = k.n1d_on_face(self.G, face_id, 2)
                    
                    k1 = k.psi_on_face(self.G, face_id, 1)
                    k2 = k.psi_on_face(self.G, face_id, 2)
                    Ps1 = k.prin_funcs(k1)
                    Ps2 = k.prin_funcs(k2)

                else:
                    assert Ph.shape[0] == nip

                phi = np.zeros((n2, n1))
                Fpq = np.zeros(n2 * n1)

                mode = 0

                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pfA_p = Ps1.column(pp)
                    Qmax = self.Q(pp) + 1

                    for qq in range(Qmax):
                        pfA_q = Ps2.column(qq)

                        # TODO: comparar velocidade dessa solucao
                        # versus meshgrid

                        # There is no gain in declaring phi and Fpq as
                        # class's attributes as '=' is not an actual
                        # attribution.
                        np.multiply(np.reshape(pfA_q, (n2, 1)), pfA_p, phi)
                        np.multiply(Ph, np.ravel(phi), Fpq)

                        Ipq[mode] = self.op(face_id, Fpq)
                        mode += 1

        # end of factory_stdinnerproduct1d.StdInnerProduct

        return StdInnerProduct

    elif region.G == StdRegionType.DG_Triangle:

        class StdInnerProduct(StdInnerProductBase):

            __slots__ = []

            def __init__(self, key):
                """Constructor of StdInnerProduct objects for
                StdRegionType.DG_Triangle.

                Args:

                    key (ExpansionKey): required for the
                        initialization of Parent.
                """
                StdInnerProductBase.__init__(self, key)

            def __call__(self, face_id, Ph, Ipq):
                """Executes inner product of the physical represented values
                with every basis function.

                Args:

                    face_id (int) [in]: local face index.

                    Ph (numpy.array) [in]: values of a scalar function at the
                        integration points (in streamlined ordering; see
                        gauss_strm.StreamlinedGaussWeights).

                    Iqp (numpy.array) [out]: each element of this array is the
                        the inner product of Ph wiht a basis function.

                """
                assert Ipq.shape[0] == self.nm

                Ps1 = self.PF1[face_id]
                Ps2 = self.PF2[face_id]

                n1 = self.n_coord[0][face_id]
                n2 = self.n_coord[1][face_id]

                nip = n1 * n2

                #
                # This engine supports non-homogeneous p-order. This
                # way, reconstruction of values on faces may be
                # required on a number of integration points higher
                # than that defined by the argument "key" in
                # self.__init__(self, key). In this case, the number
                # of integration points and the values of the
                # principal functions on those points must be updated locally.
                #
                if Ph.shape[0] > nip:
                    #
                    # Be careful when using ExpansionKey to get integration
                    # points on faces: the second argument of the constructor
                    # is the one-dimensional number of integration points for
                    # numerical integration INSIDE the element. The number of
                    # integration points generated on faces by ExpansionKey is
                    # the second argument of the constructor PLUS ONE. Thus,
                    # if you know the number of integration points on face (as
                    # here) you must SUBTRACT ONE from it to get the correct
                    # integration points on faces.
                    #
                    k = ExpansionKeyFactory.make(self.o, Ph.shape[0]-1)
                    n1 = k.n1d_on_face(self.G, face_id, 1)
                    n2 = k.n1d_on_face(self.G, face_id, 2)
                    
                    k1 = k.psi_on_face(self.G, face_id, 1)
                    k2 = k.psi_on_face(self.G, face_id, 2)
                    Ps1 = k.prin_funcs(k1)
                    Ps2 = k.prin_funcs(k2)

                else:
                    assert Ph.shape[0] == nip

                phi = np.zeros((n2, n1))
                Fpq = np.zeros(n2 * n1)

                mode = 0
                tab = 0  # Extra indexer for triangle

                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pfA_p = Ps1.column(pp)
                    Qmax = self.Q(pp) + 1

                    adv = tab  # Extra indexer for triangle

                    for qq in range(Qmax):
                        pfB_pq = Ps2.column(adv)  # Extra indexer for triangle

                        np.multiply(np.reshape(pfB_pq, (n2, 1)), pfA_p, phi)
                        np.multiply(Ph, np.ravel(phi), Fpq)

                        Ipq[mode] = self.op(face_id, Fpq)
                        mode += 1
                        adv += 1

                    tab += self.o - pp + 1  # Extra indexer for triangle

        # end of factory_stdinnerproduct1d.StdInnerProduct

        return StdInnerProduct

    else:
        raise AssertionError("Incorrect region_type!")


# end of manticore.lops.modal_dg.stdops1d.factory_stdinnerproduct1d
