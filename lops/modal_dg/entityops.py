#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Operations over computational units of a mesh.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.engine import EntityOperator
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.dgtypes import FieldType, RegionAdapter
from manticore.lops.modal_dg.class_instances import (
    class_quad_wadg_physforward,  class_quad_rwadg_physforward,
    class_tria_wadg_physforward,  class_tria_rwadg_physforward,
    class_quad_physintegrator2d,  class_tria_physintegrator2d,
    class_quad_wadg_inverse_mass, class_tria_wadg_inverse_mass,
    class_quad_physevaluator2d,   class_tria_physevaluator2d,
    class_quad_physbackward2d,    class_tria_physbackward2d )


class InitializeFields(EntityOperator):
    """Allocation of fields storage of computational elements."""

    def __init__(self, state, residual, cte = [], auxiliary=[], vertex=[]):
        super().__init__()
        self.stv = state
        self.rsv = residual
        self.axv = auxiliary
        self.vtv = vertex
        self.ctv = cte

    def __call__(self, e):
        assert len(self.stv) > 0
        assert len(self.rsv) > 0

        e.init_variables(self.stv, self.rsv, self.axv)
        e.init_ctes(self.ctv)
        e.init_vertex_variables(self.vtv)

    def container_operator(self, group, key, container): pass

        
class NormalizeMesh(EntityOperator):
    """Scaling the physical dimensions of a mesh."""

    def __init__(self, length):
        super().__init__()
        self.d = length

    def __call__(self, e):
        assert self.d > 0.

        e.set_coeff( e.get_coeff() / self.d )
        e.eval_jacobian()
        e.eval_mass()
        e.eval_face_char_length()

    def container_operator(self, group, key, container): pass


class InitConstantFieldValues(EntityOperator):
    """Initialization of constant field value for all elements."""

    def __init__(self, var, value, norm_cte=1.0):
        super().__init__()
        self.op  = [[None, None],[None, None]]
        self.var = var
        self.cte = norm_cte
        self.value = value

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        ctx = EntityOperator.contexts

        #       type       subtype
        self.op[ctx[0][0]][ctx[0][1]] = class_quad_wadg_physforward(k)
        self.op[ctx[1][0]][ctx[1][1]] = class_quad_rwadg_physforward(k)
        self.op[ctx[2][0]][ctx[2][1]] = class_tria_wadg_physforward(k)
        self.op[ctx[3][0]][ctx[3][1]] = class_tria_rwadg_physforward(k)

    def __call__(self, e):
        state_ph = e.state(FieldType.ph, self.var)
        
        state_ph.fill(self.value/self.cte)
        
        self.op[e.type][e.subtype](e.get_jacobian(), e.get_inv_mass(),
                    state_ph, e.state(FieldType.tr, self.var))

class InitAveragedFieldValues(EntityOperator):
    """Initialization of a field that is constant inside each element.

    Notes:
         * DO NOT USE inside "engine.foreach" operators.
         * Useful for reading initial constant solutions of evolutive problems.
         * Useful for reading fields from standard finite volume method.
    """

    def __init__(self, var, v, norm_cte=1.0):
        """
        Args:
            var (FieldVariable):
            v (Numpy.array): values define over JUST ONE container.
            norm_cte (float):
        """
        super().__init__()
        self.op  = [[None, None],[None, None]]
        self.var = var
        self.cte = norm_cte
        self.v   = v
        self.ii  = 0

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        ctx = EntityOperator.contexts

        #       type       subtype
        self.op[ctx[0][0]][ctx[0][1]] = class_quad_wadg_physforward(k)
        self.op[ctx[1][0]][ctx[1][1]] = class_quad_rwadg_physforward(k)
        self.op[ctx[2][0]][ctx[2][1]] = class_tria_wadg_physforward(k)
        self.op[ctx[3][0]][ctx[3][1]] = class_tria_rwadg_physforward(k)

    def __call__(self, e):
        state_ph = e.state(FieldType.ph, self.var)

        state_ph.fill(self.v[self.ii]/self.cte)
        self.ii += 1
        
        self.op[e.type][e.subtype](e.get_jacobian(), e.get_inv_mass(),
            state_ph, e.state(FieldType.tr, self.var))

        
class GetAveragedFieldValues(EntityOperator):
    """Extraction of a physical averaged field from elements.

    Notes:
         * DO NOT USE inside "engine.foreach" operators.
         * Useful for outputing fields in standard finite volume format.
    """

    def __init__(self, var, v, norm_cte=1.0):
        """
        Args:
            var (FieldVariable):
            v (Numpy.array): values define over JUST ONE container.
            norm_cte (float):
        """
        super().__init__()
        self.integ = [None, None]
        self.var = var
        self.cte = norm_cte
        self.v   = v
        self.ii  = 0

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)

        self.integ[0] = class_quad_physintegrator2d(k)
        self.integ[1] = class_tria_physintegrator2d(k)

    def __call__(self, e):        
        vol = e.volume
        state_ph = e.state(FieldType.ph, self.var)

        self.v[self.ii]=(
            self.integ[e.type](state_ph,e.get_jacobian())/vol)*self.cte

        self.ii += 1
        
        
class InitPhysicalFieldValues(EntityOperator):
    """Initialization of a field from physical values defined per
    integration point.

    Notes:
        * DO NOT USE inside "engine.foreach" operators.
    """

    def __init__(self, var, v, norm_cte=1.0):
        """
        Args:
            var (FieldVariable):
            v (Numpy.array): values define over JUST ONE container.
            norm_cte (float):
        """
        super().__init__()
        self.op  = [[None, None],[None, None]]
        self.var = var
        self.cte = norm_cte
        self.v   = v
        self.nip = None
        self.ii  = 0

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        ctx = EntityOperator.contexts

        #       type       subtype
        self.op[ctx[0][0]][ctx[0][1]] = class_quad_wadg_physforward(k)
        self.op[ctx[1][0]][ctx[1][1]] = class_quad_rwadg_physforward(k)
        self.op[ctx[2][0]][ctx[2][1]] = class_tria_wadg_physforward(k)
        self.op[ctx[3][0]][ctx[3][1]] = class_tria_rwadg_physforward(k)

        self.nip = k.n2d

    def __call__(self, e):
        state_ph = e.state(FieldType.ph, self.var)

        np.copyto( state_ph, self.v[self.ii : self.ii+self.nip] )
        state_ph /= self.cte
        self.ii  += self.nip

        self.op[e.type][e.subtype](e.get_jacobian(), e.get_inv_mass(),
            state_ph, e.state(FieldType.tr, self.var))        


class GetPhysicalFieldValues(EntityOperator):
    """Extraction of physical values of a field defined per integration point.

    Notes:
        * DO NOT USE inside "engine.foreach" operators.
    """

    def __init__(self, var, v, norm_cte=1.0):
        """
        Args:
            var (FieldVariable):
            v (Numpy.array): values define over JUST ONE container.
            norm_cte (float):
        """
        super().__init__()
        self.var = var
        self.cte = norm_cte
        self.v   = v
        self.nip = None
        self.ii  = 0

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)

        self.nip = k.n2d

    def __call__(self, e):
        state_ph = e.state(FieldType.ph, self.var)

        np.copyto( self.v[self.ii : self.ii+self.nip], state_ph )
        self.v[self.ii : self.ii+self.nip] *= self.cte
        self.ii  += self.nip
        
        
class InitModalFieldValues(EntityOperator):
    """Initialization of a field from modal coefficients.

    Notes:
    * Do not use inside "engine.foreach" operators.
    """

    def __init__(self, var, v, norm_cte=1.0):
        """
        Args:
            var (FieldVariable):
            v (Numpy.array): values define over JUST ONE container.
            norm_cte (float):
        """
        super().__init__()
        self.bw  = [None, None]
        self.var = var
        self.cte = norm_cte
        self.v   = v
        self.ns  = [None, None]
        self.ii  = 0

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        ctx = EntityOperator.contexts

        self.bw[0] = class_quad_physbackward2d(k)
        self.bw[1] = class_tria_physbackward2d(k)

        self.ns[0] = ExpansionSize.get(0, key.order)
        self.ns[1] = ExpansionSize.get(1, key.order)

    def __call__(self, e):
        state_tr = e.state(FieldType.tr, self.var)

        np.copyto(state_tr, self.v[self.ii : self.ii+self.ns[e.type]] )
        state_tr /= self.cte
        self.ii  += self.ns[e.type]
        
        self.bw[e.type](state_tr, e.state(FieldType.ph, self.var) )

        
class GetModalFieldValues(EntityOperator):
    """Extraction of modal coefficients of a field.

    Notes:
    * Do not use inside "engine.foreach" operators.
    """

    def __init__(self, var, v, norm_cte=1.0):
        """
        Args:
            var (FieldVariable):
            v (Numpy.array): values define over JUST ONE container.
            norm_cte (float):
        """
        super().__init__()
        self.var = var
        self.cte = norm_cte
        self.v   = v
        self.ns  = [None, None]
        self.ii  = 0

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)

        self.ns[0] = ExpansionSize.get(0, key.order)
        self.ns[1] = ExpansionSize.get(1, key.order)

    def __call__(self, e):
        state_tr = e.state(FieldType.tr, self.var)

        np.copyto(self.v[self.ii : self.ii+self.ns[e.type]], state_tr)
        self.v[self.ii : self.ii+self.ns[e.type]] *= self.cte
        self.ii  += self.ns[e.type]

        
class InitFieldValues(EntityOperator):
    
    def __init__(self, var, f, norm_cte=1.0):
        super().__init__()
        self.fw  = [[None, None],[None, None]]
        self.ev  = [None, None]
        self.var = var
        self.cte = norm_cte
        self.f   = f
    
    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        ctx = EntityOperator.contexts

        #       type       subtype
        self.fw[ctx[0][0]][ctx[0][1]] = class_quad_wadg_physforward(k)
        self.fw[ctx[1][0]][ctx[1][1]] = class_quad_rwadg_physforward(k)
        self.fw[ctx[2][0]][ctx[2][1]] = class_tria_wadg_physforward(k)
        self.fw[ctx[3][0]][ctx[3][1]] = class_tria_rwadg_physforward(k)

        self.ev[0] = class_quad_physevaluator2d(k)
        self.ev[1] = class_tria_physevaluator2d(k)

    def __call__(self, e):
        e_type    = e.type
        e_geotype = e.geom_type
        state_ph  = e.state(FieldType.ph, self.var)
        
        values  = self.ev[e_type](self.f, e.get_coeff(), e_geotype, e.vmap)
        values /= self.cte     
        np.copyto(state_ph, values)
        
        self.fw[e_type][e.subtype](e.get_jacobian(), e.get_inv_mass(),
                    state_ph, e.state(FieldType.tr, self.var))
        
        
class ComputeEntityVolume(EntityOperator):

    def __init__(self):
        super().__init__()
        self.integ = [None, None]

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.integ[0] = class_quad_physintegrator2d(k)
        self.integ[1] = class_tria_physintegrator2d(k)

        self.ones = np.ones(k.n2d)

    def __call__(self, e):
       e.volume = self.integ[e.type](self.ones, e.get_jacobian())
        

class ComputeEntityMassMatrix(EntityOperator):

    def __init__(self):
        super().__init__()
        self.op = [None, None]
        self.ns = [None, None]

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.op[0] = class_quad_wadg_inverse_mass(k)
        self.op[1] = class_tria_wadg_inverse_mass(k)

        self.ns[0] = ExpansionSize.get(0, key.order)
        self.ns[1] = ExpansionSize.get(1, key.order)

    def __call__(self, e):
        J = e.get_jacobian()        
        assert J.shape[0] > 0
        
        S = self.ns[e.type]
        e.IMass = np.zeros((S, S))
        
        self.op[e.type](J, e.IMass)
       
