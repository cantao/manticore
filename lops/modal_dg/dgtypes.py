#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Modal DG abstractions
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

from enum import Enum, IntEnum
from manticore.geom.standard_geometries import BaseGeometry


class GaussPointsType(IntEnum):
    """
    Enumeration of the several types of Gauss points.

    We restrict ourselves to the needed cases:
    - Legendre (\f$\alpha = \beta = 0\f$)
    - Jacobi with \f$\alpha = 1\f$ and \f$\beta = 0\f$
    - Jacobi with \f$\alpha = 2\f$ and \f$\beta = 0\f$
    - Fake Gauss points at \f$\eta = -1, +1\f$, for face integration
    - Lobatto
    """

    Null = -1  # No points at all
    GaussLegendre = 0  # Gauss-Legendre (0,0)
    GaussJacobi10 = 1  # Gauss-Jacobi (1,0)
    GaussJacobi20 = 2  # Gauss-Jacobi (2,0) (TODO: eliminar????)
    NegativeOne = 3  # Evaluation at -1.0
    PositiveOne = 4  # Evaluation at +1.0
    GaussLobatto = 5  # Gauss-Legendre-Lobatto

    def size():
        """ Enum size (valid itens). """
        return 6


# end of manticore.lops.modal_dg.dgtypes.GaussPointsType


class StdRegionType(IntEnum):
    """
    Types of standard elemental regions.
    """
    DG_NoRegion = -1
    DG_Quadrangle = 0
    DG_Triangle = 1

    def size():
        """ Enum size (valid itens). """
        return 2


# end of manticore.lops.modal_dg.dgtypes.StdRegionType


def factory_class_region(region_type):
    """Factory for building classes with a default StdRegionType enumerate
    attribute. A class built by this factory may be used as parent of
    classes that require the StdRegionType enumerate as an input for
    conditional definition/initialization.

    """

    class Region:
        __slots__ = ['G']

        def __init__(self, rtype=region_type):
            if rtype in StdRegionType:
                self.G = rtype
            else:
                raise AssertionError("Incorrect initialization!")

    # end of factory_class_region.Region

    return Region


# end of manticore.lops.modal_dg.dgtypes.factory_class_region


class RegionAdapter(IntEnum):
    """
    Possible variations of the region.
    """
    WADG    = 0  # Full jacobians and WADG inverse mass approximation
    RWADG   = 1  # Optimized with constant jacobians and WADG inverse
                 # mass approximation
    Default = 2  # Full jacobians and mass matrix
    Regular = 3  # Optimized with constant jacobians

    def size():
        """ Enum size (valid itens). """
        return 4


# end of manticore.lops.modal_dg.dgtypes.RegionAdapter


class RegionInfo:
    # Region's number of vertices
    NumberOfVertices = [4, 3]

    # Region's number of faces
    NumberOfFaces = [4, 3]

    MeshToDG = {
        BaseGeometry.QUAD: StdRegionType.DG_Quadrangle,
        BaseGeometry.TRI: StdRegionType.DG_Triangle
    }

    def number_of_faces(region):
        assert region in StdRegionType
        assert region != StdRegionType.DG_NoRegion

        return RegionInfo.NumberOfFaces[region]

    def number_of_vertices(region):
        assert region in StdRegionType
        assert region != StdRegionType.DG_NoRegion

        return RegionInfo.NumberOfVertices[region]

    def mesh_to_dg(base_geometry):
        """
        Args:

            base_geometry (BaseGeometry)
        """
        return RegionInfo.MeshToDG.get(base_geometry, StdRegionType.DG_NoRegion)

    # def face_to_linear_coeff()
    # def first_axis()
    # def second_axis()

    # end of manticore.lops.modal_dg.dgtypes.RegionInfo


class PrincipalFunctionType(Enum):
    """ Enumeration for the Principal Functions types

        Principal function types are defined in K&S, page 101 as
            - OrthoPrincFuncA: \f$\tilda{\psi_p^a}\f$
            - OrthoPrincFuncB: \f$\tilda{\psi_{pq}^b}\f$
            - NullPrincFunc  : non valid principal function
    """

    Null = -1
    OrthoPrincFuncA = 0  # psi_a_p
    OrthoPrincFuncB = 1  # psi_b_pq

    def size():
        """ Enum size (valid itens). """
        return 2


# end of manticore.lops.modal_dg.dgtypes.PrincipalFunctionsType


class FacePrincipalFunctionType(IntEnum):
    """Enumeration for the Principal Functions types

        Principal face functions are a result of the combination of 2
        geometries, the active one, and the passive one (the
        neighbour).

    """
    Null = -1
    QuadQuad = 0  # Passive: quadrangle
    TriaQuad = 1  # Passive: quadrangle
    QuadTria = 2  # Passive: triangle
    TriaTria = 3  # Passive: triangle

    def size():
        """ Enum size (valid itens). """
        return 4


# end of manticore.lops.modal_dg.dgtypes.FacePrincipalFunctionsType

class FieldType(IntEnum):
    
    ph = 0 # Physical
    tr = 1 # Transformed

    def size():
        """ Enum size (valid itens). """
        return 2

# end of manticore.lops.modal_dg.dgtypes.FieldType

class FieldRole(IntEnum):
    
    State     = 0
    Residual  = 1
    Auxiliar1 = 2
    Auxiliar2 = 3

    def size():
        """ Enum size (valid itens). """
        return 4

# end of manticore.lops.modal_dg.dgtypes.FieldRole

class EntityRole(IntEnum):
    """
    Types of expansion entity's behavior.
    """
    
    GHOST      = -1
    INVALID    =  0
    PHYSICAL   =  1
    COMM_GHOST =  2

    def size():
        """ Enum size (valid itens). """
        return 4


# end of manticore.lops.modal_dg.dgtypes.EntityRole
