#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Single element abstraction.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from array import array
from copy import copy
from collections import defaultdict
import manticore.services.const as const
from manticore.geom.standard_geometries import (
    geom, number_of_edges, std_geometry_as_list, geometry_name)
from manticore.lops.modal_dg.dgtypes import (
    StdRegionType, RegionInfo, RegionAdapter, EntityRole, FieldRole, FieldType)
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.class_instances import (
    class_quad_stdbackward1d, class_tria_stdbackward1d,
    def_quad_wadg_inverse_mass, def_tria_wadg_inverse_mass)
from manticore.lops.modal_dg.fields import FieldVarList, LocalField
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.nodal_cg.interpol import InterpolationKey


class ExpansionNeighbourhood:

    __slots__ = ['neigh', 'neighface', 'face_v']

    def __init__(self, region):
        """ Constructor of ExpansionNeighbourhood objects.

        Args:

            region (dgtypes.StdRegionType): modal DG standard region

        Attributes:

            neigh (list): list of references to the adjacent entities.

            neighface (array): local face numbers of the adjacent entities.

            face_v (list(array.array)('L')): global IDs of the vertices
                of each face.

        """

        numfaces = RegionInfo.number_of_faces(region)

        self.neigh = [None for i in range(numfaces)]
        self.neighface = array('i',
                               [const.INVALID_RESULT for i in range(numfaces)])
        self.face_v = [array('L', [0, 0]) for i in range(numfaces)]

    def insert_neighbour(self, myface, entity, neigh_face):

        if myface < len(self.neigh):
            self.neigh[myface] = entity
            self.neighface[myface] = neigh_face
        else:
            self.neigh.append(entity)
            self.neighface.append(neigh_face)

    def get_neighbour(self, face_idx):
        assert self.neigh[face_idx]
        return self.neigh[face_idx]

    def is_valid_neighbour(self, face_idx):
        return bool(self.neigh[face_idx] != const.INVALID_RESULT)

    def get_neighbourhood(self):
        return self.neigh

    def get_neighbour_face_id(self, face_idx):
        assert self.neighface[face_idx] != const.INVALID_RESULT
        return self.neighface[face_idx]

    def set_face_vertices(self, face_idx, *IDs):
        self.face_v[face_idx][0] = IDs[0]
        self.face_v[face_idx][1] = IDs[1]

    def get_face_vertices(self, face_idx):
        return self.face_v[face_idx]


# end of manticore.lops.modal_dg.entity.ExpansionNeighbourhood


class CoGeometry(ExpansionNeighbourhood):
    """ Encapsulation of all co-geometric information (jacobians, mass).

    Notes:

        * Full geometrical Jacobians
        * Per integration-point inverted Jacobian matrices
        * Mass matrices
        * Face characteristic lentghs (I am not certain if this is used in 2d)

    """

    __slots__ = [
        '_vmap', '_fmap', 'C', 'V', 'J', 'IJM', 'IMass', 'FaceJ',
        'FaceCharLength'
    ]

    def __init__(self, volmap, facemap):
        """ Constructor of CoGeometry objects.

        Args:

            stdtype (dgtypes.StdRegionType): modal DG standard region

            volmap (CollapsedToGlobalMaps): standard region to global mapping.

            facemap (CollapsedToGlobalFaceMaps): standard region to
                global mapping restricted to faces.

        Attributes:

            _vmap (CollapsedToGlobalMaps): standard to global mapping.

            _fmap (CollapsedToGlobalFaceMaps): standard to global
                mapping restricted to faces.

            Mp (WADG_inverse_mass function): operator for calculanting
                the mass matrix inverse

            C (np.array): nodal coordinates of the Lagrange element
                representing the geometry, shape = (2, number_of_nodes).

            V (array.array('L')): vertices global IDs, array of
                unsigned longs.

            J  (np.array): Jacobians at the integration points.

            IJM (list(np.array)): Jacobian matrix inverse at each
                integration point.

            IMass (np.array): mass matrix inverse.

            FaceJ (list(np.array)): Jacobians at the integration
                points of each face.

            FaceCharLength (np.array): face charcteristic lengths (I
                do not know if they are necessary in 2D problems).

        """

        self._vmap = volmap
        self._fmap = facemap

        self.C = None
        self.V = array('L')
        self.J = None
        self.IJM = None
        self.IMass = None
        self.FaceJ = None
        self.FaceCharLength = None

    @property
    def vmap(self):
        return self._vmap

    @property
    def fmap(self):
        return self._fmap

    def set_coeff(self, C):
        self.C = np.copy(C)  # np.zeros((2, number_of_nodes(geotype)))

    def get_coeff(self):
        return self.C

    def set_vertices(self, V):
        self.V = copy(V)

    def get_vertices(self):
        return self.V

    def get_jacobian(self):
        return self.J

    def get_inv_jacobian_matrix(self):
        return self.IJM

    def get_inv_mass(self):
        return self.IMass

    def get_face_jacobian(self, face_idx):
        return self.FaceJ[face_idx]

    def get_face_char_length(self):
        return self.FaceCharLength

    def eval_jacobian(self, geotype, expansion_key, face_comm_sizes):
        """

        Args:
            geotype (geom.StandardGeometry): type of geometric interpolation.

            expansion_key (ExpansioKey):

        """

        n2d = expansion_key.n2d
        self.J = np.zeros(n2d)
        self.IJM = [np.zeros((2, 2)) for i in range(n2d)]

        gk2d = InterpolationKey.get_instance(geotype, n2d)
        self._vmap.inv_jacobian_matrix(gk2d, self.C, self.IJM, self.J)

        nfaces = number_of_edges(geotype)
        self.FaceJ = [np.zeros(face_comm_sizes[i]) for i in range(nfaces)]

        for face in range(nfaces):
            gk1d = InterpolationKey.get_instance(geotype,
                                                 face_comm_sizes[face], face)
            self._fmap.arc_length(gk1d, self.C, self.FaceJ[face])

    def eval_mass(self, stdtype, expansion_key):
        """

       Args:
            stdtype (dgtypes.StdRegionType): modal DG standard region

            expansion_key (ExpansioKey):

        """
        assert stdtype in StdRegionType
        assert self.J.shape[0] > 0

        S = ExpansionSize.get(stdtype, expansion_key.order)
        self.IMass = np.zeros((S, S))

        if stdtype == StdRegionType.DG_Quadrangle:
            Mp = def_quad_wadg_inverse_mass
        elif stdtype == StdRegionType.DG_Triangle:
            Mp = def_tria_wadg_inverse_mass
        else:
            raise AssertionError("Incorrect StdRegionType!")

        Mp(expansion_key, self.J, self.IMass)

    def eval_face_char_length(self, geotype):
        pass


# end of manticore.lops.modal_dg.entity.CoGeometry


class ExpansionEntity(CoGeometry):
    """ This class defines a single elemental expansion (a.k.a. finite element).

    """

    __slots__ = [
        '_geotype', '_type', '_subtype', '_ID', '_seqID', '_role', '_key',
        '_fc_n1d', '_vol', 'var', 'cte', 'field', 'cte_field', 'vertex_var',
        'vertex_field', 'vertex_map', 'op', '_gID', 'DR'
    ]

    def __init__(self, geotype, volmap, facemap):
        """ Constructor of ExpansionEntity objects.

        Args:
            geotype (geom.StandardGeometry): type of geometric interpolation.

            volmap (CollapsedToGlobalMaps): standard region to global mapping.

            facemap (CollapsedToGlobalFaceMaps): standard region to
                global mapping restricted to faces.

        Attributes:
            _geotype (geom.StandardGeometry): type of geometric interpolation.

            _type (dgtypes.StdRegionType): modal DG standard region

            _subtype (dgtypes.RegionAdapter):

            _ID (np.uint64): Entity id, from mesh generator

            _seqID (np.uint64): Sequential ID, for book keeping

            _role (dgtypes.EntityRole): entity's role (physical,
                ghost, comm_ghost)

            _key (ExpansionKey): expansion key associated to this element.

            _fc_n1d (array): number of integration points required for face
                communication between two elements with different expansions
                max(_key.n1d, ng.n1d) at each face.

            var (list(FieldVarList)): model/problem names of fields
                interpolated by the DG polynomial expansion. These fields can
                be: state variables, residual variables, auxiliary variables.

            cte (FieldVarList): model/problem names of fields that are constant
                by element.

            field (list(LocalField)): model/problem values of fields
                interpolated by the DG polynomial expansion. These
                fields can be: state variables, residual variables,
                auxiliary variables.

            cte_field (np.array): model/problem values of fields that are
                constant by element.

            vertex_var (FieldVarList): model/problem names of fields that are
                interpoled by vertex nodal shape functions.

             vertex_field (LocalField): model/problem values of
                fields that are interpoled by vertex nodal shape
                functions.

            vertex_map (dict): map from global vertices IDs to local indices
                (V->local vertex index).

        """

        assert geotype in std_geometry_as_list()

        self._geotype = geotype
        self._type = RegionInfo.mesh_to_dg(geom(geotype))

        self._subtype = RegionAdapter.WADG
        self._ID = np.uint64(0)
        self._seqID = np.uint64(0)
        self._role = EntityRole.GHOST
        self._gID = 0
        self._key = None
        self._fc_n1d = array('I', [])
        self._vol = 0.

        self.var = [None for i in range(FieldRole.size())]
        self.cte = None
        self.field = [None for i in range(FieldRole.size())]
        self.cte_field = None
        self.vertex_var = None
        self.vertex_field = None
        self.vertex_map = None
        self.DR = np.zeros(0)

        #
        # The StdBackward operator is identical to the PhysBackward
        # one. No need for an extra function calling overhead.
        #
        # self.op[0] = factory_stdbackward1d(DG_Quadrangle)
        # self.op[1] = factory_stdbackward1d(DG_Triangle)
        self.op = [None, None]

        #
        # Parents initialization
        #
        ExpansionNeighbourhood.__init__(self, self._type)
        CoGeometry.__init__(self, volmap, facemap)

    def init_variables(self, stv, rsv=[], axv=[]):
        """ Initialize the fields interpolated by the DG expansion
        within the element.

        Args:
            stv (list(FieldVariable)): State variables

            rsv (list(FieldVariable)): Residual variables

            axv (list(FieldVariable)): Auxiliar variables

        """

        role = self._role
        key = self._key

        if (role == EntityRole.PHYSICAL):

            S = ExpansionSize.get(self._type, key.order)
            N = key.n2d

            assert S > 0
            assert N > 0

            stv_size = len(stv)
            self.var[FieldRole.State] = FieldVarList.get_instance(*stv)
            self.field[FieldRole.State] = LocalField(stv_size, N, S)

            rsv_size = len(rsv)
            if len(rsv) > 0:
                self.var[FieldRole.Residual] = FieldVarList.get_instance(*rsv)
                self.field[FieldRole.Residual] = LocalField(rsv_size, 0, S)

            axv_size = len(axv)
            if len(axv) > 0:
                self.var[FieldRole.Auxiliar1] = FieldVarList.get_instance(*axv)
                self.field[FieldRole.Auxiliar1] = LocalField(axv_size, 0, S)
                self.var[FieldRole.Auxiliar2] = FieldVarList.get_instance(*axv)
                self.field[FieldRole.Auxiliar2] = LocalField(axv_size, 0, S)

            sz = rsv_size * S
            self.DR.resize( (sz, sz), refcheck=False )

        elif (role == EntityRole.COMM_GHOST):
            S = ExpansionSize.get(self._type, key.order)
            N = key.n2d

            assert S > 0
            assert N > 0

            stv_size = len(stv)
            self.var[FieldRole.State] = FieldVarList.get_instance(*stv)
            self.field[FieldRole.State] = LocalField(stv_size, N, S)

        elif (role == EntityRole.GHOST):
            # We need the expansion from the neighbour (physical elment)

            kn = self.get_neighbour(0).key
            N = kn.n1d + 1  # <--- Note: the correct size is n1d+1! K&S, p. 558

            stv_size = len(stv)
            self.var[FieldRole.State] = FieldVarList.get_instance(*stv)
            self.field[FieldRole.State] = LocalField(stv_size, N)

        else:
            raise AssertionError("Malformed Expansion entity!")

    def init_ctes(self, ctv):
        """Initialize the constant fields within the element.

        Args:

            ctv (list(FieldVariable)): constant fields.

        """
        if len(ctv) > 0:
            self.cte = FieldVarList.get_instance(*ctv)
            self.cte_field = np.zeros(len(ctv))

    def init_vertex_variables(self, vtv):
        """ Initialize fields that are interpoled by vertex nodal
        shape functions.

        Args:

            vtv (list(FieldVariable)): vertex fields.

        """
        if len(vtv) > 0:
            self.vertex_var = FieldVarList.get_instance(*vtv)
            self.vertex_field = LocalField(
                len(vtv), self._key.n2d, number_of_edges(self._geotype))

    def init_vertex_map(self, vertices_list):
        """ Initialize the map from global vertices IDs to local
        indices.

        Args:
            vertices_list (iterable(np.uint64))

        """

        assert len(vertices_list) == number_of_edges(self._geotype)
        local_idx = 0
        for vertex in vertices_list:
            self.vertex_map[vertex] = local_idx
            local_idx += 1

    @property
    def type(self):
        return self._type

    @property
    def subtype(self):
        return self._subtype

    @subtype.setter
    def subtype(self, value):
        assert value in RegionAdapter
        self._subtype = value

    @property
    def geom_type(self):
        return self._geotype

    @geom_type.setter
    def geom_type(self, value):
        assert value in std_geometry_as_list()
        self._geotype = value
        self._type = base_to_std_region[geom(value)]

    @property
    def ID(self):
        return self._ID

    @ID.setter
    def ID(self, value):
        self._ID = np.uint64(value)

    @property
    def seqID(self):
        return self._seqID

    @seqID.setter
    def seqID(self, value):
        self._seqID = np.uint64(value)

    @property
    def gID(self):
        return self._gID

    @gID.setter
    def gID(self, value):
        self._gID = int(value)

    @property
    def role(self):
        return self._role

    @role.setter
    def role(self, value):
        assert value in EntityRole
        self._role = value

    @property
    def key(self):
        return self._key

    @key.setter
    def key(self, k):
        assert type(k) is ExpansionKey
        self._key = k
        self.op[0] = class_quad_stdbackward1d(k)
        self.op[1] = class_tria_stdbackward1d(k)

    @property
    def n1d(self):
        return self._key.n1d

    @property
    def volume(self):
        return self._vol

    @volume.setter
    def volume(self, value):
        assert value > 0.
        self._vol = value

    def init_face_comm_sizes(self):
        """Number of integration points on each face.

        Notes:

        * According to K&S p.558, the number of integration points
        on each face must be n1d+1 for obtaining the correct mesh
        convergence.
        """
        n = self.n1d

        if ((self.role == EntityRole.PHYSICAL)
                or (self.role == EntityRole.COMM_GHOST)):
            for ng in self.neigh:
                self._fc_n1d.append(max(n + 1, ng.n1d + 1))

    def face_comm_size(self, ff):
        return self._fc_n1d[ff]

    @property
    def face_comm_sizes(self):
        return self._fc_n1d

    def set_vertices(self, V):
        CoGeometry.set_vertices(self, V)

        nfaces = number_of_edges(self._geotype)

        if nfaces == 3:
            self.set_face_vertices(0, V[0], V[1])
            self.set_face_vertices(2, V[1], V[2])
            self.set_face_vertices(1, V[2], V[0])
        elif nfaces == 4:
            self.set_face_vertices(0, V[0], V[1])
            self.set_face_vertices(3, V[1], V[2])
            self.set_face_vertices(1, V[2], V[3])
            self.set_face_vertices(2, V[3], V[0])
        else:
            raise AssertionError("Incorrect number of faces!")

    def eval_jacobian(self):
        CoGeometry.eval_jacobian(self, self._geotype, self._key, self._fc_n1d)

    def eval_mass(self):
        CoGeometry.eval_mass(self, self._type, self._key)

    def eval_face_char_length(self):
        pass

    def normals_tangents(self, face_id, n, t):
        # The correct number of integration points on faces is n1d+1,
        # see K&S p.558.
        k = InterpolationKey.get_instance(self._geotype, self._key.n1d + 1,
                                          face_id)
        self._fmap.normals_tangents(k, self.C, n, t)

    def get_field(self, role, field_type, v):
        """ Get field. """
        return self.field[role].get(field_type, self.var[role](v))

    def state(self, field_type, v):
        role = FieldRole.State
        return self.field[role].get(field_type, self.var[role](v))

    def residual(self, field_type, v):
        role = FieldRole.Residual
        return self.field[role].get(field_type, self.var[role](v))

    def auxiliar1(self, field_type, v):
        role = FieldRole.Auxiliar1
        return self.field[role].get(field_type, self.var[role](v))

    def auxiliar2(self, field_type, v):
        role = FieldRole.Auxiliar2
        return self.field[role].get(field_type, self.var[role](v))

    def copy_field(self, e):

        if ((self._role == EntityRole.PHYSICAL)
                or (self._role == EntityRole.COMM_GHOST)):

            for role in FieldRole:
                self.field[role].copy(FieldType.ph, e.field[role])
                self.field[role].copy(FieldType.tr, e.field[role])

    def get_face_field(self, field_role, v, face_id, face_values):

        if ((self._role == EntityRole.PHYSICAL)
                or (self._role == EntityRole.COMM_GHOST)):
            # Returns values at i.p.s on the face:
            self.op[self._type].at_ips(face_id,
                                       self.get_field(field_role, FieldType.tr,
                                                      v), face_values)

        elif (self._role == EntityRole.GHOST):
            # Values are already stored at i.p.s on the face but, in
            # this case, it only makes sense field_role==FieldRole.State
            # and field_type==FieldType.ph).

            assert field_role == FieldRole.State

            np.copyto(face_values, self.get_field(field_role, FieldType.ph, v))

        else:
            raise AssertionError("Malformed Expansion entity!")

    def get_face_field_as_passive(self, field_role, v, actV, actF, actNIP,
                                  pasF, face_values):

        assert ((self._role == EntityRole.PHYSICAL)
                or (self._role == EntityRole.COMM_GHOST))

        face_values = self.op[self._type].at_ips_as_passive(
            actV, actF, actNIP, pasF,
            self.get_field(field_role, FieldType.tr, v), face_values)

    def get_cte(self, v):
        return self.cte_field[self.cte(v)]

    def set_cte(self, v, value):
        self.cte_field[self.cte(v)] = value

    def get_vertex_field(self, field_type, v):
        return self.vertex_field.get(field_type, self.vertex_var(v))

    def get_vertex_value(self, v, node_global_id):
        assert node_global_id in self.vertex_map
        return self.vertex_field.get(
            FieldType.tr, self.vertex_var(v))[self.vertex_map[node_global_id]]

    def set_vertex_value(self, v, node_global_id, value):
        assert node_global_id in self.vertex_map
        self.vertex_field.get(
            FieldType.tr,
            self.vertex_var(v))[self.vertex_map[node_global_id]] = value

    def __repr__(self):

        msg = '<{} (ID: {}, seqID: {}, geo: {}, subtype: {}, role: {})'.format(
            self.__class__, self.ID, self.seqID,
            geometry_name(self.geom_type), self.subtype, self.role)
        msg += '\n{}'.format(self.key.key)
        msg += '\nVertices: {}'.format(self.get_vertices())
        msg += '\nNodal coordinates:\n{}'.format(self.get_coeff())
        msg += '\nNeighborhood([ '

        neigh = self.get_neighbourhood()
        for ng in neigh:
            if ng is not None:
                msg += '{} '.format(ng.ID)

        msg += '])'

        msg += '\n>'

        return msg


# end of manticore.lops.modal_dg.entity.ExpansionEntity

from manticore.lops.modal_dg.geomfactors import CollapsedToGlobalMaps
from manticore.lops.modal_dg.geomfactors import CollapsedToGlobalFaceMaps


class ExpansionEntityFactory:

    CGMaps = CollapsedToGlobalMaps(ExpansionKeyFactory.gaussWIP)
    CGFMaps = CollapsedToGlobalFaceMaps(ExpansionKeyFactory.gaussWIP)

    def make(geotype):
        e = ExpansionEntity(geotype, ExpansionEntityFactory.CGMaps,
                            ExpansionEntityFactory.CGFMaps)

        return e


# end of manticore.lops.modal_dg.entity.ExpansionEntityFactory


class GlobalExpansionContainer:
    def __init__(self):

        self._data = []  # sequential IDs -> ExpansionEntity
        self._by_id = {}  # id from mesh   -> ExpansionEntity
        self._by_std = defaultdict(
            list)  # StdRegionType -> list(ExpansionEntity)
        self._by_role = defaultdict(
            list)  # EntityRole -> list(ExpansionEntity)
        self._by_adapter = defaultdict(list)  # RegionAdapter -> list
        self._seq_id = np.uint64(0)  # Counter

    def size(self):
        return len(self._data)

    def __len__(self):
        return len(self._data)

    def empty(self):
        return bool(not len(self._data))

    def insert(self, e):
        """ Insert an EnpansionEntity instance in the container.

        Args:

            e (ExpansionEntity): instance with ID, type, subtype and
                role already set up.

        """

        assert isinstance(e, ExpansionEntity)

        e.seqID = self._seq_id
        self._seq_id += 1

        self._data.append(e)  # only a reference is appended here

        if e.ID in self._by_id:
            raise AssertionError(
                "Attempt to overwrite a previously defined entity!")
        else:
            self._by_id[e.ID] = self._data[-1]

        self._by_std[e.type].append(self._data[-1])
        self._by_role[e.role].append(self._data[-1])
        self._by_adapter[e.subtype].append(self._data[-1])

        return self._data[-1]

    def find_by_id(self, ID):
        """Returns entity by its unique ID."""
        return self._by_id.get(ID)


# end of manticore.lops.modal_dg.entity.GlobalExpansionContainer


class GlobalSequentialIterator:
    """Iterator for ExpansionContainer objects following the insertion
       order of the entity in the container."""

    def __init__(self, container):
        self.wrapped = container

    def __iter__(self):

        self.it = iter(self.wrapped._data)

        return self

    def __next__(self):

        try:
            item = next(self.it)
        except StopIteration:
            raise StopIteration

        return item

    def __contains__(self, e):

        return e.ID in self.wrapped._by_id

    def find(self, e):
        return self.wrapped._by_id.get(e.ID)


# end of manticore.lops.modal_dg.entity.GlobalSequentialIterator


class GlobalIdentifierIterator:
    """Iterator for ExpansionContainer objects following an internally
       hashed ordering based on the externally defined entities' IDs.

    """

    def __init__(self, container):
        self.wrapped = container

    def __iter__(self):

        self.it = iter(self.wrapped._by_id)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.wrapped._by_id[key]

    def __contains__(self, e):

        return e.ID in self.wrapped._by_id

    def find(self, e):
        return self.wrapped._by_id.get(e.ID)


# end of manticore.lops.modal_dg.entity.GlobalIdentifierIterator


class GlobalRoleIterator:
    def __init__(self, container):
        self.wrapped = container

    def __iter__(self):

        self.it = iter(self.wrapped._by_role)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.wrapped._by_role[key]

    def __contains__(self, role):

        return role in self.wrapped._by_role

    def find(self, role):
        return self.wrapped._by_role.get(role)


# end of manticore.lops.modal_dg.entity.GlobalRoleIterator


class GlobalAdapterIterator:
    def __init__(self, container):
        self.wrapped = container

    def __iter__(self):

        self.it = iter(self.wrapped._by_adapter)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.wrapped._by_adapter[key]

    def __contains__(self, adapter):

        return adapter in self.wrapped._by_adapter

    def find(self, adapter):
        return self.wrapped._by_adapter.get(adapter)


# end of manticore.lops.modal_dg.entity.GlobalAdapterIterator
