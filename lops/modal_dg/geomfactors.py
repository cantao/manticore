#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Geometric procedures for local DG computations.
#
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from math import sqrt
from manticore.geom.standard_geometries import BaseGeometry
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.dgtypes import RegionInfo
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions
from manticore.lops.nodal_cg.interpol import InterpolationTable
from manticore.lops.nodal_cg.interpol import hasblInterpolationKey
from manticore.lops.modal_dg.expansion import ExpansionKey

def m1p2(eta_1a, eta_2a):
    return -1.0 * eta_1a, eta_2a


def p1m2(eta_1a, eta_2a):
    return eta_1a, -1.0 * eta_2a


def p2p1(eta_1a, eta_2a):
    return eta_2a, eta_1a


def m2m1(eta_1a, eta_2a):
    return -1.0 * eta_2a, -1.0 * eta_1a


def pOm1(eta_1a, eta_2a):
    return np.array([1.0]), -1.0 * eta_1a


def pOp1(eta_1a, eta_2a):
    return np.array([1.0]), eta_1a


def pOm2(eta_1a, eta_2a):
    return np.array([1.0]), -1.0 * eta_2a


def pOp2(eta_1a, eta_2a):
    return np.array([1.0]), eta_2a


def m2mO(eta_1a, eta_2a):
    return -1.0 * eta_2a, np.array([-1.0])


def p2pO(eta_1a, eta_2a):
    return eta_2a, np.array([1.0])


def mOp2(eta_1a, eta_2a):
    return np.array([-1.0]), eta_2a


class FaceDocking:
    "Mapping from two faces (active and passive) on standard regions."

    # Mappings QuadQuad[Active Face][Passive Face]
    tQQ = [[m1p2, p1m2, p2p1, m2m1], [p1m2, m1p2, m2m1, p2p1],
           [p2p1, m2m1, p1m2, m1p2], [m2m1, p2p1, m1p2, p1m2]]

    # Mappings QuadTria[Active Face][Passive Face]
    tQT = [[m1p2, p2p1, pOm1], [p1m2, m2m1, pOp1], [p2p1, p1m2, pOp2],
           [m2m1, m1p2, pOm2]]

    # Mappings TriaQuad[Active Face][Passive Face]
    tTQ = [[m1p2, p1m2, p2p1, m2m1], [p2p1, m2m1, p1m2, m1p2],
           [m2mO, p2pO, mOp2, pOm2]]

    # Mappings TriaTria[Active Face][Passive Face]
    tTT = [[m1p2, p2p1, pOm1], [p2p1, p1m2, pOp2], [m2mO, mOp2, pOm2]]

    # Mappings [Active Region][Passive Region], see dgtypes.StdRegionType
    tAP = [[tQQ, tQT], [tTQ, tTT]]

    def __init__(self):
        self.mapping = None

    def setup(self, actV, actF, pasV, pasF):
        """Selects the suitable mapping according to active\passive region and
        active\passive local face.

        """
        self.mapping = FaceDocking.tAP[actV][pasV][actF][pasF]

    def eval(self, eta_1a, eta_2a):
        """Evaluates the mapping (eta_1, eta_2)_active -> (eta_1, eta_2)_passive
        selected by setup method.

        Args:
            eta_1a (numpy.array) [in]: local coordinates [-1,+1]
            eta_2a (numpy.array) [in]: local coordinates [-1,+1[

        Returns:
            numpy.array[dtype=numpy.float_], numpy.array[dtype=numpy.float_]

        """
        return self.mapping(eta_1a, eta_2a)


# end of manticore.lops.modal_dg.geomfactors.FaceDocking


def factory_colapsed_to_std_maps(region_type):
    """ Mapping eta->xi, Karniadakis & Sherwin 2nd ed, Chapter 3, p.93-94.
    """

    if region_type == StdRegionType.DG_Quadrangle:

        class CollapsedToStdMap:
            def eval(eta1, eta2):
                # xi1 = eta1; xi2 = eta2

                return eta1, eta2

            def inverse(xi1, xi2):

                return xi1, xi2

        return CollapsedToStdMap

    elif region_type == StdRegionType.DG_Triangle:

        class CollapsedToStdMap:
            def eval(eta1, eta2):

                # xi1 = 0.5*( 1.0+eta1 )*( 1.0-eta2 )-1.0

                # Optimized for numpy arrays
                xi1 = np.ones(eta1.shape[0])
                xi1 += eta1
                xi1 *= (1.0 - eta2)
                xi1 *= 0.5
                xi1 -= 1.0

                # xi2 = eta2

                return xi1, eta2

            def inverse(xi1, xi2):
                """
                Std to collapsed mapping.

                Args:
                    xi1 (double) [in]: [-1, 1]
                    xi2 (double) [in]: [-1, 1[
                """

                # Optimized for numpy arrays
                eta1 = np.ones(xi1.shape[0])
                eta1 += xi1
                eta1 /= (1.0 - xi2)
                eta1 *= 2.0
                eta1 -= 1.0

                return eta1, xi2

        return CollapsedToStdMap

    else:
        raise AssertionError("Incorrect StdRegionType!")


# end of manticore.lops.modal_dg.geomfactors.factory_colapsed_to_std_maps


def factory_form_factors(region_type):
    """ Form Factors, as in K&S pg. 150

    Chain rule used to transform derivatives on collapsed coordinates to
    derivatives on standard coordinates:

        [dxi_1, dxi_2]^T = [Jac(eta<-xi)]*[deta_1, deta_2]^T

    """

    if region_type == StdRegionType.DG_Quadrangle:

        class FormFactors:
            """Quadrangle form factors (K&S, page 150)

            Args:

                eta_1,2 (double)[in] Integration points on collapsed coordinates
                deta1,2 (double)[in] Derivatives on collapsed coordinates

            Returns:

                double: Derivatives dxi1, dx2 on standard coordinates on output.

            """

            def eval_scalar(eta1, eta2, deta1, deta2):
                # dxi1 = deta1, dxi2 = deta2

                return deta1, deta2

            def eval(eta1, eta2, deta1, deta2, dxi1, dxi2):

                np.copyto(dxi1, deta1)
                np.copyto(dxi2, deta2)

        return FormFactors

    elif region_type == StdRegionType.DG_Triangle:

        class FormFactors:
            """Triangle form factors (K&S, page 150)

            Args:

                eta_1,2 (double)[in] Integration points on collapsed coordinates
                deta1,2 (double)[in] Derivatives on collapsed coordinates

            Returns:

                double: Derivatives dxi1, dx2 on standard coordinates on output.

            """

            def eval_scalar(eta1, eta2, deta1, deta2):

                m2 = 1.0 - eta2

                dxi1 = 2.0 * deta1 / m2
                dxi2 = (1.0 + eta1) * deta1 / m2 + deta2

                return dxi1, dxi2

            def eval(eta1, eta2, deta1, deta2, dxi1, dxi2):
                """ Version for numpy arrays.

                Notes:

                    * Note the in-place operations used on dxi1, dxi2.

                """
                m2 = np.ones(eta1.shape[0])
                m2 -= eta2

                np.copyto(dxi1, deta1)
                dxi1 *= 2.
                dxi1 /= m2

                np.copyto(dxi2, eta1)
                dxi2 += 1.
                dxi2 *= deta1
                dxi2 /= m2
                dxi2 += deta2

        return FormFactors

    else:
        raise AssertionError("Incorrect StdRegionType!")


def factory_std_to_area_maps(region_type):

    def keep_tangent(t1, t2):
        return t1, t2

    def invert_tangent(t1, t2):
        return -t1, -t2

    if region_type == StdRegionType.DG_Quadrangle:

        def dt_for_t_equiv_xi1(dl1, dl2):
            """d/dt, t \equiv xi1, for line integral on the boundary.

            Args:

                dl1 (double)[in]: d/dl1.

                dl2 (double)[in]: d/dl2.

            Returns:

                double: d/dt, t \equiv xi1, xi1 \equiv l1

            """

            return dl1

        def dt_for_t_equiv_xi2(dl1, dl2):
            """d/dt, t \equiv xi2, for line integral on the boundary.

            Args:

                dl1 (double)[in]: d/dl1.

                dl2 (double)[in]: d/dl2.

            Returns:

                double: d/dt, t \equiv xi2, xi2 \equiv l2

            """

            return dl2        

        class StdToAreaMap:

            dt_funcs = [
                dt_for_t_equiv_xi1,
                dt_for_t_equiv_xi1,  # faces 0 and 1
                dt_for_t_equiv_xi2,
                dt_for_t_equiv_xi2  # faces 2 and 3
            ]

            correct_tg = [
                keep_tangent,
                invert_tangent,
                invert_tangent,
                keep_tangent
                ]

            def eval(xi1, xi2):  # l = f(xi)

                return xi1, xi2

            def inverse(xi1, xi2):  # xi = f^{-1}(l)

                return xi1, xi2

            def det_jacobian():  # det(grad f(xi))
                return 1.0

            def jacobian():
                J00 = 1.0  # dl_1/dxi_1
                J01 = 0.0  # dl_2/dxi_1
                J10 = 0.0  # dl_1/dxi_2
                J11 = 1.0  # dl_2/dxi_2

                return J00, J01, J10, J11

            def inv_jacobian():
                IJ00 = 1.0  # dxi_1/dl_1
                IJ01 = 0.0  # dxi_2/dl_1
                IJ10 = 0.0  # dxi_1/dl_2
                IJ11 = 1.0  # dxi_2/dl_2

                return IJ00, IJ01, IJ10, IJ11

            def complement_inv_jacobian(invJ):
                pass

            def dt(dl1, dl2, face_id):
                """d/dt at face face_id.

                t scalar parameter for the line defining the boundary at
                face face_id.

                """
                return StdToAreaMap.dt_funcs[face_id](dl1, dl2)

            def correct_tangent(t1, t2, face_id):
                return StdToAreaMap.correct_tg[face_id](t1, t2)

        return StdToAreaMap

    elif region_type == StdRegionType.DG_Triangle:

        def dt_face0(dl1, dl2):
            """d/dt, t \equiv xi1, for line integral on the boundary at face0.

            Args:

                dl1 (double)[in]: d/dl1.

                dl2 (double)[in]: d/dl2.

            Returns:

                double: d/dt = d/dxi1 = dl1/dxi1*d/dl1 + dl2/dxi1*d/dl2

            """

            return -0.5 * dl1 + 0.5 * dl2

        def dt_face1(dl1, dl2):
            """d/dt, t \equiv xi2, for line integral on the boundary at face1.

            Args:

                dl1 (double)[in]: d/dl1.

                dl2 (double)[in]: d/dl2.

            Returns:

                real: d/dt = d/dxi2 = dl1/dxi2*d/dl1 + dl2/dxi2*d/dl2

            """

            return -0.5 * dl1

        def dt_face2(dl1, dl2):
            """d/dt, for line integral on the boundary at face2.

            t \equiv xi2, xi1 = -xi2
            d/dt = dxi1/dt*d/dxi1 + dxi2/dt*d/dxi2 = -d/dxi1 + d/dxi2
            d/dt = -(dl1/dxi1*d/dl1 + dl2/dxi1*d/dl2)
                   +(dl1/dxi2*d/dl1 + dl2/dxi2*d/dl2)
                 = (dl1/dxi2 - dl1/dxi1)*d/dl1 + (dl2/dxi2 - dl2/dxi1)*d/dl2
                 = -dl2/dxi1*d/dl2

            Args:

                dl1 (double)[in]: d/dl1.

                dl2 (double)[in]: d/dl2.

            Returns:

                double: d/dt = -dl2/dxi1*d/dl2

            """

            return -0.5 * dl2

        class StdToAreaMap:
            """Mapping xi->area coords, Karniadakis & Sherwin 2nd ed, Chap
            3, p.97-99.
            """

            dt_funcs = [dt_face0, dt_face1, dt_face2]

            correct_tg = [keep_tangent, invert_tangent, keep_tangent]

            def eval(xi1, xi2):  # l = f(xi)

                # l3 = 0.5*(1+xi2)
                l3 = np.ones(xi1.shape[0])
                l3 += xi2
                l3 *= 0.5

                # l1 = 0.5*(1-xi1) - 0.5*(1+xi2)
                l1 = np.ones(xi1.shape[0])
                l1 -= xi1
                l1 *= 0.5
                l1 -= l3

                # l2 = 0.5*(1+xi1)
                l2 = np.ones(xi1.shape[0])
                l2 += xi1
                l2 *= 0.5

                return l1, l2

            def inverse(l1, l2):  # xi = f^{-1}(l)

                l3 = np.ones(l1.shape[0])
                l3 -= l1
                l3 -= l2

                # xi1 = 2*l2-1
                xi1 = 2.0 * l2
                xi1 -= 1.0

                # xi2 = 2*(1-l1-l2)-1
                xi2 = 2.0 * l3
                xi2 -= 1.0

                return xi1, xi2

            def det_jacobian():  # det(grad f(xi))
                return 0.25

            def jacobian():
                J00 = -0.5  # dl_1/dxi_1
                J01 = 0.5  # dl_2/dxi_1
                J10 = -0.5  # dl_1/dxi_2
                J11 = 0.0  # dl_2/dxi_2

                return J00, J01, J10, J11

            def inv_jacobian():
                IJ00 = 0.0  # dxi_1/dl_1
                IJ01 = -2.0  # dxi_2/dl_1
                IJ10 = 2.0  # dxi_1/dl_2
                IJ11 = -2.0  # dxi_2/dl_2

                return IJ00, IJ01, IJ10, IJ11

            def complement_inv_jacobian(invJ):
                """Executes [dxi/dx] = [dl/dx]*[dxi/dl]

                Args:

                    invJ (list(numpy.array)) [inout] [dl/dx],
                        invJ[ii].shape = (2,2)

                """

                for IJ in invJ:

                    IG00 = IJ[0, 0]
                    IG01 = IJ[0, 1]
                    IG10 = IJ[1, 0]
                    IG11 = IJ[1, 1]

                    IJ[0, 0] = 2. * IG01
                    IJ[1, 0] = 2. * IG11
                    IJ[0, 1] = -2. * (IG00 + IG01)
                    IJ[1, 1] = -2. * (IG10 + IG11)

            def dt(dl1, dl2, face_id):
                """d/dt at face face_id.

                t scalar parameter for the line defining the boundary at
                face face_id.

                """
                return StdToAreaMap.dt_funcs[face_id](dl1, dl2)

            def correct_tangent(t1, t2, face_id):
                return StdToAreaMap.correct_tg[face_id](t1, t2)

        return StdToAreaMap

    else:
        raise AssertionError("Incorrect StdRegionType!")


# end of manticore.lops.modal_dg.geomfactors.factory_std_to_area_maps


class CollapsedToGlobalMaps:
    """ Geometric interpolation from collapsed to real elements.

    Notes:

        * This class takes care of the complete mapping from collapsed to
            real coordinates: eta->xi->area->x

        * This class contains a hash table of InterpolationTable objects
           (continuous nodal shape functions evaluated at integration points)
           indexed by hasblInterpolationKey objects. These objects describe the
           geometric interpolation of real elements (in terms of local area
           coordinates).

        * All InterpolationTable objects of this table are associated to the
            same JacobiGaussQuadratures object.

    """

    def __init__(self, gauss_quad):
        """Constructor for CollapsedToGlobalMaps objects.

        Attributes:

            gauss_quad (JacobiGaussQuadratures): weights and integration points
                used to build PrincipalFunctionTable objects.

            tables (dict(InterpolationTable)): area->x mapping
                hash table of InterpolationTable objects indexed as:
                [hasblInterpolationKey]: InterpolationTable

            eta_xi (dict(CollapsedToStdMap)): eta->xi mapping
                [BaseGeometry]: CollapsedToStdMap

            xi_area (dict(StdToAreaMap)): xi->area mapping
                [BaseGeometry]: StdToAreaMap

        Args:

            gauss_quad (JacobiGaussQuadratures): weights and integration points.
        """

        self.gauss_quad = gauss_quad

        # dict {hasblInterpolationKey: InterpolationTable}
        self.tables = {}

        # dict {StdRegionType: static class}
        self.eta_xi = {}

        # dict {StdRegionType: static class}
        self.xi_area = {}

    def __call__(self, key):
        """Returns a InterpolationTable object built according to key.

        Args:

            key (InterpolationKey)[in]

        Returns:

            InterpolationTable

        Notes:

           * Each table entry is created when first called. Consecutive calls
                to the same argument retrive an already computed
                InterpolationTable object from the hash table.

        """

        k = key.hasblkey

        assert type(k) is hasblInterpolationKey

        if k not in self.tables:
            N = int(sqrt(key.nip))
            shape = key.shape

            if shape in RegionInfo.MeshToDG:
                region = RegionInfo.mesh_to_dg(shape)
            else:
                raise AssertionError("Incorrect StandardGeometry!")

            if shape not in self.eta_xi:
                self.eta_xi[shape] = factory_colapsed_to_std_maps(region)
                self.xi_area[shape] = factory_std_to_area_maps(region)

            ktemp = ExpansionKey.get_instance(1, N) # order = 1: any
                                                    # value works here
            ktemp.set_gauss_quadrature(self.gauss_quad)
            px1 = ktemp.p(region, 1)  # integration points in eta1 direction
            px2 = ktemp.p(region, 2)  # integration points in eta2 direction
            px1, px2 = np.meshgrid(px1, px2)

            px1_s, px2_s = self.eta_xi[shape].eval(
                np.ravel(px1), np.ravel(px2))
            px1_a, px2_a = self.xi_area[shape].eval(px1_s, px2_s)

            self.tables[k] = InterpolationTable(key, px1_a, px2_a)

        return self.tables[k]

    def det_jacobian(self, key, coords, detJ):
        """Returns the values of the Jacobian determinant of the mapping xi->area->x at integration points.

        Args:
            key (InterpolationKey)[in]

            coords (numpy.array)[in] Nodal coordinates of the geometric
                interpolation.

            detJ (numpy.array)[out] Array for the storage of determinant
                values at integration points (in streamlined ordering; see
                manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).
                This array must enter arealdy allocated with the correct size.

        Notes:

            * The Jacobian determinant of the mapping eta->xi is already
                stored at the integration weights.
        """

        # det Jacobian area-to-real
        self.__call__(key).det_jacobian(coords, detJ)

        # det Jacobian xi-to-area
        detJ *= self.xi_area[key.shape].det_jacobian()

    def inv_jacobian_matrix(self, key, coords, invJ, detJ):
        """Computes the Jacobian matrix inverse at all integration points.

        Args:
            key (InterpolationKey)    [in]

            coordinates (numpy.array) [in]

            invJ (list(numpy.array))  [out] List of 2x2 numpy.array's

            detJ (numpy.array)        [out]
        """

        # invJ = [dl/dx]
        self.__call__(key).inv_jacobian_matrix(coords, invJ, detJ)

        shape = key.shape

        # [dxi/dx] = [dl/dx]*[dxi/dl]
        self.xi_area[shape].complement_inv_jacobian(invJ)

        # det[dx/dxi] = det[dx/dl]*det[dl/dxi]
        detJ *= self.xi_area[shape].det_jacobian()


# end of manticore.lops.modal_dg.geomfactors.CollapsedToGlobalMaps


class CollapsedToGlobalFaceMaps:
    def __init__(self, gauss_quad):
        """Constructor for CollapsedToGlobalFaceMaps objects.

        Attributes:

            gauss_quad (JacobiGaussQuadratures): weights and integration points
                used to build PrincipalFunctionTable objects.

            tables (dict(InterpolationTable)): area->x mapping
                hash table of InterpolationTable objects indexed as:
                [hasblInterpolationKey]: InterpolationTable

            eta_xi (dict(CollapsedToStdMap)): eta->xi mapping
                [BaseGeometry]: CollapsedToStdMap

            xi_area (dict(StdToAreaMap)): xi->area mapping
                [BaseGeometry]: StdToAreaMap

        Args:

            gauss_quad (JacobiGaussQuadratures): weights and integration points.
        """

        self.gauss_quad = gauss_quad

        # dict {hasblInterpolationKey: InterpolationTable}
        self.tables = {}

        # dict {StdRegionType: static class}
        self.eta_xi = {}

        # dict {StdRegionType: static class}
        self.xi_area = {}

    def __call__(self, key):
        """Returns a InterpolationTable object built according to key.

        Args:

            key (InterpolationKey)[in]

        Returns:

            InterpolationTable

        Notes:

           * Each table entry is created when first called. Consecutive calls
                to the same argument retrive an already computed
                InterpolationTable object from the hash table.

        """

        k = key.hasblkey

        assert type(k) is hasblInterpolationKey

        if k not in self.tables:

            shape = key.shape
            nip   = key.nip
            face  = key.restricted_to_face

            assert face > -1

            if shape in RegionInfo.MeshToDG:
                region = RegionInfo.mesh_to_dg(shape)
            else:
                raise AssertionError("Incorrect StandardGeometry!")

            #
            # Be careful when using ExpansionKey to get integration
            # points on faces: the second argument of the constructor
            # is the one-dimensional number of integration points for
            # numerical integration INSIDE the element. The number of
            # integration points generated on faces by ExpansionKey is
            # the second argument of the constructor PLUS ONE. Thus,
            # if you know the number of integration points on face (as
            # here) you must SUBTRACT ONE from it to get the correct
            # integration points on faces (see the assertion below).
            #
            ktemp = ExpansionKey.get_instance(1, nip-1) # order = 1: any
                                                        # value works here
            ktemp.set_gauss_quadrature(self.gauss_quad)

            p1 = ktemp.p_on_face(region, face, 1)
            p2 = ktemp.p_on_face(region, face, 2)

            n1 = p1.shape[0]
            n2 = p2.shape[0]

            assert n1 * n2 == nip  # Checks if n1 or n2 is 1 as it must be

            if shape not in self.eta_xi:
                self.eta_xi[shape] = factory_colapsed_to_std_maps(region)
                self.xi_area[shape] = factory_std_to_area_maps(region)

            p1, p2 = np.meshgrid(p1, p2)
            p1_s, p2_s = self.eta_xi[shape].eval(np.ravel(p1), np.ravel(p2))
            p1_a, p2_a = self.xi_area[shape].eval(p1_s, p2_s)

            self.tables[k] = InterpolationTable(key, p1_a, p2_a)

        return self.tables[k]

    def arc_length(self, key, coordinates, arc):
        """Retuns the arc lengths at integration points on the boundary.

        Args:

            key (InterpolationKey)[in]

            coordinates (numpy.array)[in] Nodal coordinates of the geometric
                interpolation.

            arc (numpy.array)[out] Array for the storage of arc lengths
                values at integration points. This array must enter arealdy
                allocated with the correct size.

        Notes:
            * arc length = sqrt(dxdt**2 + dydt**2)

            * (x,y): geometry of the real element

            * t: scalar parameterization of the real key.face at the standard
                element (mapping xi->area->x). See factory_std_to_area_maps.

            * The Jacobian determinant of the mapping eta->xi is already
                stored at the integration weights.

        """

        sf    = self.__call__(key)
        nip   = key.nip
        shape = key.shape
        face  = key.face

        mapp_xi_area = self.xi_area[shape]

        for ii in range(nip):
            dsf = sf.d_row(ii)

            dxdl1 = np.dot(coordinates[0, :], dsf[0, :])
            dxdl2 = np.dot(coordinates[0, :], dsf[1, :])
            dydl1 = np.dot(coordinates[1, :], dsf[0, :])
            dydl2 = np.dot(coordinates[1, :], dsf[1, :])

            dxdt = mapp_xi_area.dt(dxdl1, dxdl2, face)
            dydt = mapp_xi_area.dt(dydl1, dydl2, face)

            arc[ii] = sqrt(dxdt**2 + dydt**2)


    def normals_tangents(self, key, coordinates, n, t):
        """Retuns the normal and tangent vectors at integration points
        on the boundary.

        Args:

            key (InterpolationKey)[in]

            coordinates (numpy.array)[in] Nodal coordinates of the geometric
                interpolation.

            n (numpy.array)[out] Array for the storage of normal vectors
                at the integration points on the boundary. This array must 
                enter arealdy allocated with the correct size (2, nip).

            t (numpy.array)[out] Array for the storage of tangent vectors
                at the integration points on the boundary. This array must 
                enter arealdy allocated with the correct size (2, nip).

        """

        sf    = self.__call__(key)
        nip   = key.nip
        shape = key.shape
        face  = key.face

        assert nip == n.shape[1]
        assert nip == t.shape[1]

        mapp_xi_area = self.xi_area[shape]

        for ii in range(nip):
            dsf = sf.d_row(ii)

            dxdl1 = np.dot(coordinates[0, :], dsf[0, :])
            dxdl2 = np.dot(coordinates[0, :], dsf[1, :])
            dydl1 = np.dot(coordinates[1, :], dsf[0, :])
            dydl2 = np.dot(coordinates[1, :], dsf[1, :])

            dxdt = mapp_xi_area.dt(dxdl1, dxdl2, face)
            dydt = mapp_xi_area.dt(dydl1, dydl2, face)

            norm = sqrt(dxdt**2 + dydt**2)

            # t[0,ii] = dxdt/norm
            # t[1,ii] = dydt/norm

            t[0,ii],t[1,ii] = mapp_xi_area.correct_tangent(
                dxdt/norm, dydt/norm, face)

            n[0,ii] =  t[1,ii]
            n[1,ii] = -t[0,ii]

# end of manticore.lops.modal_dg.geomfactors.CollapsedToGlobalFaceMaps
