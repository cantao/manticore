#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Executar (no diretorio acima do manticore):
# $ python3 -mtimeit -s 'import manticore.lops.modal_dg.tests.test_speed_detjac as dj' 'dj.f1()'
# e
# $ python3 -mtimeit -s 'import manticore.lops.modal_dg.tests.test_speed_detjac as dj' 'dj.f2()'

import numpy as np
import manticore
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.gauss import JacobiGaussQuadratures
from manticore.geom.standard_geometries import StandardGeometry
from manticore.lops.nodal_cg.interpol import InterpolationTable
from manticore.lops.nodal_cg.interpol import InterpolationKey
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.gauss import interior_nip_rule
from manticore.lops.modal_dg.geomfactors import CollapsedToGlobalMaps

gaussWIP = JacobiGaussQuadratures()

M = 4  # h-order 1d quad
N = 4 * M - 2  # det jacobian order

n1d = interior_nip_rule(GaussPointsType.GaussLegendre, N)
n2d = n1d * n1d

key = InterpolationKey(StandardGeometry.QUAD25, n2d)
geo = CollapsedToGlobalMaps(gaussWIP)
sf = geo(key)
coords = np.array([
    [-1.0, -1.0],  # 0
    [1.0, -1.0],  # 1
    [1.0, 1.0],  # 2
    [-1.0, 1.0],  # 3
    #
    [-0.5, -1.0],  # 4
    [0.0, -1.0],  # 5
    [0.5, -1.0],  # 6
    #
    [1.0, -0.5],  # 7
    [1.0, 0.0],  # 8
    [1.0, 0.5],  # 9
    #
    [0.5, 1.0],  # 10
    [0.0, 1.0],  # 11
    [-0.5, 1.0],  # 12
    #
    [-1.0, 0.5],  # 13
    [-1.0, 0.0],  # 14
    [-1.0, -0.5],  # 15
    #
    [-0.5, -0.5],  # 16
    [0.5, -0.5],  # 17
    [0.5, 0.5],  # 18
    [-0.5, 0.5],  # 19
    #
    [0.0, -0.5],  # 20
    [0.5, 0.0],  # 21
    [0.0, 0.5],  # 22
    [-0.5, 0.0],  # 23
    #
    [0.0, 0.0]
])  # 24

coords = coords.T

detJ = np.zeros(n2d)


def f1():

    for ii in range(n2d):
        #J, detJ[ii] = sf.jacobian_matrix(coords, ii)
        J, detJ[ii] = geo(key).jacobian_matrix(coords, ii)
        J *= geo.xi_area[key.shape].det_jacobian()


def f2():

    #sf.det_jacobian(coords, detJ)
    geo.det_jacobian(key, coords, detJ)
