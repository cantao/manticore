#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_gauss_00.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.gauss import GaussKey, JacobiGaussQuadratures
from manticore.lops.modal_dg.gauss import standard_nip_rule
from manticore.lops.modal_dg.gauss import interior_nip_rule


def main():
    logger = manticore.logging.getLogger('MANTICORE')

    logger.info("Running example JacobiGaussQuadratures 00")

    N = 10

    k1 = GaussKey(GaussPointsType.GaussLegendre, N)
    k2 = GaussKey(GaussPointsType.GaussJacobi10, N)
    k3 = GaussKey(GaussPointsType.NegativeOne, N)
    k4 = GaussKey(GaussPointsType.PositiveOne, N)

    GaussIWP = JacobiGaussQuadratures()

    logger.info("Jacobi-Gauss Quadrature Weights for %s points" % N)

    logger.debug("\nGaussLegendre\n%s\n" % GaussIWP.weights(k1))
    logger.debug("\nGaussJacobi10\n%s\n" % GaussIWP.weights(k2))
    logger.debug("\nNegativeOne\n%s\n" % GaussIWP.weights(k3))
    logger.debug("\nPositiveOne\n%s\n" % GaussIWP.weights(k4))

    logger.info("Example ended!")

    # end of main

# -----------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Manticore example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
