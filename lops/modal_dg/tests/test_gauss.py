#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# LOPS.MODAL_DG tests
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#
# $ python3 -m unittest -v manticore/lops/modal_dg/tests/test_gauss.py

import unittest
import numpy as np
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.gauss import GaussKey, JacobiGaussQuadratures
from manticore.lops.modal_dg.gauss import standard_nip_rule
import manticore.lops.modal_dg.tests.jacobi_gauss_quad as gauss
from manticore.lops.modal_dg.tests.simulation_data import SimulationData
from manticore.lops.modal_dg.tests.simulation_data import QuadratureNodes


class GaussQuadrature(unittest.TestCase):
    gaussWIP = JacobiGaussQuadratures()

    for order in range(21):

        int_order = 2 * (order)

        nip1 = standard_nip_rule(GaussPointsType.GaussLegendre, int_order)

        gaussWIP(GaussKey(GaussPointsType.GaussLegendre, nip1))

        gaussWIP(GaussKey(GaussPointsType.GaussJacobi10, nip1))

    def test_gauss_GL_until_order_20(self):

        for order in range(21):

            int_order = 2 * (order)

            nip = standard_nip_rule(GaussPointsType.GaussLegendre, int_order)

            w, p = GaussQuadrature.gaussWIP(
                GaussKey(GaussPointsType.GaussLegendre, nip))

            sim = SimulationData(
                poly_order=order, node_dist=QuadratureNodes.GL)
            g = gauss.JacobiGaussQuad(sim)

            self.assertTrue(np.allclose(p, g.xi))
            self.assertTrue(np.allclose(w, g.w))

    def test_gauss_GJ10_until_order_20(self):

        for order in range(21):

            int_order = 2 * (order)

            nip = standard_nip_rule(GaussPointsType.GaussLegendre, int_order)

            w, p = GaussQuadrature.gaussWIP(
                GaussKey(GaussPointsType.GaussJacobi10, nip))

            sim = SimulationData(
                poly_order=order, node_dist=QuadratureNodes.GJ10)

            g = gauss.JacobiGaussQuad(sim)

            self.assertTrue(np.allclose(p, g.xi))
            self.assertTrue(np.allclose(w, g.w))


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_gauss.py ------------------------------------------------------------
