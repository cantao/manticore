#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#

import numpy as np
import manticore
from manticore.lops.modal_dg.entity import ExpansionEntityFactory
from manticore.lops.modal_dg.entity import GlobalSequentialIterator
from manticore.lops.modal_dg.computemesh import ComputeMesh
from manticore.geom.standard_geometries import (StandardGeometry,
                                                geometry_name)
from manticore.lops.modal_dg.dgtypes import EntityRole
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions

logger = manticore.logging.getLogger('M_DG_TESTS')


def mesh00():

    logger.info("Generating Simple Mesh 00...")

    nnx = np.arange(0, 3.5, .5)
    nny = np.copy(nnx)
    nnx, nny = np.meshgrid(nnx, nny)
    nx = np.ravel(nnx)
    ny = np.ravel(nny)

    cm = ComputeMesh()
    shfuncs = factory_shape_functions(StandardGeometry.QUAD9)()

    # Physical Subdomains
    subd_id = cm.create_subdomain("FLUID", EntityRole.PHYSICAL)

    # Two different expansions inside the domain (two different
    # ExpansionGroups)
    # 1
    M = 3  # p-order
    N = 2 * M + shfuncs.jacobian_order()  # Number of 1D integration points
    k1, c1 = cm.create_expansion(subd_id, M, N)
    # 2
    M = 4  # p-order
    N = 2 * M + shfuncs.jacobian_order()  # Number of 1D integration points
    k2, c2 = cm.create_expansion(subd_id, M, N)

    # Empty elements: geotype, type, subtype are initialized
    logger.debug("Inserting physical elements...")

    # --------------------------------------------------------------------------
    # Example of how to create and insert an element in mesh:
    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 103  # unique, non sequential integer ID
    e = cm.insert(subd_id, k1, e_quad)  # e is the actual element

    #
    # Up to this point, the following entity's attributes are
    # initialized by the insertion process:
    #
    #       geotype, type, subtype, ID, seqID, role, key
    #

    # Setting incidence and nodal coordinates
    i = np.array([0, 2, 16, 14, 1, 9, 15, 7, 8])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))
    # --------------------------------------------------------------------------

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 250
    e = cm.insert(subd_id, k1, e_quad)  # Group k1
    i = np.array([2, 4, 18, 16, 3, 11, 17, 9, 10])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_tria = ExpansionEntityFactory.make(StandardGeometry.TRI6)
    e_tria.ID = 15
    e = cm.insert(subd_id, k2, e_tria)  # Group k2
    i = np.array([4, 20, 18, 12, 19, 11])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_tria = ExpansionEntityFactory.make(StandardGeometry.TRI6)
    e_tria.ID = 1011
    e = cm.insert(subd_id, k2, e_tria)  # Group k2
    i = np.array([4, 6, 20, 5, 13, 12])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 45
    e = cm.insert(subd_id, k1, e_quad)  # Group k1
    i = np.array([14, 16, 30, 28, 15, 23, 29, 21, 22])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_tria = ExpansionEntityFactory.make(StandardGeometry.TRI6)
    e_tria.ID = 56
    e = cm.insert(subd_id, k1, e_tria)  # Group k1
    i = np.array([16, 18, 30, 17, 24, 23])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_tria = ExpansionEntityFactory.make(StandardGeometry.TRI6)
    e_tria.ID = 70
    e = cm.insert(subd_id, k1, e_tria)  # Group k1
    i = np.array([18, 32, 30, 25, 31, 24])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 22
    e = cm.insert(subd_id, k2, e_quad)  # Group k2
    i = np.array([18, 20, 34, 32, 19, 27, 33, 25, 26])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 1
    e = cm.insert(subd_id, k1, e_quad)  # Group k1
    i = np.array([28, 30, 44, 42, 29, 37, 43, 35, 36])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 2
    e = cm.insert(subd_id, k1, e_quad)  # Group k1
    i = np.array([30, 32, 46, 44, 31, 39, 45, 37, 38])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 30
    e = cm.insert(subd_id, k2, e_quad)  # Group k2
    i = np.array([32, 34, 48, 46, 33, 41, 47, 39, 40])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    # Ghost subdomains
    logger.debug("Inserting ghosts...")

    bc1 = cm.create_subdomain("P_INT", EntityRole.GHOST)
    bc2 = cm.create_subdomain("WALL", EntityRole.GHOST)
    bc3 = cm.create_subdomain("EXIT", EntityRole.GHOST)
    bc4 = cm.create_subdomain("FARFIELD", EntityRole.GHOST)

    # bc1
    # Order have no meaning for ghosts as they do not have polynomial
    # expansions but the number of integration points MUST BE THE SAME
    # to the one of its physical neighbor:
    kgh, cgh = cm.create_expansion(bc1, 0, k1.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2010
    e = cm.insert(bc1, kgh, e_quad)
    # The only relevant attribute for a ghost element is its physical
    # neighbour (Here, the "2" refers to the local face number of the
    # neighbour):
    e.insert_neighbour(0, cm.get_entity(1), 2)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2011
    e = cm.insert(bc1, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(45), 2)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2012
    e = cm.insert(bc1, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(103), 2)

    # bc2
    kgh, cgh = cm.create_expansion(bc2, 0, k1.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2001
    e = cm.insert(bc2, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(103), 0)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2002
    e = cm.insert(bc2, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(250), 0)

    kgh, cgh = cm.create_expansion(bc2, 0, k2.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2003
    e = cm.insert(bc2, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(1011), 0)

    # bc3
    kgh, cgh = cm.create_expansion(bc3, 0, k2.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2004
    e = cm.insert(bc3, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(1011), 2)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2005
    e = cm.insert(bc3, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(22), 3)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2006
    e = cm.insert(bc3, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(30), 3)

    # bc4
    kgh, cgh = cm.create_expansion(bc4, 0, k2.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2007
    e = cm.insert(bc4, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(30), 1)

    kgh, cgh = cm.create_expansion(bc4, 0, k1.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2008
    e = cm.insert(bc4, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(2), 1)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2009
    e = cm.insert(bc4, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(1), 1)

    # Defining neighbourdhoods of internal elements
    logger.debug("Inserting facial neighbourhood...")
    e = cm.get_entity(103)
    e.insert_neighbour(0, cm.get_entity(2001), 0)
    e.insert_neighbour(1, cm.get_entity(45), 0)
    e.insert_neighbour(2, cm.get_entity(2012), 0)
    e.insert_neighbour(3, cm.get_entity(250), 2)

    e = cm.get_entity(250)
    e.insert_neighbour(0, cm.get_entity(2002), 0)
    e.insert_neighbour(1, cm.get_entity(56), 0)
    e.insert_neighbour(2, cm.get_entity(103), 3)
    e.insert_neighbour(3, cm.get_entity(15), 1)

    e = cm.get_entity(15)
    e.insert_neighbour(0, cm.get_entity(1011), 1)
    e.insert_neighbour(1, cm.get_entity(250), 3)
    e.insert_neighbour(2, cm.get_entity(22), 0)

    e = cm.get_entity(1011)
    e.insert_neighbour(0, cm.get_entity(2003), 0)
    e.insert_neighbour(1, cm.get_entity(15), 0)
    e.insert_neighbour(2, cm.get_entity(2004), 0)

    e = cm.get_entity(45)
    e.insert_neighbour(0, cm.get_entity(103), 1)
    e.insert_neighbour(1, cm.get_entity(1), 0)
    e.insert_neighbour(2, cm.get_entity(2011), 0)
    e.insert_neighbour(3, cm.get_entity(56), 1)

    e = cm.get_entity(56)
    e.insert_neighbour(0, cm.get_entity(250), 1)
    e.insert_neighbour(1, cm.get_entity(45), 3)
    e.insert_neighbour(2, cm.get_entity(70), 1)

    e = cm.get_entity(70)
    e.insert_neighbour(0, cm.get_entity(22), 2)
    e.insert_neighbour(1, cm.get_entity(56), 2)
    e.insert_neighbour(2, cm.get_entity(2), 0)

    e = cm.get_entity(22)
    e.insert_neighbour(0, cm.get_entity(15), 2)
    e.insert_neighbour(1, cm.get_entity(30), 0)
    e.insert_neighbour(2, cm.get_entity(70), 0)
    e.insert_neighbour(3, cm.get_entity(2005), 0)

    e = cm.get_entity(1)
    e.insert_neighbour(0, cm.get_entity(45), 1)
    e.insert_neighbour(1, cm.get_entity(2009), 0)
    e.insert_neighbour(2, cm.get_entity(2010), 0)
    e.insert_neighbour(3, cm.get_entity(2), 2)

    e = cm.get_entity(2)
    e.insert_neighbour(0, cm.get_entity(70), 2)
    e.insert_neighbour(1, cm.get_entity(2008), 0)
    e.insert_neighbour(2, cm.get_entity(1), 3)
    e.insert_neighbour(3, cm.get_entity(30), 2)

    e = cm.get_entity(30)
    e.insert_neighbour(0, cm.get_entity(22), 1)
    e.insert_neighbour(1, cm.get_entity(2007), 0)
    e.insert_neighbour(2, cm.get_entity(2), 3)
    e.insert_neighbour(3, cm.get_entity(2006), 0)

    # Compute the number of "communication integration points" of each
    # face of physical elements.
    logger.debug("Computing face communication sizes...")
    elist = GlobalSequentialIterator(cm.container)
    for e in elist:
        e.init_face_comm_sizes()

    logger.info("Simple Mesh 00 was generated!")

    return cm


# end of manticore.lops.modal_dg.tests.simple_meshes.mesh00


def mesh01():

    logger.info("Generating Simple Mesh 01...")

    nnx = np.arange(0, 3.5, .5)
    nny = np.copy(nnx)
    nnx, nny = np.meshgrid(nnx, nny)
    nx = np.ravel(nnx)
    ny = np.ravel(nny)

    cm = ComputeMesh()
    shfsq = factory_shape_functions(StandardGeometry.QUAD9)()
    shfst = factory_shape_functions(StandardGeometry.TRI6)()

    # Physical Subdomains
    subd_id = cm.create_subdomain("FLUID", EntityRole.PHYSICAL)

    # Four different expansions inside the domain (four ExpansionGroups)
    # 1
    M = 3  # p-order
    N = 2 * M + shfsq.jacobian_order()  # Number of 1D integration points
    k1q, c1q = cm.create_expansion(subd_id, M, N)

    # 2
    M = 3  # p-order
    N = 2 * M + shfst.jacobian_order()  # Number of 1D integration points
    k1t, c1t = cm.create_expansion(subd_id, M, N)

    # 3
    M = 4  # p-order
    N = 2 * M + shfsq.jacobian_order()  # Number of 1D integration points
    k2q, c2q = cm.create_expansion(subd_id, M, N)

    # 4
    M = 4  # p-order
    N = 2 * M + shfst.jacobian_order()  # Number of 1D integration points
    k2t, c2t = cm.create_expansion(subd_id, M, N)

    # Empty elements: geotype, type, subtype are initialized
    logger.debug("Inserting physical elements...")

    # --------------------------------------------------------------------------
    # Example of how to create and insert an element in mesh:
    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 103  # unique, non sequential integer ID
    e = cm.insert(subd_id, k1q, e_quad)  # e is the actual element

    #
    # Up to this point, the following entity's attributes are
    # initialized by the insertion process:
    #
    #       geotype, type, subtype, ID, seqID, role, key
    #

    # Setting incidence and nodal coordinates
    i = np.array([0, 2, 16, 14, 1, 9, 15, 7, 8])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))
    # --------------------------------------------------------------------------

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 250
    e = cm.insert(subd_id, k1q, e_quad)  # Group k1
    i = np.array([2, 4, 18, 16, 3, 11, 17, 9, 10])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_tria = ExpansionEntityFactory.make(StandardGeometry.TRI6)
    e_tria.ID = 15
    e = cm.insert(subd_id, k2t, e_tria)  # Group k2
    i = np.array([4, 20, 18, 12, 19, 11])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_tria = ExpansionEntityFactory.make(StandardGeometry.TRI6)
    e_tria.ID = 1011
    e = cm.insert(subd_id, k2t, e_tria)  # Group k2
    i = np.array([4, 6, 20, 5, 13, 12])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 45
    e = cm.insert(subd_id, k1q, e_quad)  # Group k1
    i = np.array([14, 16, 30, 28, 15, 23, 29, 21, 22])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_tria = ExpansionEntityFactory.make(StandardGeometry.TRI6)
    e_tria.ID = 56
    e = cm.insert(subd_id, k1t, e_tria)  # Group k1
    i = np.array([16, 18, 30, 17, 24, 23])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_tria = ExpansionEntityFactory.make(StandardGeometry.TRI6)
    e_tria.ID = 70
    e = cm.insert(subd_id, k1t, e_tria)  # Group k1
    i = np.array([18, 32, 30, 25, 31, 24])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 22
    e = cm.insert(subd_id, k2q, e_quad)  # Group k2
    i = np.array([18, 20, 34, 32, 19, 27, 33, 25, 26])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 1
    e = cm.insert(subd_id, k1q, e_quad)  # Group k1
    i = np.array([28, 30, 44, 42, 29, 37, 43, 35, 36])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 2
    e = cm.insert(subd_id, k1q, e_quad)  # Group k1
    i = np.array([30, 32, 46, 44, 31, 39, 45, 37, 38])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD9)
    e_quad.ID = 30
    e = cm.insert(subd_id, k2q, e_quad)  # Group k2
    i = np.array([32, 34, 48, 46, 33, 41, 47, 39, 40])
    e.set_vertices(i)
    e.set_coeff(np.array([nx[i], ny[i]]))

    # Ghost subdomains
    logger.debug("Inserting ghosts...")

    bc1 = cm.create_subdomain("P_INT", EntityRole.GHOST)
    bc2 = cm.create_subdomain("WALL", EntityRole.GHOST)
    bc3 = cm.create_subdomain("EXIT", EntityRole.GHOST)
    bc4 = cm.create_subdomain("FARFIELD", EntityRole.GHOST)

    # bc1
    # Order have no meaning for ghosts as they do not have polynomial
    # expansions but the number of integration points MUST BE THE SAME
    # to the one of its physical neighbor:
    kgh, cgh = cm.create_expansion(bc1, 0, k1q.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2010
    e = cm.insert(bc1, kgh, e_quad)
    # The only relevant attribute for a ghost element is its physical
    # neighbour:
    e.insert_neighbour(0, cm.get_entity(1), 2)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2011
    e = cm.insert(bc1, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(45), 2)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2012
    e = cm.insert(bc1, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(103), 2)

    # bc2
    kgh, cgh = cm.create_expansion(bc2, 0, k1q.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2001
    e = cm.insert(bc2, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(103), 0)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2002
    e = cm.insert(bc2, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(250), 0)

    kgh, cgh = cm.create_expansion(bc2, 0, k2t.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2003
    e = cm.insert(bc2, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(1011), 0)

    # bc3
    kgh, cgh = cm.create_expansion(bc3, 0, k2t.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2004
    e = cm.insert(bc3, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(1011), 2)

    kgh, cgh = cm.create_expansion(bc3, 0, k2q.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2005
    e = cm.insert(bc3, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(22), 3)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2006
    e = cm.insert(bc3, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(30), 3)

    # bc4
    kgh, cgh = cm.create_expansion(bc4, 0, k2q.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2007
    e = cm.insert(bc4, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(30), 1)

    kgh, cgh = cm.create_expansion(bc4, 0, k1q.n1d)  # <--

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2008
    e = cm.insert(bc4, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(2), 1)

    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 2009
    e = cm.insert(bc4, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(1), 1)

    # Defining neighbourdhoods of internal elements
    logger.debug("Inserting facial neighbourhood...")
    e = cm.get_entity(103)
    e.insert_neighbour(0, cm.get_entity(2001), 0)
    e.insert_neighbour(1, cm.get_entity(45), 0)
    e.insert_neighbour(2, cm.get_entity(2012), 0)
    e.insert_neighbour(3, cm.get_entity(250), 2)

    e = cm.get_entity(250)
    e.insert_neighbour(0, cm.get_entity(2002), 0)
    e.insert_neighbour(1, cm.get_entity(56), 0)
    e.insert_neighbour(2, cm.get_entity(103), 3)
    e.insert_neighbour(3, cm.get_entity(15), 1)

    e = cm.get_entity(15)
    e.insert_neighbour(0, cm.get_entity(1011), 1)
    e.insert_neighbour(1, cm.get_entity(250), 3)
    e.insert_neighbour(2, cm.get_entity(22), 0)

    e = cm.get_entity(1011)
    e.insert_neighbour(0, cm.get_entity(2003), 0)
    e.insert_neighbour(1, cm.get_entity(15), 0)
    e.insert_neighbour(2, cm.get_entity(2004), 0)

    e = cm.get_entity(45)
    e.insert_neighbour(0, cm.get_entity(103), 1)
    e.insert_neighbour(1, cm.get_entity(1), 0)
    e.insert_neighbour(2, cm.get_entity(2011), 0)
    e.insert_neighbour(3, cm.get_entity(56), 1)

    e = cm.get_entity(56)
    e.insert_neighbour(0, cm.get_entity(250), 1)
    e.insert_neighbour(1, cm.get_entity(45), 3)
    e.insert_neighbour(2, cm.get_entity(70), 1)

    e = cm.get_entity(70)
    e.insert_neighbour(0, cm.get_entity(22), 2)
    e.insert_neighbour(1, cm.get_entity(56), 2)
    e.insert_neighbour(2, cm.get_entity(2), 0)

    e = cm.get_entity(22)
    e.insert_neighbour(0, cm.get_entity(15), 2)
    e.insert_neighbour(1, cm.get_entity(30), 0)
    e.insert_neighbour(2, cm.get_entity(70), 0)
    e.insert_neighbour(3, cm.get_entity(2005), 0)

    e = cm.get_entity(1)
    e.insert_neighbour(0, cm.get_entity(45), 1)
    e.insert_neighbour(1, cm.get_entity(2009), 0)
    e.insert_neighbour(2, cm.get_entity(2010), 0)
    e.insert_neighbour(3, cm.get_entity(2), 2)

    e = cm.get_entity(2)
    e.insert_neighbour(0, cm.get_entity(70), 2)
    e.insert_neighbour(1, cm.get_entity(2008), 0)
    e.insert_neighbour(2, cm.get_entity(1), 3)
    e.insert_neighbour(3, cm.get_entity(30), 2)

    e = cm.get_entity(30)
    e.insert_neighbour(0, cm.get_entity(22), 1)
    e.insert_neighbour(1, cm.get_entity(2007), 0)
    e.insert_neighbour(2, cm.get_entity(2), 3)
    e.insert_neighbour(3, cm.get_entity(2006), 0)

    # Compute the number of "communication integration points" of each
    # face of physical elements.
    logger.debug("Computing face communication sizes...")
    elist = GlobalSequentialIterator(cm.container)
    for e in elist:
        e.init_face_comm_sizes()

    logger.info("Simple Mesh 01 was generated!")

    return cm


# end of manticore.lops.modal_dg.tests.simple_meshes.mesh01


def mesh02(l=10.0, n=8, p_order=1):
    """Generates a mesh of n**2 four-sided elements over a square domain

    [0,l]X[-l/2,l/2] with p-order and analytical Dirichlet boundary
    conditions.
    """

    geom_type = StandardGeometry.QUAD4
    name = geometry_name(geom_type)

    logger.info("Generating Simple Mesh 02...")
    logger.info("%s elements, p-order = %s, geom = %s" % (n**2, p_order, name))
    dl = l / n

    nnx = np.arange(0., l + dl, dl)
    nny = np.arange(-l / 2., l / 2. + dl, dl)
    nnx, nny = np.meshgrid(nnx, nny)
    nx = np.ravel(nnx)
    ny = np.ravel(nny)

    cm = ComputeMesh()
    shfsq = factory_shape_functions(geom_type)()

    # Physical Subdomains
    subd_id = cm.create_subdomain("FLUID", EntityRole.PHYSICAL)

    # Four different expansions inside the domain (four ExpansionGroups)
    # 1
    M = p_order  # p-order
    N = 2 * M + shfsq.jacobian_order()  # Number of 1D integration points
    k1q, c1q = cm.create_expansion(subd_id, M, N)

    logger.debug("Inserting physical elements...")
    ID = 0
    for j in range(n):
        for i in range(n):
            jnp1 = j * (n + 1)
            e_quad = ExpansionEntityFactory.make(geom_type)
            e_quad.ID = ID
            e = cm.insert(subd_id, k1q, e_quad)
            incid = np.array(
                [i + jnp1, i + jnp1 + 1, i + jnp1 + n + 2, i + jnp1 + n + 1])
            e.set_vertices(incid)
            e.set_coeff(np.array([nx[incid], ny[incid]]))
            ID += 1

    # Ghost subdomains
    logger.debug("Inserting ghosts...")

    bc0 = cm.create_subdomain("FACE_0", EntityRole.GHOST)
    bc1 = cm.create_subdomain("FACE_1", EntityRole.GHOST)
    bc2 = cm.create_subdomain("FACE_2", EntityRole.GHOST)
    bc3 = cm.create_subdomain("FACE_3", EntityRole.GHOST)

    # bc0 - lower boundary
    #
    # Order have no meaning for ghosts as they do not have polynomial
    # expansions but the number of integration points MUST BE THE SAME
    # to the one of its physical neighbor:
    kgh, cgh = cm.create_expansion(bc0, 0, k1q.n1d)  # <--

    bc0_ids = []
    for i in range(n):
        e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
        e_quad.ID = ID
        e = cm.insert(bc0, kgh, e_quad)
        # The only relevant attribute for a ghost element is its physical
        # neighbour:
        e.insert_neighbour(0, cm.get_entity(i), 0)
        bc0_ids.append(ID)
        ID += 1

    # bc1 - upper boundary
    kgh, cgh = cm.create_expansion(bc1, 0, k1q.n1d)  # <--

    bc1_ids = []
    elems = np.arange((n - 1) * n, n * n, 1)  # list of physical neighbors
    for i in elems:
        e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
        e_quad.ID = ID
        e = cm.insert(bc1, kgh, e_quad)
        # The only relevant attribute for a ghost element is its physical
        # neighbour:
        e.insert_neighbour(0, cm.get_entity(i), 1)
        bc1_ids.append(ID)
        ID += 1

    # bc2 - left boundary
    kgh, cgh = cm.create_expansion(bc2, 0, k1q.n1d)  # <--

    bc2_ids = []
    elems = np.arange(0, n * n, n)  # list of physical neighbors
    for i in elems:
        e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
        e_quad.ID = ID
        e = cm.insert(bc2, kgh, e_quad)
        # The only relevant attribute for a ghost element is its physical
        # neighbour:
        e.insert_neighbour(0, cm.get_entity(i), 2)
        bc2_ids.append(ID)
        ID += 1

    # bc3 - right boundary
    kgh, cgh = cm.create_expansion(bc3, 0, k1q.n1d)  # <--

    bc3_ids = []
    elems = np.arange(n - 1, (n + 2) * (n - 1),
                      n)  # list of physical neighbors
    for i in elems:
        e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
        e_quad.ID = ID
        e = cm.insert(bc3, kgh, e_quad)
        # The only relevant attribute for a ghost element is its physical
        # neighbour:
        e.insert_neighbour(0, cm.get_entity(i), 3)
        bc3_ids.append(ID)
        ID += 1

    # Defining neighbourdhoods of internal elements
    logger.debug("Inserting facial neighbourhood...")

    # left lower corner
    e = cm.get_entity(0)
    e.insert_neighbour(0, cm.get_entity(bc0_ids[0]), 0)
    e.insert_neighbour(1, cm.get_entity(n), 0)
    e.insert_neighbour(2, cm.get_entity(bc2_ids[0]), 0)
    e.insert_neighbour(3, cm.get_entity(1), 2)

    # right lower corner
    e = cm.get_entity(n - 1)
    e.insert_neighbour(0, cm.get_entity(bc0_ids[-1]), 0)
    e.insert_neighbour(1, cm.get_entity(2 * n - 1), 0)
    e.insert_neighbour(2, cm.get_entity(n - 2), 3)
    e.insert_neighbour(3, cm.get_entity(bc3_ids[0]), 0)

    # left upper corner
    e = cm.get_entity((n - 1) * n)
    e.insert_neighbour(0, cm.get_entity((n - 2) * n), 1)
    e.insert_neighbour(1, cm.get_entity(bc1_ids[0]), 0)
    e.insert_neighbour(2, cm.get_entity(bc2_ids[-1]), 0)
    e.insert_neighbour(3, cm.get_entity((n - 1) * n + 1), 2)

    # right upper corner
    e = cm.get_entity(n * n - 1)
    e.insert_neighbour(0, cm.get_entity((n + 1) * (n - 1) - n), 1)
    e.insert_neighbour(1, cm.get_entity(bc1_ids[-1]), 0)
    e.insert_neighbour(2, cm.get_entity(n * n - 2), 3)
    e.insert_neighbour(3, cm.get_entity(bc3_ids[-1]), 0)

    # lower internal band
    elems = np.arange(1, (n - 1), 1)
    for i in elems:
        e = cm.get_entity(i)
        e.insert_neighbour(0, cm.get_entity(bc0_ids[i]), 0)
        e.insert_neighbour(1, cm.get_entity(i + n), 0)
        e.insert_neighbour(2, cm.get_entity(i - 1), 3)
        e.insert_neighbour(3, cm.get_entity(i + 1), 2)

    # upper internal band
    elems = np.arange((n - 1) * n + 1, n * n - 1, 1)
    for j, i in enumerate(elems):
        e = cm.get_entity(i)
        e.insert_neighbour(0, cm.get_entity(i - n), 1)
        e.insert_neighbour(1, cm.get_entity(bc1_ids[j + 1]), 0)
        e.insert_neighbour(2, cm.get_entity(i - 1), 3)
        e.insert_neighbour(3, cm.get_entity(i + 1), 2)

    # left internal band
    elems = np.arange(n, (n - 1) * n, n)
    for j, i in enumerate(elems):
        e = cm.get_entity(i)
        e.insert_neighbour(0, cm.get_entity(i - n), 1)
        e.insert_neighbour(1, cm.get_entity(i + n), 0)
        e.insert_neighbour(2, cm.get_entity(bc2_ids[j + 1]), 0)
        e.insert_neighbour(3, cm.get_entity(i + 1), 2)

    # right internal band
    elems = np.arange(2 * n - 1, n * n - 1, n)
    for j, i in enumerate(elems):
        e = cm.get_entity(i)
        e.insert_neighbour(0, cm.get_entity(i - n), 1)
        e.insert_neighbour(1, cm.get_entity(i + n), 0)
        e.insert_neighbour(2, cm.get_entity(i - 1), 3)
        e.insert_neighbour(3, cm.get_entity(bc3_ids[j + 1]), 0)

    # internal core
    for j in range(n - 2):
        elems = np.arange(n + 1, 2 * n - 1, 1)
        elems += j * n
        for i in elems:
            e = cm.get_entity(i)
            e.insert_neighbour(0, cm.get_entity(i - n), 1)
            e.insert_neighbour(1, cm.get_entity(i + n), 0)
            e.insert_neighbour(2, cm.get_entity(i - 1), 3)
            e.insert_neighbour(3, cm.get_entity(i + 1), 2)

    # Compute the number of "communication integration points" of each
    # face of physical elements.
    logger.debug("Computing face communication sizes...")
    elist = GlobalSequentialIterator(cm.container)
    for e in elist:
        e.init_face_comm_sizes()

    logger.info("Simple Mesh 02 was generated!")

    return cm


# end of manticore.lops.modal_dg.tests.simple_meshes.mesh02


def mesh03(l=10.0, n=8, p_order=1):
    """Generates a mesh of 2*n**2 three-sided elements over a square domain

    [0,l]X[-l/2,l/2] with p-order and analytical Dirichlet boundary
    conditions.
    """

    geom_type = StandardGeometry.TRI3
    name = geometry_name(geom_type)

    logger.info("Generating Simple Mesh 03...")
    logger.info("%s elements, p-order = %s, geom = %s" % (2 * n**2, p_order,
                                                          name))
    dl = l / n

    nnx = np.arange(0., l + dl, dl)
    nny = np.arange(-l / 2., l / 2. + dl, dl)
    nnx, nny = np.meshgrid(nnx, nny)
    nx = np.ravel(nnx)
    ny = np.ravel(nny)

    cm = ComputeMesh()
    shfsq = factory_shape_functions(geom_type)()

    # Physical Subdomains
    subd_id = cm.create_subdomain("FLUID", EntityRole.PHYSICAL)

    M = p_order  # p-order
    N = 2 * M + shfsq.jacobian_order()  # Number of 1D integration points
    k1q, c1q = cm.create_expansion(subd_id, M, N)

    logger.debug("Inserting physical elements...")
    ID = 0
    for j in range(n):
        for i in range(n):
            jnp1 = j * (n + 1)
            e_tria = ExpansionEntityFactory.make(geom_type)
            e_tria.ID = ID
            e = cm.insert(subd_id, k1q, e_tria)
            incid = np.array([i + jnp1, i + jnp1 + 1, i + jnp1 + n + 1])
            e.set_vertices(incid)
            e.set_coeff(np.array([nx[incid], ny[incid]]))
            ID += 1
            #
            e_tria = ExpansionEntityFactory.make(geom_type)
            e_tria.ID = ID
            e = cm.insert(subd_id, k1q, e_tria)
            incid = np.array(
                [i + jnp1 + n + 2, i + jnp1 + n + 1, i + jnp1 + 1])
            e.set_vertices(incid)
            e.set_coeff(np.array([nx[incid], ny[incid]]))
            ID += 1

    # Ghost subdomains
    logger.debug("Inserting ghosts...")

    bc0 = cm.create_subdomain("FACE_0", EntityRole.GHOST)
    bc1 = cm.create_subdomain("FACE_1", EntityRole.GHOST)
    bc2 = cm.create_subdomain("FACE_2", EntityRole.GHOST)
    bc3 = cm.create_subdomain("FACE_3", EntityRole.GHOST)

    # bc0 - lower boundary
    #
    # Order have no meaning for ghosts as they do not have polynomial
    # expansions but the number of integration points MUST BE THE SAME
    # to the one of its physical neighbor:
    kgh, cgh = cm.create_expansion(bc0, 0, k1q.n1d)  # <--

    bc0_ids = []
    elems = np.arange(0, 2 * n, 2)
    for i in elems:
        e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
        e_quad.ID = ID
        e = cm.insert(bc0, kgh, e_quad)
        # The only relevant attribute for a ghost element is its physical
        # neighbour:
        e.insert_neighbour(0, cm.get_entity(i), 0)
        bc0_ids.append(ID)
        ID += 1

    # bc1 - upper boundary
    kgh, cgh = cm.create_expansion(bc1, 0, k1q.n1d)  # <--

    bc1_ids = []
    elems = np.arange(2 * (n - 1) * n + 1, 2 * n * n + 1,
                      2)  # list of physical neighbors
    for i in elems:
        e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
        e_quad.ID = ID
        e = cm.insert(bc1, kgh, e_quad)
        # The only relevant attribute for a ghost element is its physical
        # neighbour:
        e.insert_neighbour(0, cm.get_entity(i), 0)
        bc1_ids.append(ID)
        ID += 1

    # bc2 - left boundary
    kgh, cgh = cm.create_expansion(bc2, 0, k1q.n1d)  # <--

    bc2_ids = []
    elems = np.arange(0, 2 * n * n, 2 * n)  # list of physical neighbors
    for i in elems:
        e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
        e_quad.ID = ID
        e = cm.insert(bc2, kgh, e_quad)
        # The only relevant attribute for a ghost element is its physical
        # neighbour:
        e.insert_neighbour(0, cm.get_entity(i), 1)
        bc2_ids.append(ID)
        ID += 1

    # bc3 - right boundary
    kgh, cgh = cm.create_expansion(bc3, 0, k1q.n1d)  # <--

    bc3_ids = []
    elems = np.arange(2 * n - 1, 2 * n * n + 1,
                      2 * n)  # list of physical neighbors
    for i in elems:
        e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
        e_quad.ID = ID
        e = cm.insert(bc3, kgh, e_quad)
        # The only relevant attribute for a ghost element is its physical
        # neighbour:
        e.insert_neighbour(0, cm.get_entity(i), 1)
        bc3_ids.append(ID)
        ID += 1

    # Defining neighbourdhoods of internal elements
    logger.debug("Inserting facial neighbourhood...")

    # left lower corner
    e = cm.get_entity(0)
    e.insert_neighbour(0, cm.get_entity(bc0_ids[0]), 0)
    e.insert_neighbour(1, cm.get_entity(bc2_ids[0]), 0)
    e.insert_neighbour(2, cm.get_entity(1), 2)
    #
    e = cm.get_entity(1)
    e.insert_neighbour(0, cm.get_entity(2 * n), 0)
    e.insert_neighbour(1, cm.get_entity(2), 1)
    e.insert_neighbour(2, cm.get_entity(0), 2)

    # right lower corner
    e = cm.get_entity(2 * n - 2)
    e.insert_neighbour(0, cm.get_entity(bc0_ids[-1]), 0)
    e.insert_neighbour(1, cm.get_entity(2 * n - 3), 1)
    e.insert_neighbour(2, cm.get_entity(2 * n - 1), 2)
    #
    e = cm.get_entity(2 * n - 1)
    e.insert_neighbour(0, cm.get_entity(4 * n - 2), 0)
    e.insert_neighbour(1, cm.get_entity(bc3_ids[0]), 0)
    e.insert_neighbour(2, cm.get_entity(2 * n - 2), 2)

    # left upper corner
    e = cm.get_entity((n - 1) * (2 * n))
    e.insert_neighbour(0, cm.get_entity((n - 2) * 2 * n + 1), 0)
    e.insert_neighbour(1, cm.get_entity(bc2_ids[-1]), 0)
    e.insert_neighbour(2, cm.get_entity((n - 1) * 2 * n + 1), 2)
    #
    e = cm.get_entity((n - 1) * (2 * n) + 1)
    e.insert_neighbour(0, cm.get_entity(bc1_ids[0]), 0)
    e.insert_neighbour(1, cm.get_entity((n - 1) * 2 * n + 2), 1)
    e.insert_neighbour(2, cm.get_entity((n - 1) * (2 * n)), 2)

    # right upper corner
    e = cm.get_entity(n * 2 * n - 2)
    e.insert_neighbour(0, cm.get_entity((n - 1) * (2 * n) - 1), 0)
    e.insert_neighbour(1, cm.get_entity(n * 2 * n - 3), 1)
    e.insert_neighbour(2, cm.get_entity(n * 2 * n - 1), 2)
    #
    e = cm.get_entity(n * 2 * n - 1)
    e.insert_neighbour(0, cm.get_entity(bc1_ids[-1]), 0)
    e.insert_neighbour(1, cm.get_entity(bc3_ids[-1]), 0)
    e.insert_neighbour(2, cm.get_entity(n * 2 * n - 2), 2)

    # lower internal band
    elems = np.arange(2, (2 * n - 2), 2)
    for j, i in enumerate(elems):
        e = cm.get_entity(i)
        e.insert_neighbour(0, cm.get_entity(bc0_ids[j + 1]), 0)
        e.insert_neighbour(1, cm.get_entity(i - 1), 1)
        e.insert_neighbour(2, cm.get_entity(i + 1), 2)
        #
        e = cm.get_entity(i + 1)
        e.insert_neighbour(0, cm.get_entity(i + 2 * n), 0)
        e.insert_neighbour(1, cm.get_entity(i + 2), 1)
        e.insert_neighbour(2, cm.get_entity(i), 2)

    # upper internal band
    elems = np.arange((n - 1) * (2 * n) + 2, n * 2 * n - 2, 2)
    for j, i in enumerate(elems):
        e = cm.get_entity(i)
        e.insert_neighbour(0, cm.get_entity(i - 2 * n + 1), 0)
        e.insert_neighbour(1, cm.get_entity(i - 1), 1)
        e.insert_neighbour(2, cm.get_entity(i + 1), 2)
        #
        e = cm.get_entity(i + 1)
        e.insert_neighbour(0, cm.get_entity(bc1_ids[j + 1]), 0)
        e.insert_neighbour(1, cm.get_entity(i + 2), 1)
        e.insert_neighbour(2, cm.get_entity(i), 2)

    # left internal band
    elems = np.arange(2 * n, (n - 1) * (2 * n), 2 * n)
    for j, i in enumerate(elems):
        e = cm.get_entity(i)
        e.insert_neighbour(0, cm.get_entity(i - 2 * n + 1), 0)
        e.insert_neighbour(1, cm.get_entity(bc2_ids[j + 1]), 0)
        e.insert_neighbour(2, cm.get_entity(i + 1), 2)
        #
        e = cm.get_entity(i + 1)
        e.insert_neighbour(0, cm.get_entity(i + 2 * n), 0)
        e.insert_neighbour(1, cm.get_entity(i + 2), 1)
        e.insert_neighbour(2, cm.get_entity(i), 2)

    # right internal band
    elems = np.arange(4 * n - 2, (n - 1) * (2 * n), 2 * n)
    for j, i in enumerate(elems):
        e = cm.get_entity(i)
        e.insert_neighbour(0, cm.get_entity(i - 2 * n + 1), 0)
        e.insert_neighbour(1, cm.get_entity(i - 1), 1)
        e.insert_neighbour(2, cm.get_entity(i + 1), 2)
        #
        e = cm.get_entity(i + 1)
        e.insert_neighbour(0, cm.get_entity(i + 2 * n), 0)
        e.insert_neighbour(1, cm.get_entity(bc3_ids[j + 1]), 0)
        e.insert_neighbour(2, cm.get_entity(i), 2)

    # internal core
    for j in range(n - 2):
        elems = np.arange(2 * n + 2, 4 * n - 2, 2)
        elems += j * 2 * n
        for i in elems:
            e = cm.get_entity(i)
            e.insert_neighbour(0, cm.get_entity(i - 2 * n + 1), 0)
            e.insert_neighbour(1, cm.get_entity(i - 1), 1)
            e.insert_neighbour(2, cm.get_entity(i + 1), 2)
            #
            e = cm.get_entity(i + 1)
            e.insert_neighbour(0, cm.get_entity(i + 2 * n), 0)
            e.insert_neighbour(1, cm.get_entity(i + 2), 1)
            e.insert_neighbour(2, cm.get_entity(i), 2)

    # Compute the number of "communication integration points" of each
    # face of physical elements.
    logger.debug("Computing face communication sizes...")
    elist = GlobalSequentialIterator(cm.container)
    for e in elist:
        e.init_face_comm_sizes()

    logger.info("Simple Mesh 03 was generated!")

    return cm
