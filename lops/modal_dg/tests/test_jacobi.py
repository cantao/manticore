#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# LOPS.MODAL_DG tests
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#
# $ python3 -m unittest -v manticore/lops/modal_dg/tests/test_jacobi.py

import unittest
import manticore
import numpy as np
from manticore.lops.modal_dg.tests.jacobi_p import jacobi_p
from manticore.lops.modal_dg.tests.djacobi_p import djacobi_p
from manticore.lops.modal_dg.tests.jacobi_roots import jacobi_roots
from manticore.lops.modal_dg.jacobi import JacobiPolynomial


class Jacobi(unittest.TestCase):
    def test_jacobi00_until_order_20(self):
        for ii in range(20):
            order = ii + 1
            x = np.linspace(-1, 1, 40)

            J_ = jacobi_p(x, order)
            DJ_ = djacobi_p(x, order)

            r1_ = jacobi_roots(order)
            r2_ = jacobi_roots(order + 2)

            jp = JacobiPolynomial(order)

            J = jp.eval(x)
            DJ = jp.deval(x)

            r1 = jp.roots()
            jp.configure(order + 2, jp.alpha, jp.beta)
            r2 = jp.roots()

            self.assertTrue(np.allclose(J, J_))
            self.assertTrue(np.allclose(DJ, DJ_))
            self.assertTrue(np.allclose(r1, r1_))
            self.assertTrue(np.allclose(r2, r2_))

    def test_jacobi10_until_order_20(self):
        for ii in range(20):
            order = ii + 1
            x = np.linspace(-1, 1, 40)

            J_ = jacobi_p(x, order, alpha=1.0)
            DJ_ = djacobi_p(x, order, alpha=1.0)

            r1_ = jacobi_roots(order, alpha=1.0)
            r2_ = jacobi_roots(order + 2, alpha=1.0)

            jp = JacobiPolynomial(order, alpha=1.0)

            J = jp.eval(x)
            DJ = jp.deval(x)

            r1 = jp.roots()
            jp.configure(order + 2, jp.alpha, jp.beta)
            r2 = jp.roots()

            self.assertTrue(np.allclose(J, J_))
            self.assertTrue(np.allclose(DJ, DJ_))
            self.assertTrue(np.allclose(r1, r1_))
            self.assertTrue(np.allclose(r2, r2_))
