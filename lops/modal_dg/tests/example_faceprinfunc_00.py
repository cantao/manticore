#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_faceprinfunc_00.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.dgtypes import FacePrincipalFunctionType
from manticore.lops.modal_dg.gauss import JacobiGaussQuadratures
from manticore.lops.modal_dg.faceprinfunc import FacePrincipalFunctionKey
from manticore.lops.modal_dg.faceprinfunc import FacePrincipalFunctions


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example FacePrincipalFunctions 00")

    k = FacePrincipalFunctionKey.get_instance(
        StdRegionType.DG_Quadrangle,
        0,
        StdRegionType.DG_Quadrangle,
        1,
        order=4,
        nip=5)

    gaussWIP = JacobiGaussQuadratures()
    FPFuncs = FacePrincipalFunctions(gaussWIP)

    logger.debug("face_key = %s" % (k))

    logger.debug("\n%s" % (FPFuncs(k)))

    logger.info("Example ended!")

    # end of main

# ------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
