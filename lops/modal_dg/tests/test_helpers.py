#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np


class Ones:
    def F(x, y):
        return np.ones(x.shape[0])


class TrigPoly1:
    def F(x, y):
        # P = x*sin(y)
        P = np.sin(y)
        P *= x
        return P

    def Fx(x, y):  # dF/dx = sin(y)
        P = np.sin(y)
        return P

    def Fy(x, y):  # dF/dy = x*cos(y)
        P = np.cos(y)
        P *= x
        return P

    # Divergent of [Fx Fy]^T
    def Fdiv(x, y):
        P = -np.sin(y)
        P *= x
        return P


class TrigPoly2:
    def F(x, y):
        # P = sin(x)*y*y
        P = np.sin(x)
        P *= np.power(y, 2)
        return P

    def Fx(x, y):  # dF/dx  = cos(x)*y*y
        P = np.cos(x)
        P *= np.power(y, 2)
        return P

    def Fy(x, y):  # dF/dy = 2*y*sin(x)
        P = np.sin(x)
        P *= 2. * y
        return P

    # Divergent of [Fx Fy]^T
    def Fdiv(x, y):
        Px = -np.sin(x)
        Px *= np.power(y, 2)

        Py = np.sin(x)
        Py *= 2.
        return Px + Py


class Polynomial:
    def __init__(self, i, j):
        self.i = i
        self.j = j

    def __call__(self, x, y):
        return np.power(x, self.i) * np.power(y, self.j)


class Polynomial_Dx:
    def __init__(self, i, j):
        self.i = i
        self.j = j

    def __call__(self, x, y):
        if self.i == 0:
            return 0.0
        return self.i * np.power(x, self.i - 1.) * np.power(y, self.j)


class Polynomial_Dy:
    def __init__(self, i, j):
        self.i = i
        self.j = j

    def __call__(self, x, y):
        if self.j == 0:
            return 0.0
        return self.j * np.power(x, self.i) * np.power(y, self.j - 1.)


class Polynomial_Div:
    def __init__(self, i, j):
        self.i = i
        self.j = j

    def __call__(self, x, y):

        pdx = Polynomial_Dx(self.i, self.j)
        pdy = Polynomial_Dy(self.i, self.j)

        return pdx(x, y) + pdy(x, y)
