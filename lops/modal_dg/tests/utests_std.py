#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.stdops2d import factory_stdbackward2d
from manticore.lops.modal_dg.stdops2d import factory_stdforward2d
from manticore.lops.modal_dg.stdops2d import factory_stdevaluator2d
from manticore.lops.modal_dg.stdops2d import factory_stddifferentiator2d
from manticore.lops.modal_dg.stdops2d import factory_stddivergent2d
from manticore.lops.modal_dg.tests.test_helpers import Polynomial
from manticore.lops.modal_dg.tests.test_helpers import Polynomial_Dx
from manticore.lops.modal_dg.tests.test_helpers import Polynomial_Dy


def doForward(classRegion, k):

    region = classRegion()

    S = ExpansionSize.get(region.G, k.order)

    C = np.ones(S)  # Coefficient array
    Ph = np.zeros(k.n2d)  # Physical values array

    bw = factory_stdbackward2d(classRegion)(k)
    bw.at_ips(C, Ph)

    Cl = np.zeros(S)  # Another coefficient array

    fw = factory_stdforward2d(classRegion)(k)

    fw(Ph, Cl)

    return np.linalg.norm(C - Cl)


def doPolyDerivative(classRegion, k, a, b):

    Ntot = k.n2d

    # Evaluation operator to create physical values on integration points
    ev = factory_stdevaluator2d(classRegion)(k)

    P = Polynomial(a, b)
    Px = Polynomial_Dx(a, b)
    Py = Polynomial_Dy(a, b)

    # Arrays of physical values on integration points
    Ph = ev(P)
    Phx = ev(Px)
    Phy = ev(Py)

    # Arrays for numerical derivatives
    dPhdx = np.zeros(Ntot)
    dPhdy = np.zeros(Ntot)

    op = factory_stddifferentiator2d(classRegion)(k)

    op(Ph, dPhdx, dPhdy)

    checkx = np.isnan(dPhdx)
    checky = np.isnan(dPhdy)
    assert not checkx.all()
    assert not checky.all()

    ex = np.linalg.norm(dPhdx - Phx)
    ey = np.linalg.norm(dPhdy - Phy)

    if np.linalg.norm(Phx) > 0.:
        ex /= np.linalg.norm(Phx)

    if np.linalg.norm(Phy) > 0.:
        ey /= np.linalg.norm(Phy)

    return ex, ey


def doDivergent(classRegion, function, k):

    Ntot = k.n2d

    # Evaluation operator to create physical values on integration points
    ev = factory_stdevaluator2d(classRegion)(k)

    Fxi_1 = ev(function.Fx)
    Fxi_2 = ev(function.Fy)
    Div = ev(function.Fdiv)

    nDiv = np.zeros(Ntot)

    op = factory_stddivergent2d(classRegion)(k)

    op(Fxi_1, Fxi_2, nDiv)

    return np.linalg.norm(nDiv - Div)
