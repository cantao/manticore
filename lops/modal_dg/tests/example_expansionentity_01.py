#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_expansionentity_01.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.modal_dg.entity import ExpansionEntityFactory
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.geom.standard_geometries import (
    StandardGeometry, geom, number_of_nodes)
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldRole, FieldType


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example ExpansionEntity 01")

    e1 = ExpansionEntityFactory.make(StandardGeometry.QUAD16)
    e2 = ExpansionEntityFactory.make(StandardGeometry.TRI10)
    k  = ExpansionKeyFactory.make(5, 10)
    

    # Evaluation on each element
    logger.debug("e1 = (%s%s, %s)" % (
        geom(e1.geom_type), number_of_nodes(e1.geom_type), e1.type))
    logger.debug("DG expansion: p-order = %s, n.i.p. 1D = %s" % k.key)
    

    vlist = [FieldVariable.RHO,
             FieldVariable.RHOU,
             FieldVariable.RHOV,
             FieldVariable.RHOE]

    e1.key = k
    e1.role = EntityRole.PHYSICAL
    e1.init_variables(vlist)
    e1.get_field(FieldRole.State, FieldType.ph, FieldVariable.RHO ).fill(1.0)
    e1.get_field(FieldRole.State, FieldType.ph, FieldVariable.RHOU).fill(1.2)
    e1.get_field(FieldRole.State, FieldType.ph, FieldVariable.RHOV).fill(1.4)
    e1.get_field(FieldRole.State, FieldType.ph, FieldVariable.RHOE).fill(1.8)

    for v in vlist:

        logger.debug("e1.field(%s, %s).size = %s" % (
            v,
            FieldType.ph,
            e1.get_field(FieldRole.State, FieldType.ph, v ).shape[0]) )

    for v in vlist:
        
        logger.debug("e1.field(%s, %s)[57] = %s" % (
            v,
            FieldType.ph,
            e1.get_field(FieldRole.State, FieldType.ph, v )[57]) )

    e2.key = k
    e2.role = EntityRole.PHYSICAL    
    e2.init_variables(vlist)
    e2.get_field(FieldRole.State, FieldType.ph, FieldVariable.RHO ).fill(1.1)
    e2.get_field(FieldRole.State, FieldType.ph, FieldVariable.RHOU).fill(1.3)
    e2.get_field(FieldRole.State, FieldType.ph, FieldVariable.RHOV).fill(1.5)
    e2.get_field(FieldRole.State, FieldType.ph, FieldVariable.RHOE).fill(1.9)

    logger.debug("e2 = (%s%s, %s)" % (
        geom(e2.geom_type), number_of_nodes(e2.geom_type), e2.type))
    logger.debug("DG expansion: p-order = %s, n.i.p. 1D = %s" % k.key)

    for v in vlist:

        logger.debug("e2.field(%s, %s).size = %s" % (
            v,
            FieldType.ph,
            e2.get_field(FieldRole.State, FieldType.ph, v ).shape[0]) )

    for v in vlist:
        
        logger.debug("e2.field(%s, %s)[57] = %s" % (
            v,
            FieldType.ph,
            e2.get_field(FieldRole.State, FieldType.ph, v )[57]) )

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
