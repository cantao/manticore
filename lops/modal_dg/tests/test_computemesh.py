#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# LOPS.MODAL_DG tests
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#
# $ python3 -m unittest -v manticore/lops/modal_dg/tests/test_computemesh.py

import unittest
import manticore
import numpy as np
from manticore.lops.modal_dg.tests.simple_meshes import mesh02
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldType
from manticore.lops.modal_dg.entity import GlobalRoleIterator
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.lops.modal_dg.entityops import (
    ComputeEntityVolume, ComputeEntityMassMatrix,
    InitializeFields, InitFieldValues )
from manticore.lops.modal_dg.class_instances import (
    class_quad_physbackward2d, class_tria_physbackward2d )


class FaceDockingTestCase(unittest.TestCase):
    def setUp(self):
        self.cm = mesh02(l=10.0, n=8, p_order=1)

    def test_neighborhood(self):
        """ Tests if neighborhood was correctly built.
        """

        elist = GlobalRoleIterator(self.cm.container).find(EntityRole.PHYSICAL)

        for e in elist:
            neigh = e.get_neighbourhood()

            for face,ng in enumerate(neigh):
                ng_face = e.get_neighbour_face_id(face)
                self.assertEqual(face, ng.get_neighbour_face_id(ng_face))
