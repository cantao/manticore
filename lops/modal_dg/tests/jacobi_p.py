#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Jacobi polynomials evaluated at a point or vector
#
# by
#    Renato Cantao (Python version)
#
from numbers import Number
import numpy as np


def jacobi_p(x, m, alpha=0.0, beta=0.0):
    """Jacobi polynomial."""
    number_flag = False

    if isinstance(x, Number):
        x = np.array([x])
        number_flag = True

    aPb = alpha + beta  # mnemonics: alpha plus beta
    aMb = alpha - beta  # mnemonics: alpha minus beta

    Pn = np.ones(x.shape)
    Pn1 = 0.5 * (aMb + (aPb + 2.0) * x)

    if m == 0:
        Pm = Pn
    elif m == 1:
        Pm = Pn1
    else:
        for n in range(1, m):
            n1 = n + 1.0
            n2 = 2.0 * n

            a1n = 2.0 * n1 * (n1 + aPb) * (n2 + aPb)
            a2n = (n2 + aPb + 1.0) * aPb * aMb
            a3n = (n2 + aPb) * (n2 + aPb + 1.0) * (n2 + aPb + 2.0)
            a4n = 2.0 * (n + alpha) * (n + beta) * (n2 + aPb + 2.0)

            Pn2 = ((a2n + a3n * x) * Pn1 - a4n * Pn) / a1n
            Pn = Pn1
            Pn1 = Pn2

        Pm = Pn2

    if number_flag:
        return Pm[0]
    else:
        return Pm


#-- jacobi_p.py ----------------------------------------------------------------
