#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
from math import pi, cos, sin

Q = np.array([[cos(pi), sin(pi)], [-sin(pi), cos(pi)]])


class SingleElementGenerator:
    """Generatrion of different physical single elements for testing.
    """

    def std_quad4():
        """ Four node, standard quadrangle. """

        coords = np.array([
            [-1.0, -1.0],  # 0
            [1.0, -1.0],   # 1
            [1.0, 1.0],    # 2
            [-1.0, 1.0],   # 3
        ])

        return coords.T

    def std_quad9():
        """ Nine node, standard quadrangle. """

        coords = np.array([
            [-1.0, -1.0],  # 0
            [1.0, -1.0],  # 1
            [1.0, 1.0],  # 2
            [-1.0, 1.0],  # 3
            #
            [0.0, -1.0],  # 4
            [1.0, 0.0],  # 5
            [0.0, 1.0],  # 6
            [-1.0, 0.0],  # 7
            #
            [0.0, 0.0]  # 8
        ])

        return coords.T

    def std_quad16():
        """ 16 node, standard quadrangle. """

        coords = np.array([
            [-1.0, -1.0],  # 0
            [1.0, -1.0],  # 1
            [1.0, 1.0],  # 2
            [-1.0, 1.0],  # 3
            #
            [-1. / 3., -1.0],  # 4
            [1. / 3., -1.0],  # 5
            #
            [1.0, -1. / 3.],  # 6
            [1.0, 1. / 3.],  # 7
            #
            [1. / 3., 1.0],  # 8
            [-1. / 3., 1.0],  # 9
            #
            [-1.0, 1. / 3.],  # 10
            [-1.0, -1. / 3.],  # 11
            #
            [-1. / 3., -1. / 3.],  # 12
            [1. / 3., -1. / 3.],  # 13
            [1. / 3., 1. / 3.],  # 14
            [-1. / 3., 1. / 3.]  # 15
        ])

        return coords.T

    def std_quad25():
        """ 25 node, standard quadrangle. """

        coords = np.array([
            [-1.0, -1.0],  # 0
            [1.0, -1.0],  # 1
            [1.0, 1.0],  # 2
            [-1.0, 1.0],  # 3
            #
            [-0.5, -1.0],  # 4
            [0.0, -1.0],  # 5
            [0.5, -1.0],  # 6
            #
            [1.0, -0.5],  # 7
            [1.0, 0.0],  # 8
            [1.0, 0.5],  # 9
            #
            [0.5, 1.0],  # 10
            [0.0, 1.0],  # 11
            [-0.5, 1.0],  # 12
            #
            [-1.0, 0.5],  # 13
            [-1.0, 0.0],  # 14
            [-1.0, -0.5],  # 15
            #
            [-0.5, -0.5],  # 16
            [0.5, -0.5],  # 17
            [0.5, 0.5],  # 18
            [-0.5, 0.5],  # 19
            #
            [0.0, -0.5],  # 20
            [0.5, 0.0],  # 21
            [0.0, 0.5],  # 22
            [-0.5, 0.0],  # 23
            #
            [0.0, 0.0]  # 24
        ])

        return coords.T

    # Homogeneous Scaling: Unity area
    def quad4_1():

        return 0.5 * SingleElementGenerator.std_quad4()

    def quad9_1():

        return 0.5 * SingleElementGenerator.std_quad9()

    def quad16_1():

        return 0.5 * SingleElementGenerator.std_quad16()

    def quad25_1():

        return 0.5 * SingleElementGenerator.std_quad25()

    # Translation plus scaling: Unity area
    def quad4_2():

        coords = SingleElementGenerator.std_quad4()
        coords += 5.

        return 0.5 * coords

    def quad9_2():

        coords = SingleElementGenerator.std_quad9()
        coords += 5.

        return 0.5 * coords

    def quad16_2():

        coords = SingleElementGenerator.std_quad16()
        coords += 5.

        return 0.5 * coords

    def quad25_2():

        coords = SingleElementGenerator.std_quad25()
        coords += 5.

        return 0.5 * coords

    # Pure Shear: Unity area
    def quad4_3():

        coords = SingleElementGenerator.std_quad4()
        coords[1, :] += 1.  # translation in y
        coords[0, :] += 0.2 * coords[1, :]  # shear

        return 0.5 * coords

    def quad9_3():

        coords = SingleElementGenerator.std_quad9()
        coords[1, :] += 1.  # translation in y
        coords[0, :] += 0.2 * coords[1, :]  # shear

        return 0.5 * coords

    def quad16_3():

        coords = SingleElementGenerator.std_quad16()
        coords[1, :] += 1.  # translation in y
        coords[0, :] += 0.2 * coords[1, :]  # shear

        return 0.5 * coords

    def quad25_3():

        coords = SingleElementGenerator.std_quad25()
        coords[1, :] += 1.  # translation in y
        coords[0, :] += 0.2 * coords[1, :]  # shear

        return 0.5 * coords

    # Nonlinear Deformation+Translation+Scaling: Unity area
    def quad9_4():

        coords = SingleElementGenerator.std_quad9()

        # Symmetric nonlinear deformation
        coords[1, 4] += 0.1
        coords[1, 6] += 0.1

        coords += 5.

        return 0.5 * coords

    def quad16_4():

        coords = SingleElementGenerator.std_quad16()

        # Symmetric nonlinear deformation
        coords[1, 4] += 0.1
        coords[1, 5] += 0.1
        coords[1, 8] += 0.1
        coords[1, 9] += 0.1

        coords += 5.

        return 0.5 * coords

    def quad25_4():

        coords = SingleElementGenerator.std_quad25()

        # Symmetric nonlinear deformation
        coords[1, 4] += 0.1
        coords[1, 5] += 0.1
        coords[1, 6] += 0.1
        coords[1, 10] += 0.1
        coords[1, 11] += 0.1
        coords[1, 12] += 0.1

        return 0.5 * coords

    # Non homogeneous Scaling: Unity area
    def quad4_5():

        coords = SingleElementGenerator.std_quad4()
        coords[0, :] *= 2.
        coords[1, :] *= 0.5

        return 0.5 * coords

    def quad9_5():

        coords = SingleElementGenerator.std_quad9()
        coords[0, :] *= 2.
        coords[1, :] *= 0.5

        return 0.5 * coords

    def quad16_5():

        coords = SingleElementGenerator.std_quad16()
        coords[0, :] *= 2.
        coords[1, :] *= 0.5

        return 0.5 * coords

    def quad25_5():

        coords = SingleElementGenerator.std_quad25()
        coords[0, :] *= 2.
        coords[1, :] *= 0.5

        return 0.5 * coords


class SingleTriElementGenerator:
    """Generatrion of different physical single elements for testing.
    """

    def std_tri3():
        """ Three node, standard triangle. """

        coords = np.array([
            [-1.0, -1.0],  # 0
            [1.0, -1.0],  # 1
            [-1.0, 1.0]  # 2
        ])

        return coords.T

    def std_tri6():
        """ Six node, standard triangle. """

        coords = np.array([
            [-1.0, -1.0],  # 0
            [1.0, -1.0],  # 1
            [-1.0, 1.0],  # 2
            #
            [0.0, -1.0],  # 3
            [0.0, 0.0],  # 4
            [-1.0, 0.0]  # 5
        ])

        return coords.T

    def std_tri10():
        """ Nine node, standard triangle. """

        coords = np.array([
            [-1.0, -1.0],  # 0
            [1.0, -1.0],  # 1
            [-1.0, 1.0],  # 2
            #
            [-1. / 3., -1.0],  # 3
            [1. / 3., -1.0],  # 4
            #
            [1. / 3., -1. / 3.],  # 5
            [-1. / 3., 1. / 3.],  # 6
            #
            [-1.0, 1. / 3.],  # 7
            [-1.0, -1. / 3.],  # 8
            #
            [-1. / 3., -1. / 3.]  # 9
        ])

        return coords.T

    def std_tri15():
        """ Nine node, standard quadrangle. """

        coords = np.array([
            [-1.0, -1.0],  # 0
            [1.0, -1.0],  # 1
            [-1.0, 1.0],  # 2
            #
            [-0.5, -1.0],  # 3
            [0.0, -1.0],  # 4
            [0.5, -1.0],  # 5
            #
            [0.5, -0.5],  # 6
            [0.0, 0.0],  # 7
            [-0.5, 0.5],  # 8
            #
            [-1.0, 0.5],  # 9
            [-1.0, 0.0],  # 10
            [-1.0, -0.5],  # 11
            #
            [-0.5, -0.5],  # 12
            [0.0, -0.5],  # 13
            [-0.5, 0.0]  # 14
        ])

        return coords.T

    # Scaling: 1/2 area
    def tri3_1():

        return 0.5 * SingleTriElementGenerator.std_tri3()

    def tri6_1():

        return 0.5 * SingleTriElementGenerator.std_tri6()

    def tri10_1():

        return 0.5 * SingleTriElementGenerator.std_tri10()

    def tri15_1():

        return 0.5 * SingleTriElementGenerator.std_tri15()

    # Translation plus scaling: 1/2 area
    def tri3_2():

        coords = SingleTriElementGenerator.std_tri3()
        coords += 5.

        return 0.5 * coords

    def tri6_2():

        coords = SingleTriElementGenerator.std_tri6()
        coords += 5.

        return 0.5 * coords

    def tri10_2():

        coords = SingleTriElementGenerator.std_tri10()
        coords += 5.

        return 0.5 * coords

    def tri15_2():

        coords = SingleTriElementGenerator.std_tri15()
        coords += 5.

        return 0.5 * coords

    # Pure Shear: 1/2 area
    def tri3_3():

        coords = SingleTriElementGenerator.std_tri3()
        coords[1, :] += 1.  # translation in y
        coords[0, :] += 0.2 * coords[1, :]  # shear

        return 0.5 * coords

    def tri6_3():

        coords = SingleTriElementGenerator.std_tri6()
        coords[1, :] += 1.  # translation in y
        coords[0, :] += 0.2 * coords[1, :]  # shear

        return 0.5 * coords

    def tri10_3():

        coords = SingleTriElementGenerator.std_tri10()
        coords[1, :] += 1.  # translation in y
        coords[0, :] += 0.2 * coords[1, :]  # shear

        return 0.5 * coords

    def tri15_3():

        coords = SingleTriElementGenerator.std_tri15()
        coords[1, :] += 1.  # translation in y
        coords[0, :] += 0.2 * coords[1, :]  # shear

        return 0.5 * coords

    # Nonlinear Deformation+Translation+Scaling: Unity area
    def tri6_4a():

        coords = SingleTriElementGenerator.std_tri6()

        # Symmetric nonlinear deformation
        coords[1, 3] += 0.1

        coords += 5.

        return 0.5 * coords

    def tri6_4b():

        coords = Q.dot(SingleTriElementGenerator.std_tri6())

        # Symmetric nonlinear deformation
        coords[1, 3] += 0.1

        coords += 5.

        return 0.5 * coords

    # Nonlinear Deformation+Translation+Scaling: Unity area
    def tri6_4c():

        coords = SingleTriElementGenerator.std_tri6()

        # Symmetric nonlinear deformation
        coords[1, 3] += 0.1
        coords[1, 4] += 0.1
        coords[0, 5] -= 0.1

        coords += 5.

        return 0.5 * coords

    def tri10_4a():

        coords = SingleTriElementGenerator.std_tri10()

        # Symmetric nonlinear deformation
        coords[1, 3] += 0.1
        coords[1, 4] += 0.1

        coords += 5.

        return 0.5 * coords

    def tri10_4b():

        coords = Q.dot(SingleTriElementGenerator.std_tri10())

        # Symmetric nonlinear deformation
        coords[1, 3] += 0.1
        coords[1, 4] += 0.1

        coords += 5.

        return 0.5 * coords

    def tri10_4c():

        coords = SingleTriElementGenerator.std_tri10()

        # Symmetric nonlinear deformation
        coords[1, 3] += 0.1
        coords[1, 4] += 0.1
        coords[1, 5] += 0.1
        coords[1, 6] += 0.1
        coords[0, 7] -= 0.1
        coords[0, 8] -= 0.1

        coords += 5.

        return 0.5 * coords

    def tri15_4a():

        coords = SingleTriElementGenerator.std_tri15()

        # Symmetric nonlinear deformation
        coords[1, 3] += 0.1
        coords[1, 4] += 0.1
        coords[1, 5] += 0.1

        coords += 5.

        return 0.5 * coords

    def tri15_4b():

        coords = Q.dot(SingleTriElementGenerator.std_tri15())

        # Symmetric nonlinear deformation
        coords[1, 3] += 0.1
        coords[1, 4] += 0.1
        coords[1, 5] += 0.1

        coords += 5.

        return 0.5 * coords

    def tri15_4c():

        coords = SingleTriElementGenerator.std_tri15()

        # Symmetric nonlinear deformation
        coords[1, 3] += 0.1
        coords[1, 4] += 0.1
        coords[1, 5] += 0.1
        coords[1, 6] += 0.1
        coords[1, 7] += 0.1
        coords[1, 8] += 0.1
        coords[0, 9] -= 0.1
        coords[0, 10] -= 0.1
        coords[0, 11] -= 0.1

        coords += 5.

        return 0.5 * coords
