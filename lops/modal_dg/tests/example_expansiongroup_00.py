#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_expansioncontainer_00.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.modal_dg.entity import ExpansionEntityFactory
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.entity import GlobalExpansionContainer
from manticore.lops.modal_dg.computemesh import ExpansionGroup
from manticore.geom.standard_geometries import (
    StandardGeometry, geom, number_of_nodes)
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldRole, FieldType


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example ExpansionGroup 00")

    M = 5
    N = 2*M+1
    S = 10

    g = ExpansionGroup(1, "FLUID", EntityRole.PHYSICAL)
    k1, c1 = g.create_container(M, N)
    k2, c2 = g.create_container(2*M, 2*N)

    for i in range(S):
        e1 = ExpansionEntityFactory.make(StandardGeometry.QUAD16)
        e1.ID = i
        e1.key = k1
        e2 = ExpansionEntityFactory.make(StandardGeometry.TRI10)
        e2.ID = i+S
        e2.key = k1

        e = c1.insert(e1)
        logger.debug(
            "Inserted element ID = %s in container (%s,%s)" % (
            e.ID, k1.key.order, k1.key.nip))
        e = c1.insert(e2)
        logger.debug(
            "Inserted element ID = %s in container (%s,%s)" % (
            e.ID, k1.key.order, k1.key.nip))

        
        e1 = ExpansionEntityFactory.make(StandardGeometry.QUAD25)
        e1.ID = i+2*S
        e1.key = k2
        e2 = ExpansionEntityFactory.make(StandardGeometry.TRI15)
        e2.ID = i+3*S
        e2.key = k2

        e = c2.insert(e1)
        logger.debug(
            "Inserted element ID = %s in container (%s,%s)" % (
            e.ID, k2.key.order, k2.key.nip))
        e = c2.insert(e2)
        logger.debug(
            "Inserted element ID = %s in container (%s,%s)" % (
            e.ID, k2.key.order, k2.key.nip))

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
