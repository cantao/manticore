#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_physmassmatrix_03.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0,'.')
#
#
import manticore
from manticore.geom.standard_geometries import StandardGeometry
from manticore.geom.standard_geometries import geometry_name
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.gauss import interior_nip_rule
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.dgtypes import factory_class_region
from manticore.lops.modal_dg.dgtypes import RegionInfo
from manticore.lops.modal_dg.geomfactors import CollapsedToGlobalMaps
from manticore.lops.nodal_cg.interpol import InterpolationKey
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions
from manticore.lops.modal_dg.phyops2d import factory_physmassmatrix
from manticore.lops.modal_dg.phyops2d import factory_WADG_invmassmatrix
from manticore.lops.modal_dg.tests.test_helpers import Ones
from manticore.lops.modal_dg.tests.singel import SingleElementGenerator
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory

def main():
    logger = manticore.logging.getLogger('MTC_LOGGER') 

    logger.info("Running example PhysMassMatrix 03")
    
    geo      = StandardGeometry.QUAD25         # <----------
    shfuncs  = factory_shape_functions(geo)()

    M   = 4
    N   = 2*M + shfuncs.jacobian_order()
    nip_1d = interior_nip_rule(GaussPointsType.GaussLegendre, N)
    nip_2d = nip_1d**2

    logger.debug( "Geometric interpolation = %s" % (geometry_name(geo)) )
    logger.debug( "Mass matrix order = %s" % (N) )
    logger.debug( "Number of 1D integration points = %s" % (nip_1d) )
    logger.debug( "Number of 2D integration points = %s" % (nip_2d) )

    k  = ExpansionKeyFactory.make(M, nip_1d)         # DG expansion key
    gk = InterpolationKey(geo, nip_2d)   # Geom interpolation key

    # Setting up operators
    classRegion = factory_class_region( RegionInfo.mesh_to_dg(gk.shape) )
    region      = classRegion()
    S    = ExpansionSize.get(region.G, k.order)
    detJ = np.zeros(k.n2d)
    Mass = np.zeros((S, S))
    WADGInv = np.zeros((S, S))
    mass_op    = factory_physmassmatrix( classRegion )(k)
    invmass_op = factory_WADG_invmassmatrix( classRegion )(k)
    CGMaps   = CollapsedToGlobalMaps(ExpansionKeyFactory.gaussWIP)

    logger.debug( "Expansion size = %s" % (S) )
    np.set_printoptions(precision=4,linewidth=120)

    # Evaluation on each element
    logger.info("Scaling...")
    coords = SingleElementGenerator.quad25_1() # <----------
    CGMaps.det_jacobian(gk, coords, detJ)
    mass_op( detJ, Mass )
    invMass = np.linalg.inv(Mass)
    invmass_op( detJ, WADGInv )
    norm_inv  = np.linalg.norm(invMass)
    norm_diff = np.linalg.norm(WADGInv-invMass)
    logger.debug( "Mass Matrix = \n%s" % (Mass) )
    logger.debug( "Numpy Mass Matrix Inv = \n%s" % (invMass) )
    logger.debug( "WADG Mass Matrix Inv = \n%s" % (WADGInv) )
    logger.debug( "||Inv||= %s" % (norm_inv) )
    logger.debug( "||WADGInv - Inv||= %s" % (norm_diff) )
    logger.debug( "||WADGInv - Inv||/||Inv||= %s" % (norm_diff/norm_inv) )

    logger.info("Nonlinear Deformation+Translation+Scaling...")
    coords = SingleElementGenerator.quad25_4() # <----------
    CGMaps.det_jacobian(gk, coords, detJ)
    mass_op( detJ, Mass )
    invMass = np.linalg.inv(Mass)
    invmass_op( detJ, WADGInv )
    norm_inv  = np.linalg.norm(invMass)
    norm_diff = np.linalg.norm(WADGInv-invMass)
    logger.debug( "Mass Matrix = \n%s" % (Mass) )
    logger.debug( "Numpy Mass Matrix Inv = \n%s" % (invMass) )
    logger.debug( "WADG Mass Matrix Inv = \n%s" % (WADGInv) )
    logger.debug( "||Inv||= %s" % (norm_inv) )
    logger.debug( "||WADGInv - Inv||= %s" % (norm_diff) )
    logger.debug( "||WADGInv - Inv||/||Inv||= %s" % (norm_diff/norm_inv) )

    logger.info(  "Example ended!\n" )

    # end of main

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument('-ll', '--loglevel',
                        type=str,
                        choices=['DEBUG','INFO','WARNING','ERROR','CRITICAL'],
                        default='CRITICAL',
                        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':'%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt': "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()
    
    manticore.logging.shutdown()


