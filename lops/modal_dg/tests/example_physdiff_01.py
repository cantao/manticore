#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_physdiff_01.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.geom.standard_geometries import StandardGeometry
from manticore.geom.standard_geometries import number_of_nodes, geom
from manticore.lops.modal_dg.entity import ExpansionEntityFactory
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.gauss import interior_nip_rule
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.dgtypes import factory_class_region
from manticore.lops.modal_dg.dgtypes import RegionInfo
from manticore.lops.modal_dg.phyops2d import factory_physdifferentiator2d
from manticore.lops.nodal_cg.interpol import InterpolationKey
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions
from manticore.lops.modal_dg.tests.utests_phys import doPolyDerivative
from manticore.lops.modal_dg.tests.singel import SingleTriElementGenerator

def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example PhysDiff 01")

    entity_option = {
        StandardGeometry.TRI3: [
            (SingleTriElementGenerator.tri3_1, 'Homogeneous Scaling'),
            (SingleTriElementGenerator.tri3_2, 'Translation plus scaling'),
            (SingleTriElementGenerator.tri3_3, 'Pure Shear')],
        StandardGeometry.TRI6: [
            (SingleTriElementGenerator.tri6_1, 'Homogeneous Scaling'),
            (SingleTriElementGenerator.tri6_2, 'Translation plus scaling'),
            (SingleTriElementGenerator.tri6_3, 'Pure Shear'),
            (SingleTriElementGenerator.tri6_4a,'Nonlinear Def+Transl+Scal')],
        StandardGeometry.TRI10:[
            (SingleTriElementGenerator.tri10_1, 'Homogeneous Scaling'),
            (SingleTriElementGenerator.tri10_2, 'Translation plus scaling'),
            (SingleTriElementGenerator.tri10_3, 'Pure Shear'),
            (SingleTriElementGenerator.tri10_4a,'Nonlinear Def+Trans+Scal')],
        StandardGeometry.TRI15:[
            (SingleTriElementGenerator.tri15_1, 'Homogeneous Scaling'),
            (SingleTriElementGenerator.tri15_2, 'Translation plus scaling'),
            (SingleTriElementGenerator.tri15_3, 'Pure Shear'),
            (SingleTriElementGenerator.tri15_4a,'Nonlinear Def+Trans+Scal')]
        }

    for geo in entity_option:

        logger.info("Testing %s%s, %s elements..." % (
            geom(geo), number_of_nodes(geo), len(entity_option[geo])))
        
        shfuncs = factory_shape_functions(geo)()
        M = 10
        MM = int(M/2)
        
        shfo = shfuncs.jacobian_order()
        if shfo == 0:
            shfo = 1
        
        N = 2 * M + shfo
        
        #nip_1d = interior_nip_rule(GaussPointsType.GaussLegendre, N)
        nip_1d = N
        nip_2d = nip_1d**2

        gk = InterpolationKey.get_instance(geo, nip_2d)
        k  = ExpansionKeyFactory.make(M, nip_1d)
        e  = ExpansionEntityFactory.make(geo)
        e.key = k

        for ii in range(3):
            e._fc_n1d.append(nip_1d+1)

        classRegion = factory_class_region(RegionInfo.mesh_to_dg(gk.shape))
        
        for elgen in entity_option[geo]:
            
            logger.info('Testing %s... Press <ENTER>' % elgen[1])
            input()
            
            coords  = elgen[0]()            
            e.set_coeff(coords)
            e.eval_jacobian()

            for a in range(MM+1):
                for b in range(MM+1):
                    ex, ey = doPolyDerivative(classRegion, e, a, b)
                    logger.debug("Deriving x^%s y^%s:" % (a, b))
                    logger.debug("Error dx = %s Error dy = %s" % (ex, ey))

            logger.info('Finished testing %s... Press <ENTER>' % elgen[1])
            input()

        logger.info("Finished testing %s%s..." % (
            geom(geo), number_of_nodes(geo)))
            

    logger.info("Example ended!")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
