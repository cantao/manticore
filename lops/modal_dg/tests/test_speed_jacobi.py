#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Executar (no diretorio acima do manticore):
# $ python3 -mtimeit -s 'import manticore.lops.modal_dg.tests.test_jacobi_speed as jac' 'jac.f1()'
# e
# $ python3 -mtimeit -s 'import manticore.lops.modal_dg.tests.test_jacobi_speed as jac' 'jac.f2()'

from math import cos, pi, fabs
from sys import float_info
from manticore.lops.modal_dg.tests.jacobi_p import jacobi_p
from manticore.lops.modal_dg.tests.djacobi_p import djacobi_p
from manticore.lops.modal_dg.tests.jacobi_roots import jacobi_roots
from manticore.lops.modal_dg.jacobi import JacobiPolynomial
import numpy as np


def f1():
    order = 10
    x = np.linspace(-1, 1, 20)

    J = jacobi_p(x, order)
    DJ = djacobi_p(x, order)

    r1 = jacobi_roots(order)
    r2 = jacobi_roots(order + 2)
    #r1 = jacobi_roots(order)
    #r2 = jacobi_roots(order+2)


def f2():
    order = 10
    x = np.linspace(-1, 1, 20)

    jp = JacobiPolynomial(order)

    J = jp.eval(x)
    DJ = jp.deval(x)

    r1 = jp.roots()
    jp.configure(order + 2)
    r2 = jp.roots()
    #jp.set_order(order)
    #r1 = jp.roots()
    #jp.set_order(order+2)
    #r2 = jp.roots()
