#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_fields_00.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.modal_dg.fields import FieldVarList
from manticore.services.fieldvariables import FieldVariable


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example Fields 00")

    vlist = list(FieldVariable)

    vnames = [v.name for v in vlist]

    logger.info("Testing bidirectional map...")
    logger.debug("List of variables = %s" % (vnames))

    list_vars = FieldVarList(*vlist)

    for v in vlist:
        idx = list_vars(v)
        logger.debug("[%s]: Index %s" % (v, idx))
        logger.debug("[Index %s]: %s" % (idx, list_vars.right(idx)))

    logger.info("Testing flyweight storage...")
    l = [FieldVariable.DELTAF, FieldVariable.DFV4]
    a = FieldVarList.get_instance(*l)
    logger.debug("a =  %s" % (a._vars))    
    b = FieldVarList.get_instance(*l)
    logger.debug("b =  %s" % (b._vars))
    logger.debug("a is b?  %s" % (a is b))
    logger.debug("hash(a) =  %s" % (hash(a)))
    logger.debug("hash(b) =  %s" % (hash(b)))

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
