# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Executar (no diretorio acima do manticore):
# $ python3 -mtimeit -s 'import manticore.lops.modal_dg.tests.test_jacobi_speed as jac' 'jac.f1()'
# e
# $ python3 -mtimeit -s 'import manticore.lops.modal_dg.tests.test_jacobi_speed as jac' 'jac.f2()'

import numpy as np


def f1():
    p = 20
    n = p + 1
    x = np.linspace(-1, 1, n)
    C = 2.0 * np.ones(n * n)

    Fy = np.dot(np.reshape(C, (n, n)), x)
    Fx = np.dot(Fy, x)


def f2():
    p = 20
    n = p + 1
    x = np.linspace(-1, 1, n)
    C = 2.0 * np.ones(n * n)

    xy = np.reshape(x, (n, 1)) * x
    Fx = np.dot(C, np.ravel(xy))
