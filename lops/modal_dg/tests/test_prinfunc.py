#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# LOPS.MODAL_DG tests
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#
# $ python3 -m unittest -v manticore/lops/modal_dg/tests/test_prinfunc.py

import unittest
import manticore
import numpy as np
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.gauss import GaussKey, JacobiGaussQuadratures
from manticore.lops.modal_dg.gauss import standard_nip_rule
from manticore.lops.modal_dg.gauss import interior_nip_rule
from manticore.lops.modal_dg.dgtypes import PrincipalFunctionType
from manticore.lops.modal_dg.prinfunc import PrincipalFunctionKey
from manticore.lops.modal_dg.prinfunc import PrincipalFunctionTable
from manticore.lops.modal_dg.prinfunc import PrincipalFunctions


class PrincipalFunctionsTestCase(unittest.TestCase):
    gaussWIP = JacobiGaussQuadratures()

    pfs = PrincipalFunctions(gaussWIP)

    for order in range(20):

        int_order = 2 * (order + 1)

        nip0 = interior_nip_rule(GaussPointsType.GaussLegendre, int_order)

        nip1 = standard_nip_rule(GaussPointsType.GaussLegendre, int_order)

        key = PrincipalFunctionKey(
            PrincipalFunctionType.OrthoPrincFuncA, order,
            GaussKey(GaussPointsType.GaussLegendre, nip0))

        t = pfs(key)

        key = PrincipalFunctionKey(
            PrincipalFunctionType.OrthoPrincFuncA, order,
            GaussKey(GaussPointsType.GaussLegendre, nip1))

        t = pfs(key)

        key = PrincipalFunctionKey(
            PrincipalFunctionType.OrthoPrincFuncB, order,
            GaussKey(GaussPointsType.GaussLegendre, nip0))

        t = pfs(key)

        key = PrincipalFunctionKey(
            PrincipalFunctionType.OrthoPrincFuncB, order,
            GaussKey(GaussPointsType.GaussLegendre, nip1))

        t = pfs(key)

        key = PrincipalFunctionKey(
            PrincipalFunctionType.OrthoPrincFuncB, order,
            GaussKey(GaussPointsType.GaussJacobi10, nip0))

        t = pfs(key)

        key = PrincipalFunctionKey(
            PrincipalFunctionType.OrthoPrincFuncB, order,
            GaussKey(GaussPointsType.GaussJacobi10, nip1))

        t = pfs(key)

    def test_psiA(self):

        p_order = 10

        integrand_order = 22

        nip = standard_nip_rule(GaussPointsType.GaussLegendre, integrand_order)

        key = PrincipalFunctionKey(
            PrincipalFunctionType.OrthoPrincFuncA,
            order=p_order,
            gauss_key=GaussKey(GaussPointsType.GaussLegendre, nip))

        table = PrincipalFunctionTable(key,
                                       PrincipalFunctionsTestCase.gaussWIP)

        self.assertEqual(table.nmodes, p_order + 1)

        self.assertEqual(table.nip, nip)

        t = PrincipalFunctionsTestCase.pfs(key)

        self.assertEqual(table.nmodes, t.nmodes)

        self.assertEqual(table.nip, t.nip)

    def test_psiB(self):

        p_order = 10

        integrand_order = 22

        nip = standard_nip_rule(GaussPointsType.GaussLegendre, integrand_order)

        key = PrincipalFunctionKey(
            PrincipalFunctionType.OrthoPrincFuncB,
            order=p_order,
            gauss_key=GaussKey(GaussPointsType.GaussJacobi10, nip))

        table = PrincipalFunctionTable(key,
                                       PrincipalFunctionsTestCase.gaussWIP)

        self.assertEqual(table.nmodes, ((p_order + 1) * (p_order + 2)) / 2)

        self.assertEqual(table.nip, nip)

        t = PrincipalFunctionsTestCase.pfs(key)

        self.assertEqual(table.nmodes, t.nmodes)

        self.assertEqual(table.nip, t.nip)

        key = PrincipalFunctionKey(
            PrincipalFunctionType.OrthoPrincFuncB,
            order=p_order,
            gauss_key=GaussKey(GaussPointsType.GaussLegendre, nip))

        t = PrincipalFunctionsTestCase.pfs(key)

        self.assertEqual(((p_order + 1) * (p_order + 2)) / 2, t.nmodes)

        self.assertEqual(nip, t.nip)
