#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# LOPS.MODAL_DG tests
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#
# $ python3 -m unittest -v manticore/lops/modal_dg/tests/test_facedocking.py

import unittest
import manticore
import numpy as np
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.gauss import GaussKey, JacobiGaussQuadratures
from manticore.lops.modal_dg.gauss import standard_nip_rule
from manticore.lops.modal_dg.gauss import standard_nip_rule
from manticore.lops.modal_dg.gauss import interior_nip_rule
from manticore.lops.modal_dg.gauss import CollocationDiffMat
from manticore.lops.modal_dg.gauss_strm import StreamlinedGaussWeights
from manticore.lops.modal_dg.dgtypes import PrincipalFunctionType
from manticore.lops.modal_dg.prinfunc import PrincipalFunctionKey
from manticore.lops.modal_dg.prinfunc import PrincipalFunctionTable
from manticore.lops.modal_dg.prinfunc import PrincipalFunctions
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.geomfactors import FaceDocking


class FaceDockingTestCase(unittest.TestCase):
    def setUp(self):

        self.p_order = 20
        self.integrand_order = 42
        self.face_nip = standard_nip_rule(GaussPointsType.GaussLegendre,
                                          self.integrand_order)
        self.gaussWIP = JacobiGaussQuadratures()
        self.diff_mat = CollocationDiffMat(self.gaussWIP)
        self.strm_gaussW = StreamlinedGaussWeights(self.gaussWIP)
        self.tables = PrincipalFunctions(self.gaussWIP)

        self.key = ExpansionKey(self.p_order, self.face_nip)
        self.key.set_gauss_quadrature(self.gaussWIP)
        self.key.set_principal_functions(self.tables)
        self.key.set_strm_gauss_quadrature(self.strm_gaussW)
        self.key.set_diff_matrix(self.diff_mat)

    def test_QuadQuadDocking(self):
        """Tests if all direct mappings followed by each respective inverse mapping returns to the original points.

        """

        docker = FaceDocking()

        actV = StdRegionType.DG_Quadrangle
        pasV = StdRegionType.DG_Quadrangle

        for actF in range(4):
            for pasF in range(4):
                p1 = self.key.p_on_face(actV, actF, 1)
                p2 = self.key.p_on_face(actV, actF, 2)

                docker.setup(actV, actF, pasV, pasF)
                p1p, p2p = docker.eval(p1, p2)

                docker.setup(pasV, pasF, actV, actF)
                p1a, p2a = docker.eval(p1p, p2p)

                check1 = p1 == p1a
                check2 = p2 == p2a
                self.assertTrue(check1.all())
                self.assertTrue(check2.all())

    def test_QuadTriaDocking(self):
        """Tests if all direct mappings followed by each respective inverse mapping returns to the original points.

        """

        docker = FaceDocking()

        actV = StdRegionType.DG_Quadrangle
        pasV = StdRegionType.DG_Triangle

        for actF in range(4):
            for pasF in range(3):
                p1 = self.key.p_on_face(actV, actF, 1)
                p2 = self.key.p_on_face(actV, actF, 2)

                docker.setup(actV, actF, pasV, pasF)
                p1p, p2p = docker.eval(p1, p2)

                docker.setup(pasV, pasF, actV, actF)
                p1a, p2a = docker.eval(p1p, p2p)

                check1 = p1 == p1a
                check2 = p2 == p2a
                self.assertTrue(check1.all())
                self.assertTrue(check2.all())

    def test_TriaTriaDocking(self):
        """Tests if all direct mappings followed by each respective inverse mapping returns to the original points.

        """

        docker = FaceDocking()

        actV = StdRegionType.DG_Triangle
        pasV = StdRegionType.DG_Triangle

        for actF in range(3):
            for pasF in range(3):
                p1 = self.key.p_on_face(actV, actF, 1)
                p2 = self.key.p_on_face(actV, actF, 2)

                docker.setup(actV, actF, pasV, pasF)
                p1p, p2p = docker.eval(p1, p2)

                docker.setup(pasV, pasF, actV, actF)
                p1a, p2a = docker.eval(p1p, p2p)

                check1 = p1 == p1a
                check2 = p2 == p2a
                self.assertTrue(check1.all())
                self.assertTrue(check2.all())
