#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Executar (no diretorio acima do manticore):
# $ python3 -mtimeit -s 'import manticore.lops.modal_dg.tests.test_speed_stdevaluator as stdev' 'stdev.f1()'
# e
# $ python3 -mtimeit -s 'import manticore.lops.modal_dg.tests.test_speed_stdevaluator as stdev' 'stdev.f2()'

import numpy as np
import manticore
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.gauss import JacobiGaussQuadratures
import manticore.lops.modal_dg.stdmixins2d as _2d_mx
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.dgtypes import factory_class_region
from manticore.lops.modal_dg.geomfactors import factory_colapsed_to_std_maps

gaussWIP = JacobiGaussQuadratures()

M = 10
N = 2 * M + 1

k = ExpansionKey(M, N)
k.set_gauss_quadrature(gaussWIP)

op = _2d_mx.factory_wp_mixin(
    _2d_mx.factory_number_of_ip(
        factory_class_region(StdRegionType.DG_Quadrangle)))(k)

mapp = factory_colapsed_to_std_maps(StdRegionType.DG_Quadrangle)
Ph = np.zeros(op.n2d)


def f1():
    px = op.P1
    py = op.P2
    n = op.n1d
    idx = 0

    for jj in range(n):
        for ii in range(n):
            px_t, py_t = mapp.eval(px[ii], py[jj])
            Ph[idx] = px_t * px_t + py_t * py_t
            idx += 1


def f2():  # 10x faster!
    px = op.P1
    py = op.P2
    px, py = np.meshgrid(px, py)

    px_t, py_t = mapp.eval(np.ravel(px), np.ravel(py))
    Ph = px_t
    Ph *= px_t
    Ph += py_t * py_t
