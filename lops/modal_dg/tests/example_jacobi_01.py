#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_jacobi_01.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.modal_dg.jacobi import JacobiPolynomial


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example JacobiPolynomial 01")

    J0 = JacobiPolynomial(0, 2.0, 4.0)
    J1 = JacobiPolynomial(1, 2.0, 4.0)
    J2 = JacobiPolynomial(2, 2.0, 4.0)
    J3 = JacobiPolynomial(3, 2.0, 4.0)

    xx = np.zeros((6, 6))

    for ii in range(xx.shape[0]):
        for jj in range(xx.shape[1]):
            xx[ii, jj] = 0.2 * ii - 0.7 * jj + 1.0

    y = np.ravel(xx)
    idx = (y >= -1.0) & (y <= 1.0)
    x = y[idx]
    x = np.sort(x)  # Just to observe function behavior more easily

    logger.debug("\nx = \n%s\n" % (x))

    logger.info("Evaluation on Matrix")

    logger.debug("\nJ0(x) = \n%s\n" % (J0.eval(x)))
    logger.debug("\nJ1(x) = \n%s\n" % (J1.eval(x)))
    logger.debug("\nJ2(x) = \n%s\n" % (J2.eval(x)))
    logger.debug("\nJ3(x) = \n%s\n" % (J3.eval(x)))

    logger.info("Derivative evaluation on Matrix")

    logger.debug("\nx = \n%s\n" % (x))

    logger.debug("\nd/dx J0(x) = \n%s\n" % (J0.deval(x)))
    logger.debug("\nd/dx J1(x) = \n%s\n" % (J1.deval(x)))
    logger.debug("\nd/dx J2(x) = \n%s\n" % (J2.deval(x)))
    logger.debug("\nd/dx J3(x) = \n%s\n" % (J3.deval(x)))

    logger.info("Example ended!")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
