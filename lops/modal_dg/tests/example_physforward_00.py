#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_physforward_00.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.geom.standard_geometries import (
    StandardGeometry, geom, number_of_nodes, geometry_name)
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.tests.simple_meshes import mesh03
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldType
from manticore.lops.modal_dg.entity import ExpansionEntityFactory
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.entityops import (
    ComputeEntityVolume, ComputeEntityMassMatrix,
    InitializeFields, InitFieldValues )
from manticore.lops.modal_dg.expantools import ExpansionSize
from math import sqrt
import numpy as np
from manticore.lops.modal_dg.class_instances import (
    class_quad_wadg_physforward, class_quad_physbackward2d)

def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example PhysForward 00")

    M = 8
    N = 2 * M
    k = ExpansionKeyFactory.make(M, N)

    geom_type=StandardGeometry.QUAD4
    e      = ExpansionEntityFactory.make(geom_type)
    e.ID   = 26
    e.role = EntityRole.PHYSICAL
    e.key  = k

    S = ExpansionSize.get(e.type, k.order)

    incid = np.array([31, 32, 41, 40])
    nx = np.array([5.,    6.25,  6.25,  5.  ])
    ny = np.array([-1.25, -1.25,  0.,    0.  ])
    e.set_vertices(incid)
    e.set_coeff(np.array([nx, ny]))
    fsz = e.face_comm_sizes
    fsz.append(N+1)
    fsz.append(N+1)
    fsz.append(N+1)
    fsz.append(N+1)
    
    e.eval_jacobian()

    logger.info("Jacobian = \n%s" % (e.get_jacobian()))

    mass_op = ComputeEntityMassMatrix()
    mass_op.container_operator(0.0, k.key, 0.0)
    mass_op(e)

    logger.info("Inv Mass = \n%s" % (e.get_inv_mass()))

    # mass = e.get_inv_mass()
    # idx = mass < 1E-14
    # mass[idx]=0.0
    # logger.info("Inv Mass = \n%s" % (e.get_inv_mass()))
    
    vol_op = ComputeEntityVolume()
    vol_op.container_operator(0.0, k.key, 0.0)
    vol_op(e)

    logger.info("Volume = %s" % (e.volume))

    func = np.ones(k.n2d)
    Ph   = np.ones(k.n2d)
    Tr   = np.zeros(S)

    fw = class_quad_wadg_physforward(k)
    fw(e.get_jacobian(), e.get_inv_mass(), func, Tr)

    logger.info("Modal coeff = \n%s" % (Tr))

    bw = class_quad_physbackward2d(k)
    bw(Tr, Ph)

    logger.info("Physical values = \n%s" % (Ph))

    logger.info("Diff = \n%s" % (func-Ph))

    logger.info("Error = %s" % (np.linalg.norm(func-Ph)))

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')

    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
                }
            },
            'handlers': {
                'console': {
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                },
                'file':{
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': 'example_euler_01.log',
                    'mode': 'w',
                    'maxBytes': 1048576,
                    'backupCount': 3,
                },
            },
            'loggers': {
                '': {
                    #'handlers': ['console'],
                    'handlers': ['file', 'console'],
                    'level': args.loglevel,
                },
            }
        }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
