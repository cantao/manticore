#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_physintegrator_05.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.geom.standard_geometries import StandardGeometry
from manticore.geom.standard_geometries import geometry_name
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.prinfunc import PrincipalFunctions
from manticore.lops.modal_dg.faceprinfunc import FacePrincipalFunctions
from manticore.lops.modal_dg.gauss import CollocationDiffMat
from manticore.lops.modal_dg.gauss import JacobiGaussQuadratures
from manticore.lops.modal_dg.gauss import interior_nip_rule
from manticore.lops.modal_dg.gauss_strm import StreamlinedGaussWeights
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.dgtypes import factory_class_region
from manticore.lops.modal_dg.dgtypes import RegionInfo
from manticore.lops.modal_dg.geomfactors import CollapsedToGlobalMaps
from manticore.lops.nodal_cg.interpol import InterpolationKey
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions
from manticore.lops.modal_dg.phyops2d import factory_physintegrator2d
from manticore.lops.modal_dg.phyops2d import factory_physevaluator2d
from manticore.lops.modal_dg.tests.test_helpers import Ones
from manticore.lops.modal_dg.tests.singel import SingleTriElementGenerator


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example PhysIntegrator 05")

    gaussWIP = JacobiGaussQuadratures()
    PFuncs = PrincipalFunctions(gaussWIP)
    FPFuncs = FacePrincipalFunctions(gaussWIP)
    CDiffMat = CollocationDiffMat(gaussWIP)
    SGaussW = StreamlinedGaussWeights(gaussWIP)

    CGMaps = CollapsedToGlobalMaps(gaussWIP)
    geo = StandardGeometry.TRI6  # <----------
    shfuncs = factory_shape_functions(geo)()

    M = 1
    N = shfuncs.jacobian_order()
    nip_1d = interior_nip_rule(GaussPointsType.GaussLegendre, N)
    nip_2d = nip_1d**2

    logger.debug("Geometric interpolation = %s" % (geometry_name(geo)))
    logger.debug("Jacobian order = %s" % (N))
    logger.debug("Number of 1D integration points = %s" % (nip_1d))
    logger.debug("Number of 2D integration points = %s" % (nip_2d))

    k = ExpansionKey(M, nip_1d)  # DG expansion key
    gk = InterpolationKey(geo, nip_2d)  # Geom interpolation key

    # ExpansionKey needs access to all these objects (not necessarily
    # for all operations but I going to the safe side here):
    k.set_gauss_quadrature(gaussWIP)
    k.set_strm_gauss_quadrature(SGaussW)
    k.set_diff_matrix(CDiffMat)
    k.set_principal_functions(PFuncs)
    k.set_face_principal_functions(FPFuncs)

    # Setting up operators
    classRegion = factory_class_region(RegionInfo.mesh_to_dg(gk.shape))
    ev = factory_physevaluator2d(classRegion)(k, gk.type, CGMaps)
    op = factory_physintegrator2d(classRegion)(k)
    detJ = np.zeros(gk.nip)

    # Evaluation on each element
    logger.info("Scaling...")
    coords = SingleTriElementGenerator.tri6_1()  # <----------
    Ph = ev(Ones.F, coords)
    CGMaps.det_jacobian(gk, coords, detJ)
    A = op(Ph, detJ)
    logger.debug("Jacobian = %s" % (detJ))
    logger.debug("Numeric integral (area) = %s" % (A))

    logger.info("Translation+Scaling...")
    coords = SingleTriElementGenerator.tri6_2()  # <----------
    Ph = ev(Ones.F, coords)
    CGMaps.det_jacobian(gk, coords, detJ)
    A = op(Ph, detJ)
    logger.debug("Jacobian = %s" % (detJ))
    logger.debug("Numeric integral (area) = %s" % (A))

    logger.info("Translation+Shear+Scaling...")
    coords = SingleTriElementGenerator.tri6_3()  # <----------
    Ph = ev(Ones.F, coords)
    CGMaps.det_jacobian(gk, coords, detJ)
    A = op(Ph, detJ)
    logger.debug("Jacobian = %s" % (detJ))
    logger.debug("Numeric integral (area) = %s" % (A))

    logger.info("Nonlinear Deformation+Translation+Scaling...")
    coords = SingleTriElementGenerator.tri6_4a()  # <----------
    Ph = ev(Ones.F, coords)
    CGMaps.det_jacobian(gk, coords, detJ)
    A = op(Ph, detJ)
    logger.debug("Jacobian tri A = %s" % (detJ))
    #
    coords = SingleTriElementGenerator.tri6_4b()  # <----------
    Ph = ev(Ones.F, coords)
    CGMaps.det_jacobian(gk, coords, detJ)
    A += op(Ph, detJ)
    logger.debug("Jacobian tri B = %s" % (detJ))
    logger.debug("Numeric integral (area) = %s" % (A))

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
