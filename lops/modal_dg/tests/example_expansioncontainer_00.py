#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_expansioncontainer_00.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.modal_dg.entity import ExpansionEntityFactory
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.entity import GlobalExpansionContainer
from manticore.geom.standard_geometries import (
    StandardGeometry, geom, number_of_nodes)
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldRole, FieldType


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example ExpansionContainer 00")

    c = GlobalExpansionContainer()

    logger.debug("Empty container C: size = %s" % c.size())

    e1 = ExpansionEntityFactory.make(StandardGeometry.QUAD16)
    e2 = ExpansionEntityFactory.make(StandardGeometry.TRI10)
    k  = ExpansionKeyFactory.make(5, 10)
    
    e1.role = EntityRole.PHYSICAL
    e1.ID = 12345

    e2.role = EntityRole.PHYSICAL    
    e2.ID = 67890

    logger.info("Inserting elements...")
    e = c.insert(e1)
    logger.debug("Inserted element ID = %s" % e.ID)
    e = c.insert(e2)
    logger.debug("Inserted element ID = %s" % e.ID)
    e = ExpansionEntityFactory.make(StandardGeometry.TRI15)
    e.ID = 1122
    e = c.insert(e)
    logger.debug("Inserted element ID = %s" % e.ID)
    e = ExpansionEntityFactory.make(StandardGeometry.TRI15)
    e.ID = 1123
    e = c.insert(e)
    logger.debug("Inserted element ID = %s" % e.ID)
    logger.debug("Container C: size = %s" % c.size())
    logger.info("Searching elements...")
    logger.debug("Found element ID = %s" % c.find_by_id(67890).ID)
    logger.debug("Found element ID = %s" % c.find_by_id(1122).ID)
    logger.debug("Found element ID = %s" % c.find_by_id(1123).ID)
    logger.debug("Found element ID = %s" % c.find_by_id(12345).ID)

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
