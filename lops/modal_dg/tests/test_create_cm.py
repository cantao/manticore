#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#

import logging
import argparse
from manticore.mesh.mdf_mesh_parser import MdfMeshParser
from manticore.mesh.umesh import (
    double_link_region_to_edge, double_link_region_to_region
    )
from manticore.lops.modal_dg.create_computemesh import create_computemesh


if __name__ == '__main__':
    # Command line parsing
    parser = argparse.ArgumentParser()

    parser.add_argument('mdffile', type=str, help='MDF file to test')

    args = parser.parse_args()

    try:
        parser = MdfMeshParser(args.mdffile)

        M = parser.parse()

        double_link_region_to_edge(M)
        double_link_region_to_region(M)

        CM = create_computemesh(M)

        logging.debug(CM)

    except Exception as e:
        logging.critical(type(e))
        logging.critical(e)

# -- test_create_cm.py --------------------------------------------------------
