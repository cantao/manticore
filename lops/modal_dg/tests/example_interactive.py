#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
import manticore
import numpy as np
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.gauss import GaussKey, JacobiGaussQuadratures
from manticore.lops.modal_dg.gauss import standard_nip_rule
from manticore.lops.modal_dg.gauss import interior_nip_rule
from manticore.lops.modal_dg.gauss import CollocationDiffMat
from manticore.lops.modal_dg.gauss_strm import StreamlinedGaussWeights
from manticore.lops.modal_dg.dgtypes import PrincipalFunctionType
from manticore.lops.modal_dg.prinfunc import PrincipalFunctionKey
from manticore.lops.modal_dg.prinfunc import PrincipalFunctionTable
from manticore.lops.modal_dg.prinfunc import PrincipalFunctions
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.dgtypes import factory_class_region
from manticore.lops.modal_dg.dgtypes import StdRegionType
import manticore.lops.modal_dg.stdmixins2d as _2d_mx
import manticore.lops.modal_dg.stdops2d as _2d_std
from manticore.lops.modal_dg.geomfactors import FaceDocking
from manticore.lops.modal_dg.dgtypes import FacePrincipalFunctionType
from manticore.lops.modal_dg.faceprinfunc import FacePrincipalFunctionKey
from manticore.lops.modal_dg.faceprinfunc import FacePrincipalFunctions

p_order = 20

integrand_order = 42

nip = interior_nip_rule(GaussPointsType.GaussLegendre, integrand_order)
face_nip = standard_nip_rule(GaussPointsType.GaussLegendre, integrand_order)

gaussWIP = JacobiGaussQuadratures()

keyA = PrincipalFunctionKey(
    PrincipalFunctionType.OrthoPrincFuncA,
    order=p_order,
    gauss_key=GaussKey(GaussPointsType.GaussLegendre,
                       standard_nip_rule(GaussPointsType.GaussLegendre,
                                         integrand_order)))

tableA = PrincipalFunctionTable(keyA, gaussWIP)

keyB = PrincipalFunctionKey(
    PrincipalFunctionType.OrthoPrincFuncB,
    order=p_order,
    gauss_key=GaussKey(GaussPointsType.GaussLegendre,
                       standard_nip_rule(GaussPointsType.GaussLegendre,
                                         integrand_order)))

tableB = PrincipalFunctionTable(keyB, gaussWIP)

tables = PrincipalFunctions(gaussWIP)

tA = tables(keyA)
tB = tables(keyB)

keyC = PrincipalFunctionKey(
    PrincipalFunctionType.OrthoPrincFuncB,
    order=p_order,
    gauss_key=GaussKey(GaussPointsType.GaussJacobi10,
                       standard_nip_rule(GaussPointsType.GaussJacobi10,
                                         integrand_order)))

tC = tables(keyC)

# Testing mixin factories
diff_mat = CollocationDiffMat(gaussWIP)
strm_gaussW = StreamlinedGaussWeights(gaussWIP)

key = ExpansionKey(p_order, nip)
key.set_gauss_quadrature(gaussWIP)
key.set_principal_functions(tables)
key.set_strm_gauss_quadrature(strm_gaussW)
key.set_diff_matrix(diff_mat)

RegionQuad = factory_class_region(StdRegionType.DG_Quadrangle)
RegionTria = factory_class_region(StdRegionType.DG_Triangle)

nip_quad = _2d_mx.factory_number_of_ip(RegionQuad)(key)
nip_tria = _2d_mx.factory_number_of_ip(RegionTria)(key)

nm_quad = _2d_mx.factory_number_of_modes(
    _2d_mx.factory_number_of_ip(RegionQuad))(key)
nm_tria = _2d_mx.factory_number_of_modes(
    _2d_mx.factory_number_of_ip(RegionTria))(key)

wp_quad = _2d_mx.factory_wp_mixin(
    _2d_mx.factory_number_of_modes(_2d_mx.factory_number_of_ip(RegionQuad)))(
        key)

wp_tria = _2d_mx.factory_wp_mixin(
    _2d_mx.factory_number_of_modes(_2d_mx.factory_number_of_ip(RegionTria)))(
        key)

# pfs_quad = _2d_mx.factory_princ_func_mixin(
#     _2d_mx.factory_number_of_modes(
#         _2d_mx.factory_number_of_ip(RegionQuad)))(key)

# Forma mais templetica (apesar de verborragico) de criar os mixins
# (criacao indexada pelo tipo diretamente e nao por uma classe)
pfs_quad = _2d_mx.factory_princ_func_mixin(
    _2d_mx.factory_number_of_modes(
        _2d_mx.factory_number_of_ip(
            factory_class_region(StdRegionType.DG_Quadrangle))))(key)

pfs_tria = _2d_mx.factory_princ_func_mixin(
    _2d_mx.factory_number_of_modes(_2d_mx.factory_number_of_ip(RegionTria)))(
        key)

stdback_quad = _2d_std.factory_stdbackward2d(RegionQuad)(key)
stdback_tria = _2d_std.factory_stdbackward2d(RegionTria)(key)

actV = StdRegionType.DG_Quadrangle
actF = 0
pasV = StdRegionType.DG_Quadrangle
pasF = 3

p1 = key.p_on_face(actV, actF, 1)
p2 = key.p_on_face(actV, actF, 2)

docker = FaceDocking()
docker.setup(actV, actF, pasV, pasF)
p1p, p2p = docker.eval(p1, p2)

k = FacePrincipalFunctionKey(
    StdRegionType.DG_Quadrangle,
    0,
    StdRegionType.DG_Quadrangle,
    1,
    order=4,
    nip=5)

FPFuncs = FacePrincipalFunctions(gaussWIP)

t = FPFuncs(k)
