#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
import manticore.services.const as const
from manticore.services.mconsts import CTEs
from manticore.lops.modal_dg.phyops2d import factory_physdifferentiator2d
from manticore.lops.modal_dg.phyops2d import factory_physdivergent2d
from manticore.lops.modal_dg.phyops2d import factory_physevaluator2d
from manticore.lops.modal_dg.tests.test_helpers import Polynomial
from manticore.lops.modal_dg.tests.test_helpers import Polynomial_Dx
from manticore.lops.modal_dg.tests.test_helpers import Polynomial_Dy


def doPolyDerivative(classRegion, e, a, b):

    k = e.key

    Ntot = k.n2d

    # Evaluation operator to create physical values on integration points
    ev = factory_physevaluator2d(classRegion)(k)

    P  = Polynomial(a, b)
    Px = Polynomial_Dx(a, b)
    Py = Polynomial_Dy(a, b)

    # Arrays of physical values on integration points
    Ph  = ev(P,  e.get_coeff(), e.geom_type, e.vmap)
    Phx = ev(Px, e.get_coeff(), e.geom_type, e.vmap)
    Phy = ev(Py, e.get_coeff(), e.geom_type, e.vmap)

    # Arrays for numerical derivatives
    dPhdx = np.zeros(Ntot)
    dPhdy = np.zeros(Ntot)

    op = factory_physdifferentiator2d(classRegion)(k)

    op(e.get_inv_jacobian_matrix(), Ph, dPhdx, dPhdy)

    checkx = np.isnan(dPhdx)
    checky = np.isnan(dPhdy)
    assert not checkx.all()
    assert not checky.all()

    npx = np.linalg.norm(Phx)
    npy = np.linalg.norm(Phy)

    if npx < const.INF:
        npx = 1.

    if npy < const.INF:
        npy = 1.

    ex = np.linalg.norm(dPhdx - Phx)/npx
    ey = np.linalg.norm(dPhdy - Phy)/npy

    return ex, ey


def doDivergent(classRegion, function, e):

    k = e.key

    Ntot = k.n2d

    # Evaluation operator to create physical values on integration points
    ev = factory_physevaluator2d(classRegion)(k, e.geom_type, e.vmap)

    Fxi_1 = ev(function.Fx,  e.get_coeff())
    Fxi_2 = ev(function.Fy,  e.get_coeff())
    Div   = ev(function.Fdiv,e.get_coeff())

    nDiv = np.zeros(Ntot)

    op = factory_physdivergent2d(classRegion)(k)

    op(e.get_inv_jacobian_matrix(), Fxi_1, Fxi_2, nDiv)

    return np.linalg.norm(nDiv - Div)
