#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Roots of Jacobi polynomials with parameters alpha and beta
#
#
# by
#    Renato Cantao (Python version)
#
from math import cos, pi, fabs
from sys import float_info
from manticore.lops.modal_dg.tests.jacobi_p import jacobi_p
from manticore.lops.modal_dg.tests.djacobi_p import djacobi_p
import numpy as np


def jacobi_roots(m, alpha=0.0, beta=0.0):
    """Polynomial deflation to find the zeros of a Jacobi polynomial."""
    if m == 0:
        raise AssertionError("0-degree Jacobi polynomial detected!")
    elif m == 1:
        return np.array([(beta - alpha) / (alpha + beta + 2.0)])
    else:
        x = np.zeros(m)  # Allocating vector for quadrature points

        # Newton-Raphson algorithm with polynomial deflation
        for k in range(0, m):
            # Initial guess: roots of the Chebyshev polynomial of order m
            r = -cos(pi * (2 * k + 1) / (2 * m))
            if k > 0:
                r = 0.5 * (r + x[k - 1])
                #print("r = %s, x[%i] = %s" % (r, k, x[k]))
            delta = 1.0

            while fabs(delta) > float_info.epsilon:
                s = np.sum(1.0 / (r - x[range(0, k)]))
                Pm = jacobi_p(r, m, alpha, beta)
                DPm = djacobi_p(r, m, alpha, beta)
                delta = -Pm / (DPm - Pm * s)
                r = r + delta

            x[k] = r

    return x


#-- jacobi_roots.py ------------------------------------------------------------
