#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_stdbackward_01.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.prinfunc import PrincipalFunctions
from manticore.lops.modal_dg.faceprinfunc import FacePrincipalFunctions
from manticore.lops.modal_dg.gauss import CollocationDiffMat
from manticore.lops.modal_dg.gauss import JacobiGaussQuadratures
from manticore.lops.modal_dg.gauss_strm import StreamlinedGaussWeights
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.dgtypes import factory_class_region
from manticore.lops.modal_dg.stdops2d import factory_stdbackward2d


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example StdBackward 01")

    gaussWIP = JacobiGaussQuadratures()
    PFuncs = PrincipalFunctions(gaussWIP)
    FPFuncs = FacePrincipalFunctions(gaussWIP)
    CDiffMat = CollocationDiffMat(gaussWIP)
    SGaussW = StreamlinedGaussWeights(gaussWIP)

    M = 2
    N = 2 * M + 1

    k = ExpansionKey(M, N)

    # ExpansionKey needs access to all these objects (not necessarily
    # for all operations but I going to the safe side here):
    k.set_gauss_quadrature(gaussWIP)
    k.set_strm_gauss_quadrature(SGaussW)
    k.set_diff_matrix(CDiffMat)
    k.set_principal_functions(PFuncs)
    k.set_face_principal_functions(FPFuncs)

    S = ExpansionSize.get(StdRegionType.DG_Triangle, k.order)

    C = np.zeros(S)
    Ph = np.zeros(k.n2d)

    #C.fill(1.0)
    C[0]=1.0

    op = factory_stdbackward2d(
        factory_class_region(StdRegionType.DG_Triangle))(k)

    op.at_ips(C, Ph)

    logger.debug("\nCoefficients = \n%s" % (C))

    logger.debug("\nPhysical = \n%s" % (Ph))

    logger.info("Example ended!")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='StdBackward example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
