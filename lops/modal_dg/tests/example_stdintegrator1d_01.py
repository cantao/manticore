#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/lops/modal_dg/tests/example_stdintegrator1d_01.py -ll DEBUG
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.prinfunc import PrincipalFunctions
from manticore.lops.modal_dg.faceprinfunc import FacePrincipalFunctions
from manticore.lops.modal_dg.gauss import CollocationDiffMat
from manticore.lops.modal_dg.gauss import JacobiGaussQuadratures
from manticore.lops.modal_dg.gauss import interior_nip_rule
from manticore.lops.modal_dg.gauss_strm import StreamlinedGaussWeights
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.dgtypes import factory_class_region
from manticore.lops.modal_dg.phyops1d import factory_physintegrator1d
import numpy as np
from math import sqrt


def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example StdIntegrator 1D 01")

    gaussWIP = JacobiGaussQuadratures()
    PFuncs   = PrincipalFunctions(gaussWIP)
    FPFuncs  = FacePrincipalFunctions(gaussWIP)
    CDiffMat = CollocationDiffMat(gaussWIP)
    SGaussW  = StreamlinedGaussWeights(gaussWIP)

    M = 5
    N = 2 * M

    k = ExpansionKey(M, interior_nip_rule(GaussPointsType.GaussLegendre, N))

    # ExpansionKey needs access to all these objects (not necessarily
    # for all operations but I going to the safe side here):
    k.set_gauss_quadrature(gaussWIP)
    k.set_strm_gauss_quadrature(SGaussW)
    k.set_diff_matrix(CDiffMat)
    k.set_principal_functions(PFuncs)
    k.set_face_principal_functions(FPFuncs)

    region = StdRegionType.DG_Triangle
    op = factory_physintegrator1d(factory_class_region(region))(k)
    
    # Face 0 
    Ph = np.ones(k.n1d_on_face(region, face_id=0, coord=1))
    J = np.ones(k.n1d_on_face(region, face_id=0, coord=1))
    L = op(0, Ph, J)
    logger.debug("Length face 0 = %s" % (L))

    # Face 1 
    Ph = np.ones(k.n1d_on_face(region, face_id=1, coord=2))
    J = np.ones(k.n1d_on_face(region, face_id=1, coord=2))
    L = op(1, Ph, J)
    logger.debug("Length face 1 = %s" % (L))

    # Face 2 
    Ph = np.ones(k.n1d_on_face(region, face_id=2, coord=2))
    J = sqrt(2.)*np.ones(k.n1d_on_face(region, face_id=2, coord=2))
    L = op(2, Ph, J)
    logger.debug("Length face 2 = %s" % (L))  

    logger.info("Example ended!")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '%(asctime)s: %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': args.loglevel,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': args.loglevel,
            },
        }
    }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
