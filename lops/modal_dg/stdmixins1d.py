#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Mixins for 1D operations on standard regions.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

from manticore.lops.modal_dg.dgtypes import RegionInfo


def factory_number_of_ip(classRegion):
    class NumberOfIp(classRegion):
        __slots__ = ['n_coord', 'n1d']

        def __init__(self, key):
            classRegion.__init__(self)
            self.__configure(key)

        def set_expansion_key(self, key):
            self.__configure(key)

        def __configure(self, key):
            sz = RegionInfo.number_of_faces(self.G)
            # assert sz == len(keys)

            self.n_coord = [None, None]

            self.n_coord[0] = [
                key.n1d_on_face(self.G, ff, 1) for ff in range(sz)
            ]
            self.n_coord[1] = [
                key.n1d_on_face(self.G, ff, 2) for ff in range(sz)
            ]

            self.n1d = [key.n1d for ff in range(sz)]

    # end of factory_number_of_ip.NumberOfIp

    return NumberOfIp


def factory_wp_mixin(classBase):
    class WPMixin(classBase):
        __slots__ = ['W1', 'P1', 'W2', 'P2', 'W']

        def __init__(self, key):
            classBase.__init__(self, key)
            self.__configure(key)

        def set_expansion_key(self, key):
            classBase.set_expansion_key(self, key)
            self.__configure(key)

        def __configure(self, key):
            sz = RegionInfo.number_of_faces(self.G)

            self.W1 = [None for ff in range(sz)]
            self.P1 = [None for ff in range(sz)]
            self.W2 = [None for ff in range(sz)]
            self.P2 = [None for ff in range(sz)]
            self.W = [None for ff in range(sz)]

            for ff in range(sz):
                self.W1[ff], self.P1[ff] = key.wp_on_face(self.G, ff, 1)
                self.W2[ff], self.P2[ff] = key.wp_on_face(self.G, ff, 2)
                self.W[ff] = self.W1[ff] * self.W2[ff]

    # end of factory_wp_mixin.WPMixin

    return WPMixin


def factory_princ_func_mixin(classBase):
    class PrincipalFuncMixin(classBase):
        __slots__ = ['PF1', 'PF2', 'FPFs']

        def __init__(self, key):
            classBase.__init__(self, key)
            self.__configure(key)

        def set_expansion_key(self, key):
            classBase.set_expansion_key(self, key)
            self.__configure(key)

        def __configure(self, key):
            sz = RegionInfo.number_of_faces(self.G)

            self.PF1 = [None for ff in range(sz)]
            self.PF2 = [None for ff in range(sz)]

            for ff in range(sz):
                k1 = key.psi_on_face(self.G, ff, 1)
                k2 = key.psi_on_face(self.G, ff, 2)

                self.PF1[ff] = key.prin_funcs(k1)
                self.PF2[ff] = key.prin_funcs(k2)

            self.FPFs = key.face_prin_funcs

        # end of factory_princ_func_mixin.PrincipalFuncMixin

    return PrincipalFuncMixin
