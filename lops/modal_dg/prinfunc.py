#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Principal functions accorting to Karniadakis & Sherwin 2nd ed,
# Chapter 3, p.101.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from math import sqrt, pow
from numbers import Number
from collections import namedtuple
from manticore.lops.modal_dg.jacobi import JacobiPolynomial
from manticore.lops.modal_dg.gauss import GaussKey
from manticore.lops.modal_dg.dgtypes import PrincipalFunctionType

# """Key indexer to principal functions storage.
#
#     Notes:
#        * We are assuming that P == Q.
#
#     Attributes:
#         type (PrincipalFunctionType)
#         order (int)
#         gauss_key (GaussKey)
#
# """
PrincipalFunctionKey = namedtuple('PrincipalFunctionKey',
                                  ['type', 'order', 'gauss_key'])


class psi_a_eval_policy:
    """Evaluation of $\tilde{\psi}^a_p(xi)$

    Notes:
        * Original implementation was
            @code
                def __call__(self,x):
                    return self.J.eval(x)
            @endcode
        * Implementation now reflects normalization made by S. Sherwin.

    """

    __slots__ = ['J', 'p']

    def __init__(self, p):
        self.J = JacobiPolynomial(p, 0.0, 0.0)
        self.p = p

    def configure(self, p):
        self.J.configure(p, 0.0, 0.0)
        self.p = p

    def __call__(self, x):
        return sqrt(0.5 * (2.0 * self.p + 1.0)) * self.J.eval(x)


## end of manticore.lops.modal_dg.prinfunc.psi_a_eval_policy


class dpsi_a_eval_policy(psi_a_eval_policy):
    """Evaluation of $d/dxi \tilde{\psi}^a_p(xi)$

    """
    __slots__ = []

    def __init__(self, p):
        psi_a_eval_policy.__init__(self, p)

    def __call__(self, x):
        return sqrt(0.5 * (2.0 * self.p + 1.0)) * self.J.deval(x)


## end of manticore.lops.modal_dg.prinfunc.dpsi_a_eval_policy


class psi_b_eval_policy:
    """Evaluation of $\tilde{\psi}^b_{pq}(xi)$

    Notes:
        * Original implementation was
            @code
                def __call__(self,x):
                    return pow( 0.5*(1.0-x), self.p ) * self.J.eval(x)
            @endcode
        * Implementation now reflects normalization made by S. Sherwin.
        * Orthonality is shown in K&S chapter 3, p. 103-104

    """

    __slots__ = ['J', 'p', 'q']

    def __init__(self, p, q):
        self.J = JacobiPolynomial(q, 2.0 * p + 1.0, 0.0)
        self.p = p
        self.q = q

    def configure(self, p, q):
        self.J.configure(q, 2.0 * p + 1.0, 0.0)
        self.p = p
        self.q = q

    def __call__(self, x, out=None):
        p = self.p
        q = self.q

        if isinstance(x, Number):
            power = pow
        else:
            power = np.power

        if isinstance(out, np.ndarray):
            # out *= 0.0
            # out += 1.0
            out.fill(1.0)
            out -= x
            out *= 0.5
            power(out, p, out)
            out *= sqrt(p + q + 1.0)
            out *= self.J.eval(x)
            return out
        else:
            return sqrt(p + q + 1.0) * power(0.5 *
                                             (1.0 - x), p) * self.J.eval(x)


## end of manticore.lops.modal_dg.prinfunc.psi_b_eval_policy


class dpsi_b_eval_policy(psi_b_eval_policy):
    """Evaluation of $d/dxi \tilde{\psi}^b_{pq}(xi)$
    """

    __slots__ = []

    def __init__(self, p, q):
        psi_b_eval_policy.__init__(self, p, q)

    def __call__(self, x, out=None):
        p = self.p
        q = self.q
        J = self.J.eval
        dJ = self.J.deval

        if isinstance(x, Number):
            power = pow
        else:
            power = np.power

        if isinstance(out, np.ndarray):
            out *= 0.0
            out += 1.0
            out -= x  # out = 1.0-x
            #
            aux = J(x)
            aux *= -p
            aux /= out  # aux = -p*J(x)/(1.0-x)
            aux += dJ(x)  # aux = -p*J(x)/(1.0-x) + dJ(x)
            #
            out *= 0.5  # out = 0.5*(1.0-x)
            power(out, p, out)  # out = power(0.5*(1.0-x),p)
            out *= sqrt(
                p + q + 1.0)  # out = sqrt(p+q+1.0)*power(0.5*(1.0-x),p)
            #
            out *= aux
        else:
            return sqrt(p + q + 1.0) * power(0.5 * (1.0 - x), p) * (
                -p * J(x) / (1.0 - x) + dJ(x))


## end of manticore.lops.modal_dg.prinfunc.dpsi_b_eval_policy


class PrincipalFunctionTable:
    """Table to record values of a principal function over a DG region.

    Notes:

        * The PrincipalFunctionsTable class is organized as a
            list(np.array) effectivelly emulating a matrix. The size
            of the list corresponds to the number of integration
            points, while the np.array size is related to the number
            of modes stored on the table.

    """

    def __init__(self, pfunc_key, gauss_quad):
        """Constructor for PrincipalFunctionTable objects.

        Attributes:

            table (list(np.array)): table of Principal Functions,
                stored by order,
                table[ii][jj] = ii-th integration point, jj-th mode

            tableT (list(np.array)): table of Principal Functions,
                stored by integration point,
                tableT[jj][ii] = jj-th mode, ii-th integration point

            table_d (list(np.array)): derivatives on $\eta_{1,2}$

            tableT_d (list(np.array)): transposed derivatives

            gauss_quad (JacobiGaussQuadratures): weights and
                integration points.

        Args:

            pfunc_key (PrincipalFunctionKey): defines type of
                principal function, p_order, type of Gauss-Jacobi
                integration, order of the integrand this function will
                be applied.

            gauss_quad (JacobiGaussQuadratures): weights and
                integration points.

        """
        self.gauss_quad = gauss_quad

        if pfunc_key.type == PrincipalFunctionType.OrthoPrincFuncA:
            self.__create_tables_psi_a(pfunc_key)
        elif pfunc_key.type == PrincipalFunctionType.OrthoPrincFuncB:
            self.__create_tables_psi_b(pfunc_key)
        else:
            raise AssertionError("Incorrect initialization!")

    def row(self, ii):
        """Access the ii-th table row: values of all modes at the ii-th integration point

        Args:

            ii (int): row index, [0, number_of_integration_points[

        Returns:

            np.array: the ii-th table row

        """
        return self.table[ii]

    def d_row(self, ii):
        """Access the ii-th derivative table row: values of all modes derivatives at the ii-th integration point

        Args:

            ii (int): row index, [0, number_of_integration_points[

        Returns:

            np.array: the ii-th derivative table row

        """
        return self.table_d[ii]

    def column(self, jj):
        """Access the jj-th table column. Values of the jj-th mode at all the integration points

        Args:

            jj (int): column index, [0, number_of_modes[

        Returns:

            np.array: the jj-th table column

        """
        return self.tableT[jj]

    def d_column(self, jj):
        """Access the jj-th derivative table column. Values of the jj-th mode derivative at all the integration points

        Args:

            jj (int): column index, [0, number_of_modes[

        Returns:

            np.array: the jj-th derivative table column

        """
        return self.tableT_d[jj]

    @property
    def nip(self):
        return len(self.table)

    @property
    def nmodes(self):
        return self.nModes

    def __init_tables(self, nip, nm):
        """Table allocation.

        Args:
            nip (int): Number of integration points

            nm (int): Number of modes (e.g. for psi_a, nm = order+1,
                that is, 0 to order modes)

        """
        nip = int(nip)
        nm = int(nm)

        self.nModes = nm

        self.table = [np.zeros(nm) for i in range(nip)]
        self.table_d = [np.zeros(nm) for i in range(nip)]
        self.tableT = [np.zeros(nip) for i in range(nm)]
        self.tableT_d = [np.zeros(nip) for i in range(nm)]

    def __create_tables_psi_a(self, pfunc_key):

        points = self.gauss_quad.ips(pfunc_key.gauss_key)

        p_order = pfunc_key.order
        nip = points.shape[0]

        # Number of modes = p-order plus one
        self.__init_tables(nip, p_order + 1)

        psiA = [psi_a_eval_policy(pp) for pp in range(p_order + 1)]
        dpsiA = [dpsi_a_eval_policy(pp) for pp in range(p_order + 1)]

        for pp in range(p_order + 1):
            self.tableT[pp] = psiA[pp](points)
            self.tableT_d[pp] = dpsiA[pp](points)

            for ii in range(nip):
                self.table[ii][pp] = self.tableT[pp][ii]
                self.table_d[ii][pp] = self.tableT_d[pp][ii]

    def __create_tables_psi_b(self, pfunc_key):

        points = self.gauss_quad.ips(pfunc_key.gauss_key)

        p_order = pfunc_key.order
        nip = points.shape[0]

        # Number of modes
        self.__init_tables(nip, ((p_order + 1) * (p_order + 2)) / 2)

        mode = 0
        oP1 = p_order + 1

        for pp in range(oP1):
            for qq in range(oP1 - pp):
                psiB = psi_b_eval_policy(pp, qq)
                dpsiB = dpsi_b_eval_policy(pp, qq)

                psiB(points, self.tableT[mode])
                dpsiB(points, self.tableT_d[mode])

                for ii in range(nip):
                    self.table[ii][mode] = self.tableT[mode][ii]
                    self.table_d[ii][mode] = self.tableT_d[mode][ii]

                mode += 1

    def __str__(self):
        nip = self.nip
        nm = self.nmodes

        msg = "table[(%s points,%s modes)]" % (nip, nm)

        msg += "\nTable ="

        for ii in range(nip):
            msg += "\n%s" % self.table[ii]

        msg += "\nTable D ="

        for ii in range(nip):
            msg += "\n%s" % self.table_d[ii]

        return msg

    def __repr__(self):
        msg = "<%s object, id:%s, table:(%s points,%s modes)>" % (
            self.__class__, id(self), self.nip, self.nmodes)

        return msg


## end of manticore.lops.modal_dg.prinfunc.PrincipalFunctionTable


class PrincipalFunctions:
    """Basically a hash table of PrincipalFunctionTable objects indexed by
PrincipalFunctionKey objects.

    Notes:

        * Each table entry is created when first called.

        * All PrincipalFunctionTable objects of this table are
                associated to the same JacobiGaussQuadratures object.

    """

    def __init__(self, gauss_quad):
        """Constructor for PrincipalFunctions objects.

        Attributes:

            gauss_quad (JacobiGaussQuadratures): weights and
                integration points used to build
                PrincipalFunctionTable objects.

            tables (dict(PrincipalFunctionTable)): actual hash table
                of PrincipalFunctionTable objects indexed as:
                [PrincipalFunctionKey]: PrincipalFunctionTable

        Args:

            gauss_quad (JacobiGaussQuadratures): weights and
                integration points.

        """
        self.gauss_quad = gauss_quad

        # dict {PrincipalFunctionKey: PrincipalFunctionTable}
        self.tables = {}

    def __call__(self, key):
        """Returns a PrincipalFunctionTable object built according to key.

        Args:

            key (PrincipalFunctionKey)

        Returns:

            PrincipalFunctionTable

        Notes:

           * Each table entry is created when first
             called. Consecutive calls with the same argument retrive
             an already computed PrincipalFunctionTable object from
             the hash table.

        """

        assert type(key) is PrincipalFunctionKey

        if key not in self.tables:
            self.tables[key] = PrincipalFunctionTable(key, self.gauss_quad)

        return self.tables[key]

    def __str__(self):
        n = len(self.tables)

        msg = ""

        for key in self.tables:
            msg += "[%s: %s]\n" % (key, self.tables[key])

        msg += "[%s tables]" % (n)

        return msg

    def __repr__(self):
        msg = "<%s object, id:%s, %s tables>" % (self.__class__, id(self),
                                                 len(self.tables))

        return msg


## end of manticore.lops.modal_dg.prinfunc.PrincipalFunctions
