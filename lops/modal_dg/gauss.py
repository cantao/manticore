#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Gaussian quadrature based on Jacobi polynomials
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from math import gamma, ceil, floor
from array import array
from collections import namedtuple
from manticore.lops.modal_dg.jacobi import JacobiPolynomial
from manticore.lops.modal_dg.dgtypes import GaussPointsType


#
# Generic rules for computing the number of integration
# points. Eventually, each implementation of specific EDP solution may
# define its rule.
#
def standard_nip_rule(gptype, order):
    """Standard rule for the computation of the number of 'gptype' Gauss
    integration points required for the exact 1D-integration of a
    polynomial integrand of a given 'order'.

    Args:
        gptype (GaussPointsType): type of Gauss points.
        order (int): order of the compound polynomial to be integrated.

    Returns:
        int: number of integration points

    """
    if ((gptype == GaussPointsType.GaussLegendre) or
        (gptype == GaussPointsType.GaussJacobi10) or
        (gptype == GaussPointsType.GaussJacobi20)):

        return int(ceil(0.5 * (order + 1)))

    elif (gptype == GaussPointsType.GaussLobatto):
        return int(ceil(0.5 * (order + 3)))  # ceil is correct for modal DG

    elif ((gptype == GaussPointsType.NegativeOne) or
          (gptype == GaussPointsType.PositiveOne)):

        return 1

    else:
        return None


## end of manticore.lops.modal_dg.gauss.standard_nip_rule


def interior_nip_rule(gptype, order):
    """Rule for the computation of the number of 'gptype' Gauss
    integration points required *in each direction* for the exact
    2D-integration of a 2D-polynomial integrand of a given 'order'.

    Args:
        gptype (GaussPointsType): type of Gauss points.
        order (int): order of the compound polynomial to be integrated.

    Returns:
        int: number of integration points

    """
    if order == 0:
        order += 1  # Just a hack to avoid erros when computing Jacobi roots.
    elif order < 0:
        raise AssertionError("Negative order not supported!")

    if ((gptype == GaussPointsType.GaussLegendre) or
        (gptype == GaussPointsType.GaussJacobi10) or
        (gptype == GaussPointsType.GaussJacobi20)):

        return int(floor(0.5 * (order + 1)))

    elif (gptype == GaussPointsType.GaussLobatto):
        return int(floor(0.5 * (order + 3)))

    elif ((gptype == GaussPointsType.NegativeOne) or
          (gptype == GaussPointsType.PositiveOne)):

        return 1

    else:
        return None


## end of manticore.lops.modal_dg.gauss.interior_nip_rule

#
# """Key indexer to Gauss data structures.
#
#     Attributes:
#         gptype (GaussPointsType): type of Gauss points.
#         nip (int): number of integration points.
# """
GaussKey = namedtuple('GaussKey', ['type', 'nip'])


class JacobiGaussQuadratures:
    """Points and weights for one dimensional Gaussian quadrature based on
    Jacobi polynomials.

    Based on previous works of Cantão! (rfcantao@gmail.com)

    Notes:
         * References: K&S, p. 144, 145, 596.
    """

    def __init__(self):
        """ Constructor for a JacobiGaussQuadrature object.

        Attributes:

            wi (dict(dict(np.array))): [GaussKey]:np.array,
               storage for integration weights.

            xi (dict(dict(np.array))): [GaussKey]:np.array,
               storage for one-dimensional integration points on the [-1,1]
               interval.

        """

        self.wi = {}  # [GaussKey]: np.array
        self.xi = {}  # [GaussKey]: np.array

    def __call__(self, gauss_key):
        """Returns arrays of weights and one dimensional Gauss integration
        points of 'gauss_key.type'.

        Args:
            gauss_key (GaussKey)

        Returns:
            (np.array, np.array): weights, one-dimensional integration
                points on the [-1,1] interval. If an incorrect
                'gauss_key.type' is given, returns (None, None).

        Notes:

            * 'gauss_key.nip' points and weights of a given
              'gauss_key.type' are computed only at the first call and
              internally stored for future calls.

            * One JacobiGaussQuadrature object can store all different
              types of points (according to 'gauss_key.type' and
              'gauss_key.nip').

        """
        assert type(gauss_key) is GaussKey

        if gauss_key not in self.wi:
            self.__compute(gauss_key)

        return self.wi[gauss_key], self.xi[gauss_key]

    def ips(self, gauss_key):
        """Returns arrays of one dimensional Gauss integration
        points of 'gauss_key.type'.

        Args:
            gauss_key (GaussKey)

        Returns:
            np.array: one-dimensional integration
                points on the [-1,1] interval. If an incorrect
                'gauss_key.type' is given, returns None.

        Notes:

            * 'gauss_key.nip' points and weights of a given
              'gauss_key.type' are computed only at the first call and
              internally stored for future calls.

            * One JacobiGaussQuadrature object can store all different
              types of points (according to 'gauss_key.type' and
              'gauss_key.nip').

        """
        assert type(gauss_key) is GaussKey

        if gauss_key not in self.wi:
            self.__compute(gauss_key)

        return self.xi[gauss_key]

    def weights(self, gauss_key):
        """Returns arrays of weights for one dimensional Gauss integration
        points of 'gauss_key.type'.

        Args:

            gauss_key (GaussKey)

        Returns:

            np.array: weights for one-dimensional integration on
                the [-1,1] interval. If an incorrect 'gauss_key.type' is
                given, returns None.

        Notes:

            * 'gauss_key.nip' points and weights of a given
              'gauss_key.type' are computed only at the first call and
              internally stored for future calls.

            * One JacobiGaussQuadrature object can store all different
              types of points (according to 'gauss_key.type' and
              'gauss_key.nip').

        """
        assert type(gauss_key) is GaussKey

        if gauss_key not in self.wi:
            self.__compute(gauss_key)

        return self.wi[gauss_key]

    def __compute(self, gauss_key):
        """Computes 'nip' weights and integration points of 'gptype' type and
        stores both arrays as class's attributes.

        Args:
            gptype (GaussPointsType): type of Gauss points.
            nip (int): number of integration points

        """
        gptype = gauss_key.type
        nip = gauss_key.nip

        switcher_alpha = {
            GaussPointsType.GaussLegendre: 0.0,
            GaussPointsType.GaussJacobi10: 1.0,
            GaussPointsType.GaussJacobi20: 2.0
        }

        # Default: sets parameters to obtain quadrature points from Legendre
        # polynomials as Legendre == Jacobi(alpha,beta) with alpha = beta = 0
        alpha = switcher_alpha.get(gptype, 0.0)
        beta = 0.0

        if ((gptype == GaussPointsType.GaussLegendre) or
            (gptype == GaussPointsType.GaussJacobi10) or
            (gptype == GaussPointsType.GaussJacobi20)):

            jp = JacobiPolynomial(nip, alpha, beta)

            # Quadrature points:
            self.xi[gauss_key] = jp.roots()
            xi = self.xi[gauss_key]  # proxy

            # Weights:

            # Note the simplification of weights computation from the
            # original formulae at K&S, appendix B, p. 596, if
            # beta=0.0 (as set above)
            #
            #     1) All terms involving \Gamma function and factorial
            #         cancel each other.
            #
            #     2) If alpha=1.0, C1 = 2.0**(2.0), but the final
            #         weights are later divided by 2.0 for a tensorial
            #         integration (coordinates \eta_1,\eta_2) on a
            #         triangle (K&S, chapter 4, p. 144.). Considering
            #         this division here, C1 = 2.0. (alpha=1.0 and that
            #         division by 2.0 take into account the Jacobian
            #         of \eta_1,\eta_2 to \ksi_1,\ksi_2).
            #
            #     3) If alpha=2.0, C1 = 2.0**(4.0), but the final
            #         weights are later divided by 4.0 for a tensorial
            #         integration (coordinates \eta_1,\eta_2,\eta_3)
            #         on a tetrahedra (K&S, chapter 4, p. 145.). Considering
            #         this division here, C1 = 2.0. (alpha=2.0 and that
            #         division by 2.0 take into account the Jacobian
            #         of \eta_1,\eta_2,\eta_3 to \ksi_1,\ksi_2,\ksi_3).
            #
            #  Original formulas below are kept commented as a
            #  reminder that those simplifications were carefully
            #  observed ad revised. References and uses of
            #  GaussJacobi10 and GaussJacobi20 rules can then be made
            #  with the assumption that the weights are correct and do
            #  not need further adjusts.

            # C1
            # C1  = 2.0**(alpha+beta+1.0)
            # C1 *= gamma(alpha+nip+1.0)*gamma(beta+nip+1.0)
            C1 = 2.0

            # C2
            # C2 = gamma(nip+1.0)*gamma(alpha+beta+nip+1.0)*(1.0-xi**2 )
            #
            # For more efficient numpy in-place operations, large
            # expression must be broken in atomic operations to avoid
            # temporary allocations.
            C2 = xi**2
            C2 *= -1.0
            C2 += 1.0
            # C2 *= gamma(nip+1.0)*gamma(alpha+beta+nip+1.0)

            DPm = jp.deval(xi)

            # wi  =  C1*DPm**(-2)/C2
            self.wi[gauss_key] = DPm**(-2)
            self.wi[gauss_key] *= C1
            self.wi[gauss_key] /= C2

        elif (gptype == GaussPointsType.GaussLobatto):

            # Quadrature points:
            jp1 = JacobiPolynomial(nip - 2, alpha + 1.0, beta + 1.0)

            self.xi[gauss_key] = np.zeros(nip)
            xi = self.xi[gauss_key]  # proxy
            xi[0] = -1.0
            xi[1:-1] = jp1.roots()
            xi[-1] = 1.0

            # Weights:
            jp2 = JacobiPolynomial(nip - 1, alpha, beta)

            C1 = (2.0**(
                alpha + beta + 1.0)) * gamma(alpha + nip) * gamma(beta + nip)
            C2 = (nip - 1) * gamma(nip) * gamma(alpha + beta + nip + 1.0)

            Pm = jp2.eval(xi)

            # wi = C1*Pm**(-2)/C2
            self.wi[gauss_key] = Pm**(-2)
            self.wi[gauss_key] *= C1
            self.wi[gauss_key] /= C2

            self.wi[gauss_key][0] *= (beta + 1.0)
            self.wi[gauss_key][-1] *= (alpha + 1.0)

        elif gptype == GaussPointsType.NegativeOne:
            self.xi[gauss_key] = np.ones(1)
            self.xi[gauss_key] *= -1.0
            self.wi[gauss_key] = np.ones(1)

        elif gptype == GaussPointsType.PositiveOne:
            self.xi[gauss_key] = np.ones(1)
            self.wi[gauss_key] = np.ones(1)

        else:
            pass


## end of manticore.lops.modal_dg.gauss.JacobiGaussQuadratures


class CollocationDiffMat:
    """Matrix for collocation differentiation.

    Notes:

        * Karniadakis, G. E. and Sherwin, S. J., Spectral/hp Element
              Methods for Computational Fluid Dynamics, appendix C [1]

    """

    def __init__(self, gauss_quad):
        """Constructor for a CollocationDiffMat object.

        Attributes:

            gauss_quad (JacobiGaussQuadratures): reference to
                integration points and weights.

            D (dict(dict(np.array))): [GaussPointsType][nip]:np.array,
               storage of differentiation matrices.

        Args:
            gauss_quad (JacobiGaussQuadratures)

        """

        self.gauss_quad = gauss_quad

        self.D = {}  # [GaussKey]: np.array

    def __call__(self, gauss_key):
        """Returns the differentiation matrix D associated to 'gauss_key.type'
        and 'gauss_key.nip' number of integration points.

        Args:
            gauss_key (GaussKey)

        Returns:

            (np.array): matrix np.shape =(gauss_key.nip,gauss_key.nip).
                If an incorrect 'gauss_key.type' is given, returns None.

        Notes:

            * Each D[gauss_key.type][gauss_key.nip] is computed only
              at the first call and internally stored for future
              calls.

            * One CollocationDiffMat object can store all different
              types of differentiation matrices (according to
              gauss_key.type and gauss_key.nip).

        """
        assert type(gauss_key) is GaussKey

        if gauss_key not in self.D:
            self.__compute(gauss_key)

        return self.D[gauss_key]

    def __compute(self, gauss_key):
        """Computes computes the differentiation matrix associated to
        gauss_key.type and gauss_key.nip and stores that matrix as
        class's attribute.

        Args:
            gauss_key (GaussKey)

        """

        gptype = gauss_key.type
        nip = gauss_key.nip

        if nip == 0:
            return None

        p = self.gauss_quad.ips(gauss_key)
        c0 = array('d', [1.0, 0.5, 1.0])
        c1 = array('d', [0.0, 1.0, 1.0])
        c2 = array('d', [1.0, 3.0, 2.0])

        switcher_alpha = {
            GaussPointsType.GaussLegendre: 0.0,
            GaussPointsType.GaussJacobi10: 1.0,
            GaussPointsType.GaussJacobi20: 2.0
        }

        alpha = switcher_alpha.get(gptype, 0.0)
        beta = 0.0

        if ((gptype == GaussPointsType.GaussLegendre) or
            (gptype == GaussPointsType.GaussJacobi10) or
            (gptype == GaussPointsType.GaussJacobi20)):

            J = JacobiPolynomial(nip, alpha, beta)
            d = J.deval(p)

            self.D[gauss_key] = np.zeros((nip, nip))
            D = self.D[gauss_key]

            ii = np.arange(nip)
            aux = np.zeros(nip)

            # Diagonal
            # D[ii,ii] = c0[gptype]*(c1[gptype]+c2[gptype]*p)/(1.0-p*p)
            aux = p * p
            aux *= -1.0
            aux += 1.0
            D[ii, ii] += c2[gptype] * p
            D[ii, ii] += c1[gptype]
            D[ii, ii] *= c0[gptype]
            D[ii, ii] /= aux

            # Upper triangular
            ii, jj = np.triu_indices(nip, 1)
            # D[ii,jj] = d[ii] / ( d[jj]*( p[ii] - p[jj] ) )
            aux = p[ii] - p[jj]
            aux *= d[jj]
            D[ii, jj] = d[ii] / aux

            # Lower triangular
            ii, jj = np.tril_indices(nip, -1)
            # D[ii,jj] = d[ii] / ( d[jj]*( p[ii] - p[jj] ) )
            aux = p[ii] - p[jj]
            aux *= d[jj]
            D[ii, jj] = d[ii] / aux

        elif (gptype == GaussPointsType.GaussLobatto):
            raise AssertionError("Not implemented!")

        elif ((gptype == GaussPointsType.NegativeOne) or
              (gptype == GaussPointsType.PositiveOne)):
            self.D[gauss_key] = np.zeros((1, 1))

        else:
            None


## end of manticore.lops.modal_dg.gauss.CollocationDiffMat

#
# """Key indexer for streamed Gauss weights.
#
#     This key is a little different from its cousin GaussKey because it
#         wraps a geometry (so, it is 2D) instead of a single tensor
#         function (1D).  It is used to index streamed weights for
#         improved non-tensorial integration.
#
#     Attributes:
#         region_type (dgtypes.StdRegionType): Geometry we streaming the
#             weights
#
#         nip_1d (int): 1D number of weights, per direction
# """
StreamlinedGaussKey = namedtuple('StreamlinedGaussKey', ['type', 'n1d'])
