#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Jacobi polynomials
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from numbers import Number
from math import gamma, cos, pi, fabs
from sys import float_info


class JacobiPolynomial:
    """Evaluation of Jacobi polynomials, their derivatives and roots.

       Based on previous works of Cantão! (rfcantao@gmail.com)

    """
    JACOBI_ZERO_TOL = float_info.epsilon
    JACOBI_ZERO_MAX = 25000

    def __init__(self, order, alpha=0.0, beta=0.0):
        """Constructor for a JacobiPolynomial object.

        Attributes:
            m (int): order of the Jacobi polynomial.

            alpha (real):  weight.

            beta (real):   weight.

            aPb (real): alpha + beta

            aMb (real): alpha - beta

            jp (list): list of functions for computing a Jacobi
                polinonial of n-th order at x (scalar).

            djp (list): list of functions for computing the first
                derivative if a Jacobi polinonial of n-th order at x
                (scalar).

            np_jp (list): list of functions for computing a Jacobi
                polinonial of n-th order at the elements of x (numpy.array).

            np_djp (list): list of functions for computing the first
                derivative if a Jacobi polinonial of n-th order at the
                elements of x (numpy.array).

            jp_roots(dict): storage of the roots of the
                JacobiPolynomial(alpha, beta).

        Args:
            order (int): order of the Jacobi polynomial.

            alpha (real):  weight.

            beta (real):   weight.

        Notes:
            * JacobiPolynomial(alpha=0.0, beta=0.0) == Legendre polynomial.

        """
        self.__alpha = alpha
        self.__beta = beta
        self.m = int(order)
        self.aPb = alpha + beta
        self.aMb = alpha - beta

        self.jp = [self.j0, self.j1, self.jn]
        self.djp = [self.dj0, self.dj1, self.djn]
        self.np_jp = [self.np_j0, self.np_j1, self.np_jn]
        self.np_djp = [self.np_dj0, self.np_dj1, self.np_djn]
        self.jp_roots = {0: None}

    def configure(self, order, alpha, beta):
        """Sets Jacobi polynomial parameters.

        Args:
            order (int): order of the Jacobi polynomial.

            alpha (real):  weight.

            beta (real):   weight.

        Notes:

            * JacobiPolynomial(alpha=0.0, beta=0.0) == Legendre polynomial.

            * IMPORTANT: Use this method carefully as it may break the
                meaning of the storage of the roots of the
                JacobiPolynomial(alpha, beta). Be careful.

            * IMPORTANT: It is very dangerous to have default
                  arguments here. Any misuse may introduce serious and
                  stealth errors by changing the meaning of Jacobi
                  polynomials inside algorithms.

        """
        self.m = int(order)
        self.__alpha = alpha
        self.__beta = beta
        self.aPb = alpha + beta
        self.aMb = alpha - beta

    @property
    def order(self):
        """Returns the order this Jacobi polynomial.

        Returns:
           int: order this Jacobi polynomial.
        """
        return self.m

    @property
    def alpha(self):
        return self.__alpha

    @property
    def beta(self):
        return self.__beta

    def eval(self, x):
        """Evaluates this Jacobi polynomial at x.

        Args:
            x: scalar or NumPy array

        Returns:
            scalar or NumPy array (according to the argument):
                values of the polynomial.

        """
        if isinstance(x, Number):
            return self.jp[min(self.m, 2)](x)  # x is scalar
        else:
            return self.np_jp[min(self.m, 2)](x)  # x is a numpy.array

    def deval(self, x):
        """Evaluates the derivative of this Jacobi polynomial at x.

        Args:
            x: scalar or NumPy array

        Returns:
            scalar or NumPy array (according to the argument):
                values of the polynomial.

        """
        if isinstance(x, Number):
            return self.djp[min(self.m, 2)](x)  # x is scalar
        else:
            return self.np_djp[min(self.m, 2)](x)  # x is a numpy.array

    def roots(self):
        """Returns the roots of this Jacobi polynomial. The roots of the m-th
           order are computed at the first call and are stored for
           future calls.

        Returns:
            NumPy array: roots of this Jacobi polynomial.

        """
        if self.m not in self.jp_roots:
            self.compute_roots()

        return self.jp_roots.get(self.m)

    def j0(self, x):
        """Returns the value a of the 0-th order Jacobi polynomial at x.

        Args:
            x (float)

        Returns:
            float
        """
        return 1.0

    def dj0(self, x):
        """Returns the value a of the derivative of the 0-th order Jacobi
        polynomial at x.

        Args:
            x (float)

        Returns:
            float
        """
        return 0.0

    def j1(self, x):
        """Returns the value a of the 1-th order Jacobi polynomial at x.

        Args:
            x (float)
        """
        return 0.5 * (self.aMb + (self.aPb + 2.0) * x)

    def dj1(self, x):
        """Returns the value a of the derivative of the 1-th order Jacobi
        polynomial at x.

        Args:
            x (float)

        Returns:
            float
        """
        return 0.5 * (self.aPb + 2.0)

    def jn(self, x):
        """Returns the value a of the m-th (m>1) order Jacobi polynomial at x.

        Args:
            x (float)

        Returns:
            float
        """
        alpha = self.__alpha
        beta = self.__beta
        aMb = self.aMb
        aPb = self.aPb
        Pn0 = 1.0
        Pn1 = 0.5 * (aMb + (aPb + 2.0) * x)
        Pm = 0.0

        for n in range(1, self.m):
            n1 = n + 1.0
            n2 = 2.0 * n

            a1n = 2.0 * n1 * (n1 + aPb) * (n2 + aPb)
            a2n = (n2 + aPb + 1.0) * aPb * aMb
            a3n = (n2 + aPb) * (n2 + aPb + 1.0) * (n2 + aPb + 2.0)
            a4n = 2.0 * (n + alpha) * (n + beta) * (n2 + aPb + 2.0)

            Pm = ((a2n + a3n * x) * Pn1 - a4n * Pn0) / a1n
            Pn0 = Pn1
            Pn1 = Pm

        return Pm

    def djn(self, x):
        """Returns the value of the first derivative of the m-th (m>1) order
        Jacobi polynomial at x.

        Args:
            x (float)

        Returns:
            float

        """
        alpha = self.__alpha
        beta = self.__beta
        m = self.m
        aMb = self.aMb
        aPb = self.aPb
        Pn0 = 1.0
        Pn1 = 0.5 * (aMb + (aPb + 2.0) * x)
        Pm = 0.0
        DPm = 0.0

        for n in range(1, m + 1):  # Note: the range limit differs from jn!
            n1 = n + 1.0
            n2 = 2.0 * n

            a1n = 2.0 * n1 * (n1 + aPb) * (n2 + aPb)
            a2n = (n2 + aPb + 1.0) * aPb * aMb
            a3n = (n2 + aPb) * (n2 + aPb + 1.0) * (n2 + aPb + 2.0)
            a4n = 2.0 * (n + alpha) * (n + beta) * (n2 + aPb + 2.0)

            facA = gamma(m + alpha + 1.0)
            # Gamma(n) = (n-1)!
            facB = gamma(m + beta + 1.0)

            if x == -1:
                DPm = (-1.0)**(m - 1) * 0.5 * (aPb + m + 1.0) * facB / (
                    gamma(beta + 2.0) * gamma(m))
            elif x == 1:
                DPm = 0.5 * (aPb + m + 1.0) * facA / (
                    gamma(alpha + 2.0) * gamma(m))
            else:
                b1n = (n2 + aPb) * (1.0 - x**2)
                b2n = n * (aMb - (n2 + aPb) * x)
                b3n = 2.0 * (n + alpha) * (n + beta)
                DPm = (b2n * Pn1 + b3n * Pn0) / b1n

            Pm = ((a2n + a3n * x) * Pn1 - a4n * Pn0) / a1n
            Pn0 = Pn1
            Pn1 = Pm

        return DPm

    def np_j0(self, x):
        """Returns the value of the 0-th order Jacobi polynomial at x.

        Args:
            x (numpy.array[dtype=numpy.float_])

        Returns:
            numpy.array[dtype=numpy.float_]
        """
        return np.ones(x.shape)

    def np_dj0(self, x):
        """Returns the value of the first derivative of the 0-th order Jacobi
        polynomial at x.

        Args:
            x (numpy.array[dtype=numpy.float_])

        Returns:
            numpy.array[dtype=numpy.float_]

        """
        return np.zeros(x.shape)

    def np_j1(self, x):
        """Returns the value of the 1-th order Jacobi polynomial at x.

        Args:
            x (numpy.array[dtype=numpy.float_])

        Returns:
            numpy.array[dtype=numpy.float_]
        """
        # 0.5*(self.aMb+(self.aPb+2.0)*x) split in in-place operations
        Pn1 = (self.aPb + 2.0) * x
        Pn1 += self.aMb
        Pn1 *= 0.5
        return Pn1

    def np_dj1(self, x):
        """Returns the value of the first derivative of the 1-th order Jacobi
        polynomial at x.

        Args:
            x (numpy.array[dtype=numpy.float_])

        Returns:
            numpy.array[dtype=numpy.float_]

        """
        return 0.5 * (self.aPb + 2.0) * np.ones(x.shape)

    def np_jn(self, x):
        """Returns the value of the m-th order Jacobi polynomial at x.

        Args:
            x (numpy.array[dtype=numpy.float_])

        Returns:
            numpy.array[dtype=numpy.float_]
        """
        alpha = self.__alpha
        beta = self.__beta
        aMb = self.aMb
        aPb = self.aPb

        Pn0 = np.ones(x.shape)
        Pn1 = (aPb + 2.0) * x
        Pn1 += aMb
        Pn1 *= 0.5
        Pm = np.zeros(x.shape)

        for n in range(1, self.m):
            n1 = n + 1.0
            n2 = 2.0 * n

            a1n = 2.0 * n1 * (n1 + aPb) * (n2 + aPb)
            a2n = (n2 + aPb + 1.0) * aPb * aMb
            a3n = (n2 + aPb) * (n2 + aPb + 1.0) * (n2 + aPb + 2.0)
            a4n = 2.0 * (n + alpha) * (n + beta) * (n2 + aPb + 2.0)

            # Pm = (( a2n+a3n*x )*Pn1-a4n*Pn0)/a1n split in in-place operations
            Pm = a3n * x
            Pm += a2n
            Pm *= Pn1
            Pn0 *= a4n
            Pm -= Pn0
            Pm /= a1n

            Pn0 = Pn1
            Pn1 = Pm

        return Pm

    def np_djn(self, x):
        """Returns the value of the first derivative of the m-th order Jacobi
        polynomial at x.

        Args:
            x (numpy.array[dtype=numpy.float_], len(x.shape) == 1)

        Returns:
            numpy.array[dtype=numpy.float_]

        """
        if len(x.shape) > 1:
            raise AssertionError("The input must be an one-dimensional array!")

        alpha = self.__alpha
        beta = self.__beta
        m = self.m
        aMb = self.aMb
        aPb = self.aPb

        Pn0 = np.ones(x.shape)
        Pn1 = (aPb + 2.0) * x
        Pn1 += aMb
        Pn1 *= 0.5
        Pm = np.zeros(x.shape)
        DPm = np.zeros(x.shape)

        # Finds extreme points
        idx = (x > -1.0) & (x < 1.0)
        idxm1 = (x == -1.0)
        idxp1 = (x == 1.0)
        idxlm1 = (x < -1.0)
        idxgp1 = (x > 1.0)

        # Check if we *really* have at most one x=-1.0 and one x=1.0
        if len(np.where(idxm1)) > 1 or len(np.where(idxp1)) > 1:
            raise AssertionError("Too much -1.0 or 1.0 on the domain!")

        if np.any(idxlm1) or np.any(idxgp1):
            raise AssertionError("Values outside [-1.0, 1.0]")

        x_inner = x[idx]
        DPm_inner = DPm[idx]

        for n in range(1, m + 1):  # Note: the range limit differs from np_jn!
            n1 = n + 1.0
            n2 = 2.0 * n

            a1n = 2.0 * n1 * (n1 + aPb) * (n2 + aPb)
            a2n = (n2 + aPb + 1.0) * aPb * aMb
            a3n = (n2 + aPb) * (n2 + aPb + 1.0) * (n2 + aPb + 2.0)
            a4n = 2.0 * (n + alpha) * (n + beta) * (n2 + aPb + 2.0)

            b1n = (n2 + aPb) * (1.0 - x_inner**2)
            b2n = n * (aMb - (n2 + aPb) * x_inner)
            b3n = 2.0 * (n + alpha) * (n + beta)

            DPm[idx] = b2n * Pn1[idx]
            DPm[idx] += b3n * Pn0[idx]
            DPm[idx] /= b1n

            facA = gamma(m + alpha + 1.0)
            # Gamma(n) = (n-1)!
            facB = gamma(m + beta + 1.0)

            DPm[idxm1] = (-1.0)**(m - 1) * 0.5 * (aPb + m + 1.0) * facB / (
                gamma(beta + 2.0) * gamma(m))

            DPm[idxp1] = 0.5 * (aPb + m + 1.0) * facA / (
                gamma(alpha + 2.0) * gamma(m))

            # Pm = (( a2n+a3n*x )*Pn1-a4n*Pn0)/a1n split in in-place operations
            Pm = a3n * x
            Pm += a2n
            Pm *= Pn1
            Pn0 *= a4n
            Pm -= Pn0
            Pm /= a1n

            Pn0 = Pn1
            Pn1 = Pm

        return DPm

    def compute_roots(self):
        """Compute the roots pf this Jacobi polynomial."""
        alpha = self.__alpha
        beta = self.__beta
        m = self.m
        epsilon = JacobiPolynomial.JACOBI_ZERO_TOL
        maxIt = JacobiPolynomial.JACOBI_ZERO_MAX

        if m == 0: pass
        elif m == 1:
            self.jp_roots[1] = np.array([(beta - alpha) /
                                         (alpha + beta + 2.0)])
        else:
            self.jp_roots[m] = np.zeros(m)
            x = self.jp_roots[m]

            # Newton-Raphson algorithm with polynomial deflation
            for k in range(0, m):
                # Initial guess: roots of the Chebyshev polynomial of order m
                r = -cos(pi * (2 * k + 1) / (2 * m))

                if k > 0:
                    r = 0.5 * (r + x[k - 1])

                delta = 1.0
                c = 0

                while (fabs(delta) > epsilon) and (c < maxIt):
                    s = np.sum(1.0 / (r - x[range(0, k)]))
                    Pm = self.jn(r)
                    DPm = self.djn(r)
                    delta = -Pm / (DPm - Pm * s)
                    r = r + delta
                    c += 1

                x[k] = r
