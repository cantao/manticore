#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Interpolation expansion auxiliary tools
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

from array import array
import numpy as np
import manticore.services.const as const
from manticore.services.mconsts import CTEs


class ExpansionNeighbourhood:
    def __init__(self, number_of_faces):
        self.neighbourhood = [None for i in range(number_of_faces)]
        self.neigh_face = array('i', [-1 for i in range(number_of_faces)])

    def insert_neighbour(self, neigh_entity, where, neigh_face_idx):
        size = len(self.neighbourhood)

        if where >= size:
            for i in range(where - size + 1):
                self.neighbourhood.append(None)
                self.neigh_face.append(-1)

        self.neighbourhood[where] = neigh_entity
        self.neigh_face[where] = neigh_face_idx

    def get_neighbour(self, face_id):
        assert face_id != const.INVALID_RESULT
        assert self.neighbourhood[face_id] != None

        return self.neighbourhood[face_id]

    def is_valid_neighbour(self, face_id):
        assert face_id != const.INVALID_RESULT

        return self.neighbourhood[face_id] != None

    def get_neighbour_face_id(self, face_id):
        assert face_id != const.INVALID_RESULT

        return self.neigh_face[face_id]


class ExpansionSize:
    def quad(p_order):
        return (p_order + 1) * (p_order + 1)

    def tria(p_order):
        return int(((p_order + 1) * (p_order + 2)) / 2)

    def get(region_type, p_order):
        size_functors = [ExpansionSize.quad, ExpansionSize.tria]
        return size_functors[region_type](p_order)


class ModeLoopLimit:
    def quad_p(o):
        return o

    def quad_q(o, p):
        return o

    def tria_p(o):
        return o

    def tria_q(o, p):
        return o - p

    def P(region_type, o):
        loop_functors = [ModeLoopLimit.quad_p, ModeLoopLimit.tria_p]
        return loop_functors[region_type](o)

    def Q(region_type, o, p):
        loop_functors = [ModeLoopLimit.quad_q, ModeLoopLimit.tria_q]
        return loop_functors[region_type](o, p)

class OrderMigration:

    def __init__(self, region_type, ord_from, ord_to):
        self.std_dg = region_type
        self.ord_from = ord_from
        self.ord_to   = ord_to
        sz_from = ExpansionSize.get(region_type, ord_from)
        sz_to   = ExpansionSize.get(region_type, ord_to)
        self.pq_from = np.zeros(sz_from, dtype=np.int)
        self.pq_to   = np.zeros(sz_to,   dtype=np.int)

        mode = 0
        ord_from1 = ord_from+1

        for pp in range(ModeLoopLimit.P(region_type, ord_from)+1):
            for qq in range(ModeLoopLimit.Q(region_type, ord_from, pp)+1):
                self.pq_from[qq + ord_from1*pp] = mode
                mode+=1

        mode = 0
        ord_to1 = ord_to+1

        for pp in range(ModeLoopLimit.P(region_type, ord_to)+1):
            for qq in range(ModeLoopLimit.Q(region_type, ord_to, pp)+1):
                self.pq_to[qq + ord_to1*pp] = mode
                mode+=1

        self.idx_from = np.zeros(sz_from, dtype=np.int)
        self.idx_to   = np.zeros(sz_from, dtype=np.int)

        ii = 0
        for pp in range(ModeLoopLimit.P(region_type, ord_from)+1):
            for qq in range(ModeLoopLimit.Q(region_type, ord_from, pp)+1):
                self.idx_from[ii] = self.pq_from[qq + ord_from1*pp]
                self.idx_to[ii]   = self.pq_to[qq + ord_to1*pp]
                ii+=1
        
    def migrate(self, buff_from, buff_to, n):
        sz_from = ExpansionSize.get(self.std_dg, self.ord_from)
        sz_to   = ExpansionSize.get(self.std_dg, self.ord_to)

        jump_from = 0
        jump_to   = 0
        for el in range(n):
            buff_to[jump_to + self.idx_to]= buff_from[jump_from + self.idx_from]
            jump_from += sz_from
            jump_to   += sz_to
