#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Operations on real 2D elements.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import scipy.linalg as sla
import manticore.lops.modal_dg.stdmixins2d as _2d_mx
from manticore.lops.nodal_cg.interpol import InterpolationKey
from manticore.lops.modal_dg.stdops2d import factory_stdintegrator2d
from manticore.lops.modal_dg.stdops2d import factory_stddifferentiator2d
from manticore.lops.modal_dg.stdops2d import factory_stdinnerproduct2d
from manticore.lops.modal_dg.stdops2d import factory_stdbackward2d
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.dgtypes import (
    StdRegionType, RegionAdapter, RegionInfo )
from manticore.lops.modal_dg.geomfactors import factory_form_factors
#import scipy.sparse as spa


def factory_physintegrator2d(classRegion):
    """Factory of classes that perform 2D integration of a scalar function on a
    real element.

    Notes:

        * Values of the scalar function and jacobian at the integration points
            must be in streamlined ordering; see
            manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights)
    """

    Parent = factory_stdintegrator2d(classRegion)

    class PhysIntegrator(Parent):

        __slots__ = []

        def __init__(self, key):
            """Constructor of PhysIntegrator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

        def __call__(self, Ph, J):
            """Executes a 2D integration of a scalar function on a real element.

            Args:
                Ph (numpy.array) [in]: values of a scalar function at
                    the integration points (in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).

                J (numpy.array) [in]: values of the jacobian (determinant of
                    the Jacobian matrix) of the complete standard to real
                    mapping (xi -> area coords -> x).

            Returns:
                double
            """

            return Parent.__call__(self, Ph * J)

    # end of factory_physintegrator2d.PhysIntegrator

    return PhysIntegrator


# end of manticore.lops.modal_dg.phyops2d.factory_physintegrator2d


def factory_physevaluator2d(classRegion):
    """Factory of classes that evaluate a scalar function f(x,y) over the
    integration points of a real element.

    Notes:
        * Values of the scalar function at the integration points are
            returned in streamlined ordering; see
            manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights
    """

    # Using stdmixins2d factories to built a class that provides
    # access to principal functions, number of modes and number of
    # integration points.
    Parent = _2d_mx.factory_wp_mixin(_2d_mx.factory_number_of_ip(classRegion))

    class PhysEvaluator(Parent):

        __slots__ = ['px', 'py']

        def __init__(self, key):
            """Constructor of PhysEvaluator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.

            """

            Parent.__init__(self, key)

            self.px = np.zeros(self.n2d)
            self.py = np.zeros(self.n2d)

        def __call__(self, f, coords, geom_type, collapsed_to_global):
            """Evaluates a scalar function f(x,y) over the integration points
            of a standard element.

            Args:

                f (function) [in]: scalar function able to receive Numpy
                    1d arrays as arguments also returning a Numpy array.

                coords (numpy.array) [in]: nodal coordinates of the geometric
                   interpolation. coords.shape = (2, number of nodes)

                geom_type (StandardGeometry): type of geometric interpolation
                    (the collapsed_to_global mapping depends on the type of
                    geometric interpolation).

                collapsed_to_global (CollapsedToGlobalMaps): complete mapping
                    from collapsed to real coordinates: eta->xi->area->x

            Returns:

                Numpy 1d array in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights

            """

            map_key = InterpolationKey.get_instance(geom_type, self.n2d)

            assert self.G == RegionInfo.mesh_to_dg(map_key.shape)

            # Maps integration points coordinates (eta) to local
            # (area) coordinates, evaluates shape functions at those
            # points and returns an Interpolation object.
            c2g_map = collapsed_to_global(map_key)  # <- Interpolation object

            # Returns the global coordinates of this element
            # integration points.
            c2g_map.local_to_global(coords, self.px, self.py)

            return f(self.px, self.py)

    # end of factory_physevaluator2d.PhysEvaluator

    return PhysEvaluator


# end of manticore.lops.modal_dg.phyops2d.factory_physevaluator2d


def factory_physdifferentiator2d(classRegion):
    """ Physical numerical gradient on real coordinates.
    """

    Parent = factory_stddifferentiator2d(classRegion)

    class PhysDifferentiator(Parent):

        __slots__ = []

        def __init__(self, key):
            """Constructor of PhysIntegrator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

        def __call__(self, JM, Ph, dPhdx, dPhdy):
            """Physical numerical gradient on real coordinates.

            Args:

                Ph (numpy.array) [in]: values of a scalar function at
                    the integration points (in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).
                    Physical values being differentiated

                JM (list(numpy.array)) [in]: List of [dxi/dx] matrices (2x2
                    numpy.array's) at the integration points (see
                    manticore.lops.modal_dg.geomfactors.CollapsedToGlobalMaps).
                    [dxi/dx] is the Jacobian matrix of x->local->xi

                dPhdx (numpy.array) [out]:

                dPhdy (numpy.array) [out]:

            """

            nip = self.n2d

            assert Ph.shape[0] == nip
            assert dPhdx.shape[0] == nip
            assert dPhdy.shape[0] == nip
            assert len(JM) == nip

            Parent.__call__(self, Ph, dPhdx, dPhdy)

            ddxi = np.zeros(2)
            ddx = np.zeros(2)

            for ij in range(nip):
                ddxi[0] = dPhdx[ij]
                ddxi[1] = dPhdy[ij]

                np.dot(JM[ij], ddxi, ddx)

                dPhdx[ij] = ddx[0]
                dPhdy[ij] = ddx[1]

    # end of factory_physdifferentiator2d.PhysDifferentiator

    return PhysDifferentiator


# end of manticore.lops.modal_dg.phyops2d.factory_physdifferentiator2d


def factory_physdivergent2d(classRegion):

    # Using stdmixins2d factories to built a class that provides
    # access to number of integration points.
    Parent = _2d_mx.factory_number_of_ip(classRegion)

    class PhysDivergent(Parent):

        __slots__ = ['op', 't1', 't2']

        def __init__(self, key):
            """Constructor of PhysDivergent objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.op = factory_physdifferentiator2d(classRegion)(key)
            self.t1 = np.zeros(self.n2d)
            self.t2 = np.zeros(self.n2d)

        def __call__(self, JM, Fx, Fy, Div):
            """Physical numerical divergent on real coordinates.

            Args:

                Fx (numpy.array) [in]:

                Fy (numpy.array) [in]:

                JM (list(numpy.array)) [in]: List of [dxi/dx] matrices (2x2
                    numpy.array's) at the integration points (see
                    manticore.lops.modal_dg.geomfactors.CollapsedToGlobalMaps).
                    [dxi/dx] is the Jacobian matrix of x->local->xi.

                Div (numpy.array) [out]: values of divergent at
                    the integration points (in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).
                    Physical values being differentiated

            """

            self.op(JM, Fx, Div, self.t1)
            self.op(JM, Fy, self.t1, self.t2)
            Div += self.t2

    return PhysDivergent


# end of manticore.lops.modal_dg.phyops2d.factory_physdivergent2d


def factory_physinnerproduct2d(classRegion):
    """Factory of classes that evaluates the inner product of the physical
    represented values with every basis function.

    Notes:

        * Values of the scalar function and jacobian at the integration points
            must be in streamlined ordering; see
            manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights)
    """

    Parent = factory_stdinnerproduct2d(classRegion)

    class PhysInnerProduct(Parent):

        __slots__ = []

        def __init__(self, key):
            """Constructor of PhysInnerProduct objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

        def __call__(self, Ph, J, Ipq):
            """Executes inner product of the physical represented values
            with every basis function.

            Args:
                Ph (numpy.array) [in]: values of a scalar function at
                    the integration points (in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).

                J (numpy.array) [in]: values of the jacobian (determinant of
                    the Jacobian matrix) of the complete standard to real
                    mapping (xi -> area coords -> x). Integration weights take
                    care of the eta->xi jacobian.

                Iqp (numpy.array) [out]: each element of this array is the
                        the inner product of Ph wiht a basis function.

            """

            Parent.__call__(self, Ph * J, Ipq)

        def eval(self, Ph):
            """ Evaluates all basis functions at all integration points.

            Args:
                Ph (numpy.array) [out]: values of all basis functions at
                    the integration points (in streamlined ordering; see
                    gauss_strm.StreamlinedGaussWeights). Matrix, a basis
                    function per line, an integration point per column.
            """

            Parent.eval(self, Ph)

    # end of factory_physinnerproduct2d.PhysInnerProduct

    return PhysInnerProduct


# end of manticore.lops.modal_dg.phyops2d.factory_physinnerproduct2d


def factory_physinnerproductgrad2d(classRegion):
    
    # Using stdmixins2d factories to built a class that provides
    # access to principal functions, number of modes and number of
    # integration points.
    Parent = _2d_mx.factory_princ_func_mixin(
        _2d_mx.factory_wp_mixin(
            _2d_mx.factory_number_of_modes(
                _2d_mx.factory_number_of_ip(classRegion))))

    class PhysInnerProductGradBase(Parent):
        """Base class for all specializations of StdInnerProductGrad."""

        __slots__ = ['nm', 'op', 'dphi0', 'dphi1', 'Fpq1', 'Fpq2', 'correction',
                         'JMC', 'ddxi1', 'ddxi2', 'ddxi', 'ddx']

        def __init__(self, key):
            """Constructor of StdInnerProductBase objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.nm = ExpansionSize.get(self.G, self.o)
            self.op = factory_physintegrator2d(classRegion)(key)
            self.correction = factory_form_factors(self.G)

            n  = self.n1d
            n2 = self.n2d # n2 = n * n
            sz = 2*n2
            
            self.dphi0 = np.zeros((n, n))
            self.dphi1 = np.zeros((n, n))
            self.Fpq1 = np.zeros(n2)
            self.Fpq2 = np.zeros(n2)
            
            self.JMC   = np.zeros((sz,sz))
            self.ddxi1 = np.zeros(n2)
            self.ddxi2 = np.zeros(n2)
            self.ddxi  = np.zeros(sz)
            self.ddx   = np.zeros(sz)

    # Specializations
    region = classRegion()

    if region.G == StdRegionType.DG_Quadrangle:

        class PhysInnerProductGrad(PhysInnerProductGradBase):

            __slots__ = []

            def __init__(self, key):
                """Constructor of StdInnerProduct objects for
                StdRegionType.DG_Quadrangle.

                Args:
                key (ExpansionKey): required for the initialization
                of Parent.
                """
                PhysInnerProductGradBase.__init__(self, key)

            def eval(self, J, JM, Ph, Ipq1, Ipq2):
                """Executes inner product of a vector of physical represented 
                values with the global derivatives of every basis function at 
                the integration points.

                Args:
                    J (numpy.array) [in]: values of the jacobian (determinant of
                        the Jacobian matrix) of the complete standard to real
                        mapping (xi -> area coords -> x).
                    JM (list(numpy.array)) [in]: List of [dxi/dx] matrices (2x2
                        numpy.array's) at the integration points.
                        [dxi/dx] is the Jacobian matrix of x->local->xi
                    Ph (numpy.array) [in]: values of a scalar function at the
                        integration points (in streamlined ordering; see
                        gauss_strm.StreamlinedGaussWeights).
                    Iqp1 (numpy.array) [out]: each element of this array is the
                        the inner product of Ph with the dphi/dx.
                    Iqp2 (numpy.array) [out]: each element of this array is the
                        the inner product of Ph with the dphi/dy.

                """
                assert Ph.shape[0]  == self.n2d
                assert Ipq1.shape[0] == self.nm
                assert Ipq2.shape[0] == self.nm

                Ps1 = self.PF1
                Ps2 = self.PF2

                px = self.P1
                py = self.P2
                ppx, ppy = np.meshgrid(px, py)
                
                n = self.n1d
                n2 = self.n2d
                sz = 2*n2

                correction = self.correction.eval

                # Assemble all Jacobian matrices in one structure for speed
                for i in range(n2):
                    self.JMC[2*i:2*i+2, 2*i:2*i+2] = JM[i]
                
                mode = 0
                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pf_p_1  = Ps1.column(pp)   # xi_1
                    pf_p_1d = Ps1.d_column(pp) # d/dxi_1
                    
                    Qmax = self.Q(pp) + 1

                    for qq in range(Qmax):
                        pf_q_2  = Ps2.column(qq)   # xi_2
                        pf_q_2d = Ps2.d_column(qq) # d/dxi_2

                        # Gradient on eta
                        np.multiply(
                            np.reshape(pf_q_2d,(n, 1)), pf_p_1,  self.dphi1)
                        np.multiply(
                            np.reshape(pf_q_2, (n, 1)), pf_p_1d, self.dphi0)

                        # Gradient on eta -> gradient on xi
                        correction(
                            np.ravel(ppx), np.ravel(ppy),
                            np.ravel(self.dphi0), np.ravel(self.dphi1),
                            self.ddxi1, self.ddxi2)

                        self.ddxi[0:sz-1:2] = self.ddxi1
                        self.ddxi[1:sz  :2] = self.ddxi2

                        # Gradient on xi -> gradient on x
                        np.dot(self.JMC, self.ddxi, out=self.ddx)
                        
                        np.multiply(Ph, self.ddx[0:sz-1:2], self.Fpq1)
                        Ipq1[mode] = self.op(self.Fpq1, J)
                        np.multiply(Ph, self.ddx[1:sz  :2], self.Fpq2)
                        Ipq2[mode] = self.op(self.Fpq2, J)
                        mode += 1

            def __call__(self, J, JM, Ph1, Ph2, Ipq):
                """Executes inner product of a vector of physical represented 
                values with the global derivatives of every basis function at 
                the integration points.

                Notes:
                    * This is the S-operator in the DG discrete statement.

                Args:
                    J (numpy.array) [in]: values of the jacobian (determinant of
                        the Jacobian matrix) of the complete standard to real
                        mapping (xi -> area coords -> x).
                    JM (list(numpy.array)) [in]: List of [dxi/dx] matrices (2x2
                        numpy.array's) at the integration points.
                        [dxi/dx] is the Jacobian matrix of x->local->xi
                    Ph1 (numpy.array) [in]: values of a scalar function in 
                        x at the integration points (in streamlined ordering; 
                        see gauss_strm.StreamlinedGaussWeights).
                    Ph2 (numpy.array) [in]: values of a scalar function in 
                        y at the integration points (in streamlined ordering; 
                        see gauss_strm.StreamlinedGaussWeights).
                    Iqp (numpy.array) [out]: each element of this array is the
                        the inner product of [Ph1,Ph2] with 
                        [dphi/dx, dphi/dy].
                """
                assert Ph1.shape[0]  == self.n2d
                assert Ph2.shape[0]  == self.n2d
                assert Ipq.shape[0] == self.nm

                Ps1 = self.PF1
                Ps2 = self.PF2

                px = self.P1
                py = self.P2
                ppx, ppy = np.meshgrid(px, py)
                
                n  = self.n1d
                n2 = self.n2d
                sz = 2*n2

                correction = self.correction.eval

                # Assemble all Jacobian matrices in one structure for speed
                for i in range(n2):
                    self.JMC[2*i:2*i+2, 2*i:2*i+2] = JM[i]

                mode = 0
                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pf_p_1  = Ps1.column(pp)   # xi_1
                    pf_p_1d = Ps1.d_column(pp) # d/dxi_1
                    
                    Qmax = self.Q(pp) + 1

                    for qq in range(Qmax):
                        pf_q_2  = Ps2.column(qq)   # xi_2
                        pf_q_2d = Ps2.d_column(qq) # d/dxi_2

                        # Gradient on eta
                        np.multiply(
                            np.reshape(pf_q_2d,(n, 1)), pf_p_1,  self.dphi1)
                        np.multiply(
                            np.reshape(pf_q_2, (n, 1)), pf_p_1d, self.dphi0)

                        # Gradient on eta -> gradient on xi
                        correction(
                            np.ravel(ppx), np.ravel(ppy),
                            np.ravel(self.dphi0), np.ravel(self.dphi1),
                            self.ddxi1, self.ddxi2)

                        self.ddxi[0:sz-1:2] = self.ddxi1
                        self.ddxi[1:sz  :2] = self.ddxi2

                        # Gradient on xi -> gradient on x
                        np.dot(self.JMC, self.ddxi, out=self.ddx)

                        np.multiply(Ph1, self.ddx[0:sz-1:2], self.Fpq1)
                        np.multiply(Ph2, self.ddx[1:sz  :2], self.Fpq2)

                        self.Fpq1+=self.Fpq2 # avoiding the temporary Fpq1+Fpq2
                        
                        Ipq[mode] = self.op(self.Fpq1, J)
                        mode += 1

            def grad(self, JM, psiDx, psiDy):
                """Returns dphi/dx and dphi/dy at the
                integration points.
                """
                assert psiDx.shape == (self.nm,self.n2d)
                assert psiDy.shape == (self.nm,self.n2d)

                Ps1 = self.PF1
                Ps2 = self.PF2

                px = self.P1
                py = self.P2
                
                n = self.n1d

                ddxi = np.zeros(2)
                ddx  = np.zeros(2)
                
                mode = 0
                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pf_p_1  = Ps1.column(pp)   # xi_1
                    pf_p_1d = Ps1.d_column(pp) # d/dxi_1
                    
                    Qmax = self.Q(pp) + 1

                    for qq in range(Qmax):
                        pf_q_2  = Ps2.column(qq)   # xi_2
                        pf_q_2d = Ps2.d_column(qq) # d/dxi_2

                        np.multiply(
                            np.reshape(pf_q_2d,(n, 1)), pf_p_1,  self.dphi1)
                        np.multiply(
                            np.reshape(pf_q_2, (n, 1)), pf_p_1d, self.dphi0)

                        ij = 0
                        for j in range(n):
                            py_j = py[j]
                            for i in range(n):
                                # Gradient on eta -> gradient on xi
                                ddxi[0], ddxi[1] = self.correction.eval_scalar(
                                    px[i],py_j,self.dphi0[i,j],self.dphi1[i,j])

                                # Gradient on xi -> gradient on x
                                np.dot(JM[ij], ddxi, ddx)
                                
                                psiDx[mode, ij] = ddx[0]
                                psiDy[mode, ij] = ddx[1]
                                ij+=1
                                
                        mode += 1

        # end of manticore.lops.modal_dg.phyops2d.PhysInnerProductGrad
        
        return PhysInnerProductGrad

    elif region.G == StdRegionType.DG_Triangle:

        class PhysInnerProductGrad(PhysInnerProductGradBase):

            __slots__ = []

            def __init__(self, key):
                """Constructor of StdInnerProduct objects for
                StdRegionType.DG_Triangle.

                Args:
                key (ExpansionKey): required for the initialization
                of Parent.
                """
                PhysInnerProductGradBase.__init__(self, key)

            def eval(self, J, JM, Ph, Ipq1, Ipq2):
                
                assert Ph.shape[0]  == self.n2d
                assert Ipq1.shape[0] == self.nm
                assert Ipq2.shape[0] == self.nm

                Ps1 = self.PF1
                Ps2 = self.PF2

                px = self.P1
                py = self.P2
                
                n = self.n1d
                n2 = self.n2d
                sz = 2*n2

                correction = self.correction.eval

                # Assemble all Jacobian matrices in one structure for speed
                for i in range(n2):
                    self.JMC[2*i:2*i+2, 2*i:2*i+2] = JM[i]
                
                mode = 0
                tab = 0  # Extra indexer for triangle
                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pf_p_1  = Ps1.column(pp)   # xi_1
                    pf_p_1d = Ps1.d_column(pp) # d/dxi_1
                    
                    Qmax = self.Q(pp) + 1

                    adv = tab  # Extra indexer for triangle

                    for qq in range(Qmax):
                        pf_q_2  = Ps2.column(adv)   # xi_2
                        pf_q_2d = Ps2.d_column(adv) # d/dxi_2

                        # Gradient on eta
                        np.multiply(
                            np.reshape(pf_q_2d,(n, 1)), pf_p_1,  self.dphi1)
                        np.multiply(
                            np.reshape(pf_q_2, (n, 1)), pf_p_1d, self.dphi0)

                        # Gradient on eta -> gradient on xi
                        correction(
                            np.ravel(ppx), np.ravel(ppy),
                            np.ravel(self.dphi0), np.ravel(self.dphi1),
                            self.ddxi1, self.ddxi2)

                        self.ddxi[0:sz-1:2] = self.ddxi1
                        self.ddxi[1:sz  :2] = self.ddxi2

                        # Gradient on xi -> gradient on x
                        np.dot(self.JMC, self.ddxi, out=self.ddx)
                        
                        np.multiply(Ph, self.ddx[0:sz-1:2], self.Fpq1)
                        Ipq1[mode] = self.op(self.Fpq1, J)
                        np.multiply(Ph, self.ddx[1:sz  :2], self.Fpq2)
                        Ipq2[mode] = self.op(self.Fpq2, J)
                        mode += 1
                        adv += 1

                    tab += self.o - pp + 1  # Extra indexer for triangle

            def __call__(self, J, JM, Ph1, Ph2, Ipq):
                
                assert Ph1.shape[0]  == self.n2d
                assert Ph2.shape[0]  == self.n2d
                assert Ipq.shape[0] == self.nm

                Ps1 = self.PF1
                Ps2 = self.PF2

                px = self.P1
                py = self.P2
                ppx, ppy = np.meshgrid(px, py)
                
                n  = self.n1d
                n2 = self.n2d
                sz = 2*n2

                correction = self.correction.eval

                # Assemble all Jacobian matrices in one structure for speed
                for i in range(n2):
                    self.JMC[2*i:2*i+2, 2*i:2*i+2] = JM[i]
                
                mode = 0
                tab = 0  # Extra indexer for triangle
                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pf_p_1  = Ps1.column(pp)   # xi_1
                    pf_p_1d = Ps1.d_column(pp) # d/dxi_1
                    
                    Qmax = self.Q(pp) + 1

                    adv = tab  # Extra indexer for triangle

                    for qq in range(Qmax):
                        pf_q_2  = Ps2.column(adv)   # xi_2
                        pf_q_2d = Ps2.d_column(adv) # d/dxi_2

                        # Gradient on eta
                        np.multiply(
                            np.reshape(pf_q_2d,(n, 1)), pf_p_1,  self.dphi1)
                        np.multiply(
                            np.reshape(pf_q_2, (n, 1)), pf_p_1d, self.dphi0)

                        # Gradient on eta -> gradient on xi
                        correction(
                            np.ravel(ppx), np.ravel(ppy),
                            np.ravel(self.dphi0), np.ravel(self.dphi1),
                            self.ddxi1, self.ddxi2)

                        self.ddxi[0:sz-1:2] = self.ddxi1
                        self.ddxi[1:sz  :2] = self.ddxi2

                        # Gradient on xi -> gradient on x
                        np.dot(self.JMC, self.ddxi, out=self.ddx)

                        np.multiply(Ph1, self.ddx[0:sz-1:2], self.Fpq1)
                        np.multiply(Ph2, self.ddx[1:sz  :2], self.Fpq2)

                        self.Fpq1+=self.Fpq2 # avoiding the temporary Fpq1+Fpq2
                        
                        Ipq[mode] = self.op(self.Fpq1, J)
                        mode += 1
                        adv += 1

                    tab += self.o - pp + 1  # Extra indexer for triangle

            def grad(self, JM, psiDx, psiDy):
                
                assert psiDx.shape == (self.nm,self.n2d)
                assert psiDy.shape == (self.nm,self.n2d)

                Ps1 = self.PF1
                Ps2 = self.PF2

                px = self.P1
                py = self.P2
                
                n = self.n1d

                ddxi = np.zeros(2)
                ddx  = np.zeros(2)
                
                mode = 0
                tab = 0  # Extra indexer for triangle
                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pf_p_1  = Ps1.column(pp)   # xi_1
                    pf_p_1d = Ps1.d_column(pp) # d/dxi_1
                    
                    Qmax = self.Q(pp) + 1

                    adv = tab  # Extra indexer for triangle

                    for qq in range(Qmax):
                        pf_q_2  = Ps2.column(adv)   # xi_2
                        pf_q_2d = Ps2.d_column(adv) # d/dxi_2

                        np.multiply(
                            np.reshape(pf_q_2d,(n, 1)), pf_p_1,  self.dphi1)
                        np.multiply(
                            np.reshape(pf_q_2, (n, 1)), pf_p_1d, self.dphi0)

                        ij = 0
                        for j in range(n):
                            py_j = py[j]
                            for i in range(n):
                                # Gradient on eta -> gradient on xi
                                ddxi[0], ddxi[1] = self.correction.eval_scalar(
                                    px[i],py_j,self.dphi0[i,j],self.dphi1[i,j])

                                # Gradient on xi -> gradient on x
                                np.dot(JM[ij], ddxi, ddx)
                                
                                psiDx[mode, ij] = ddx[0]
                                psiDy[mode, ij] = ddx[1]
                                ij+=1
                                
                        mode += 1
                        adv += 1
                        
                    tab += self.o - pp + 1  # Extra indexer for triangle
        
        # end of manticore.lops.modal_dg.phyops2d.PhysInnerProductGrad
        
        return PhysInnerProductGrad
    
    else:
        raise AssertionError("Incorrect region_type!")
    
# end of manticore.lops.modal_dg.phyops2d.factory_physinnerproductgrad2d


def factory_physbackward2d(classRegion):
    """Factory of classes that perform backward tranformation on real elements.

    Backward transformation: reconstruction from coefficient space
    $\hat{u}$ to physical space $u$ on two dimensions according
    to element's shape information (classRegion) and an ExpansionKey
    object.

    Notes:

        * See manticore.lops.modal_dg.dgtypes.factory_class_region
        * See manticore.lops.modal_dg.stdops2d.py

    """

    Parent = factory_stdbackward2d(classRegion)

    class PhysBackward(Parent):

        __slots__ = []

        def __init__(self, key):
            """Constructor of PhysBackward objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

        def __call__(self, C, Ph):
            """Executes inner product of the physical represented values
            with every basis function.

            Args:
                C (Numpy.array) [in]: real-valued (P*Q, 1) array
                    (P = Q = number_of_modes) containing the
                    coeficients of the polynomial expansion
                    \sum_p\sum_q C_{pq}\phi_p\phi_q

                Ph (Numpy.array) [out]: real-valued (nip*nip, 1)
                    array containing the physical values of the
                    interpolation at the integration poits ordered
                    as Phi[ii+jj*nip], ii=0,..., nip in xi_1,
                    jj=0,..., nip in xi_2.
            """

            Parent.at_ips(self, C, Ph)

        def at_point(self, C, xi_1, xi_2):
            """Backward transformation at local coordinates (xi_1, xi_2)
            of the standard region.

            Args:

                C (Numpy.array) [in]: real-valued (P*(Q+1)/2, 1)
                    array (P = Q) containing the coeficients of
                    the polynomial expansion
                    \sum_p\sum_q C_{pq}\psi_p\psi_pq

                xi_1 (double) [in]: \eta_1 coordinate [-1, +1]

                xi_2 (double) [in]: \eta_2 coordinate [-1, +1[

            Returns:

                double

            """

            return Parent.at_point(self, C, xi_1, xi_2)

    # end of factory_physbackward2d.PhysBackward

    return PhysBackward


# end of manticore.lops.modal_dg.phyops2d.factory_physbackward2d


def factory_physmassmatrix(classRegion):
    """ Mass matrix for real elements.

    Notes:
        * See K&S, pages 175
    """

    Parent = factory_physinnerproduct2d(classRegion)

    class PhysMassMatrix(Parent):

        __slots__ = ['bw', 'nm', 'C', 'Tmp', 'Ph']

        def __init__(self, key):
            """Constructor of PhysMassMatrix objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.bw = factory_physbackward2d(classRegion)(key)
            self.nm = ExpansionSize.get(self.G, self.o)
            self.C = np.zeros(self.nm)
            self.Tmp = np.zeros(self.nm)
            self.Ph = np.zeros(self.n2d)

        def __call__(self, J, M):
            """Computes the mass matrix of an real element.

            Args:
                J (numpy.array) [in]: values of the jacobian (determinant of
                    the Jacobian matrix) of the complete standard to real
                    mapping (xi -> area coords -> x). Integration weights take
                    care of the eta->xi jacobian.

                M (numpy.array) [out]: mass matrix.

            """

            for ii in range(self.nm):

                self.C[ii] = 1.0

                self.bw.at_ips(self.C, self.Ph)  # call method from bw's parent

                Parent.__call__(self, self.Ph, J, self.Tmp)

                M[ii, :] = self.Tmp

                self.C[ii] = 0.0

    return PhysMassMatrix


# end of manticore.lops.modal_dg.phyops2d.factory_physmassmatrix


def factory_WADG_invmassmatrix(classRegion):
    """ WADG approximation of the Mass matrix inverse for real elements.

    Notes:
        * See J. Chan, R. Hewett, T. Warburton, Weight-adjusted discontinuous
            Galerkin methods: curvilinear meshes, August 2016, p. 6
    """

    Parent = factory_physinnerproduct2d(classRegion)

    class WADGMassMatrixInverse(Parent):

        __slots__ = ['bw', 'nm', 'C', 'Tmp', 'Ph', 'invJ']

        def __init__(self, key):
            """Constructor of PhysMassMatrix objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.bw = factory_physbackward2d(classRegion)(key)
            self.nm = ExpansionSize.get(self.G, self.o)
            self.C = np.zeros(self.nm)
            self.Tmp = np.zeros(self.nm)
            self.Ph = np.zeros(self.n2d)
            self.invJ = np.zeros(self.n2d)

        def __call__(self, J, invM):
            """Computes the mass matrix of an real element.

            Args:
                J (numpy.array) [in]: values of the jacobian (determinant of
                    the Jacobian matrix) of the complete standard to real
                    mapping (xi -> area coords -> x). Integration weights take
                    care of the eta->xi jacobian.

                invM (numpy.array) [out]: WADG approximation of the mass matrix
                    inverse.

            """
            np.reciprocal(J, self.invJ)  # invJ = 1/J

            for ii in range(self.nm):

                self.C[ii] = 1.0

                self.bw.at_ips(self.C, self.Ph)

                Parent.__call__(self, self.Ph, self.invJ, self.Tmp)

                invM[ii, :] = self.Tmp

                self.C[ii] = 0.0

            # Filter
            idx = invM < 1E-14
            invM[idx] = 0.0

    return WADGMassMatrixInverse


# end of manticore.lops.modal_dg.phyops2d.factory_WADG_invmassmatrix


def factory_mass(classRegion):
    """Factory of function that computes the mass matrix of an real element.

    Args:
        classRegion (dgtypes.factory_class_region.Region) [in]: type of DG
            standard element.

        key (ExpansionKey) [in]:

        J (numpy.array) [in]: values of the jacobian (determinant of the
            Jacobian matrix) of the complete standard to real mapping
            (xi -> area coords -> x). Integration weights take care of
            the eta->xi jacobian.

        M (numpy.array) [out]: mass matrix.
    """

    class_op = factory_physmassmatrix(classRegion)

    def mass(key, J, M):
        op = class_op(key)
        op(J, M)

    return mass


# end of manticore.lops.modal_dg.phyops2d.factory_mass


def factory_wadg_inverse_mass(classRegion):
    """Factory of function that computes the WAD mass matrix inverse
    approximation of an real element.

    Args:
        classRegion (dgtypes.factory_class_region.Region) [in]: type of DG
            standard element.

        key (ExpansionKey) [in]:

        J (numpy.array) [in]: values of the jacobian (determinant of the
            Jacobian matrix) of the complete standard to real mapping
            (xi -> area coords -> x). Integration weights take care of
            the eta->xi jacobian.

        invM (numpy.array) [out]: WADG approximation of the mass matrix
            inverse.
    """

    class_op = factory_WADG_invmassmatrix(classRegion)

    def WADG_inverse_mass(key, J, invM):
        op = class_op(key)
        op(J, invM)

    return WADG_inverse_mass


# end of manticore.lops.modal_dg.phyops2d.factory_wadg_inverse_mass


def factory_inverse_mass_operator(region_adapter):

    if region_adapter == RegionAdapter.Default:

        class InverseMassOperator:
            """Cholesky decomposition and linear system solution. """

            def decompose(Mass):
                """Performs the Cholesky decomposition of the matrix Mass.

                Args:
                    Mass (numpy.array) [in]: mass matrix.

                Returns:
                    tuple(Cholesky decomposition, Boolean)

                """
                return sla.cho_factor(Mass, overwrite_a=True)

            def eval(chol_Mass, C):
                """Computes {x}=[chol_Mass]^{-1}{C}.

                Args:
                    chol_Mass tuple(Cholesky decomposition, Boolean) [in]:
                        Cholesky decomposition of a mass matrix. Output of
                        scipy.linalg.cho_factor.

                    C (numpy.array) [inout]: {x}=[chol_Mass]^{-1}{C}, {C}={x}
                """
                # x = sla.cho_solve(chol_Mass, C, overwrite_b=True)
                # C *= 0.
                # C += x

                C = sla.cho_solve(chol_Mass, C, overwrite_b=True)

        return InverseMassOperator

    elif region_adapter == RegionAdapter.Regular:

        class InverseMassOperator:
            """Cholesky decomposition and linear system solution. """

            def decompose(Mass):
                """Performs the Cholesky decomposition of the matrix Mass.

                Args:
                    Mass (numpy.array) [in]: mass matrix.

                Returns:
                    tuple(Cholesky decomposition, Boolean)

                """
                return sla.cho_factor(Mass, overwrite_a=True)

            def eval(chol_Mass, C):
                """Computes {x}=[chol_Mass]^{-1}{C}.

                Args:
                    chol_Mass tuple(Cholesky decomposition, Boolean) [in]:
                        Cholesky decomposition of a mass matrix. Output of
                        scipy.linalg.cho_factor.

                    C (numpy.array) [inout]: {x}=[chol_Mass]^{-1}{C}, {C}={x}
                """

                diag = np.diag(chol_Mass[0])

                C /= (diag**2)

        return InverseMassOperator

    elif region_adapter == RegionAdapter.WADG:

        class InverseMassOperator:
            """Solution based on the WADG mass matrix inverse approximation.
            """

            def decompose(inv_Mass):
                pass

            def eval(inv_Mass, C):
                """Computes {x}=[Mass]^{-1}{C}.

                Args:
                    inv_Mass(numpy.array) [in]: WADG mass matrix inverse
                        approximation.

                    C (numpy.array) [inout]: {x}=[Mass]^{-1}{C}, {C}={x}
                """
                x = np.dot(inv_Mass, C)
                np.copyto(C, x)

            def solve(inv_Mass, C, x):
                """Computes {x}=[Mass]^{-1}{C}.

                Args:
                    inv_Mass(numpy.array) [in]: WADG mass matrix inverse
                        approximation.

                    C (numpy.array) [in]: {x}=[Mass]^{-1}{C}

                    x (numpy.array) [out]:
                """
                np.dot(inv_Mass, C, out=x)

        return InverseMassOperator

    elif region_adapter == RegionAdapter.RWADG:

        class InverseMassOperator:
            """Solution based on the WADG mass matrix inverse approximation.
            """

            def decompose(inv_Mass):
                pass

            def eval(inv_Mass, C):
                """Computes {x}=[Mass]^{-1}{C}.

                Args:
                    inv_Mass(numpy.array) [in]: WADG mass matrix inverse
                        approximation.

                    C (numpy.array) [inout]: {x}=[Mass]^{-1}{C}, {C}={x}
                """
                diag = np.diag(inv_Mass)
                C *= diag

            def solve(inv_Mass, C, x):
                """Computes {x}=[Mass]^{-1}{C}.

                Args:
                    inv_Mass(numpy.array) [in]: WADG mass matrix inverse
                        approximation.

                    C (numpy.array) [in]: {x}=[Mass]^{-1}{C}

                    x (numpy.array) [out]:
                """
                diag = np.diag(inv_Mass)
                np.multiply(C, diag, out=x)

        return InverseMassOperator

    else:
        raise AssertionError("Incorrect region_adapter!")


# end of manticore.lops.modal_dg.phyops2d.factory_inverse_mass_operator


def factory_physforward(classRegion, region_adapter=RegionAdapter.WADG):
    """ Forward transformation evaluation.

    Transformation from the physical space to the coefficient space.

    """

    Parent = factory_physinnerproduct2d(classRegion)

    if region_adapter == RegionAdapter.Default:

        class PhysForward(Parent):
            __slots__ = []

            def __init__(self, key):
                """Constructor of PhysForward objects.

                Args:

                    key (ExpansionKey): required for the initialization of
                        Parent.
                """

                Parent.__init__(self, key)

            def __call__(self, J, chol_Mass, Ph, C):
                """Forward transformation evaluation.

                Args:

                    J (numpy.array) [in]: values of the jacobian
                        (determinant of the Jacobian matrix) of the complete
                        standard to real mapping (xi -> area coords ->
                        x). Integration weights take care of the eta->xi
                        jacobian.

                    chol_Mass tuple(Cholesky decomposition, Boolean) [in]:
                        Cholesky decomposition of a mass matrix. Output of
                        scipy.linalg.cho_factor.

                    Ph (numpy.array) [in]: Physical values

                    C (numpy.array) [out]: Resulting coefficients
                """
                Parent.__call__(self, Ph, J, C)

                x = sla.cho_solve(chol_Mass, C, overwrite_b=True)
                np.copyto(C, x)

        return PhysForward

    elif region_adapter == RegionAdapter.Regular:

        class PhysForward(Parent):
            __slots__ = []

            def __init__(self, key):
                """Constructor of PhysForward objects.

                Args:

                    key (ExpansionKey): required for the initialization of
                        Parent.
                """

                Parent.__init__(self, key)

            def __call__(self, J, chol_Mass, Ph, C):
                """Forward transformation evaluation.

                Args:

                    J (numpy.array) [in]: values of the jacobian
                        (determinant of the Jacobian matrix) of the complete
                        standard to real mapping (xi -> area coords ->
                        x). Integration weights take care of the eta->xi
                        jacobian.

                    chol_Mass tuple(Cholesky decomposition, Boolean) [in]:
                        Cholesky decomposition of a mass matrix. Output of
                        scipy.linalg.cho_factor.

                    Ph (numpy.array) [in]: Physical values

                    C (numpy.array) [out]: Resulting coefficients
                """
                Parent.__call__(self, Ph, J, C)

                diag = np.diag(chol_Mass[0])

                C /= (diag**2)

        return PhysForward

    elif ((region_adapter == RegionAdapter.WADG) or
          (region_adapter == RegionAdapter.RWADG)):

        class PhysForward(Parent):
            __slots__ = []

            def __init__(self, key):
                """Constructor of PhysForward objects.

                Args:

                    key (ExpansionKey): required for the initialization of
                        Parent.
                """

                Parent.__init__(self, key)

            def __call__(self, J, inv_Mass, Ph, C):
                """Forward transformation evaluation.

                Args:

                    J (numpy.array) [in]: values of the jacobian
                        (determinant of the Jacobian matrix) of the complete
                        standard to real mapping (xi -> area coords ->
                        x). Integration weights take care of the eta->xi
                        jacobian.

                    inv_Mass(numpy.array) [in]: WADG mass matrix inverse
                        approximation.

                    Ph (numpy.array) [in]: Physical values

                    C (numpy.array) [out]: Resulting coefficients
                """
                Parent.__call__(self, Ph, J, C)

                x = np.dot(inv_Mass, C)
                np.copyto(C, x)

        return PhysForward

    else:
        raise AssertionError("Incorrect region_adapter!")


# end of manticore.lops.modal_dg.phyops2d.factory_physmassmatrix
