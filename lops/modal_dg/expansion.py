#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Interpolation expansion abstractions
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

from collections import namedtuple
import manticore.services.const as const
from manticore.services.mconsts import CTEs
from manticore.lops.modal_dg.gauss import GaussKey
from manticore.lops.modal_dg.gauss import StreamlinedGaussKey
from manticore.lops.modal_dg.prinfunc import PrincipalFunctionKey
from manticore.lops.modal_dg.dgtypes import GaussPointsType
from manticore.lops.modal_dg.dgtypes import PrincipalFunctionType
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.services.datatypes import FlyweightMixin

hashblExpansionKey = namedtuple('hashblExpansionKey', ['order', 'nip'])


class ExpansionKey(FlyweightMixin):
    """Key to the composite properties of a DG polynomial expansion.

    Notes:

        * We have decided to keep just a single order and a single
              number of integration points for all 2 axis.

    """

    number_of_superficial_keys  = 2
    mumber_of_edge_combinations = 5
    number_of_streamed_weights  = 2

    # Superficial dispatch table: principal function indices
    #                 Qd Tr
    dispatchTable = [
        [0, 0],  # x
        [0, 1]]  # y

    UU = const.INVALID_RESULT

    #
    # Geometry X Local Face Number X Local coord -> Function Index in self.fkeys
    #
    # Function indices:
    #
    #       +----+----+----+
    #       | 00 | -1 | +1 |
    # +-----+----+----+----+
    # |  P  |  0 |  1 |  2 |
    # +-----+----+----+----+
    # | PQ  |  3 |  4 |  X |
    # +-----+----+----+----+
    #
    faceDispatchTable = [
        [[0, 1], [0, 2], [1, 0], [2, 0]],  # DG_Quadrangle
        [[0, 4], [1, 3], [2, 3], [UU, UU]]]  # DG_Triangle

    #    face 0, face 1, face 2, face 3
    #    xi2=-1, xi2=+1, xi1=-1, xi1=+1

    def __init__(self, p_order, nip):
        """Constructor for ExpansionKey objects.

        Attributes:

            p_order (int): maximum expansion order in each direction.

            nip (int): number of integration points **in each
                direction**. Client code must initialize the correct
                number according to the type of geometrical region
                where the expansion must be evaluated (2D or 1D as 1D
                edges may require one more point here).

            keys (list(PrincipalFunctionKey)): keys to the principal
                functions of the 2D expansion.

            fkeys (list(PrincipalFunctionKey)): keys to the principal
                functions of the expansion restricted to the edges of
                elements.

            skeys (list(StreamlinedGaussKey)): keys to the streamlined
                version of integration weights.

        """
        self.__data = hashblExpansionKey(int(p_order), int(nip))

        self.keys = []
        self.fkeys = []
        self.skeys = []

        #----------------------------------------------------------------------
        # Superficial keys (indices from dispatchTable)
        #----------------------------------------------------------------------
        # A-function
        # 0
        self.keys.append(
            PrincipalFunctionKey(
                PrincipalFunctionType.OrthoPrincFuncA,
                order=self.__data.order,
                gauss_key=GaussKey(GaussPointsType.GaussLegendre,
                                   self.__data.nip)))

        # B-function
        # 1
        self.keys.append(
            PrincipalFunctionKey(
                PrincipalFunctionType.OrthoPrincFuncB,
                order=self.__data.order,
                gauss_key=GaussKey(GaussPointsType.GaussJacobi10,
                                   self.__data.nip)))

        #----------------------------------------------------------------------
        # Edge keys (indices from faceDispatchTable)
        #----------------------------------------------------------------------

        #-- PSI_P,Q,R^A -------------------------------------------------------

        #----------------------------------------------------------------------
        # According to K&S p.558, the number of integration points
        # on each face must be n1d+1 for obtaining the correct mesh
        # convergence.
        #----------------------------------------------------------------------

        # 0 0
        self.fkeys.append(
            PrincipalFunctionKey(
                PrincipalFunctionType.OrthoPrincFuncA,
                order=self.__data.order,
                gauss_key=GaussKey(GaussPointsType.GaussLegendre,
                                   self.__data.nip+1)))  # GJL00

        # 3 1
        self.fkeys.append(
            PrincipalFunctionKey(
                PrincipalFunctionType.OrthoPrincFuncA,
                order=self.__data.order,
                gauss_key=GaussKey(GaussPointsType.NegativeOne,
                                   self.__data.nip+1)))  # -1.0

        # 4 2
        self.fkeys.append(
            PrincipalFunctionKey(
                PrincipalFunctionType.OrthoPrincFuncA,
                order=self.__data.order,
                gauss_key=GaussKey(GaussPointsType.PositiveOne,
                                   self.__data.nip+1)))  # +1.0

        #-- PSI_PR,PQ^B --------------------------------------------------------

        # 5 3
        self.fkeys.append(
            PrincipalFunctionKey(
                PrincipalFunctionType.OrthoPrincFuncB,
                order=self.__data.order,
                gauss_key=GaussKey(GaussPointsType.GaussLegendre,
                                   self.__data.nip+1)))  # GJL00

        # 8 4
        self.fkeys.append(
            PrincipalFunctionKey(
                PrincipalFunctionType.OrthoPrincFuncB,
                order=self.__data.order,
                gauss_key=GaussKey(GaussPointsType.NegativeOne,
                                   self.__data.nip+1)))  # -1.0

        #----------------------------------------------------------------------
        # Streamed keys
        #----------------------------------------------------------------------

        self.skeys.append(
            StreamlinedGaussKey(StdRegionType.DG_Quadrangle, self.__data.nip))
        self.skeys.append(
            StreamlinedGaussKey(StdRegionType.DG_Triangle, self.__data.nip))

    @property
    def order(self):
        return self.__data.order

    @property
    def n1d(self):
        return self.__data.nip

    @property
    def n2d(self):
        return self.__data.nip * self.__data.nip

    @property
    def key(self):
        return self.__data

    def n1d_on_face(self, region_type, face_id, coord):
        """ Returns the number of integration points over a edge.

        Args:
            region_type (dgtypes.StdRegionType)

            face_id  (int): [0, number_of_faces[

            coord (int): 1 or 2.

        Returns:
            int

        """
        return self.p_on_face(region_type, face_id, coord).shape[0]

    def psi_a(self):
        return self.keys[0]

    def psi_b(self):
        return self.keys[1]

    def psi_on_face(self, region_type, face_id, coord):
        """ Returns the key to the principal function over a edge.

        Args:
            region_type (dgtypes.StdRegionType)

            face_id  (int): [0, number_of_faces[

            coord (int): 1 or 2.

        Returns:
            PrincipalFunctionKey

        """
        return self.fkeys[ExpansionKey.faceDispatchTable[region_type][face_id][
            coord - 1]]

    def set_gauss_quadrature(self, gauss_quad):
        """Stores reference to JacobiGaussQuadratures.

        Notes:

            * This method must be executed before calls to wp, wp_on_edge.

        """
        self.gauss_quad = gauss_quad

    def set_strm_gauss_quadrature(self, strm_gauss):
        """Stores reference to a StreamlinedGaussWeights object.

        Notes:

            * This method must be executed before calls to sw.

        """
        self.strm_gauss = strm_gauss

    def set_diff_matrix(self, diff_mat):
        """Stores reference to a CollocationDiffMat object.

        Notes:

            * This method must be executed before calls to D.

        """
        self.diff_mat = diff_mat

    def set_principal_functions(self, prin_funcs):
        """Stores reference to a PrincipalFunctions object.

        Notes:

            * This method must be executed before calls to principal
              functions accesses.

        """
        self.prin_funcs = prin_funcs

    def set_face_principal_functions(self, fprin_funcs):
        """Stores reference to a FacePrincipalFunctions object.

        Notes:

            * This method must be executed before calls to face
              principal functions accesses.

        """
        self.face_prin_funcs = fprin_funcs

    def wp(self, region_type, coord):
        key = self.keys[ExpansionKey.dispatchTable[coord - 1][region_type]]

        return self.gauss_quad(key.gauss_key)

    def wp_on_face(self, region_type, face_id, coord):
        key = self.psi_on_face(region_type, face_id, coord)

        return self.gauss_quad(key.gauss_key)

    def p(self, region_type, coord):
        key = self.keys[ExpansionKey.dispatchTable[coord - 1][region_type]]

        return self.gauss_quad.ips(key.gauss_key)

    def p_on_face(self, region_type, face_id, coord):
        key = self.psi_on_face(region_type, face_id, coord)

        return self.gauss_quad.ips(key.gauss_key)

    def w(self, region_type, coord):
        key = self.keys[ExpansionKey.dispatchTable[coord - 1][region_type]]

        return self.gauss_quad.weights(key.gauss_key)

    def w_on_face(self, region_type, face_id, coord):
        key = self.psi_on_face(region_type, face_id, coord)

        return self.gauss_quad.weights(key.gauss_key)

    def sw(self, region_type):
        return self.strm_gauss(self.skeys[region_type])

    def D(self, region_type, coord):
        key = self.keys[ExpansionKey.dispatchTable[coord - 1][region_type]]

        return self.diff_mat(key.gauss_key)


## end of manticore.lops.modal_dg.expansion.ExpansionKey
