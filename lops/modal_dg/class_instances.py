#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Instantiation of classes defined inside factories
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

from manticore.lops.modal_dg.dgtypes import (
    StdRegionType, RegionAdapter, factory_class_region )
from manticore.lops.modal_dg.phyops2d import (
    factory_physforward, factory_physintegrator2d, factory_wadg_inverse_mass,
    factory_WADG_invmassmatrix, factory_physinnerproduct2d,
    factory_physinnerproductgrad2d, factory_inverse_mass_operator,
    factory_physbackward2d, factory_physevaluator2d,
    factory_physdifferentiator2d )
from manticore.lops.modal_dg.phyops1d import (
    factory_physinnerproduct1d, factory_physevaluator1d )
from manticore.lops.modal_dg.stdops1d import (
    factory_stdbackward1d )

#
#
class_quad_region = factory_class_region(StdRegionType.DG_Quadrangle)
class_tria_region = factory_class_region(StdRegionType.DG_Triangle)

#
#
class_quad_wadg_physforward = factory_physforward(
    class_quad_region, RegionAdapter.WADG)
class_quad_rwadg_physforward = factory_physforward(
    class_quad_region, RegionAdapter.RWADG)
class_tria_wadg_physforward = factory_physforward(
    class_tria_region, RegionAdapter.WADG)
class_tria_rwadg_physforward = factory_physforward(
    class_tria_region, RegionAdapter.RWADG)

#
#
class_quad_physbackward2d = factory_physbackward2d(class_quad_region)
class_tria_physbackward2d = factory_physbackward2d(class_tria_region)

#
#
class_quad_physintegrator2d = factory_physintegrator2d(class_quad_region)
class_tria_physintegrator2d = factory_physintegrator2d(class_tria_region)

#
#
def_quad_wadg_inverse_mass = factory_wadg_inverse_mass(class_quad_region)
def_tria_wadg_inverse_mass = factory_wadg_inverse_mass(class_tria_region)

#
#
class_quad_wadg_inverse_mass = factory_WADG_invmassmatrix(class_quad_region)
class_tria_wadg_inverse_mass = factory_WADG_invmassmatrix(class_tria_region)

#
class_wadg_inverse_mas_op  = factory_inverse_mass_operator(RegionAdapter.WADG)
class_rwadg_inverse_mas_op = factory_inverse_mass_operator(RegionAdapter.RWADG)

#
#
class_quad_physinnerproductgrad2d = factory_physinnerproductgrad2d(
    class_quad_region)
class_tria_physinnerproductgrad2d = factory_physinnerproductgrad2d(
    class_tria_region)

#
#
class_quad_physinnerproduct2d = factory_physinnerproduct2d(class_quad_region)
class_tria_physinnerproduct2d = factory_physinnerproduct2d(class_tria_region)

#
#
class_quad_physevaluator2d = factory_physevaluator2d(class_quad_region)
class_tria_physevaluator2d = factory_physevaluator2d(class_tria_region)

#
#
class_quad_physdifferentiator2d= factory_physdifferentiator2d(class_quad_region)
class_tria_physdifferentiator2d= factory_physdifferentiator2d(class_tria_region)

#
#
class_quad_physinnerproduct1d = factory_physinnerproduct1d(class_quad_region)
class_tria_physinnerproduct1d = factory_physinnerproduct1d(class_tria_region)

#
#
class_quad_stdbackward1d = factory_stdbackward1d(class_quad_region)
class_tria_stdbackward1d = factory_stdbackward1d(class_tria_region)

#
#
class_quad_physevaluator1d = factory_physevaluator1d(class_quad_region)
class_tria_physevaluator1d = factory_physevaluator1d(class_tria_region)
