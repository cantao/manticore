#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Principal functions accorting to Karniadakis & Sherwin 2nd ed,
# Chapter 3, p.101.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from collections import namedtuple
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.dgtypes import FacePrincipalFunctionType
from manticore.lops.modal_dg.prinfunc import psi_a_eval_policy
from manticore.lops.modal_dg.prinfunc import psi_b_eval_policy
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.geomfactors import FaceDocking
from manticore.services.datatypes import FlyweightMixin

hashblFacePrinFuncKey = namedtuple('hashblFacePrinFuncKey', [
    'order', 'nip', 'activeV', 'activeF', 'passiveV', 'passiveF', 'type'
])


class FacePrincipalFunctionKey(FlyweightMixin):
    """Key indexer to principal functions facial tables.

    """

    t = [[
        FacePrincipalFunctionType.QuadQuad, FacePrincipalFunctionType.TriaQuad
    ], [
        FacePrincipalFunctionType.QuadTria, FacePrincipalFunctionType.TriaTria
    ]]

    __slots__ = ['__data']

    def __init__(self, activeV, activeF, passiveV, passiveF, order, nip):
        """Constructor for FacePrincipalFunctionKey objects.

        Args:

            order (int): Polinomial order (assuming P = Q) of the passive
                element.

            nip (int): Number of integration points on the face of the
                active/passive pair (this number is defined by the
                element having the integrand with higher order).

            activeV (StdRegionType): Active volume type

            activeF (int): Active face local id

            passiveV (StdRegionType): Passive volume type

            passiveF (int): Passive face local id

        """

        if ((activeV in StdRegionType) and (passiveV in StdRegionType)):

            self.__data = hashblFacePrinFuncKey(
                order,
                int(nip), activeV,
                int(activeF), passiveV,
                int(passiveF), FacePrincipalFunctionKey.t[passiveV][activeV])

        else:
            raise AssertionError("Incorrect initialization!")

    @property
    def type(self):
        return self.__data.type

    @property
    def nip(self):
        return self.__data.nip

    @property
    def order(self):
        return self.__data.order

    @property
    def activeV(self):
        return self.__data.activeV

    @property
    def activeF(self):
        return self.__data.activeF

    @property
    def passiveV(self):
        return self.__data.passiveV

    @property
    def passiveF(self):
        return self.__data.passiveF

    @property
    def key(self):
        return self.__data

    def __str__(self):
        msg = "%s, Active [%s, Face: %s], Passive [%s, Face: %s, Order: %s]" % (
            self.type, self.activeV, self.activeF, self.passiveV,
            self.passiveF, self.order)

        msg += ", %s i.p.s" % (self.nip)

        return msg

    def __repr__(self):
        msg = "<%s object, id:%s (%s)>" % (self.__class__, id(self),
                                           self.__str__())

        return msg


# end of manticore.lops.modal_dg.faceprinfunc.FacePrincipalFunctionKey


class FacePrincipalFunctionTable:
    """Table to record values of a principal function on a face.

    Notes:

        * The FacePrincipalFunctionsTable class is organized as a
            list(np.array) effectivelly emulating a matrix. The size
            of the list corresponds to the number of combined modes of
            the neighbor (passive element), while the np.array size is
            the number of integration points on the face shared para
            the active/passive pair.

    """

    def __init__(self, key, gauss_quad):
        """Constructor for FacePrincipalFunctionTable objects.

        Attributes:

            table (list(np.array)): table of Principal Functions,
                stored by order,
                table[ii][jj] = ii-th integration point, jj-th mode

            tableT (list(np.array)): table of Principal Functions,
                stored by integration point,
                tableT[jj][ii] = jj-th mode, ii-th integration point

            gauss_quad (JacobiGaussQuadratures): weights and
                integration points.

        Args:

            key (FacePrincipalFunctionKey)

            gauss_quad (JacobiGaussQuadratures): weights and
                integration points table.

        """

        self.gauss_quad = gauss_quad

        nModes = ExpansionSize.get(key.passiveV, key.order)

        self.__init_tables(key.nip, nModes)

        self.__nip = key.nip
        self.__nmodes = nModes

        if key.passiveV == StdRegionType.DG_Quadrangle:
            self.__passiveQuadrangleTable(key)
        elif key.passiveV == StdRegionType.DG_Triangle:
            self.__passiveTriangleTable(key)
        else:
            raise AssertionError("Incorrect initialization!")

    @property
    def nip(self):
        return self.__nip

    @property
    def nmodes(self):
        return self.__nmodes

    def __str__(self):
        nip = self.nip
        nm = self.nmodes

        msg = "table[(%s points,%s modes)]" % (nip, nm)

        msg += "\nTable ="

        for ii in range(nip):
            msg += "\n%s" % self.table[ii]

        return msg

    def __repr__(self):
        msg = "<%s object, id:%s, table:(%s points,%s modes)>" % (
            self.__class__, id(self), self.nip, self.nmodes)

    def row(self, ii):
        """Access the ii-th table row. Values of all modes at the ii-th integration point.

        Args:

            ii (int): row index, [0, number_of_integration_points[

        Returns:

            np.array: the ii-th table row

        """
        return self.table[ii]

    def column(self, jj):
        """Access the jj-th table column. Values of the jj-th mode at all the integration points.

        Args:

            jj (int): column index, [0, number_of_modes[

        Returns:

            np.array: the jj-th table column

        """
        return self.tableT[jj]

    def __init_tables(self, nip, nm):

        self.table = [np.zeros(nm) for i in range(nip)]
        self.tableT = [np.zeros(nip) for i in range(nm)]

    def __passiveQuadrangleTable(self, key):

        #
        # Be careful when using ExpansionKey to get integration
        # points on faces: the second argument of the constructor
        # is the one-dimensional number of integration points for
        # numerical integration INSIDE the element. The number of
        # integration points generated on faces by ExpansionKey is
        # the second argument of the constructor PLUS ONE. Thus,
        # if you know the number of integration points on face (as
        # here) you must SUBTRACT ONE from it to get the correct
        # integration points on faces (see the assertion below).
        #
        ktemp = ExpansionKey.get_instance(key.order, key.nip-1)
        ktemp.set_gauss_quadrature(self.gauss_quad)

        p1 = ktemp.p_on_face(key.activeV, key.activeF, 1)
        p2 = ktemp.p_on_face(key.activeV, key.activeF, 2)

        n1 = p1.shape[0]
        n2 = p2.shape[0]
        nip = key.nip

        assert n1 * n2 == nip  # Checks if n1 or n2 is 1 as must be

        oP1 = key.order + 1

        psiAp = [psi_a_eval_policy(pp) for pp in range(oP1)]
        psiAq = [psi_a_eval_policy(qq) for qq in range(oP1)]

        docker = FaceDocking()
        docker.setup(key.activeV, key.activeF, key.passiveV, key.passiveF)
        p1p, p2p = docker.eval(p1, p2)

        mode = 0

        for pp in range(oP1):
            for qq in range(oP1):
                self.tableT[mode] = psiAp[pp](p1p) * psiAq[qq](p2p)

                for ii in range(nip):
                    self.table[ii][mode] = self.tableT[mode][ii]

                mode += 1

    def __passiveTriangleTable(self, key):

        #
        # Be careful when using ExpansionKey to get integration
        # points on faces: the second argument of the constructor
        # is the one-dimensional number of integration points for
        # numerical integration INSIDE the element. The number of
        # integration points generated on faces by ExpansionKey is
        # the second argument of the constructor PLUS ONE. Thus,
        # if you know the number of integration points on face (as
        # here) you must SUBTRACT ONE from it to get the correct
        # integration points on faces (see the assertion below).
        #
        ktemp = ExpansionKey.get_instance(key.order, key.nip-1)
        ktemp.set_gauss_quadrature(self.gauss_quad)

        p1 = ktemp.p_on_face(key.activeV, key.activeF, 1)
        p2 = ktemp.p_on_face(key.activeV, key.activeF, 2)

        n1 = p1.shape[0]
        n2 = p2.shape[0]
        nip = key.nip

        assert n1 * n2 == nip  # Checks if n1 or n2 is 1 as must be

        oP1 = key.order + 1

        psiAp = psi_a_eval_policy(0)
        psiBpq = psi_b_eval_policy(0, 0)

        docker = FaceDocking()
        docker.setup(key.activeV, key.activeF, key.passiveV, key.passiveF)
        p1p, p2p = docker.eval(p1, p2)

        mode = 0

        for pp in range(oP1):
            psiAp.configure(pp)

            for qq in range(oP1 - pp):
                psiBpq.configure(pp, qq)
                #self.tableT[ mode ] = psiAp(p1p) * psiBpq(p2p)
                # The above line was rewritten below avoiding some
                # temporary allocation:
                psiBpq(p2p, self.tableT[mode])
                self.tableT[mode] *= psiAp(p1p)

                for ii in range(nip):
                    self.table[ii][mode] = self.tableT[mode][ii]

                mode += 1


# end of manticore.lops.modal_dg.faceprinfunc.FacePrincipalFunctionTable


class FacePrincipalFunctions:
    """Basically a hash table of FacePrincipalFunctionTable objects indexed by FacePrincipalFunctionKey objects.

    Notes:

        * Each table entry is created when first called.

        * All FacePrincipalFunctionTable objects of this table are
                associated to the same JacobiGaussQuadratures object.

    """

    def __init__(self, gauss_quad):
        """Constructor for FacePrincipalFunctions objects.

        Attributes:

            gauss_quad (JacobiGaussQuadratures): weights and
                integration points used to build
                PrincipalFunctionTable objects.

            tables (dict(FacePrincipalFunctionTable)): actual hash table
                of FacePrincipalFunctionTable objects indexed as:
                [FacePrincipalFunctionKey]: FacePrincipalFunctionTable

        Args:

            gauss_quad (JacobiGaussQuadratures): weights and
                integration points.

        """
        self.gauss_quad = gauss_quad

        # dict {FacePrincipalFunctionKey: FacePrincipalFunctionTable}
        self.tables = {}

    def __call__(self, fpf_key):
        """Returns a FacePrincipalFunctionTable object built according to key.

        Args:

            fpf_key (FacePrincipalFunctionKey)

        Returns:

            FacePrincipalFunctionTable

        Notes:

           * Each table entry is created when first
             called. Consecutive calls with the same argument retrive
             an already computed FacePrincipalFunctionTable object from
             the hash table.

        """

        assert type(fpf_key) is FacePrincipalFunctionKey

        if fpf_key.key not in self.tables:
            self.tables[fpf_key.key] = FacePrincipalFunctionTable(
                fpf_key, self.gauss_quad)

        return self.tables[fpf_key.key]

    def __str__(self):
        n = len(self.tables)

        msg = ""

        for key in self.tables:
            msg += "[%s: %s]\n" % (key, self.tables[key])

        msg += "[%s tables]" % (n)

        return msg

    def __repr__(self):
        msg = "<%s object, id:%s, %s tables>" % (self.__class__, id(self),
                                                 len(self.tables))

        return msg


# end of manticore.lops.modal_dg.faceprinfunc.FacePrincipalFunctions
