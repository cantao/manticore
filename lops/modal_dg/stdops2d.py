#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Operations on standard 2D regions.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore.lops.modal_dg.stdmixins2d as _2d_mx
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.prinfunc import psi_a_eval_policy
from manticore.lops.modal_dg.prinfunc import psi_b_eval_policy
from manticore.lops.modal_dg.geomfactors import factory_colapsed_to_std_maps
from manticore.lops.modal_dg.geomfactors import factory_form_factors
from manticore.lops.modal_dg.expantools import ExpansionSize


def factory_stdbackward2d(classRegion):
    """Factory of classes that perform backward tranformation on standard elements.

    Backward transformation: reconstruction from coefficient space
    $\hat{u}$ to physical space $u$ on two dimensions according
    to element's shape information (classRegion) and an ExpansionKey
    object.

    Notes:
        * See K&S, page 171
        * See manticore.lops.modal_dg.dgtypes.factory_class_region
        * See manticore.lops.modal_dg.stdops1d.py
    """

    # Using stdmixins2d factories to built a class that provides
    # access to principal functions, number of modes and number of
    # integration points.
    Parent = _2d_mx.factory_princ_func_mixin(
        _2d_mx.factory_number_of_modes(
            _2d_mx.factory_number_of_ip(classRegion)))

    class StdBackwardBase(Parent):
        """Base class for all specializations of StdBackward."""

        __slots__ = ['nm', 'Fp', 'Psi']

        def __init__(self, key):
            """Constructor of StdBackwardBase objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.nm = self.o + 1  # Number of modes on
            # non-colapsed directions.

            self.Fp = np.zeros(self.nm)  # Auxiliar storage
            self.Psi = np.zeros(self.nm)  # Auxiliar storage

    # end of factory_stdbackward2d.StdBackwardBase

    # Specializations
    region = classRegion()

    if region.G == StdRegionType.DG_Quadrangle:

        class StdBackward(StdBackwardBase):
            """Backward transformation for StdRegionType.DG_Quadrangle regions.

            """
            __slots__ = []

            def __init__(self, key):
                """Constructor of StdBackward objects.

                Args:

                    key (ExpansionKey): required for the
                        initialization of Parent.
                """
                StdBackwardBase.__init__(self, key)

            def at_ips(self, C, Ph):
                """Backward transformation at all integration points.

                Args:

                    C (Numpy.array) [in]: real-valued (P*Q, 1) array
                        (P = Q = number_of_modes) containing the
                        coeficients of the polynomial expansion
                        \sum_p\sum_q C_{pq}\phi_p\phi_q

                    Ph (Numpy.array) [out]: real-valued (nip*nip, 1)
                        array containing the physical values of the
                        interpolation at the integration poits ordered
                        as Phi[ii+jj*nip], ii=0,..., nip in xi_1,
                        jj=0,..., nip in xi_2.

                """

                nm = self.nm
                n1d = self.n1d
                Cpq = np.reshape(C, (nm, nm))  # This reshape returns a view.
                Ps1 = self.PF1
                Ps2 = self.PF2
                phys = 0

                for jj in range(n1d):
                    np.dot(Cpq, Ps2.row(jj), self.Fp)

                    for ii in range(n1d):
                        Ph[phys] = np.dot(self.Fp, Ps1.row(ii))
                        phys += 1

            def at_point(self, C, xi_1, xi_2):
                """Backward transformation at local coordinates (xi_1, xi_2)
                of the standard region.

                Args:

                    C (Numpy.array) [in]: real-valued (P*Q, 1) array
                        (P = Q = number_of_modes) containing the
                        coeficients of the polynomial expansion
                        \sum_p\sum_q C_{pq}\psi_p\psi_q

                    xi_1 (double): [-1, +1]

                    xi_2 (double): [-1, +1]

                Returns:

                    double

                """

                nm = self.nm

                psiQ = psi_a_eval_policy(self.o)

                for qq in range(nm):
                    psiQ.configure(qq)
                    self.Psi[qq] = psiQ(xi_2)  # The [] operator is in-place!

                np.dot(np.reshape(C, (nm, nm)), self.Psi, self.Fp)

                psiP = psi_a_eval_policy(self.o)

                for pp in range(nm):
                    psiP.configure(pp)
                    self.Psi[pp] = psiP(xi_1)

                Fx = np.dot(self.Fp, self.Psi)

                return Fx

        # end of factory_stdbackward2d.StdBackward

        return StdBackward

    elif region.G == StdRegionType.DG_Triangle:

        class StdBackward(StdBackwardBase):
            """Backward transformation for StdRegionType.DG_Triangle regions.

            """
            __slots__ = []

            def __init__(self, key):
                """Constructor of StdBackward objects.

                Args:

                    key (ExpansionKey): required for the
                        initialization of Parent.
                """
                StdBackwardBase.__init__(self, key)

            def at_ips(self, C, Ph):
                """Backward transformation at all integration points.

                Args:

                    C (Numpy.array) [in]: real-valued (P*(Q+1)/2, 1)
                        array (P = Q) containing the coeficients of
                        the polynomial expansion
                        \sum_p\sum_q C_{pq}\psi_p\psi_pq

                    Ph (Numpy.array) [out]: real-valued (nip*nip, 1)
                        array containing the physical values of the
                        interpolation at the integration poits ordered
                        as Phi[ii+jj*nip], ii=0,..., nip in xi_1,
                        jj=0,..., nip in xi_2.

                """

                n1d = self.n1d
                Ps1 = self.PF1
                Ps2 = self.PF2
                oP1 = self.nm
                phys = 0

                for jj in range(n1d):
                    psi = Ps2.row(jj)
                    mode = 0

                    for pp in range(oP1):
                        self.Fp[pp] = np.dot(C[mode:mode + oP1 - pp],
                                             psi[mode:mode + oP1 - pp])
                        mode += oP1 - pp

                    for ii in range(n1d):
                        Ph[phys] = np.dot(self.Fp, Ps1.row(ii))
                        phys += 1

            def at_point(self, C, xi_1, xi_2):
                """Backward transformation at local coordinates (xi_1, xi_2)
                of the standard region.

                Args:

                    C (Numpy.array) [in]: real-valued (P*(Q+1)/2, 1)
                        array (P = Q) containing the coeficients of
                        the polynomial expansion
                        \sum_p\sum_q C_{pq}\psi_p\psi_pq

                    xi_1 (double) [in]: \eta_1 coordinate [-1, +1]

                    xi_2 (double) [in]: \eta_2 coordinate [-1, +1[

                Returns:

                    double

                """

                oP1 = self.nm
                mode = 0

                psiPQ = psi_b_eval_policy(self.o, self.o)

                for pp in range(oP1):
                    for qq in range(oP1 - pp):
                        psiPQ.configure(pp, qq)
                        self.Psi[qq] = psiPQ(xi_2)

                    self.Fp[pp] = np.dot(C[mode:mode + oP1 - pp],
                                         self.Psi[:oP1 - pp])
                    mode += oP1 - pp

                psiP = psi_a_eval_policy(self.o)

                for pp in range(self.nm):
                    psiP.configure(pp)
                    self.Psi[pp] = psiP(xi_1)

                Fx = np.dot(self.Fp, self.Psi)

                return Fx

        # end of factory_stdbackward2d.StdBackward

        return StdBackward

    else:
        raise AssertionError("Incorrect region_type!")


# end of manticore.lops.modal_dg.stdops2d.factory_stdbackward2d


def factory_stdintegrator2d(classRegion):
    """Factory of classes that perform 2D integration of a scalar function on a
    standard element.

    Notes:
        * Values of the scalar function at the integration points must be in
            streamlined ordering; see
            manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights)
    """

    # Using stdmixins2d factories to built a class that provides
    # access to principal functions, number of modes and number of
    # integration points.
    Parent = _2d_mx.factory_sw_mixin(_2d_mx.factory_number_of_ip(classRegion))

    class StdIntegrator(Parent):

        __slots__ = []

        def __init__(self, key):
            """Constructor of StdIntegrator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

        def __call__(self, Ph):
            """Executes a 2D integration of a scalar function on a standard element.

            Args:
                Ph (numpy.array) [in]: values of a scalar function at
                    the integration points (in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).

            Returns:
                double

            """
            assert Ph.shape[0] == self.W.shape[0]

            return np.dot(Ph, self.W)

    # end of factory_stdintegrator2d.StdIntegrator

    return StdIntegrator


# end of manticore.lops.modal_dg.stdops2d.factory_stdintegrator2d


def factory_stdevaluator2d(classRegion):
    """Factory of classes that evaluate a scalar function f(x,y) over the
    integration points of a standard element.

    Notes:
        * Values of the scalar function at the integration points are
            returned in streamlined ordering; see
            manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights)
    """

    # Using stdmixins2d factories to built a class that provides
    # access to principal functions, number of modes and number of
    # integration points.
    Parent = _2d_mx.factory_wp_mixin(_2d_mx.factory_number_of_ip(classRegion))

    class StdEvaluator(Parent):

        __slots__ = ['mapp']

        def __init__(self, key):
            """Constructor of StdEvaluator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.mapp = factory_colapsed_to_std_maps(self.G)

        def __call__(self, f):
            """Evaluates a scalar function f(x,y) over the integration points
            of a standard element.

            Args:

                f (function): scalar function able to receive Numpy
                    1d arrays as arguments also returning a Numpy array.

            Returns:

                Numpy 1d array in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights

            """
            px = self.P1
            py = self.P2
            px, py = np.meshgrid(px, py)
            px_t, py_t = self.mapp.eval(np.ravel(px), np.ravel(py))

            return f(px_t, py_t)

    # end of factory_stdevaluator2d.StdEvaluator

    return StdEvaluator


# end of manticore.lops.modal_dg.stdops2d.factory_stdevaluator2d


def factory_stdinnerproduct2d(classRegion):
    """Factory of classes that evaluates the inner product of the physical
    represented values with every basis function.

    Notes:
        * See K&S, page 173-176
    """

    # Using stdmixins2d factories to built a class that provides
    # access to principal functions, number of modes and number of
    # integration points.
    Parent = _2d_mx.factory_princ_func_mixin(
        _2d_mx.factory_number_of_modes(
            _2d_mx.factory_number_of_ip(classRegion)))

    class StdInnerProductBase(Parent):
        """Base class for all specializations of StdInnerProduct."""

        __slots__ = ['nm', 'op', 'phi', 'Fpq']

        def __init__(self, key):
            """Constructor of StdInnerProductBase objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.nm = ExpansionSize.get(self.G, self.o)
            self.op = factory_stdintegrator2d(classRegion)(key)

            n = self.n1d
            self.phi = np.zeros((n, n))
            self.Fpq = np.zeros(n * n)

    # Specializations
    region = classRegion()

    if region.G == StdRegionType.DG_Quadrangle:

        class StdInnerProduct(StdInnerProductBase):

            __slots__ = []

            def __init__(self, key):
                """Constructor of StdInnerProduct objects for
                StdRegionType.DG_Quadrangle.

                Args:
                key (ExpansionKey): required for the initialization
                of Parent.
                """
                StdInnerProductBase.__init__(self, key)

            def __call__(self, Ph, Ipq):
                """Executes inner product of the physical represented values
                with every basis function.

                Args:
                    Ph (numpy.array) [in]: values of a scalar function at the
                        integration points (in streamlined ordering; see
                        gauss_strm.StreamlinedGaussWeights).
                    Iqp (numpy.array) [out]: each element of this array is the
                        the inner product of Ph wiht a basis function.

                """
                assert Ph.shape[0] == self.n2d
                assert Ipq.shape[0] == self.nm

                Ps1 = self.PF1
                Ps2 = self.PF2
                n = self.n1d

                mode = 0

                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pfA_p = Ps1.column(pp)
                    Qmax = self.Q(pp) + 1

                    for qq in range(Qmax):
                        pfA_q = Ps2.column(qq)

                        # TODO: comparar velocidade dessa solucao
                        # versus meshgrid

                        np.multiply(np.reshape(pfA_q, (n, 1)), pfA_p, self.phi)
                        np.multiply(Ph, np.ravel(self.phi), self.Fpq)

                        Ipq[mode] = self.op(self.Fpq)
                        mode += 1

            def eval(self, Ph):
                """ Evaluates all basis functions at all integration points.

                Args:
                    Ph (numpy.array) [out]: values of all basis functions at
                        the integration points (in streamlined ordering; see
                        gauss_strm.StreamlinedGaussWeights). Matrix, a basis
                        function per line, an integration point per column.
                """
                assert Ph.shape[1] == self.n2d  # columns
                assert Ph.shape[0] == self.nm  # rows

                Ps1 = self.PF1
                Ps2 = self.PF2
                n = self.n1d

                mode = 0

                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pfA_p = Ps1.column(pp)
                    Qmax = self.Q(pp) + 1

                    for qq in range(Qmax):
                        pfA_q = Ps2.column(qq)

                        # There is no gain in declaring phi as
                        # class's attribute as '=' is not an actual
                        # attribution.
                        np.multiply(np.reshape(pfA_q, (n, 1)), pfA_p, self.phi)
                        Ph[mode] = np.ravel(self.phi)
                        mode += 1

        # end of factory_stdinnerproduct2d.StdInnerProduct

        return StdInnerProduct

    elif region.G == StdRegionType.DG_Triangle:

        class StdInnerProduct(StdInnerProductBase):

            __slots__ = []

            def __init__(self, key):
                """Constructor of StdInnerProduct objects for StdRegionType.DG_Triangle.
                Args:

                    key (ExpansionKey): required for the
                        initialization of Parent.
                """
                StdInnerProductBase.__init__(self, key)

            def __call__(self, Ph, Ipq):
                """Executes inner product of a vector of physical represented 
                values with every basis function.

                Args:
                    Ph (numpy.array) [in]: values of a scalar function at the
                        integration points (in streamlined ordering; see
                        gauss_strm.StreamlinedGaussWeights).
                    Iqp (numpy.array) [out]: each element of this array is the
                        the inner product of Ph wiht a basis function.

                """
                assert Ph.shape[0] == self.n2d
                assert Ipq.shape[0] == self.nm

                Ps1 = self.PF1
                Ps2 = self.PF2
                n = self.n1d

                mode = 0
                tab = 0  # Extra indexer for triangle

                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pfA_p = Ps1.column(pp)
                    Qmax = self.Q(pp) + 1

                    adv = tab  # Extra indexer for triangle

                    for qq in range(Qmax):
                        pfB_pq = Ps2.column(adv)  # Extra indexer for triangle

                        np.multiply(
                            np.reshape(pfB_pq, (n, 1)), pfA_p, self.phi)
                        np.multiply(Ph, np.ravel(self.phi), self.Fpq)

                        Ipq[mode] = self.op(self.Fpq)
                        mode += 1
                        adv += 1

                    tab += self.o - pp + 1  # Extra indexer for triangle

            def eval(self, Ph):
                """ Evaluates all bassis functions at all integration points.

                Args:
                    Ph (numpy.array) [out]: values of all basis functions at
                        the integration points (in streamlined ordering; see
                        gauss_strm.StreamlinedGaussWeights). Matrix, a basis
                        function per line, an integration point per column.
                """
                assert Ph.shape[1] == self.n2d  # columns
                assert Ph.shape[0] == self.nm  # rows

                Ps1 = self.PF1
                Ps2 = self.PF2
                n = self.n1d

                mode = 0
                tab = 0  # Extra indexer for triangle

                Pmax = self.P() + 1

                for pp in range(Pmax):  # pp in [0, Pmax]
                    pfA_p = Ps1.column(pp)
                    Qmax = self.Q(pp) + 1

                    adv = tab  # Extra indexer for triangle

                    for qq in range(Qmax):
                        pfB_pq = Ps2.column(adv)  # Extra indexer for triangle

                        np.multiply(
                            np.reshape(pfB_pq, (n, 1)), pfA_p, self.phi)
                        Ph[mode] = np.ravel(self.phi)
                        mode += 1
                        adv += 1

                    tab += self.o - pp + 1  # Extra indexer for triangle

        # end of factory_stdinnerproduct2d.StdInnerProduct

        return StdInnerProduct

    else:
        raise AssertionError("Incorrect region_type!")


# end of manticore.lops.modal_dg.stdops2d.factory_stdinnerproduct2d


def factory_stdforward2d(classRegion):
    """Factory of classes that perform forward transformation over standard
    elements.

    Forward transformation: transformation from physical space $u$ to
    coefficient space $\hat{u}$

    Notes:
        * Values of the scalar function at the integration points must be in
            streamlined ordering; see
            manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights)
        * See K&S, page 172.
        * For standard basis functions, the forward transformation requires
            a linear systems solution. However, our basis functions are
            orthonormal and then the system's matrix is a identity matrix.
    """

    StdInnerProduct = factory_stdinnerproduct2d(classRegion)

    class StdForward(StdInnerProduct):

        __slots__ = []

        def __init__(self, key):
            """Constructor of StdIntegrator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """
            StdInnerProduct.__init__(self, key)

        def __call__(self, Ph, C):
            """Transformation from the physical space to the coefficient space.

            Args:
                Ph (numpy.array) [in]: values of a scalar function at
                    the integration points (in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).

                C (numpy.array) [out] Resulting coefficients

            """
            StdInnerProduct.__call__(self, Ph, C)

    # end of factory_stdforward2d.StdForward

    return StdForward


# end of manticore.lops.modal_dg.stdops2d.factory_stdforward2d


def factory_stddifferentiator2d(classRegion):
    """Factory of classes that perform differentiation on standard 2D elements.

    Numerical differentiation on standard elements. The differentiation is
    performed on collapsed coordinates (eta_i) and then transformed
    through the appropriate correction factors to standard space (xi_i).

    Notes:
        * See K&S, pages 147-153
    """

    # Using stdmixins2d factories to built a class that provides
    # access to differentiation matrix, integration poinst and
    # weights, number of modes and number of integration points.
    Parent = _2d_mx.factory_collocation_mixin(
        _2d_mx.factory_wp_mixin(
            _2d_mx.factory_number_of_modes(
                _2d_mx.factory_number_of_ip(classRegion))))

    class StdDifferentiator(Parent):

        __slots__ = ['correction', 'd1', 'd2']

        def __init__(self, key):
            """Constructor of StdDifferentiator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            n1d = self.n1d

            self.correction = factory_form_factors(self.G)
            self.d1 = np.zeros((n1d, n1d))
            self.d2 = np.zeros((n1d, n1d))

        def __call__(self, Ph, dxi_1, dxi_2):
            """Physical numerical derivative with respect to xi_i.

            The term "physical" refers to the fact that it is evaluated
            on the integration points, not on the coefficient space. In
            other words, we don't need to know in advance the
            derivatives of the basis functions.

            Args:
                Ph (numpy.array) [in]: values of a scalar function at
                    the integration points (in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).

                dxi_1 (numpy.array) [out] Derivative
                dxi_2 (numpy.array) [out] Derivative
            """

            n1d = self.n1d
            n2d = self.n2d

            # Integration points
            px = self.P1
            py = self.P2
            assert n1d == px.shape[0]
            assert n1d == py.shape[0]

            # Collocation matrices
            Dx = self.D1
            Dy = self.D2
            assert Dx.shape == (n1d, n1d)
            assert Dy.shape == (n1d, n1d)

            # Input, output checks
            assert n2d == Ph.shape[0]
            assert n2d == dxi_1.shape[0]
            assert n2d == dxi_2.shape[0]

            # This reshape creates a view: does not change the original
            # object nor allocates additional memory.
            Ph2d = np.reshape(Ph, (n1d, n1d))

            # Versao nao vetorizada
            # ----
            # ij = 0
            # for jj in range(n1d):

            #     py_jj = py[jj]

            #     for ii in range(n1d):

            #         d1 = np.dot(Dx[ii,:], Ph2d[jj,:])
            #         d2 = np.dot(Dy[jj,:], Ph2d[:,ii])

            #         dxi_1[ij], dxi_2[ij] = self.correction.eval_scalar(
            #             px[ii], py_jj, d1, d2 )

            #         ij += 1
            # ----

            # Alternativa vetorizada ao loop jj, ii comentado acima.
            np.dot(Ph2d, Dx.T, self.d1)
            np.dot(Dy, Ph2d, self.d2)
            px, py = np.meshgrid(px, py)
            self.correction.eval(px.ravel(),py.ravel(),
                                 self.d1.ravel(), self.d2.ravel(),
                                 dxi_1, dxi_2)

    return StdDifferentiator


# end of manticore.lops.modal_dg.stdops2d.factory_stddifferentiator2d


def factory_stddivergent2d(classRegion):

    # Using stdmixins2d factories to built a class that provides
    # access to number of integration points.
    Parent = _2d_mx.factory_number_of_ip(classRegion)

    class StdDivergent(Parent):

        __slots__ = ['op', 't1', 't2']

        def __init__(self, key):
            """Constructor of StdDivergent objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.op = factory_stddifferentiator2d(classRegion)(key)
            self.t1 = np.zeros(self.n2d)
            self.t2 = np.zeros(self.n2d)

        def __call__(self, Fxi_1, Fxi_2, Div):

            n2d = self.n2d

            assert n2d == Fxi_1.shape[0]
            assert n2d == Fxi_2.shape[0]
            assert n2d == Div.shape[0]

            self.op(Fxi_1, Div, self.t1)
            self.op(Fxi_2, self.t1, self.t2)
            Div += self.t2

    return StdDivergent


# end of manticore.lops.modal_dg.stdops2d.factory_stddivergent2d


def factory_stdmassmatrix(classRegion):
    """ Mass matrix for standard elements

    Notes:
        * See K&S, pages 175
    """

    # Using stdmixins2d factories to built a class that provides
    # access to  number of modes and number of integration points.
    Parent = _2d_mx.factory_number_of_modes(
        _2d_mx.factory_number_of_ip(classRegion))

    class StdMassMatrix(Parent):

        __slots__ = ['bw', 'innerp', 'nm', 'C', 'Tmp', 'Ph']

        def __init__(self, key):
            """Constructor of StdMassMatrix objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

            self.bw = factory_stdbackward2d(classRegion)(key)
            self.innerp = factory_stdinnerproduct2d(classRegion)(key)
            self.nm = ExpansionSize.get(self.G, self.o)
            self.C = np.zeros(self.nm)
            self.Tmp = np.zeros(self.nm)
            self.Ph = np.zeros(self.n2d)

        def __call__(self, M):

            for ii in range(self.nm):

                self.C[ii] = 1.0

                self.bw.at_ips(self.C, self.Ph)

                self.innerp(self.Ph, self.Tmp)

                M[ii] = self.Tmp[ii]

                self.C[ii] = 0.0

    return StdMassMatrix


# end of manticore.lops.modal_dg.stdops2d.factory_stdmassmatrix
