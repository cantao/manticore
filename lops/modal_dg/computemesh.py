#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Organization of all computational units of a mesh.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

from collections import defaultdict
import manticore.services.const as const
from manticore.lops.modal_dg.entity import ExpansionEntity
from manticore.lops.modal_dg.entity import GlobalExpansionContainer
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.dgtypes import RegionAdapter


class ExpansionContainer:
    def __init__(self):

        self.sz = RegionAdapter.size()

        self.container = [[[] for i in range(self.sz)] for j in range(2)]

    def size(self):
        sz_quad = 0
        for i in range(self.sz):
            sz_quad += len(self.container[0][i])

        sz_tria = 0
        for i in range(self.sz):
            sz_tria += len(self.container[1][i])

        return sz_quad + sz_tria

    def __len__(self):
        return self.size()

    def empty(self):
        return bool(self.size() == 0)

    def insert(self, e):
        """ Insert an EnpansionEntity instance in the container.

        Args:

            e (ExpansionEntity): : instance with ID, type, subtype and
                role already set up.

        """

        assert isinstance(e, ExpansionEntity)

        self.container[e.type][e.subtype].append(e)

        # returns reference to the actual object
        return self.container[e.type][e.subtype][-1]

    def get_container(self, region, adapter=RegionAdapter.WADG):
        return self.container[region][adapter]

    def write_to_txt(self, file_txt):

        for etype in range(2):
            for i in range(self.sz):
                for e in self.container[etype][i]:
                    c = e.get_coeff()
                    nnodes = c.shape[1]
                    file_txt.write('%s, %s' % (e.ID, nnodes))

                    for i in range(nnodes):
                        file_txt.write(', %s, %s' % (c[0, i], c[1, i]))

                    file_txt.write('\n')


# end of manticore.lops.modal_dg.computemesh.ExpansionContainer


class ExpansionGroup:
    """An ExpansionGroup is isomorphic to a geometric subdomain.

    Notes:

    * An Expansion group contains any number of diferent expansions
    (stored as ExpansionContainer objects).

    * Although it is possible to store trias and quads inside the
    same expansion, there is no gain in doing so as it will resukt
    in an unecessary over integration on trias.

    * This way, it is recommended to store trias and quads in different
    expansions.
    """
    def __init__(self, group_id, group_name, role):

        self._group = defaultdict(ExpansionContainer)
        self._id = group_id
        self._name = group_name
        self._role = role  # EntityRole

    def __iter__(self):

        self.it = iter(self._group)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self._group[key]

    def __contains__(self, k):

        return k.key in self._group

    def __repr__(self):
        msg = '<{} (ID: {}, Name: {}, Role: {}\n\t'.format(
            self.__class__, self._id, self._name, self._role)

        for k in self._group:
            msg += '[{}: {} elements] '.format(k, self._group[k].size())

        msg += ')>'

        return msg

    def write_to_txt(self, file_txt):
        for k in self._group:
            self._group[k].write_to_txt(file_txt)

    def insert(self, k, e):

        if k.key in self._group:
            e.role = self._role
            e.key = k
            return self._group[k.key].insert(e)
        else:
            return None

    @property
    def ID(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def role(self):
        return self._role

    def create_container(self, p_order, nip):  # former createGroup!

        k = ExpansionKeyFactory.make(p_order, nip)

        if k.key not in self._group:
            self._group[k.key]

        return k, self._group[k.key]

    def __call__(self, k):
        return self._group.get(k.key)


# end of manticore.lops.modal_dg.computemesh.ExpansionGroup


def GROUP_KEY(it):
    """ Retrives the ExpansionKey object related to an
    ExpansionContainer object of an ExpansionGroup.

    Notes:

        * This Function acts on dict_items([(hashblExpansionKey,
    ExpansionContainer)]) items inside loops over ExpansionGroup's
    contents.

    Returns:

        ExpansionKey object associated to an ExpansionContainer.

    Args:

        it (tuple (hashblExpansionKey, ExpansionContainer)): iterable
        returned by ExpansionGroup.__iter__().
    """
    return ExpansionKey.get_instance(it[0].order, it[0].nip)


def GROUP_INSERT(it, e):
    """ Inserts a ExpansionEntity object in an ExpansionContainer
    object of an ExpansionGroup.

    Notes:

        * This Function acts on dict_items([(hashblExpansionKey,
    ExpansionContainer)]) items inside loops over ExpansionGroup's
    contents.

    Returns:
        Reference to the actual ExpansionEntity object stored inside the group.

    Args:

        it (tuple (hashblExpansionKey, ExpansionContainer)): iterable
        returned by ExpansionGroup.__iter__().
    """

    return it[1].insert(e)


class ComputeMesh:
    class GroupContainer:
        def __init__(self):

            self._data = []
            self._by_id = defaultdict(ExpansionGroup)
            self._by_name = defaultdict(ExpansionGroup)
            self._by_role = defaultdict(list)  # list(ExpansionGroup)

        def size(self):
            return len(self._data)

        def __len__(self):
            return len(self._data)

        def empty(self):
            return bool(not len(self._data))

        def insert(self, g):
            self._data.append(g)

            if g.ID in self._by_id:
                raise AssertionError(
                    "Attempt to overwrite a previously defined group!")
            else:
                self._by_id[g.ID] = self._data[-1]

            if g.name in self._by_name:
                raise AssertionError(
                    "Attempt to overwrite a previously defined group!")
            else:
                self._by_name[g.name] = self._data[-1]

            self._by_role[g.role].append(self._data[-1])

    def __init__(self):
        self.container = GlobalExpansionContainer()
        self.subdomains = ComputeMesh.GroupContainer()
        self.subd_id_gen = 0

    def __iter__(self):

        self.it = iter(self.subdomains._data)

        return self

    def __next__(self):

        try:
            item = next(self.it)
        except StopIteration:
            raise StopIteration

        return item

    def __repr__(self):
        msg = '<{} ({} Subdomains: \n'.format(self.__class__,
                                              len(self.subdomains._data))

        for s in self.subdomains._data:
            msg += '{}\n'.format(s)

        msg += ')>'

        return msg

    def create_subdomain(self, subd_name, subd_role):

        if subd_name in self.subdomains._by_name:
            raise AssertionError("Attempt to duplicate subdomain!")
        else:
            self.subdomains.insert(
                ExpansionGroup(self.subd_id_gen, subd_name, subd_role))

        aux = self.subd_id_gen
        self.subd_id_gen += 1

        return aux

    def create_expansion(self, subd_id, p_order, nip):
        assert subd_id in self.subdomains._by_id

        return self.subdomains._by_id[subd_id].create_container(p_order, nip)

    # def insert(self, e_container, e):
    #     return self.container.insert(e_container.insert(e))

    def insert(self, subd_id, k, e):
        return self.container.insert(
            self.subdomains._by_id[subd_id].insert(k, e))

    def insert_entity(self, it_group, e):
        return self.container.insert(GROUP_INSERT(it_group, e))

    def find_subdomain_id(self, subd_name):

        if subd_name in self.subdomains._by_name:
            return self.subdomains._by_name[subd_name].ID
        else:
            return const.INVALID_RESULT

    def get_entity(self, ID):
        """Returns reference entity by its unique ID."""
        return self.container.find_by_id(ID)


# end of manticore.lops.modal_dg.computemesh.ComputeMesh


class SubDomainIdentifierIterator:
    """ Iterator for ExpansionGroup objects stored inside a
       ComputeMesh object following an internally hashed ordering
       based on the externally defined entities' IDs.

    """

    def __init__(self, mesh):
        self.wrapped = mesh

    def __iter__(self):

        self.it = iter(self.wrapped.subdomains._by_id)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.wrapped.subdomains._by_id[key]

    def __contains__(self, ID):

        return ID in self.wrapped.subdomains._by_id

    def find(self, ID):
        return self.wrapped.subdomains._by_id.get(ID)


# end of manticore.lops.modal_dg.computemesh.SubDomainIdentifierIterator


class SubDomainNameIterator:
    """ Iterator for ExpansionGroup objects stored inside a
       ComputeMesh object following an internally hashed ordering
       based on the externally defined entities' names.

    """

    def __init__(self, mesh):
        self.wrapped = mesh

    def __iter__(self):

        self.it = iter(self.wrapped.subdomains._by_name)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.wrapped.subdomains._by_name[key]

    def __contains__(self, name):

        return name in self.wrapped.subdomains._by_name

    def find(self, name):
        return self.wrapped.subdomains._by_name.get(name)


# end of manticore.lops.modal_dg.computemesh.SubDomainIdentifierIterator


class SubDomainRoleIterator:
    def __init__(self, mesh):
        self.wrapped = mesh

    def __iter__(self):

        self.it = iter(self.wrapped.subdomains._by_role)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.wrapped.subdomains._by_role[key]

    def __contains__(self, role):

        return role in self.wrapped.subdomains._by_role

    def find(self, role):
        return self.wrapped.subdomains._by_role.get(role)


# end of manticore.lops.modal_dg.computemesh.SubDomainRoleIterator
