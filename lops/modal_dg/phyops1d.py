#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Operations on faces of real 2D elements.
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore.lops.modal_dg.stdmixins1d as _1d_mx
from manticore.lops.modal_dg.stdops1d import factory_stdintegrator1d
from manticore.lops.modal_dg.stdops1d import factory_stdinnerproduct1d
from manticore.lops.modal_dg.dgtypes import RegionInfo
from manticore.lops.nodal_cg.interpol import InterpolationKey
from manticore.geom.standard_geometries import geom


def factory_physintegrator1d(classRegion):
    """Factory of classes that perform line integration of a scalar function on faces of a real element.

    """

    Parent = factory_stdintegrator1d(classRegion)

    class PhysIntegrator(Parent):

        __slots__ = []

        def __init__(self, key):
            """Constructor of PhysIntegrator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

        def __call__(self, face_id, Ph, J):
            """Executes a 2D integration of a scalar function on face of a
            real element.

            Args:

                face_id (int) [in]: local face index.

                Ph (numpy.array) [in]: values of a scalar function at
                    the integration points (in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).

                J (numpy.array) [in]: values of the jacobian (arc length) of
                    the complete standard to real coordinates mapping
                    (xi -> area coords -> x).

            Returns:
                double
            """

            return Parent.__call__(self, face_id, Ph * J)

    ## end of factory_physintegrator1d.PhysIntegrator

    return PhysIntegrator


## end of manticore.lops.modal_dg.phyops1d.factory_physintegrator1


def factory_physevaluator1d(classRegion):
    """Factory of classes that evaluate a scalar function f(x,y) over the
    integration points of a face of a real element.

    """

    Parent = _1d_mx.factory_number_of_ip(classRegion)

    class PhysEvaluator(Parent):

        __slots__ = []

        def __init__(self, key):
            """Constructor of PhysEvaluator objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.

            """

            Parent.__init__(self, key)

        def __call__(self, face_id, f, coords, geom_type, collapsed_to_global):
            """Evaluates a scalar function f(x,y) over the integration points
            of a face of a real element.

            Args:

                face_id (int) [in]: local face index.

                f (function) [in]: scalar function able to receive Numpy
                    1d arrays as arguments also returning a Numpy array.

                coords (np.array) [in]: nodal coordinates of the geometric
                   interpolation. coords.shape = (2, number of nodes)

                geom_type (StandardGeometry): the collapsed_to_global mapping
                    depends on the type of geometric interpolation.

                collapsed_to_global (CollapsedToGlobalFaceMaps): complete
                    mapping from collapsed to real coordinates: eta->xi->area->x

            Returns:

                Numpy 1d array in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights

            """
            n1 = self.n_coord[0][face_id]
            n2 = self.n_coord[1][face_id]
            n  = n1 * n2

            map_key = InterpolationKey.get_instance(geom_type, n, face_id)

            assert self.G == RegionInfo.mesh_to_dg(map_key.shape)

            px = np.zeros(n)
            py = np.zeros(n)

            # Maps integration points coordinates (eta) to local
            # (area) coordinates, evaluates shape functions at those
            # points and returns an Interpolation object.
            c2gfc_map = collapsed_to_global(map_key)  # <- Interpolation object

            # Maps integration points coordinates (eta) to local
            # (area) coordinates, evaluates shape functions at those
            # points and performs local-to-global mapping.
            c2gfc_map.local_to_global(coords, px, py)

            return f(px, py)

    ## end of factory_physevaluator1d.PhysEvaluator

    return PhysEvaluator


## end of manticore.lops.modal_dg.phyops1d.factory_physevaluator1d


def factory_physinnerproduct1d(classRegion):
    """Factory of classes that evaluates the inner product of the physical
    represented values with every basis function on faces of a real element.

    """
    Parent = factory_stdinnerproduct1d(classRegion)

    class PhysInnerProduct(Parent):

        __slots__ = []

        def __init__(self, key):
            """Constructor of PhysInnerProduct objects.

            Args:
                key (ExpansionKey): required for the initialization of
                    Parent.
            """

            Parent.__init__(self, key)

        def __call__(self, face_id, Ph, J, Ipq):
            """Executes inner product of the physical represented values
            with every basis function on a face of a real element.

            Args:

                face_id (int) [in]: local face index.

                Ph (numpy.array) [in]: values of a scalar function at
                    the integration points (in streamlined ordering; see
                    manticore.lops.modal_dg.gauss_strm.StreamlinedGaussWeights).

                J (numpy.array) [in]: values of the jacobian (arc length) of
                    the complete standard to real mapping
                    (xi -> area coords -> x).

            Returns:
                double
            """

            return Parent.__call__(self, face_id, Ph * J, Ipq)

    ## end of factory_physinnerproduct1d.PhysInnerProduct

    return PhysInnerProduct


## end of manticore.lops.modal_dg.phyops1d.factory_physinnerproduct1d
