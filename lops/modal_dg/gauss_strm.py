#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Points and weights for Gaussian quadrature based on Jacobi polynomials
#
# @addtogroup LOPS.MODAL_DG
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from manticore.lops.modal_dg.expansion import ExpansionKey
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.gauss import StreamlinedGaussKey


class StreamlinedGaussWeights:
    """Streamlined version of integration weights

    Notes:

        * Use it for non sum-factorised integration. Faster on SIMD
            machines with Eigen. It pre-multiplies all weights,
            combining all two coordinate direction on a simple Np^2
            for 2D.

    """

    def __init__(self, gauss_quad):
        """ Constructor for a JacobiGaussQuadrature object.

        Attributes:

            wi (dict(dict(np.array))): [GaussPointsType][nip]:np.array,
               storage for integration weights.
               interval.

        """

        self.gauss_quad = gauss_quad

        self.wi = {}  # [StreamlinedGaussKey]: np.array

    def __call__(self, strmgauss_key):

        assert type(strmgauss_key) is StreamlinedGaussKey

        if strmgauss_key not in self.wi:
            self.__compute(strmgauss_key)

        return self.wi[strmgauss_key]

    def __compute(self, strmgauss_key):

        n = strmgauss_key.n1d

        if n == 0:
            return None

        G = strmgauss_key.type

        # The use of ExpansionKey below is just to generate the
        # weights in each direction (no data about principal functions
        # is required here and then the p_order argument is not
        # relevant).
        ek = ExpansionKey.get_instance(p_order=0, nip=n)

        ek.set_gauss_quadrature(self.gauss_quad)

        wx = ek.w(G, 1)
        wy = ek.w(G, 2)

        self.wi[strmgauss_key] = np.ravel(np.reshape(wy, (n, 1)) * wx)
