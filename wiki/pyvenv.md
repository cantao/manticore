# Creating a Python Virtual Environment (pyvenv)

Python Virtual Environments are a convenient way to install local copies of all the Python packages you need without interfering with your system. Lets create one for Manticore development.

1. Edit the configuration file `.bashrc` if you are using Bash. Otherwise chances are the you are a Zsh user: then edit `.zshrc`. Add the following line to the file

        export WORKON_HOME=$HOME/.virtualenvs

1. Restart your terminal or your ssh session in order to the change to take place.

1. Create the environment

        pyvenv $WORKON_HOME/manticore

1. Enter the enviroment

        source $WORKON_HOME/manticore/bin/activate

1. Upgrade de default packages

        pip install --upgrade pip setuptools

1. Install the supporting packages

        pip install jupyter numpy
