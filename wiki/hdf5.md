# HDF5 stack

The HDF5 stack is composed of two components:

* The main [HDF5](https://www.hdfgroup.org/HDF5) library
* The [h5py](http://docs.h5py.org/en/latest/index.html) Python wrapper

They will be both installed in virtual environments, completely apart from the system. This level of isolation keeps things under control during development/testing.

## Installing the HDF5 library

> **Warning!** These instructions cover *only* GNU/Linux installation.

1. Be sure to have a reasonable development enviroment, including the usual tools (*gcc*, *gfortran*, *make*, etc.) Check your distribution repos.

1. Install [CMake](https://www.cmake.org) (**version 3.1.0 or better**). Again, check your distribution repos.

1. [Download](http://www.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.0-patch1/src/hdf5-1.10.0-patch1.tar.bz2) the HDF5 source code (**version 1.10.0-patch1**).

1. Unpack the HDF5 library file

        tar jxvf hdf5-1.10.0-patch1.tar.bz2

1. Enter the unpacked directory and create a `build` directory. This is a temporary workplace to build the library.

        cd hdf5-1.10.0-patch1
        mkdir build
        cd build

1. Run CMake with the following options

        cmake -DBUILD_TESTING=OFF -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/local/HDF5 ..

    Note the `CMAKE_INSTALL_PREFIX` above. It tells CMake **where** we want to install the libraries and binaries after compilation. This variable usually defaults to `/usr/local`, allowing installation for all users. Given our purpose of isolating the environment, we opted for a `$HOME` based location.

1. Run make. Change the '8' below to the number of cores/cpus of your machine.

        make -j8

1. Install the library

        make install

At this point you should have a `$HOME/local` directory populated with `bin` (several HDF5 tools), `include` (C/C++ include files) and `lib` (the static and shared libraries).

## Installing h5py

1. Inside the virtual environment, run

        HDF5_DIR=$HOME/local/HDF5 pip install h5py
