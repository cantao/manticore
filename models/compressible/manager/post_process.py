#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# ????
#
# @addtogroup MODELS.COMPRESSIBLE.MANAGER
# @author ????
#

import numpy as np
import manticore
import manticore.services.const as const
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import FieldType, FieldRole, EntityRole
from manticore.lops.modal_dg.engine import (
    EntityOperator, foreach_entity_in_subdomain)
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.models.compressible.services.predicatemixins import (
    model_description_mixin, equation_domain_signature)
from manticore.models.compressible.description.mdtypes import (
    FaceParameter, EqType, FaceType)
from manticore.lops.modal_dg.class_instances import (
    class_quad_physevaluator1d, class_tria_physevaluator1d)
from manticore.models.compressible.services.datatypes import (
    ModelTime )
from manticore.lops.modal_dg.phyops1d import factory_physintegrator1d
from manticore.models.compressible.equations.viscfluxes import (
    Tau11, Tau22, Tau12)
from manticore.models.compressible.equations.workspaces import (
    TauWsp_v, IntegralParametersWsp_v )

#Performs pos-processing in the field variables in order to evaluate
#global parameters

class drag_coefficient_global(
    model_description_mixin, equation_domain_signature):

    def __init__(self, model_desc, classRegion):

        model_description_mixin.__init__(self, model_desc)
        equation_domain_signature.__init__(self)

        self.p_inf_value = model_desc.get_post_process_data()['pressure']

        self.rho  = np.zeros(0)
        self.rhou = np.zeros(0)
        self.rhov = np.zeros(0)
        self.rhoe = np.zeros(0)

        self.mu = np.zeros(0)
        self.dudx = np.zeros(0)
        self.dudy = np.zeros(0)
        self.dvdx = np.zeros(0)
        self.dvdy = np.zeros(0)

        self.u = np.zeros(0)
        self.v = np.zeros(0)

        self.p = np.zeros(0)
        self.p_inf = np.zeros(0)
        self.dc_array = np.zeros(0)
        self.spatial_integrator_t = factory_physintegrator1d(classRegion)
        self.fluid = None

        self.n = np.zeros(0)
        self.t = np.zeros(0)

    def setup(self, eqname):

        matname = self.desc.get_equation(eqname).mat
        self.fluid = self.desc.get_material(matname)

    def resize(self, nip):

        self.rho.resize((nip,), refcheck=False)
        self.rhou.resize((nip,), refcheck=False)
        self.rhov.resize((nip,), refcheck=False)
        self.rhoe.resize((nip,), refcheck=False)

        self.mu.resize((nip,), refcheck=False)
        self.dudx.resize((nip,), refcheck=False)
        self.dudy.resize((nip,), refcheck=False)
        self.dvdx.resize((nip,), refcheck=False)
        self.dvdy.resize((nip,), refcheck=False)

        self.u.resize((nip,), refcheck=False)
        self.v.resize((nip,), refcheck=False)

        self.p.resize((nip,), refcheck=False)
        self.p_inf.resize((nip,), refcheck=False)
        self.dc_array.resize((nip,), refcheck=False)

        #normals and tangents
        self.n.resize((2,nip), refcheck=False)
        self.t.resize((2,nip), refcheck=False)

    def do_neighbour_reconstruction(self, ng, ng_face_id, fluid):

        # B.C.s are applied on ghosts. Thus their neighbours are PHYSICAL.
        assert ng.role==EntityRole.PHYSICAL

        state = FieldRole.State

        # Computation of values on boundary face
        ng.get_face_field(state, FieldVariable.RHO,  ng_face_id, self.rho)
        ng.get_face_field(state, FieldVariable.RHOU, ng_face_id, self.rhou)
        ng.get_face_field(state, FieldVariable.RHOV, ng_face_id, self.rhov)
        ng.get_face_field(state, FieldVariable.RHOE, ng_face_id, self.rhoe)

        ng.get_face_field(state, FieldVariable.MU, ng_face_id, self.mu)
        ng.get_face_field(state, FieldVariable.DUDX, ng_face_id, self.dudx)
        ng.get_face_field(state, FieldVariable.DUDY, ng_face_id, self.dudy)
        ng.get_face_field(state, FieldVariable.DVDX, ng_face_id, self.dvdx)
        ng.get_face_field(state, FieldVariable.DVDY, ng_face_id, self.dvdy)

        #Primitive variables
        np.copyto(self.u, self.rhou)
        self.u /= self.rho
        np.copyto(self.v, self.rhov)
        self.v /= self.rho

        # Pressure
        fluid.pressure(self.rho, self.rhoe, self.u, self.v, self.p)
        self.p_inf.fill(self.p_inf_value)

    def __call__(self, e):

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)

        self.resize(nip)

        # Physical element (internal) reconstruction
        self.do_neighbour_reconstruction(ng, ng_face_id, self.fluid)

        ng.normals_tangents(ng_face_id, self.n, self.t)

        Tau_v = TauWsp_v.get_instance(nip)
        IntPar_v = IntegralParametersWsp_v.get_instance(nip)

        ###Pressure drag
        #(p-p_inf)*(n.i)
        np.subtract(self.p, self.p_inf, out=IntPar_v.temp_p)
        np.multiply(IntPar_v.temp_p, self.n[0,:], out=IntPar_v.p_cont)

        ###Viscous drag
        ###tau*(t.i)
        #tau
        Tau11( self.mu, self.dudx, self.dvdy, Tau_v.t11 )
        Tau12( self.mu, self.dudy, self.dvdx, Tau_v.t12 )
        Tau22( self.mu, self.dudx, self.dvdy, Tau_v.t22 )

        #tau11*nx
        np.multiply(Tau_v.t11, self.n[0,:], out=IntPar_v.visc_temp11)
        #tau12*ny
        np.multiply(Tau_v.t12, self.n[1,:], out=IntPar_v.visc_temp12)
        #tau11*nx + tau12*ny

        np.add(IntPar_v.visc_temp11, IntPar_v.visc_temp12, out=IntPar_v.visc_cont)


        np.add(IntPar_v.p_cont, IntPar_v.visc_cont, out=self.dc_array)

        faceJ = ng.get_face_jacobian(ng_face_id)

        k = ExpansionKeyFactory.make(ng.key.order, nip-1)
        spatial_integrator = self.spatial_integrator_t(k)
        dc = spatial_integrator(ng_face_id, self.dc_array, faceJ)

        return dc


def accumulate_in_subdomain(g, op):

    amount = 0
    for c in g: # for each EntityContainer

        for context in EntityOperator.contexts: # for each context
            elist = c[1].get_container(context[0], context[1])

            for e in elist: # for each ExpansionEntity
                res = op(e)
                amount +=res
        return amount
