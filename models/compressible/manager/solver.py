#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/models/compressible/manager/solver.py -ll DEBUG -j jobname
# or
#
# $ python3 -m cProfile -o prof manticore/models/compressible/manager/solver.py
#

#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore

##
## Execution directives
##
from manticore.models.compressible.directives import ExecDirective
#manticore.models.compressible.directive = ExecDirective.process('STANDARD')
manticore.models.compressible.directive = ExecDirective.process(
                                                        'WARBURTON_EX_6_1')
##

from manticore.models.compressible.manager.manager import Manager

##
from manticore.mdf.enums import MapType
from manticore.lops.modal_dg.dgtypes import EntityRole
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.models.compressible.services.datatypes import ModelTime
from manticore.models.compressible.tests.test_helpers import (
    simple_model_01, example_warburton_book_6p1 )
from manticore.models.compressible.timeint.generic import CheckError
from math import sqrt
##

def main(jobname):
    logger = manticore.logging.getLogger('MDL_CMP_MN')

    logger.info("Compressible Flow Solver")

    # # --
    # # Model description
    # path = "hiocfd/"
    # model_desc = inviscid_smooth_bump()
    # model_desc.data.get_node(
    #     'MESH').data = path+"SmoothBump_quad_ref0_Q4_p1.mdf"
    # model_desc.data.get_node(
    #     'RESULTS').data = path+"SmoothBump_quad_ref0_Q4_p1-res.mdf"
    # model_desc.data.get_node(
    #     'INITIAL').data = path+"SmoothBump_quad_ref0_Q4_p1-init.mdf"
    # model_desc.data.get_node('INITIAL_TYPE').data = MapType.PER_ELEM
    # model_desc.get_time_settings().tstep.set_cfl(1.0)
    # # --

    # --
    # Model description
    path = "manticore/models/compressible/tests/"
    model_desc = simple_model_01()
    model_desc.data.get_node('MESH').data    = path+"square64_p1.mdf"
    model_desc.data.get_node('RESULTS').data = "square64_p1-res.mdf"
    model_desc.data.get_node('RESTART').data = path+"square64_p1-restart.mdf"
    model_desc.data.get_node('INITIAL').data = path+"square64_p1-init.mdf"
    model_desc.data.get_node('INITIAL_TYPE').data = MapType.HIERARCHICAL
    model_desc.get_time_settings().deltat = 0.05
    # --

    model = Manager(jobname)

    model.read_description(model_desc)

    model.read_mesh()

    model.config_solver()

    model.solve()

    # --
    logger.info('Checking error...')
    fluid = model_desc.get_material('Air')
    g = fluid.thermo.gamma
    time_glb = ModelTime()
    t_now = time_glb.get()
    func = example_warburton_book_6p1(x0=5.,y0=0.,beta=5.,gamma=g,t=t_now)
    subds = SubDomainRoleIterator(model.cm).find(EntityRole.PHYSICAL)
    stt = model.eqs.residual_variables().get()
    check_error = CheckError(func, stt)
    for s in subds:            
        foreach_entity_in_subdomain(s, check_error)

    logger.info('Discrete l2:')
    for v in stt:
        logger.info('Error in %s = %s' % (v, sqrt(check_error.l2_norm[v])))

    logger.info('Continuous l2:')
    for v in stt:
        logger.info('Error in %s = %s' % (v, sqrt(check_error.l2_cont[v])))
    # --


    logger.info("Solution procedure ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Manticore compressible flow solver')
    
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    
    parser.add_argument(
        '-j', '--jobname', type=str, default='mtc_solver', help='Jobname')

    args = parser.parse_args()

    log_file = str(args.jobname) + '.log'

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
                }
            },
            'handlers': {
                'console': {
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                },
                'file':{
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': log_file,
                    'mode': 'a',
                    'maxBytes': 1048576,
                    'backupCount': 3,
                },
            },
            'loggers': {
                '': {
                    'handlers': ['file', 'console'],
                    'level': args.loglevel,
                },
            }
        }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main(args.jobname)

    manticore.logging.shutdown()
