#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Writer of MDF fields.
#
# @addtogroup MODELS.COMPRESSIBLE.MANAGER
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore
from manticore.mdf.file import File
from manticore.mdf.enums import MapType, MeshChange, FieldType
from manticore.mdf.modeldata import ModelData
from manticore.mdf.partitiondata import PartitionData
from manticore.mdf.fielddata import FieldData
from manticore.geom.standard_geometries import (
    StandardGeometryGenerator, geom )
from manticore.lops.modal_dg.dgtypes import (
    RegionInfo, RegionAdapter, GaussPointsType )
from manticore.lops.modal_dg.gauss import standard_nip_rule
from manticore.lops.modal_dg.computemesh import SubDomainNameIterator
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.expantools import ExpansionSize, OrderMigration
from manticore.lops.modal_dg.entityops import (
    GetAveragedFieldValues, GetPhysicalFieldValues,
    GetModalFieldValues )
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions
from manticore.models.compressible.manager.field_tags import FieldTags
from manticore.models.compressible.description.reference import (
    DimensionConstants)


logger = manticore.logging.getLogger('MDL_CMP_MN')

class MdfFieldWriter:

    adapters = [RegionAdapter.WADG, RegionAdapter.RWADG]

    def __init__(self, model_desc, cmesh, equations):
        self.desc = model_desc
        self.cm   = cmesh
        self.rst_vars = dict()
        self.ppc_vars = dict()
        self.tags = FieldTags()
        self.dim_ctes = DimensionConstants(model_desc.get_reference())

        elset_nodes = self.desc.get_elementsets()
        for elset_node in elset_nodes:
            elset = elset_node.data
            nm    = elset.name
            self.rst_vars[nm] = equations.residual_variables_in_elset(
                elset).get()
            self.ppc_vars[nm] = equations.postprocessing_variables_in_elset(
                elset).get()

        self.all_rst_vars = equations.residual_variables().get()
        self.all_ppc_vars = equations.postprocessing_variables().get()

    def write(self, out_file, iteration=0, time=0.0, restart=False):
        try:
            logger.debug('Writing fields to a MDF file...')
            if restart:
                logger.debug('Writing restart file...')
                out_vars = self.rst_vars
                all_vars = self.all_rst_vars
            else:
                logger.debug('Writing solution file...')
                out_vars = self.ppc_vars
                all_vars = self.all_ppc_vars

            logger.debug('Opening MDF files...')
            mshfile  = self.desc.get_data('MESH')
            self.msh = File(mshfile, mode='r')
            self.msh.create()
            
            self.fds = File(out_file, mode='w')

            # Clone structure and organization from mesh file to fields file
            self.__setup_fields_file(self.fds, iteration, time, restart)

            # Creates a bidirectional map for associating the field
            # with the field identifying index in the MDF file.
            fields_index = dict()
            for idx, var in enumerate(all_vars):
                fields_index[var] = idx

            msh_data = self.msh.model_data
            part = 0 # no MPI yet
            
            logger.debug('Writing fields from Partition [%s]...' % (part) )
            subdomain_by_name = SubDomainNameIterator(self.cm)
            
            for subd in range(msh_data.n_subdomains):
                subd_name = self.msh.query_subdomain_name(part, subd)

                # Check subdomain's name coherence    
                assert subd_name in out_vars

                # Access subdomain where the field will be read from
                s = subdomain_by_name.find(subd_name)

                # Write fields if subdomain was found in mesh
                assert s

                logger.debug(
                    'Writing fields from subdomain [%s]: %s' %
                    (subd, subd_name) )

                list_vars = out_vars[subd_name]

                # Discovering what kind of geometric elements compose
                # the partition
                shapes = self.msh.query_element_shapes(part, subd)
                    
                # Read field for each shape
                for sh in shapes:
                    # map: p_order->number of elements
                    els_by_order = self.msh.query_number_elements(
                        part, subd, sh )
                    std_geo  = StandardGeometryGenerator.gen_from_string(sh)
                    std_dg   = RegionInfo.mesh_to_dg(geom(std_geo))

                    shape_factory = factory_shape_functions(std_geo)()

                    nip_jac = standard_nip_rule(GaussPointsType.GaussLegendre,
                                                shape_factory.jacobian_order() )

                    for p_order, nels in els_by_order.items():

                        # This number of integration points is related to
                        # the compounded order of the nonlinear operator
                        # we are integrating in a weak DG formulation of the
                        # Navier-Stokes equation for compressible flow.
                        nip = 2*p_order + nip_jac

                        # key for accessing the container of elements
                        k = ExpansionKeyFactory.make(p_order, nip)

                        # Container of elements
                        c  = s(k)
                        nS = ExpansionSize.get(std_dg, p_order)

                        # Writes hierarchical data
                        field = np.zeros(nels*nS)
                        for var in list_vars:
                            dim_cte = self.dim_ctes.get(var)
                            op = GetModalFieldValues(var, field, dim_cte)
                            
                            # Set up the operator according to the expansion
                            op.container_operator(s, k.key, c)

                            for adapter in MdfFieldWriter.adapters:
                                elist = c.get_container(std_dg, adapter)

                                for e in elist: # for each ExpansionEntity
                                    op(e)

                            field_id = fields_index[var]
                            self.fds.write_field(field_id, part, subd,
                                                     p_order, std_geo, field)
                        if not restart:                            

                            # Write data at integration points
                            jump = len(all_vars)
                            field = np.zeros(nels*nip*nip)

                            for var in list_vars:
                                dim_cte = self.dim_ctes.get(var)
                                op = GetPhysicalFieldValues(var, field, dim_cte)
                            
                                # Set up the operator according to the expansion
                                op.container_operator(s, k.key, c)

                                for adapter in MdfFieldWriter.adapters:
                                    elist = c.get_container(std_dg, adapter)

                                    for e in elist: # for each ExpansionEntity
                                        op(e)

                                field_id = fields_index[var]
                                self.fds.write_field(field_id+jump, part, subd,
                                                        p_order, std_geo, field)

                            # Write averaged data at each element
                            jump = 2*len(all_vars)
                            field = np.zeros(nels)

                            for var in list_vars:
                                dim_cte = self.dim_ctes.get(var)
                                op = GetAveragedFieldValues(var, field, dim_cte)
                            
                                # Set up the operator according to the expansion
                                op.container_operator(s, k.key, c)

                                for adapter in MdfFieldWriter.adapters:
                                    elist = c.get_container(std_dg, adapter)

                                    for e in elist: # for each ExpansionEntity
                                        op(e)

                                field_id = fields_index[var]
                                self.fds.write_field(field_id+jump, part, subd,
                                                        p_order, std_geo, field)
                            
        except Exception as e:
            logger.critical( str(e) )
            logger.critical('Failed to write MDF fields!')
            raise e

        finally:
            self.msh.close()
            self.fds.close()


    def migrate_to_next_order(self, out_file, iteration=0, time=0.0):
        try:
            logger.debug('Writing fields to a MDF file...')

            logger.debug('Writing restart file for higher order...')
            out_vars = self.rst_vars
            all_vars = self.all_rst_vars

            logger.debug('Opening MDF files...')
            mshfile  = self.desc.get_data('MESH')
            self.msh = File(mshfile, mode='r')
            self.msh.create()
            
            fds = File(out_file, mode='w')

            # Clone structure and organization from mesh file to fields file
            self.__setup_fields_file(fds, iteration, time, restart=True)

            # Creates a bidirectional map for associating the field
            # with the field identifying index in the MDF file.
            fields_index = dict()
            for idx, var in enumerate(all_vars):
                fields_index[var] = idx

            msh_data = self.msh.model_data
            part = 0 # no MPI yet
            
            logger.debug('Writing fields from Partition [%s]...' % (part) )
            subdomain_by_name = SubDomainNameIterator(self.cm)
            
            for subd in range(msh_data.n_subdomains):
                subd_name = self.msh.query_subdomain_name(part, subd)

                # Check subdomain's name coherence    
                assert subd_name in out_vars

                # Access subdomain where the field will be read from
                s = subdomain_by_name.find(subd_name)

                # Write fields if subdomain was found in mesh
                assert s

                logger.debug(
                    'Writing fields from subdomain [%s]: %s' %
                    (subd, subd_name) )

                list_vars = out_vars[subd_name]

                # Discovering what kind of geometric elements compose
                # the partition
                shapes = self.msh.query_element_shapes(part, subd)
                    
                # Read field for each shape
                for sh in shapes:
                    # map: p_order->number of elements
                    els_by_order = self.msh.query_number_elements(
                        part, subd, sh )
                    std_geo  = StandardGeometryGenerator.gen_from_string(sh)
                    std_dg   = RegionInfo.mesh_to_dg(geom(std_geo))

                    shape_factory = factory_shape_functions(std_geo)()

                    nip_jac = standard_nip_rule(GaussPointsType.GaussLegendre,
                                                shape_factory.jacobian_order() )

                    for p_order, nels in els_by_order.items():

                        # This number of integration points is related to
                        # the compounded order of the nonlinear operator
                        # we are integrating in a weak DG formulation of the
                        # Navier-Stokes equation for compressible flow.
                        nip = 2*p_order + nip_jac

                        # key for accessing the container of elements
                        k = ExpansionKeyFactory.make(p_order, nip)

                        # Container of elements
                        c  = s(k)

                        nS = ExpansionSize.get(std_dg, p_order)

                        new_order = p_order+1
                        new_nS = ExpansionSize.get(std_dg, new_order)
                        m = OrderMigration(std_dg, p_order, new_order)

                        # Writes hierarchical data
                        field = np.zeros(nels*nS)
                        new_field = np.zeros(nels*new_nS)
                        
                        for var in list_vars:
                            dim_cte = self.dim_ctes.get(var)
                            op = GetModalFieldValues(var, field, dim_cte)
                            
                            # Set up the operator according to the expansion
                            op.container_operator(s, k.key, c)

                            for adapter in MdfFieldWriter.adapters:
                                elist = c.get_container(std_dg, adapter)

                                for e in elist: # for each ExpansionEntity
                                    op(e)

                            field_id = fields_index[var]
                            m.migrate(field, new_field, nels)
                            fds.write_field(field_id, part, subd,
                                                new_order, std_geo, new_field)

            fds.close()
                            
        except Exception as e:
            logger.critical( str(e) )
            logger.critical('Failed to write MDF fields!')
            raise e

        finally:
            self.msh.close()
            

    def __setup_fields_file(self, fds_file, iteration, time, restart):
        
        logger.debug('Setting up fields file structure...')

        # Gets the correct information according to the context
        if restart:
            all_fields = self.all_rst_vars
            field_type = "Restart"

        else:
            all_fields = self.all_ppc_vars
            field_type = "Solution"

        # Gets generic attributes from mesh file to use in the fields file
        msh_data = self.msh.model_data
        mshfile  = self.desc.get_data('MESH')

        # Setting generic attibutes
        fds_file.set_model_attr(
            field_type +' Fields for Mesh '+ mshfile +' Generated by Manticore',
            time_step=iteration,
            time_value=time,
            interpolation=msh_data.interpolation,
            mesh_change=MeshChange.STATIC_MESH,
            writing_conn=False,
            last_geom_file=mshfile)


        # Cloning partition data
        fds_file.partition_data = self.msh.partition_data
        
        # Setting fields structure:
        # Hierarchical fields are always present
        for idx, field in enumerate(all_fields):
            fds_file.set_field_attr(
                idx,
                self.tags.get_name(field),
                description=self.tags.get_name(field),
                field_type=FieldType.SCALAR_FIELD,
                map_type=MapType.HIERARCHICAL)
                

        # Solution is also written at PER_INTP and PER_ELEM
        if not restart: 
            
            jump = len(all_fields)            
            for idx, field in enumerate(all_fields):
                fds_file.set_field_attr(
                    idx+jump,
                    self.tags.get_name(field),
                    description=self.tags.get_name(field),
                    field_type=FieldType.SCALAR_FIELD,
                    map_type=MapType.PER_INTP)

            jump = 2*len(all_fields)
            for idx, field in enumerate(all_fields):
                fds_file.set_field_attr(
                    idx+jump,
                    self.tags.get_name(field),
                    description=self.tags.get_name(field),
                    field_type=FieldType.SCALAR_FIELD,
                    map_type=MapType.PER_ELEM)        
            
        # Creates file structure
        fds_file.create()
