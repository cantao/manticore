#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Tags for MDF fields.
#
# @addtogroup MODELS.COMPRESSIBLE.MANAGER
# @author cacs (claudio.acsilva@gmail.com)
#

from manticore.services.fieldvariables import FieldVariable
from manticore.services.datatypes import bidict

class FieldTags:

    def __init__(self):

        self.tag = bidict()

        self.tag[FieldVariable.RHO   ] = "Density"
        self.tag[FieldVariable.RHOU  ] = "MomentumX"
        self.tag[FieldVariable.RHOV  ] = "MomentumY"
        self.tag[FieldVariable.RHOE  ] = "SpecificTotalEnergy"
        self.tag[FieldVariable.U     ] = "VelocityX"
        self.tag[FieldVariable.V     ] = "VelocityY"
        self.tag[FieldVariable.P     ] = "Pressure"
        self.tag[FieldVariable.T     ] = "Temperature"
        self.tag[FieldVariable.MU    ] = "MolecularViscosity"
        self.tag[FieldVariable.MUT   ] = "TurbulentViscosity"
        self.tag[FieldVariable.DUDX  ] = "VelocityXDerivativeX"
        self.tag[FieldVariable.DUDY  ] = "VelocityXDerivativeY"
        self.tag[FieldVariable.DVDX  ] = "VelocityYDerivativeX"
        self.tag[FieldVariable.DVDY  ] = "VelocityYDerivativeY"
        self.tag[FieldVariable.DPDX  ] = "PressureDerivativeX"
        self.tag[FieldVariable.DPDY  ] = "PressureDerivativeY"
        self.tag[FieldVariable.DTDX  ] = "TemperatureDerivativeX"
        self.tag[FieldVariable.DTDY  ] = "TemperatureDerivativeY"
        self.tag[FieldVariable.EPSHAT] = "ArtificialViscosity"
        
        self.tag[FieldVariable.DEIDX ] = "SpecificTotalEnergyDerivativeX"
        self.tag[FieldVariable.DEIDY ] = "SpecificTotalEnergyDerivativeY"

    def get_name(self, field):
        return self.tag.get(field)

    def get_field(self, name):
        return self.tag.inverse.get(name)[0]
