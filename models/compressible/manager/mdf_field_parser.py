#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Parser of MDF fields.
#
# @addtogroup MODELS.COMPRESSIBLE.MANAGER
# @author cacs (claudio.acsilva@gmail.com)
#

import manticore
from manticore.mdf.file import File
from manticore.mdf.enums import MapType
from manticore.geom.standard_geometries import (StandardGeometryGenerator, geom)
from manticore.lops.modal_dg.dgtypes import (
    RegionInfo, RegionAdapter, GaussPointsType )
from manticore.lops.modal_dg.gauss import standard_nip_rule
from manticore.lops.modal_dg.computemesh import SubDomainNameIterator
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.entityops import (
    InitAveragedFieldValues, InitPhysicalFieldValues, InitModalFieldValues )
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions
from manticore.models.compressible.manager.field_tags import FieldTags
from manticore.models.compressible.description.reference import (
    DimensionConstants )

logger = manticore.logging.getLogger('MDL_CMP_MN')


class MdfFieldParser:

    adapters = [RegionAdapter.WADG, RegionAdapter.RWADG]

    def __init__(self, model_desc, cmesh, equations):
        self.desc = model_desc
        self.cm = cmesh
        self.vars = dict()
        self.tags = FieldTags()
        self.dim_ctes = DimensionConstants(model_desc.get_reference())

        # "Residual variables" form the subset of state variables that
        # come out as a solution of the differential equation and
        # then an initial solution is required for them.
        elset_nodes = self.desc.get_elementsets()
        for elset_node in elset_nodes:
            elset = elset_node.data
            self.vars[elset.name] = equations.residual_variables_in_elset(
                                                                    elset).get()

    def parse(self):
        try:
            mshfile = self.desc.get_data('MESH')
            fdsfile = self.desc.get_data('INITIAL')
            initype = self.desc.get_data('INITIAL_TYPE')

            logger.debug('Opening MDF mesh file...')
            self.msh = File(mshfile, mode='r')
            self.msh.create()

            logger.debug('Opening MDF fields file...')
            self.fds = File(fdsfile, mode='r')
            self.fds.create()

            # Reading descriptive info about the data
            fds_data   = self.fds.model_data
            field_data = self.fds.field_data

            attr = self.fds.read_model_attr()
            self.iteration_start = attr.time_step
            
            logger.debug('Processing MDF fields...\n%s' % fds_data)

            # External loop: field, partition, subdomain
            for field in range(fds_data.n_fields):
                field_desc = field_data.get(field)

                # Only reads the type of data specified in model description
                if initype == field_desc.map_type:
                    logger.debug('Processing field [%s]: %s, %s' %
                                 (field, field_desc.name,
                                  field_desc.description))

                    part = 0  # no MPI yet                    

                    logger.debug('Processing partition [%s]...' % (part))

                    for subd in range(fds_data.n_subdomains):
                        self.__process_field(field, field_desc, part, subd)

        except Exception as e:
            logger.critical(str(e))
            logger.critical('Failed to read MDF fields!')
            raise e

        finally:
            self.msh.close()
            self.fds.close()

    def __process_field(self, field_idx, field_desc, part, subd):

        var = self.tags.get_field(field_desc.name)
        subd_name = self.msh.query_subdomain_name(part, subd)
        map_type = field_desc.map_type
        dim_cte = self.dim_ctes.get(var)

        # Check subdomain's name coherence
        assert subd_name in self.vars

        # Access subdomain where the field will be read to
        subdomain_by_name = SubDomainNameIterator(self.cm)
        s = subdomain_by_name.find(subd_name)

        # Read field if subdomain was found in mesh and var is
        # variable associated to that subdomain
        if s and (var in self.vars[subd_name]):
            logger.debug('Processing subdomain [%s]: %s' % (subd, subd_name))

            # Discovering what kind of geometric elements compose the partition
            shapes = self.msh.query_element_shapes(part, subd)

            # Read field for each shape
            for sh in shapes:
                # map: p_order->number of elements
                els_by_order = self.msh.query_number_elements(part, subd, sh)

                std_geo = StandardGeometryGenerator.gen_from_string(sh)
                base_geo = geom(std_geo)
                std_dg = RegionInfo.mesh_to_dg(base_geo)

                shape_factory = factory_shape_functions(std_geo)()

                nip_jac = standard_nip_rule(GaussPointsType.GaussLegendre,
                                                shape_factory.jacobian_order() )

                for p_order, nels in els_by_order.items():

                    # Get array with the field
                    v = self.fds.read_field(
                        field_idx, part, subd, p_order, std_geo)

                    # This number of integration points is related to
                    # the compounded order of the nonlinear operator
                    # we are integrating in a weak DG formulation of
                    # the Navier-Stokes equation for compressible
                    # flow.
                    nip = 2 * p_order + nip_jac

                    # key for accessing the container of elements
                    k = ExpansionKeyFactory.make(p_order, nip)

                    # Container of elements
                    c = s(k)

                    # Choose the correct operator for importing data
                    # into elements
                    if map_type == MapType.PER_ELEM:
                        op = InitAveragedFieldValues(var, v, dim_cte)
                    elif map_type == MapType.HIERARCHICAL:
                        op = InitModalFieldValues(var, v, dim_cte)
                    elif map_type == MapType.PER_INTP:
                        op = InitPhysicalFieldValues(var, v, dim_cte)
                    else:
                        RuntimeError('Incorrect data written to the MDF file!')

                    # Set up the operator according to the expansion
                    op.container_operator(s, k.key, c)

                    for adapter in MdfFieldParser.adapters:
                        elist = c.get_container(std_dg, adapter)

                        for e in elist:  # for each ExpansionEntity
                            op(e)

# -- mdf_field_parser.py -----------------------------------------------------
