#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/models/compressible/manager/find_dt.py -ll DEBUG 
# or
#
# $ python3 -m cProfile -o prof manticore/models/compressible/manager/solver.py
#

#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore

##
## Execution directives
##
from manticore.models.compressible.directives import ExecDirective
manticore.models.compressible.directive = ExecDirective.process('STANDARD')
##

from manticore.models.compressible.manager.manager import Manager
from manticore.models.compressible.manager.bump import smooth_bump_setup

##
from manticore.mdf.enums import MapType
from manticore.lops.modal_dg.dgtypes import EntityRole
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.models.compressible.services.datatypes import (
    ModelTime, CFLBroadCast )
from manticore.models.compressible.timeint.generic import (
    VariableTimeStep, CharacteristicLength )

def main(jobname):
    logger = manticore.logging.getLogger('MDL_CMP_MN')

    logger.info("Compressible Flow Solver - DT Estimation")

    # --
    # Model description
    path = "hiocfd/"
    model_desc = smooth_bump_setup()
    model_desc.data.get_node(
        'MESH').data = path+"SmoothBump_quad_ref0_Q4_p1.mdf"
    model_desc.data.get_node(
        'INITIAL').data = path+"SmoothBump_quad_ref0_Q4_p1-init.mdf"
    #model_desc.data.get_node('INITIAL_TYPE').data = MapType.PER_ELEM
    # --

    model = Manager(jobname)

    model.read_description(model_desc)

    model.read_mesh()

    model.config_solver()

    subds = SubDomainRoleIterator(model.cm).find(EntityRole.PHYSICAL)
    
    logger.info("Evaluating characteristic lengths...")
    cl_op = CharacteristicLength()
    for s in subds:            
        foreach_entity_in_subdomain(s, cl_op)

    # Broadcasting the global value of CFL: other portions of the code
    # will access that global value
    CFLBroadCast().set(1.0)

    logger.info('Computing variable time setps...')
    var_dt = VariableTimeStep(model_desc)
    for s in subds:
        var_dt.setup('inviscid', s.name)
        foreach_entity_in_subdomain(s, var_dt)

    max_dt = var_dt.max_dt
    min_dt = var_dt.min_dt

    logger.info('Max DT = %s at e[%s]' % (max_dt.value, max_dt.e_ID))
    logger.info('Min DT = %s at e[%s]' % (min_dt.value, min_dt.e_ID))

    logger.info("Solution procedure ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Manticore compressible flow solver')
    
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
                }
            },
            'handlers': {
                'console': {
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                },
                'file':{
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': 'find_dt.log',
                    'mode': 'a',
                    'maxBytes': 1048576,
                    'backupCount': 3,
                },
            },
            'loggers': {
                '': {
                    'handlers': ['file', 'console'],
                    'level': args.loglevel,
                },
            }
        }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main('find_dt')

    manticore.logging.shutdown()
