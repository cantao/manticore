#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Analysis manager
#
# @addtogroup MODELS.COMPRESSIBLE.MANAGER
# @author cacs (claudio.acsilva@gmail.com)
#
import sys
from collections import OrderedDict
from enum import IntEnum
from array import array
from math import sqrt
###
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.mesh.mdf_mesh_parser import MdfMeshParser
from manticore.mesh.umesh import (
    double_link_region_to_edge, double_link_region_to_region )
###
from manticore.lops.modal_dg.create_computemesh import create_computemesh
from manticore.lops.modal_dg.dgtypes import EntityRole
from manticore.lops.modal_dg.computemesh import (
    SubDomainNameIterator, SubDomainRoleIterator )
from manticore.lops.modal_dg.entity import (
    GlobalRoleIterator, GlobalSequentialIterator )
from manticore.lops.modal_dg.entityops import (
    InitializeFields, NormalizeMesh, ComputeEntityVolume,
    ComputeEntityMassMatrix )
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
###
from manticore.models.compressible.equations.manager import EquationManager
from manticore.models.compressible.equations.bcs import UpdateBC
from manticore.models.compressible.equations.viscositymodels import (
    cte_viscosity_model, sutherland_viscosity_model )
from manticore.models.compressible.description.model import ModelDescription
from manticore.models.compressible.description.mdtypes import (
    TISetting, EqType, TransportModel )
from manticore.models.compressible.description.materials import Fluid
from manticore.models.compressible.timeint.integrator import (
    SSP_5_4_RK )
from manticore.models.compressible.timeint.generic import (
    CharacteristicLength, norm_residual)
from manticore.models.compressible.services.datatypes import (
    CFLBroadCast, ITRBroadCast, ModelTime )
from manticore.models.compressible.manager.mdf_field_parser import (
    MdfFieldParser )
from manticore.models.compressible.manager.mdf_field_writer import (
    MdfFieldWriter )
#
from manticore.models.compressible.timeint.generic import ( WriteFields )
#

logger = manticore.logging.getLogger('MDL_CMP_MN')

class ConvCheckType(IntEnum):
    NO_CONVERGENCE = 0
    CONVERGENCE    = 1
    DIVERGENCE     = 2
    
class Manager:

    # Convergence test matrix
    conv_matrix = ((0, 0, 2),
                   (0, 1, 2),
                   (2, 2, 2))

    def __init__(self, jobname):
        self.job   = jobname           # string
        self.desc  = None              # ModelDescription
        self.eqs   = EquationManager()
        self.cm    = None              # ComputeMesh
        self.tints = OrderedDict()

        # self.msgfile = open(jobname + '-part_{}'.format(rank) + '.msg', 'w')
        self.rhsfile = None
        self.rhsline = None
       
    def parse_xml(self, setup_file): pass

    def read_description(self, model_desc):
        
        logger.info('READING MODEL DESCRIPTION...')

        self.desc = model_desc
        
    def read_mesh(self):
        logger.info('READING MESH...')        

        # Name of MDF file
        mdffile = self.desc.get_data('MESH')

        # MDF parser
        parser = MdfMeshParser(mdffile)

        # Geometric mesh
        gm = parser.parse()

        # Building neighborhood topologies
        double_link_region_to_edge(gm)
        double_link_region_to_region(gm)

        # Plain compute mesh
        self.cm = create_computemesh(gm)

        # Print mesh information
        logger.debug('Computation Mesh:\n%s' % self.cm)

        del gm

        # Compute the number of "communication integration points" of each
        # face of physical elements.
        logger.debug("Computing face communication sizes...")
        elist = GlobalSequentialIterator(self.cm.container)
        for e in elist:
            e.init_face_comm_sizes()

        # Lists for iterating over physical elements
        elist = GlobalRoleIterator(self.cm.container).find(EntityRole.PHYSICAL)
        subds = SubDomainRoleIterator(self.cm).find(EntityRole.PHYSICAL)

        # Nondimensionalization of mesh
        logger.info("Mesh normalization...")
        norm_mesh = NormalizeMesh(self.desc.get_reference().lgt)
        for s in subds:            
            foreach_entity_in_subdomain(s, norm_mesh)

        # Computing basic geometric data: jacobians, mass matrices, volumes
        logger.info("Evaluating jacobians of %s elements..." % len(elist))
        for e in elist:
            e.eval_jacobian()

        logger.info("Evaluating mass matrices...")
        mass_op = ComputeEntityMassMatrix()
        for s in subds:            
            foreach_entity_in_subdomain(s, mass_op)

        logger.info("Evaluating physical elements' volumes...")
        vol_op = ComputeEntityVolume()
        for s in subds:            
            foreach_entity_in_subdomain(s, vol_op)
        

    def config_solver(self):
        logger.info('INITIALIZING SOLVER...')

        logger.debug('Initializing equations...')
        self.eqs.set_equations(self.desc)

        logger.debug('Initializing fields storage...')

        subdomain_by_name = SubDomainNameIterator(self.cm)

        logger.debug('Physical elements...')
        elset_nodes = self.desc.get_elementsets()
        for elset_node in elset_nodes:
            elset = elset_node.data
            state_vars    = self.eqs.state_variables_in_elset(elset)
            residual_vars = self.eqs.residual_variables_in_elset(elset)
            standard_vars = self.eqs.standard_variables_in_elset(elset)

            s = subdomain_by_name.find(elset.name)
            
            if not s:
                raise RuntimeError('Subdomain not found!!!')

            stt = state_vars.get()
            rsd = residual_vars.get()
            cte = standard_vars.get()

            init_fields = InitializeFields(stt, rsd, cte, auxiliary=rsd)

            logger.debug('Initializing fields in subdomain %s...' % (s.name))
            foreach_entity_in_subdomain(s, init_fields)

        logger.debug('Ghost elements...')
        fset_nodes = self.desc.get_facesets()
        for fset_node in fset_nodes:
            fset = fset_node.data

            s = subdomain_by_name.find(fset.name)

            if not s:
                raise RuntimeError('Face set not found!!!')

            left_name = fset.left # name of the physical neighbour
            elset = self.desc.get_elementset(left_name)
            state_vars    = self.eqs.state_variables_in_elset(elset)
            residual_vars = self.eqs.residual_variables_in_elset(elset)

            stt = state_vars.get()
            rsd = residual_vars.get()

            init_fields = InitializeFields(stt, rsd)

            logger.debug('Initializing fields in subdomain %s...' % (s.name))
            foreach_entity_in_subdomain(s, init_fields)

        logger.info('READING INITIAL SOLUTION...!')
        fields_reader = MdfFieldParser(self.desc, self.cm, self.eqs)
        fields_reader.parse()
        
        self.it_cnt = fields_reader.iteration_start
        
        logger.debug('Initialization of time integrators...')

        switcher = {
            TISetting.INTEGRATOR_SSP54RK: SSP_5_4_RK
            }
        
        for eq_name, equation in self.eqs:
            ti = self.desc.get_time_integration(eq_name)
            self.tints[eq_name] = switcher.get(ti)(self.desc, equation)

        self.desc.data.get_node('RESTART').data = self.job + '-restart.tdf'
        

    def solve(self):
        logger.info('SOLUTION...')
        
        time_accuracy = self.desc.get_time_settings().type

        if time_accuracy==TISetting.TACC_STEADYSTATE:
            self.solve_steady_state()

        elif time_accuracy==TISetting.TACC_TRANSIENT:
            self.solve_transient()

        else:
            raise RuntimeError('Incorrect time integration setting!')

    def solve_steady_state(self):
        logger.info('SOLVING STEADY STATE PROBLEM...')

        rank = 0 # for mpi communication
        time_settings = self.desc.get_time_settings()
        
        if self.restart_flag:
            it_cnt = self.it_cnt
        else:
            it_cnt = time_settings.it_init # initial iteration,
                                           # usually 0, except in case
                                           # of restart
                    
        it_max = time_settings.it_max  # maximum iteration
        it_sav = time_settings.it_save # saving frequency

        
        it_glb = ITRBroadCast()
        it_glb.set(it_cnt)

        convergence = ConvCheckType.NO_CONVERGENCE # convergence monitoring

        self.eval_char_lenght()  # characteristic lengths (once for static mesh)
        self.eval_time_step()    # compute time steps in each element
        self.eval_viscosity()    # compute initial viscosities in each element

        csvfile = self.job + '-rhs.csv'

        if 0 == rank:
            self.rhsfile = open(csvfile, 'a+')
            self.rhsfile.write(self.csv_header()+'\n')
            self.rhsfile.close()

        logger.info('COMPUTING THE RESIDUAL OF THE INITIAL SOLUTION...')
        convergence = self.eval_residual()

        if convergence == ConvCheckType.DIVERGENCE:
            if 0 == rank:
                self.rhsfile = open(csvfile, 'a')
                self.rhsfile.write(self.rhsline)
                self.rhsfile.close()
            raise RuntimeError('DIVERGENCE!')

        fields_writer = MdfFieldWriter(self.desc, self.cm, self.eqs)

        logger.info('MAIN TIME-INTEGRATION LOOP...')
        while ( (it_cnt < it_max) and (not convergence) ):
            if 0 == rank:
                self.rhsfile = open(csvfile, 'a')
                self.rhsfile.write(self.rhsline)
                self.rhsfile.close()

            # Update iteration counter
            it_cnt+=1
            it_glb.set(it_cnt)
            logger.info('ITERATION %s...' % (it_cnt))
            sys.stdout.flush()
            
            # Time step
            for eq_name in self.tints:
                logger.debug('Time step in equation %s...' % (eq_name))
                self.tints[eq_name].update_state(self.cm)

            self.update_time_step()
            self.update_viscosity()

            logger.debug('Computing the residual of the current state...')
            convergence = self.eval_residual()

            if convergence==ConvCheckType.DIVERGENCE:
                if 0==rank:
                    self.rhsfile = open(csvfile, 'a')
                    self.rhsfile.write(self.rhsline)
                    self.rhsfile.close()
                raise RuntimeError('DIVERGENCE!')

            if (it_cnt % it_sav)==0:
                logger.info('WRITING RESTART FILE...')
                fields_writer.write(self.desc.get_data('RESTART'),
                                        iteration=it_cnt, time=it_cnt,
                                        restart=True)

        self.rhsfile = open(csvfile, 'a')
        self.rhsfile.write(self.rhsline)
        self.rhsfile.close()

        logger.info('WRITING RESULTS FOR POSTPROCESSING...')
        fields_writer.write(self.desc.get_data('RESULTS'),
                                iteration=it_cnt, time=it_cnt)

        name = self.desc.get_data('RESULTS').split('.')

        logger.info('MIGRATING FIELDS To THE NEXT ORDER...')
        filename = name[0] + '-next-order.mdf'
        fields_writer.migrate_to_next_order(
            filename,iteration=it_cnt, time=it_cnt)        

        # Write ascii file for post-processing
        filename = name[0] + '.txt'
        myfile=open(filename,'w')

        subds = SubDomainRoleIterator(self.cm).find(EntityRole.PHYSICAL)
    
        write_fields = WriteFields(myfile)
        for s in subds:            
            foreach_entity_in_subdomain(s, write_fields)
        
        myfile.close()
        #
        
    
    def solve_transient(self):
        logger.info('SOLVING TRANSIENT PROBLEM...')

        rank = 0 # for mpi communication

        time_settings = self.desc.get_time_settings()
        t     = time_settings.time_init # initial iteration, usually 0
        dt    = time_settings.deltat
        t_max = time_settings.time_max  # maximum iteration
        t_fsv = time_settings.time_save # saving frequency
        t_sav = t_fsv
        it_cnt = 0

        time_glb = ModelTime()
        time_glb.set(t)

        self.eval_char_lenght()  # characteristic lengths (once for static mesh)
        self.eval_time_step()    # compute time steps in each element
        self.eval_viscosity()    # compute initial viscosities in each element

        csvfile = self.job + '-rhs.csv'

        if 0 == rank:
            self.rhsfile = open(csvfile, 'w')
            self.rhsfile.write(self.csv_header()+'\n')
            self.rhsfile.close()


        logger.info('COMPUTING THE RESIDUAL OF THE INITIAL SOLUTION...')
        convergence = self.eval_residual()

        if convergence == ConvCheckType.DIVERGENCE:
            if 0 == rank:
                self.rhsfile = open(csvfile, 'a')
                self.rhsfile.write(self.rhsline)
                self.rhsfile.close()
            raise RuntimeError('DIVERGENCE!')

        fields_writer = MdfFieldWriter(self.desc, self.cm, self.eqs)
        file_name = self.desc.get_data('RESULTS').split('.')

        logger.info('MAIN TIME-INTEGRATION LOOP...')
        while ( (t < t_max) and (convergence!=ConvCheckType.DIVERGENCE) ):
            it_cnt +=1
            
            if 0 == rank:
                self.rhsfile = open(csvfile, 'a')
                self.rhsfile.write(self.rhsline)
                self.rhsfile.close()

            logger.info('INSTANT %s...' % (t))

            # Time step
            for eq_name in self.tints:
                logger.debug('Time step in equation %s...' % (eq_name))
                self.tints[eq_name].update_state(self.cm)

            # Update time
            t += dt
            time_glb.set(t)

            self.update_viscosity()

            logger.debug('Computing the residual of the current state...')
            convergence = self.eval_residual()

            if convergence==ConvCheckType.DIVERGENCE:
                if 0==rank:
                    self.rhsfile = open(csvfile, 'a')
                    self.rhsfile.write(self.rhsline)
                    self.rhsfile.close()
                raise RuntimeError('DIVERGENCE!')

            if (t >= t_sav):
                t_sav+=t_fsv
                logger.info('WRITING RESULTS FOR POSTPROCESSING...')
                out_file = file_name[0]+'_{}'.format(it_cnt)+'.'+file_name[1]
                fields_writer.write(out_file, iteration=it_cnt, time=t)

        out_file = file_name[0]+'_last_time'+'.'+file_name[1]
        fields_writer.write(out_file, iteration=it_cnt, time=t)

        self.rhsfile = open(csvfile, 'a')
        self.rhsfile.write(self.rhsline)
        self.rhsfile.close()


    def eval_char_lenght(self):
        
        cl = CharacteristicLength()
        
        subds = SubDomainRoleIterator(self.cm).find(EntityRole.PHYSICAL)

        for s in subds:
            foreach_entity_in_subdomain(s, cl)

    def eval_time_step(self):
        time_settings = self.desc.get_time_settings()
        tstep = time_settings.time_stepping
        
        if tstep==TISetting.TSTEP_CONSTANT:
            logger.debug('Initializing constant time steps...')
            for eq_name, equation in self.eqs:
                equation.compute_fixed_time_step(self.desc, self.cm)

        elif tstep==TISetting.TSTEP_VARIABLE:
            cfl_type = time_settings.tstep.cfl_type
            it_glb = ITRBroadCast()
            cfl_glb = CFLBroadCast()
            cfl_glb.set(time_settings.tstep.cfl(it_glb.get()))

            logger.debug('Initializing space variable time steps...')
            for eq_name, equation in self.eqs:
                equation.compute_variable_time_step(self.desc, self.cm)

        else:
            AssertionError('Invalid time-integration option.')

    def update_time_step(self):
        time_settings = self.desc.get_time_settings()
        tstep = time_settings.time_stepping
        
        if tstep==TISetting.TSTEP_VARIABLE:
            cfl_type = time_settings.tstep.cfl_type
            it_glb = ITRBroadCast()
            cfl_glb = CFLBroadCast()
            cfl_glb.set(time_settings.tstep.cfl(it_glb.get()))

            logger.debug('Updating space variable time steps...')
            for eq_name, equation in self.eqs:                
                equation.compute_variable_time_step(self.desc, self.cm)

    def eval_viscosity(self):
        allowed_types = [EqType.NAVIERSTOKES, EqType.LES, EqType.RANS,
                            EqType.RANS_SA, EqType.RANS_SST]
            
        subdomain_by_name = SubDomainNameIterator(self.cm)

        viscmodel = None
        
        for eq_name, equation in self.eqs:
            if equation.type in allowed_types:
                logger.debug('Initializing viscosity values...')
                eq_desc  = self.desc.get_equation(eq_name)
                material = self.desc.get_material(eq_desc.material)
                esets = equation.element_sets
                
                if material.transport.type==TransportModel.CONSTANT:
                    viscmodel = cte_viscosity_model(self.desc)
                    
                elif material.transport.type==TransportModel.SUTHERLAND:
                    viscmodel = sutherland_viscosity_model(self.desc)

                else:
                    raise RuntimeError('Incorrect viscosity model setting!')
                    
                for eset_name in esets:
                    s = subdomain_by_name.find(eset_name)
                    viscmodel.setup(eq_name, s.name)
                    foreach_entity_in_subdomain(s, viscmodel)

    def update_viscosity(self):
        allowed_types = [EqType.NAVIERSTOKES, EqType.LES,
                            EqType.RANS_SA, EqType.RANS_SST]
            
        subdomain_by_name = SubDomainNameIterator(self.cm)

        for eq_name, equation in self.eqs:
            eq_desc  = self.desc.get_equation(eq_name)
            material = self.desc.get_material(eq_desc.material)
            
            if ((equation.type in allowed_types) and
                (material.transport.type==TransportModel.SUTHERLAND)) :
                
                logger.debug('Updating viscosity values...')
                
                esets = equation.element_sets
                viscmodel = sutherland_viscosity_model(self.desc)
                    
                for eset_name in esets:
                    s = subdomain_by_name.find(eset_name)
                    viscmodel.setup(eq_name, s.name)
                    foreach_entity_in_subdomain(s, viscmodel)

    def eval_residual(self):

        subdomain_by_name = SubDomainNameIterator(self.cm)
        aux_conv = ConvCheckType.CONVERGENCE
        self.rhsline = ''

        for eq_name, equation in self.eqs:
            equation.compute_residual(self.desc, self.cm)

            list_vars = equation.residual_variables

            norm = norm_residual(list_vars)

            esets = equation.element_sets
            for eset_name in esets:
                s = subdomain_by_name.find(eset_name)
                foreach_entity_in_subdomain(s, norm)

            rank = 0

            eq_desc = self.desc.get_equation(eq_name)
            
            list_min = array('d', [])
            list_max = array('d', [])
            list_li  = array('d', [])
            list_l2  = array('d', [])

            for v in list_vars:
                list_min.append(eq_desc.get_limits(v).min)
                list_max.append(eq_desc.get_limits(v).max)
                list_li.append(norm.get_inf_norm(v))
                list_l2.append(sqrt(norm.get_sq2_norm(v)))

            check = self.check_conv(list_min, list_max, list_li, list_l2)

            aux_conv = Manager.conv_matrix[aux_conv][check]

            # Writing residual norms to a CSV-formatted string
            self.rhsline += self.csv_line(list_li, list_l2)

        self.rhsline +='\n'

        return aux_conv

    def check_conv(self, list_min, list_max, list_li, list_l2):
        
        criteria = None
        norm     = None        
        time_settings = self.desc.get_time_settings()
        time_accuracy = time_settings.type
        
        if time_accuracy==TISetting.TACC_STEADYSTATE:
            criteria = time_settings.stop_criteria
        elif time_accuracy==TISetting.TACC_TRANSIENT:
            criteria = TISetting.STOPCRITERIA_MAX # for monitoring divergence
        else:
            raise RuntimeError('Incorrect time integration setting!')

        if criteria==TISetting.STOPCRITERIA_MAX:
            norm = list_li
        elif criteria==TISetting.STOPCRITERIA_RMS:
            norm = list_l2
        else:
            raise RuntimeError('Incorrect time integration setting!')

        if not self.check_limit(list_max, norm):
            return ConvCheckType.DIVERGENCE
        elif self.check_limit(list_min, norm):
            return ConvCheckType.CONVERGENCE
        else:
            return ConvCheckType.NO_CONVERGENCE
        
    def check_limit(self, limit, norm):
        for (l,n) in zip(limit, norm):
            if n > l:
                return False
        return True
        
    def csv_header(self):

        header = []
        for eq_name, equation in self.eqs:
            list_vars = equation.residual_variables

            for v in list_vars:
                name = eq_name + '_' + v.name
                header.append(name + '_MAX')
                header.append(name + '_RMS')

        return ','.join(header)
            

    def csv_line(self, list_li, list_l2):
        
        line = []        
        for (vi, v2) in zip(list_li, list_l2):
            line.append('{0:11.10E},{1:11.10E}'.format(vi,v2))

        return ','.join(line)
            

