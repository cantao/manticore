#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Special specific data types.
#
# @addtogroup MODELS.COMPRESSIBLE.SERVICES
# @author cacs (claudio.acsilva@gmail.com)
#

from collections import namedtuple
from manticore.services.datatypes import Singleton

element_value_pair = namedtuple('element_value_pair', ['value', 'e_ID'])

class CFLBroadCast(metaclass=Singleton):
    """Class for the management of CFL value.

    Note:
        * It is declared as a singleton because its is a global value and 
        accessing and/or possibly updating its value are performed at far 
        apart portions of the code.

    """
    
    def __init__(self):
        self._value = 0.0

    def get(self):
        return self._value

    def set(self, value):
        self._value = value

class ITRBroadCast(metaclass=Singleton):
    """Class for the management of iteration value.

    Note:
        * It is declared as a singleton because its is a global value and 
        accessing and/or possibly updating its value are performed at far 
        apart portions of the code.

    """
    
    def __init__(self):
        self._value = 0

    def get(self):
        return self._value

    def set(self, value):
        self._value = int(value)

class ModelTime(metaclass=Singleton):
    """Class for the management of current time value of the simulation model.

    Note:
        * It is declared as a singleton because its is a global value and 
        accessing and/or possibly updating its value are performed at far 
        apart portions of the code.

    """
    def __init__(self):
        self._value = 0.0

    def get(self):
        return self._value

    def set(self, value):
        self._value = value


