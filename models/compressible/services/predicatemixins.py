#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Precicate mixins: just convience access interfaces.
#
# @addtogroup MODELS.COMPRESSIBLE.SERVICES
# @author cacs (claudio.acsilva@gmail.com)
#

from manticore.lops.modal_dg.expantools import ExpansionSize

class model_description_mixin:

    def __init__(self, model_desc):
        self._desc = model_desc

    @property
    def desc(self):
        return self._desc

class equation_domain_signature:

    def __init__(self):
        self._eq = None
        self._dm = None

    def setup(self, eq, domain):
        self._eq = eq
        self._dm = domain

    @property
    def equation(self):
        return self._eq

    @property
    def domain(self):
        return self._dm

class expansion_numbers:

    def __init__(self):
        self._nq = -1
        self._ns = -1

    def eval(self, std_region, expansion_key):
        self._nq = expansion_key.n2d
        self._ns = ExpansionSize.get(std_region, expansion_key.order)

    @property
    def nQ(self):
        return self._nq

    @property
    def nS(self):
        return self._ns
        
