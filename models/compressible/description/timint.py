#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Model description
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#

import manticore.services.const as const
from manticore.services.mconsts import CTEs
from manticore.models.compressible.description.mdtypes import TISetting

class TimeStepping:
    """Time stepping description: spatially constant or variable time
       steps, constant CFL or variable CFL.

    """

    def __init__(self,
                 tstype=TISetting.TISETTING_NONE,
                 cfltype=TISetting.TISETTING_NONE):

        # Type of time stepping:
        #   TISetting.TSTEP_CONSTANT  Spatially constant time steps
        #   TISetting.TSTEP_VARIABLE  Spatially variable time steps
        self._tstep = tstype

        # Type of CFL stepping
        self._cflstep = cfltype

        # delta t or CFL. Depends on the self.__tstep value:
        #   TISetting.TSTEP_CONSTANT  deltat
        #   TISetting.TSTEP_VARIABLE  constant CFL
        self._data = 0.0

        # Minimun value of CFL when it is linearly variable.
        self.cfl_min = 0.0

        # Maximun value of CFL when it is variable.
        self.cfl_max = 0.0

        # Linear cfl-stepping must be done from cfl_it_min.
        self.it_min = 0

        # Linear cfl-stepping must be done until cfl_it_max.
        self.it_max = -1

    def set_type(self, tstype):
        self._tstep = tstype

    @property
    def type(self):
        return self._tstep

    def set_cfl_type(self, cfltype):
        self._cflstep = cfltype

    @property
    def cfl_type(self):
        return self._cflstep

    def set_deltat(self, dt):
        self._data = dt

    @property
    def deltat(self):
        return self._data

    def set_cfl(self, c):
        self._data = c

    def cfl(self, it=None):
        if self._cflstep == TISetting.CFL_CONSTANT:
            return self._data
        elif self._cflstep == TISetting.CFL_LINEAR:
            if it:
                if it <= self.it_min:
                    return self.cfl_min
                elif it >= self.it_max:
                    return self.cfl_max
                else:
                    return (((self.cfl_max - self.cfl_min) /
                             (self.it_max - self.it_min)) *
                            (it - self.it_min) + self.cfl_min)
            else:
                raise RuntimeError('Please, check function call parameters!')
        else:
            raise RuntimeError('Please, check your the setup of your problem!')


class SteadyState:
    """Controls for time integration of steady state problems: iteration,
       convergence and time stepping settings.

    """

    def __init__(self):

        #
        # Iteration controls:

        # Iteration of the initial solution
        self.it_init = 0

        # Maximum number of iterations allowed for time integration.
        self.it_max = -1

        # Save frequency. In steady-state problems, this is the
        # frequency for saving intermadiate non-converged response
        # fields that could be used for restarting the time
        # integration.
        self.it_save = -1

        # Residual norm mode: $L_\infty$ or $L_2$.
        self.stop_criteria = TISetting.TISETTING_NONE

        #
        # Time stepping:
        self.tstep = TimeStepping()

    @property
    def type(self):
        return TISetting.TACC_STEADYSTATE

    @property
    def time_stepping(self):
        return self.tstep.type

    @property
    def cfl_stepping(self):
        return self.tstep.cfl_type

    @property
    def deltat(self):
        return self.tstep.deltat

    def cfl(self, it=None):
        return self.tstep.cfl(it)


class Transient:
    """Controls for time integration of transient problems.

    """

    def __init__(self):

        # Instant of the initial solution
        self.time_init = 0.0

        # Maximum time allowed for time integration.
        self.time_max = 0.0

        # Time step.
        self.deltat = 0.0

        # Save interval.
        self.time_save = 0.0

    @property
    def type(self):
        return TISetting.TACC_TRANSIENT

    @property
    def time_stepping(self):
        return TISetting.TSTEP_CONSTANT

    def nondimensionalization(self, rho, vel, length, mu, temp):
        if vel > const.INF and length > const.INF:
            aux = vel / length  # vel/length=1/t

            self.time_init *= aux
            self.time_max  *= aux
            self.deltat    *= aux
            self.time_save *= aux
        else:
            raise RuntimeError('Data is lower than the required precision!')


def init_time_acc(tacc):
    """Allocation of time accuracy object: steady state or trasient."""
    switcher = {
        TISetting.TACC_STEADYSTATE: SteadyState(),
        TISetting.TACC_TRANSIENT: Transient(),
    }
    return switcher.get(tacc, None)
