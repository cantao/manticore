#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Material models
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#

from manticore.models.compressible.description.thermo import SimpleSpecies
from manticore.models.compressible.description.thermo import ThermoConstantCp
from manticore.models.compressible.description.thermo import TransportInviscid
from manticore.models.compressible.description.thermo import TransportConstant
from manticore.models.compressible.description.thermo import TransportSutherland
from manticore.models.compressible.description.thermo import IdealGasEquation


class Fluid:
    """Fluid material."""

    def __init__(self, specie, thermo, transport, equation):
        """Constructor for a FieldData object: .
        """

        # Reading specie's data
        self.specie = SimpleSpecies(specie)

        # Reading thermodynamic model
        if not 'TYPE' in thermo:
            raise RuntimeError('Please, check your arguments!')
        elif thermo['TYPE'] == 'CONSTANTCP':
            self.thermo = ThermoConstantCp(thermo)
        elif thermo['TYPE'] == 'CONSTANTCV':
            raise RuntimeError('Not supported thermodynamic model!')
        else:
            raise RuntimeError('Please, check your arguments!')

        # Readinf transport model
        if not 'TYPE' in transport:
            raise RuntimeError('Please, check your arguments!')
        elif transport['TYPE'] == 'INVISCID':
            self.transport = TransportInviscid()
        elif transport['TYPE'] == 'CONSTANT':
            self.transport = TransportConstant(transport)
        elif transport['TYPE'] == 'SUTHERLAND':
            self.transport = TransportSutherland(transport)
        else:
            raise RuntimeError('Please, check your arguments!')

        # Reading equation of state
        if not 'TYPE' in equation:
            raise RuntimeError('Please, check your arguments!')
        elif equation['TYPE'] == 'IDEALGAS':
            self.equation = IdealGasEquation(equation)
        else:
            raise RuntimeError('Please, check your arguments!')

    def init_material_model(self):
        self.equation.set_model(self.specie, self.thermo)

    def nondimensionalization(self, rho, vel, length, mu, temp):
        self.thermo.nondimensionalization(rho, vel, length, mu, temp)
        self.transport.nondimensionalization(rho, vel, length, mu, temp)
        self.equation.nondimensionalization(rho, vel, length, mu, temp)

    @property
    def type(self):
        return self.thermo.type, self.transport.type, self.equation.type

    def stagnation(self):
        # self.equation.stagnation.set_model(self.thermo)
        return self.equation.stagnation

    def pressure_from_scalar(self, rho, rhoe, u, v):
        """Compute pressure from flow state at a point (scalar arguments).

        Args:
            rho  (double)[in]: density
            rhoe (double)[in]: total energy
            u    (double)[in]: velocity x-component
            v    (double)[in]: velocity v-component

        Returns:
            pressure
        """
        return self.equation.pressure_from_flow_scalar(
            self.thermo, rho, rhoe, u, v)

    def pressure(self, rho, rhoe, u, v, p):
        """Compute pressure from flow state at a set of points (vectorial args).

        Args:
            rho  (np.array)[in] : density
            rhoe (np.array)[in] : total energy
            u    (np.array)[in] : velocity x-component
            v    (np.array)[in] : velocity y-component
            p    (np.array)[out]: static pressure

        """
        self.equation.pressure_from_flow(self.thermo, rho, rhoe, u, v, p)

    def pressure_from_conserved(self, rho, rhou, rhov, rhoe, p):
        """Compute pressure from flow state at a set of points (vectorial args).

        Args:
            rho  (np.array)[in] : density
            rhou (np.array)[in] : momentum x-component
            rhov (np.array)[in] : momentum y-component
            rhoe (np.array)[in] : total energy
            p    (np.array)[out]: pressure

        """
        self.equation.pressure_from_conserved(
            self.thermo, rho, rhou, rhov, rhoe, p)

    def energy(self, rho, u, v, p, rhoe):
        """Compute energy from flow state at a set of points (vectorial args).

        Args:
            rho  (np.array)[in] : density
            u    (np.array)[in] : velocity x-component
            v    (np.array)[in] : velocity y-component
            p    (np.array)[in] : static pressure
            rhoe (np.array)[out]: total energy

        """
        self.equation.energy_from_flow(self.thermo, rho, u, v, p, rhoe)
    

    def temperature_from_conserved(self, rho, rhou, rhov, rhoe, T):
        """Compute temperature from flow state at a set of points (vectorial 
        args).

        Args:
            rho  (np.array)[in] : density
            rhou (np.array)[in] : momentum x-component
            rhov (np.array)[in] : momentum y-component
            rhoe (np.array)[in] : total energy
            T    (np.array)[out]: temperature

        """
        self.equation.temperature_from_conserved(
                                          self.thermo, rho, rhou, rhov, rhoe, T)

    def sonic_speed(self, rho, p, a):
        """Compute sonic speed at a set of points (vectorial args).

        Args:
            rho  (np.array)[in] : density
            p    (np.array)[in] : pressure
            a    (np.array)[out]: sonic speed

        """
        self.equation.speed_of_sound(self.thermo, rho, p, a)
