#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Equation descriptions
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#

from collections import namedtuple
from manticore.services.fieldvariables import FieldVariable
from manticore.models.compressible.description.mdtypes import EqType
from manticore.models.compressible.description.mdtypes import EqParameter
from manticore.models.compressible.description.mdtypes import EqSetting
from manticore.models.compressible.description.mdtypes import FormulationType

min_max = namedtuple('min_max', ['min', 'max'])

class EquationDescription:
    """
        Descriptive data required for the execution of a numeric solution
        equation: type of equation, material name (string), setup parameters
        related to numerical fluxes, state variables and their respective lower
        and upper limits.
    """

    def __init__(self, eqtype=EqType.EQTYPE_NONE, mat=None, info=None):
        """Declares (empty) attributes required for a equation
           description. Subclasses must make the correct
           attributions.

        Atrributes:

            eq (EqType): type of this equation.

            mat (string): name of the material associated to this equation.

            name (string): name of this equation.

            limits (dict): limits[FieldVariable] = (min,max). Lower and upper
                validity limits imposed on state variables.

            data (dict): data[EqParameter] = int, float, string, bool, etc. 
                setup parameters.

        """
        
        self.eq     = eqtype
        self.mat    = mat
        self.name   = info
        self.limits = {}  
        self.data   = {}

    @property
    def type(self):
        """Returns the type of this equation (Eqtype enumerate)."""
        return self.eq

    @property
    def material(self):
        return self.mat

    @material.setter
    def material(self, mat_name):
        self.mat = mat_name
        
    def get_limits(self, key):
        """Returns tuple (min,max) associated to a FieldVariable enumerate."""
        if key in self.limits:
            return self.limits[key]
        else:
            raise KeyError('Please, check your arguments!')

    def set_limits(self, key, inf, sup):
        """Associates a tuple (min,max) to a FieldVariable enumerate."""
        if key in self.limits:
            self.limits[key] = min_max(inf, sup)
        else:
            raise KeyError('Please, check your arguments!')

    def set_limit_key(self, key):
        """Associates a state variable to this equation."""
        if key in list(FieldVariable):
            self.limits[key] = None
        else:
            raise KeyError('Please, check your arguments!')

    def get_data(self, key):
        """Return a solution setting associated to a EqParameter enumerate."""
        if key in self.data:
            return self.data[key]
        else:
            raise KeyError('Please, check your arguments!')

    def set_data(self, key, value):
        """Defines a solution setting associated to a EqParameter enumerate."""
        if key in self.data:
            self.data[key] = value
        else:
            raise KeyError('Please, check your arguments!')


class Euler(EquationDescription):
    """Parameters for the compressible Euler equation."""

    def __init__(self):
        """Defines solution setting slots and state variables associated to
           this equation."""
        EquationDescription.__init__(self, EqType.EULER)

        self.data = dict.fromkeys([
            EqParameter.CONVECTIVE_FLUX, EqParameter.PRINCIPAL_DGFORM,
            EqParameter.DISSIPATION, EqParameter.DISSIPATIVE_FLUX,
            EqParameter.BR2_ETA, EqParameter.DAUXILIAR_DGFORM,
            EqParameter.ROECLIPPING, EqParameter.ROECLIPCTE,
            EqParameter.ROECLIPCTEFRAC, EqParameter.ROECLIPCTESTATUS,
            EqParameter.ROECLIPCTELIM, EqParameter.ROECLIPSPECTRAL,
            EqParameter.D_INDICATOR, EqParameter.D_PHI0A, EqParameter.D_PHI0B,
            EqParameter.D_DELTAPHI, EqParameter.D_THETAS,
            EqParameter.D_THETACTE, EqParameter.D_SCALING,
            EqParameter.D_ALGEBRAIC_TYPE
        ])

        var = [
            FieldVariable.RHO, FieldVariable.RHOU, FieldVariable.RHOV,
            FieldVariable.RHOE
        ]

        for v in var:
            self.set_limit_key(v)


class NSLaminar(Euler):
    """Parameters for the compressible laminar Navier-Stokes equation"""

    def __init__(self):
        """Defines solution setting slots and state variables associated to
           this equation."""
        Euler.__init__(self)

        self.eq = EqType.NAVIERSTOKES
        self.data[EqParameter.VAUXILIAR_DGFORM] = None
        self.data[EqParameter.VISCOUS_FLUX]     = None
        self.data[EqParameter.VISCOSITY_MODEL]  = None


def init_equation_data(eqtype):
    """Allocation of EquationDescription objects according to a given
    'EqType'.
    """
    switcher = {
        EqType.EQTYPE_NONE: EquationDescription(),
        EqType.EULER: Euler(),
        EqType.NAVIERSTOKES: NSLaminar(),
    }
    return switcher.get(eqtype, EquationDescription())
