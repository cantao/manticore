#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Element sets
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#

import math
import manticore.services.const as const
from manticore.models.compressible.description.mdtypes import FaceType
from manticore.models.compressible.description.mdtypes import FaceParameter


class FaceSetData:
    """Abstract class for data (real valued parameters) associated to a
       specific subset of the discretized boundary of a simulation
       subdomain.

       For 3D models, a subset of a discretized boundary is a set of
       connected discrete simple geometry faces which is generaly
       called 'face set'. For 2D models, the discretized boundary is,
       in fact, a set of connected **edges** but we will use the same
       name to represent the information about boundaries.

       Parameters associated to face sets are tipically boundary
       condition data or conectivity between mesh partitions.
    """

    def __init__(self):
        """Constructor of a FaceSetData object.

        Attributes:
            face (FaceType enumerate): type of the set.

            ddata (dict {FaceParameter: float,...}): Dimensional parameters.

            ndata (dict {FaceParameter: float,...}): Nondimensional parameters.

            dctes (dict {FaceParameter: float,...}): Dimensional constants.
        """

        self.face = FaceType.FACETYPE_NONE
        self.ddata = {}  # dimensional data
        self.ndata = {}  # nondimensional data
        self.dctes = {}  # dimensional constants

    @property
    def type(self):
        return self.face

    def set_dim_data(self, param, value):
        if param in self.ddata:
            self.ddata[param] = value
        else:
            raise RuntimeError('Please, check your arguments!')

    def get_dim_data(self, param):
        if param in self.ddata:
            return self.ddata[param]
        else:
            raise RuntimeError('Please, check your arguments!')

    def get_data(self, param):
        if param in self.ndata:
            return self.ndata[param]
        else:
            raise RuntimeError('Please, check your arguments!')

    def nondimensionalization(self, rho, vel, length, mu, temp):
        for key in self.dctes:
            self.ndata[key] = self.ddata[key] / self.dctes[key]

    def set_keys(self):
        pass

    def set_storage(self, keys):
        self.ddata = dict.fromkeys(keys)
        self.ndata = dict.fromkeys(keys)
        self.dctes = dict.fromkeys(keys)


class FaceSetNoData(FaceSetData):
    """Face sets that do not require any parameter."""

    def __init__(self, ftype):
        FaceSetData.__init__(self)
        self.face = ftype

    def nondimensionalization(self, rho, vel, length, mu, temp): pass


class BCWall(FaceSetData):
    """Wall boundary condition."""

    def __init__(self, ftype):
        FaceSetData.__init__(self)

        if ftype == FaceType.SMOOTHWALL or ftype == FaceType.ADIABATICWALL:
            self.face = ftype
            self.set_keys()
        else:
            raise AssertionError('Please, check your arguments!')

    def set_keys(self):
        keys = [FaceParameter.VELX, FaceParameter.VELY]
        FaceSetData.set_storage(self, keys)

    def nondimensionalization(self, rho, vel, length, mu, temp):
        self.dctes[FaceParameter.VELX] = vel
        self.dctes[FaceParameter.VELY] = vel

        FaceSetData.nondimensionalization(self, rho, vel, length, mu, temp)


class BCFarField(FaceSetData):
    """Farfield boundary condition."""

    def __init__(self):
        FaceSetData.__init__(self)

        self.face = FaceType.FARFIELD
        self.set_keys()

    def set_keys(self):
        keys = [
            FaceParameter.VELX, FaceParameter.VELY, FaceParameter.STATIC_PRESS,
            FaceParameter.STATIC_TEMP
        ]
        FaceSetData.set_storage(self, keys)

    def nondimensionalization(self, rho, vel, length, mu, temp):
        self.dctes[FaceParameter.VELX] = vel
        self.dctes[FaceParameter.VELY] = vel
        self.dctes[FaceParameter.STATIC_PRESS] = rho * vel * vel
        self.dctes[FaceParameter.STATIC_TEMP] = temp

        FaceSetData.nondimensionalization(self, rho, vel, length, mu, temp)


class BCPInlet(FaceSetData):
    """Pressure inlet boundary condition."""

    def __init__(self):
        FaceSetData.__init__(self)

        self.face = FaceType.PRESSUREINLET
        self.set_keys()

    def set_keys(self):
        keys = [
            FaceParameter.DIRX, FaceParameter.DIRY, FaceParameter.TOTAL_PRESS,
            FaceParameter.TOTAL_TEMP, FaceParameter.STATIC_PRESS
        ]
        FaceSetData.set_storage(self, keys)

    def nondimensionalization(self, rho, vel, length, mu, temp):
        norm = math.sqrt(self.ddata[FaceParameter.DIRX]**2 +
                         self.ddata[FaceParameter.DIRY]**2) + const.INF

        self.dctes[FaceParameter.DIRX] = norm
        self.dctes[FaceParameter.DIRY] = norm
        self.dctes[FaceParameter.TOTAL_PRESS] = rho * vel * vel
        self.dctes[FaceParameter.TOTAL_TEMP] = temp
        self.dctes[FaceParameter.STATIC_PRESS] = rho * vel * vel

        FaceSetData.nondimensionalization(self, rho, vel, length, mu, temp)


class BCBackP(FaceSetData):
    """Back pressure."""

    def __init__(self):
        FaceSetData.__init__(self)

        self.face = FaceType.BACKPRESSURE
        self.set_keys()

    def set_keys(self):
        keys = [FaceParameter.STATIC_PRESS]
        FaceSetData.set_storage(self, keys)

    def nondimensionalization(self, rho, vel, length, mu, temp):
        self.dctes[FaceParameter.STATIC_PRESS] = rho * vel * vel

        FaceSetData.nondimensionalization(self, rho, vel, length, mu, temp)


class BCVInlet(FaceSetData):
    """Velocity inlet boundary condition."""

    def __init__(self):
        FaceSetData.__init__(self)

        self.face = FaceType.VELOCITYINLET
        self.set_keys()

    def set_keys(self):
        keys = [
            FaceParameter.DIRX, FaceParameter.DIRY, FaceParameter.VEL,
            FaceParameter.STATIC_TEMP, FaceParameter.STATIC_PRESS
        ]
        FaceSetData.set_storage(self, keys)

    def nondimensionalization(self, rho, vel, length, mu, temp):
        norm = math.sqrt(self.ddata[FaceParameter.DIRX]**2 +
                         self.ddata[FaceParameter.DIRY]**2) + const.INF

        self.dctes[FaceParameter.DIRX] = norm
        self.dctes[FaceParameter.DIRY] = norm
        self.dctes[FaceParameter.VEL] = vel
        self.dctes[FaceParameter.STATIC_TEMP] = temp
        self.dctes[FaceParameter.STATIC_PRESS] = rho * vel * vel

        FaceSetData.nondimensionalization(self, rho, vel, length, mu, temp)


class BCConsDirichlet(FaceSetData):
    """Dirichlet boundary specified in conservative quantities."""

    def __init__(self):
        FaceSetData.__init__(self)

        self.face = FaceType.DIRICHLET_CONS
        self.set_keys()

    def set_keys(self):
        keys = [
            FaceParameter.DENSITY, FaceParameter.MOMENTUMX,
            FaceParameter.MOMENTUMY, FaceParameter.TOTALENERGY
        ]
        FaceSetData.set_storage(self, keys)

    def nondimensionalization(self, rho, vel, length, mu, temp):
        self.dctes[FaceParameter.DENSITY] = rho
        self.dctes[FaceParameter.MOMENTUMX] = rho * vel
        self.dctes[FaceParameter.MOMENTUMY] = rho * vel
        self.dctes[FaceParameter.TOTALENERGY] = rho * vel * vel

        FaceSetData.nondimensionalization(self, rho, vel, length, mu, temp)


class BCParallelComm(FaceSetData):
    """Communicates boundary conditions between interconnected ghosts."""

    def __init__(self):
        FaceSetData.__init__(self)

        self.face = FaceType.PARALLEL_COMM
        self.set_keys()

    def set_keys(self):
        keys = [FaceParameter.NEIGHBOUR_RANK]
        FaceSetData.set_storage(self, keys)

    def nondimensionalization(self, rho, vel, length, mu, temp):
        pass


def init_face_data(ftype):
    """Allocation of FaceSetData objects according to a given 'FaceType'."""
    switcher = {
        FaceType.FACETYPE_NONE: FaceSetNoData(FaceType.FACETYPE_NONE),
        FaceType.SIMPLE_INTERNAL: FaceSetNoData(FaceType.SIMPLE_INTERNAL),
        FaceType.SYMMETRY: FaceSetNoData(FaceType.SYMMETRY),
        FaceType.SUPERSONICEXIT: FaceSetNoData(FaceType.SUPERSONICEXIT),
        FaceType.SMOOTHWALL: FaceSetNoData(FaceType.SMOOTHWALL),
        FaceType.ADIABATICWALL: BCWall(FaceType.ADIABATICWALL),
        FaceType.FARFIELD: BCFarField(),
        FaceType.PRESSUREINLET: BCPInlet(),
        FaceType.BACKPRESSURE: BCBackP(),
        FaceType.VELOCITYINLET: BCVInlet(),
        FaceType.DIRICHLET_CONS: BCConsDirichlet(),
        FaceType.DIRICHLET: FaceSetNoData(FaceType.DIRICHLET),
        FaceType.PARALLEL_COMM: BCParallelComm(),
    }
    return switcher.get(ftype, FaceSetData())


class FaceSet:
    """Properties of a boundary segment associated to a subdomain.

    In this context, these sets are external boundaries, interfaces
    between subdomains or immersed sets. Their properties can
    be null (internal regular interfaces between elements defined
    solely for modelling purposes), define boundary conditions,
    precribe interface behavior between diferent materials or
    interface behavior between regions of the same material.

    The description here is associated to the geometric
    characteristics of the mesh (which is the way the user models the
    problem) and not to the specifics of numerical algorithms. For
    example, an interface between a solid and a fluid subdomains is
    described by an object of this class as an unique set having two
    neighbours. However, inside the numerical solution, this interface
    will be converted in two diferent sets (connected in some sense
    and each one hvingo only one neighbour), each one defining boundary
    conditions for each respective medium.

    In the same sense, sets of faces are only modeling
    abstractions. They do not exist inside the solver kernel (despite
    users' beliefs...).

    """

    def __init__(self, name=None):
        """Constructor of a VolumeSet object.

        Attributes:

            name (string): the (unique) name of this boundary.

            nneighbours (int in (1,2)): neighbourhood cardinality.

            left (string): Name of the subdomain (ElementSet) on the left
                (internal set). This volume set always exists in a well-posed
                2D problem.

            right (string): Name of the volume set on the right. Sets on the
                external boundary do not have an actual set on their right.
                This is defined only for internal sets of faces.

            data (FaceSetData): Properties of the set.

            altdata (FaceSetData): Alternate properties of the set (not 
                available yet).

            trigger (double or int): Instant for switching b.c. characteristics
                (not available yet).
        """

        self.name = name
        self.nneighbours = 0
        self.left = ''
        self.right = None  # standard case
        self.data = None
        self.altdata = None  # not available yet
        self.trigger = None  # not available yet

    def set_neighbours(self, left, right=None):
        self.left = left
        self.nneighbours = 1
        self.right = right
        if right:
            self.nneighbours += 1

    def set_face_type(self, ftype, init_face=init_face_data):
        self.data = init_face(ftype)


class ElementSet:
    """Description of a simulation subdomain (defined by a set of
       elements)."""

    def __init__(self, name=None):
        """Constructor of a VolumeSet object.

        Args:

            name (string) [in]

        Attributes:

            name (string): A name of the mesh subdomain where the physical
                properties represented by these set of equations will be
                applied to.

            equations (list(string)): List of names (strings) of equations
                (keys in the property tree).

            facesets (list(string)): List of names (strings) of facesets on the
                boundary of this subdomain.

        """

        self.name = name
        self.equations = []
        self.facesets = []

    def add_equation(self, equation):
        """Add equation names to the list associate to this subdomain.

        Args:

            equation (string) [in]: Name of a equation associated to this
                subdomain.

        """
        self.equations.append(equation)

    def get_number_equations(self):
        """Number of equations associated to this subdomain."""
        return len(self.equations)

    def add_faceset(self, faceset):
        """Add faceset names to the list associate to this subdomain.

        Args:

            faceset (string) [in]: Name of a neighbour faceset associated to 
                this subdomain.

        """
        self.facesets.append(faceset)

    def get_number_facesets(self):
        return len(self.facesets)

# -- elsets.py ---------------------------------------------------------------
