#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Model description
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#

from treelib import Tree, Node
from manticore.mdf.enums import MapType
from manticore.models.compressible.description.reference import ReferenceData
from manticore.models.compressible.description.elsets import (
    ElementSet, FaceSet )
from manticore.models.compressible.description.timint import init_time_acc
import manticore.models.compressible.description.equations as mant_descr_eqs
from manticore.models.compressible.description.post_process import (
    PostProcessData )


class ModelDescription:
    """Model objects contain all descriptive information about a simulation 
    model.

    """

    def __init__(self):
        """Model objects are made of a Tree object with a fixed
           structure of master nodes describing the main components of
           a simulation model. Specific model's data must be supplied
           as Node's data or new sub-nodes.

        """
        self.data = Tree()
        self.data.create_node(None, 'MODEL')  #root node
        self.data.create_node(None, 'FILES', 'MODEL')
        self.data.create_node(None, 'REFERENCE', 'MODEL', ReferenceData())
        self.data.create_node(None, 'MATERIALS', 'MODEL')
        self.data.create_node(None, 'EQUATIONS', 'MODEL')
        self.data.create_node(None, 'DOMAIN', 'MODEL')
        self.data.create_node(None, 'SOLUTION', 'MODEL')
        self.data.create_node(None, 'OUTPUT', 'MODEL')

        # FILE branch
        #                       tag        ID       parent   data
        self.data.create_node('MshFile' , 'MESH'   , 'FILES', '')
        self.data.create_node('InitFile', 'INITIAL', 'FILES', '')
        self.data.create_node('ResFile' , 'RESULTS', 'FILES', '')
        self.data.create_node('RstFile' , 'RESTART', 'FILES', '')
        self.data.create_node(None      , 'READXML', 'FILES', False)
        #
        self.data.create_node(None, 'INITIAL_TYPE', 'INITIAL', MapType.PER_ELEM)

        # DOMAIN branch
        self.data.create_node(None, 'ELEMENTSETS', 'DOMAIN')
        self.data.create_node(None, 'FACESETS', 'DOMAIN')

        # SOLUTION branch
        self.data.create_node(None, 'TIME_PARAMETERS', 'SOLUTION')
        self.data.create_node(None, 'TIME_INTEGRATION', 'SOLUTION')

        # OUTPUT branch
        self.data.create_node(None, 'POST_PROCESS', 'OUTPUT')
        self.data.create_node(None, 'POST_PROCESS_OPTIONS', 'POST_PROCESS')
        self.data.create_node(None, 'POST_PROCESS_DATA', 'POST_PROCESS',
                              PostProcessData(pressure=1.0) )

    def get_data(self, name):
        """Access any object stored in the model."""
        return self.data.get_node(name).data

    def set_reference(self, rho, vel, length, mu, temp):
        """Sets reference data values."""
        self.data.get_node('REFERENCE').data.set(rho, vel, length, mu, temp)

    def get_reference(self):
        """Returns dict object with indexes 'rho', 'vel', 'len', 'mu', 'temp'"""
        return self.data.get_node('REFERENCE').data

    def get_reference_data(self):
        """Returns dict object with indexes 'rho', 'vel', 'len', 'mu', 'temp'"""
        return self.data.get_node('REFERENCE').data.get_data()

    def set_post_process_data(self, **kwargs):
        self.data.get_node('POST_PROCESS_DATA').data.set(**kwargs)

    def get_post_process_data(self):
        return self.data.get_node('POST_PROCESS_DATA').data.get_data()

    def get_equations(self):
        """Returns list of Node objects."""
        return self.data.children('EQUATIONS')

    def get_equation(self, name):
        return self.data.get_node(name).data

    def get_materials(self):
        """Returns list of Node objects."""
        return self.data.children('MATERIALS')

    def get_material(self, name):
        return self.data.get_node(name).data

    def get_elementsets(self):
        """Returns list of Node objects."""
        return self.data.children('ELEMENTSETS')

    def get_elementset(self, name):
        return self.data.get_node(name).data

    def get_facesets(self):
        """Returns list of Node objects."""
        return self.data.children('FACESETS')

    def get_faceset(self, name):
        return self.data.get_node(name).data

    def find_elsets_by_equation(self, equation):
        """Returns list of Node objects."""

        vsets = self.data.children('ELEMENTSETS')
        vfound = list()

        for v in vsets:
            if equation in v.data.equations:
                vfound.append(v.data.name)

        return vfound

    def get_time_settings(self):
        return self.data.get_node('GENERIC_TIME_SETTINGS').data

    def get_time_integration(self, eq_name):
        name = 'TIME_INTEGRATOR_EQ_' + eq_name
        return self.data.get_node(name).data

    def init_equation(self, name, eqtype):
        self.data.create_node(None, name, 'EQUATIONS',
                              mant_descr_eqs.init_equation_data(eqtype))

        self.data.get_node(name).data.name = name

    def init_elementset(self, name):
        self.data.create_node(None, name, 'ELEMENTSETS', ElementSet(name))

    def init_faceset(self, name, ftype):
        self.data.create_node(None, name, 'FACESETS', FaceSet(name))
        self.data.get_node(name).data.set_face_type(ftype)

    def init_material(self, name, matdata):
        self.data.create_node(None, name, 'MATERIALS', matdata)
        self.data.get_node(name).data.init_material_model()  # matdata's method

    def init_time_settings(self, tacc):
        self.data.create_node(None, 'TIME_ACCURACY', 'TIME_PARAMETERS', tacc)
        self.data.create_node(None, 'GENERIC_TIME_SETTINGS', 'TIME_PARAMETERS',
                              init_time_acc(tacc))

    def init_time_integration(self, eq_name, tint):
        name = 'TIME_INTEGRATOR_EQ_' + eq_name
        self.data.create_node(None, name, 'TIME_INTEGRATION', tint)


# -- model.py -----------------------------------------------------------------
