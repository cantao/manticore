#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Thermophysical models
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#

import math
import numpy as np
import manticore.services.const as const
from manticore.services.mconsts import CTEs
from manticore.models.compressible.description.mdtypes import ThermoModel
from manticore.models.compressible.description.mdtypes import TransportModel
from manticore.models.compressible.description.mdtypes import EquationOfState


class SimpleSpecies:
    """Simple pure substance defined in terms of molar mass and number of
       moles.

       Notes:
           * Mixtures are a combination of multiple SimpleSpecies instances.

    """

    def __init__(self, species):
        """Constructor of SimpleSpecies objects.

        Args:
            species (dict): must contain 'NMOLES' and 'MOLARMASS' keys.

        Attributes:

            __DMolecularMass: mass per mole. DIMENSIONAL: the unit
                must be consistent with the other units.

            __NumberMoles: number of moles of this component. It is
                not used in a single species flow.

        """

        if 'NMOLES' and 'MOLARMASS' in species:
            self.__DMolecularMass = species['MOLARMASS']
            self.__NumberMoles    = species['NMOLES']
            
        else:
            raise RuntimeError('Please, check your arguments!')

    @property
    def DMolecularMass(self):
        return self.__DMolecularMass

    @property
    def NumberMoles(self):
        return self.__NumberMoles


class ThermoConstantCp:
    """Constant specific heat Cp model (with evaluation of entalphy and
    entropy).

    Notes:
        * For ideal gases Cp=Cp(T).

        * At low pressures, all real gases approach that ideal gas behavior.

        * Variation of specific heats with temperature is smooth
          and may be approximated as linear over small temperature
          intervals (a few hundred degrees or less).

        * Therefore, the specific heat can be replaced by the constant
          average of the specific heats evaluated at Tmin and Tmax.

        * The constant Cp model uses that average Cp value.

        * See Cengel, Thermodynamics, 5th ed. chapter 4.

    """

    def __init__(self, thermo):
        """ Constructor of ThermoConstantCp objects.

        Args:

            thermo (disct): must contain a 'CP' key.

        Attributes:

            DCp (double): dimensional constant pressure specific heat.

            DCv (double): dimensional constant volume specific heat.

            Cp (double): nondimensional constant pressure specific heat.

            Cv (double): nondimensional constant volume specific heat.

            gamma (double): Cp/Cv
        
        """
        
        if 'CP' in thermo:
            self.__DCp = thermo['CP']
            #

            self.__DCv = 0.0
            self.__Cp  = 0.0
            self.__Cv  = 0.0
            self.__gamma = 0.0 # Cp/Cv
            
        else:
            raise RuntimeError('Please, check your arguments!')

    @property
    def type(self):
        return ThermoModel.CONSTANTCP

    @property
    def DCp(self): return self.__DCp

    @property
    def DCv(self): return self.__DCv

    @DCv.setter
    def DCv(self, dcv):
        self.__DCv = dcv

    @property
    def Cp(self): return self.__Cp
    
    @property
    def Cv(self): return self.__Cv

    @property
    def gamma(self): return self.__gamma

    @gamma.setter
    def gamma(self, g):
        self.__gamma = g

    def nondimensionalization(self, rho, vel, length, mu, temp):
        if vel > const.INF:
            aux      = temp / (vel*vel)
            self.__Cp  = self.__DCp * aux  # nondimensional
            self.__Cv  = self.__DCv * aux  # nondimensional

        else:
            raise RuntimeError('Data is lower than the required precision!')

    def internal_energy(self, T, iE=None):
        if isinstance(iE, np.ndarray):
            np.multiply(self.__Cv, T, out=iE)
        else:
            return self.__Cv * T

class TransportInviscid:
    """Inviscid fluid.

    Notes:

        * The transport modelling concerns evaluating dynamic
          viscosity \mu, thermal conductivity \kappa and thermal diffusivity
          \alpha (for internal energy and enthalpy equations).
    """

    def __init__(self): pass
        
    @property
    def type(self): return TransportModel.INVISCD

    def mu(self, T=None):      
        return 0.0             # dynamic viscosity

    @property
    def Pr(self): return 0.0   # Prandtl number

    def nondimensionalization(self, rho, vel, length, mu, temp):
        pass


class TransportConstant:

    """Fluid with constant viscosity.

    Notes:

        * The transport modelling concerns evaluating dynamic
          viscosity \mu, thermal conductivity \kappa and thermal diffusivity
          \alpha (for internal energy and enthalpy equations).
    """
    
    def __init__(self, transport):
        """ Constructor of TransportConstant objects.

        Args:
            transport (dict): dictionary containing 'MU' and 'PRANDTL' keys.

        Attributes:

            DMu (double): dimensional constant dynamic viscosity.
            
            Pr  (double): Prandtl number, (\nu)/(\alpha) = (c_p\mu)/(\kappa)
            
            Mu  (double): nondimensional constant dynamic viscosity.

        """
        
        if 'MU' and 'PRANDTL' in transport:
            self.__DMu = transport['MU']
            self.__Pr  = transport['PRANDTL']
            #
            self.__Mu  = 0.0
        else:
            raise RuntimeError('Please, check your arguments!')

    @property
    def type(self): return TransportModel.CONSTANT

    @property
    def mu(self): return self.__Mu

    @property
    def Pr(self): return self.__Pr

    def nondimensionalization(self, rho, vel, length, mu, temp):
        if mu > const.INF:
            self.__Mu  = self.__DMu /mu  # nondimensional
        else:
            raise RuntimeError('Data is lower than the required precision!')


class TransportSutherland:

    """Fluid with viscosity described by the Sutherland's model.

    Notes:

        * The transport modelling concerns evaluating dynamic
          viscosity \mu, thermal conductivity \kappa and thermal diffusivity
          \alpha (for internal energy and enthalpy equations).

    """
    
    def __init__(self, transport):
        """ Constructor of TransportSutherland objects.

        Args:
            transport (dict): dictionary containing 'SUTHVISC',
                'SUTHCTE', 'SUTHTEMP', 'PRANDTL' keys.

        Attributes:

            DS     (double): dimensional Sutherland constant.
            DMuRef (double): dimensional reference viscosity at DTRef.
            DTRef  (double): dimensional absolute reference temperature (K).
            
            Pr  (double): Prandtl number, (\nu)/(\alpha) = (c_p\mu)/(\kappa)

            S     (double): nondimensional Sutherland constant.
            MuRef (double): nondimensional reference viscosity at DTRef.
            TRef  (double): nondimensional absolute reference temperature.
            
        """
        
        if 'SUTHVISC' and 'SUTHCTE' and 'SUTHTEMP' and 'PRANDTL' in transport:
            self.DS = transport['SUTHCTE']
            self.DMuRef = transport['SUTHVISC']
            self.DTRef  = transport['SUTHTEMP']
            self.__Pr   = transport['PRANDTL']
            #
            self.S      = 1.0
            self.MuRef  = 0.0
            self.TRef   = 1.0
        else:
            raise RuntimeError('Please, check your arguments!')

    @property    
    def type(self): return TransportModel.SUTHERLAND

    def mu(self, T):
        """
        Dynamic viscosity by the Sutherland model.

        Args:
        
           T (double): nondimensional absolute temperature.

        Notes:

            * See Smits, Alexander J.; Dussauge, Jean-Paul
              (2006). Turbulent shear layers in supersonic
              flow. Birkhauser. p. 46. ISBN 0-387-26140-0.

            * See https://en.wikipedia.org/wiki/Viscosity

            * Air: DS = 120K, DTRef = 291.15K, DMuRef = 18.27E-6 Pa.s

        """
        
        Mu = self.MuRef*((T/self.TRef)**1.5)*((self.TRef+self.S)/(T+self.S))
        return Mu

    @property
    def lambda_cte(self):
        cte = self.MuRef*(self.TRef+self.S)/(self.TRef**1.5)
        return cte

    @property
    def Pr(self): return self.__Pr
    
    def nondimensionalization(self, rho, vel, length, mu, temp):
        if mu > const.INF and temp > const.INF:
            self.MuRef  = self.DMuRef / mu
            self.S      = self.DS     / temp
            self.TRef   = self.DTRef  / temp

        else:
            raise RuntimeError('Data is lower than the required precision!')


class ConstantCpStagnation:
    """Computation of stagnation properties for compressible flow of
    ideal gas with constant c_p, c_v and gamma (polytropic gas). 

    Notes:
    
        * See Y. A. Cengel and M. A. Boles, Thermodynamics, 5th,
          Chapters 17 (Compressible Flow) and 7 (Entropy).

        * Some properties also assumes isentropic process until stagnation. 

    """

    def __init__(self, thermo_model):
        self.thermo = thermo_model                
                
    def total_temperature(self, static_temp, vel):
        """Stagnation temperature.

        Notes:
            * Assumes adiabatic process with no work until stagnation.

            * Nondimensional material properties are used here, which
              requires nondimensional temperature and nondimensional
              velocity as parameters and returns nondimensional
              temperature.

        """
        return static_temp + vel**2. / (2. * self.thermo.Cp)

    def total_pressure(self, static_press, static_temp, vel):
        """Stagnation pressure.

        Notes:
        
            * Assumes isentropic process (adiabatic and reversible)
              with no work until stagnation. The stagnation pressure
              computed this way is higher than actual stagnation
              pressure.

            * Note that the nondimensional material properties are
              used here, which requires nondimensional pressure,
              temperature and velocity as parameters and returns
              nondimensional pressure.

        """
        total_temp = self.total_temperature(static_temp, vel)

        gamma = self.thermo.gamma
        
        return (static_press*(total_temp/static_temp)**(gamma/(gamma-1.0)))
        
    def total_density(self, static_rho, static_temp, vel):
        """Stagnation density.

        Notes:

            * Assumes isentropic process (adiabatic and reversible)
              with no work until stagnation. The stagnation density
              computed this way is higher than actual stagnation
              density.

            * Note that the nondimensional material properties are
              used here, which requires nondimensional density,
              temperature and velocity as parameters and returns
              nondimensional density.

        """
        total_temp = self.total_temperature(static_temp, vel)

        gamma = self.thermo.gamma

        return (static_rho * (total_temp / static_temp)**(1.0 / (gamma - 1.0)))

    def mach(self, T0_T_ratio):
        return math.sqrt( 2.0 * (T0_T_ratio - 1.0) / (self.thermo.gamma-1.0) )


class IdealGasEquation:
    """State equation of ideal gases.

    """

    def __init__(self, equation):
        if 'GASCTE' in equation:
            self.DUniversalGasConstant = equation['GASCTE']
            #
            self.DR = 0.0
            self.R = 0.0
        else:
            raise RuntimeError('Please, check your arguments!')

    @property
    def type(self): return EquationOfState.IDEALGAS

    def set_model(self, specie, thermo):
        """Initialization of the equation of state.

        Args:
            specie: substance/mixture data.

            thermo: thermodynamic model.
        """
        
        if specie.DMolecularMass > const.INF and thermo.DCp  > const.INF:
            self.DR      = self.DUniversalGasConstant / specie.DMolecularMass
            thermo.gamma = (thermo.DCp / (thermo.DCp - self.DR))
            thermo.DCv = (thermo.DCp / thermo.gamma)

            self.thermo = thermo

            if thermo.type == ThermoModel.CONSTANTCP:
                self.stagnation = ConstantCpStagnation(thermo)
            else:
                raise RuntimeError('Please, check your arguments!')

        else:
            raise RuntimeError('Data is lower than the required precision!')

    def nondimensionalization(self, rho, vel, length, mu, temp):
        if vel > const.INF:
            aux = temp / (vel * vel)
            self.R = self.DR * aux  # nondimensional
        else:
            raise RuntimeError('Data is lower than the required precision!')

    def density(self, p, T, rho=None):

        if isinstance(rho, np.ndarray):
            np.divide(p, self.R, rho)
            rho /= T
        else:
            return p / (self.R * T)

    def density_isentropic_from_sonic_speed(self, rho0, p0, sonic_speed, rho):
        """Compute the density relative to a sonic_speed considering an
        isentropic process of a polytropic gas from (rho0, p0) to (rho, p) 
        where sonic_speed=sqrt(gama*p/rho).

        rho = ((a^2 * rho_0^gamma)/(gamma * p0))^(1/(gamma-1))

        Args:
             rho0 (np.array) [in]

             p0 (np.array) [in]

             sonic_speed (np.array) [in]

             rho (np.array) [out]

        Notes:
            * Polytropic gas: constant cp, cv, gamma.
        
        """
        gamma = self.thermo.gamma
        
        np.power(sonic_speed, 2., out=rho)
        rho *= np.power(rho0, gamma)
        rho /= p0
        rho /= gamma
        np.power(rho, 1./(gamma-1.), out=rho)


    def density_isentropic(self, rho1, T1, T2):
        """Compute the density relative to temperature T2 considering an
        isentropic process (adiabatic and reversible) of a polytropic gas 
        from (rho1, T1) to (rho2, T2).

        Notes:
            * Note that the nondimensional material properties are
              used here, which requires nondimensional density,
              temperature and velocity as parameters and returns
              nondimensional density.

            * If T2=stagnation_temperature, returns the isentropic stagnation 
              density.

        """
        gamma = self.thermo.gamma

        return (rho1 * (T2 / T1)**(1.0 / (gamma - 1.0)))

        
    def pressure(self, rho, T):
        """Ideal gas equation of state.

        Returns:
            Static pressure (double).

        """
        return self.R * rho * T

    def pressure_isentropic(self, p1, T1, T2, p2=None):
        """Compute the pressure relative to temperature T2 considering an
        isentropic process (adiabatic and reversible) of a polytropic gas 
        from (p1, T1) to (p2, T2)

        Notes:
        
            * If T2=stagnation_temperature, returns the isentropic stagnation 
              pressure. The stagnation pressure computed this way is higher 
              than actual stagnation pressure.

            * Note that the nondimensional material properties are
              used here, which requires nondimensional pressure,
              temperature and velocity as parameters and returns
              nondimensional pressure.

        """
        gamma = self.thermo.gamma
        
        if isinstance(p2, np.ndarray):
            np.divide(T2, T1, out=p2)
            np.power(p2, (gamma/(gamma-1.0)), out=p2)
            p2 *= p1

        else:
            return (p1*(T2/T1)**(gamma/(gamma-1.0)))    

        
    def pressure_from_flow_scalar(self, thermo, rho, rhoe, u, v):
        """Ideal gas equation of state.

        This version of the ideal gas equation of state computes the
        static pressure at a spatial point of the fluid using flow
        conservative values:
            gamma-1.0 = (cp-cv)/cv = R/cv
            rhoe-0.5*rho*(u*u+v*v) = rho*(ei), ei = internal energy
            ei2-ei1 = cv*(T2 - T1) => ei=cv*T

            Thus, (gamma-1.0)*(rhoe-0.5*rho*(u*u+v*v))= (R*rho*ei/cv)

        Returns:
            Static pressure (double).
        """
        return (thermo.gamma-1.0)*(rhoe-0.5*rho*(u*u+v*v))

    def pressure_from_flow(self, thermo, rho, rhoe, u, v, p):
        """Ideal gas equation of state.

        Version optimzed for Numpy arrays as arguments.

        Args:
             thermo [in]: thermodynamic model

             rho (np.array) [in]

             rhoe (np.array) [in]

             u (np.array) [in]

             v (np.array) [in]

             p (np.array) [out] : Static pressure.

        """
        np.power(u, 2., out=p)
        p += v*v
        p *= rho
        p *= -0.5
        p += rhoe
        p *= (thermo.gamma-1.0)

    def pressure_from_conserved(self, thermo, rho, rhou, rhov, rhoe, p):
        """Ideal gas equation of state.

        Version optimzed for numpy arrays as arguments.

        Args:
             thermo [in]: thermodynamic model

             rho (np.array) [in]

             rhou (np.array) [in]

             rhov (np.array) [in]

             rhoe (np.array) [in]

             p (np.array) [out] : Static pressure.

        """
        np.power(rhou, 2., out=p)
        p += rhov*rhov
        p /= rho
        p *= -0.5
        p += rhoe
        p *= (thermo.gamma-1.0)

    def pressure_from_speed_of_sound(self, rho, a, p):
        """Computes the speed of sound knowning that p/rho = R*T

        Version optimzed for numpy arrays as arguments.

        """
        np.power(a, 2., p)
        p *= rho
        p /= self.thermo.gamma

    def energy_from_flow(self, thermo, rho, u, v, p, rhoe):
        """Ideal gas equation of state.

        Version optimzed for Numpy arrays as arguments.

        Args:
             thermo [in]: thermodynamic model

             rho (np.array) [in]

             u (np.array) [in]

             v (np.array) [in]

             p (np.array) [in] : Static pressure.

             rhoe (np.array) [out]

        """
        np.power(u, 2., out=rhoe)
        rhoe += v*v
        rhoe *= rho
        rhoe *= 0.5
        rhoe += p/(thermo.gamma-1.0)

    def temperature_from_conserved(self, thermo, rho, rhou, rhov, rhoe, T):
        np.power(rhou, 2., out=T)
        T += rhov*rhov
        T /= rho
        T *= -0.5
        T += rhoe
        T /= rho
        T /= thermo.Cv

    def static_to_total_temperature_ratio(self,total_temperature,V2,T_T0_ratio):
        """Compute the T/T0 ratio, T static, T0 stagnation temperatures.

        Args:
            total_temperature (double) [in]: stagnation temperature.
            V2 (np.array) [in]: squared velocity norm at T.
            T_T0_ratio (np.array) [out]: (T/T0) ratio.

        """
        gamma = self.thermo.gamma
        
        a0_sqr     = gamma * self.R * total_temperature # stagnation sonic speed
        a_crit_sqr = 2.*a0_sqr/(gamma+1.)               # critical sonic speed

        np.divide(V2, a_crit_sqr, T_T0_ratio)
        T_T0_ratio *= (-(gamma-1.)/(gamma+1.))
        T_T0_ratio += 1.

    def temperature_ratio_isentropic(self, p1, p2):
        gamma = self.thermo.gamma

        return (p1/p2)**((gamma-1.)/gamma)
        

    def speed_of_sound_at_T(self, T):
        """Computes the speed of sound at T.

        Notes:

            * According to Y.A. Cengel, M.A. Boles, Thermodynamics, in
              the chapter about Compressible Flow.

            * Note that the nondimensional gas constant is used here,
              which requires nondimensional temperature as parameter
              and a nondimensional velocity is returned.

        """
        return math.sqrt(self.thermo.gamma * self.R * T)

    def speed_of_sound_scalar(self, thermo, rho, p):
        """Computes the speed of sound knowning that p/rho = R*T"""
        
        return math.sqrt(thermo.gamma * (p/rho))

    def speed_of_sound_scalar_sqr(self, thermo, rho, p):
        """Computes the square of the speed of sound knowning that p/rho=R*T"""
        
        return thermo.gamma * (p/rho)

    def speed_of_sound(self, thermo, rho, p, a):
        """Computes the speed of sound knowning that p/rho = R*T

        Version optimzed for numpy arrays as arguments.

        """
        np.divide(p, rho, a)
        a *= thermo.gamma
        np.sqrt(a, a)

    def speed_of_sound_sqr(self, rho, p, a2):
        """Computes the square of the speed of sound knowning that p/rho=R*T

        Version optimzed for numpy arrays as arguments.

        """        
        np.divide(p, rho, a2)
        a2 *= self.thermo.gamma

    def speed_of_sound_from_riemann(self, inv_plus, inv_minus, a):
        np.subtract(inv_plus, inv_minus, a)
        a *= (self.thermo.gamma-1.0)/4.

    def riemann_inv_sos_term(self, speed_of_sound, out=None):
        """Riemann invariant speed of sound (sos) term: inv_plus=v+out,
        inv_minus=v-out.
        """
        if isinstance(out, np.ndarray):
            np.copyto(out, speed_of_sound)
            out *= 2.0/(self.thermo.gamma-1.0)
        else:
            return (2.0/(self.thermo.gamma-1.0))*speed_of_sound
        
