#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# ReferenceData
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#

from functools import wraps
import manticore.services.const as const

def check_precision(f):
    """Decorator that checks the precision of a variable."""

    @wraps(f)
    def decorated(self, value):
        if value < const.INF:
            raise RuntimeError('Data cannot satisfy the required precision!')
        return f(self, value)

    return decorated


class ReferenceData:
    def __init__(self, rho=1.0, vel=1.0, length=1.0, mu=1.0, temp=1.0):
        self.set(rho, vel, length, mu, temp)

    def set(self, rho, vel, length, mu, temp):
        """All in one setter."""
        if any(var < const.INF for var in (rho, vel, length, mu, temp)):
            raise RuntimeError('Data cannot satisfy the required precision!')
        else:
            self.__rho = rho
            self.__vel = vel
            self.__lgt = length
            self.__mu = mu
            self.__temp = temp
            self.__update_reynolds()

    def get(self):
        return self.__rho, self.__vel, self.__lgt, self.__mu, self.__temp

    def get_data(self):
        return {
            'rho': self.__rho,
            'vel': self.__vel,
            'len': self.__lgt,
            'mu': self.__mu,
            'temp': self.__temp
        }

    # private
    def __update_reynolds(self):
        self.__Re = (self.__rho * self.__vel * self.__lgt) / self.__mu

    #
    # Properties
    #
    # The 'getters' are absolutely standard: they just return the
    # corresponding member.
    #
    @property
    def rho(self):
        return self.__rho

    @property
    def vel(self):
        return self.__vel

    @property
    def lgt(self):
        return self.__lgt

    @property
    def mu(self):
        return self.__mu

    @property
    def temp(self):
        return self.__temp

    @property
    def Re(self):
        return self.__Re

    #
    # The 'setter' on the other hand, automatically checks if the physical
    # quantity respects the minimal numerical precision. It also updates the
    # Reynolds number (this last one I couldn't turn into a decorator. It
    # seems that set setter is final.
    #
    @rho.setter
    @check_precision
    def rho(self, value):
        self.__rho = value
        self.__update_reynolds()

    @vel.setter
    @check_precision
    def vel(self, value):
        self.__vel = value
        self.__update_reynolds()

    @lgt.setter
    @check_precision
    def lgt(self, value):
        self.__lgt = value
        self.__update_reynolds()

    @mu.setter
    @check_precision
    def mu(self, value):
        self.__mu = value
        self.__update_reynolds()

    @temp.setter
    @check_precision
    def temp(self, value):
        self.__temp = value


from manticore.services.fieldvariables import FieldVariable

class DimensionConstants:
    """Dimension constants for field variables.

    Notes:

    * Dimension constant for total energy is rho*vel**2, which is
    consistent with the adimensionalization of the energy equation,
    where the same dimension constant is adopted for all the terms
    related to energy, such as the pressure.

    """

    def __init__(self, reference_data):
        self.cte = dict()

        self.cte[FieldVariable.RHO ] = reference_data.rho
        self.cte[FieldVariable.RHOU] = reference_data.rho*reference_data.vel
        self.cte[FieldVariable.RHOV] = reference_data.rho*reference_data.vel
        self.cte[FieldVariable.RHOE] = reference_data.rho*reference_data.vel**2

    def get(self, var):
        return self.cte.get(var, 1.0)

        


# -- reference.py -------------------------------------------------------------
