#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Model description
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#
import manticore

logger = manticore.logging.getLogger('MDL_CMP_DC')

class PostProcessOptions: pass

class PostProcessData:
    
    def __init__(self, **kwargs):
        params = {'pressure': None}

        # Sanity check: avoid mispellings!
        if not set(kwargs.keys()).issubset(params.keys()):
            raise RuntimeError('Please, check your arguments!')

        for param, default in params.items():
            setattr(self, '__'+param, kwargs.get(param, default))
            # logger.debug(
            #     'Created "%s" as "%s"', param, getattr(self, '__'+param))

    def get_data(self):
        return {            
            'pressure':self.__pressure
        }

    def set(self, **kwargs):
        self.__pressure = kwargs.get('pressure', None)
