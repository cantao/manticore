#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Model description abstractions
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#

from enum import Enum
from manticore.services.datatypes import AutoNumberEnum


class ThermoModel(Enum):
    CONSTANTCP = 1
    CONSTANTCV = 2


class TransportModel(Enum):
    INVISCID   = 1
    CONSTANT   = 2
    SUTHERLAND = 3


class EquationOfState(Enum):
    IDEALGAS       = 1
    INCOMPRESSIBLE = 2


class EqType(Enum):
    EQTYPE_NONE = 0
    EULER       = 1  # Euler equation
    NAVIERSTOKES= 2  # Navier-Stokes equation: laminar or DNS
    RANS        = 3  # Navier-Stokes equation with hooks to RANS models
    SA          = 4  # Uncoupled SA: use with RANS
    SST         = 5  # Uncoupled SST: use with RANS
    LES         = 6
    RANS_SA     = 7  # Coupled RANS
    RANS_SST    = 8  # Coupled RANS


class FaceType(AutoNumberEnum):
    FACETYPE_NONE   = ()  #
    SIMPLE_INTERNAL = ()  # Internal faces: no property
    SYMMETRY        = ()  #
    SMOOTHWALL      = ()  # Adiabatic wall without friction (a symmetry
                          # condition)
    ADIABATICWALL   = ()  # Adiabatic wall with friction
    FARFIELD        = ()  # Freestream BC based on Riemann invariants
    PRESSUREINLET   = ()  # Inlet with fixed pressure (extrapolation)
    VELOCITYINLET   = ()  # Inlet with fixed velocity (extrapolation)
    BACKPRESSURE    = ()  # Outlet with fixed pressure (extrapolation)
    SUPERSONICEXIT  = ()  # Extrapolation outlet
    DIRICHLET       = ()  # Dirichlet used only for validation tests
    DIRICHLET_CONS  = ()  # Properties directly imposed on conservative
                          # quantities
    PARALLEL_COMM   = ()  # Boundary between partitions


class FaceParameter(AutoNumberEnum):
    FACEPARAM_NONE = ()
    VEL            = ()  # Velocity magnitude
    VELX           = ()  # Velocity component in \f$X\f$-direction
    VELY           = ()  # Velocity component in \f$Y\f$-direction
    VELZ           = ()  # Velocity component in \f$Z\f$-direction
    STATIC_TEMP    = ()  # Static temperature
    STATIC_PRESS   = ()  # Static pressure
    TOTAL_PRESS    = ()  # Total pressure
    TOTAL_TEMP     = ()  # Total temperature
    DIRX           = ()  # \f$X\f$-direction
    DIRY           = ()  # \f$Y\f$-direction
    DIRZ           = ()  # \f$Z\f$-direction
    DENSITY        = ()  # Density
    MOMENTUMX      = ()  # Momentum component 1
    MOMENTUMY      = ()  # Momentum component 2
    MOMENTUMZ      = ()  # Momentum component 3
    TOTALENERGY    = ()  # Total energy
    NEIGHBOUR_RANK = ()  # MPI rank of the neighbouring set


class FormulationType(Enum):
    FORMTYPE_NONE = 0
    STRONG        = 1  # Strong DG formulation
    WEAK          = 2  # Weak DG formulation


class EqParameter(AutoNumberEnum):
    EQPAR_NONE       = ()
    CONVECTIVE_FLUX  = ()  # pval: type of convective flix
    VISCOUS_FLUX     = ()  # pval: type of viscous flux
    VISCOSITY_MODEL  = ()  # TODO (REQUIRED???) pval: type of viscosity model
    TURBULENCE_MODEL = ()  # pval: type of turbulence model
    PRINCIPAL_DGFORM = ()  # pval: type of DG form
    DAUXILIAR_DGFORM = ()  # pval: type of DG form
    VAUXILIAR_DGFORM = ()  # pval: type of DG form
    BR2_ETA          = ()  # rval: real parameter for the BR2 method
    ROECLIPPING      = ()  # bval: will clipping be applied to the Roe's flux?
    ROECLIPCTE       = ()  # rval: alp_Roe of Bru3D
    ROECLIPCTEFRAC   = ()  # rval: fracRoe of Bru3D
    ROECLIPCTESTATUS = ()  # pval: ROECLIPMAX or ROECLIPDCR
    ROECLIPCTELIM    = ()  # ival: iteration limit for ROECLIPDCR
    ROECLIPSPECTRAL  = ()  # bool: spectral clipping?
    DISSIPATION      = ()  # pval: type of dissipation: ALGEBRAIC or PDE
    DISSIPATIVE_FLUX = ()  # pval: type of viscous flux used in art. dissipation
    D_INDICATOR      = ()  # pval: RESOLUTION or JUMP
    D_PHI0A          = ()  # rval: indicator scaling constant
    D_PHI0B          = ()  # rval: indicator scaling constant
    D_DELTAPHI       = ()  # rval: indicator scaling constant
    D_THETAS         = ()  # rval: artificial dissipation scaling constant
    D_THETACTE       = ()  # rval: artificial dissipation scaling constant
    D_SCALING        = ()  # pval: type of dissipation scaling LOG or STD
    D_ALGEBRAIC_TYPE = ()  # pval: discontinuous or connected
    BUOYANCY         = ()  # bool


class EqSetting(AutoNumberEnum):
    EQSET_NONE       = ()
    CONVECTIVE_ROE   = ()
    CONVECTIVE_HLLC  = ()
    VISCOUS_BR1      = ()
    VISCOUS_BR2      = ()
    VISCOUS_CDG      = ()
    VISCOUS_LDG      = ()
    STRONGFORM       = ()
    WEAKFORM         = ()
    NO_VISCOSITY     = ()  # TODO (REQUIRED??? SEE TransportModel)
    CTE_VISCOSITY    = ()  # TODO (REQUIRED??? SEE TransportModel)
    SUTH_VISCOSITY   = ()  # TODO (REQUIRED??? SEE TransportModel)
    NO_TURBULENCE    = ()
    RANS_SA          = ()  # Uncoupled SA turbulence: used with EqType.RANS
    RANS_SST         = ()  # Uncoupled SST turbulence: used with EqType.RANS
    ROECLIPMAX       = ()  # Roe clipping stays at the maximum value
    ROECLIPDCR       = ()  # Roe clipping decreases over time
    DISSIP_ALGEBRAIC = ()  # Artificial dissipation algebraically computed
    DISSIP_PDE       = ()  # Artificial dissipation computed by PDE
    D_ALGEBRAIC_DISC = ()  # Default algebraically computed dissipation
    D_ALGEBRAIC_C0   = ()  # Post connected algebraically computed dissipation
    D_INDICATOR_RES  = ()  # Resolution indicator
    D_INDICATOR_JUMP = ()  # Jump indicator
    D_SCALING_LOG    = ()  # Logaritmic scaling of dissipation indicator
    D_SCALING_STD    = ()  # Standard scaling of dissipation indicator


class TISetting(AutoNumberEnum):
    TISETTING_NONE            = ()
    STOPCRITERIA_MAX          = ()
    STOPCRITERIA_RMS          = ()
    CFL_CONSTANT              = ()
    CFL_LINEAR                = ()
    TACC_STEADYSTATE          = ()  # Time accuracy: steady-state
    TACC_TRANSIENT            = ()  # Time accuracy: transient
    TSTEP_CONSTANT            = ()  # Spatially constant time steps
    TSTEP_VARIABLE            = ()  # Spatially variable time steps
    INTEGRATOR_SSP54RK        = ()
    INTEGRATOR_EXPLICIT_EULER = ()
    INTEGRATOR_POINT_IMPLICIT = ()
