#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execution directives
#
# @addtogroup MODELS.COMPRESSIBLE
# @author cacs (claudio.acsilva@gmail.com)
#

from enum import Enum

class ExecDirective(Enum):
    STANDARD         = 0
    NULL_DIRICHLET   = 1
    WARBURTON_EX_6_1 = 2
    CTE_EULER_FIELDS = 3
    NS_VALIDATION    = 4

    def process(dir):
        EXEC_DIRECTIVE = {
            'STANDARD'        : ExecDirective.STANDARD,
            'NULL_DIRICHLET'  : ExecDirective.NULL_DIRICHLET,
            'WARBURTON_EX_6_1': ExecDirective.WARBURTON_EX_6_1,
            'CTE_EULER_FIELDS': ExecDirective.CTE_EULER_FIELDS,
            'NS_VALIDATION'   : ExecDirective.NS_VALIDATION
            }

        return EXEC_DIRECTIVE.get(dir)
