#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Viscous fluxes
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#
import numpy as np
import manticore
import manticore.services.const as const
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import FieldRole, EntityRole
from manticore.models.compressible.equations.workspaces import(
    FaceReconstructWsp_f,
    ViscousBR1Wsp_f, ViscousBR1Wsp_v, ViscousBR1Wsp_p)

logger = manticore.logging.getLogger('MDL_CMP_EQ')

def TauII(Mu, A, B, Tau, Mult=1.0):
    # Tau = ( ( (2.0*Mu) * ( 2.0*A-B ) )/3.0 ) * Mult
    np.multiply(2., A, out=Tau)
    Tau -= B
    Tau *= 2./3.
    Tau *= Mu
    Tau *= Mult

def Tau11(Mu, Ux, Vy, Tau, Mult=1.0):
    TauII(Mu, Ux, Vy, Tau, Mult)

def Tau22(Mu, Ux, Vy, Tau, Mult=1.0):
    TauII(Mu, Vy, Ux, Tau, Mult)
    
def Tau12(Mu, Uy, Vx, Tau, Mult=1.0):
    # Tau = ( Mu * ( Uy+Vx ) ) * Mult
    np.add(Uy, Vx, out=Tau)
    Tau *= Mu
    Tau *= Mult

def Beta1(Gamma, Pr, Mu, T11,  T12, U, V, dE, Fv, Mult = 1.0):
    # Fv = ( T11*U + T12*V + Gamma*Mu*dE/ Pr ) * Mult
    np.multiply(Mu, dE, out=Fv)
    Fv *= Gamma/Pr
    aux = T12*V
    Fv += aux
    np.multiply(T11, U, out=aux)
    Fv += aux
    Fv *= Mult

def BETA1(Gamma, Pr, Mu, T11,  T12, U, V, dE, Fv):
    # Fv = ( T11*U + T12*V + Gamma*Mu*dE/ Pr )
    np.multiply(Mu, dE, out=Fv)
    Fv *= Gamma/Pr
    aux = T12*V
    Fv += aux
    np.multiply(T11, U, out=aux)
    Fv += aux

def Beta2(Gamma, Pr, Mu, T21,  T22, U, V, dE, Fv, Mult = 1.0):
    # Fv = ( T21*U + T22*V + Gamma*Mu*dE/ Pr ) * Mult
    np.multiply(Mu, dE, out=Fv)
    Fv *= Gamma/Pr
    aux = T22*V
    Fv += aux
    np.multiply(T21, U, out=aux)
    Fv += aux
    Fv *= Mult

def BETA2(Gamma, Pr, Mu, T21,  T22, U, V, dE, Fv):
    # Fv = ( T21*U + T22*V + Gamma*Mu*dE/ Pr )
    np.multiply(Mu, dE, out=Fv)
    Fv *= Gamma/Pr
    aux = T22*V
    Fv += aux
    np.multiply(T21, U, out=aux)
    Fv += aux

#
# First viscous flux equation components
#
def ViscousFlux_1_X(Fv):
    Fv.fill(0.0)

def ViscousFlux_1_Y(Fv):
    Fv.fill(0.0)

#
# Second viscous flux equation components
#
def ViscousFlux_2_X(Re, Mu, Ux, Vy, Fv):
    Tau11(Mu, Ux, Vy, Fv, 1.0/Re)

def ViscousFlux_2_Y(Re, Mu, Uy, Vx, Fv):
    Tau12(Mu, Uy, Vx, Fv, 1.0/Re)

#
# Third viscous flux equation components
#
def ViscousFlux_3_X(Re, Mu, Uy, Vx, Fv):
    Tau12(Mu, Uy, Vx, Fv, 1.0/Re) # Tau21==Tau12

def ViscousFlux_3_Y(Re, Mu, Ux, Vy, Fv):
    Tau22(Mu, Ux, Vy, Fv, 1.0/Re)

#
# Fourth viscous flux equation components
#
def ViscousFlux_4_X(Re, Gamma, Pr, Mu, T11, T12, U, V, dE, Fv):
    Beta1(Gamma, Pr, Mu, T11, T12, U, V, dE, Fv, 1.0/Re)

def ViscousFlux_4_Y(Re, Gamma, Pr, Mu, T21, T22, U, V, dE, Fv):
    Beta2(Gamma, Pr, Mu, T21, T22, U, V, dE, Fv, 1.0/Re)


class ViscousBR1:
    """Viscous Bassi-Rebay numerical flux, version 1."""

    Workspace_f = ViscousBR1Wsp_f
    Workspace_v = ViscousBR1Wsp_v
    Workspace_p = ViscousBR1Wsp_p

    def __init__(self, model_desc, eqname):

        matname = model_desc.get_equation(eqname).mat
        self.fluid = model_desc.get_material(matname)

        self.gamma    = self.fluid.thermo.gamma
        self.Prandtl  = self.fluid.transport.Pr
        self.Reynolds = model_desc.get_reference().Re

    def eval(self, e, ff):

        # Number of integration points for computations on face ff
        # (max of the pair I/E)
        nip = e.face_comm_size(ff)

        # Workspace associated to this flux: room for all BR1 flux
        # temporaries on *one* face.
        Wsp_f = ViscousBR1Wsp_f.get_instance(nip)

        # Neighbour element
        ng = e.get_neighbour(ff)
        ng_ff   = e.get_neighbour_face_id(ff)
        ng_role = ng.role
        e_type  = e.type

        state = FieldRole.State

        # Current element (internal) reconstruction                 
        e.get_face_field(state, FieldVariable.DUDX,  ff, Wsp_f.dudxI)
        e.get_face_field(state, FieldVariable.DUDY,  ff, Wsp_f.dudyI)

        e.get_face_field(state, FieldVariable.DVDX,  ff, Wsp_f.dvdxI)
        e.get_face_field(state, FieldVariable.DVDY,  ff, Wsp_f.dvdyI)

        e.get_face_field(state, FieldVariable.DEIDX, ff, Wsp_f.deidxI)
        e.get_face_field(state, FieldVariable.DEIDY, ff, Wsp_f.deidyI)

        e.get_face_field(state, FieldVariable.MU,    ff, Wsp_f.muI)

        # Neighbour element (external) reconstruction
        if ((ng_role==EntityRole.PHYSICAL) or (ng_role==EntityRole.COMM_GHOST)):
            
            ng.get_face_field_as_passive(
                state, FieldVariable.DUDX, e_type, ff, nip, ng_ff, Wsp_f.dudxE)
            ng.get_face_field_as_passive(
                state, FieldVariable.DUDY, e_type, ff, nip, ng_ff, Wsp_f.dudyE)

            ng.get_face_field_as_passive(
                state, FieldVariable.DVDX, e_type, ff, nip, ng_ff, Wsp_f.dvdxE)
            ng.get_face_field_as_passive(
                state, FieldVariable.DVDY, e_type, ff, nip, ng_ff, Wsp_f.dvdyE)

            ng.get_face_field_as_passive(
                state, FieldVariable.DEIDX, e_type, ff, nip, ng_ff,Wsp_f.deidxE)
            ng.get_face_field_as_passive(
                state, FieldVariable.DEIDY, e_type, ff, nip, ng_ff,Wsp_f.deidyE)

            ng.get_face_field_as_passive(
                state, FieldVariable.MU, e_type, ff, nip, ng_ff, Wsp_f.muE)

        else: # ng_role==EntityRole.GHOST
            
            # If we are on a ghost, the boundary condition for BR1 is
            # copying internal values
            np.copyto(Wsp_f.dudxE, Wsp_f.dudxI)
            np.copyto(Wsp_f.dudyE, Wsp_f.dudyI)

            np.copyto(Wsp_f.dvdxE, Wsp_f.dvdxI)
            np.copyto(Wsp_f.dvdyE, Wsp_f.dvdyI)

            np.copyto(Wsp_f.deidxE, Wsp_f.deidxI)
            np.copyto(Wsp_f.deidyE, Wsp_f.deidyI)

            np.copyto(Wsp_f.muE, Wsp_f.muI)

        # First equation
        ViscousFlux_1_X( Wsp_f.F_star[0][0] )
        ViscousFlux_1_Y( Wsp_f.F_star[0][1] )

        # Second equation
        Tau11( Wsp_f.muI, Wsp_f.dudxI, Wsp_f.dvdyI, Wsp_f.t11I )
        Tau11( Wsp_f.muE, Wsp_f.dudxE, Wsp_f.dvdyE, Wsp_f.t11E )

        Tau12( Wsp_f.muI, Wsp_f.dudyI, Wsp_f.dvdxI, Wsp_f.t12I )
        Tau12( Wsp_f.muE, Wsp_f.dudyE, Wsp_f.dvdxE, Wsp_f.t12E )

        np.add(Wsp_f.t11I, Wsp_f.t11E, out=Wsp_f.F_star[1][0])
        Wsp_f.F_star[1][0] *= 0.5
        np.add(Wsp_f.t12I, Wsp_f.t12E, out=Wsp_f.F_star[1][1])
        Wsp_f.F_star[1][1] *= 0.5

        # Third equation
        Tau22( Wsp_f.muI, Wsp_f.dudxI, Wsp_f.dvdyI, Wsp_f.t22I )
        Tau22( Wsp_f.muE, Wsp_f.dudxE, Wsp_f.dvdyE, Wsp_f.t22E )

        np.add(Wsp_f.t12I, Wsp_f.t12E, out=Wsp_f.F_star[2][0])
        Wsp_f.F_star[2][0] *= 0.5
        np.add(Wsp_f.t22I, Wsp_f.t22E, out=Wsp_f.F_star[2][1])
        Wsp_f.F_star[2][1] *= 0.5

        # We are now reusing information from the convective residual that
        # must have been executed over this cell just before entering
        # here!
        Face_f = FaceReconstructWsp_f.get_instance(*(e.face_comm_sizes) )
        
        BETA1(self.gamma, self.Prandtl, Wsp_f.muI, Wsp_f.t11I, Wsp_f.t12I,
                  Face_f.uI[ff], Face_f.vI[ff], Wsp_f.deidxI, Wsp_f.b1I)
        BETA1(self.gamma, self.Prandtl, Wsp_f.muE, Wsp_f.t11E, Wsp_f.t12E,
                  Face_f.uE[ff], Face_f.vE[ff], Wsp_f.deidxE, Wsp_f.b1E)

        BETA2(self.gamma, self.Prandtl, Wsp_f.muI, Wsp_f.t12I, Wsp_f.t22I,
                  Face_f.uI[ff], Face_f.vI[ff], Wsp_f.deidyI, Wsp_f.b2I)
        BETA2(self.gamma, self.Prandtl, Wsp_f.muE, Wsp_f.t12E, Wsp_f.t22E,
                  Face_f.uE[ff], Face_f.vE[ff], Wsp_f.deidyE, Wsp_f.b2E)

        np.add(Wsp_f.b1I, Wsp_f.b1E, out=Wsp_f.F_star[3][0])
        Wsp_f.F_star[3][0] *= 0.5
        np.add(Wsp_f.b2I, Wsp_f.b2E, out=Wsp_f.F_star[3][1])
        Wsp_f.F_star[3][1] *= 0.5

    def tilda(self, e, ff):
        
        # Number of integration points for computations on face ff
        # (max of the pair I/E)
        nip = e.face_comm_size(ff)

        # Workspace associated to this flux: room for all BR1 flux
        # temporaries on *one* face.
        Wsp_f = ViscousBR1Wsp_f.get_instance(nip)

        # Accessing face reconstruction workspace: we are assuming
        # that Face_f.reconstruct was called just before entering in
        # the present context and then the ff-th face's data is stored
        # in the face reconstruction workspace.
        Face_f = FaceReconstructWsp_f.get_instance(*(e.face_comm_sizes) )

        # eI = Face_f.rhoeI[ff]/Face_f.rhoI[ff] - 0.5*(
        #     Face_f.uI[ff]**2 + Face_f.vI[ff]**2 )
        np.power(Face_f.uI[ff], 2., out=Wsp_f.eI)
        np.power(Face_f.vI[ff], 2., out=Wsp_f.temp)
        Wsp_f.eI += Wsp_f.temp
        Wsp_f.eI *= -0.5
        np.divide(Face_f.rhoeI[ff], Face_f.rhoI[ff], out=Wsp_f.temp)
        Wsp_f.eI += Wsp_f.temp

        # eE = Face_f.rhoeE[ff]/Face_f.rhoE[ff] - 0.5*(
        #     Face_f.uE[ff]**2 + Face_f.vE[ff]**2 )
        np.power(Face_f.uE[ff], 2., out=Wsp_f.eE)
        np.power(Face_f.vE[ff], 2., out=Wsp_f.temp)
        Wsp_f.eE += Wsp_f.temp
        Wsp_f.eE *= -0.5
        np.divide(Face_f.rhoeE[ff], Face_f.rhoE[ff], out=Wsp_f.temp)
        Wsp_f.eE += Wsp_f.temp

        # For BR1, tilda means average:
        np.add(Face_f.uI[ff], Face_f.uE[ff], out=Wsp_f.uTilda)
        Wsp_f.uTilda *= 0.5
        np.add(Face_f.vI[ff], Face_f.vE[ff], out=Wsp_f.vTilda)
        Wsp_f.vTilda *= 0.5
        np.add(Wsp_f.eI, Wsp_f.eE, out=Wsp_f.eTilda)
        Wsp_f.eTilda *= 0.5
        
    
    
