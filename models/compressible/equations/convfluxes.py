#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Convective fluxes
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#
import numpy as np
import manticore
import manticore.services.const as const
from manticore.models.compressible.equations.workspaces import(
    FaceReconstructWsp_f, ConvectiveRoeWsp_f, ConvectiveHLLCWsp_f)

logger = manticore.logging.getLogger('MDL_CMP_EQ')

#
# First convective flux equation components
#
def ConvectiveFlux_1_X(rho, u, Fe):
    np.multiply(rho, u, Fe)

def ConvectiveFlux_1_Y(rho, v, Fe):
    np.multiply(rho, v, Fe)

#
# Second convective flux equation components
#    
def ConvectiveFlux_2_X(rhou, u, p, Fe):
    np.multiply(rhou, u, Fe)
    Fe += p

def ConvectiveFlux_2_Y(rhou, v, Fe):
    np.multiply(rhou, v, Fe)

#
# Third convective flux equation components
#    
def ConvectiveFlux_3_X(rhov, u, Fe):
    np.multiply(rhov, u, Fe)

def ConvectiveFlux_3_Y(rhov, v, p, Fe):
    np.multiply(rhov, v, Fe)
    Fe += p    

#
# Fourth convective flux equation components
#    
def ConvectiveFlux_4_X(rhoe_plus_p, u, Fe):
    np.multiply(rhoe_plus_p, u, Fe)

def ConvectiveFlux_4_Y(rhoe_plus_p, v, Fe):
    np.multiply(rhoe_plus_p, v, Fe)


class ConvectiveRoe:
    """Roe's numerical flux."""

    Workspace_f = ConvectiveRoeWsp_f

    def __init__(self, model_desc, eqname):

        matname = model_desc.get_equation(eqname).mat
        self.fluid = model_desc.get_material(matname)

    def eval(self, e, ff):

        # Number of integration points for computations on face ff
        # (max of the pair I/E)
        nip = e.face_comm_size(ff)

        # Workspace associated to this flux: room for all Roe flux
        # temporaries on *one* face.
        Wsp_f  = ConvectiveRoeWsp_f.get_instance(nip)

        # Accessing face reconstruction workspace: we are assuming
        # that Face_f.reconstruct was called just before entering in
        # the present context and then the ff-th face's data is stored
        # in the face reconstruction workspace.
        Face_f = FaceReconstructWsp_f.get_instance(*(e.face_comm_sizes) )

        # Notation: E suffix stands for "external" (neighbour) and I
        # stands for "internal" (the current element)

        # Roe weighted averages:
        np.sqrt(Face_f.rhoI[ff], out=Wsp_f.tempI) # tempI = sqrt(rhoI[ff])
        np.sqrt(Face_f.rhoE[ff], out=Wsp_f.tempE) # tempE = sqrt(rhoE[ff])
        #     temp = tempI + tempE
        np.add(Wsp_f.tempI, Wsp_f.tempE, out=Wsp_f.temp) 

        # Roe average properties:
        #     rho_star = tempI*tempE
        np.multiply(Wsp_f.tempI, Wsp_f.tempE, out=Wsp_f.rho_star)
        #     u_star = (tempI*uI+tempE*uE)/temp
        np.multiply(Wsp_f.tempI, Face_f.uI[ff], out=Wsp_f.u_star)
        np.multiply(Wsp_f.tempE, Face_f.uE[ff], out=Wsp_f.aux)
        Wsp_f.u_star += Wsp_f.aux
        Wsp_f.u_star /= Wsp_f.temp    
        #     v_star = (tempI*vI+tempE*vE)/temp
        np.multiply(Wsp_f.tempI, Face_f.vI[ff], out=Wsp_f.v_star)
        np.multiply(Wsp_f.tempE, Face_f.vE[ff], out=Wsp_f.aux)
        Wsp_f.v_star += Wsp_f.aux
        Wsp_f.v_star /= Wsp_f.temp
        #     H_star = (tempI*HI+tempE*HE)/temp
        np.multiply(Wsp_f.tempI, Face_f.HI[ff], out=Wsp_f.H_star)
        np.multiply(Wsp_f.tempE, Face_f.HE[ff], out=Wsp_f.aux)
        Wsp_f.H_star += Wsp_f.aux
        Wsp_f.H_star /= Wsp_f.temp
        
        # Temporaries:
        #     T1 = 0.5*(u_star**2 + v_star**2)
        np.multiply(Wsp_f.u_star, Wsp_f.u_star, out=Wsp_f.T1)
        np.multiply(Wsp_f.v_star, Wsp_f.v_star, out=Wsp_f.aux)
        Wsp_f.T1 += Wsp_f.aux
        Wsp_f.T1 *= 0.5

        # Square of the local speed of sound (for a perfect gas):
        gamma = self.fluid.thermo.gamma
        #     a2 = ei + p/rho = h
        np.subtract(Wsp_f.H_star, Wsp_f.T1, out=Wsp_f.a2)
        #     a2 = (gamma - 1.0)*h = a**2
        Wsp_f.a2 *= (gamma - 1.0)     

        if Wsp_f.a2.min() < const.INF:
            msg = "Inconsistency when computing Roe's flux intermediate state!!"
            msg+= '\nSquare of LOCAL SPEED OF SOUND is negative or too small!'
            msg+= '\nElement : {}'.format(e.ID)
            msg+= ' Face : {}'.format(ff)
            msg+= '\nA^2 : \n{}'.format(Wsp_f.a2)
            raise AssertionError(msg)

        #     a = sqrt(a**2) 
        np.sqrt(Wsp_f.a2, out=Wsp_f.a)
        #     T2 = (gamma - 1.0)/a2 = 1/h
        Wsp_f.T2.fill(gamma - 1.0)
        Wsp_f.T2 /= Wsp_f.a2
        #     T4 = T1 * T2 = (1/h) * kinect_energy
        np.multiply(Wsp_f.T1, Wsp_f.T2, out=Wsp_f.T4) 

        # Normals to face ff at the integration points
        n = Face_f.n[ff] 

        # Normal velocity: vn = u_star * n[0,:] + v_star * n[1,:]
        np.multiply(Wsp_f.u_star, n[0,:], out=Wsp_f.vn)
        np.multiply(Wsp_f.v_star, n[1,:], out=Wsp_f.aux)
        Wsp_f.vn += Wsp_f.aux

        
        #
        # Roe Matrix application:
        #
        
        #     d_rho  = rhoE[ff] - rhoI[ff]
        np.subtract(Face_f.rhoE[ff],  Face_f.rhoI[ff],  out=Wsp_f.d_rho)
        #     d_rhou = rhouE[ff] - rhouI[ff]
        np.subtract(Face_f.rhouE[ff], Face_f.rhouI[ff], out=Wsp_f.d_rhou)
        #     d_rhov = rhovE[ff] - rhovI[ff]
        np.subtract(Face_f.rhovE[ff], Face_f.rhovI[ff], out=Wsp_f.d_rhov)
        #     d_rhoe = rhoeE[ff] - rhoeI[ff]
        np.subtract(Face_f.rhoeE[ff], Face_f.rhoeI[ff], out=Wsp_f.d_rhoe)

        #
        #     BIG WARNING!
        #     We are *reusing* temp, tempI and tempE, as 
        #     they are no longer needed
        #
        
        #     tempI = (1. - T4) * d_rho
        Wsp_f.tempI.fill(1.0)
        Wsp_f.tempI -= Wsp_f.T4
        Wsp_f.tempI *= Wsp_f.d_rho  

        #     tempE = T2*(u_star*d_rhou +  v_star*d_rhov - d_rhoe)
        np.multiply(Wsp_f.u_star, Wsp_f.d_rhou, out=Wsp_f.tempE)
        np.multiply(Wsp_f.v_star, Wsp_f.d_rhov, out=Wsp_f.aux)
        Wsp_f.tempE += Wsp_f.aux
        Wsp_f.tempE -= Wsp_f.d_rhoe
        Wsp_f.tempE *= Wsp_f.T2

        #     Wsp_f.temp = abs(Wsp_f.vn)
        np.absolute(Wsp_f.vn, out=Wsp_f.temp)

        # Product with first row of L
        # Wsp_f.LL1 = Wsp_f.temp * ( n[0] * (Wsp_f.tempI + Wsp_f.tempE) )
        np.add(Wsp_f.tempI, Wsp_f.tempE, out=Wsp_f.LL1)
        Wsp_f.LL1 *= n[0,:]
        Wsp_f.LL1 *= Wsp_f.temp
        
        # Product with second row of L
        # Wsp_f.LL2 = Wsp_f.temp * ( n[1] * (Wsp_f.tempI + Wsp_f.tempE) )
        np.add(Wsp_f.tempI, Wsp_f.tempE, out=Wsp_f.LL2)
        Wsp_f.LL2 *= n[1,:]
        Wsp_f.LL2 *= Wsp_f.temp

        # Product with third row of L
        # Wsp_f.LL3 = Wsp_f.temp * ( 
        #     + ((n[0] * Wsp_f.v_star - n[1] * Wsp_f.u_star) * Wsp_f.d_rho
        #     +   n[1] * Wsp_f.d_rhou - n[0] * Wsp_f.d_rhov) / Wsp_f.a)
        np.multiply(Wsp_f.v_star, n[0,:], out=Wsp_f.LL3)
        np.multiply(Wsp_f.u_star, n[1,:], out=Wsp_f.aux)
        Wsp_f.LL3 -= Wsp_f.aux
        Wsp_f.LL3 *= Wsp_f.d_rho
        np.multiply(n[1,:], Wsp_f.d_rhou, out=Wsp_f.aux)
        Wsp_f.LL3 += Wsp_f.aux
        np.multiply(n[0,:], Wsp_f.d_rhov, out=Wsp_f.aux)
        Wsp_f.LL3 -= Wsp_f.aux
        Wsp_f.LL3 /= Wsp_f.a
        Wsp_f.LL3 *= Wsp_f.temp
        
        # Wsp_f.tempI = ( n[0] * Wsp_f.d_rhou + n[1] * Wsp_f.d_rhov ) / Wsp_f.a
        np.multiply(n[0,:], Wsp_f.d_rhou, out=Wsp_f.tempI)
        np.multiply(n[1,:], Wsp_f.d_rhov, out=Wsp_f.aux)
        Wsp_f.tempI += Wsp_f.aux
        Wsp_f.tempI /= Wsp_f.a
        
        # Product with fourth row of L
        # Wsp_f.LL4 = 0.5 * ((Wsp_f.vn + Wsp_f.a).abs()
        #     * ((Wsp_f.T2 * Wsp_f.T1 - Wsp_f.vn / Wsp_f.a) *  Wsp_f.d_rho
        #         - Wsp_f.tempE + Wsp_f.tempI))
        np.multiply(Wsp_f.T2, Wsp_f.T1, out=Wsp_f.LL4)
        np.divide(  Wsp_f.vn, Wsp_f.a,  out=Wsp_f.aux)
        Wsp_f.LL4 -= Wsp_f.aux
        Wsp_f.LL4 *= Wsp_f.d_rho
        Wsp_f.LL4 -= Wsp_f.tempE
        Wsp_f.LL4 += Wsp_f.tempI
        np.add(Wsp_f.vn, Wsp_f.a, out=Wsp_f.aux)
        np.absolute(Wsp_f.aux, Wsp_f.aux)
        Wsp_f.LL4 *= Wsp_f.aux
        Wsp_f.LL4 *= 0.5

        # Product with fifth row of L
        # Wsp_f.LL5 = 0.5 * ((Wsp_f.vn - Wsp_f.a).abs()
        #     * ((Wsp_f.T2 * Wsp_f.T1 + Wsp_f.vn / Wsp_f.a) *  Wsp_f.d_rho
        #         - Wsp_f.tempE - Wsp_f.tempI))
        np.multiply(Wsp_f.T2, Wsp_f.T1, out=Wsp_f.LL5)
        np.divide(  Wsp_f.vn, Wsp_f.a,  out=Wsp_f.aux)
        Wsp_f.LL5 += Wsp_f.aux
        Wsp_f.LL5 *= Wsp_f.d_rho
        Wsp_f.LL5 -= Wsp_f.tempE
        Wsp_f.LL5 -= Wsp_f.tempI
        np.subtract(Wsp_f.vn, Wsp_f.a, out=Wsp_f.aux)
        np.absolute(Wsp_f.aux, Wsp_f.aux)
        Wsp_f.LL5 *= Wsp_f.aux
        Wsp_f.LL5 *= 0.5
        
        # Product with first row of R
        # Wsp_f.RLLd1 = n[0]*Wsp_f.LL1 + n[1]*Wsp_f.LL2 + Wsp_f.LL4 + Wsp_f.LL5
        np.multiply(n[0,:], Wsp_f.LL1, out=Wsp_f.RLLd1)
        np.multiply(n[1,:], Wsp_f.LL2, out=Wsp_f.aux)
        Wsp_f.RLLd1 += Wsp_f.aux
        Wsp_f.RLLd1 += Wsp_f.LL4
        Wsp_f.RLLd1 += Wsp_f.LL5

        # Wsp_f.temp = Wsp_f.LL4 - Wsp_f.LL5
        np.subtract(Wsp_f.LL4, Wsp_f.LL5, out=Wsp_f.temp)
        
        # Product with second row of R
        # Wsp_f.RLLd2 = Wsp_f.u_star * Wsp_f.RLLd1 + Wsp_f.a * (
        #     n[0] * Wsp_f.temp + n[1] * Wsp_f.LL3 )
        np.multiply(n[0,:], Wsp_f.temp, out=Wsp_f.RLLd2)
        np.multiply(n[1,:], Wsp_f.LL3,  out=Wsp_f.aux)
        Wsp_f.RLLd2 += Wsp_f.aux
        Wsp_f.RLLd2 *= Wsp_f.a
        np.multiply(Wsp_f.u_star, Wsp_f.RLLd1, out=Wsp_f.aux)
        Wsp_f.RLLd2 += Wsp_f.aux
        
        # Product with third row of R
        # Wsp_f.RLLd3 = Wsp_f.v_star * Wsp_f.RLLd1 + Wsp_f.a * (
        #     n[1] * Wsp_f.temp - n[0] * Wsp_f.LL3 )
        np.multiply(n[1,:], Wsp_f.temp, out=Wsp_f.RLLd3)
        np.multiply(n[0,:], Wsp_f.LL3,  out=Wsp_f.aux)
        Wsp_f.RLLd3 -= Wsp_f.aux
        Wsp_f.RLLd3 *= Wsp_f.a
        np.multiply(Wsp_f.v_star, Wsp_f.RLLd1, out=Wsp_f.aux)
        Wsp_f.RLLd3 += Wsp_f.aux
        
        # Product with fourth row of R
        # Wsp_f.RLLd4 = 0.

        # Product with fifth row of R
        # Wsp_f.RLLd5 = Wsp_f.T1 * ( n[0] * Wsp_f.LL1 + n[1] * Wsp_f.LL2 )
        # + Wsp_f.H_star * (Wsp_f.LL4 + Wsp_f.LL5)
        # + Wsp_f.a * ( Wsp_f.vn * Wsp_f.temp +
        #               Wsp_f.u_star * ( n[1] * Wsp_f.LL3) +
        #               Wsp_f.v_star * (-n[0] * Wsp_f.LL3) )
        np.multiply(n[1,:], Wsp_f.u_star, out=Wsp_f.RLLd5)
        np.multiply(n[0,:], Wsp_f.v_star, out=Wsp_f.aux)
        Wsp_f.RLLd5 -= Wsp_f.aux
        Wsp_f.RLLd5 *= Wsp_f.LL3
        np.multiply(Wsp_f.vn, Wsp_f.temp, out=Wsp_f.aux)
        Wsp_f.RLLd5 += Wsp_f.aux
        Wsp_f.RLLd5 *= Wsp_f.a
        np.add(Wsp_f.LL4, Wsp_f.LL5, out=Wsp_f.aux)
        Wsp_f.aux   *= Wsp_f.H_star
        Wsp_f.RLLd5 += Wsp_f.aux
        np.multiply(n[0,:], Wsp_f.LL1, out=Wsp_f.aux)
        np.multiply(n[1,:], Wsp_f.LL2, out=Wsp_f.T4)
        Wsp_f.aux += Wsp_f.T4 # Wsp_f.aux += n[1,:] * Wsp_f.LL2
        Wsp_f.aux *= Wsp_f.T1
        Wsp_f.RLLd5 += Wsp_f.aux

        
        #
        # Equation 1
        # 
        ConvectiveFlux_1_X( Face_f.rhoE[ff], Face_f.uE[ff], Wsp_f.tempE )
        ConvectiveFlux_1_X( Face_f.rhoI[ff], Face_f.uI[ff], Wsp_f.tempI )
        
        # Wsp_f.F_star[0][0] = 0.5*(Wsp_f.tempE+Wsp_f.tempI-Wsp_f.RLLd1*n[0])
        np.add(Wsp_f.tempE, Wsp_f.tempI, Wsp_f.F_star[0][0])
        np.multiply(Wsp_f.RLLd1, n[0,:], Wsp_f.aux)
        Wsp_f.F_star[0][0] -= Wsp_f.aux
        Wsp_f.F_star[0][0] *= 0.5

        ConvectiveFlux_1_Y( Face_f.rhoE[ff], Face_f.vE[ff], Wsp_f.tempE )
        ConvectiveFlux_1_Y( Face_f.rhoI[ff], Face_f.vI[ff], Wsp_f.tempI )

        # Wsp_f.F_star[0][1] = 0.5*(Wsp_f.tempE+Wsp_f.tempI-Wsp_f.RLLd1*n[1])
        np.add(Wsp_f.tempE, Wsp_f.tempI, Wsp_f.F_star[0][1])
        np.multiply(Wsp_f.RLLd1, n[1,:], Wsp_f.aux)
        Wsp_f.F_star[0][1] -= Wsp_f.aux
        Wsp_f.F_star[0][1] *= 0.5

        #
        # Equation 2
        #
        ConvectiveFlux_2_X( Face_f.rhouE[ff], Face_f.uE[ff], Face_f.pE[ff],
                            Wsp_f.tempE )
        ConvectiveFlux_2_X( Face_f.rhouI[ff], Face_f.uI[ff], Face_f.pI[ff],
                            Wsp_f.tempI )

        # Wsp_f.F_star[1][0] = 0.5*(Wsp_f.tempE+Wsp_f.tempI-Wsp_f.RLLd2*n[0])
        np.add(Wsp_f.tempE, Wsp_f.tempI, Wsp_f.F_star[1][0])
        np.multiply(Wsp_f.RLLd2, n[0,:], Wsp_f.aux)
        Wsp_f.F_star[1][0] -= Wsp_f.aux
        Wsp_f.F_star[1][0] *= 0.5

        ConvectiveFlux_2_Y( Face_f.rhouE[ff], Face_f.vE[ff], Wsp_f.tempE )
        ConvectiveFlux_2_Y( Face_f.rhouI[ff], Face_f.vI[ff], Wsp_f.tempI )

        # Wsp_f.F_star[1][1] = 0.5*(Wsp_f.tempE+Wsp_f.tempI-Wsp_f.RLLd2*n[1])
        np.add(Wsp_f.tempE, Wsp_f.tempI, Wsp_f.F_star[1][1])
        np.multiply(Wsp_f.RLLd2, n[1,:], Wsp_f.aux)
        Wsp_f.F_star[1][1] -= Wsp_f.aux
        Wsp_f.F_star[1][1] *= 0.5

        #
        # Equation 3
        #
        ConvectiveFlux_3_X( Face_f.rhovE[ff], Face_f.uE[ff], Wsp_f.tempE )
        ConvectiveFlux_3_X( Face_f.rhovI[ff], Face_f.uI[ff], Wsp_f.tempI )

        # Wsp_f.F_star[2][0] = 0.5*(Wsp_f.tempE+Wsp_f.tempI-Wsp_f.RLLd3*n[0])
        np.add(Wsp_f.tempE, Wsp_f.tempI, Wsp_f.F_star[2][0])
        np.multiply(Wsp_f.RLLd3, n[0,:], Wsp_f.aux)
        Wsp_f.F_star[2][0] -= Wsp_f.aux
        Wsp_f.F_star[2][0] *= 0.5

        ConvectiveFlux_3_Y( Face_f.rhovE[ff], Face_f.vE[ff], Face_f.pE[ff],
                            Wsp_f.tempE )
        ConvectiveFlux_3_Y( Face_f.rhovI[ff], Face_f.vI[ff], Face_f.pI[ff],
                            Wsp_f.tempI )

        # Wsp_f.F_star[2][1] = 0.5*(Wsp_f.tempE+Wsp_f.tempI-Wsp_f.RLLd3*n[1])
        np.add(Wsp_f.tempE, Wsp_f.tempI, Wsp_f.F_star[2][1])
        np.multiply(Wsp_f.RLLd3, n[1,:], Wsp_f.aux)
        Wsp_f.F_star[2][1] -= Wsp_f.aux
        Wsp_f.F_star[2][1] *= 0.5

        #
        # Equation 4
        #
        np.add(Face_f.rhoeE[ff], Face_f.pE[ff], Wsp_f.T1)
        np.add(Face_f.rhoeI[ff], Face_f.pI[ff], Wsp_f.T2)

        ConvectiveFlux_4_X( Wsp_f.T1, Face_f.uE[ff], Wsp_f.tempE )
        ConvectiveFlux_4_X( Wsp_f.T2, Face_f.uI[ff], Wsp_f.tempI )

        # Wsp_f.F_star[3][0] = 0.5*(Wsp_f.tempE+Wsp_f.tempI-Wsp_f.RLLd5*n[0])
        np.add(Wsp_f.tempE, Wsp_f.tempI, Wsp_f.F_star[3][0])
        np.multiply(Wsp_f.RLLd5, n[0,:], Wsp_f.aux)
        Wsp_f.F_star[3][0] -= Wsp_f.aux
        Wsp_f.F_star[3][0] *= 0.5

        ConvectiveFlux_4_Y( Wsp_f.T1, Face_f.vE[ff], Wsp_f.tempE )
        ConvectiveFlux_4_Y( Wsp_f.T2, Face_f.vI[ff], Wsp_f.tempI )

        # Wsp_f.F_star[3][1] = 0.5*(Wsp_f.tempE+Wsp_f.tempI-Wsp_f.RLLd5*n[1])
        np.add(Wsp_f.tempE, Wsp_f.tempI, Wsp_f.F_star[3][1])
        np.multiply(Wsp_f.RLLd5, n[1,:], Wsp_f.aux)
        Wsp_f.F_star[3][1] -= Wsp_f.aux
        Wsp_f.F_star[3][1] *= 0.5

# end of manticore.models.compressible.equations.convfluxes.ConvectiveRoe


class ConvectiveHLLC:
    """HLLC numerical flux."""

    Workspace_f = ConvectiveHLLCWsp_f

    def __init__(self, model_desc, eqname):

        matname = model_desc.get_equation(eqname).mat
        self.fluid = model_desc.get_material(matname)
        self.speed_of_sound = self.fluid.sonic_speed

    def compute_conditional_masks(self, Wsp_f):
        """Computes indexing masks for the different conditions to
        compute HLLC flux output:

        for ii in range(nip):
            if Wsp_f.SI[ii] > 0.0: 
                f_star = Fe(QI): supersonic upwind
            elif Wsp_f.SE[ii] < 0.0:
                f_star = Fe(QE): supersonic downwid
            elif Wsp_f.SI[ii] <= 0.0 and Wsp_f.SM[ii] > 0.0:
                f_star = Fe(QI) + SI*n*(QIM - QI)
            elif Wsp_f.SM[ii] <= 0.0 and Wsp_f.SE[ii] >= 0.0:
                f_star = Fe(QE) + SE*n*(QEM - QE)
            else:
                raise AssertionError("No flux condition was met!")

        Args:
            Wsp_f (ConvectiveHLLCWsp_f)
        """
        
        Wsp_f.cond1.fill(False)
        Wsp_f.cond2.fill(False)
        Wsp_f.cond3.fill(False)
        Wsp_f.cond4.fill(False)
        Wsp_f.success.fill(False)

        SI_leq_0 = Wsp_f.SI <= 0.0
        SE_geq_0 = Wsp_f.SE >= 0.0 
        SM_gt_0  = Wsp_f.SM  > 0.0
        SM_leq_0 = np.invert(SM_gt_0)

        Wsp_f.cond1+= Wsp_f.SI > 0.0
        Wsp_f.cond2+= Wsp_f.SE < 0.0 
        Wsp_f.cond3[SI_leq_0]+= SM_gt_0[SI_leq_0] # Wsp_f.SI<=0. and Wsp_f.SM>0.
        Wsp_f.cond4[SE_geq_0]+= SM_leq_0[SE_geq_0]# Wsp_f.SE>=0. and Wsp_f.SM<=0

        # Forcing elif:
        Wsp_f.cond2[Wsp_f.cond1] = False
        Wsp_f.cond3[Wsp_f.cond1] = False
        Wsp_f.cond4[Wsp_f.cond1] = False
        #
        Wsp_f.cond3[Wsp_f.cond2] = False
        Wsp_f.cond4[Wsp_f.cond2] = False
        #
        Wsp_f.cond4[Wsp_f.cond3] = False

        # Success measure: a flux condition was found for each point
        Wsp_f.success += Wsp_f.cond1
        Wsp_f.success += Wsp_f.cond2
        Wsp_f.success += Wsp_f.cond3
        Wsp_f.success += Wsp_f.cond4

    def supersonicUpwind(self, Wsp_f, Face_f, ii, ff):
        aux = np.zeros(np.where(ii)[0].shape[0])                

        # -- FIRST EQUATION    
        ConvectiveFlux_1_X( Face_f.rhoI[ff][ii], Face_f.uI[ff][ii], aux )
        Wsp_f.F_star[0][0][ii] = aux        
        ConvectiveFlux_1_Y( Face_f.rhoI[ff][ii], Face_f.vI[ff][ii], aux )
        Wsp_f.F_star[0][1][ii] = aux

        # -- SECOND EQUATION
        ConvectiveFlux_2_X( Face_f.rhouI[ff][ii], Face_f.uI[ff][ii], 
                            Face_f.pI[ff][ii], aux )
        Wsp_f.F_star[1][0][ii] = aux
        ConvectiveFlux_2_Y( Face_f.rhouI[ff][ii], Face_f.vI[ff][ii], aux )
        Wsp_f.F_star[1][1][ii] = aux

        # -- THIRD EQUATION
        ConvectiveFlux_3_X( Face_f.rhovI[ff][ii], Face_f.uI[ff][ii], aux )
        Wsp_f.F_star[2][0][ii] = aux
        ConvectiveFlux_3_Y( Face_f.rhovI[ff][ii], Face_f.vI[ff][ii],
                            Face_f.pI[ff][ii], aux )
        Wsp_f.F_star[2][1][ii] = aux

        # -- FOURTH EQUATION
        rhoe_plus_p = Wsp_f.temp # just a mnemonic reference
        np.add(Face_f.rhoeI[ff], Face_f.pI[ff], rhoe_plus_p)
        ConvectiveFlux_4_X( rhoe_plus_p[ii], Face_f.uI[ff][ii], aux )
        Wsp_f.F_star[3][0][ii] = aux
        ConvectiveFlux_4_Y( rhoe_plus_p[ii], Face_f.vI[ff][ii], aux )
        Wsp_f.F_star[3][1][ii] = aux

    def supersonicDownwind(self, Wsp_f, Face_f, ii, ff):
        aux = np.zeros(np.where(ii)[0].shape[0])

        # -- FIRST EQUATION    
        ConvectiveFlux_1_X( Face_f.rhoE[ff][ii], Face_f.uE[ff][ii], aux )
        Wsp_f.F_star[0][0][ii] = aux        
        ConvectiveFlux_1_Y( Face_f.rhoE[ff][ii], Face_f.vE[ff][ii], aux )
        Wsp_f.F_star[0][1][ii] = aux

        # -- SECOND EQUATION
        ConvectiveFlux_2_X( Face_f.rhouE[ff][ii], Face_f.uE[ff][ii], 
                            Face_f.pE[ff][ii], aux )
        Wsp_f.F_star[1][0][ii] = aux
        ConvectiveFlux_2_Y( Face_f.rhouE[ff][ii], Face_f.vE[ff][ii], aux )
        Wsp_f.F_star[1][1][ii] = aux

        # -- THIRD EQUATION
        ConvectiveFlux_3_X( Face_f.rhovE[ff][ii], Face_f.uE[ff][ii], aux )
        Wsp_f.F_star[2][0][ii] = aux
        ConvectiveFlux_3_Y( Face_f.rhovE[ff][ii], Face_f.vE[ff][ii],
                            Face_f.pE[ff][ii], aux )
        Wsp_f.F_star[2][1][ii] = aux

        # -- FOURTH EQUATION
        rhoe_plus_p = Wsp_f.temp # just a mnemonic reference
        np.add(Face_f.rhoeE[ff], Face_f.pE[ff], rhoe_plus_p)
        ConvectiveFlux_4_X( rhoe_plus_p[ii], Face_f.uE[ff][ii], aux )
        Wsp_f.F_star[3][0][ii] = aux
        ConvectiveFlux_4_Y( rhoe_plus_p[ii], Face_f.vE[ff][ii], aux )
        Wsp_f.F_star[3][1][ii] = aux

    def fluxConditionA(self, Wsp_f, Face_f, ii, ff):
        aux  = np.zeros(np.where(ii)[0].shape[0])
        pDI  = np.zeros(np.where(ii)[0].shape[0])
        Temp = pDI
        
        # Normals to face ff at the integration points
        n = Face_f.n[ff]

        # Computes the average internal state

        # auxiliars
        np.subtract(Wsp_f.SI[ii], Wsp_f.SM[ii],  out=aux)
        Wsp_f.aux1[ii] = aux
        np.subtract(Wsp_f.SI[ii], Wsp_f.vnI[ii], out=aux)
        Wsp_f.aux2[ii] = aux

        # intermediate pressure
        np.subtract(Wsp_f.vnI[ii], Wsp_f.SM[ii], out=aux)
        Wsp_f.pM[ii]  = aux
        Wsp_f.pM[ii] *= Face_f.rhoI[ff][ii]
        Wsp_f.pM[ii] *= Wsp_f.aux2[ii]
        Wsp_f.pM[ii] *= -1.
        Wsp_f.pM[ii] += Face_f.pI[ff][ii]

        np.subtract(Wsp_f.pM[ii], Face_f.pI[ff][ii], out=pDI)

        # intermediate internal density
        np.multiply(Face_f.rhoI[ff][ii], Wsp_f.aux2[ii], out=aux)
        Wsp_f.rhoIM[ii]  = aux
        Wsp_f.rhoIM[ii] /= Wsp_f.aux1[ii]

        np.subtract(Wsp_f.rhoIM[ii], Face_f.rhoI[ff][ii], out=aux)
        Wsp_f.rhoDI[ii] = aux

        # intermediate internal momentums
        np.multiply(Face_f.rhoI[ff][ii], Face_f.uI[ff][ii], out=aux)
        Wsp_f.rhouIM[ii]  = aux
        Wsp_f.rhouIM[ii] *= Wsp_f.aux2[ii]
        np.multiply(pDI, n[0, ii], out=aux)
        Wsp_f.rhouIM[ii] += aux
        Wsp_f.rhouIM[ii] /= Wsp_f.aux1[ii]

        np.subtract(Wsp_f.rhouIM[ii], Face_f.rhouI[ff][ii], out=aux)
        Wsp_f.rhouDI[ii] = aux

        np.multiply(Face_f.rhoI[ff][ii], Face_f.vI[ff][ii], out=aux)
        Wsp_f.rhovIM[ii]  = aux
        Wsp_f.rhovIM[ii] *= Wsp_f.aux2[ii]
        np.multiply(pDI, n[1, ii], out=aux)
        Wsp_f.rhovIM[ii] += aux
        Wsp_f.rhovIM[ii] /= Wsp_f.aux1[ii]

        np.subtract(Wsp_f.rhovIM[ii], Face_f.rhovI[ff][ii], out=aux)
        Wsp_f.rhovDI[ii] = aux

        # intermediate internal energy
        np.multiply(Wsp_f.aux2[ii], Face_f.rhoeI[ff][ii], out=aux)        
        Wsp_f.rhoeIM[ii]  = aux
        np.multiply(Face_f.pI[ff][ii], Wsp_f.vnI[ii], out=aux)
        Wsp_f.rhoeIM[ii] -= aux
        np.multiply(Wsp_f.pM[ii], Wsp_f.SM[ii], out=aux)
        Wsp_f.rhoeIM[ii] += aux
        Wsp_f.rhoeIM[ii] /= Wsp_f.aux1[ii]

        np.subtract(Wsp_f.rhoeIM[ii], Face_f.rhoeI[ff][ii], out=aux)
        Wsp_f.rhoeDI[ii] = aux

        # Computes the flux
        
        self.supersonicUpwind( Wsp_f, Face_f, ii, ff )

        # -- FIRST EQUATION
        np.multiply(Wsp_f.SI[ii], Wsp_f.rhoDI[ii], out=Temp)
        np.multiply(Temp, n[0, ii], out=aux)
        Wsp_f.F_star[0][0][ii] += aux
        np.multiply(Temp, n[1, ii], out=aux)
        Wsp_f.F_star[0][1][ii] += aux

        # -- SECOND EQUATION
        np.multiply(Wsp_f.SI[ii], Wsp_f.rhouDI[ii], out=Temp)
        np.multiply(Temp, n[0, ii], out=aux)
        Wsp_f.F_star[1][0][ii] += aux
        np.multiply(Temp, n[1, ii], out=aux)
        Wsp_f.F_star[1][1][ii] += aux

        # -- THIRD EQUATION
        np.multiply(Wsp_f.SI[ii], Wsp_f.rhovDI[ii], out=Temp)
        np.multiply(Temp, n[0, ii], out=aux)
        Wsp_f.F_star[2][0][ii] += aux
        np.multiply(Temp, n[1, ii], out=aux)
        Wsp_f.F_star[2][1][ii] += aux

        # -- FOURTH EQUATION
        np.multiply(Wsp_f.SI[ii], Wsp_f.rhoeDI[ii], out=Temp)
        np.multiply(Temp, n[0, ii], out=aux)
        Wsp_f.F_star[3][0][ii] += aux
        np.multiply(Temp, n[1, ii], out=aux)
        Wsp_f.F_star[3][1][ii] += aux


    def fluxConditionB(self, Wsp_f, Face_f, ii, ff):
        aux  = np.zeros(np.where(ii)[0].shape[0])
        pDE  = np.zeros(np.where(ii)[0].shape[0])
        Temp = pDE
        
        # Normals to face ff at the integration points
        n = Face_f.n[ff]

        # Computes the average internal state

        # auxiliars
        np.subtract(Wsp_f.SE[ii], Wsp_f.SM[ii],  out=aux)
        Wsp_f.aux1[ii] = aux
        np.subtract(Wsp_f.SE[ii], Wsp_f.vnE[ii], out=aux)
        Wsp_f.aux2[ii] = aux

        # intermediate pressure
        np.subtract(Wsp_f.vnE[ii], Wsp_f.SM[ii], out=aux)
        Wsp_f.pM[ii]  = aux
        Wsp_f.pM[ii] *= Face_f.rhoE[ff][ii]
        Wsp_f.pM[ii] *= Wsp_f.aux2[ii]
        Wsp_f.pM[ii] *= -1.
        Wsp_f.pM[ii] += Face_f.pE[ff][ii]

        np.subtract(Wsp_f.pM[ii], Face_f.pE[ff][ii], out=pDE)

        # intermediate internal density
        np.multiply(Face_f.rhoE[ff][ii], Wsp_f.aux2[ii], out=aux)
        Wsp_f.rhoEM[ii]  = aux
        Wsp_f.rhoEM[ii] /= Wsp_f.aux1[ii]

        np.subtract(Wsp_f.rhoEM[ii], Face_f.rhoE[ff][ii], out=aux)
        Wsp_f.rhoDE[ii] = aux

        # intermediate internal momentums
        np.multiply(Face_f.rhoE[ff][ii], Face_f.uE[ff][ii], out=aux)
        Wsp_f.rhouEM[ii]  = aux
        Wsp_f.rhouEM[ii] *= Wsp_f.aux2[ii]
        np.multiply(pDE, n[0, ii], out=aux)
        Wsp_f.rhouEM[ii] += aux
        Wsp_f.rhouEM[ii] /= Wsp_f.aux1[ii]

        np.subtract(Wsp_f.rhouEM[ii], Face_f.rhouE[ff][ii], out=aux)
        Wsp_f.rhouDE[ii] = aux

        np.multiply(Face_f.rhoE[ff][ii], Face_f.vE[ff][ii], out=aux)
        Wsp_f.rhovEM[ii]  = aux
        Wsp_f.rhovEM[ii] *= Wsp_f.aux2[ii]
        np.multiply(pDE, n[1, ii], out=aux)
        Wsp_f.rhovEM[ii] += aux
        Wsp_f.rhovEM[ii] /= Wsp_f.aux1[ii]

        np.subtract(Wsp_f.rhovEM[ii], Face_f.rhovE[ff][ii], out=aux)
        Wsp_f.rhovDE[ii] = aux

        # intermediate internal energy
        np.multiply(Wsp_f.aux2[ii], Face_f.rhoeE[ff][ii], out=aux)        
        Wsp_f.rhoeEM[ii]  = aux
        np.multiply(Face_f.pE[ff][ii], Wsp_f.vnE[ii], out=aux)
        Wsp_f.rhoeEM[ii] -= aux
        np.multiply(Wsp_f.pM[ii], Wsp_f.SM[ii], out=aux)
        Wsp_f.rhoeEM[ii] += aux
        Wsp_f.rhoeEM[ii] /= Wsp_f.aux1[ii]

        np.subtract(Wsp_f.rhoeEM[ii], Face_f.rhoeE[ff][ii], out=aux)
        Wsp_f.rhoeDE[ii] = aux

        # Computes the flux
        
        self.supersonicDownwind( Wsp_f, Face_f, ii, ff )

        # -- FIRST EQUATION
        np.multiply(Wsp_f.SE[ii], Wsp_f.rhoDE[ii], out=Temp)
        np.multiply(Temp, n[0, ii], out=aux)
        Wsp_f.F_star[0][0][ii] += aux
        np.multiply(Temp, n[1, ii], out=aux)
        Wsp_f.F_star[0][1][ii] += aux

        # -- SECOND EQUATION
        np.multiply(Wsp_f.SE[ii], Wsp_f.rhouDE[ii], out=Temp)
        np.multiply(Temp, n[0, ii], out=aux)
        Wsp_f.F_star[1][0][ii] += aux
        np.multiply(Temp, n[1, ii], out=aux)
        Wsp_f.F_star[1][1][ii] += aux

        # -- THIRD EQUATION
        np.multiply(Wsp_f.SE[ii], Wsp_f.rhovDE[ii], out=Temp)
        np.multiply(Temp, n[0, ii], out=aux)
        Wsp_f.F_star[2][0][ii] += aux
        np.multiply(Temp, n[1, ii], out=aux)
        Wsp_f.F_star[2][1][ii] += aux

        # -- FOURTH EQUATION
        np.multiply(Wsp_f.SE[ii], Wsp_f.rhoeDE[ii], out=Temp)
        np.multiply(Temp, n[0, ii], out=aux)
        Wsp_f.F_star[3][0][ii] += aux
        np.multiply(Temp, n[1, ii], out=aux)
        Wsp_f.F_star[3][1][ii] += aux
        
    def eval(self, e, ff):

        # Number of integration points for computations on face ff
        # (max of the pair I/E)
        nip = e.face_comm_size(ff)

        # Workspace associated to this flux: room for all Roe flux
        # temporaries on *one* face.
        Wsp_f  = ConvectiveHLLCWsp_f.get_instance(nip)

        # Accessing face reconstruction workspace: we are assuming
        # that Face_f.reconstruct was called just before entering in
        # the present context and then the ff-th face's data is stored
        # in the face reconstruction workspace.
        Face_f = FaceReconstructWsp_f.get_instance(*(e.face_comm_sizes) )

        # Notation: E suffix stands for "external" (neighbour) and I
        # stands for "internal" (the current element)

        # Roe weighted averages:
        np.sqrt(Face_f.rhoI[ff], out=Wsp_f.tempI) # tempI = sqrt(rhoI[ff])
        np.sqrt(Face_f.rhoE[ff], out=Wsp_f.tempE) # tempE = sqrt(rhoE[ff])
        #     temp = tempI + tempE
        np.add(Wsp_f.tempI, Wsp_f.tempE, out=Wsp_f.temp) 

        # Roe average properties:
        #     rho_star = tempI*tempE
        np.multiply(Wsp_f.tempI, Wsp_f.tempE, out=Wsp_f.rho_star)
        #     u_star = (tempI*uI+tempE*uE)/temp
        np.multiply(Wsp_f.tempI, Face_f.uI[ff], out=Wsp_f.u_star)
        np.multiply(Wsp_f.tempE, Face_f.uE[ff], out=Wsp_f.aux)
        Wsp_f.u_star += Wsp_f.aux
        Wsp_f.u_star /= Wsp_f.temp    
        #     v_star = (tempI*vI+tempE*vE)/temp
        np.multiply(Wsp_f.tempI, Face_f.vI[ff], out=Wsp_f.v_star)
        np.multiply(Wsp_f.tempE, Face_f.vE[ff], out=Wsp_f.aux)
        Wsp_f.v_star += Wsp_f.aux
        Wsp_f.v_star /= Wsp_f.temp
        #     H_star = (tempI*HI+tempE*HE)/temp
        np.multiply(Wsp_f.tempI, Face_f.HI[ff], out=Wsp_f.H_star)
        np.multiply(Wsp_f.tempE, Face_f.HE[ff], out=Wsp_f.aux)
        Wsp_f.H_star += Wsp_f.aux
        Wsp_f.H_star /= Wsp_f.temp
        
        # Temporaries:
        #     T1 = 0.5*(u_star**2 + v_star**2)
        np.multiply(Wsp_f.u_star, Wsp_f.u_star, out=Wsp_f.T1)
        np.multiply(Wsp_f.v_star, Wsp_f.v_star, out=Wsp_f.aux)
        Wsp_f.T1 += Wsp_f.aux
        Wsp_f.T1 *= 0.5

        # Square of the local speed of sound (for a perfect gas):
        gamma = self.fluid.thermo.gamma
        #     a2 = ei + p/rho = h
        np.subtract(Wsp_f.H_star, Wsp_f.T1, out=Wsp_f.a2)
        #     a2 = (gamma - 1.0)*h = a**2
        Wsp_f.a2 *= (gamma - 1.0)     

        if Wsp_f.a2.min() < const.INF:
            msg = "Inconsistency when computing Roe's flux intermediate state!!"
            msg+= '\nSquare of LOCAL SPEED OF SOUND is negative or too small!'
            msg+= '\nElement : {}'.format(e.ID)
            msg+= ' Face : {}'.format(ff)
            msg+= '\nA^2 : \n{}'.format(Wsp_f.a2)
            raise AssertionError(msg)

        #     a = sqrt(a**2) 
        np.sqrt(Wsp_f.a2, out=Wsp_f.a)

        # Normals to face ff at the integration points
        n = Face_f.n[ff] 

        # Normal velocity of Roe averaged state:
        # vn = u_star * n[0,:] + v_star * n[1,:]
        np.multiply(Wsp_f.u_star, n[0,:], out=Wsp_f.vn)
        np.multiply(Wsp_f.v_star, n[1,:], out=Wsp_f.aux)
        Wsp_f.vn += Wsp_f.aux

        # Normal velocities I and E:
        np.multiply(Face_f.uI[ff], n[0,:], out=Wsp_f.vnI)
        np.multiply(Face_f.vI[ff], n[1,:], out=Wsp_f.aux)
        Wsp_f.vnI += Wsp_f.aux
        np.multiply(Face_f.uE[ff], n[0,:], out=Wsp_f.vnE)
        np.multiply(Face_f.vE[ff], n[1,:], out=Wsp_f.aux)
        Wsp_f.vnE += Wsp_f.aux

        # Sound speeds I and E: no checking of a**2 > 0.0 here as the
        # positivity preserving limiter garantees p,rho > 0
        self.speed_of_sound(Face_f.rhoI[ff], Face_f.pI[ff], Wsp_f.aI)
        self.speed_of_sound(Face_f.rhoE[ff], Face_f.pE[ff], Wsp_f.aE)

        # Computes the estimates of waves speeds SI and SE
        np.subtract(Wsp_f.vnI, Wsp_f.aI, Wsp_f.SI)
        np.subtract(Wsp_f.vn,  Wsp_f.a,  Wsp_f.aux)
        less_than = Wsp_f.SI > Wsp_f.aux
        Wsp_f.SI[less_than] = Wsp_f.aux[less_than]
        
        np.add(Wsp_f.vnE, Wsp_f.aE, Wsp_f.SE)
        np.add(Wsp_f.vn,  Wsp_f.a,  Wsp_f.aux)
        more_than = Wsp_f.SE < Wsp_f.aux
        Wsp_f.SE[more_than] = Wsp_f.aux[more_than]

        #----------------------------------------------------------------------
        #     BIG WARNING!
        #     We are *reusing* temp, tempI and tempE from this point as 
        #     they are no longer needed
        #----------------------------------------------------------------------

        # Computes the middle wave speed SM
        # Complete expression:
        #
        # Wsp_f.SM = 
        # ( Face_f.pI[ff]-Face_f.pE[ff]+
        #     ( Face_f.rhoE[ff]*Wsp_f.vnI )*( Wsp_f.SE-Wsp_f.vnE ) - 
        #     ( Face_f.rhoI[ff]*Wsp_f.vnE )*( Wsp_f.SI-Wsp_f.vnI )
        # )
        #     /
        # (
        #     Face_f.rhoE[ff]*( Wsp_f.SE-Wsp_f.vnE ) -
        #     Face_f.rhoI[ff]*( Wsp_f.SI-Wsp_f.vnI )
        # )
        #
        # Computes the middle wave speed SM
        # The same expression as above but executed in-memory:
        #
        # ( Face_f.rhoE[ff]*Wsp_f.vnI )*( Wsp_f.SE-Wsp_f.vnE )
        np.subtract(Wsp_f.SE, Wsp_f.vnE, Wsp_f.tempE)
        Wsp_f.tempE *= Face_f.rhoE[ff]
        np.multiply(Wsp_f.tempE, Wsp_f.vnE, out=Wsp_f.SM) # <- check this: vnI?
        # ( Face_f.rhoI[ff]*Wsp_f.vnE )*( Wsp_f.SI-Wsp_f.vnI )
        np.subtract(Wsp_f.SI, Wsp_f.vnI, Wsp_f.tempI)
        Wsp_f.tempI *= Face_f.rhoI[ff]
        np.multiply(Wsp_f.tempI, Wsp_f.vnI, out=Wsp_f.aux) # <- check this: vnE?
        #
        Wsp_f.SM -= Wsp_f.aux
        Wsp_f.SM += Face_f.pI[ff]
        Wsp_f.SM -= Face_f.pE[ff]
        #
        np.subtract(Wsp_f.tempE, Wsp_f.tempI, out=Wsp_f.aux)
        Wsp_f.SM /= Wsp_f.aux
        
        #
        # Computes the HLLC flux
        #

        # Masks for different conditions to compute HLLC flux
        self.compute_conditional_masks(Wsp_f) # cond1, cond2, cond3, cond4

        # Checks if a flux condition was met for all points
        if not Wsp_f.success.all():
            msg = 'A flux condition was not met at all points.'
            msg+= '\nElement : {}'.format(e.ID)
            msg+= ' Face : {}'.format(ff)
            msg+= 'SI: {}'.format(Wsp_f.SI)
            msg+= 'SE: {}'.format(Wsp_f.SE)
            msg+= 'SM: {}'.format(Wsp_f.SM)
            raise AssertionError(msg)

        # if Wsp_f.SI[ii] > 0.0: f_star = Fe(QI), supersonic upwind
        #
        if Wsp_f.cond1.any():
            self.supersonicUpwind( Wsp_f, Face_f, Wsp_f.cond1, ff )

        # elif Wsp_f.SE[ii] < 0.0: f_star = Fe(QE): supersonic downwind
        #
        if Wsp_f.cond2.any():
            self.supersonicDownwind( Wsp_f, Face_f, Wsp_f.cond2, ff )

        # elif Wsp_f.SI[ii] <= 0.0 and Wsp_f.SM[ii] > 0.0:
        #     f_star = Fe(QI) + SI*n*(QIM - QI)
        if Wsp_f.cond3.any():
            self.fluxConditionA( Wsp_f, Face_f, Wsp_f.cond3, ff )

        # elif Wsp_f.SM[ii] <= 0.0 and Wsp_f.SE[ii] >= 0.0:
        #     f_star = Fe(QE) + SE*n*(QEM - QE)
        if Wsp_f.cond4.any():
            self.fluxConditionB( Wsp_f, Face_f, Wsp_f.cond4, ff )

# end of manticore.models.compressible.equations.convfluxes.ConvectiveHLLC

        
