#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Convective residual
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import StdRegionType, FieldType
from manticore.lops.modal_dg.engine import EntityOperator
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.class_instances import (
    class_quad_physinnerproductgrad2d, class_tria_physinnerproductgrad2d,
    class_quad_physinnerproduct1d, class_tria_physinnerproduct1d,
    class_wadg_inverse_mas_op, class_rwadg_inverse_mas_op )
from manticore.models.compressible.services.predicatemixins import (
    model_description_mixin, equation_domain_signature, expansion_numbers)
from manticore.models.compressible.equations.workspaces import(
    PrimitiveWsp_v, FaceReconstructWsp_f, ResidualWsp_p )
from manticore.models.compressible.equations.convfluxes import (
    ConvectiveFlux_1_X, ConvectiveFlux_1_Y,
    ConvectiveFlux_2_X, ConvectiveFlux_2_Y,
    ConvectiveFlux_3_X, ConvectiveFlux_3_Y,
    ConvectiveFlux_4_X, ConvectiveFlux_4_Y )

logger = manticore.logging.getLogger('MDL_CMP_EQ')

#
#
from manticore.models.compressible.directives import ExecDirective
#
# Execution accordingly to command line directives
exec_dir = manticore.models.compressible.directive

if exec_dir==ExecDirective.NS_VALIDATION:
    from manticore.models.compressible.tests.test_helpers import (
        ns_validation )
    from manticore.lops.modal_dg.class_instances import (
        class_quad_physevaluator2d, class_tria_physevaluator2d,
        class_quad_physinnerproduct2d, class_tria_physinnerproduct2d)
#

def factory_weak_dg_convres(conv_flux_t):
    """Factory for the residual of the weak DG form of the Euler equation.
    
    Args:
        conv_flux_t: A template parameter on the kind of convective
            flux we are using (Lax-Friedrich, Roe, HLLC, etc).

    """

    Workspace_f = conv_flux_t.Workspace_f
    InverseMassOperator = [class_wadg_inverse_mas_op,class_rwadg_inverse_mas_op]

    class weak_dg_convective_residual(
            EntityOperator,
            model_description_mixin,
            equation_domain_signature,
            expansion_numbers):

        def __init__(self, model_desc):
            model_description_mixin.__init__(self, model_desc)
            equation_domain_signature.__init__(self)
            expansion_numbers.__init__(self)
            EntityOperator.__init__(self)

            self.innerpg  = [None, None] # InnerProductGradient[quad, tria]
            self.innerp_t = [None, None] # InnerProduct1d[quad, tria]
            self.Fx       = np.zeros(0)
            self.Fy       = np.zeros(0)
            self.inner_temp = [np.zeros(0), np.zeros(0)]

            self.temp = np.zeros(0)
            self.aux  = np.zeros(0)

            if exec_dir==ExecDirective.NS_VALIDATION:
                self.innerpval = [None, None] # InnerProduct2d[quad, tria]
                self.evalSrc   = [None, None]

        def setup(self, eqname, domain):
            equation_domain_signature.setup(self, eqname, domain)
            matname = self.desc.get_equation(eqname).mat
            self.fluid = self.desc.get_material(matname)

            if exec_dir==ExecDirective.NS_VALIDATION:
                g       = self.fluid.thermo.gamma
                cp      = self.fluid.thermo.Cp
                cv      = self.fluid.thermo.Cv
                mu      = self.fluid.transport.mu
                Pr      = self.fluid.transport.Pr
                k       = mu*cp/Pr

                # Get analytical expression
                self.fval = ns_validation(g, mu, k, cv)

        def container_operator(self, group, key, container):
            k = ExpansionKeyFactory.make(key.order, key.nip)

            regions =  [StdRegionType.DG_Quadrangle, StdRegionType.DG_Triangle]
            for r in regions:
                expansion_numbers.eval(self, r, k) # set nS and nQ=k.n2d
                self.inner_temp[r].resize( (self.nS,), refcheck=False)

            self.innerpg[regions[0]] = class_quad_physinnerproductgrad2d(k)
            self.innerpg[regions[1]] = class_tria_physinnerproductgrad2d(k)

            self.innerp_t[regions[0]] = class_quad_physinnerproduct1d
            self.innerp_t[regions[1]] = class_tria_physinnerproduct1d

            self.Fx.resize( (self.nQ,), refcheck=False)
            self.Fy.resize( (self.nQ,), refcheck=False)

            self.temp.resize( (key.nip+1,), refcheck=False)
            self.aux.resize( (key.nip+1,), refcheck=False)

            if exec_dir==ExecDirective.NS_VALIDATION:
                self.innerpval[regions[0]] = class_quad_physinnerproduct2d(k)
                self.innerpval[regions[1]] = class_tria_physinnerproduct2d(k)
                
                self.evalSrc[regions[0]] = class_quad_physevaluator2d(k)
                self.evalSrc[regions[1]] = class_tria_physevaluator2d(k)

        def __call__(self, e):
            """Evaluates the convective residual of a single element.

            The usual __call__() is provided to be used as a standalone
            residual evaluator (it will multiply the residuals by the
            inverse of the mass matrix). If you need to use it as a part of
            a bigger residual evaluation (Navier-Stokes, for instance), you
            should resort to eval().
            """

            #logger.debug('Evaluating residual on element %4d' % (e.ID))
            
            # Check setup was done
            assert self.domain

            # Set nS and nQ for this element
            expansion_numbers.eval(self, e.type, e.key)

            self.eval(e)

            Residual_p = ResidualWsp_p.get_instance(self.nS)

            # M^{-1} product
            IM = e.get_inv_mass()
            tr = FieldType.tr
            
            InverseMassOperator[e.subtype].solve(
                IM, Residual_p.rho,e.residual(tr,FieldVariable.RHO) )
            InverseMassOperator[e.subtype].solve(
                IM, Residual_p.rhou, e.residual(tr,FieldVariable.RHOU) )
            InverseMassOperator[e.subtype].solve(
                IM, Residual_p.rhov, e.residual(tr,FieldVariable.RHOV) )
            InverseMassOperator[e.subtype].solve(
                IM, Residual_p.rhoe, e.residual(tr,FieldVariable.RHOE) )
            
        def eval(self, e):
            """Evaluates the residual without the inverse mass matrix.

            Note: 

                * This residual evaluation 'kernel' is useful for
                    combining with more complex residuals (e.g. artificial
                    dissipation, viscous flow, etc.)
            """
            # Filling the primitive variables workspace
            Primitive_v = PrimitiveWsp_v.get_instance(self.nQ)
            Primitive_v.fill(e, self.fluid)

            # Zeroing residuals
            Residual_p = ResidualWsp_p.get_instance(self.nS)
            Residual_p.zero()

            # Volumetric operations
            self.volumeContribution(e)

            # Facial operations
            self.faceContribution(e)


        # def residualRHO(self, e):
        #     """FIRST EQUATION: rho * [u, v]"""

        #     Primitive_v = PrimitiveWsp_v.get_instance(self.nQ)
            
        #     rho = e.state(FieldType.ph, FieldVariable.RHO)
            
        #     ConvectiveFlux_1_X(rho, Primitive_v.u, self.Fx )
        #     ConvectiveFlux_1_Y(rho, Primitive_v.v, self.Fy )

        #     Residual_p = ResidualWsp_p.get_instance(self.nS)

        #     # S operator, physical -> modal
        #     self.innerpg[e.type](e.get_jacobian(),e.get_inv_jacobian_matrix(),
        #                             self.Fx, self.Fy, Residual_p.rho )
            
        # def residualRHOU(self, e):
        #     """SECOND EQUATION: [rhou*u+p, rhou*v]"""

        #     Primitive_v = PrimitiveWsp_v.get_instance(self.nQ)

        #     rhou = e.state(FieldType.ph, FieldVariable.RHOU)
            
        #     ConvectiveFlux_2_X(rhou, Primitive_v.u, Primitive_v.p, self.Fx )
        #     ConvectiveFlux_2_Y(rhou, Primitive_v.v,                self.Fy )

        #     Residual_p = ResidualWsp_p.get_instance(self.nS)

        #     # S operator, physical -> modal
        #     self.innerpg[e.type](e.get_jacobian(),e.get_inv_jacobian_matrix(),
        #                             self.Fx, self.Fy, Residual_p.rhou )

        # def residualRHOV(self, e):
        #     """THIRD EQUATION: [rhov*u, rhov*v+p]"""

        #     Primitive_v = PrimitiveWsp_v.get_instance(self.nQ)

        #     rhov = e.state(FieldType.ph, FieldVariable.RHOV)
            
        #     ConvectiveFlux_3_X(rhov, Primitive_v.u,                self.Fx )
        #     ConvectiveFlux_3_Y(rhov, Primitive_v.v, Primitive_v.p, self.Fy )

        #     Residual_p = ResidualWsp_p.get_instance(self.nS)

        #     # S operator, physical -> modal
        #     self.innerpg[e.type](e.get_jacobian(),e.get_inv_jacobian_matrix(),
        #                             self.Fx, self.Fy, Residual_p.rhov )

        # def residualRHOE(self, e):
        #     """FOURTH EQUATION: (e+p)*[u, v]"""

        #     Primitive_v = PrimitiveWsp_v.get_instance(self.nQ)

        #     aux = e.state(FieldType.ph, FieldVariable.RHOE) + Primitive_v.p

        #     ConvectiveFlux_4_X( aux, Primitive_v.u, self.Fx )
        #     ConvectiveFlux_4_Y( aux, Primitive_v.v, self.Fy )

        #     Residual_p = ResidualWsp_p.get_instance(self.nS)

        #     # S operator, physical -> modal
        #     self.innerpg[e.type](e.get_jacobian(),e.get_inv_jacobian_matrix(),
        #                             self.Fx, self.Fy, Residual_p.rhoe )

        def volumeContribution(self, e):
            
            Primitive_v = PrimitiveWsp_v.get_instance(self.nQ)
            Residual_p  = ResidualWsp_p.get_instance(self.nS)
            J  = e.get_jacobian()
            IM = e.get_inv_jacobian_matrix()

            #
            # FIRST EQUATION: rho * [u, v]
            #
            rho = e.state(FieldType.ph, FieldVariable.RHO)
            ConvectiveFlux_1_X(rho, Primitive_v.u, self.Fx )
            ConvectiveFlux_1_Y(rho, Primitive_v.v, self.Fy )
            # S operator, physical -> modal
            self.innerpg[e.type](J, IM, self.Fx, self.Fy, Residual_p.rho )

            #
            # SECOND EQUATION: [rhou*u+p, rhou*v]
            #
            rhou = e.state(FieldType.ph, FieldVariable.RHOU)
            ConvectiveFlux_2_X(rhou, Primitive_v.u, Primitive_v.p, self.Fx )
            ConvectiveFlux_2_Y(rhou, Primitive_v.v,                self.Fy )
            # S operator, physical -> modal
            self.innerpg[e.type](J, IM, self.Fx, self.Fy, Residual_p.rhou )

            #
            # THIRD EQUATION: [rhov*u, rhov*v+p]
            #
            rhov = e.state(FieldType.ph, FieldVariable.RHOV)
            ConvectiveFlux_3_X(rhov, Primitive_v.u,                self.Fx )
            ConvectiveFlux_3_Y(rhov, Primitive_v.v, Primitive_v.p, self.Fy )
            # S operator, physical -> modal
            self.innerpg[e.type](J, IM, self.Fx, self.Fy, Residual_p.rhov )

            #
            # FOURTH EQUATION: (e+p)*[u, v]
            #
            aux = e.state(FieldType.ph, FieldVariable.RHOE) + Primitive_v.p
            ConvectiveFlux_4_X( aux, Primitive_v.u, self.Fx )
            ConvectiveFlux_4_Y( aux, Primitive_v.v, self.Fy )
            # S operator, physical -> modal
            self.innerpg[e.type](J, IM, self.Fx, self.Fy, Residual_p.rhoe )

            if exec_dir==ExecDirective.NS_VALIDATION:
                C    = e.get_coeff()
                geo  = e.geom_type
                vmap = e.vmap
                f    = self.fval
                inner_temp = self.inner_temp[e.type]
                evalSrc    = self.evalSrc[e.type]
                innerpval  = self.innerpval[e.type]
                
                src = evalSrc(f.src_rho, C, geo, vmap)
                innerpval(src, J, inner_temp)
                Residual_p.rho += inner_temp

                src = evalSrc(f.src_rhou, C, geo, vmap)
                innerpval(src, J, inner_temp)
                Residual_p.rhou += inner_temp

                src = evalSrc(f.src_rhov, C, geo, vmap)
                innerpval(src, J, inner_temp)
                Residual_p.rhov += inner_temp

                src = evalSrc(f.src_rhoe, C, geo, vmap)
                innerpval(src, J, inner_temp)
                Residual_p.rhoe += inner_temp

            
        def faceContribution(self, e):

            # Convective flux
            Flux = conv_flux_t(self.desc, self.equation)

            # Access the residual workspace
            Residual_p = ResidualWsp_p.get_instance(self.nS)

            num_faces  = len(e.neigh)

            # Access the workspace of face reconstructed data
            Face_f = FaceReconstructWsp_f.get_instance( *(e.face_comm_sizes) )

            # 
            Temp = self.temp
            Aux  = self.aux
            inner_temp = self.inner_temp[e.type]

            for ff in range(num_faces):                
                ng  = e.get_neighbour(ff)  # Get the ff-th neighbour
                nip = e.face_comm_size(ff) # Communication size on face ff

                #
                # Be careful when using ExpansionKey to get integration
                # points on faces: the second argument of the constructor
                # is the one-dimensional number of integration points for
                # numerical integration INSIDE the element. The number of
                # integration points generated on faces by ExpansionKey is
                # the second argument of the constructor PLUS ONE. Thus,
                # if you know the number of integration points on face (as
                # here) you must SUBTRACT ONE from it to get the correct
                # integration points on faces (see the assertion below).
                #
                k = ExpansionKeyFactory.make(e.key.order, nip-1)

                # In a mesh with non-constant p-order, the number of
                # integration points on each face can be different
                # from the others and then the inner product operator
                # my be set up when reaching each face.
                innerp = self.innerp_t[e.type](k)

                # Physical reconstruction on face ff
                Face_f.reconstruct(ff, e, ng, self.fluid)

                # Evaluating the numerical flux f*(.)
                Flux.eval(e, ff)

                # Access 
                Wsp_f = conv_flux_t.Workspace_f.get_instance(nip)

                # Normals to face ff at the integration points
                n = Face_f.n[ff]

                # Correct allocation
                Temp.resize( (nip,), refcheck=False)
                Aux.resize( (nip,), refcheck=False)
                
                # RHO
                np.multiply(n[0,:], Wsp_f.F_star[0][0], Temp)
                np.multiply(n[1,:], Wsp_f.F_star[0][1], Aux)
                Temp += Aux

                # logger.debug("e[%s].face[%s].rhoI = \n%s" %
                #                  (e.ID, ff, Face_f.rhoI[ff]))
                # logger.debug("e[%s].face[%s].rhoE = \n%s" %
                #                  (e.ID, ff, Face_f.rhoE[ff]))

                # logger.debug("e[%s].face[%s].flux.RHO = \n%s" %
                #                  (e.ID, ff, Temp))
                
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Residual_p.rho -= inner_temp

                # RHOU
                np.multiply(n[0,:], Wsp_f.F_star[1][0], Temp)
                np.multiply(n[1,:], Wsp_f.F_star[1][1], Aux)
                Temp += Aux
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Residual_p.rhou -= inner_temp
                
                # RHOV
                np.multiply(n[0,:], Wsp_f.F_star[2][0], Temp)
                np.multiply(n[1,:], Wsp_f.F_star[2][1], Aux)
                Temp += Aux
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Residual_p.rhov -= inner_temp

                # RHOE
                np.multiply(n[0,:], Wsp_f.F_star[3][0], Temp)
                np.multiply(n[1,:], Wsp_f.F_star[3][1], Aux)
                Temp += Aux
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Residual_p.rhoe -= inner_temp
                
    # end of factory_weak_dg_convres.weak_dg_convective_residual
    
    return weak_dg_convective_residual

# end of
# manticore.models.compressible.equations.convres.factory_weak_dg_convres
