#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Viscous residual
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import StdRegionType, FieldType
from manticore.lops.modal_dg.engine import EntityOperator
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.class_instances import (
    class_quad_physinnerproductgrad2d, class_tria_physinnerproductgrad2d,
    class_quad_physinnerproduct1d, class_tria_physinnerproduct1d,
    class_quad_physbackward2d, class_tria_physbackward2d,
    class_wadg_inverse_mas_op, class_rwadg_inverse_mas_op )
from manticore.models.compressible.services.predicatemixins import (
    model_description_mixin, equation_domain_signature, expansion_numbers)
from manticore.models.compressible.equations.workspaces import (
    PrimitiveWsp_v, FaceReconstructWsp_f, ResidualWsp_p, TauWsp_v )
from manticore.models.compressible.equations.convres import (
    factory_weak_dg_convres )
from manticore.models.compressible.equations.viscfluxes import (
    Tau11, Tau22, Tau12, ViscousFlux_4_X, ViscousFlux_4_Y)


logger = manticore.logging.getLogger('MDL_CMP_EQ')

def factory_weak_auxiliar_eq(visc_flux_t):
    """Factory for the auxiliar problem responsible by computibg spatial
    derivatives of state variables for the BR1 viscous flux.
    
    Args:
        visc_flux_t: A template parameter on the kind of viscous
            flux we are using (BR1, BR2, etc).
    """

    Workspace_f = visc_flux_t.Workspace_f
    Workspace_v = visc_flux_t.Workspace_v
    Workspace_p = visc_flux_t.Workspace_p
    InverseMassOperator = [class_wadg_inverse_mas_op,class_rwadg_inverse_mas_op]

    class weak_auxiliar_eq(
            EntityOperator,
            model_description_mixin,
            equation_domain_signature,
            expansion_numbers):

        def __init__(self, model_desc):
            model_description_mixin.__init__(self, model_desc)
            equation_domain_signature.__init__(self)
            expansion_numbers.__init__(self)
            EntityOperator.__init__(self)

            self.innerpg  = [None, None] # InnerProductGradient[quad, tria]
            self.innerp_t = [None, None] # InnerProduct[quad, tria]
            self.bw       = [None, None] # Backward[quad, tria]
            
            self.inner_temp = [np.zeros(0), np.zeros(0)]
            self.temp = np.zeros(0)

        def setup(self, eqname, domain):
            equation_domain_signature.setup(self, eqname, domain)
            matname = self.desc.get_equation(eqname).mat
            self.fluid = self.desc.get_material(matname)

        def container_operator(self, group, key, container):
            k = ExpansionKeyFactory.make(key.order, key.nip)

            regions =  [StdRegionType.DG_Quadrangle, StdRegionType.DG_Triangle]
            for r in regions:
                expansion_numbers.eval(self, r, k) # set nS and nQ=k.n2d
                self.inner_temp[r].resize( (self.nS,), refcheck=False)

            self.innerpg[regions[0]] = class_quad_physinnerproductgrad2d(k)
            self.innerpg[regions[1]] = class_tria_physinnerproductgrad2d(k)

            self.innerp_t[regions[0]] = class_quad_physinnerproduct1d
            self.innerp_t[regions[1]] = class_tria_physinnerproduct1d

            self.bw[regions[0]] = class_quad_physbackward2d(k)
            self.bw[regions[1]] = class_tria_physbackward2d(k)

            self.temp.resize( (key.nip+1,), refcheck=False)

        def __call__(self, e):

            # Check setup was done
            assert self.domain

            # Set nS and nQ for this element
            expansion_numbers.eval(self, e.type, e.key)

            #
            # STEP 0: Preparing workspaces
            #
            
            # Filling the primitive variables workspace
            Primitive_v = PrimitiveWsp_v.get_instance(self.nQ)
            Primitive_v.fill(e, self.fluid)

            # Acessing temporary storage for gradient computation
            Grad_p = visc_flux_t.Workspace_p.get_instance(self.nS)
            Grad_p.zero()

            # Acessing temporary storage for gradient computation
            Aux_v = visc_flux_t.Workspace_v.get_instance(self.nQ)

            # Internal energy
            np.power(Primitive_v.u, 2., out=Aux_v.eInner)
            np.power(Primitive_v.v, 2., out=Aux_v.temp)
            Aux_v.eInner += Aux_v.temp
            Aux_v.eInner *= -0.5
            np.divide(
                e.state(FieldType.ph, FieldVariable.RHOE),
                e.state(FieldType.ph, FieldVariable.RHO), out=Aux_v.temp)
            Aux_v.eInner += Aux_v.temp

            #
            # STEP 1: Evaluation of -( U, Grad phi )_e
            #

            # uxc --> result of the inner product of the u component of
            # velocity against the x derivative of basis functions. All other
            # variables follow the same convention, including internal energy.

            J  = e.get_jacobian()
            IM = e.get_inv_jacobian_matrix()

            np.multiply(Primitive_v.u, -1., Aux_v.temp)
            self.innerpg[e.type].eval(J, IM, Aux_v.temp, Grad_p.uxc, Grad_p.uyc)
            np.multiply(Primitive_v.v, -1., Aux_v.temp)
            self.innerpg[e.type].eval(J, IM, Aux_v.temp, Grad_p.vxc, Grad_p.vyc)
            np.multiply(Aux_v.eInner, -1., Aux_v.temp)
            self.innerpg[e.type].eval(J, IM, Aux_v.temp, Grad_p.exc, Grad_p.eyc)

            #
            # STEP 2: Face iteration
            # 
            
            Flux = visc_flux_t(self.desc, self.equation)

            num_faces  = len(e.neigh)

            # Access the workspace of face reconstructed data
            Face_f = FaceReconstructWsp_f.get_instance( *(e.face_comm_sizes) )

            # Guess a probable allocation for temporary arrays
            Temp = self.temp
            inner_temp = self.inner_temp[e.type]

            for ff in range(num_faces):                
                ng  = e.get_neighbour(ff)  # Get the ff-th neighbour
                nip = e.face_comm_size(ff) # Communication size on face ff

                #
                # Be careful when using ExpansionKey to get integration
                # points on faces: the second argument of the constructor
                # is the one-dimensional number of integration points for
                # numerical integration INSIDE the element. The number of
                # integration points generated on faces by ExpansionKey is
                # the second argument of the constructor PLUS ONE. Thus,
                # if you know the number of integration points on face (as
                # here) you must SUBTRACT ONE from it to get the correct
                # integration points on faces (see the assertion below).
                #
                k = ExpansionKeyFactory.make(e.key.order, nip-1)

                # In a mesh with non-constant p-order, the number of
                # integration points on each face can be different
                # from the others and then the inner product operator
                # my be set up when reaching each face.
                innerp = self.innerp_t[e.type](k)

                # Physical reconstruction on face ff
                Face_f.reconstruct(ff, e, ng, self.fluid)

                # Evaluating the numerical flux f*(.)
                Flux.tilda(e, ff)

                # Access 
                Wsp_f = visc_flux_t.Workspace_f.get_instance(nip)

                # Normals to face ff at the integration points
                n = Face_f.n[ff]

                # Correct allocation
                Temp.resize( (nip,), refcheck=False)

                np.multiply(n[0,:], Wsp_f.uTilda, Temp)
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Grad_p.uxc += inner_temp

                np.multiply(n[1,:], Wsp_f.uTilda, Temp)
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Grad_p.uyc += inner_temp

                np.multiply(n[0,:], Wsp_f.vTilda, Temp)
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Grad_p.vxc += inner_temp

                np.multiply(n[1,:], Wsp_f.vTilda, Temp)
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Grad_p.vyc += inner_temp

                np.multiply(n[0,:], Wsp_f.eTilda, Temp)
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Grad_p.exc += inner_temp

                np.multiply(n[1,:], Wsp_f.eTilda, Temp)
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                Grad_p.eyc += inner_temp

            # M^{-1} product
            IM = e.get_inv_mass()
            tr = FieldType.tr

            dudx = e.state(tr,FieldVariable.DUDX)
            dudy = e.state(tr,FieldVariable.DUDY)
            dvdx = e.state(tr,FieldVariable.DVDX)
            dvdy = e.state(tr,FieldVariable.DVDY)
            dedx = e.state(tr,FieldVariable.DEIDX)
            dedy = e.state(tr,FieldVariable.DEIDY)
            
            InverseMassOperator[e.subtype].solve(IM, Grad_p.uxc, dudx)
            InverseMassOperator[e.subtype].solve(IM, Grad_p.uyc, dudy)
            InverseMassOperator[e.subtype].solve(IM, Grad_p.vxc, dvdx)
            InverseMassOperator[e.subtype].solve(IM, Grad_p.vyc, dvdy)
            InverseMassOperator[e.subtype].solve(IM, Grad_p.exc, dedx)
            InverseMassOperator[e.subtype].solve(IM, Grad_p.eyc, dedy)

            # Computing physical values
            bw = self.bw[e.type]
            ph = FieldType.ph

            bw(dudx, e.state(ph, FieldVariable.DUDX))
            bw(dudy, e.state(ph, FieldVariable.DUDY))
            bw(dvdx, e.state(ph, FieldVariable.DVDX))
            bw(dvdy, e.state(ph, FieldVariable.DVDY))
            bw(dedx, e.state(ph, FieldVariable.DEIDX))
            bw(dedy, e.state(ph, FieldVariable.DEIDY))

    # end of factory_weak_auxiliar_eq.weak_auxiliar_eq
    
    return weak_auxiliar_eq

# end of
# manticore.models.compressible.equations.viscres.factory_weak_auxiliar_eq


def factory_weak_dg_viscous_res(conv_flux_t, visc_flux_t):

    """Factory for the residual of the weak DG form of the Navier Stokes
    equation for laminar flow.
    
    Args:
        conv_flux_t: A template parameter on the kind of convective
            flux we are using (Lax-Friedrich, Roe, HLLC, etc).

        visc_flux_t: A template parameter on the kind of viscous
            flux we are using
    """

    ViscFluxWorkspace_f = visc_flux_t.Workspace_f
    ViscFluxWorkspace_v = visc_flux_t.Workspace_v
    ViscFluxWorkspace_p = visc_flux_t.Workspace_p
    
    weak_dg_convective_res = factory_weak_dg_convres(conv_flux_t)

    class weak_dg_viscous_res(weak_dg_convective_res):

        def __init__(self, model_desc):
            weak_dg_convective_res.__init__(self, model_desc)

            self.Re = model_desc.get_reference().Re # Reynolds

        def setup(self, eqname, domain):
            weak_dg_convective_res.setup(self, eqname, domain)

            self.gamma = self.fluid.thermo.gamma
            self.Pr    = self.fluid.transport.Pr # Prandtl

        def eval(self, e):
            """Evaluates the residual without the inverse mass matrix.

            """
            super().eval(e)

            Tau_v = TauWsp_v.get_instance(self.nQ)
            ph = FieldType.ph

            mu   = e.state(ph, FieldVariable.MU)
            dudx = e.state(ph, FieldVariable.DUDX)
            dudy = e.state(ph, FieldVariable.DUDY)
            dvdx = e.state(ph, FieldVariable.DVDX)
            dvdy = e.state(ph, FieldVariable.DVDY)
                        
            Tau11( mu, dudx, dvdy, Tau_v.t11 )        
            Tau12( mu, dudy, dvdx, Tau_v.t12 )
            Tau22( mu, dudx, dvdy, Tau_v.t22 )

            # Volumetric operations
            self.visc_volumeContribution(e)

            # Facial operations
            self.visc_faceContribution(e)

        def visc_volumeContribution(self, e):
            
            Tau_v      = TauWsp_v.get_instance(self.nQ)
            p_wsp      = PrimitiveWsp_v.get_instance(self.nQ)
            Residual_p = ResidualWsp_p.get_instance(self.nS)
            J  = e.get_jacobian()
            IM = e.get_inv_jacobian_matrix()
            
            inner_temp = self.inner_temp[e.type]
            innerpg    = self.innerpg[e.type]
            Reynolds   = self.Re

            #
            # FIRST EQUATION: Zero.
            #    

            #
            # SECOND EQUATION: 
            #
            innerpg(J, IM, Tau_v.t11, Tau_v.t12, inner_temp)
            inner_temp /= Reynolds
            Residual_p.rhou -= inner_temp

            #
            # THIRD EQUATION: 
            #
            innerpg(J, IM, Tau_v.t12, Tau_v.t22, inner_temp)
            inner_temp /= Reynolds
            Residual_p.rhov -= inner_temp

            #
            # FOURTH EQUATION:
            #
            mu   = e.state(FieldType.ph, FieldVariable.MU)
            dedx = e.state(FieldType.ph, FieldVariable.DEIDX)
            dedy = e.state(FieldType.ph, FieldVariable.DEIDY)

            ViscousFlux_4_X(
                Reynolds, self.gamma, self.Pr, mu, Tau_v.t11, Tau_v.t12,
                p_wsp.u, p_wsp.v, dedx, self.Fx )
            
            ViscousFlux_4_Y(
                Reynolds, self.gamma, self.Pr, mu, Tau_v.t12, Tau_v.t22,
                p_wsp.u, p_wsp.v, dedy, self.Fy )

            innerpg(J, IM, self.Fx, self.Fy, inner_temp)
            Residual_p.rhoe -= inner_temp

        def visc_faceContribution(self, e):

            Reynolds   = self.Re
            
            # Convective flux
            ViscFlux = visc_flux_t(self.desc, self.equation)

            # Access the residual workspace
            Residual_p = ResidualWsp_p.get_instance(self.nS)

            num_faces  = len(e.neigh)

            # Access the workspace of face reconstructed data
            Face_f = FaceReconstructWsp_f.get_instance( *(e.face_comm_sizes) )

            #
            # 
            Temp = self.temp
            Aux  = self.aux
            inner_temp = self.inner_temp[e.type]
            
            for ff in range(num_faces):
                ng  = e.get_neighbour(ff)  # Get the ff-th neighbour
                nip = e.face_comm_size(ff) # Communication size on face ff

                #
                # Be careful when using ExpansionKey to get integration
                # points on faces: the second argument of the constructor
                # is the one-dimensional number of integration points for
                # numerical integration INSIDE the element. The number of
                # integration points generated on faces by ExpansionKey is
                # the second argument of the constructor PLUS ONE. Thus,
                # if you know the number of integration points on face (as
                # here) you must SUBTRACT ONE from it to get the correct
                # integration points on faces (see the assertion below).
                #
                k = ExpansionKeyFactory.make(e.key.order, nip-1)

                # In a mesh with non-constant p-order, the number of
                # integration points on each face can be different
                # from the others and then the inner product operator
                # my be set up when reaching each face.
                innerp = self.innerp_t[e.type](k)

                # The data on all faces was reconstructed by the
                # convective flux.

                # Evaluating the numerical flux f*(.)
                ViscFlux.eval(e, ff)
                
                # Access 
                Wsp_f = visc_flux_t.Workspace_f.get_instance(nip)

                # Normals to face ff at the integration points
                n = Face_f.n[ff]

                # Correct allocation
                Temp.resize( (nip,), refcheck=False)
                Aux.resize( (nip,), refcheck=False)

                # RHO: null contribution

                # RHOU
                np.multiply(n[0,:], Wsp_f.F_star[1][0], Temp)
                np.multiply(n[1,:], Wsp_f.F_star[1][1], Aux)
                Temp += Aux
                innerp(ff, Temp , e.get_face_jacobian(ff), inner_temp)
                inner_temp /= Reynolds
                Residual_p.rhou += inner_temp

                # RHOV
                np.multiply(n[0,:], Wsp_f.F_star[2][0], Temp)
                np.multiply(n[1,:], Wsp_f.F_star[2][1], Aux)
                Temp += Aux
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                inner_temp /= Reynolds
                Residual_p.rhov += inner_temp

                # RHOE
                np.multiply(n[0,:], Wsp_f.F_star[3][0], Temp)
                np.multiply(n[1,:], Wsp_f.F_star[3][1], Aux)
                Temp += Aux
                innerp(ff, Temp, e.get_face_jacobian(ff), inner_temp)
                inner_temp /= Reynolds
                Residual_p.rhoe += inner_temp

    # end of factory_weak_dg_viscous_res.weak_dg_viscous_res

    return weak_dg_viscous_res

# end of
# manticore.models.compressible.equations.viscres.factory_weak_dg_viscous_res
