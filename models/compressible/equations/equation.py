#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Equations
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

from abc import ABCMeta, abstractmethod
from copy import copy, deepcopy
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import EntityRole
from manticore.lops.modal_dg.computemesh import (
    SubDomainNameIterator, SubDomainRoleIterator)
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.models.compressible.description.mdtypes import EqType
from manticore.models.compressible.equations.convres import (
    factory_weak_dg_convres )
from manticore.models.compressible.equations.viscres import (
    factory_weak_auxiliar_eq, factory_weak_dg_viscous_res )
from manticore.models.compressible.equations.bcs import UpdateBC
from manticore.models.compressible.timeint.generic import (
    ConstantTimeStep, VariableTimeStep )

logger = manticore.logging.getLogger('MDL_CMP_EQ')

class Equation(metaclass=ABCMeta):
    """Generic class of a equation to be solved by the DG method.

    Notes:

        * It assumes an equation in integral form and DG
        discretization, weak or strong form.

        * Responsibility: definition of the public interface for 
            * Computation of residuals.
            * Query for the required variables.
    
    """
    def __init__(self):
        self._name  = None
        self._esets = []

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = copy(value)

    @property
    def element_sets(self):
        return self._esets

    @element_sets.setter
    def element_sets(self, value):
        self._esets = deepcopy(value)

    @property
    @abstractmethod
    def type(self):
        return EqType.EQTYPE_NONE

    @abstractmethod
    def compute_residual(self, desc, cm): pass

    @abstractmethod
    def compute_residual_derivative(self, desc, cm): pass

    def compute_fixed_time_step(self, desc, cm):
        cte_dt = ConstantTimeStep(desc.get_time_settings().deltat)

        subds = SubDomainRoleIterator(cm).find(EntityRole.PHYSICAL)
            
        for s in subds:
            foreach_entity_in_subdomain(s, cte_dt)

    @abstractmethod
    def compute_variable_time_step(self, desc, cm): pass

    @property
    @abstractmethod
    def state_variables(self): pass

    @property
    @abstractmethod
    def residual_variables(self): pass

    @property
    def standard_variables(self):
        return [FieldVariable.DELTAT,
                FieldVariable.CHAR_LENGTH]

    @property
    def vertex_variables(self): pass

    @property
    @abstractmethod
    def postprocessing_variables(self): pass

# end of manticore.models.compressible.equations.Equation

        
def factory_weak_euler(conv_flux_t):
    """Factory for the weak DG integral form of the conservative Euler equation.
    
    Args:
        conv_flux_t: A template parameter on the kind of convective
            flux we are using (Lax-Friedrich, Roe, HLLC, etc).

    """
    
    weak_dg_convective_res = factory_weak_dg_convres(conv_flux_t)

    class WeakDGEulerEquation(Equation):

        def __init__(self, desc):
            logger.debug("Weak DG form of Euler's equations...")
            Equation.__init__(self)
            self.euler_r = weak_dg_convective_res( desc )
            self.bc      = UpdateBC( desc )

        @property
        def type(self):
            return EqType.EULER

        @property
        def state_variables(self):
            return [FieldVariable.RHO,  FieldVariable.RHOU,
                    FieldVariable.RHOV, FieldVariable.RHOE]

        @property
        def residual_variables(self):
            return [FieldVariable.RHO,  FieldVariable.RHOU,
                    FieldVariable.RHOV, FieldVariable.RHOE]

        @property
        def postprocessing_variables(self):
            return [FieldVariable.RHO,  FieldVariable.RHOU,
                    FieldVariable.RHOV, FieldVariable.RHOE]

        def compute_residual(self, desc, cm):

            logger.debug('Updating boundary conditions...')
            self.bc.update( cm )
            logger.debug('Synchronization step...')
            self.bc.synchronize( cm ) # for parallel execution

            esets = self.element_sets
            subdomain_by_name = SubDomainNameIterator(cm)

            for eset in esets:

                logger.debug('Computing residual on subdomain %s' % (eset))
                
                s = subdomain_by_name.find(eset)

                if not s:
                    raise RuntimeError('Subdomain not found!!!')

                self.euler_r.setup(self.name, s.name)

                foreach_entity_in_subdomain(s, self.euler_r)

        def compute_residual_derivative(self, desc, cm): pass

        def compute_variable_time_step(self, desc, cm):

            var_dt = VariableTimeStep(desc)

            # #----------------------------------------------------------------
            # # Original version:
            #
            # subds = SubDomainRoleIterator(cm).find(EntityRole.PHYSICAL)
            # for s in subds:            
            #     eqs = desc.get_data(s.name).equations

            #     for eq in eqs:
            #         eqtype = desc.get_data(eq).type

            #         if ((eqtype==EqType.Euler) or
            #             (eqtype==EqType.NAVIERSTOKES)):
                    
            #             var_dt.setup(eq, s.name)
            #             foreach_entity_in_subdomain(s, var_dt)
                    
            #         else:
            #             raise RuntimeError('Equation not implemented!')
            # #----------------------------------------------------------------

            # Proposed version: it performs the same operation but
            # only acts on the subdomains associated to this
            # equation. This seems more coherent.
            esets = self.element_sets
            subdomain_by_name = SubDomainNameIterator(cm)

            for eset in esets:
                s = subdomain_by_name.find(eset)

                if not s:
                    raise RuntimeError('Subdomain not found!!!')

                var_dt.setup(self.name, s.name)
                foreach_entity_in_subdomain(s, var_dt)

    # end of factory_weak_euler.WeakDGEulerEquation

    return WeakDGEulerEquation

# end of manticore.models.compressible.equations.factory_weak_euler


def factory_weak_ns(conv_flux_t, visc_flux_t):

    weak_auxiliar_eq    = factory_weak_auxiliar_eq(visc_flux_t)
    weak_dg_viscous_res = factory_weak_dg_viscous_res(conv_flux_t, visc_flux_t)

    class WeakDGNSEquation(Equation):

        def __init__(self, desc):
            logger.debug("Weak DG form of Navier Stokes' equations...")
            Equation.__init__(self)
            self.aux_eq = weak_auxiliar_eq( desc )
            self.ns_r   = weak_dg_viscous_res( desc )
            self.bc     = UpdateBC( desc )

        @property
        def type(self):
            return EqType.NAVIERSTOKES

        @property
        def state_variables(self):
            return [FieldVariable.RHO,  FieldVariable.RHOU,
                    FieldVariable.RHOV, FieldVariable.RHOE,  # Conserved
                    FieldVariable.MU,                        # Viscosity
                    FieldVariable.DUDX,  FieldVariable.DUDY, # Grad u
                    FieldVariable.DVDX,  FieldVariable.DVDY, # Grad v
                    FieldVariable.DEIDX, FieldVariable.DEIDY]# Grad EI

        @property
        def residual_variables(self):
            return [FieldVariable.RHO,  FieldVariable.RHOU,
                    FieldVariable.RHOV, FieldVariable.RHOE]

        @property
        def postprocessing_variables(self):
            return [FieldVariable.RHO,  FieldVariable.RHOU,
                    FieldVariable.RHOV, FieldVariable.RHOE,
                    FieldVariable.MU,                        
                    FieldVariable.DUDX,  FieldVariable.DUDY, 
                    FieldVariable.DVDX,  FieldVariable.DVDY, 
                    FieldVariable.DEIDX, FieldVariable.DEIDY]

        def ApproximateGradients(self, desc, cm):
            
            logger.debug('Updating boundary conditions...')
            self.bc.update( cm )
            logger.debug('Synchronization step...')
            self.bc.synchronize( cm ) # for parallel execution

            esets = self.element_sets
            subdomain_by_name = SubDomainNameIterator(cm)

            for eset in esets:

                logger.debug(
                    'Computing auxiliar problem on subdomain %s' % (eset))
                
                s = subdomain_by_name.find(eset)

                if not s:
                    raise RuntimeError('Subdomain not found!!!')

                self.aux_eq.setup(self.name, s.name)

                foreach_entity_in_subdomain(s, self.aux_eq)            

        def compute_residual(self, desc, cm):

            self.ApproximateGradients(desc, cm)

            logger.debug('Synchronization step...')
            self.bc.synchronize( cm ) # for parallel execution

            esets = self.element_sets
            subdomain_by_name = SubDomainNameIterator(cm)

            for eset in esets:

                logger.debug('Computing residual on subdomain %s' % (eset))
                
                s = subdomain_by_name.find(eset)

                if not s:
                    raise RuntimeError('Subdomain not found!!!')

                self.ns_r.setup(self.name, s.name)

                foreach_entity_in_subdomain(s, self.ns_r)

        def compute_residual_derivative(self, desc, cm): pass

        def compute_variable_time_step(self, desc, cm):

            var_dt = VariableTimeStep(desc)

            esets = self.element_sets
            subdomain_by_name = SubDomainNameIterator(cm)

            for eset in esets:
                s = subdomain_by_name.find(eset)

                if not s:
                    raise RuntimeError('Subdomain not found!!!')

                var_dt.setup(self.name, s.name)
                foreach_entity_in_subdomain(s, var_dt)

    # end of factory_weak_ns.WeakDGNSEquation

    return WeakDGNSEquation

# end of manticore.models.compressible.equations.factory_weak_ns


