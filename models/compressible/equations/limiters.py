#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Limiters
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from array import array
import manticore
import manticore.services.const as const
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import FieldType, EntityRole
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.engine import (
    EntityOperator, foreach_entity_in_subdomain)
from manticore.lops.modal_dg.class_instances import (
    class_quad_wadg_physforward, class_quad_rwadg_physforward,
    class_tria_wadg_physforward, class_tria_rwadg_physforward,
    class_quad_physintegrator2d, class_tria_physintegrator2d )
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.models.compressible.services.predicatemixins import (
    model_description_mixin, equation_domain_signature)
from manticore.models.compressible.equations.workspaces import(
    FaceReconstructWsp_f)
from manticore.models.compressible.description.mdtypes import EqType

logger = manticore.logging.getLogger('MDL_CMP_EQ')

class positive_limit_cell(EntityOperator,
        model_description_mixin,
        equation_domain_signature):

    """Positivity preserving limiter applied to one cell according to:

    M. Zhang and C.-W. Shu, On positivity-preserving high order
    discontinuous Galerkin schemes for compressible Euler equations on
    rectangular meshes, Journal of Computational Physics, v. 229,
    pp. 8918-8934, 2010.

    """

    def __init__(self, model_desc):
        model_description_mixin.__init__(self, model_desc)
        equation_domain_signature.__init__(self)
        EntityOperator.__init__(self)

        self.fluid = None
        self.integ = [None, None]
        self.op    = [[None, None],[None, None]]
        self.u = np.zeros(0)
        self.v = np.zeros(0)
        self.p = np.zeros(0)

    def setup(self, eqname, domain):
        
        equation_domain_signature.setup(self, eqname, domain)

        matname = self.desc.get_equation(eqname).mat

        self.fluid = self.desc.get_material(matname)
        

    def container_operator(self, group, key, container):
        k  = ExpansionKeyFactory.make(key.order, key.nip)

        self.integ[0] = class_quad_physintegrator2d(k)
        self.integ[1] = class_tria_physintegrator2d(k)

        ctx = EntityOperator.contexts

        #       type       subtype
        self.op[ctx[0][0]][ctx[0][1]] = class_quad_wadg_physforward(k)
        self.op[ctx[1][0]][ctx[1][1]] = class_quad_rwadg_physforward(k)
        self.op[ctx[2][0]][ctx[2][1]] = class_tria_wadg_physforward(k)
        self.op[ctx[3][0]][ctx[3][1]] = class_tria_rwadg_physforward(k)

        nip = k.n2d
                
        if nip != self.u.shape[0]:
            self.u.resize( (nip,), refcheck=False )
            self.v.resize( (nip,), refcheck=False )
            self.p.resize( (nip,), refcheck=False )
    
    def __call__(self, e):

        assert self.domain
        assert e.role == EntityRole.PHYSICAL
        
        vol = e.volume
        if vol <= 0.:
            msg = 'Null or negative volume!'
            msg+= '\nElement : {}'.format(e.ID)
            raise AssertionError(msg)

        # Get internal physical values
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)

        # Cell average of density
        avg_rho  = self.integ[e.type](rho, e.get_jacobian())/vol

        # This limiter assumes positive density average
        if avg_rho <= 0.:
            msg = 'Null or negative average density!'
            msg+= '\nElement : {}'.format(e.ID)
            raise AssertionError(msg)

        # Cell averages of the other state variables and pressure
        pressure = self.fluid.pressure_from_scalar
        avg_rhou = self.integ[e.type](rhou, e.get_jacobian())/vol
        avg_rhov = self.integ[e.type](rhov, e.get_jacobian())/vol
        avg_rhoe = self.integ[e.type](rhoe, e.get_jacobian())/vol
        avg_u = avg_rhou/avg_rho
        avg_v = avg_rhov/avg_rho    
        avg_p = pressure(avg_rho, avg_rhoe, avg_u, avg_v)

        # This limiter assumes positive pressure average
        if avg_p <= 0.:
            msg = 'Null or negative average pressure!'
            msg+= '\nElement : {}'.format(e.ID)
            raise AssertionError(msg)

        eps = min(const.INF_VAR, avg_rho, avg_p)

        # Access workspace storage for face reconstruction
        FaceReconstruct_f = FaceReconstructWsp_f.get_instance(
            *(e.face_comm_sizes) )

        min_rho_ff = array('d')
        num_faces  = len(e.neigh)

        # Reconstruct values on the boundary of the element and find
        # minimun DENSITY values
        for ff in range(num_faces):
            FaceReconstruct_f.reconstruct_internal(ff, e, self.fluid)
            min_rho_ff.append(FaceReconstruct_f.rhoI[ff].min())

        min_rho = min(rho.min(), min(min_rho_ff))

        # Compute DENSITY correction factor
        if min_rho < avg_rho:
            theta1 = (avg_rho - eps)/(avg_rho - min_rho)
        else:
            theta1 = 1.0

        # Apply limiter on DENSITY (physical and polynomial)
        if theta1 < 1.0: # min_rho < eps
            logger.debug("Null/neg DENSITY detected at Element %s" % (e.ID))
            logger.debug("Limiting DENSITY on Element %s, theta = %s "
                             % (e.ID, theta1))
            rho *= theta1
            rho += (avg_rho - theta1*avg_rho)

            # Rebuild DENSITY polynomial interpolation: this way the
            # face reconstruction for PRESSURE limiting will use the
            # limited DENSITY.
            self.op[e.type][e.subtype](e.get_jacobian(), e.get_inv_mass(), rho,
                                       e.state(FieldType.tr, FieldVariable.RHO))

        # Recompute PRESSURE
        np.copyto(self.u, rhou)
        self.u /= rho
        np.copyto(self.v, rhov)
        self.v /= rho
        self.fluid.pressure(rho, rhoe, self.u, self.v, self.p)

        # Find the indices of physical values where PRESSURE < eps
        p_int_flag_idx = np.where(self.p < eps)[0]
        t_int = np.ones(p_int_flag_idx.shape[0])

        # Reconstruct values on the boundary of the element and find
        # points with PRESSURE < eps
        p_faces_flag_idx = []
        t_faces = []
        for ff in range(num_faces):
            FaceReconstruct_f.reconstruct_internal(ff, e, self.fluid)
            p_faces_flag_idx.append(np.where(FaceReconstruct_f.pI[ff] < eps)[0])
            t_faces.append(np.ones(p_faces_flag_idx[-1].shape[0]))
            

        # Compute aditional correction factor related to
        # PRESSURE. Note that correction o PRESSURE will only occur if
        # p_int_flag_idx and/or p_faces_flag_idx are not empty.

        # Control parameters **I assumed ** for the iterative process
        # inspired on the Armijo's rule for line search (the original
        # paper says that the PRESSURE is quadratic in t_int but it is
        # cubic and, as the interval surrounding the root is know, it
        # is safer to do it iteratively.)
        cut = 0.90
        max_cuts = 20

        # Compute internal PRESSURE correction factors
        for ii in range(p_int_flag_idx.shape[0]):
            idx = p_int_flag_idx[ii]
            p_s = self.p[idx]
            n_cuts = 0
            while (p_s < eps) and (n_cuts < max_cuts):
                t_int[ii] *= cut
                t = t_int[ii]
                rho_s  = (1.-t)*avg_rho  + t*rho[ idx]
                rhou_s = (1.-t)*avg_rhou + t*rhou[idx]
                rhov_s = (1.-t)*avg_rhov + t*rhov[idx]
                rhoe_s = (1.-t)*avg_rhoe + t*rhoe[idx]
                u_s = rhou_s/rho_s
                v_s = rhov_s/rho_s
                p_s = pressure(rho_s, rhoe_s, u_s, v_s)
                n_cuts +=1
            if p_s < eps:
                raise RuntimeError('Limiter could not find a viable pressure!')

        # Compute boundary PRESSURE correction factors
        t_face_min = array('d')
        for ff in range(num_faces):
            for ii in range(p_faces_flag_idx[ff].shape[0]):
                idx = p_faces_flag_idx[ff][ii]
                p_s = FaceReconstruct_f.pI[ff][idx]
                n_cuts = 0
                while (p_s < eps) and (n_cuts < max_cuts):
                    t_faces[ff][ii] *= cut
                    t = t_faces[ff][ii]
                    rho_s  = (1.-t)*avg_rho + t*FaceReconstruct_f.rhoI[ff][ idx]
                    rhou_s = (1.-t)*avg_rhou+ t*FaceReconstruct_f.rhouI[ff][idx]
                    rhov_s = (1.-t)*avg_rhov+ t*FaceReconstruct_f.rhovI[ff][idx]
                    rhoe_s = (1.-t)*avg_rhoe+ t*FaceReconstruct_f.rhoeI[ff][idx]
                    u_s = rhou_s/rho_s
                    v_s = rhov_s/rho_s
                    p_s = pressure(rho_s, rhoe_s, u_s, v_s)
                    n_cuts +=1
                if p_s < eps:
                    raise RuntimeError(
                        'Limiter could not find a viable pressure!')    
            if t_faces[ff].shape[0]:
                t_face_min.append(t_faces[ff].min())

        if t_int.shape[0] and len(t_face_min):
            theta2 = min(t_int.min(), min(t_face_min))
        elif t_int.shape[0]:
            theta2 = t_int.min()
        elif len(t_face_min):
            theta2 = min(t_face_min)
        else:
            theta2 = 1.0
        
        # Apply limiter on all state variables (physical and polynomial)
        if theta2 < 1.0: # p_min < eps
            logger.debug("Null/neg PRESSURE detected at Element %s" % (e.ID))
            logger.debug("Limiting STATE on Element %s, theta = %s"
                             % (e.ID, theta2))
            rho  *= theta2
            rho  += (avg_rho - theta2*avg_rho)

            rhou *= theta2
            rhou += (avg_rhou - theta2*avg_rhou)

            rhov *= theta2
            rhov += (avg_rhov - theta2*avg_rhov)

            rhoe *= theta2
            rhoe += (avg_rhoe - theta2*avg_rhoe)

            self.op[e.type][e.subtype](
                e.get_jacobian(), e.get_inv_mass(), rho,
                e.state(FieldType.tr, FieldVariable.RHO))

            self.op[e.type][e.subtype](
                e.get_jacobian(), e.get_inv_mass(), rhou,
                e.state(FieldType.tr, FieldVariable.RHOU))

            self.op[e.type][e.subtype](
                e.get_jacobian(), e.get_inv_mass(), rhov,
                e.state(FieldType.tr, FieldVariable.RHOV))

            self.op[e.type][e.subtype](
                e.get_jacobian(), e.get_inv_mass(), rhoe,
                e.state(FieldType.tr, FieldVariable.RHOE))

class PositivePreservingLimiter:
    """Positivity preserving limiter applied to all flow cells."""

    def __init__(self, model_desc):
        self._desc   = model_desc
        self.limiter = positive_limit_cell(model_desc)

    def apply(self, cm):
        # Access list of PHYSICAL subdomains 
        subds = SubDomainRoleIterator(cm).find(EntityRole.PHYSICAL)
        
        allowed_types = [EqType.EULER, EqType.NAVIERSTOKES,
                             EqType.LES, EqType.RANS_SA, EqType.RANS_SST]

        for s in subds:            
            eqs = self._desc.get_data(s.name).equations

            for eq in eqs:
                eqtype = self._desc.get_data(eq).type

                if eqtype in allowed_types:
                    self.limiter.setup(eq, s.name)
                    foreach_entity_in_subdomain(s, self.limiter)
