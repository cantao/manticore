#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Equation manager
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

from collections import OrderedDict, namedtuple
import manticore
from manticore.lops.modal_dg.fields import FieldVarList
from manticore.models.compressible.description.mdtypes import (
    EqType, EqParameter, EqSetting )
from manticore.models.compressible.equations.equation import (
    Equation, factory_weak_euler, factory_weak_ns )
from manticore.models.compressible.equations.convfluxes import (
    ConvectiveRoe, ConvectiveHLLC )
from manticore.models.compressible.equations.viscfluxes import (
    ViscousBR1 )

logger = manticore.logging.getLogger('MDL_CMP_EQ')

class EquationManager:

    def __init__(self):
        self._eqs = OrderedDict()

    def set_equations(self, desc):
        # Each equation in ModelDescription.data.children('EQUATIONS')
        # is mapped to a Equation instance.

        model_eqs_nodes = desc.get_equations() # List of Node objects

        for eq_desc_node in model_eqs_nodes:
            eq_desc = eq_desc_node.data
            n = eq_desc.name

            if n not in self._eqs:
                self._eqs[n] = self.init_equation(eq_desc, desc)
                self._eqs[n].name = n
                self._eqs[n].element_sets = desc.find_elsets_by_equation(n)
            else:
                raise RuntimeError('Different equations identified with the \
                                      same name. Please, check your arguments!')

    def __iter__(self):

        self.it = iter(self._eqs)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self._eqs[key]

    def __getitem__(self, index):
        
        names = list(self._eqs.keys())
        return self._eqs[names[index]] #because self._eqs isinstace(OrderedDict)

    def get(self, name):
        return self._eqs.get(name)

    def state_variables_in_elset(self, elset):
        v = set()

        for eq_name in elset.equations:
            for var in self._eqs[eq_name].state_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def state_variables(self):
        v = set()

        for eq_name in self._eqs:
            for var in self._eqs[eq_name].state_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def residual_variables_in_elset(self, elset):
        v = set()

        for eq_name in elset.equations:
            for var in self._eqs[eq_name].residual_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def residual_variables(self):
        v = set()

        for eq_name in self._eqs:
            for var in self._eqs[eq_name].residual_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def standard_variables_in_elset(self, elset):
        v = set()

        for eq_name in elset.equations:
            for var in self._eqs[eq_name].standard_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def standard_variables(self):
        v = set()

        for eq_name in self._eqs:
            for var in self._eqs[eq_name].standard_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def vertex_variables_in_elset(self, elset):
        v = set()

        for eq_name in elset.equations:
            for var in self._eqs[eq_name].vertex_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def vertex_variables(self):
        v = set()

        for eq_name in self._eqs:
            for var in self._eqs[eq_name].vertex_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def postprocessing_variables(self):
        v = set()

        for eq_name in self._eqs:
            for var in self._eqs[eq_name].postprocessing_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def postprocessing_variables_in_elset(self, elset):
        v = set()

        for eq_name in elset.equations:
            for var in self._eqs[eq_name].postprocessing_variables:
                v.add(var)

        return FieldVarList.get_instance(*list(v))

    def _init_euler_equation(self, eq_desc):

        logger.debug("Initialization of Euler's flow equations...")

        euler_option = namedtuple(
            'euler_option', ['cflux', 'pdg', 'dtype', 'dflux', 'adg'])

        settings = euler_option(
            cflux = eq_desc.get_data(EqParameter.CONVECTIVE_FLUX),
            pdg   = eq_desc.get_data(EqParameter.PRINCIPAL_DGFORM),
            dtype = eq_desc.get_data(EqParameter.DISSIPATION),
            dflux = eq_desc.get_data(EqParameter.DISSIPATIVE_FLUX),
            adg   = eq_desc.get_data(EqParameter.DAUXILIAR_DGFORM) )

        logger.debug("Settings:\n\t%s" % (settings._asdict()))

        # Supported settings:
        option1 = euler_option(
            cflux = EqSetting.CONVECTIVE_ROE,
            pdg   = EqSetting.WEAKFORM,
            dtype = EqSetting.EQSET_NONE,
            dflux = EqSetting.EQSET_NONE,
            adg   = EqSetting.EQSET_NONE )

        option2 = euler_option(
            cflux = EqSetting.CONVECTIVE_HLLC,
            pdg   = EqSetting.WEAKFORM,
            dtype = EqSetting.EQSET_NONE,
            dflux = EqSetting.EQSET_NONE,
            adg   = EqSetting.EQSET_NONE )

        switcher = {
            option1: factory_weak_euler(ConvectiveRoe),
            option2: factory_weak_euler(ConvectiveHLLC)
            }

        return switcher.get(settings, Equation)

    def _init_ns_equation(self, eq_desc):

        logger.debug("Initialization of Navier Stokes' equations...")

        ns_option = namedtuple(
            'ns_option',
            ['cflux', 'pdg', 'dtype', 'dflux', 'dadg', 'vadg', 'vflux'])

        settings = ns_option(
            cflux = eq_desc.get_data(EqParameter.CONVECTIVE_FLUX),
            pdg   = eq_desc.get_data(EqParameter.PRINCIPAL_DGFORM),
            dtype = eq_desc.get_data(EqParameter.DISSIPATION),
            dflux = eq_desc.get_data(EqParameter.DISSIPATIVE_FLUX),
            dadg  = eq_desc.get_data(EqParameter.DAUXILIAR_DGFORM),
            vadg  = eq_desc.get_data(EqParameter.VAUXILIAR_DGFORM),
            vflux = eq_desc.get_data(EqParameter.VISCOUS_FLUX) )

        logger.debug("Settings:\n\t%s" % (settings._asdict()))

        # Supported settings:
        option1 = ns_option(
            cflux = EqSetting.CONVECTIVE_ROE,
            pdg   = EqSetting.WEAKFORM,
            dtype = EqSetting.EQSET_NONE,
            dflux = EqSetting.EQSET_NONE,
            dadg  = EqSetting.EQSET_NONE,
            vadg  = EqSetting.WEAKFORM,
            vflux = EqSetting.VISCOUS_BR1)

        option2 = ns_option(
            cflux = EqSetting.CONVECTIVE_HLLC,
            pdg   = EqSetting.WEAKFORM,
            dtype = EqSetting.EQSET_NONE,
            dflux = EqSetting.EQSET_NONE,
            dadg  = EqSetting.EQSET_NONE,
            vadg  = EqSetting.WEAKFORM,
            vflux = EqSetting.VISCOUS_BR1 )

        switcher = {
            option1: factory_weak_ns(ConvectiveRoe, ViscousBR1),
            option2: factory_weak_ns(ConvectiveHLLC, ViscousBR1)
            }

        return switcher.get(settings, Equation)

    def init_equation(self, eq_desc, desc):

        switcher = {
            EqType.EULER:        self._init_euler_equation,
            EqType.NAVIERSTOKES: self._init_ns_equation
            }

        return switcher.get(eq_desc.type, Equation)(eq_desc)(desc)
        
