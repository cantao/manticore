#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Viscosity models
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.engine import EntityOperator
from manticore.lops.modal_dg.dgtypes import FieldType, EntityRole
from manticore.lops.modal_dg.class_instances import (
    class_quad_wadg_physforward, class_quad_rwadg_physforward,
    class_tria_wadg_physforward, class_tria_rwadg_physforward )
from manticore.models.compressible.services.predicatemixins import (
    model_description_mixin, equation_domain_signature)
from manticore.models.compressible.description.mdtypes import TransportModel


logger = manticore.logging.getLogger('MDL_CMP_EQ')


class cte_viscosity_model(EntityOperator,
        model_description_mixin,
        equation_domain_signature):


    def __init__(self, model_desc):
        model_description_mixin.__init__(self, model_desc)
        equation_domain_signature.__init__(self)
        EntityOperator.__init__(self)

        self.mu = 0.0
        self.op = [[None, None],[None, None]]


    def setup(self, eqname, domain):

        equation_domain_signature.setup(self, eqname, domain)

        matname = self.desc.get_equation(eqname).mat

        fluid = self.desc.get_material(matname)

        assert fluid.type.count(TransportModel.CONSTANT)

        self.mu = fluid.transport.mu

    def container_operator(self, group, key, container):
        k  = ExpansionKeyFactory.make(key.order, key.nip)

        ctx = EntityOperator.contexts

        #       type       subtype
        self.op[ctx[0][0]][ctx[0][1]] = class_quad_wadg_physforward(k)
        self.op[ctx[1][0]][ctx[1][1]] = class_quad_rwadg_physforward(k)
        self.op[ctx[2][0]][ctx[2][1]] = class_tria_wadg_physforward(k)
        self.op[ctx[3][0]][ctx[3][1]] = class_tria_rwadg_physforward(k)

    def __call__(self, e):

        assert self.domain
        assert e.role == EntityRole.PHYSICAL

        mu = e.state(FieldType.ph, FieldVariable.MU)
        mu.fill(self.mu)

        self.op[e.type][e.subtype](e.get_jacobian(), e.get_inv_mass(), mu,
                                       e.state(FieldType.tr, FieldVariable.MU))


class sutherland_viscosity_model(EntityOperator,
        model_description_mixin,
        equation_domain_signature):

    def __init__(self, model_desc):
        model_description_mixin.__init__(self, model_desc)
        equation_domain_signature.__init__(self)
        EntityOperator.__init__(self)

        self.fluid = None
        self.op    = [[None, None],[None, None]]
        self.aux   = np.zeros(0)

    def setup(self, eqname, domain):

        equation_domain_signature.setup(self, eqname, domain)

        matname = self.desc.get_equation(eqname).mat

        self.fluid = self.desc.get_material(matname)

        assert self.fluid.type == TransportModel.SUTHERLAND

        self.lambda_cte = self.fluid.transport.lambda_cte
        self.S          = self.fluid.transport.S

    def container_operator(self, group, key, container):
        k  = ExpansionKeyFactory.make(key.order, key.nip)

        ctx = EntityOperator.contexts

        #       type       subtype
        self.op[ctx[0][0]][ctx[0][1]] = class_quad_wadg_physforward(k)
        self.op[ctx[1][0]][ctx[1][1]] = class_quad_rwadg_physforward(k)
        self.op[ctx[2][0]][ctx[2][1]] = class_tria_wadg_physforward(k)
        self.op[ctx[3][0]][ctx[3][1]] = class_tria_rwadg_physforward(k)

        nip = k.n2d

        if nip != self.aux.shape[0]:
            self.aux.resize( (nip,), refcheck=False )

    def __call__(self, e):

        """
        Dynamic viscosity by the Sutherland model.

        Notes:

            * See Smits, Alexander J.; Dussauge, Jean-Paul
              (2006). Turbulent shear layers in supersonic
              flow. Birkhauser. p. 46. ISBN 0-387-26140-0.

            * See https://en.wikipedia.org/wiki/Viscosity:
            lambda_cte = mu_0 * (T_0+S)/(T_0**1.5)
            mu = lambda_cte * (T**1.5)/(T+S)


            * Air: DS = 120K, DTRef = 291.15K, DMuRef = 18.27E-6 Pa.s

        """

        assert self.domain
        assert e.role == EntityRole.PHYSICAL

        # Get internal physical values
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)
        mu = e.state(FieldType.ph, FieldVariable.MU)

        # Compute temperature: self.aux = T
        self.fluid.temperature_from_conserved(rho, rhou, rhov, rhoe, self.aux)

        np.copyto(mu, self.aux) # mu <- T
        np.power(mu, 1.5, mu)   # T**1.5
        self.aux += self.S      # T+S
        mu /= self.aux          # (T**1.5)/(T+S)
        mu *= self.lambda_cte   # lambda_cte * (T**1.5)/(T+S)

        self.op[e.type][e.subtype](e.get_jacobian(), e.get_mass(), mu,
                                       e.state(FieldType.tr, FieldVariable.MU))
