#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Workspaces
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from enum import Enum
import manticore.services.const as const
from manticore.services.datatypes import FlyweightMixin
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.dgtypes import FieldType, FieldRole, EntityRole
from manticore.lops.nodal_cg.interpol import InterpolationKey

class WorkspaceType(Enum):

    NoWorkspace = -1
    Facial     = 0
    Volumetric = 1
    Polynomial = 2

    def size():
        """ Enum size (valid itens). """
        return 4

def factory_primitive_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class PrimitiveWsp(FlyweightMixin):

            def __init__(self, size):
                assert size > 0

                self.size = size
                self.u = np.zeros(size)
                self.v = np.zeros(size)
                self.p = np.zeros(size)

            def fill(self, e, fluid):

                rho  = e.state(FieldType.ph, FieldVariable.RHO)
                rhoe = e.state(FieldType.ph, FieldVariable.RHOE)

                if rho.min() < const.INF_VAR:
                    msg = 'Primitive computation failed: '
                    msg+= 'DENSITY is negative or too small!'
                    msg+= '\nElement : {}'.format(e.ID)
                    msg+= '\nRHO: {}'.format(rho)
                    raise AssertionError(msg)

                np.copyto(self.u, e.state(FieldType.ph, FieldVariable.RHOU))
                self.u /= rho

                np.copyto(self.v, e.state(FieldType.ph, FieldVariable.RHOV))
                self.v /= rho

                fluid.pressure(rho, rhoe, self.u, self.v, self.p)

                if self.p.min() < const.INF_VAR:
                    msg = 'Primitive computation failed: '
                    msg+= 'PRESSURE is negative or too small!'
                    msg+= '\nElement : {}'.format(e.ID)
                    msg+= '\nRHO: {}'.format(rho)
                    msg+= '\nP: {}'.format(self.p)
                    raise AssertionError(msg)

                #
                # Fazer filtragem em u, v, p?
                #

        return PrimitiveWsp

    elif wsp_type == WorkspaceType.Facial:

        class PrimitiveWsp:
            def __init__(self): pass

        return PrimitiveWsp

    elif wsp_type == WorkspaceType.Polynomial:

        class PrimitiveWsp:
            def __init__(self): pass

        return PrimitiveWsp

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
PrimitiveWsp_v = factory_primitive_wsp(WorkspaceType.Volumetric)

# end of manticore.models.compressible.equations.factory_primitive_wsp


def factory_facereconstruction_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class FaceReconstructionWsp:
            def __init__(self): pass

        return FaceReconstructionWsp

    elif wsp_type == WorkspaceType.Facial:

        class FaceReconstructionWsp(FlyweightMixin):

            def __init__(self, *size):
                """Constructor of FaceReconstructionWsp objects.

                Args:

                    size (tuple(int)): tuple of values
                        max(e.key.n1d, ng.key.n1d) for all faces of an
                        element e.
                """

                # Notation: E suffix stands for "external" (neighbour)
                # and I stands for "internal" (the current element)
                assert len(size) in (3,4)

                self.rhoI  = [np.zeros(sz) for sz in size]
                self.rhouI = [np.zeros(sz) for sz in size]
                self.rhovI = [np.zeros(sz) for sz in size]
                self.rhoeI = [np.zeros(sz) for sz in size]

                self.rhoE  = [np.zeros(sz) for sz in size]
                self.rhouE = [np.zeros(sz) for sz in size]
                self.rhovE = [np.zeros(sz) for sz in size]
                self.rhoeE = [np.zeros(sz) for sz in size]

                self.pI = [np.zeros(sz) for sz in size]
                self.uI = [np.zeros(sz) for sz in size]
                self.vI = [np.zeros(sz) for sz in size]
                self.HI = [np.zeros(sz) for sz in size]

                self.pE = [np.zeros(sz) for sz in size]
                self.uE = [np.zeros(sz) for sz in size]
                self.vE = [np.zeros(sz) for sz in size]
                self.HE = [np.zeros(sz) for sz in size]

                self.n =  [np.zeros((2,sz)) for sz in size]
                self.t =  [np.zeros((2,sz)) for sz in size]

            def reconstruct(self, ff, e, ng, fluid):
                """

                Args:

                    ff (int): Local numbering of current face of entity e.

                    e (ExpansionEntity): active element (also called
                        "left" or "internal").

                    ng (ExpansionEntity): passive element (also called
                        "neighbour", "right" or "external").
                """
                nip = e.face_comm_size(ff)  # nip = max(e.n1d, ng.n1d)

                assert self.uI[ff].shape[0]==nip

                state = FieldRole.State

                # Current element (internal) reconstruction
                e.get_face_field(state, FieldVariable.RHO,  ff, self.rhoI[ff])
                e.get_face_field(state, FieldVariable.RHOU, ff, self.rhouI[ff])
                e.get_face_field(state, FieldVariable.RHOV, ff, self.rhovI[ff])
                e.get_face_field(state, FieldVariable.RHOE, ff, self.rhoeI[ff])

                # Neighbour element (external) reconstruction
                ng_ff   = e.get_neighbour_face_id(ff)
                ng_role = ng.role
                e_type  = e.type

                if( (ng_role == EntityRole.PHYSICAL) or
                    (ng_role == EntityRole.COMM_GHOST) ):
                    ng.get_face_field_as_passive(state, FieldVariable.RHO,
                                        e_type, ff, nip, ng_ff, self.rhoE[ff])
                    ng.get_face_field_as_passive(state, FieldVariable.RHOU,
                                        e_type, ff, nip, ng_ff, self.rhouE[ff])
                    ng.get_face_field_as_passive(state, FieldVariable.RHOV,
                                        e_type, ff, nip, ng_ff, self.rhovE[ff])
                    ng.get_face_field_as_passive(state, FieldVariable.RHOE,
                                        e_type, ff, nip, ng_ff, self.rhoeE[ff])

                else: # If neighbour is a ghost, copying is enough for now
                    ng.get_face_field(
                        state, FieldVariable.RHO, ng_ff,self.rhoE[ff])
                    ng.get_face_field(
                        state, FieldVariable.RHOU,ng_ff,self.rhouE[ff])
                    ng.get_face_field(
                        state, FieldVariable.RHOV,ng_ff,self.rhovE[ff])
                    ng.get_face_field(
                        state, FieldVariable.RHOE,ng_ff,self.rhoeE[ff])

                #
                # Fazer filtragem?
                #

                if ((self.rhoI[ff].min() < const.INF_VAR) or
                    (self.rhoE[ff].min() < const.INF_VAR)):
                    msg = 'Facial reconstruction failed: '
                    msg+= 'DENSITY is negative or too small!'
                    msg+= '\nElement : {}'.format(e.ID)
                    msg+= ' Face : {}'.format(ff)
                    msg+= '\nRHO on face [I]: \n{}'.format(self.rhoI[ff])
                    msg+= '\nNeighbor : {}'.format(ng.ID)
                    msg+= '\nRHO on face [E]: \n{}'.format(self.rhoE[ff])
                    raise AssertionError(msg)

                # Primitive velocities
                np.copyto(self.uI[ff], self.rhouI[ff])
                self.uI[ff] /= self.rhoI[ff]
                np.copyto(self.vI[ff], self.rhovI[ff])
                self.vI[ff] /= self.rhoI[ff]

                np.copyto(self.uE[ff], self.rhouE[ff])
                self.uE[ff] /= self.rhoE[ff]
                np.copyto(self.vE[ff], self.rhovE[ff])
                self.vE[ff] /= self.rhoE[ff]

                # Pressure
                fluid.pressure(
                    self.rhoI[ff], self.rhoeI[ff],
                    self.uI[ff], self.vI[ff], self.pI[ff])
                fluid.pressure(
                    self.rhoE[ff], self.rhoeE[ff],
                    self.uE[ff], self.vE[ff], self.pE[ff])

                if ((self.pI[ff].min() < const.INF_VAR) or
                    (self.pE[ff].min() < const.INF_VAR)):
                    msg = 'Facial reconstruction failed: '
                    msg+= 'PRESSURE is negative or too small!'
                    msg+= '\nElement : {}'.format(e.ID)
                    msg+= ' Face : {}'.format(ff)
                    msg+= '\nRHO      on face [I]: \n{}'.format(self.rhoI[ff])
                    msg+= '\nPRESSURE on face [I]: \n{}'.format(self.pI[ff])
                    msg+= '\nNeighbor : {}'.format(ng.ID)
                    msg+= '\nRHO      on face [E]: \n{}'.format(self.rhoE[ff])
                    msg+= '\nPRESSURE on face [E]: \n{}'.format(self.pE[ff])
                    raise AssertionError(msg)

                # Enthalpy (well, almost):
                #     H = (RHOE + P) /RHO = (EI + P/RHO) + 0.5*(U^2+V^2) !!!
                # Be careful how this value is used.
                np.copyto(self.HI[ff], self.rhoeI[ff])
                self.HI[ff] += self.pI[ff]
                self.HI[ff] /= self.rhoI[ff]

                np.copyto(self.HE[ff], self.rhoeE[ff])
                self.HE[ff] += self.pE[ff]
                self.HE[ff] /= self.rhoE[ff]

                k = InterpolationKey.get_instance(e.geom_type, nip, ff)
                e.fmap.normals_tangents(k, e.get_coeff(),self.n[ff],self.t[ff])

            def reconstruct_internal(self, ff, e, fluid):
                """

                Args:

                    ff (int): Local numbering of current face of entity e.

                    e (ExpansionEntity): active element (also called
                        "left" or "internal").

                    ng (ExpansionEntity): passive element (also called
                        "neighbour", "right" or "external").
                """
                nip = e.face_comm_size(ff)  # nip = max(e.n1d, ng.n1d)

                assert self.uI[ff].shape[0]==nip

                state = FieldRole.State

                # Current element (internal) reconstruction
                e.get_face_field(state, FieldVariable.RHO,  ff, self.rhoI[ff])
                e.get_face_field(state, FieldVariable.RHOU, ff, self.rhouI[ff])
                e.get_face_field(state, FieldVariable.RHOV, ff, self.rhovI[ff])
                e.get_face_field(state, FieldVariable.RHOE, ff, self.rhoeI[ff])

                #
                # Fazer filtragem?
                #

                # Primitive velocities
                np.copyto(self.uI[ff], self.rhouI[ff])
                self.uI[ff] /= self.rhoI[ff]
                np.copyto(self.vI[ff], self.rhovI[ff])
                self.vI[ff] /= self.rhoI[ff]

                # Pressure
                fluid.pressure(self.rhoI[ff], self.rhoeI[ff],
                               self.uI[ff], self.vI[ff], self.pI[ff])

        return FaceReconstructionWsp

    elif wsp_type == WorkspaceType.Polynomial:

        class FaceReconstructionWsp:
            def __init__(self): pass

        return FaceReconstructionWsp

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
FaceReconstructWsp_f = factory_facereconstruction_wsp(WorkspaceType.Facial)

# end of manticore.models.compressible.equations.factory_facereconstruction_wsp


def factory_convectiveroe_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class ConvectiveRoeWsp:
            def __init__(self): pass

        return ConvectiveRoeWsp

    elif wsp_type == WorkspaceType.Facial:

        class ConvectiveRoeWsp(FlyweightMixin):

            def __init__(self, size):
                assert size > 0

                # Notation: E suffix stands for "external" (neighbour)
                # and I stands for "internal" (the current element)

                self.rho_star = np.zeros(size)
                self.u_star   = np.zeros(size)
                self.v_star   = np.zeros(size)
                self.H_star   = np.zeros(size)

                self.tempI = np.zeros(size)
                self.tempE = np.zeros(size)
                self.temp  = np.zeros(size)
                self.aux   = np.zeros(size)

                self.T1 = np.zeros(size)
                self.T2 = np.zeros(size)
                self.T4 = np.zeros(size)

                self.a  = np.zeros(size)
                self.a2 = np.zeros(size)

                self.vn = np.zeros(size)

                self.d_rho  = np.zeros(size)
                self.d_rhou = np.zeros(size)
                self.d_rhov = np.zeros(size)
                self.d_rhoe = np.zeros(size)

                self.LL1 = np.zeros(size)
                self.LL2 = np.zeros(size)
                self.LL3 = np.zeros(size)
                self.LL4 = np.zeros(size)
                self.LL5 = np.zeros(size)

                self.RLLd1 = np.zeros(size)
                self.RLLd2 = np.zeros(size)
                self.RLLd3 = np.zeros(size)
                self.RLLd5 = np.zeros(size)

                # Numerical flux
                self.F_star=[[np.zeros(size),np.zeros(size)] for i in range(4)]

        return ConvectiveRoeWsp

    elif wsp_type == WorkspaceType.Polynomial:

        class ConvectiveRoeWsp:
            def __init__(self): pass

        return ConvectiveRoeWsp

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
ConvectiveRoeWsp_f = factory_convectiveroe_wsp(WorkspaceType.Facial)

# end of manticore.models.compressible.equations.factory_convectiveroe_wsp


def factory_convectivehllc_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class ConvectiveHLLCWsp:
            def __init__(self): pass

        return ConvectiveHLLCWsp

    elif wsp_type == WorkspaceType.Facial:

        class ConvectiveHLLCWsp(FlyweightMixin):

            def __init__(self, size):
                assert size > 0

                # Notation: E suffix stands for "external" (neighbour)
                # and I stands for "internal" (the current element)

                # Roe averaged state
                self.rho_star = np.zeros(size)
                self.u_star   = np.zeros(size)
                self.v_star   = np.zeros(size)
                self.H_star   = np.zeros(size)

                # Primitive
                self.tempI = np.zeros(size)
                self.tempE = np.zeros(size)
                self.temp  = np.zeros(size) # auxiliars
                self.T1 = np.zeros(size)    # temporary
                self.a  = np.zeros(size)    # sound speed
                self.a2 = np.zeros(size)
                self.vn = np.zeros(size)    # normal velocity

                # Quantities for HLLC
                self.vnI  = np.zeros(size)  # normal velocity
                self.vnE  = np.zeros(size)
                self.aI   = np.zeros(size)  # sound speed
                self.aE   = np.zeros(size)
                self.SI   = np.zeros(size)  # wave speed estimates
                self.SE   = np.zeros(size)
                self.SM   = np.zeros(size)
                self.aux1 = np.zeros(size)  # auxiliars
                self.aux2 = np.zeros(size)
                self.pM   = np.zeros(size)  # middle pressure

                self.rhoIM  = np.zeros(size)
                self.rhoDI  = np.zeros(size)
                self.rhoEM  = np.zeros(size)
                self.rhoDE  = np.zeros(size)
                self.rhouIM = np.zeros(size)
                self.rhouDI = np.zeros(size)
                self.rhouEM = np.zeros(size)
                self.rhouDE = np.zeros(size)
                self.rhovIM = np.zeros(size)
                self.rhovDI = np.zeros(size)
                self.rhovEM = np.zeros(size)
                self.rhovDE = np.zeros(size)
                self.rhoeIM = np.zeros(size)
                self.rhoeDI = np.zeros(size)
                self.rhoeEM = np.zeros(size)
                self.rhoeDE = np.zeros(size)

                self.aux = np.zeros(size)

                # masks for if-else statements
                self.cond1   = np.full((size,), False, dtype=bool)
                self.cond2   = np.full((size,), False, dtype=bool)
                self.cond3   = np.full((size,), False, dtype=bool)
                self.cond4   = np.full((size,), False, dtype=bool)
                self.success = np.full((size,), False, dtype=bool)

                # Numerical flux
                self.F_star=[[np.zeros(size),np.zeros(size)] for i in range(4)]

        return ConvectiveHLLCWsp

    elif wsp_type == WorkspaceType.Polynomial:

        class ConvectiveHLLCWsp:
            def __init__(self): pass

        return ConvectiveHLLCWsp

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
ConvectiveHLLCWsp_f = factory_convectivehllc_wsp(WorkspaceType.Facial)

# end of manticore.models.compressible.equations.factory_convectivehllc_wsp


def factory_residual_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class ResidualWsp:
            def __init__(self): pass

        return ResidualWsp

    elif wsp_type == WorkspaceType.Facial:

        class ResidualWsp:
            def __init__(self): pass

        return ResidualWsp

    elif wsp_type == WorkspaceType.Polynomial:

        class ResidualWsp(FlyweightMixin):

            def __init__(self, size):
                assert size > 0

                self.rho  = np.zeros(size)
                self.rhou = np.zeros(size)
                self.rhov = np.zeros(size)
                self.rhoe = np.zeros(size)

            def zero(self):
                self.rho.fill( 0.0)
                self.rhou.fill(0.0)
                self.rhov.fill(0.0)
                self.rhoe.fill(0.0)

        return ResidualWsp

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
ResidualWsp_p = factory_residual_wsp(WorkspaceType.Polynomial)

# end of manticore.models.compressible.equations.factory_residual_wsp


def factory_viscousbr1_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class ViscousBR1Wsp_v(FlyweightMixin):
            def __init__(self, size):
                assert size > 0

                self.eInner = np.zeros(size)
                self.temp   = np.zeros(size)

        return ViscousBR1Wsp_v

    elif wsp_type == WorkspaceType.Facial:

        class ViscousBR1Wsp_f(FlyweightMixin):
            def __init__(self, size):
                assert size > 0

                # "Tilda" properties. For BR1, that's just the mean

                self.uTilda = np.zeros(size)
                self.vTilda = np.zeros(size)
                self.eTilda = np.zeros(size)
                self.eI     = np.zeros(size)
                self.eE     = np.zeros(size)
                self.temp   = np.zeros(size)

                # Facial reconstructions
                # Notation: E suffix stands for "external" (neighbour)
                # and I stands for "internal" (the current element)

                self.dudxI  = np.zeros(size)
                self.dudxE  = np.zeros(size)
                self.dudyI  = np.zeros(size)
                self.dudyE  = np.zeros(size)
                self.dvdxI  = np.zeros(size)
                self.dvdxE  = np.zeros(size)
                self.dvdyI  = np.zeros(size)
                self.dvdyE  = np.zeros(size)
                self.deidxI = np.zeros(size)
                self.deidxE = np.zeros(size)
                self.deidyI = np.zeros(size)
                self.deidyE = np.zeros(size)

                self.muI    = np.zeros(size)
                self.muE    = np.zeros(size)

                self.t11I   = np.zeros(size)
                self.t12I   = np.zeros(size)
                self.t22I   = np.zeros(size)
                self.t11E   = np.zeros(size)
                self.t12E   = np.zeros(size)
                self.t22E   = np.zeros(size)

                self.b1I    = np.zeros(size)
                self.b1E    = np.zeros(size)
                self.b2I    = np.zeros(size)
                self.b2E    = np.zeros(size)

                # Numerical flux
                self.F_star=[[np.zeros(size),np.zeros(size)] for i in range(4)]

        return ViscousBR1Wsp_f

    elif wsp_type == WorkspaceType.Polynomial:

        class ViscousBR1Wsp_p(FlyweightMixin):

            def __init__(self, size):
                assert size > 0

                self.uxc = np.zeros(size)
                self.uyc = np.zeros(size)
                self.vxc = np.zeros(size)
                self.vyc = np.zeros(size)
                self.exc = np.zeros(size)
                self.eyc = np.zeros(size)

            def zero(self):
                self.uxc.fill(0.0)
                self.uyc.fill(0.0)
                self.vxc.fill(0.0)
                self.vyc.fill(0.0)
                self.exc.fill(0.0)
                self.eyc.fill(0.0)

        return ViscousBR1Wsp_p

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
ViscousBR1Wsp_v = factory_viscousbr1_wsp(WorkspaceType.Volumetric)
ViscousBR1Wsp_f = factory_viscousbr1_wsp(WorkspaceType.Facial)
ViscousBR1Wsp_p = factory_viscousbr1_wsp(WorkspaceType.Polynomial)

# end of manticore.models.compressible.equations.factory_viscousbr1_wsp

def factory_tau_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class TauWsp(FlyweightMixin):

            def __init__(self, size):
                assert size > 0

                self.t11 = np.zeros(size)
                self.t12 = np.zeros(size)
                self.t22 = np.zeros(size)

        return TauWsp

    elif wsp_type == WorkspaceType.Facial:

        class TauWsp:
            def __init__(self): pass

        return TauWsp

    elif wsp_type == WorkspaceType.Polynomial:

        class TauWsp:
            def __init__(self): pass

        return TauWsp

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
TauWsp_v = factory_tau_wsp(WorkspaceType.Volumetric)

# end of manticore.models.compressible.equations.factory_tau_wsp


def factory_integral_parameters(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class IntegralParametersWsp(FlyweightMixin):

            def __init__(self, size):
                assert size > 0

                self.temp_p = np.zeros(size)
                self.p_cont = np.zeros(size)
                self.visc_temp11 = np.zeros(size)
                self.visc_temp12 = np.zeros(size)
                self.visc_temp21 = np.zeros(size)
                self.visc_temp22 = np.zeros(size)
                self.visc_cont = np.zeros(size)

        return IntegralParametersWsp

    elif wsp_type == WorkspaceType.Facial:

        class IntegralParametersWsp:
            def __init__(self): pass

        return IntegralParametersWsp

    elif wsp_type == WorkspaceType.Polynomial:

        class IntegralParametersWsp:
            def __init__(self): pass

        return IntegralParametersWsp

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
IntegralParametersWsp_v = factory_integral_parameters(WorkspaceType.Volumetric)

# end of manticore.models.compressible.equations.factory_integral_parameters

