#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Boundary conditions
#
# @addtogroup MODELS.COMPRESSIBLE.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore
import manticore.services.const as const
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import FieldType, FieldRole, EntityRole
from manticore.lops.modal_dg.engine import (
    EntityOperator, foreach_entity_in_subdomain)
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.models.compressible.services.predicatemixins import (
    model_description_mixin, equation_domain_signature)
from manticore.models.compressible.description.mdtypes import (
    FaceParameter, EqType, FaceType)
from manticore.lops.modal_dg.class_instances import (
    class_quad_physevaluator1d, class_tria_physevaluator1d)
from manticore.models.compressible.services.datatypes import (
    ModelTime )
#
#
from manticore.models.compressible.directives import ExecDirective
#
# Execution accordingly to command line directives
exec_dir = manticore.models.compressible.directive

if exec_dir==ExecDirective.NULL_DIRICHLET:
    from manticore.models.compressible.equations.dirichlet_bcs import (
        null_dirichlet )

elif exec_dir==ExecDirective.WARBURTON_EX_6_1:
    from manticore.models.compressible.tests.test_helpers import (
        example_warburton_book_6p1 )
    
elif exec_dir==ExecDirective.CTE_EULER_FIELDS:
    from manticore.models.compressible.tests.test_helpers import (
        constant_euler_fields )

elif exec_dir==ExecDirective.NS_VALIDATION:
    from manticore.models.compressible.tests.test_helpers import (
        ns_validation )
#

class BC_Base(model_description_mixin, equation_domain_signature):

    def __init__(self, model_desc):
        model_description_mixin.__init__(self, model_desc)
        equation_domain_signature.__init__(self)

        self.rhoI  = np.zeros(0)
        self.rhouI = np.zeros(0)
        self.rhovI = np.zeros(0)
        self.rhoeI = np.zeros(0)

        self.pI = np.zeros(0)
        self.uI = np.zeros(0)
        self.vI = np.zeros(0)

    def setup(self, eqname, domain):
        
        equation_domain_signature.setup(self, eqname, domain)    

    def resize(self, nip):

        self.rhoI.resize( (nip,), refcheck=False)
        self.rhouI.resize((nip,), refcheck=False)
        self.rhovI.resize((nip,), refcheck=False)
        self.rhoeI.resize((nip,), refcheck=False)

        self.pI.resize((nip,), refcheck=False)
        self.uI.resize((nip,), refcheck=False)
        self.vI.resize((nip,), refcheck=False)

    def do_neighbour_reconstruction(self, ng, ng_face_id, fluid):

        # B.C.s are applied on ghosts. Thus their neighbours are PHYSICAL.
        assert ng.role==EntityRole.PHYSICAL

        state = FieldRole.State

        # Computation of values on boundary face
        ng.get_face_field(state, FieldVariable.RHO,  ng_face_id, self.rhoI)
        ng.get_face_field(state, FieldVariable.RHOU, ng_face_id, self.rhouI)
        ng.get_face_field(state, FieldVariable.RHOV, ng_face_id, self.rhovI)
        ng.get_face_field(state, FieldVariable.RHOE, ng_face_id, self.rhoeI)

        if self.rhoI.min() < const.INF_VAR:
            msg = 'Facial reconstruction failed: '
            msg+= 'DENSITY is negative or too small!'
            msg+= '\nElement : {} - Face: {}'.format(ng.ID, ng_face_id)
            msg+= '\nRHO on face [I]: {}'.format(self.rhoI)
            raise AssertionError(msg)

        np.copyto(self.uI, self.rhouI)
        self.uI /= self.rhoI              
        np.copyto(self.vI, self.rhovI)
        self.vI /= self.rhoI

        # Pressure
        fluid.pressure(self.rhoI, self.rhoeI, self.uI, self.vI, self.pI)

        if self.pI.min() < const.INF_VAR:
            msg = 'Facial reconstruction failed: '
            msg+= 'PRESSURE is negative or too small!'
            msg+= '\nElement : {} - Face: {}'.format(ng.ID, ng_face_id)
            msg+= '\nRHO     on face [I]: {}'.format(self.rhoI)
            msg+= '\PRESSURE on face [I]: {}'.format(self.pI)
            raise AssertionError(msg)

# end of manticore.models.compressible.equations.bcs.BC_Base


class dirichlet_bc(EntityOperator):
    def __init__(self, f):
        EntityOperator.__init__(self)
        self.f = f
        self.op = [None, None]

    def setup(self, eqname, domain):
        self.equation = eqname
        self.domain   = domain

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.op[0] = class_quad_physevaluator1d(k)
        self.op[1] = class_tria_physevaluator1d(k)

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)
        
        # Finally, the boundary condition imposition
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)

        np.copyto( rho, self.op[ng.type](
            ng_face_id, self.f.rho, ng.get_coeff(),  ng.geom_type, ng.fmap) )
        np.copyto( rhou, self.op[ng.type](
            ng_face_id, self.f.rhou, ng.get_coeff(), ng.geom_type, ng.fmap) )
        np.copyto( rhov, self.op[ng.type](
            ng_face_id, self.f.rhov, ng.get_coeff(), ng.geom_type, ng.fmap) )
        np.copyto( rhoe, self.op[ng.type](
            ng_face_id, self.f.rhoe, ng.get_coeff(), ng.geom_type, ng.fmap) )
        
# end of manticore.models.compressible.equations.bcs.dirichlet_bc


class dirichlet_cte_bc(EntityOperator, BC_Base):
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

        self.rho  = 0.0
        self.rhou = 0.0
        self.rhov = 0.0
        self.rhoe = 0.0

    def setup(self, eqname, domain):
        
        BC_Base.setup(self, eqname, domain)

        # In the case of ghosts domainName corresponds to the name of
        # the original face set that gave birth to the ghost set.        
        fsdata = self.desc.get_faceset(domain).data

        self.rho  = fsdata.get_data(FaceParameter.DENSITY)
        self.rhou = fsdata.get_data(FaceParameter.MOMENTUMX)
        self.rhov = fsdata.get_data(FaceParameter.MOMENTUMY)
        self.rhoe = fsdata.get_data(FaceParameter.TOTALENERGY)

    def container_operator(self, group, key, container): pass

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Finally, the boundary condition imposition
        e.state(FieldType.ph, FieldVariable.RHO ).fill(self.rho )
        e.state(FieldType.ph, FieldVariable.RHOU).fill(self.rhou)
        e.state(FieldType.ph, FieldVariable.RHOV).fill(self.rhov)
        e.state(FieldType.ph, FieldVariable.RHOE).fill(self.rhoe)
        
# end of manticore.models.compressible.equations.bcs.dirichlet_cte_bc


class adiabaticwall_bc(EntityOperator, BC_Base):
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

        self.uW = 0.0
        self.vW = 0.0
        self.uWall = np.zeros(0)
        self.vWall = np.zeros(0)
        self.velProj = np.zeros(0)
        self.fluid = None

        self.aux = np.zeros(0)

    def setup(self, eqname, domain):
        
        BC_Base.setup(self, eqname, domain)

        # In the case of ghosts domainName corresponds to the name of
        # the original face set that gave birth to the ghost set.        
        fsdata = self.desc.get_faceset(domain).data

        self.uW = fsdata.get_data(FaceParameter.VELX)
        self.vW = fsdata.get_data(FaceParameter.VELY)

        matname = self.desc.get_equation(eqname).mat

        self.fluid = self.desc.get_material(matname)

    def container_operator(self, group, key, container): pass

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)
        self.resize(nip)

        # Physical element (internal) reconstruction
        self.do_neighbour_reconstruction(ng, ng_face_id, self.fluid)

        # Spread the wall velocity value to all integration points
        self.uWall.resize( (nip,), refcheck=False)
        self.uWall.fill(self.uW)
        self.vWall.resize( (nip,), refcheck=False)
        self.vWall.fill(self.vW)

        #
        self.velProj.resize( (nip,), refcheck=False)
        self.velProj.fill(0.0)
        self.aux.resize( (nip,), refcheck=False)
        self.aux.fill(0.0)

        # Difference between inner products of wall velocity and flow velocity
        # velProj = <uW,vW> . <uW,vW> - <u,v> . <uW,vW>
        # = uW( uW-u ) + vW( vW-v )
        self.velProj += self.uWall
        self.velProj -= self.uI
        self.velProj *= self.uWall
        self.aux     += self.vWall
        self.aux     -= self.vI
        self.aux     *= self.vWall
        self.velProj += self.aux

        # Finally, the boundary condition imposition
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)
        
        np.copyto(rho, self.rhoI)
        #
        np.copyto(rhou, self.rhoI)
        rhou *= self.uWall
        #
        np.copyto(rhov, self.rhoI)
        rhov *= self.vWall
        #
        np.copyto(rhoe, self.rhoI)
        rhoe *= self.velProj
        rhoe += self.rhoeI

# end of manticore.models.compressible.equations.bcs.adiabaticwall_bc        
        
class backpressure_bc(EntityOperator, BC_Base):
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

        self.sP          = 0.0
        self.back_vn     = np.zeros(0)
        self.sound_speed = np.zeros(0)
        self.rhoe        = np.zeros(0)
        
        self.n           = np.zeros(0)
        self.t           = np.zeros(0)
        
        self.fluid       = None

    def setup(self, eqname, domain):
        
        BC_Base.setup(self, eqname, domain)

        # In the case of ghosts domainName corresponds to the name of
        # the original face set that gave birth to the ghost set.        
        fsdata = self.desc.get_faceset(domain).data

        self.sP = fsdata.get_data(FaceParameter.STATIC_PRESS)

        matname = self.desc.get_equation(eqname).mat

        self.fluid = self.desc.get_material(matname)

    def container_operator(self, group, key, container): pass

    def resize(self, nip):
        BC_Base.resize(self, nip)
        self.n.resize( (2, nip), refcheck=False )
        self.t.resize( (2, nip), refcheck=False )

        self.back_vn.resize( (nip,), refcheck=False )
        self.rhoe.resize( (nip,), refcheck=False )
        self.sound_speed.resize( (nip,), refcheck=False )

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)
        self.resize(nip)

        # Physical element (internal) reconstruction
        self.do_neighbour_reconstruction(ng, ng_face_id, self.fluid)

        ng.normals_tangents(ng_face_id, self.n, self.t)

        # back_Vn = uI*n(0) + vI*n(1)
        np.multiply(self.uI, self.n[0,:], self.back_vn)
        self.back_vn += self.vI*self.n[1,:]

        self.fluid.sonic_speed(self.rhoI, self.pI, self.sound_speed)

        self.fluid.energy(self.rhoI, self.uI, self.vI, self.sP, self.rhoe)

        back_energy = np.where(
            self.back_vn < self.sound_speed, self.rhoe, self.rhoeI)

        # Finally, the boundary condition imposition
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)

        np.copyto(rho,  self.rhoI )
        np.copyto(rhou, self.rhouI)
        np.copyto(rhov, self.rhovI)
        np.copyto(rhoe, back_energy)
    
# end of manticore.models.compressible.equations.bcs.backpressure_bc


class farfield_bc(EntityOperator, BC_Base):
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

        self.uF = 0.0
        self.vF = 0.0
        self.sP = 0.0
        self.sT = 0.0

        self.sound_speedI = np.zeros(0)
        self.riemannInf   = np.zeros(0)
        self.riemannE     = np.zeros(0)
        self.riemann_term = np.zeros(0)
        self.normalVelF   = np.zeros(0)
        self.sound_speedF = np.zeros(0)
        self.MachF        = np.zeros(0)
        self.dV           = np.zeros(0)
        
        self.riemannRho   = np.zeros(0)
        self.uR           = np.zeros(0)
        self.vR           = np.zeros(0)

        self.n = np.zeros(0)
        self.t = np.zeros(0)
 
        self.fluid       = None

    def setup(self, eqname, domain):
        
        BC_Base.setup(self, eqname, domain)

        # In the case of ghosts domainName corresponds to the name of
        # the original face set that gave birth to the ghost set.        
        fsdata = self.desc.get_faceset(domain).data

        self.uF = fsdata.get_data(FaceParameter.VELX)
        self.vF = fsdata.get_data(FaceParameter.VELY)
        self.sP = fsdata.get_data(FaceParameter.STATIC_PRESS)
        self.sT = fsdata.get_data(FaceParameter.STATIC_TEMP)

        matname = self.desc.get_equation(eqname).mat

        self.fluid = self.desc.get_material(matname)

    def container_operator(self, group, key, container): pass

    def resize(self, nip):
        BC_Base.resize(self, nip)
        self.n.resize( (2, nip), refcheck=False )
        self.t.resize( (2, nip), refcheck=False )

        self.sound_speedI.resize( (nip,), refcheck=False )
        self.riemannInf.resize( (nip,), refcheck=False )
        self.riemannE.resize( (nip,), refcheck=False )
        self.riemann_term.resize( (nip,), refcheck=False )
        self.normalVelF.resize( (nip,), refcheck=False )
        self.sound_speedF.resize( (nip,), refcheck=False )
        self.MachF.resize( (nip,), refcheck=False )
        self.dV.resize( (nip,), refcheck=False )
        
        self.riemannRho.resize( (nip,), refcheck=False )
        self.uR.resize( (nip,), refcheck=False ) 
        self.vR .resize( (nip,), refcheck=False )

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)
        self.resize(nip)

        #
        # STEP 1: Obtain information on the FarField
        #
        
        # Physical element (internal) reconstruction
        self.do_neighbour_reconstruction(ng, ng_face_id, self.fluid)

        ng.normals_tangents(ng_face_id, self.n, self.t)

        self.fluid.sonic_speed(self.rhoI, self.pI, self.sound_speedI)
        sound_speed_sT = self.fluid.equation.speed_of_sound_at_T(self.sT)

        #
        # STEP 2: Standard Riemann invariants
        #

        # Incomming Riemann
        np.multiply(self.uF, self.n[0,:], self.riemannInf)
        self.riemannInf += self.vF*self.n[1,:]
        self.riemannInf -= self.fluid.equation.riemann_inv_sos_term(
                                                                 sound_speed_sT)

        # Outgoing Riemann
        np.multiply(self.uI, self.n[0,:], self.riemannE)
        self.riemannE += self.vI*self.n[1,:]
        self.fluid.equation.riemann_inv_sos_term(
                                           self.sound_speedI, self.riemann_term)
        self.riemannE += self.riemann_term

        # Freestream boundary velocity component and sound speed
        np.add(self.riemannE, self.riemannInf, out=self.normalVelF)
        self.normalVelF *=0.5
        self.fluid.equation.speed_of_sound_from_riemann(
                              self.riemannE, self.riemannInf, self.sound_speedF)
        
        #
        # STEP 3: Laminar Riemann
        #

        # We must calculate several quantities based on velocity
        # ratios. This comparison must be made point to point

        np.divide(self.normalVelF, self.sound_speedF, self.MachF)
        np.fabs(self.MachF, self.MachF)

        # Indices for supersonic/subsonic
        supersonic = self.MachF > 1.0      
        subsonic   = np.invert(supersonic)
        super_idx  = np.where(supersonic)
        sub_idx    = np.where(subsonic)

        # Indices for supersonic outflow/inflow
        mask_out  = self.normalVelF[supersonic] > 0.0 # outflow
        mask_in   = np.invert(mask_out)               # inflow
        super_out = np.full((nip,), False, dtype=bool)
        super_in  = np.full((nip,), False, dtype=bool)
        super_out[super_idx] += mask_out # outflow boolean indices
        super_in[ super_idx] += mask_in  # inflow  boolean indices
        
        # Indices for subsonic outflow/inflow
        mask_out = self.normalVelF[subsonic] > 0.0     # outflow
        mask_in  = np.invert(mask_out)                 # inflow
        sub_out  = np.full((nip,), False, dtype=bool)
        sub_in   = np.full((nip,), False, dtype=bool)
        sub_out[sub_idx] += mask_out # outflow boolean indices
        sub_in[ sub_idx] += mask_in  # inflow  boolean indices

        # Note that a boolean index return a *copy*: it is necessary
        # to force in-place operations: slices from boolean indexing
        # cannot be changed inside functions!!!

        # Supersonic outflow
        self.riemannRho[super_out] = self.rhoI[super_out]
        self.uR[super_out] = self.uI[super_out]
        self.vR[super_out] = self.vI[super_out]

        # Supersonic inflow
        rhoF = self.fluid.equation.density(self.sP, self.sT)
        self.riemannRho[super_in] = rhoF
        self.uR[super_in] = self.uF
        self.vR[super_in] = self.vF

        # Subsonic outflow
        aux = np.zeros(np.where(sub_out)[0].shape[0])
        self.fluid.equation.density_isentropic_from_sonic_speed(
            self.rhoI[sub_out], self.pI[sub_out],
            self.sound_speedF[sub_out], aux)
        
        self.riemannRho[sub_out] = aux
        
        self.dV[sub_out]  = self.normalVelF[sub_out]
        self.dV[sub_out] -= self.uI[sub_out] * self.n[0,sub_out]
        self.dV[sub_out] -= self.vI[sub_out] * self.n[1,sub_out]
        
        self.uR[sub_out]  = self.dV[sub_out] * self.n[0,sub_out]
        self.vR[sub_out]  = self.dV[sub_out] * self.n[1,sub_out]
        self.uR[sub_out] += self.uI[sub_out]
        self.vR[sub_out] += self.vI[sub_out]
        
        # Subsonic inflow
        aux = np.zeros(np.where(sub_in)[0].shape[0])
        self.fluid.equation.density_isentropic_from_sonic_speed(
            rhoF, self.sP, self.sound_speedF[sub_in], aux)

        self.riemannRho[sub_in] = aux

        self.dV[sub_in]  = self.normalVelF[sub_in]
        self.dV[sub_in] -= self.uF * self.n[0,sub_in]
        self.dV[sub_in] -= self.vF * self.n[1,sub_in]

        self.uR[sub_in]  = self.dV[sub_in] * self.n[0,sub_in]
        self.vR[sub_in]  = self.dV[sub_in] * self.n[1,sub_in]
        self.uR[sub_in] += self.uF
        self.vR[sub_in] += self.vF

        # Finally, the boundary condition imposition
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)

        np.copyto(rho,  self.riemannRho)
        np.multiply(self.riemannRho, self.uR, out=rhou)
        np.multiply(self.riemannRho, self.vR, out=rhov)
        
        self.fluid.equation.pressure_from_speed_of_sound(
            self.riemannRho, self.sound_speedF, self.dV) # dV=p (aux vector)

        self.fluid.energy(self.riemannRho, self.uR, self.vR, self.dV, rhoe)

# end of manticore.models.compressible.equations.bcs.farfield_bc


class pressureinlet_bc(EntityOperator, BC_Base):
    
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

        self.cx = 0.0
        self.cy = 0.0
        self.sP = 0.0
        self.tP = 0.0
        self.tT = 0.0

        self.inward_n = np.zeros(0)
        self.inlet_vn = np.zeros(0)
        self.inlet_rho = np.zeros(0)
        self.inlet_energy = np.zeros(0)
        self.sound_speed = np.zeros(0)
        self.uR = np.zeros(0)
        self.vR = np.zeros(0)
        self.vt = np.zeros(0)

        self.n = np.zeros(0)
        self.t = np.zeros(0)
 
        self.fluid       = None

    def setup(self, eqname, domain):
        
        BC_Base.setup(self, eqname, domain)

        # In the case of ghosts domainName corresponds to the name of
        # the original face set that gave birth to the ghost set.        
        fsdata = self.desc.get_faceset(domain).data

        self.cx = fsdata.get_data(FaceParameter.DIRX)
        self.cy = fsdata.get_data(FaceParameter.DIRY)
        self.sP = fsdata.get_data(FaceParameter.STATIC_PRESS)
        self.tP = fsdata.get_data(FaceParameter.TOTAL_PRESS)
        self.tT = fsdata.get_data(FaceParameter.TOTAL_TEMP)

        matname = self.desc.get_equation(eqname).mat

        self.fluid = self.desc.get_material(matname)

    def container_operator(self, group, key, container): pass

    def resize(self, nip):
        BC_Base.resize(self, nip)
        self.n.resize( (2, nip), refcheck=False )
        self.t.resize( (2, nip), refcheck=False )

        self.inward_n.resize( (2, nip), refcheck=False )
        self.inlet_vn.resize( (nip,), refcheck=False )
        self.inlet_rho.resize( (nip,), refcheck=False )
        self.inlet_energy.resize( (nip,), refcheck=False )
        self.sound_speed.resize( (nip,), refcheck=False )
        self.uR.resize( (nip,), refcheck=False )
        self.vR.resize( (nip,), refcheck=False )
        self.vt.resize( (nip,), refcheck=False )

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)
        self.resize(nip)

        # Physical element (internal) reconstruction
        self.do_neighbour_reconstruction(ng, ng_face_id, self.fluid)

        ng.normals_tangents(ng_face_id, self.n, self.t)

        np.multiply(self.n, -1., out=self.inward_n)

        # inlet_vn = uI*inward_n(0) + vI*inward_n(1)
        np.multiply(self.uI, self.inward_n[0,:], self.inlet_vn)
        self.inlet_vn += self.vI*self.inward_n[1,:]

        self.fluid.sonic_speed(self.rhoI, self.pI, self.sound_speed)

        # Indices for supersonic/subsonic
        subsonic   = self.inlet_vn < self.sound_speed
        supersonic = np.invert(subsonic)

        #
        # if inlet_vn < sound_speed:
        #
        
        # vn_dir = inlet_dir.dot(inward_n)
        vn_dir = self.cx*self.inward_n[0,subsonic]
        vn_dir+= self.cy*self.inward_n[1,subsonic]

        self.vt[subsonic]  = self.cx*self.t[0,subsonic]
        self.vt[subsonic] += self.cy*self.t[1,subsonic]
        self.vt[subsonic] /= vn_dir
        self.vt[subsonic] *= self.inlet_vn[subsonic]

        self.uR[subsonic]  = self.inlet_vn[subsonic]*self.inward_n[0,subsonic]
        self.uR[subsonic] += self.vt[subsonic]*self.t[0,subsonic]

        self.vR[subsonic]  = self.inlet_vn[subsonic]*self.inward_n[1,subsonic]
        self.vR[subsonic] += self.vt[subsonic]*self.t[1,subsonic]

        vv  = self.uR[subsonic]*self.uR[subsonic]
        vv += self.vR[subsonic]*self.vR[subsonic]
        
        T_T0_ratio = np.zeros(vv.shape[0])
        self.fluid.equation.static_to_total_temperature_ratio(self.tT, vv,
                                                                  T_T0_ratio)
        T = self.tT * T_T0_ratio
        
        P = np.zeros(T.shape[0])
        self.fluid.equation.pressure_isentropic(self.tP, self.tT, T, P)
        
        RHO = np.zeros(T.shape[0])
        self.fluid.equation.density(P, T, RHO)
        
        iE = np.zeros(T.shape[0])
        self.fluid.thermo.internal_energy(T, iE)

        self.inlet_rho[subsonic]     = RHO
        self.inlet_energy[subsonic]  = vv
        self.inlet_energy[subsonic] *= 0.5
        self.inlet_energy[subsonic] += iE
        self.inlet_energy[subsonic] *= self.inlet_rho[subsonic]

        #
        # if inlet_vn >= sound_speed:
        #
        T0_T_ratio = self.fluid.equation.temperature_ratio_isentropic(
                                                               self.tP, self.sP)
        Te   = self.tT/T0_T_ratio
        a    = self.fluid.equation.speed_of_sound_at_T(Te)
        mach = self.fluid.equation.stagnation.mach(T0_T_ratio)
        vel  = mach * a

        self.uR[supersonic] = vel * self.cx
        self.vR[supersonic] = vel * self.cy

        vv  = self.uR[supersonic]*self.uR[supersonic]
        vv += self.vR[supersonic]*self.vR[supersonic]

        self.inlet_rho[supersonic] = self.fluid.equation.density(self.sP, Te)
        self.inlet_energy[supersonic]  = vv
        self.inlet_energy[supersonic] *= 0.5
        self.inlet_energy[supersonic] += self.fluid.thermo.internal_energy(Te)
        self.inlet_energy[supersonic] *= self.inlet_rho[supersonic]

        # Finally, the boundary condition imposition
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)

        np.add(self.rhoI, self.inlet_rho, out=rho)
        rho *= 0.5

        np.multiply(self.inlet_rho, self.uR, out=rhou)
        rhou += self.rhouI
        rhou *= 0.5

        np.multiply(self.inlet_rho, self.vR, out=rhov)
        rhov += self.rhovI
        rhov *= 0.5

        np.add(self.rhoeI, self.inlet_energy, out=rhoe)
        rhoe *= 0.5
        
        
# end of manticore.models.compressible.equations.bcs.pressureinlet_bc


class supersonicexit_bc(EntityOperator, BC_Base):
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

    def setup(self, eqname, domain):
        BC_Base.setup(self, eqname, domain)

    def container_operator(self, group, key, container): pass

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)

        # Finally, the boundary condition imposition
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)

        state = FieldRole.State
        ng.get_face_field(state, FieldVariable.RHO,  ng_face_id, rho)
        ng.get_face_field(state, FieldVariable.RHOU, ng_face_id, rhou)
        ng.get_face_field(state, FieldVariable.RHOV, ng_face_id, rhov)
        ng.get_face_field(state, FieldVariable.RHOE, ng_face_id, rhoe)

        if rho.min() < const.INF_VAR:
            msg = 'Facial reconstruction failed: '
            msg+= 'DENSITY is negative or too small!'
            msg+= '\nElement : {} - Face: {}'.format(ng.ID, ng_face_id)
            msg+= '\nRHO on face [I]: {}'.format(rho)
            raise AssertionError(msg)    

# end of manticore.models.compressible.equations.bcs.supersonicexit_bc


class symmetry_bc(EntityOperator, BC_Base):
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

        self.vProj = np.zeros(0)
        self.uR    = np.zeros(0)
        self.vR    = np.zeros(0)
        
        self.n           = np.zeros(0)
        self.t           = np.zeros(0)
        
        self.fluid       = None

    def setup(self, eqname, domain):
        
        BC_Base.setup(self, eqname, domain)

        matname = self.desc.get_equation(eqname).mat

        self.fluid = self.desc.get_material(matname)

    def container_operator(self, group, key, container): pass

    def resize(self, nip):
        BC_Base.resize(self, nip)
        self.n.resize( (2, nip), refcheck=False )
        self.t.resize( (2, nip), refcheck=False )

        self.vProj.resize( (nip,), refcheck=False )
        self.uR.resize( (nip,), refcheck=False )
        self.vR.resize( (nip,), refcheck=False )

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)
        self.resize(nip)

        # Physical element (internal) reconstruction
        self.do_neighbour_reconstruction(ng, ng_face_id, self.fluid)

        ng.normals_tangents(ng_face_id, self.n, self.t)

        # Projected velocity
        # velProj = <V, t>
        np.multiply(self.uI, self.t[0,:], self.vProj)
        self.vProj += self.vI*self.t[1,:]

        np.multiply(self.vProj, self.t[0,:], self.uR)
        np.multiply(self.vProj, self.t[1,:], self.vR)

        # Finally, the boundary condition imposition
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)

        np.copyto(rho, self.rhoI)
        np.multiply(self.rhoI, self.uR, rhou)
        np.multiply(self.rhoI, self.vR, rhov)
        np.copyto(rhoe, self.rhoeI)

# end of manticore.models.compressible.equations.bcs.symmetry_bc


class velocityinlet_bc(EntityOperator, BC_Base):
    
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

        self.cx = 0.0
        self.cy = 0.0
        self.sP = 0.0
        self.sT = 0.0
        self.vel= 0.0

        self.inward_n = np.zeros(0)
        self.inlet_vn = np.zeros(0)
        self.inlet_rho = np.zeros(0)
        self.sound_speed = np.zeros(0)

        self.n = np.zeros(0)
        self.t = np.zeros(0)
 
        self.fluid       = None

    def setup(self, eqname, domain):
        
        BC_Base.setup(self, eqname, domain)

        # In the case of ghosts domainName corresponds to the name of
        # the original face set that gave birth to the ghost set.        
        fsdata = self.desc.get_faceset(domain).data

        self.cx = fsdata.get_data(FaceParameter.DIRX)
        self.cy = fsdata.get_data(FaceParameter.DIRY)
        self.sP = fsdata.get_data(FaceParameter.STATIC_PRESS)
        self.sT = fsdata.get_data(FaceParameter.STATIC_TEMP)
        self.vel= fsdata.get_data(FaceParameter.VEL)

        matname = self.desc.get_equation(eqname).mat

        self.fluid = self.desc.get_material(matname)

    def container_operator(self, group, key, container): pass

    def resize(self, nip):
        BC_Base.resize(self, nip)
        self.n.resize( (2, nip), refcheck=False )
        self.t.resize( (2, nip), refcheck=False )

        self.inward_n.resize( (nip,), refcheck=False )
        self.inlet_vn.resize( (nip,), refcheck=False )
        self.inlet_rho.resize( (nip,), refcheck=False )
        self.sound_speed.resize( (nip,), refcheck=False )

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)
        self.resize(nip)

        # Physical element (internal) reconstruction
        self.do_neighbour_reconstruction(ng, ng_face_id, self.fluid)

        ng.normals_tangents(ng_face_id, self.n, self.t)

        # Inward normal
        np.multiply(self.n, -1., out=self.inward_n)

        # inlet_vn = uI*inward_n(0) + vI*inward_n(1)
        np.multiply(self.uI, self.inward_n[0,:], self.inlet_vn)
        self.inlet_vn += self.vI*self.inward_n[1,:]

        # Sound speed
        self.fluid.sonic_speed(self.rhoI, self.pI, self.sound_speed)

        iE = self.fluid.thermo.internal_energy(self.sT)
        tE = iE + 0.5*self.vel*self.vel

        # Indices for supersonic/subsonic
        subsonic   = self.inlet_vn < self.sound_speed
        supersonic = np.invert(subsonic)
        sub_idx    = np.where(subsonic)
        super_idx  = np.where(supersonic)

        sub_rho = np.zeros(sub_idx.shape[0])
        self.fluid.equation(self.pI, self.sT, sub_rho)

        self.inlet_rho[subsonic]   = sub_rho
        self.inlet_rho[supersonic] = self.fluid.equation(self.sP, self.sT)

        # Finally, the boundary condition imposition
        rho  = e.state(FieldType.ph, FieldVariable.RHO)
        rhou = e.state(FieldType.ph, FieldVariable.RHOU)
        rhov = e.state(FieldType.ph, FieldVariable.RHOV)
        rhoe = e.state(FieldType.ph, FieldVariable.RHOE)

        np.copyto(rho, self.inlet_rho)
        np.multiply(self.inlet_rho, self.vel*self.cx, rhou)
        np.multiply(self.inlet_rho, self.vel*self.cy, rhov)
        np.multiply(self.inlet_rho,               tE, rhoe)
    
# end of manticore.models.compressible.equations.bcs.velocityinlet_bc


class UpdateBC:
    """Update values on ghost elements."""

    def __init__(self, model_desc):
        self._desc      = model_desc
        self._bc_sym    = symmetry_bc(model_desc)
        self._bc_adwall = adiabaticwall_bc(model_desc)
        self._bc_far    = farfield_bc(model_desc)
        self._bc_pin    = pressureinlet_bc(model_desc)
        self._bc_backp  = backpressure_bc(model_desc)
        self._bc_super  = supersonicexit_bc(model_desc)
        self._bc_vin    = velocityinlet_bc(model_desc)
        self._bc_dirc   = dirichlet_cte_bc(model_desc)

        self._bc_selector = {
            FaceType.SYMMETRY:       self._bc_sym,
            FaceType.SMOOTHWALL:     self._bc_sym,
            FaceType.SUPERSONICEXIT: self._bc_super,
            FaceType.ADIABATICWALL:  self._bc_adwall,
            FaceType.FARFIELD:       self._bc_far,
            FaceType.PRESSUREINLET:  self._bc_pin,
            FaceType.BACKPRESSURE:   self._bc_backp,
            FaceType.VELOCITYINLET:  self._bc_vin,
            FaceType.DIRICHLET_CONS: self._bc_dirc,
            }

    def update(self, cm):

        # Access list of GHOST subdomains 
        subds = SubDomainRoleIterator(cm).find(EntityRole.GHOST)

        # The b.c's specified in self._bc_selector deal with RHO,
        # RHOU, RHOV, RHOE for the equations listed below:
        allowed_types = [EqType.EULER, EqType.NAVIERSTOKES, EqType.RANS,
                             EqType.LES, EqType.RANS_SA, EqType.RANS_SST]

        # Additional state variable will require additional
        # b.c. classes as well as addition lists of "allowed_types"
        # and additional "elif" conditional executions below.

        for s in subds:
            
            # Each ghost subdmain inherits the name of the respective
            # face set on the boundary associated to a boundary
            # condition. From the face set's name we access the
            # element set 'on the left' of the face set which is the
            # internal, physical set of elements adjacent to the face
            # set.
            lvs = self._desc.get_faceset(s.name).left
            eqs = self._desc.get_data(lvs).equations

            for eq in eqs:
                eqtype = self._desc.get_data(eq).type

                if eqtype in allowed_types:
                    self.apply_bc(s, eq)
                else:
                    raise RuntimeError('Equation not implemented!')

    def apply_bc(self, sudb, eqname):

        bctype = self._desc.get_faceset(sudb.name).data.type

        if bctype in self._bc_selector:
            
            bc = self._bc_selector[bctype]
            bc.setup(eqname, sudb.name)
            foreach_entity_in_subdomain(sudb, bc)
            
        elif bctype==FaceType.DIRICHLET:
            #
            # Specific procedures for generic Dirichlet must be put here.
            #
            if exec_dir==ExecDirective.STANDARD:
                raise RuntimeError('No functional Dirichlet BC is supported!')
            
            elif exec_dir==ExecDirective.NULL_DIRICHLET:
                
                bc = dirichlet_bc(null_dirichlet)
                bc.setup(eqname, sudb.name)
                foreach_entity_in_subdomain(sudb, bc)

            elif exec_dir==ExecDirective.WARBURTON_EX_6_1:

                # Get current global time
                time_glb = ModelTime()
                time     = time_glb.get()

                # Get material properties
                matname = self._desc.get_equation(eqname).mat
                fluid   = self._desc.get_material(matname)
                g       = fluid.thermo.gamma

                # Get analytical expression
                f = example_warburton_book_6p1(
                    x0=5., y0=0., beta=5., gamma=g,t=time)

                # Apply boundary conditions
                bc = dirichlet_bc(f)
                bc.setup(eqname, sudb.name)
                foreach_entity_in_subdomain(sudb, bc)

            elif exec_dir==ExecDirective.CTE_EULER_FIELDS:

                bc = dirichlet_bc(constant_euler_fields)
                bc.setup(eqname, sudb.name)
                foreach_entity_in_subdomain(sudb, bc)

            elif exec_dir==ExecDirective.NS_VALIDATION:

                # Get current global time
                time_glb = ModelTime()
                time     = time_glb.get()

                # Get material properties
                matname = self._desc.get_equation(eqname).mat
                fluid   = self._desc.get_material(matname)
                g       = fluid.thermo.gamma
                cp      = fluid.thermo.Cp
                cv      = fluid.thermo.Cv
                mu      = fluid.transport.mu
                Pr      = fluid.transport.Pr
                k       = mu*cp/Pr

                # Get analytical expression
                f = ns_validation(g, mu, k, cv)

                # Apply boundary conditions
                bc = dirichlet_bc(f)
                bc.setup(eqname, sudb.name)
                foreach_entity_in_subdomain(sudb, bc)
                                
            else:
                raise RuntimeError('BC not implemented!')
            
        else:
            raise RuntimeError('BC not implemented!')

        
    def apply_bc_on_element(self, subd, eqname, e):

        bctype = self._desc.get_faceset(subd.name).data.type
        k = e.key
        c = subd(k)

        if bctype in self._bc_selector:
            
            bc = self._bc_selector[bctype]
            bc.setup(eqname, subd.name)
            bc.container_operator(subd, k.key, c)
            bc(e)
            
        elif bctype==FaceType.DIRICHLET:
            #
            # Specific procedures for generic Dirichlet must be put here.
            #
            if exec_dir==ExecDirective.STANDARD:
                raise RuntimeError('No functional Dirichlet BC is supported!')
            
            elif exec_dir==ExecDirective.NULL_DIRICHLET:
                
                bc = dirichlet_bc(null_dirichlet)
                bc.setup(eqname, subd.name)
                bc.container_operator(subd, k.key, c)
                bc(e)

            elif exec_dir==ExecDirective.WARBURTON_EX_6_1:

                # Get current global time
                time_glb = ModelTime()
                time     = time_glb.get()

                # Get material properties
                matname = self._desc.get_equation(eqname).mat
                fluid   = self._desc.get_material(matname)
                g       = fluid.thermo.gamma

                # Get analytical expression
                f = example_warburton_book_6p1(
                    x0=5., y0=0., beta=5., gamma=g,t=time)

                # Apply boundary conditions
                bc = dirichlet_bc(f)
                bc.setup(eqname, subd.name)
                bc.container_operator(subd, k.key, c)
                bc(e)

            elif exec_dir==ExecDirective.CTE_EULER_FIELDS:

                bc = dirichlet_bc(constant_euler_fields)
                bc.setup(eqname, subd.name)
                bc.container_operator(subd, k.key, c)
                bc(e)

            elif exec_dir==ExecDirective.NS_VALIDATION:

                # Get current global time
                time_glb = ModelTime()
                time     = time_glb.get()

                # Get material properties
                matname = self._desc.get_equation(eqname).mat
                fluid   = self._desc.get_material(matname)
                g       = fluid.thermo.gamma
                cp      = fluid.thermo.Cp
                cv      = fluid.thermo.Cv
                mu      = fluid.transport.mu
                Pr      = fluid.transport.Pr
                k       = mu*cp/Pr

                # Get analytical expression
                f = ns_validation(g, mu, k, cv)

                # Apply boundary conditions
                bc = dirichlet_bc(f)
                bc.setup(eqname, subd.name)
                bc.container_operator(subd, k.key, c)
                bc(e)
                                
            else:
                raise RuntimeError('BC not implemented!')
            
        else:
            raise RuntimeError('BC not implemented!')        

        
    def synchronize(self, cm): pass
        
