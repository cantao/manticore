#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/models/compressible/tests/example_ns_00.py -ll DEBUG
# or
#
# $ python3 -m cProfile -o prof manticore/models/compressible/tests/example_ns_00.py
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
##
## Execution directives
##
from manticore.models.compressible.directives import ExecDirective
manticore.models.compressible.directive = ExecDirective.process('NS_VALIDATION')
##
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.tests.simple_meshes import mesh02
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldType
from manticore.lops.modal_dg.entity import GlobalRoleIterator
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.lops.modal_dg.entityops import (
    ComputeEntityVolume, ComputeEntityMassMatrix,
    InitializeFields, InitFieldValues, InitConstantFieldValues )
from manticore.models.compressible.timeint.generic import (
    BackwardTransformation, CharacteristicLength, ConstantTimeStep,
    CheckError, WriteFields )
from manticore.models.compressible.tests.test_helpers import (
    ns_validation, simple_model_03 )
from manticore.models.compressible.services.datatypes import (
    ModelTime )
from manticore.models.compressible.equations.manager import EquationManager
from manticore.models.compressible.timeint.integrator import SSP_5_4_RK
from collections import OrderedDict
from math import sqrt

def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example Laminar NS 00")

    cm = mesh02(n=8, p_order=1)

    for s in cm:
        logger.debug("Subdomain: %s Name: %s Role: %s" % (s.ID, s.name, s.role))

    # Lists for iterating over physical elements
    elist = GlobalRoleIterator(cm.container).find(EntityRole.PHYSICAL)
    subds = SubDomainRoleIterator(cm).find(EntityRole.PHYSICAL)

    # Write ascii file for post-processing
    filename = 'square{}.txt'.format(len(elist))
    myfile=open(filename,'w')
    for s in subds:
        s.write_to_txt(myfile)
    myfile.close()
    #

    logger.info(
        "Evaluating jacobians of %s elements..." % len(elist))
    for e in elist:
        e.eval_jacobian()

    # It seems more efficient to compute the mass matrix this way
    # instead of through the element's specific method.
    logger.info("Evaluating mass matrices...")
    mass_op = ComputeEntityMassMatrix()
    for s in subds:            
        foreach_entity_in_subdomain(s, mass_op)

    logger.info("Evaluating physical elements' volumes...")
    vol_op = ComputeEntityVolume()
    for s in subds:            
        foreach_entity_in_subdomain(s, vol_op)

    # Model description
    model_desc = simple_model_03()

    # Initialize the equation to be solved
    eqs = EquationManager()
    eqs.set_equations(model_desc)
    
    # Initializing fields storage
    logger.info('Initializing fields storage...')
    stt = eqs[0].state_variables
    rsd = eqs[0].residual_variables
    cte = [FieldVariable.CHAR_LENGTH, FieldVariable.DELTAT]
    init_fields = InitializeFields(stt, rsd, cte, auxiliary=rsd)
    for s in subds: 
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    gsubds = SubDomainRoleIterator(cm).find(EntityRole.GHOST)
    init_fields = InitializeFields(stt, rsd)
    for s in gsubds: 
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    # Initial condition
    time_settings = model_desc.get_time_settings()
    time_init     = time_settings.time_init
    time_glb      = ModelTime()
    time_glb.set(time_init)
    
    fluid = model_desc.get_material('Air')
    g  = fluid.thermo.gamma
    mu = fluid.transport.mu
    cv = fluid.thermo.Cv
    Pr = fluid.transport.Pr
    k  = mu*g*cv/Pr
    
    func = ns_validation(g, mu, k, cv)

    init_rho_values  = InitFieldValues(FieldVariable.RHO,  func.rho)
    init_rhou_values = InitFieldValues(FieldVariable.RHOU, func.rhou)
    init_rhov_values = InitFieldValues(FieldVariable.RHOV, func.rhov)
    init_rhoe_values = InitFieldValues(FieldVariable.RHOE, func.rhoe)
    init_mu_values   = InitConstantFieldValues(FieldVariable.MU, mu)

    logger.info('Initializing fiels values from analytical expression...')
    for s in subds: 
        logger.debug('Initializing RHO values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rho_values)
        logger.debug('Initializing RHOU values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhou_values)
        logger.debug('Initializing RHOV values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhov_values)
        logger.debug('Initializing RHOE values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhoe_values)
        logger.debug('Initializing MU values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_mu_values)

    logger.info("Evaluating characteristic lengths...")
    cl_op = CharacteristicLength()
    for s in subds:            
        foreach_entity_in_subdomain(s, cl_op)

    logger.info('Setting time setps...')
    cte_dt = ConstantTimeStep(model_desc.get_time_settings().deltat)
    for s in subds:
        foreach_entity_in_subdomain(s, cte_dt)

    t_now = time_settings.time_init
    dt    = time_settings.deltat
    t_max = time_settings.time_max
    t_fsv = time_settings.time_save

    tints = OrderedDict()
    for eq_name, eq in eqs:
        tints[eq_name] = SSP_5_4_RK(model_desc, eq)

    logger.info('MAIN TIME-INTEGRATION LOOP...')
    while (t_now < t_max):
        logger.info('INSTANT %s...' % (t_now))

        logger.debug('Computing the residual of the current state...')
        for eq_name, eq in eqs:
            eq.compute_residual(model_desc, cm)
        
        for eq_name in tints:
            logger.debug('Time step in equation %s...' % (eq_name))
            tints[eq_name].update_state(cm)

        t_now += dt
        time_glb.set(t_now)

    logger.info('Checking error...')
    check_error = CheckError(func, stt)
    for s in subds:            
        foreach_entity_in_subdomain(s, check_error)

    logger.info('Discrete l2:')
    for v in stt:
        logger.info('Error in %s = %s' % (v, sqrt(check_error.l2_norm[v])))

    logger.info('Continuous l2:')
    for v in stt:
        logger.info('Error in %s = %s' % (v, sqrt(check_error.l2_cont[v])))


    logger.info('Writing response fields...')
    # Write ascii file for post-processing
    e=cm.get_entity(0)
    order = e.key.order
    filename = 'square{}-p{}-results.txt'.format(len(elist),order)
    myfile=open(filename,'w')
    
    write_fields = WriteFields(myfile)
    for s in subds:            
        foreach_entity_in_subdomain(s, write_fields)
        
    myfile.close()
    #

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='NS validation')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')

    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
                }
            },
            'handlers': {
                'console': {
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                },
                'file':{
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': 'example_ns_00.log',
                    'mode': 'a',
                    'maxBytes': 1048576,
                    'backupCount': 3,
                },
            },
            'loggers': {
                '': {
                    'handlers': ['file', 'console'],
                    'level': args.loglevel,
                },
            }
        }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
