﻿#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
from manticore.services.fieldvariables import FieldVariable
from manticore.mdf.enums import MapType
from manticore.models.compressible.description.reference import ReferenceData
from manticore.models.compressible.description.mdtypes import (
    FaceType, EqType, TISetting, FaceParameter, EqParameter, EqSetting)
from manticore.models.compressible.description.model import ModelDescription
from manticore.models.compressible.description.materials import Fluid

class example_warburton_book_6p1:
    """Example 6.1 page 209. Hesthaven, Warburton, Nodal Discontinuous
    Galerkin.

    Exact solution of the isentropic vortex test case.
    """

    def __init__(self, x0, y0, beta, gamma, t):
        self.x0    = x0
        self.y0    = y0
        self.beta  = beta
        self.gamma = gamma
        self.t     = t

    def r2(self, x, y):
        x0    = self.x0
        y0    = self.y0
        t     = self.t

        return np.power(x-t-x0, 2.) + np.power(y-y0, 2.)

    def rho(self, x, y):
        beta  = self.beta
        gamma = self.gamma

        r2   = self.r2(x, y)
        _rho = np.power(1.-((gamma-1)/(16.*gamma*np.pi**2))*beta**2*np.power(
            np.exp(1.-r2),2.), 1./(gamma-1.))

        ##
        #_rho-=0.362
        ##

        return _rho

    def u(self, x, y):
        x0    = self.x0
        y0    = self.y0
        beta  = self.beta

        r2   = self.r2(x, y)
        _u   = 1.-beta*np.exp(1.-r2)*(y-y0)/(2.*np.pi)

        return _u

    def v(self, x, y):
        x0    = self.x0
        y0    = self.y0
        beta  = self.beta

        r2   = self.r2(x, y)
        _v   = beta*np.exp(1.-r2)*(x-x0)/(2.*np.pi)

        return _v

    def p(self, x, y):
        gamma = self.gamma

        rho = self.rho(x, y)
        _p  = np.power(rho, gamma)

        return _p

    def rhou(self, x, y):
        return self.rho(x, y)*self.u(x, y)

    def rhov(self, x, y):
        return self.rho(x, y)*self.v(x, y)

    def rhoe(self, x, y):
        gamma = self.gamma

        u   = self.u(x, y)
        v   = self.v(x, y)
        rho = self.rho(x, y)
        p   = np.power(rho, gamma)

        _rhoe = p/(gamma-1)+0.5*rho*(u*u+v*v)

        return _rhoe


class constant_euler_fields:

    def rho(x, y):
        return np.ones(x.shape[0])

    def rhou(x, y):
        return np.ones(x.shape[0])

    def rhov(x, y):
        return np.zeros(x.shape[0])

    def rhoe(x, y):
        return 3*np.ones(x.shape[0])


class ns_validation:
    """Manufactured solution for NS validation. See

    manticore/jupyter/ns_validation.ipynb
    """

    def __init__(self, gamma, mu, k, cv):

        self.gamma = gamma
        self.mu    = mu
        self.k     = k
        self.cv    = cv

    def T(self, x, y):
        return x + y**2 + 10.

    def rho(self, x, y):
        return np.ones(x.shape[0])

    def rhou(self, x, y):
        return y**2

    def rhov(self, x, y):
        return np.zeros(x.shape[0])

    def rhoe(self, x, y):
        rho  = self.rho(x, y)
        rhou = self.rhou(x, y)
        rhov = self.rhov(x, y)

        u  = rhou/rho
        v  = rhov/rho
        T  = self.T(x, y)

        ei = self.cv*T

        _rhoe = rho*(ei+(u**2+v**2)/2.)

        return _rhoe

    def src_rho(self, x, y):
        return np.zeros(x.shape[0])

    def src_rhou(self, x, y):
        return (self.cv*(self.gamma-1.)-2.*self.mu)*np.ones(x.shape[0])

    def src_rhov(self, x, y):
        return 2.*(self.cv*(self.gamma-1.))*y

    def src_rhoe(self, x, y):
        C  = self.cv
        g  = self.gamma
        mu = self.mu
        k  = self.k

        return (C*(g-1.)+C-6.*mu)*y**2 - 2.*k


def simple_model_00():
    """Just a generic model description for testing basic functions. Not
    a actual setup of a specific problem.
    """

    species = {'MOLARMASS': 28.9686e-03, 'NMOLES': 1}
    thermo = {'TYPE': 'CONSTANTCP', 'CP': 1004.5}
    transport = {'TYPE': 'CONSTANT', 'MU': 1.79e-05, 'PRANDTL': 0.72}
    equation = {'TYPE': 'IDEALGAS', 'GASCTE': 8.314472}
    mat = ['Air']
    meshfile = "flatplatehex200_o1.mdf"
    initfile = "flatplatehex200_o1-init.mdf"
    resufile = "flatplatehex200_o1-01-res.mdf"
    restfile = "flatplatehex200_o1-01-restart.mdf"
    refData = ReferenceData(0.0241, 59.01, 1.0, 1.79e-05, 216.66)
    ref = refData.get_data()
    eqname = ['inviscid']
    eqtype = [EqType.EULER]
    eqs = dict(zip(eqname, eqtype))
    var = [
        FieldVariable.RHO, FieldVariable.RHOU, FieldVariable.RHOV,
        FieldVariable.RHOE
    ]
    stdlim = {'min': 1.0e4, 'max': 1.0e15}
    conlim = {'min': 1.0e-12, 'max': 1.0e15}
    subd = ['FLUID']
    fname = ["FARFIELD", "WALL", "P_INT", "EXIT"]
    ftype = [ FaceType.FARFIELD, FaceType.ADIABATICWALL,
              FaceType.PRESSUREINLET, FaceType.BACKPRESSURE ]
    fset = dict(zip(fname, ftype))
    fparam = dict.fromkeys(ftype)

    time = {
        'tacc': TISetting.TACC_STEADYSTATE,
        'it_max': 20000,
        'it_save': 500,
        'stop': TISetting.STOPCRITERIA_MAX,
        'type': TISetting.TSTEP_VARIABLE,
        'cfl_type': TISetting.CFL_CONSTANT,
        'cfl': 0.2,
        'tint': TISetting.INTEGRATOR_SSP54RK
    }

    model = ModelDescription()

    # Set input/output files through Tree's interface
    model.data.get_node('MESH').data    = meshfile
    model.data.get_node('INITIAL').data = initfile
    model.data.get_node('RESULTS').data = resufile
    model.data.get_node('RESTART').data = restfile
    model.data.get_node('INITIAL_TYPE').data = MapType.HIERARCHICAL

    # Set reference data
    model.set_reference(ref['rho'],ref['vel'],ref['len'], ref['mu'],ref['temp'])

    # Set material

    # Insert Node with ID mat[0] as MATERIALS's
    # child using Tree's interface
    model.init_material( mat[0], Fluid(species, thermo, transport, equation) )

    # Nondimensionalization of material data:
    # Accessing ReferenceData's interface do get reference data
    ref = model.get_reference_data()

    # Accessing Fluid's interface through Tree's interface
    model.data.get_node( mat[0]).data.nondimensionalization(
                ref['rho'], ref['vel'], ref['len'], ref['mu'], ref['temp'])

    # Setting equation: (name, type)
    model.init_equation(eqname[0], eqtype[0])

    # Getting a reference to a specific equation description
    # stored as a Node object's data:
    eq = model.data.get_node(eqname[0]).data

    # Setting limits on state variables
    for v in var:
        eq.set_limits(v, stdlim['min'], stdlim['max'])

    eq.set_limits(var[0], conlim['min'], conlim['max'])

    # Setting the material associated to this equation
    eq.mat = mat[0]

    #eq.set_data(EqParameter.CONVECTIVE_FLUX,  EqSetting.CONVECTIVE_ROE)
    eq.set_data(EqParameter.CONVECTIVE_FLUX,  EqSetting.CONVECTIVE_HLLC)
    eq.set_data(EqParameter.PRINCIPAL_DGFORM, EqSetting.WEAKFORM)
    eq.set_data(EqParameter.DISSIPATION,      EqSetting.EQSET_NONE)
    eq.set_data(EqParameter.DISSIPATIVE_FLUX, EqSetting.EQSET_NONE)
    eq.set_data(EqParameter.DAUXILIAR_DGFORM, EqSetting.EQSET_NONE)

    #Set domain
    # Create sudomains
    for s in subd:
        model.init_elementset(s)

    # Set data of one subdomain
    sd = model.get_elementset(subd[0])

    sd.add_equation(eqname[0])

    for f in fset.keys():  # face sets
        sd.add_faceset(f)

    # Create boundaries
    for f in fset:
        model.init_faceset(f, fset[f])

    fs = model.get_facesets()

    for f_node in fs:

        f = f_node.data # access tree's node contents

        f.set_neighbours(subd[0])

        if f.data.type == FaceType.FARFIELD:
            f.data.set_dim_data(FaceParameter.VELX, 59.01)
            f.data.set_dim_data(FaceParameter.VELY, 0.0)
            f.data.set_dim_data(FaceParameter.STATIC_PRESS, 1498.66)
            f.data.set_dim_data(FaceParameter.STATIC_TEMP, 216.66)
        elif f.data.type == FaceType.BACKPRESSURE:
            f.data.set_dim_data(FaceParameter.STATIC_PRESS, 1498.66)
        elif f.data.type == FaceType.ADIABATICWALL:
            f.data.set_dim_data(FaceParameter.VELX, 59.01)
            f.data.set_dim_data(FaceParameter.VELY, 0.0)
        elif f.data.type == FaceType.PRESSUREINLET:
            f.data.set_dim_data(FaceParameter.DIRX, 1.0)
            f.data.set_dim_data(FaceParameter.DIRY, 1.0)
            f.data.set_dim_data(FaceParameter.TOTAL_PRESS, 1700.)
            f.data.set_dim_data(FaceParameter.TOTAL_TEMP, 300.)
            f.data.set_dim_data(FaceParameter.STATIC_PRESS, 1500.)

        f.data.nondimensionalization(ref['rho'], ref['vel'], ref['len'],
                                         ref['mu'], ref['temp'])

    model.init_time_settings(time['tacc'])
    time_settings = model.get_time_settings()
    time_settings.it_max = time['it_max']
    time_settings.it_save = time['it_save']
    time_settings.stop_criteria = time['stop']
    time_settings.tstep.set_type(time['type'])
    time_settings.tstep.set_cfl_type(time['cfl_type'])
    time_settings.tstep.set_cfl(time['cfl'])

    model.init_time_integration(eqname[0], time['tint'])

    return model

def simple_model_01():
    """Model description for executing a transient problem with
    dirichlet boundary conditions such as example_warburton_book_6p1.
    """

    species = {'MOLARMASS': 28.9686e-03, 'NMOLES': 1}
    thermo = {'TYPE': 'CONSTANTCP', 'CP': 1004.5}
    transport = {'TYPE': 'CONSTANT', 'MU': 1.79e-05, 'PRANDTL': 0.72}
    equation = {'TYPE': 'IDEALGAS', 'GASCTE': 8.314472}
    mat = ['Air']
    meshfile = "no_file_required.mdf"
    initfile = "no_file_required-init.mdf"
    resufile = "no_file_required-res.mdf"
    restfile = "no_file_required-restart.mdf"
    refData = ReferenceData(1.0, 1.0, 1.0, 1.0, 1.0)
    ref = refData.get_data()
    eqname = ['inviscid']
    eqtype = [EqType.EULER]
    eqs = dict(zip(eqname, eqtype))
    var = [
        FieldVariable.RHO, FieldVariable.RHOU, FieldVariable.RHOV,
        FieldVariable.RHOE
    ]
    stdlim = {'min': 1.0e4, 'max': 1.0e15}
    conlim = {'min': 1.0e-12, 'max': 1.0e15}
    subd = ['FLUID']
    fname = ["FACE_0", "FACE_1", "FACE_2", "FACE_3"]
    ftype = [ FaceType.DIRICHLET, FaceType.DIRICHLET,
              FaceType.DIRICHLET, FaceType.DIRICHLET ]
    fset = dict(zip(fname, ftype))
    fparam = dict.fromkeys(ftype)

    time = {
        'tacc': TISetting.TACC_TRANSIENT,
        't_init': 0.0,
        't_max' : 1.0,
        't_save': 2.0,
        'dt'    : 0.05,
        'tint'  : TISetting.INTEGRATOR_SSP54RK
    }

    model = ModelDescription()

    # Set input/output files through Tree's interface
    model.data.get_node('MESH').data    = meshfile
    model.data.get_node('INITIAL').data = initfile
    model.data.get_node('RESULTS').data = resufile
    model.data.get_node('RESTART').data = restfile
    model.data.get_node('INITIAL_TYPE').data = MapType.PER_INTP

    # Set reference data
    model.set_reference(ref['rho'],ref['vel'],ref['len'], ref['mu'],ref['temp'])

    # Set material

    # Insert Node with ID mat[0] as MATERIALS's
    # child using Tree's interface
    model.init_material( mat[0], Fluid(species, thermo, transport, equation) )

    # Nondimensionalization of material data:
    # Accessing ReferenceData's interface do get reference data
    ref = model.get_reference_data()

    # Accessing Fluid's interface through Tree's interface
    model.data.get_node( mat[0]).data.nondimensionalization(
                ref['rho'], ref['vel'], ref['len'], ref['mu'], ref['temp'])

    # Setting equation: (name, type)
    model.init_equation(eqname[0], eqtype[0])

    # Getting a reference to a specific equation description
    # stored as a Node object's data:
    eq = model.data.get_node(eqname[0]).data

    # Setting limits on state variables
    for v in var:
        eq.set_limits(v, stdlim['min'], stdlim['max'])

    eq.set_limits(var[0], conlim['min'], conlim['max'])

    # Setting the material associated to this equation
    eq.mat = mat[0]

    eq.set_data(EqParameter.CONVECTIVE_FLUX,  EqSetting.CONVECTIVE_ROE)
    eq.set_data(EqParameter.PRINCIPAL_DGFORM, EqSetting.WEAKFORM)
    eq.set_data(EqParameter.DISSIPATION,      EqSetting.EQSET_NONE)
    eq.set_data(EqParameter.DISSIPATIVE_FLUX, EqSetting.EQSET_NONE)
    eq.set_data(EqParameter.DAUXILIAR_DGFORM, EqSetting.EQSET_NONE)

    #Set domain
    # Create sudomains
    for s in subd:
        model.init_elementset(s)

    # Set data of one subdomain
    sd = model.get_elementset(subd[0])

    sd.add_equation(eqname[0])

    for f in fset.keys():  # face sets
        sd.add_faceset(f)

    # Create boundaries
    for f in fset:
        model.init_faceset(f, fset[f])

    fs = model.get_facesets()

    for f_node in fs:
        f = f_node.data # access tree's node contents
        f.set_neighbours(subd[0])

    model.init_time_settings(time['tacc'])
    time_settings = model.get_time_settings()
    time_settings.time_init = time['t_init']
    time_settings.time_max  = time['t_max']
    time_settings.time_save = time['t_save']
    time_settings.deltat    = time['dt']

    time_settings.nondimensionalization(
        ref['rho'], ref['vel'], ref['len'], ref['mu'], ref['temp'])

    model.init_time_integration(eqname[0], time['tint'])

    return model

def simple_model_02():
    """Model description for executing a transient problem with
    dirichlet boundary conditions such as example_warburton_book_6p1.
    """

    species = {'MOLARMASS': 28.9686e-03, 'NMOLES': 1}
    thermo = {'TYPE': 'CONSTANTCP', 'CP': 1004.5}
    transport = {'TYPE': 'CONSTANT', 'MU': 1.79e-05, 'PRANDTL': 0.72}
    equation = {'TYPE': 'IDEALGAS', 'GASCTE': 8.314472}
    mat = ['Air']
    meshfile = "no_file_required.mdf"
    initfile = "no_file_required-init.mdf"
    resufile = "no_file_required-res.mdf"
    restfile = "no_file_required-restart.mdf"
    refData = ReferenceData(1.0, 1.0, 1.0, 1.0, 1.0)
    ref = refData.get_data()
    eqname = ['inviscid']
    eqtype = [EqType.EULER]
    eqs = dict(zip(eqname, eqtype))
    var = [
        FieldVariable.RHO, FieldVariable.RHOU, FieldVariable.RHOV,
        FieldVariable.RHOE
    ]
    stdlim = {'min': 1.0e4, 'max': 1.0e15}
    conlim = {'min': 1.0e-12, 'max': 1.0e15}
    subd = ['FLUID']
    fname = ["FACE_0", "FACE_1", "FACE_2", "FACE_3"]
    ftype = [ FaceType.DIRICHLET, FaceType.DIRICHLET,
              FaceType.DIRICHLET, FaceType.DIRICHLET ]
    fset = dict(zip(fname, ftype))
    fparam = dict.fromkeys(ftype)

    time = {
        'tacc': TISetting.TACC_TRANSIENT,
        't_init': 0.0,
        't_max' : 1.0,
        't_save': 0.1,
        'dt'    : 0.01,
        'tint'  : TISetting.INTEGRATOR_SSP54RK
    }

    model = ModelDescription()

    # Set input/output files through Tree's interface
    model.data.get_node('MESH').data    = meshfile
    model.data.get_node('INITIAL').data = initfile
    model.data.get_node('RESULTS').data = resufile
    model.data.get_node('RESTART').data = restfile
    model.data.get_node('INITIAL_TYPE').data = MapType.PER_INTP

    # Set reference data
    model.set_reference(ref['rho'],ref['vel'],ref['len'], ref['mu'],ref['temp'])

    # Set material

    # Insert Node with ID mat[0] as MATERIALS's
    # child using Tree's interface
    model.init_material( mat[0], Fluid(species, thermo, transport, equation) )

    # Nondimensionalization of material data:
    # Accessing ReferenceData's interface do get reference data
    ref = model.get_reference_data()

    # Accessing Fluid's interface through Tree's interface
    model.data.get_node( mat[0]).data.nondimensionalization(
                ref['rho'], ref['vel'], ref['len'], ref['mu'], ref['temp'])

    # Setting equation: (name, type)
    model.init_equation(eqname[0], eqtype[0])

    # Getting a reference to a specific equation description
    # stored as a Node object's data:
    eq = model.data.get_node(eqname[0]).data

    # Setting limits on state variables
    for v in var:
        eq.set_limits(v, stdlim['min'], stdlim['max'])

    eq.set_limits(var[0], conlim['min'], conlim['max'])

    # Setting the material associated to this equation
    eq.mat = mat[0]

    eq.set_data(EqParameter.CONVECTIVE_FLUX,  EqSetting.CONVECTIVE_HLLC)
    eq.set_data(EqParameter.PRINCIPAL_DGFORM, EqSetting.WEAKFORM)
    eq.set_data(EqParameter.DISSIPATION,      EqSetting.EQSET_NONE)
    eq.set_data(EqParameter.DISSIPATIVE_FLUX, EqSetting.EQSET_NONE)
    eq.set_data(EqParameter.DAUXILIAR_DGFORM, EqSetting.EQSET_NONE)

    #Set domain
    # Create sudomains
    for s in subd:
        model.init_elementset(s)

    # Set data of one subdomain
    sd = model.get_elementset(subd[0])

    sd.add_equation(eqname[0])

    for f in fset.keys():  # face sets
        sd.add_faceset(f)

    # Create boundaries
    for f in fset:
        model.init_faceset(f, fset[f])

    fs = model.get_facesets()

    for f_node in fs:
        f = f_node.data # access tree's node contents
        f.set_neighbours(subd[0])

    model.init_time_settings(time['tacc'])
    time_settings = model.get_time_settings()
    time_settings.time_init = time['t_init']
    time_settings.time_max  = time['t_max']
    time_settings.time_save = time['t_save']
    time_settings.deltat    = time['dt']

    time_settings.nondimensionalization(
        ref['rho'], ref['vel'], ref['len'], ref['mu'], ref['temp'])

    model.init_time_integration(eqname[0], time['tint'])

    return model


def simple_model_03():
    """Model description for executing a transient problem with
    dirichlet boundary conditions ns_validation.
    """

    species = {'MOLARMASS': 1., 'NMOLES': 1}
    thermo = {'TYPE': 'CONSTANTCP', 'CP': 15.}
    transport = {'TYPE': 'CONSTANT', 'MU': 1.0e-02, 'PRANDTL': 0.15}
    equation = {'TYPE': 'IDEALGAS', 'GASCTE': 5.}
    mat = ['Air']
    meshfile = "no_file_required.mdf"
    initfile = "no_file_required-init.mdf"
    resufile = "no_file_required-res.mdf"
    restfile = "no_file_required-restart.mdf"
    refData = ReferenceData(1.0, 1.0, 1.0, 1.0, 1.0)
    ref = refData.get_data()
    eqname = ['laminar']
    eqtype = [EqType.NAVIERSTOKES]
    eqs = dict(zip(eqname, eqtype))
    var = [
        FieldVariable.RHO, FieldVariable.RHOU, FieldVariable.RHOV,
        FieldVariable.RHOE
    ]
    stdlim = {'min': 1.0e4, 'max': 1.0e15}
    conlim = {'min': 1.0e-12, 'max': 1.0e15}
    subd = ['FLUID']
    fname = ["FACE_0", "FACE_1", "FACE_2", "FACE_3"]
    ftype = [ FaceType.DIRICHLET, FaceType.DIRICHLET,
              FaceType.DIRICHLET, FaceType.DIRICHLET ]
    fset = dict(zip(fname, ftype))
    fparam = dict.fromkeys(ftype)

    time = {
        'tacc': TISetting.TACC_TRANSIENT,
        't_init': 0.0,
        't_max' : 0.01,
        't_save': 0.1,
        'dt'    : 0.01,
        'tint'  : TISetting.INTEGRATOR_SSP54RK
    }

    model = ModelDescription()

    # Set input/output files through Tree's interface
    model.data.get_node('MESH').data    = meshfile
    model.data.get_node('INITIAL').data = initfile
    model.data.get_node('RESULTS').data = resufile
    model.data.get_node('RESTART').data = restfile
    model.data.get_node('INITIAL_TYPE').data = MapType.PER_INTP

    # Set reference data
    model.set_reference(ref['rho'],ref['vel'],ref['len'], ref['mu'],ref['temp'])

    # Set material

    # Insert Node with ID mat[0] as MATERIALS's
    # child using Tree's interface
    model.init_material( mat[0], Fluid(species, thermo, transport, equation) )

    # Nondimensionalization of material data:
    # Accessing ReferenceData's interface do get reference data
    ref = model.get_reference_data()

    # Accessing Fluid's interface through Tree's interface
    model.data.get_node( mat[0]).data.nondimensionalization(
                ref['rho'], ref['vel'], ref['len'], ref['mu'], ref['temp'])

    # Setting equation: (name, type)
    model.init_equation(eqname[0], eqtype[0])

    # Getting a reference to a specific equation description
    # stored as a Node object's data:
    eq = model.data.get_node(eqname[0]).data

    # Setting limits on state variables
    for v in var:
        eq.set_limits(v, stdlim['min'], stdlim['max'])

    eq.set_limits(var[0], conlim['min'], conlim['max'])

    # Setting the material associated to this equation
    eq.mat = mat[0]

    eq.set_data(EqParameter.CONVECTIVE_FLUX,  EqSetting.CONVECTIVE_ROE)
    eq.set_data(EqParameter.PRINCIPAL_DGFORM, EqSetting.WEAKFORM)
    eq.set_data(EqParameter.DISSIPATION,      EqSetting.EQSET_NONE)
    eq.set_data(EqParameter.DISSIPATIVE_FLUX, EqSetting.EQSET_NONE)
    eq.set_data(EqParameter.DAUXILIAR_DGFORM, EqSetting.EQSET_NONE)
    eq.set_data(EqParameter.VAUXILIAR_DGFORM, EqSetting.WEAKFORM)
    eq.set_data(EqParameter.VISCOUS_FLUX,     EqSetting.VISCOUS_BR1)

    #Set domain
    # Create sudomains
    for s in subd:
        model.init_elementset(s)

    # Set data of one subdomain
    sd = model.get_elementset(subd[0])

    sd.add_equation(eqname[0])

    for f in fset.keys():  # face sets
        sd.add_faceset(f)

    # Create boundaries
    for f in fset:
        model.init_faceset(f, fset[f])

    fs = model.get_facesets()

    for f_node in fs:
        f = f_node.data # access tree's node contents
        f.set_neighbours(subd[0])

    model.init_time_settings(time['tacc'])
    time_settings = model.get_time_settings()
    time_settings.time_init = time['t_init']
    time_settings.time_max  = time['t_max']
    time_settings.time_save = time['t_save']
    time_settings.deltat    = time['dt']

    time_settings.nondimensionalization(
        ref['rho'], ref['vel'], ref['len'], ref['mu'], ref['temp'])

    model.init_time_integration(eqname[0], time['tint'])

    return model

