#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# DESCRIPTION tests
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#
# $ python3 -m unittest -v manticore/models/compressible/description/tests/test_fluid.py

import unittest
from manticore.models.compressible.description.materials import Fluid
from manticore.models.compressible.description.reference import ReferenceData
from manticore.models.compressible.description.mdtypes import ThermoModel
from manticore.models.compressible.description.mdtypes import TransportModel
from manticore.models.compressible.description.mdtypes import EquationOfState


class FluidTestCase(unittest.TestCase):
    species = {'MOLARMASS': 28.9686e-03, 'NMOLES': 1}
    thermo = {'TYPE': 'CONSTANTCP', 'CP': 1004.5}
    transport = {'TYPE': 'CONSTANT', 'MU': 1.79e-05, 'PRANDTL': 0.72}
    equation = {'TYPE': 'IDEALGAS', 'GASCTE': 8.314472}
    refData = ReferenceData(0.0241, 59.01, 1.0, 1.79e-05, 216.66)
    ref = refData.get_data()
    REF_DCp = thermo['CP']
    REF_DR = (equation['GASCTE'] / species['MOLARMASS'])
    REF_Gamma = REF_DCp / (REF_DCp - REF_DR)
    REF_DCv = REF_DCp / REF_Gamma
    REF_DMu = transport['MU']
    REF_Pr = transport['PRANDTL']
    rho = ref['rho']
    vel = ref['vel']
    lgt = ref['len']
    mu = ref['mu']
    temp = ref['temp']
    aux = temp / (vel * vel)
    REF_Cp = REF_DCp * aux
    REF_Cv = REF_DCv * aux
    REF_Mu = REF_DMu / mu
    REF_R = REF_DR * aux

    def setUp(self):
        self.fluid = Fluid(FluidTestCase.species, FluidTestCase.thermo,
                           FluidTestCase.transport, FluidTestCase.equation)
        self.fluid.init_material_model()

        self.fluid.nondimensionalization(
            FluidTestCase.ref['rho'], FluidTestCase.ref['vel'],
            FluidTestCase.ref['len'], FluidTestCase.ref['mu'],
            FluidTestCase.ref['temp'])

    def test_types(self):
        self.assertEqual(self.fluid.thermo.type   , ThermoModel.CONSTANTCP)
        self.assertEqual(self.fluid.transport.type, TransportModel.CONSTANT)
        self.assertEqual(self.fluid.equation.type , EquationOfState.IDEALGAS)

    def test_dimensional_data(self):
        self.assertAlmostEqual(self.fluid.thermo.DCp,    FluidTestCase.REF_DCp)
        self.assertAlmostEqual(self.fluid.thermo.DCv,    FluidTestCase.REF_DCv)
        self.assertAlmostEqual(self.fluid.equation.DR,   FluidTestCase.REF_DR)

    def test_nondimensional_data(self):
        self.assertAlmostEqual(
            self.fluid.transport.Pr, FluidTestCase.REF_Pr)
        self.assertAlmostEqual(
            self.fluid.thermo.gamma, FluidTestCase.REF_Gamma)
        self.assertAlmostEqual(self.fluid.thermo.Cp, FluidTestCase.REF_Cp)
        self.assertAlmostEqual(self.fluid.thermo.Cv, FluidTestCase.REF_Cv)
        self.assertAlmostEqual(self.fluid.transport.mu, FluidTestCase.REF_Mu)
        self.assertAlmostEqual(self.fluid.equation.R,   FluidTestCase.REF_R)

if __name__ == '__main__':
    unittest.main()

## -- test_fluid.py ------------------------------------------------------------
