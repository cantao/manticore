#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# DESCRIPTION tests
#
# @addtogroup DESCRIPTION
# @author Cantao! (rfcantao@gmail.com)
#
#
# @refs https://docs.python.org/3/library/unittest.html
#
# $ python3 -m unittest -v manticore/models/compressible/description/tests/test_referencedata.py

import unittest
from manticore.models.compressible.description.reference import ReferenceData


class ReferenceDataTestCase(unittest.TestCase):
    REF_RHO = 0.0241
    REF_VEL = 59.01
    REF_LGT = 1.0
    REF_MU = 1.79e-05
    REF_TEMP = 216.66

    def setUp(self):
        self.refData = ReferenceData(
            ReferenceDataTestCase.REF_RHO, ReferenceDataTestCase.REF_VEL,
            ReferenceDataTestCase.REF_LGT, ReferenceDataTestCase.REF_MU,
            ReferenceDataTestCase.REF_TEMP)

    def test_get(self):
        data = self.refData.get()

        self.assertAlmostEqual(data[0], ReferenceDataTestCase.REF_RHO)
        self.assertAlmostEqual(data[1], ReferenceDataTestCase.REF_VEL)
        self.assertAlmostEqual(data[2], ReferenceDataTestCase.REF_LGT)
        self.assertAlmostEqual(data[3], ReferenceDataTestCase.REF_MU)
        self.assertAlmostEqual(data[4], ReferenceDataTestCase.REF_TEMP)

    def test_get_data(self):
        data = self.refData.get_data()

        self.assertAlmostEqual(data['rho'], ReferenceDataTestCase.REF_RHO)
        self.assertAlmostEqual(data['vel'], ReferenceDataTestCase.REF_VEL)
        self.assertAlmostEqual(data['len'], ReferenceDataTestCase.REF_LGT)
        self.assertAlmostEqual(data['mu'], ReferenceDataTestCase.REF_MU)
        self.assertAlmostEqual(data['temp'], ReferenceDataTestCase.REF_TEMP)

    def test_Re(self):
        REF_Re = (
            ReferenceDataTestCase.REF_RHO * ReferenceDataTestCase.REF_VEL *
            ReferenceDataTestCase.REF_LGT / ReferenceDataTestCase.REF_MU)

        self.assertAlmostEqual(self.refData.Re, REF_Re)


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_referencedata.py ----------------------------------------------------
