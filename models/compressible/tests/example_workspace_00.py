#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/models/compressible/tests/example_workspace_00.py -ll DEBUG
# or
#
# $ python3 -m cProfile -o prof manticore/models/compressible/tests/example_workspace_00.py
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.tests.simple_meshes import mesh01
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldType
from manticore.lops.modal_dg.entity import GlobalRoleIterator
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.lops.modal_dg.entityops import (
    ComputeEntityVolume, ComputeEntityMassMatrix,
    InitializeFields, InitFieldValues )
from manticore.lops.modal_dg.class_instances import (
    class_quad_physbackward2d, class_tria_physbackward2d )
from manticore.models.compressible.timeint.generic import (
    BackwardTransformation, ForwardTransformation, VariableTimeStep,
    CharacteristicLength )
from manticore.models.compressible.tests.test_helpers import (
    example_warburton_book_6p1, simple_model_00 )
from manticore.models.compressible.services.datatypes import (
    CFLBroadCast )
from manticore.models.compressible.equations.bcs import UpdateBC
from manticore.models.compressible.equations.limiters import (
    PositivePreservingLimiter)
from manticore.models.compressible.equations.manager import EquationManager

def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example Workspace 00")

    cm = mesh01()

    for s in cm:
        logger.debug("Subdomain: %s Name: %s Role: %s" % (s.ID, s.name, s.role))

    elist = GlobalRoleIterator(cm.container).find(EntityRole.PHYSICAL)
    logger.info(
        "Evaluating jacobians of %s elements..." % len(elist))
    for e in elist:
        e.eval_jacobian()

    subds = SubDomainRoleIterator(cm).find(EntityRole.PHYSICAL)

    # It seems more efficient to compute the mass matrix this way
    # instead of through the element's specific method.
    logger.info("Evaluating mass matrices...")
    mass_op = ComputeEntityMassMatrix()
    for s in subds:            
        foreach_entity_in_subdomain(s, mass_op)

    logger.info("Evaluating physical elements' volumes...")
    vol_op = ComputeEntityVolume()
    for s in subds:            
        foreach_entity_in_subdomain(s, vol_op)

    # Initializing fields storage
    logger.info('Initializing fields storage...')
    stt = [FieldVariable.RHO,  FieldVariable.RHOU,
               FieldVariable.RHOV, FieldVariable.RHOE]
    rsd = [FieldVariable.RHO,  FieldVariable.RHOU,
               FieldVariable.RHOV, FieldVariable.RHOE]
    cte = [FieldVariable.CHAR_LENGTH, FieldVariable.DELTAT]
    init_fields = InitializeFields(stt, rsd, cte, auxiliary=rsd)
    for s in subds: 
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    gsubds = SubDomainRoleIterator(cm).find(EntityRole.GHOST)
    init_fields = InitializeFields(stt, rsd)
    for s in gsubds: 
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    func = example_warburton_book_6p1(x0=1.5, y0=1.5, beta=5., gamma=1.4,t=0.)

    init_rho_values  = InitFieldValues(FieldVariable.RHO,  func.rho)
    init_rhou_values = InitFieldValues(FieldVariable.RHOU, func.rhou)
    init_rhov_values = InitFieldValues(FieldVariable.RHOV, func.rhov)
    init_rhoe_values = InitFieldValues(FieldVariable.RHOE, func.rhoe)

    logger.info('Initializing fiels values from analytical expression...')
    for s in subds: 
        logger.debug('Initializing RHO values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rho_values)
        logger.debug('Initializing RHOU values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhou_values)
        logger.debug('Initializing RHOV values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhov_values)
        logger.debug('Initializing RHOE values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhoe_values)

    logger.info('Testing backward operation...')
    bkw = BackwardTransformation(stt)
    for s in subds: 
        foreach_entity_in_subdomain(s, bkw)

    logger.info('Testing forward operation...')
    frw = ForwardTransformation(stt)
    for s in subds: 
        foreach_entity_in_subdomain(s, frw)

    ph  = FieldType.ph
    tr  = FieldType.tr
    rho = FieldVariable.RHO
    
    for e in elist:
        op = [class_quad_physbackward2d(e.key),class_tria_physbackward2d(e.key)]
        values = np.zeros(e.key.n2d)
        # logger.debug("e[%s].RHO.ph = \n%s" % (e.ID, e.state(ph, rho)))
        # logger.debug("e[%s].RHO.tr = \n%s" % (e.ID, e.state(tr, rho)))
        op[e.type](e.state(tr, rho), values)
        diff = e.state(ph, rho)-values
        logger.debug("e[%4d].||ph-bw(ph)||= %s" % (e.ID, np.linalg.norm(diff)))

    model_desc = simple_model_00()

    # Limiter
    logger.info('Positive preserving limiter...')
    limiter = PositivePreservingLimiter(model_desc)
    limiter.apply(cm)

    logger.info("Evaluating characteristic lengths...")
    cl_op = CharacteristicLength()
    for s in subds:            
        foreach_entity_in_subdomain(s, cl_op)

    # Broadcasting the global value of CFL: other portions of the code
    # will access that global value
    CFLBroadCast().set(model_desc.get_time_settings().cfl())

    logger.info('Computing variable time setps...')
    var_dt = VariableTimeStep(model_desc)
    for s in subds:
        var_dt.setup('inviscid', s.name)
        foreach_entity_in_subdomain(s, var_dt)

    max_dt = var_dt.max_dt
    min_dt = var_dt.min_dt

    logger.info('Max DT = %s at e[%s]' % (max_dt.value, max_dt.e_ID))
    logger.info('Min DT = %s at e[%s]' % (min_dt.value, min_dt.e_ID))

    # Boundary conditions
    bc = UpdateBC(model_desc)

    logger.info('Updating boundary conditions...')
    bc.update(cm)

    eqs = EquationManager()
    eqs.set_equations(model_desc)

    logger.info('Evaluating residual...')
    for eq_name, eq in eqs:
            eq.compute_residual(model_desc, cm)
        
    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')
    parser.add_argument(
        '-d',
        '--directive',
        type=str,
        choices=['STANDARD', 'NULL_DIRICHLET', 'VALIDATION'],
        default='STANDARD',
        help='Set the execution directive')
    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
                }
            },
            'handlers': {
                'console': {
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                },
            },
            'loggers': {
                '': {
                    'handlers': ['console'],
                    'level': args.loglevel,
                },
            }
        }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    from manticore.models.compressible.directives import ExecDirective
    manticore.models.compressible.directive = ExecDirective.process(
                                                                 args.directive)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
