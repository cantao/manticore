//--------------------------------------------------------------- -*- c++ -*- --
side = 10.;
num_el  = 8;
sz = side/num_el;

Point(0) = {   0, -side/2., 0, sz};
Point(1) = {side, -side/2., 0, sz};
Point(2) = {side,  side/2., 0, sz};
Point(3) = {   0,  side/2., 0, sz};

Line(4) = {0, 1};
Line(5) = {2, 3};
Line(6) = {3, 0};
Line(7) = {1, 2};

Line Loop(8) = {4, 7, 5, 6};

Plane Surface(9) = {8};

Transfinite Line {4, 5, 6, 7} = num_el+1;
Transfinite Surface{9};
Recombine Surface{9};

Physical Line("FACE_0") = {4};
Physical Line("FACE_1") = {5};
Physical Line("FACE_2") = {6};
Physical Line("FACE_3") = {7};
Physical Surface("FLUID") = {9};

