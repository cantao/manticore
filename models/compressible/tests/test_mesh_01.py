#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/models/compressible/tests/test_mesh_01.py -ll DEBUG
#
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.tests.simple_meshes import mesh03, mesh02
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldType
from manticore.lops.modal_dg.entity import GlobalRoleIterator
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.lops.modal_dg.entityops import (
    ComputeEntityVolume, ComputeEntityMassMatrix,
    InitializeFields, InitFieldValues )
from manticore.models.compressible.timeint.generic import (
    BackwardTransformation, ForwardTransformation,
    CharacteristicLength, ConstantTimeStep,
    CheckError, WriteFields )
from manticore.lops.modal_dg.class_instances import (
    class_quad_physbackward2d, class_tria_physbackward2d,
    class_quad_physevaluator2d, class_tria_physevaluator2d)
from manticore.lops.modal_dg.tests.test_helpers import Polynomial

from math import sqrt

def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Test Mesh 01")

    cm = mesh03(n=4, p_order=5)

    for s in cm:
        logger.debug("Subdomain: %s Name: %s Role: %s" % (s.ID, s.name, s.role))

    # Lists for iterating over physical elements
    elist = GlobalRoleIterator(cm.container).find(EntityRole.PHYSICAL)
    subds = SubDomainRoleIterator(cm).find(EntityRole.PHYSICAL)

    logger.info(
        "Evaluating jacobians of %s elements..." % len(elist))
    for e in elist:
        e.eval_jacobian()

    # It seems more efficient to compute the mass matrix this way
    # instead of through the element's specific method.
    logger.info("Evaluating mass matrices...")
    mass_op = ComputeEntityMassMatrix()
    for s in subds:            
        foreach_entity_in_subdomain(s, mass_op)

    # for e in elist:
    #     logger.info('e[%s].InvMass = \n%s' % (e.ID, e.get_inv_mass()))
    #     ans=input("Continue?")

    logger.info("Evaluating physical elements' volumes...")
    vol_op = ComputeEntityVolume()
    for s in subds:            
        foreach_entity_in_subdomain(s, vol_op)

    # Initializing fields storage
    logger.info('Initializing fields storage...')
    stt = [FieldVariable.RHO,  FieldVariable.RHOU,
               FieldVariable.RHOV, FieldVariable.RHOE]
    rsd = [FieldVariable.RHO,  FieldVariable.RHOU,
               FieldVariable.RHOV, FieldVariable.RHOE]
    cte = [FieldVariable.CHAR_LENGTH, FieldVariable.DELTAT]
    init_fields = InitializeFields(stt, rsd, cte, auxiliary=rsd)
    for s in subds: 
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    gsubds = SubDomainRoleIterator(cm).find(EntityRole.GHOST)
    init_fields = InitializeFields(stt, rsd)
    for s in gsubds: 
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    func = Polynomial(0,4)

    init_rho_values  = InitFieldValues(FieldVariable.RHO,  func)

    logger.info('Initializing fiels values from analytical expression...')
    for s in subds: 
        logger.debug('Initializing RHO values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rho_values)

    logger.info('Initial error...')
    
    bkw = BackwardTransformation(stt)
    for s in subds: 
        foreach_entity_in_subdomain(s, bkw)

    ph  = FieldType.ph
    tr  = FieldType.tr
    rho = FieldVariable.RHO
    
    logger.info('Exact versus backward')
    
    error = 0.
    for e in elist:
        op = [class_quad_physevaluator2d(e.key),
                class_tria_physevaluator2d(e.key)]
        values = op[e.type](func, e.get_coeff(), e.geom_type, e.vmap)
        #logger.debug("e[%s].tr = \n%s" % (e.ID, e.state(tr, rho)))
        diff = e.state(ph, rho)-values
        #logger.debug("e[%4d].||ph-bw(ph)||= %s" % (e.ID, np.linalg.norm(diff)))
        error += np.linalg.norm(diff)**2
    logger.info('Error = %s' % (sqrt(error)))
    
    #ans=input("Continue?")

    logger.info('Physical versus forward/backward')
    frw = ForwardTransformation(stt)
    for s in subds: 
        foreach_entity_in_subdomain(s, frw)

    error = 0.
    for e in elist:
        op = [class_quad_physbackward2d(e.key),class_tria_physbackward2d(e.key)]
        values = np.zeros(e.key.n2d)
        # logger.debug("e[%s].RHO.ph = \n%s" % (e.ID, e.state(ph, rho)))
        # logger.debug("e[%s].RHO.tr = \n%s" % (e.ID, e.state(tr, rho)))
        op[e.type](e.state(tr, rho), values)
        diff = e.state(ph, rho)-values
        #logger.debug("e[%4d].||ph-bw(ph)||= %s" % (e.ID, np.linalg.norm(diff)))
        error += np.linalg.norm(diff)**2
    logger.info('Error = %s' % (sqrt(error)))
    
    logger.info("Test ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')

    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
                }
            },
            'handlers': {
                'console': {
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                },
                'file':{
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': 'test_mesh_00.log',
                    'mode': 'w',
                    'maxBytes': 1048576,
                    'backupCount': 3,
                },
            },
            'loggers': {
                '': {
                    'handlers': ['console'],
                    #'handlers': ['file', 'console'],
                    'level': args.loglevel,
                },
            }
        }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
