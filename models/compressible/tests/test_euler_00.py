#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/models/compressible/tests/test_euler_00.py -ll DEBUG
# or
#
# $ python3 -m cProfile -o prof manticore/models/compressible/tests/test_euler_00.py
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
##
## Execution directives
##
from manticore.models.compressible.directives import ExecDirective
manticore.models.compressible.directive = ExecDirective.process(
                                                        'WARBURTON_EX_6_1')
##
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.tests.simple_meshes import mesh02
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldType
from manticore.lops.modal_dg.entity import GlobalRoleIterator
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.lops.modal_dg.entityops import (
    ComputeEntityVolume, ComputeEntityMassMatrix,
    InitializeFields, InitFieldValues )
from manticore.models.compressible.timeint.generic import (
    BackwardTransformation, CharacteristicLength, ConstantTimeStep,
    CheckError, WriteFields )
from manticore.models.compressible.tests.test_helpers import (
    example_warburton_book_6p1, simple_model_01 )
from manticore.models.compressible.services.datatypes import (
    ModelTime )
from manticore.models.compressible.equations.manager import EquationManager
from manticore.models.compressible.timeint.integrator import SSP_5_4_RK
from collections import OrderedDict
from math import sqrt
import numpy as np
from array import array
from manticore.lops.modal_dg.entity import ExpansionEntityFactory
from manticore.lops.modal_dg.entity import GlobalSequentialIterator
from manticore.lops.modal_dg.computemesh import ComputeMesh
from manticore.geom.standard_geometries import (
    StandardGeometry, geom, number_of_nodes, geometry_name)
from manticore.lops.modal_dg.dgtypes import FieldRole
from manticore.lops.nodal_cg.shfuncs import factory_shape_functions

def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running Test Euler 00")

    #
    # Mesh of ONE physical element
    #
    geom_type=StandardGeometry.QUAD4
    incid = np.array([ 0,     1,     2,     3   ])
    # nx    = np.array([ 0.,    1.25,  1.25,  0.  ])
    # ny    = np.array([-5.,   -5.,   -3.75, -3.75])
    nx    = np.array([ 3.75,  5.,    5.,    3.75  ])
    ny    = np.array([-1.25, -1.25,  0.,    0.])

    cm = ComputeMesh()
    shfsq = factory_shape_functions(geom_type)()
    
    # Physical Subdomains
    subd_id = cm.create_subdomain("FLUID", EntityRole.PHYSICAL)

    # Four different expansions inside the domain (four ExpansionGroups)
    # 1
    M = 2  # p-order
    #N = 2 * M + shfsq.jacobian_order()  # Number of 1D integration points
    N = 2 * M
    k1q, c1q = cm.create_expansion(subd_id, M, N)
    
    e_quad = ExpansionEntityFactory.make(geom_type)
    e_quad.ID = 0
    e = cm.insert(subd_id, k1q, e_quad)
    e.set_vertices(incid)
    e.set_coeff(np.array([nx, ny]))

    bc0 = cm.create_subdomain("FACE_0", EntityRole.GHOST)
    bc1 = cm.create_subdomain("FACE_1", EntityRole.GHOST)
    bc2 = cm.create_subdomain("FACE_2", EntityRole.GHOST)
    bc3 = cm.create_subdomain("FACE_3", EntityRole.GHOST)

    kgh, cgh = cm.create_expansion(bc0, 0, k1q.n1d)
    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 100
    e = cm.insert(bc0, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(0), 0)

    kgh, cgh = cm.create_expansion(bc1, 0, k1q.n1d)
    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 101
    e = cm.insert(bc1, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(0), 1)

    kgh, cgh = cm.create_expansion(bc2, 0, k1q.n1d)
    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 102
    e = cm.insert(bc2, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(0), 2)

    kgh, cgh = cm.create_expansion(bc3, 0, k1q.n1d)
    e_quad = ExpansionEntityFactory.make(StandardGeometry.QUAD4)
    e_quad.ID = 103
    e = cm.insert(bc3, kgh, e_quad)
    e.insert_neighbour(0, cm.get_entity(0), 3)

    e = cm.get_entity(0)
    e.insert_neighbour(0, cm.get_entity(100), 0)
    e.insert_neighbour(1, cm.get_entity(101), 0)
    e.insert_neighbour(2, cm.get_entity(102), 0)
    e.insert_neighbour(3, cm.get_entity(103), 0)

    elist = GlobalSequentialIterator(cm.container)
    for e in elist:
        e.init_face_comm_sizes()

    # end of mesh generation

    for s in cm:
        logger.debug("Subdomain: %s Name: %s Role: %s" % (s.ID, s.name, s.role))

    # Lists for iterating over physical elements
    elist = GlobalRoleIterator(cm.container).find(EntityRole.PHYSICAL)
    subds = SubDomainRoleIterator(cm).find(EntityRole.PHYSICAL)

    logger.info(
        "Evaluating jacobians of %s elements..." % len(elist))
    for e in elist:
        e.eval_jacobian()

    # It seems more efficient to compute the mass matrix this way
    # instead of through the element's specific method.
    logger.info("Evaluating mass matrices...")
    mass_op = ComputeEntityMassMatrix()
    for s in subds:            
        foreach_entity_in_subdomain(s, mass_op)

    logger.info("Evaluating physical elements' volumes...")
    vol_op = ComputeEntityVolume()
    for s in subds:            
        foreach_entity_in_subdomain(s, vol_op)

    # Initializing fields storage
    logger.info('Initializing fields storage...')
    stt = [FieldVariable.RHO,  FieldVariable.RHOU,
               FieldVariable.RHOV, FieldVariable.RHOE]
    rsd = [FieldVariable.RHO,  FieldVariable.RHOU,
               FieldVariable.RHOV, FieldVariable.RHOE]
    cte = [FieldVariable.CHAR_LENGTH, FieldVariable.DELTAT]
    init_fields = InitializeFields(stt, rsd, cte, auxiliary=rsd)
    for s in subds: 
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    gsubds = SubDomainRoleIterator(cm).find(EntityRole.GHOST)
    init_fields = InitializeFields(stt, rsd)
    for s in gsubds: 
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    # Model description
    model_desc = simple_model_01()

    # Initial condition
    time_settings = model_desc.get_time_settings()
    time_init     = time_settings.time_init
    time_glb      = ModelTime()
    time_glb.set(time_init)
    
    fluid = model_desc.get_material('Air')
    g = fluid.thermo.gamma
    
    func = example_warburton_book_6p1(x0=5.,y0=0.,beta=5.,gamma=g,t=time_init)

    init_rho_values  = InitFieldValues(FieldVariable.RHO,  func.rho)
    init_rhou_values = InitFieldValues(FieldVariable.RHOU, func.rhou)
    init_rhov_values = InitFieldValues(FieldVariable.RHOV, func.rhov)
    init_rhoe_values = InitFieldValues(FieldVariable.RHOE, func.rhoe)

    logger.info('Initializing fiels values from analytical expression...')
    for s in subds: 
        logger.debug('Initializing RHO values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rho_values)
        logger.debug('Initializing RHOU values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhou_values)
        logger.debug('Initializing RHOV values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhov_values)
        logger.debug('Initializing RHOE values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhoe_values)

    logger.info("Evaluating characteristic lengths...")
    cl_op = CharacteristicLength()
    for s in subds:            
        foreach_entity_in_subdomain(s, cl_op)

    logger.info('Setting time setps...')
    cte_dt = ConstantTimeStep(model_desc.get_time_settings().deltat)
    for s in subds:
        foreach_entity_in_subdomain(s, cte_dt)

    eqs = EquationManager()
    eqs.set_equations(model_desc)

    t_now = time_settings.time_init
    dt    = time_settings.deltat
    t_max = time_settings.time_max
    t_fsv = time_settings.time_save


    # logger.info('Initial error...')
    
    # bkw = BackwardTransformation(stt)
    # for s in subds: 
    #     foreach_entity_in_subdomain(s, bkw)
    
    # func = example_warburton_book_6p1(x0=5.,y0=0.,beta=5.,gamma=g,t=t_now)
    # check_error = CheckError(func, stt)
    # for s in subds:            
    #     foreach_entity_in_subdomain(s, check_error)
        
    # for v in stt:
    #     logger.info('Error in %s = %s' % (v, sqrt(check_error.l2_norm[v])))

    # ans=input("Continue?")

    logger.info("RHO  = \n%s" % (e.state(FieldType.ph, FieldVariable.RHO)))
    logger.info("RHOU = \n%s" % (e.state(FieldType.ph,FieldVariable.RHOU)))

    for eq_name, eq in eqs:
        eq.compute_residual(model_desc, cm)

    e = cm.get_entity(0)

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Jacobi polynomials example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')

    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
                }
            },
            'handlers': {
                'console': {
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                },
                'file':{
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': 'example_euler_01.log',
                    'mode': 'a',
                    'maxBytes': 1048576,
                    'backupCount': 3,
                },
            },
            'loggers': {
                '': {
                    'handlers': ['file', 'console'],
                    'level': args.loglevel,
                },
            }
        }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
