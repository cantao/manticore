#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# DESCRIPTION tests
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#
# $ python3 -m unittest -v manticore/models/compressible/tests/test_model.py

import unittest
import manticore.models.compressible.description.mdtypes as mtypes
from manticore.models.compressible.description.model import ModelDescription
from manticore.models.compressible.description.materials import Fluid
from manticore.models.compressible.description.reference import ReferenceData
from manticore.services.fieldvariables import FieldVariable
from manticore.mdf.enums import MapType


class ModelTestCase(unittest.TestCase):
    # Data for the model
    species = {'MOLARMASS': 28.9686e-03, 'NMOLES': 1}
    thermo = {'TYPE': 'CONSTANTCP', 'CP': 1004.5}
    transport = {'TYPE': 'CONSTANT', 'MU': 1.79e-05, 'PRANDTL': 0.72}
    equation = {'TYPE': 'IDEALGAS', 'GASCTE': 8.314472}
    mat = ['Air']
    meshfile = "flatplatehex200_o1.tdf"
    initfile = "flatplatehex200_o1-init.tdf"
    resufile = "flatplatehex200_o1-01-res.tdf"
    restfile = "flatplatehex200_o1-01-restart.tdf"
    refData = ReferenceData(0.0241, 59.01, 1.0, 1.79e-05, 216.66)
    ref = refData.get_data()
    eqname = ['inviscid', 'laminar']
    eqtype = [mtypes.EqType.EULER, mtypes.EqType.NAVIERSTOKES]
    eqs = dict(zip(eqname, eqtype))
    var = [
        FieldVariable.RHO, FieldVariable.RHOU, FieldVariable.RHOV,
        FieldVariable.RHOE
    ]
    stdlim = {'min': 1.0e4, 'max': 1.0e15}
    conlim = {'min': 1.0e-12, 'max': 1.0e15}
    eqparam = [
        mtypes.EqParameter.DISSIPATIVE_FLUX, mtypes.EqParameter.VISCOUS_FLUX,
        mtypes.EqParameter.VISCOSITY_MODEL
    ]
    eqsetting = mtypes.EqSetting.VISCOUS_BR2
    subd = ['fluid1', 'fluid2']
    fname = ["farfield", "wall", "backpressure", "symmetry"]
    ftype = [
        mtypes.FaceType.FARFIELD, mtypes.FaceType.ADIABATICWALL,
        mtypes.FaceType.BACKPRESSURE, mtypes.FaceType.SYMMETRY
    ]
    fset = dict(zip(fname, ftype))
    fparam = dict.fromkeys(ftype)

    time = {
        'tacc': mtypes.TISetting.TACC_STEADYSTATE,
        'it_max': 20000,
        'it_save': 500,
        'stop': mtypes.TISetting.STOPCRITERIA_MAX,
        'type': mtypes.TISetting.TSTEP_VARIABLE,
        'cfl_type': mtypes.TISetting.CFL_CONSTANT,
        'cfl': 0.2,
        'tint': mtypes.TISetting.INTEGRATOR_SSP54RK
    }

    def setUp(self):
        # Create empty model
        self.model = ModelDescription()

        # Set input/output files through Tree's interface
        self.model.data.get_node('MESH').data = ModelTestCase.meshfile
        self.model.data.get_node('INITIAL').data = ModelTestCase.initfile
        self.model.data.get_node('RESULTS').data = ModelTestCase.resufile
        self.model.data.get_node('RESTART').data = ModelTestCase.restfile
        self.model.data.get_node('INITIAL_TYPE').data = MapType.HIERARCHICAL

        # Set reference data
        self.model.set_reference(
            ModelTestCase.ref['rho'], ModelTestCase.ref['vel'],
            ModelTestCase.ref['len'], ModelTestCase.ref['mu'],
            ModelTestCase.ref['temp'])

        # Set material

        # Insert Node with ID ModelTestCase.mat[0] as MATERIALS's
        # child using Tree's interface
        self.model.init_material(
            ModelTestCase.mat[0],
            Fluid(ModelTestCase.species, ModelTestCase.thermo,
                  ModelTestCase.transport, ModelTestCase.equation))

        # Nondimensionalization of material data:
        # Accessing ReferenceData's interface do get reference data
        ref = self.model.get_reference_data()

        # Accessing Fluid's interface through Tree's interface
        self.model.data.get_node(
            ModelTestCase.mat[0]).data.nondimensionalization(
                ref['rho'], ref['vel'], ref['len'], ref['mu'], ref['temp'])

        # Setting equation: (name, type)
        self.model.init_equation(ModelTestCase.eqname[0],
                                 ModelTestCase.eqtype[0])

        # Getting a reference to a specific equation description
        # stored as a Node object's data:
        eq = self.model.data.get_node(ModelTestCase.eqname[0]).data

        # Setting limits on state variables
        for v in ModelTestCase.var:
            eq.set_limits(v, ModelTestCase.stdlim['min'],
                          ModelTestCase.stdlim['max'])

        eq.set_limits(ModelTestCase.var[0], ModelTestCase.conlim['min'],
                      ModelTestCase.conlim['max'])

        # Setting the material associated to this equation
        eq.mat = ModelTestCase.mat[0]

        # Setting another equation
        self.model.init_equation(ModelTestCase.eqname[1],
                                 ModelTestCase.eqtype[1])
        eq = self.model.data.get_node(ModelTestCase.eqname[1]).data
        for v in ModelTestCase.var:
            eq.set_limits(v, ModelTestCase.stdlim['min'],
                          ModelTestCase.stdlim['max'])

        eq.set_limits(ModelTestCase.var[1], ModelTestCase.conlim['min'],
                      ModelTestCase.conlim['max'])

        eq.mat = ModelTestCase.mat[0]

        # Setting one solution parameter
        eq.set_data(ModelTestCase.eqparam[1], ModelTestCase.eqsetting)

        #Set domain
        # Create sudomains
        for s in ModelTestCase.subd:
            self.model.init_elementset(s)

        # Set data of one subdomain
        sd = self.model.data.get_node(ModelTestCase.subd[0]).data

        sd.add_equation(ModelTestCase.eqname[1])  # equations

        for f in ModelTestCase.fset.keys():  # face sets
            sd.add_faceset(f)

        # Create boundaries
        for f in ModelTestCase.fset:
            self.model.init_faceset(f, ModelTestCase.fset[f])

        fs = self.model.get_facesets()

        for f_node in fs:
            f = f_node.data # acesse tree's node contents
            
            if f.data.type == mtypes.FaceType.FARFIELD:
                f.data.set_dim_data(mtypes.FaceParameter.VELX, 59.01)
                f.data.set_dim_data(mtypes.FaceParameter.VELY, 0.0)
                f.data.set_dim_data(mtypes.FaceParameter.STATIC_PRESS, 1498.66)
                f.data.set_dim_data(mtypes.FaceParameter.STATIC_TEMP, 216.66)
            elif f.data.type == mtypes.FaceType.BACKPRESSURE:
                f.data.set_dim_data(mtypes.FaceParameter.STATIC_PRESS, 1498.66)
            elif f.data.type == mtypes.FaceType.ADIABATICWALL:
                f.data.set_dim_data(mtypes.FaceParameter.VELX, 59.01)
                f.data.set_dim_data(mtypes.FaceParameter.VELY, 0.0)

            f.data.nondimensionalization(ref['rho'], ref['vel'], ref['len'],
                                         ref['mu'], ref['temp'])

        self.model.init_time_settings(ModelTestCase.time['tacc'])
        time_settings = self.model.get_time_settings()
        time_settings.it_max = ModelTestCase.time['it_max']
        time_settings.it_save = ModelTestCase.time['it_save']
        time_settings.stop_criteria = ModelTestCase.time['stop']
        time_settings.tstep.set_type(ModelTestCase.time['type'])
        time_settings.tstep.set_cfl_type(ModelTestCase.time['cfl_type'])
        time_settings.tstep.set_cfl(ModelTestCase.time['cfl'])

        self.model.init_time_integration(
            ModelTestCase.eqname[0], ModelTestCase.time['tint'])

        self.model.init_time_integration(
            ModelTestCase.eqname[1], ModelTestCase.time['tint'])

    def test_equations(self):
        eq = self.model.get_equations()

        # Test storage of equations
        self.assertEqual(len(eq), len(ModelTestCase.eqname))

        # Test types of equations
        for i in range(len(eq)):
            self.assertEqual(
                self.model.data.get_node(ModelTestCase.eqname[i]).data.type,
                ModelTestCase.eqtype[i])

        # Test limits
        limits = (ModelTestCase.conlim['min'], ModelTestCase.conlim['max'])
        e = self.model.data.get_node(ModelTestCase.eqname[1]).data
        self.assertEqual(e.get_limits(ModelTestCase.var[1]), limits)

        # Test Parameters
        self.assertTrue(ModelTestCase.eqparam[0] in e.data)
        self.assertTrue(ModelTestCase.eqparam[2] in e.data)
        self.assertEqual(
            e.get_data(ModelTestCase.eqparam[1]), ModelTestCase.eqsetting)

        # Test materials
        self.assertEqual(e.mat, ModelTestCase.mat[0])

    # def tearDown(self):
    #     self.model.data.show()

    def test_subdomains(self):
        vs = self.model.get_elementsets()

        self.assertEqual(len(vs), len(ModelTestCase.subd))

        for s in ModelTestCase.subd:
            self.assertTrue(self.model.data.contains(s))

        for v in vs:
            self.assertTrue(v.data.name in ModelTestCase.subd)

        sd = self.model.data.get_node(ModelTestCase.subd[0]).data

        for f in ModelTestCase.fset.keys():
            self.assertTrue(f in sd.facesets)

        self.assertTrue(sd.equations[0] in ModelTestCase.eqname)

    def test_boundaries(self):
        fs = self.model.get_facesets()

        self.assertEqual(len(fs), len(ModelTestCase.ftype))

        for f_node in fs:

            f = f_node.data
            
            self.assertTrue(f.data.type in ModelTestCase.ftype)

            if f.data.type == mtypes.FaceType.FARFIELD:
                self.assertAlmostEqual(
                    f.data.get_data(mtypes.FaceParameter.VELX),
                    f.data.get_dim_data(mtypes.FaceParameter.VELX) /
                    ModelTestCase.ref['vel'])

    def test_solution(self):
        time_settings = self.model.get_time_settings()
        self.assertEqual(time_settings.it_max, ModelTestCase.time['it_max'])
        self.assertEqual(time_settings.it_save, ModelTestCase.time['it_save'])
        self.assertEqual(time_settings.stop_criteria,
                         ModelTestCase.time['stop'])
        self.assertEqual(time_settings.tstep.type,
                         ModelTestCase.time['type'])
        self.assertEqual(time_settings.tstep.cfl_type,
                         ModelTestCase.time['cfl_type'])
        self.assertAlmostEqual(time_settings.tstep.cfl(),
                               ModelTestCase.time['cfl'])

        self.assertEqual(
            self.model.get_time_integration(ModelTestCase.eqname[0]),
            ModelTestCase.time['tint'])


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_model.py ------------------------------------------------------------
