#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/models/compressible/tests/example_ns_00.py -ll DEBUG
# or
#
# $ python3 -m cProfile -o prof manticore/models/compressible/tests/example_ns_00.py
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore
##
## Execution directives
##
from manticore.models.compressible.directives import ExecDirective
manticore.models.compressible.directive = ExecDirective.process('NS_VALIDATION')
##
from manticore.services.fieldvariables import FieldVariable
from manticore.mesh.mdf_mesh_parser import MdfMeshParser
from manticore.mesh.umesh import (
    double_link_region_to_edge, double_link_region_to_region
    )
from manticore.lops.modal_dg.create_computemesh import create_computemesh
from manticore.mdf.enums import MapType
from manticore.lops.modal_dg.tests.simple_meshes import mesh02
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldType
from manticore.lops.modal_dg.entity import (
GlobalRoleIterator, GlobalSequentialIterator)
from manticore.lops.modal_dg.computemesh import (
SubDomainRoleIterator, SubDomainNameIterator)
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.lops.modal_dg.entityops import (
    ComputeEntityVolume, ComputeEntityMassMatrix,
    InitializeFields, InitFieldValues, InitConstantFieldValues )
from manticore.models.compressible.timeint.generic import (
    BackwardTransformation, CharacteristicLength, ConstantTimeStep,
    CheckError, WriteFields )
from manticore.models.compressible.tests.test_helpers import (
    Joukowski_laminar_00, simple_model_03 )
from manticore.models.compressible.services.datatypes import (
    ModelTime )
from manticore.lops.modal_dg.class_instances import class_quad_region, class_tria_region
from manticore.models.compressible.equations.manager import EquationManager
from manticore.models.compressible.timeint.integrator import SSP_5_4_RK
from manticore.models.compressible.manager.post_process import (
drag_coefficient_global, accumulate_in_subdomain)
from manticore.models.compressible.manager.manager import Manager
from manticore.models.compressible.manager.mdf_field_parser import (
    MdfFieldParser )
from manticore.models.compressible.manager.mdf_field_writer import (
    MdfFieldWriter )
from collections import OrderedDict
from math import sqrt

def main():

    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example Joukowski Laminar NS 00")
    #Path to the I/O files
    path = args.path
    #Model description
    model_desc = Joukowski_laminar_00()
    #I/O files settings
    model_desc.data.get_node('MESH').data    = path+"Joukowski_Laminar_Challenge_quad_ref0_Q1.mdf"
    model_desc.data.get_node('RESULTS').data = path+"Joukowski_Laminar_Challenge_quad_ref0_Q1-res.mdf"
    model_desc.data.get_node('RESTART').data = path+"Joukowski_Laminar_Challenge_quad_ref0_Q1-restart.mdf"
    model_desc.data.get_node('INITIAL').data = path+"Joukowski_Laminar_Challenge_quad_ref0_Q1-init.mdf"
    model_desc.data.get_node('INITIAL_TYPE').data = MapType.HIERARCHICAL

    # Reading mesh
    logger.info('Reading mesh...')

    # Name of MDF file
    mdffile = model_desc.get_data('MESH')

    # MDF parser
    parser = MdfMeshParser(mdffile)

    # Geometric mesh
    gm = parser.parse()

    # Building neighborhood topologies
    double_link_region_to_edge(gm)
    double_link_region_to_region(gm)

    # Plain compute mesh
    cm = create_computemesh(gm)

    del gm

    # Print mesh information
    logger.debug('Computation Mesh:\n%s' % cm)

    for s in cm:
        logger.debug("Subdomain: %s Name: %s Role: %s" % (s.ID, s.name, s.role))

    # Lists for iterating over physical elements
    elist = GlobalSequentialIterator(cm.container)

    for e in elist:
        e.init_face_comm_sizes()

    # Lists for iterating over physical elements
    elist = GlobalRoleIterator(cm.container).find(EntityRole.PHYSICAL)
    subds = SubDomainRoleIterator(cm).find(EntityRole.PHYSICAL)

    #Surface to perform the global parameters evaluation
    surface_to_observe = SubDomainNameIterator(cm).find("Wall")

    logger.info(
        "Evaluating jacobians of %s elements..." % len(elist))
    for e in elist:
        e.eval_jacobian()

    # It seems more efficient to compute the mass matrix this way
    # instead of through the element's specific method.
    logger.info("Evaluating mass matrices...")
    mass_op = ComputeEntityMassMatrix()
    for s in subds:
        foreach_entity_in_subdomain(s, mass_op)

    logger.info("Evaluating physical elements' volumes...")
    vol_op = ComputeEntityVolume()
    for s in subds:
        foreach_entity_in_subdomain(s, vol_op)

    # Initializing fields storage
    logger.info('Initializing fields storage...')
    stt = [FieldVariable.RHO,  FieldVariable.RHOU,
               FieldVariable.RHOV, FieldVariable.RHOE]
    rsd = [FieldVariable.RHO,  FieldVariable.RHOU,
               FieldVariable.RHOV, FieldVariable.RHOE]
    cte = [FieldVariable.CHAR_LENGTH, FieldVariable.DELTAT]
    init_fields = InitializeFields(stt, rsd, cte, auxiliary=rsd)
    for s in subds:
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    gsubds = SubDomainRoleIterator(cm).find(EntityRole.GHOST)
    init_fields = InitializeFields(stt, rsd)
    for s in gsubds:
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)

    # Initial condition
    T    = 303.0
    p    = 101325.0
    u    = 0
    v    = 0
    rho  = p/(287.0167*T)
    rhoe = 0.5*rho*u**2 + 0.5*rho*v**2 + p/(1.4-1.)
    mu   = 1.79e-05

    #Initial condition
    init_rho_values  = InitConstantFieldValues(FieldVariable.RHO,  rho)
    init_rhou_values = InitConstantFieldValues(FieldVariable.RHOU, rho*u)
    init_rhov_values = InitConstantFieldValues(FieldVariable.RHOV, rho*v)
    init_rhoe_values = InitConstantFieldValues(FieldVariable.RHOE, rhoe)
    init_mu_values   = InitConstantFieldValues(FieldVariable.MU, mu)

    logger.info('Initializing constant fields...')

    for s in subds:
        logger.debug('Initializing RHO values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rho_values)
        logger.debug('Initializing RHOU values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhou_values)
        logger.debug('Initializing RHOV values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhov_values)
        logger.debug('Initializing RHOE values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_rhoe_values)
        logger.debug('Initializing MU values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_mu_values)

    eqs = EquationManager()
    eqs.set_equations(model_desc)
    # Writing initial condition to a file
    mdf_writer = MdfFieldWriter(model_desc, cm, eqs)
    mdf_writer.write(model_desc.get_data('INITIAL'))

    logger.info('File %s was generated.' % model_desc.get_data('INITIAL'))

    logger.info("Evaluating characteristic lengths...")
    cl_op = CharacteristicLength()
    for s in subds:
        foreach_entity_in_subdomain(s, cl_op)

    logger.info('Setting time setps...')
    cte_dt = ConstantTimeStep(model_desc.get_time_settings().deltat)
    for s in subds:
        foreach_entity_in_subdomain(s, cte_dt)

    eqs = EquationManager()
    eqs.set_equations(model_desc)

    # Initial condition
    mdf_reader = MdfFieldParser(model_desc, cm, eqs)
    mdf_reader.parse()

    model = Manager(args.path+'Joukowski')

    model.read_description(model_desc)

    model.read_mesh()

    model.config_solver()

    model.solve()

    drag_coefficient_operator = drag_coefficient_global(model_desc, class_quad_region)

    for eq_name, eq in eqs:
        drag_coefficient_operator.setup(eq_name)
        dc = accumulate_in_subdomain(surface_to_observe, drag_coefficient_operator)

        print(dc)

    logger.info('Writing response fields...')
    # Write ascii file for post-processing
    e=cm.get_entity(0)
    order = e.key.order
    filename = 'square{}-p{}-results.txt'.format(len(elist),order)
    myfile=open(filename,'w')

    write_fields = WriteFields(myfile)
    for s in subds:
        foreach_entity_in_subdomain(s, write_fields)

    myfile.close()
    #

    logger.info("Example ended!\n")
    # end of main
    
#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='NS validation')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')

    parser.add_argument(
        '-path',
        action='store',
        dest='path',
        required=False)

    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
                }
            },
            'handlers': {
                'console': {
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                },
                'file':{
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': 'example_ns_00.log',
                    'mode': 'a',
                    'maxBytes': 1048576,
                    'backupCount': 3,
                },
            },
            'loggers': {
                '': {
                    'handlers': ['file', 'console'],
                    'level': args.loglevel,
                },
            }
        }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
