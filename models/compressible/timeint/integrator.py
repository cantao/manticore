#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Time integrators.
#
# @addtogroup MODELS.COMPRESSIBLE.TIMEINT
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from abc import ABCMeta, abstractmethod
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import FieldType
from manticore.lops.modal_dg.computemesh import SubDomainNameIterator
from manticore.lops.modal_dg.engine import (
    foreach_entity_in_subdomain, EntityOperator )
from manticore.models.compressible.description.mdtypes import TISetting
from manticore.models.compressible.equations.equation import Equation
from manticore.models.compressible.equations.limiters import (
    PositivePreservingLimiter )
from manticore.models.compressible.timeint.generic import (
    BackwardTransformation )
from manticore.models.compressible.services.datatypes import ModelTime

logger = manticore.logging.getLogger('MDL_CMP_TI')

class TimeIntegrator(metaclass=ABCMeta):
    """Generic time integrator base class for the definition of a
    standard public interface.
    """

    def __init__(self, model_desc, eq):
        
        if not isinstance(eq, Equation):
            raise AssertionError('Second parameter must be Class::Equation!')
        
        self._eq    = eq
        self._desc  = model_desc
        self._esets = eq.element_sets
        self._vars  = eq.residual_variables
        self.bw_op  = BackwardTransformation(self._vars)
        self.limiter = PositivePreservingLimiter(model_desc)

    def update_state(self, cm):
        tstep = self._desc.get_time_settings().time_stepping

        if tstep == TISetting.TSTEP_VARIABLE:
            self.update_with_variable_step(cm)
        elif tstep == TISetting.TSTEP_CONSTANT:
            self.update_with_constant_step(cm)
        else:
            raise RuntimeError('Incorrect time stepping!!!')

    @abstractmethod
    def update_with_constant_step(self, cm): pass

    @abstractmethod
    def update_with_variable_step(self, cm): pass

    def backward(self, cm):

        subdomain_by_name = SubDomainNameIterator(cm)
        
        for eset in self._esets:
            s = subdomain_by_name.find(eset)
            if not s:
                raise RuntimeError('Subdomain not found!!!')

            foreach_entity_in_subdomain(s, self.bw_op)

        # positivity preserving limiter
        self.limiter.apply(cm) 
            
# end of manticore.models.compressible.timeint.integrator.TimeIntegrator


class rkssp5_4_stage_1(EntityOperator):
    """Applies the first stage of RKSSP5_4 (see below) at element e."""

    def __init__(self, list_vars):
        super().__init__()
        self._vars = list_vars
        self.cC1 = 0.39175222700392
        self.c51 = 0.00683325884039

    def container_operator(self, group, key, container): pass
    
    def __call__(self, e):
        """Applies the first stage of RKSSP5_4 at element e.

        Notes:
            * Every stage assumes e.residual<tr> was already (externaly) 
            computed at the appropriate intermediate state.

            * e.state<tr> equals the state at last integration time.
            
            * This stage also copies the state at last integration time 
            to e.auxiliar1<tr>, which is used to progressively compound the 
            next state at each stage.

            * The "progressively compounded the next state" is stored in 
            e.auxiliar2.

        """
        tr    = FieldType.tr
        dt    = e.get_cte(FieldVariable.DELTAT)
        cCdt  = self.cC1 * dt
        c51   = self.c51

        for v in self._vars:
            u1   = e.state(tr, v)
            aux1 = e.auxiliar1(tr, v)
            u5   = e.auxiliar2(tr, v)
            
            np.copyto(aux1, u1)             # aux1 = uhat
            u1 += cCdt * e.residual(tr, v)  # u1   = uhat+c1*dt*rhs

            np.multiply(c51, aux1, u5)      # u5   = c51*uhat
                        
# end of manticore.models.compressible.timeint.integrator.rkssp5_4_stage_1


class rkssp5_4_stage_N(EntityOperator):
    """Applies the N-th (N = 2, 3, 4) stage of RKSSP5_4 at element e."""
    
    def __init__(self, stage, list_vars):
        super().__init__()
        self._N    = stage-2
        self._vars = list_vars
        self.cA = [0.44437049406734, 0.62010185138540, 0.17807995410773]
        self.cB = [0.55562950593266, 0.37989814861460, 0.82192004589227]
        self.cC = [0.36841059262959, 0.25189177424738, 0.54497475021237]
        self.c5 = [0.51723167208978, 0.12759831133288, 0.34833675773694]

    def container_operator(self, group, key, container): pass

    def __call__(self, e):
        """Applies the N-th (N = 2, 3, 4) stage of RKSSP5_4 at element e.

        Notes:
            * Every stage assumes e.residual was already (externaly) 
            computed at the appropriate intermediate state.

            * e.auxiliar1 equals the state at last integration time.

            * e.state equals the intermediate state at (N-1)th stage.

        """
        tr   = FieldType.tr
        dt   = e.get_cte(FieldVariable.DELTAT)
        cA   = self.cA[self._N]
        cB   = self.cB[self._N]
        cCdt = self.cC[self._N] * dt
        c5N  = self.c5[self._N]
        
        for v in self._vars:
            uhat = e.auxiliar1(tr, v)
            uN   = e.state(tr, v)          # uN = uN_minus_1
            u5   = e.auxiliar2(tr, v)
            
            uN *= cB
            uN += cA * uhat
            uN += cCdt * e.residual(tr, v) # uN=cA*uhat+cB*uN_minus_1+cC*dt*rhs
            
            u5 += c5N*uN                   # u5=c52*u2+c53*u3+c54*u4
            
# end of manticore.models.compressible.timeint.integrator.rkssp5_4_stage_N


class rkssp5_4_stage_4_complement(EntityOperator):
    """Applies a complement to the fourth stage of RKSSP5_4 at element e."""
    
    def __init__(self, list_vars):
        super().__init__()
        self._vars = list_vars
        self.c55 = 0.08460416338212

    def container_operator(self, group, key, container): pass

    def __call__(self, e):
        """Applies a complement to the fourth stage of RKSSP5_4 at element e.

        Notes:
            * This auxiliary stage uses the same data of rkssp5_4_stage_N(4) so
            there should no residual evaluation between rkssp5_4_stage_N(4) and
            this operator.

        """
        tr    = FieldType.tr
        dt    = e.get_cte(FieldVariable.DELTAT)
        c55dt = self.c55 * dt

        for v in self._vars:
            aux  = e.auxiliar2(tr, v)
            aux += c55dt*e.residual(tr, v) #u5 += u5+c55*dt*rhs

# end of
# manticore.models.compressible.timeint.integrator.rkssp5_4_stage_4_complent


class rkssp5_4_stage_5(EntityOperator):

    def __init__(self, list_vars):
        super().__init__()
        self._vars = list_vars
        self.c56 = 0.226007483236906

    def container_operator(self, group, key, container): pass
    
    def __call__(self, e):
        """Applies the fifth stage of RKSSP5_4 at element e.

        Notes:
            * Every stage assumes e.residual<tr> was already (externaly) 
            computed at the appropriate intermediate state.

            * e.auxiliar1 equals the state at last integration time.

            * e.state equals the intermediate state at (N-1)th stage.

        """
        tr    = FieldType.tr
        dt    = e.get_cte(FieldVariable.DELTAT)
        c56dt = self.c56 * dt

        for v in self._vars:
            uhat  = e.auxiliar2(tr, v)
            uhat += c56dt*e.residual(tr, v) # u5 += u5+c56*dt*rhs
            np.copyto(e.state(tr, v), uhat) # uhat= u5

# end of manticore.models.compressible.timeint.integrator.rkssp5_4_stage_5


class SSP_5_4_RK(TimeIntegrator):
    """Strong Stability Preserving five stages, fourth order, explicit
    Runge Kutta time integration algotihm.
    """

    def __init__(self, model_desc, eq):
        super().__init__(model_desc, eq)

        self.rk1 = rkssp5_4_stage_1(   self._vars)
        self.rk2 = rkssp5_4_stage_N(2, self._vars)
        self.rk3 = rkssp5_4_stage_N(3, self._vars)
        self.rk4 = rkssp5_4_stage_N(4, self._vars)
        self.rk5 = rkssp5_4_stage_5(   self._vars)

        self.rk4_complement = rkssp5_4_stage_4_complement(self._vars)

        # Intermediate times in case something depends explicitly on time:
        # t_inside_stage_N = t + t_inc[stage-1] * dt
        self.t_inc = [0.,
                      0.39175222700392,
                      0.58607968896780,
                      0.47454236302687,
                      0.93501063100924]

    def rkssp5_4_step1(self, cm):

        # Pre condition: residual was already evaluated before entering
        # here (it is reasonable assuming that a convergence checking
        # was performed before calling a time integrator, which is
        # very expensive procedure): uhat and rhs_uhat are known

        logger.debug('RK Stage 1...')
        
        subdomain_by_name = SubDomainNameIterator(cm)
        
        for eset in self._esets:
            s = subdomain_by_name.find(eset)
            if not s:
                raise RuntimeError('Subdomain not found!!!')

            foreach_entity_in_subdomain(s, self.rk1) # uhat -> u1

        self.backward(cm)  # updating physical space to u1
        
    def rkssp5_4_step2(self, cm):

        logger.debug('RK Stage 2...')
        
        self._eq.compute_residual(self._desc, cm) # u1 -> rhs_u1

        subdomain_by_name = SubDomainNameIterator(cm)
        
        for eset in self._esets:
            s = subdomain_by_name.find(eset)
            if not s:
                raise RuntimeError('Subdomain not found!!!')

            foreach_entity_in_subdomain(s, self.rk2) # u1 -> u2

        self.backward(cm)  # updating physical space to u2
        
    def rkssp5_4_step3(self, cm):

        logger.debug('RK Stage 3...')
        
        self._eq.compute_residual(self._desc, cm) # u2 -> rhs_u2

        subdomain_by_name = SubDomainNameIterator(cm)
        
        for eset in self._esets:
            s = subdomain_by_name.find(eset)
            if not s:
                raise RuntimeError('Subdomain not found!!!')

            foreach_entity_in_subdomain(s, self.rk3) # u2 -> u3

        self.backward(cm)  # updating physical space to u3
        
    def rkssp5_4_step4(self, cm):

        logger.debug('RK Stage 4...')
        
        self._eq.compute_residual(self._desc, cm) # u3 -> rhs_u3

        subdomain_by_name = SubDomainNameIterator(cm)
        
        for eset in self._esets:
            s = subdomain_by_name.find(eset)
            if not s:
                raise RuntimeError('Subdomain not found!!!')

            foreach_entity_in_subdomain(s, self.rk4) # u3 -> u4
            foreach_entity_in_subdomain(s, self.rk4_complement)

        self.backward(cm)  # updating physical space to u4
        
    def rkssp5_4_step5(self, cm):

        logger.debug('RK Stage 5...')

        self._eq.compute_residual(self._desc, cm) # u4 -> rhs_u4

        subdomain_by_name = SubDomainNameIterator(cm)
        
        for eset in self._esets:
            s = subdomain_by_name.find(eset)
            if not s:
                raise RuntimeError('Subdomain not found!!!')

            foreach_entity_in_subdomain(s, self.rk5) # u4 -> uhat

        self.backward(cm)  # updating physical space to uhat

    def update_with_constant_step(self, cm):

        logger.debug('Time integration with constant time step...')

        time = ModelTime()  # global time
        t    = time.get()
        dt   = self._desc.get_time_settings().deltat

        # time = t 
        self.rkssp5_4_step1(cm)

        time.set(t+self.t_inc[1]*dt) # intermediate time
        self.rkssp5_4_step2(cm)

        time.set(t+self.t_inc[2]*dt) # intermediate time
        self.rkssp5_4_step3(cm)

        time.set(t+self.t_inc[3]*dt) # intermediate time
        self.rkssp5_4_step4(cm)

        time.set(t+self.t_inc[4]*dt) # intermediate time
        self.rkssp5_4_step5(cm)

    def update_with_variable_step(self, cm):
        
        logger.debug('Time integration with variable time step...')
        
        self.rkssp5_4_step1(cm)
        self.rkssp5_4_step2(cm)
        self.rkssp5_4_step3(cm)
        self.rkssp5_4_step4(cm)
        self.rkssp5_4_step5(cm)

# end of manticore.models.compressible.timeint.integrator.SSP_5_4_RK
