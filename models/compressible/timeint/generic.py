#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Generic operations over computational units of a mesh related to time
# integration.
#
# @addtogroup MODELS.COMPRESSIBLE.TIMEINT
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.nodal_cg.interpol import InterpolationKey
from manticore.lops.modal_dg.engine import EntityOperator
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.dgtypes import FieldType, FieldRole
from manticore.lops.modal_dg.class_instances import (
    class_quad_wadg_physforward,  class_quad_rwadg_physforward,
    class_tria_wadg_physforward,  class_tria_rwadg_physforward,
    class_quad_physbackward2d, class_tria_physbackward2d,
    class_quad_physevaluator2d, class_tria_physevaluator2d,
    class_quad_physevaluator1d, class_tria_physevaluator1d,
    class_quad_physintegrator2d, class_tria_physintegrator2d)
from manticore.models.compressible.services.predicatemixins import (
    model_description_mixin, equation_domain_signature )
from manticore.models.compressible.services.datatypes import (
    CFLBroadCast, element_value_pair )
from manticore.models.compressible.equations.workspaces import(
    PrimitiveWsp_v )

logger = manticore.logging.getLogger('MDL_CMP_TI')

class BackwardTransformation(EntityOperator):

    """Operator for updating physical volumetric values."""

    def __init__(self, list_vars):
        super().__init__()
        self.op = [None, None]
        self._vars = list_vars

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.op[0] = class_quad_physbackward2d(k)
        self.op[1] = class_tria_physbackward2d(k)

    def __call__(self, e):
        op = self.op[e.type]
        tr = FieldType.tr
        ph = FieldType.ph

        for v in self._vars:
            op(e.state(tr, v), e.state(ph, v))

class ForwardTransformation(EntityOperator):

    def __init__(self, list_vars):
        super().__init__()
        self.op  = [[None, None],[None, None]]
        self._vars = list_vars

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        ctx = EntityOperator.contexts

        #       type       subtype
        self.op[ctx[0][0]][ctx[0][1]] = class_quad_wadg_physforward(k)
        self.op[ctx[1][0]][ctx[1][1]] = class_quad_rwadg_physforward(k)
        self.op[ctx[2][0]][ctx[2][1]] = class_tria_wadg_physforward(k)
        self.op[ctx[3][0]][ctx[3][1]] = class_tria_rwadg_physforward(k)

    def __call__(self, e):
        op = self.op[e.type][e.subtype]
        tr = FieldType.tr
        ph = FieldType.ph
        J  = e.get_jacobian()
        IM = e.get_inv_mass()
        
        for v in self._vars:
            op(J, IM, e.state(ph, v), e.state(tr, v))
        
class CharacteristicLength(EntityOperator):

    """Fills the elements with their characteristic length.

    Notes:

        * The characteristic length is used for computing time step values 
            per element.

    """

    def __init__(self):
        super().__init__()

    def container_operator(self, group, key, container):
        self.nip = key.nip * key.nip
    
    def __call__(self, e):

        k = InterpolationKey.get_instance(e.geom_type, self.nip)

        char_length = e.vmap(k).char_length(e.get_coeff())

        e.set_cte(FieldVariable.CHAR_LENGTH, char_length)


class ConstantTimeStep(EntityOperator):

    """Sets the same \Delta t time increment value for all elements. """

    def __init__(self, dt):
        super().__init__()
        self.dt = dt
    
    def container_operator(self, group, key, container): pass

    def __call__(self, e):
        e.set_cte(FieldVariable.DELTAT, self.dt)

        
class VariableTimeStep(EntityOperator,
        model_description_mixin,
        equation_domain_signature):

    """Sets per element variable values of \Delta t time increment
    according to a global CFL value and the characteristic length of the 
    element.

    """

    def __init__(self, model_desc):
        model_description_mixin.__init__(self, model_desc)
        equation_domain_signature.__init__(self)
        EntityOperator.__init__(self)

        self.fluid = None
        self.a   = np.zeros(0)
        self.aux = np.zeros(0)
        self.mindt = element_value_pair(np.finfo(np.float64).max, -1)
        self.maxdt = element_value_pair(np.finfo(np.float64).min, -1)

    def setup(self, eqname, domain):
        
        equation_domain_signature.setup(self, eqname, domain)

        matname = self.desc.get_equation(eqname).mat

        self.fluid = self.desc.get_material(matname)

    def reset(self):
        self.maxdt._replace(value=np.finfo.min)
        self.mindt._replace(value=np.finfo.max)

    @property
    def max_dt(self):
        return self.maxdt

    @property
    def min_dt(self):
        return self.mindt

    def container_operator(self, group, key, container):
        self.k = ExpansionKeyFactory.make(key.order, key.nip)
        self.a.resize(   (self.k.n2d,), refcheck=False )
        self.aux.resize( (self.k.n2d,), refcheck=False )

    def __call__(self, e):
        
        # Filling the primitive variables workspace: u, v, p
        Primitive_v = PrimitiveWsp_v.get_instance(self.k.n2d)
        Primitive_v.fill(e, self.fluid) 
        rho  = e.state(FieldType.ph, FieldVariable.RHO)

        self.fluid.sonic_speed(rho, Primitive_v.p, self.a)

        cl  = e.get_cte(FieldVariable.CHAR_LENGTH)
        CFL = CFLBroadCast().get()

        # dt = (CFL*cl)*((np.reciprocal(np.sqrt(u*u+v*v) + a)).min())
        np.multiply(Primitive_v.u, Primitive_v.u, self.aux)
        self.aux += Primitive_v.v*Primitive_v.v
        np.sqrt(self.aux, self.aux)
        self.aux += self.a
        np.reciprocal(self.aux, self.aux)
        dt = (CFL*cl) * self.aux.min() / ((self.k.order+1)**2)
        e.set_cte(FieldVariable.DELTAT, dt)

        # logger.debug('e[%s].sonic_speed = \n%s' % (e.ID, self.a))
        # logger.debug('e[%s].(v+a).min = %s' % (e.ID, self.aux.min() ))
        # logger.debug('e[%s].cl = %s' % (e.ID, cl))
        # logger.debug('e[%s].dt = %s' % (e.ID, dt))

        if self.maxdt.value < dt:
            self.maxdt = element_value_pair(dt, e.ID)

        if self.mindt.value > dt:
            self.mindt = element_value_pair(dt, e.ID)
        

class norm_inf_residual(EntityOperator):

    def __init__(self, list_vars):
        super().__init__()
        self._vars = list_vars
        self.norm  = {}
        self.ph = np.zeros(0)
        self.bw = [None, None]

        for v in self._vars:
            self.norm[v] = element_value_pair( 0.0, 0 )

    def get_norm(self, var):
        return self.norm.get(var).value

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.bw[0] = class_quad_physbackward2d(k)
        self.bw[1] = class_tria_physbackward2d(k)
    
        nip = k.n2d
                
        if nip != self.ph.shape[0]:
            self.ph.resize( (nip,), refcheck=False )

    def __call__(self, e):

        bw = self.bw[e.type]
        tr = FieldType.tr

        for v in self.norm:
            bw(e.residual(tr, v), self.ph)
            maxres = np.linalg.norm(self.ph, np.inf)

            if self.norm[v].value < maxres:
                self.norm[v] = element_value_pair( maxres, e.ID )

                
class norm_2_residual(EntityOperator):

    def __init__(self, list_vars):
        super().__init__()
        self._vars = list_vars
        self.norm  = {}
        self.ph = np.zeros(0)
        self.bw = [None, None]

        for v in self._vars:
            self.norm[v] = 0.0

    def get_norm(self, var):
        return self.norm.get(var)

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.bw[0] = class_quad_physbackward2d(k)
        self.bw[1] = class_tria_physbackward2d(k)
    
        nip = k.n2d
                
        if nip != self.ph.shape[0]:
            self.ph.resize( (nip,), refcheck=False )

    def __call__(self, e):

        bw = self.bw[e.type]
        tr = FieldType.tr

        for v in self.norm:
            bw(e.residual(tr, v), self.ph)
            norm_root = np.linalg.norm(self.ph)
            self.norm[v] += norm_root*norm_root

class norm_residual(EntityOperator):

    def __init__(self, list_vars):
        super().__init__()
        self._vars = list_vars
        self.li_norm  = {}
        self.l2_norm  = {}
        self.ph = np.zeros(0)
        self.err = np.zeros(0)
        self.bw = [None, None]
        self.integ = [None, None]

        for v in self._vars:
            self.li_norm[v] = element_value_pair( 0.0, 0 )
            self.l2_norm[v] = 0.0

    def get_inf_norm(self, var):
        return self.li_norm.get(var).value

    def get_sq2_norm(self, var):
        return self.l2_norm.get(var)

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.bw[0] = class_quad_physbackward2d(k)
        self.bw[1] = class_tria_physbackward2d(k)

        self.integ[0] = class_quad_physintegrator2d(k)
        self.integ[1] = class_tria_physintegrator2d(k)
    
        nip = k.n2d
                
        if nip != self.ph.shape[0]:
            self.ph.resize( (nip,), refcheck=False )
            self.err.resize( (nip,), refcheck=False )

    def __call__(self, e):

        bw = self.bw[e.type]
        tr = FieldType.tr
        integ = self.integ[e.type]

        for v in self._vars:
            bw(e.residual(tr, v), self.ph)
            maxres    = np.linalg.norm(self.ph, np.inf)
            # norm_root = np.linalg.norm(self.ph)
            
            if self.li_norm[v].value < maxres:
                self.li_norm[v] = element_value_pair( maxres, e.ID )

            # self.l2_norm[v] += norm_root*norm_root
            np.power(self.ph, 2., out=self.err)
            self.l2_norm[v] += integ(self.err, e.get_jacobian()) / e.volume
            

class CheckError(EntityOperator):

    def __init__(self, func, list_vars):
        super().__init__()
        self._vars = list_vars
        self.f = func
        self.err = np.zeros(0)
        self.eval = [None, None]
        self.integ = [None, None]
        self.l2_norm  = {}
        self.l2_cont  = {}

        for v in self._vars:
            self.l2_norm[v] = 0.0
            self.l2_cont[v] = 0.0

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.eval[0] = class_quad_physevaluator2d(k)
        self.eval[1] = class_tria_physevaluator2d(k)

        self.integ[0] = class_quad_physintegrator2d(k)
        self.integ[1] = class_tria_physintegrator2d(k)
    
        nip = k.n2d
                
        if nip != self.err.shape[0]:
            self.err.resize( (nip,), refcheck=False )

    def __call__(self, e):
        ev = self.eval[e.type]
        integ = self.integ[e.type]
        ph = FieldType.ph

        rho_ex = ev(self.f.rho, e.get_coeff(), e.geom_type, e.vmap)
        rho    = e.state(ph, FieldVariable.RHO)
        np.subtract(rho_ex, rho, self.err)
        self.l2_norm[FieldVariable.RHO] += np.dot(self.err, self.err)
        self.l2_cont[FieldVariable.RHO] += integ(self.err**2, e.get_jacobian())

        rhou_ex = ev(self.f.rhou, e.get_coeff(), e.geom_type, e.vmap)
        rhou    = e.state(ph, FieldVariable.RHOU)
        np.subtract(rhou_ex, rhou, self.err)
        self.l2_norm[FieldVariable.RHOU] += np.dot(self.err, self.err)
        self.l2_cont[FieldVariable.RHOU] += integ(self.err**2, e.get_jacobian())

        rhov_ex = ev(self.f.rhov, e.get_coeff(), e.geom_type, e.vmap)
        rhov    = e.state(ph, FieldVariable.RHOV)
        np.subtract(rhov_ex, rhov, self.err)
        self.l2_norm[FieldVariable.RHOV] += np.dot(self.err, self.err)
        self.l2_cont[FieldVariable.RHOV] += integ(self.err**2, e.get_jacobian())
            
        rhoe_ex = ev(self.f.rhoe, e.get_coeff(), e.geom_type, e.vmap)
        rhoe    = e.state(ph, FieldVariable.RHOE)
        np.subtract(rhoe_ex, rhoe, self.err)
        self.l2_norm[FieldVariable.RHOE] += np.dot(self.err, self.err)
        self.l2_cont[FieldVariable.RHOE] += integ(self.err**2, e.get_jacobian())

        
class ExtractPoints:
    def F(x, y):
        return x, y


class WriteFields(EntityOperator):

    def __init__(self, file_txt):
        super().__init__()
        self._file = file_txt
        self.f = ExtractPoints
        self.eval = [None, None]
        self.eval_f = [None, None]        
        self.values = ""

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.eval[0] = class_quad_physevaluator2d(k)
        self.eval[1] = class_tria_physevaluator2d(k)

        self.eval_f[0] = class_quad_physevaluator1d(k)
        self.eval_f[1] = class_tria_physevaluator1d(k)

        self.nip = k.n2d
                
    def __call__(self, e):
        ev = self.eval[e.type]
        ph = FieldType.ph

        # Integration points in global coordinates
        x,y  = ev(self.f.F, e.get_coeff(), e.geom_type, e.vmap)

        # Values inside the element
        rho  = e.state(ph, FieldVariable.RHO )
        rhou = e.state(ph, FieldVariable.RHOU)
        rhov = e.state(ph, FieldVariable.RHOV)
        rhoe = e.state(ph, FieldVariable.RHOE)

        for i in range(x.shape[0]):
            self._file.write('%s, %s, %s, %s, %s, %s\n' % (
                x[i], y[i], rho[i], rhou[i], rhov[i], rhoe[i]) )

        face_sz = e.face_comm_sizes
        ev_face = self.eval_f[e.type]
        state   = FieldRole.State

        # Values on faces
        for ff,sz in enumerate(face_sz):
            x,y = ev_face(ff, self.f.F, e.get_coeff(), e.geom_type, e.fmap)
            
            rho  = np.zeros(sz)
            rhou = np.zeros(sz)
            rhov = np.zeros(sz)
            rhoe = np.zeros(sz)

            e.get_face_field(state, FieldVariable.RHO,  ff, rho)
            e.get_face_field(state, FieldVariable.RHOU, ff, rhou)
            e.get_face_field(state, FieldVariable.RHOV, ff, rhov)
            e.get_face_field(state, FieldVariable.RHOE, ff, rhoe)

            for i in range(x.shape[0]):
                self._file.write('%s, %s, %s, %s, %s, %s\n' % (
                    x[i], y[i], rho[i], rhou[i], rhov[i], rhoe[i]) )

