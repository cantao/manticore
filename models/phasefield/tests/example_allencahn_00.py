#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/models/phasefield/tests/example_allencahn_00.py -ll DEBUG
# or
#
# $ python3 -m cProfile -o prof manticore/models/phasefield/tests/example_allencahn_00.py 
#
import numpy as np
#
# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import manticore

##
from collections import OrderedDict
from math import sqrt, pi
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.tests.simple_meshes import mesh02
from manticore.lops.modal_dg.dgtypes import EntityRole, FieldType
from manticore.lops.modal_dg.entity import GlobalRoleIterator
from manticore.lops.modal_dg.computemesh import (
    SubDomainRoleIterator, SubDomainNameIterator )
from manticore.lops.modal_dg.engine import foreach_entity_in_subdomain
from manticore.lops.modal_dg.entityops import (
    ComputeEntityVolume, ComputeEntityMassMatrix,
    InitializeFields, InitFieldValues )
from manticore.models.phasefield.tests.test_helpers import (
    simple_model_01 )
from manticore.models.phasefield.equations.manager import EquationManager
from manticore.models.phasefield.description.model import periodicbc_pair
# from manticore.models.compressible.timeint.generic import (
#     CheckError, WriteFields )

#
# Importing generic entities temporalily placed under models/compressible
from manticore.models.compressible.services.datatypes import (
    ModelTime )
from manticore.models.compressible.timeint.generic import (
    BackwardTransformation, CharacteristicLength, ConstantTimeStep )
from manticore.models.compressible.timeint.integrator import SSP_5_4_RK

# def condicao_inicial(x, y):
# 	return x+y
	
def main():
    logger = manticore.logging.getLogger('MTC_LOGGER')

    logger.info("Running example Allen-Cahn 00")

    #
    # Generating mesh
    n_side = 8
    cm = mesh02(l=2.*pi, n=n_side, p_order=1)

    for s in cm:
        logger.debug("Subdomain: %s Name: %s Role: %s" % (s.ID, s.name, s.role))

    #
    # Lists for iterating over physical elements
    elist = GlobalRoleIterator(cm.container).find(EntityRole.PHYSICAL)
    subds = SubDomainRoleIterator(cm).find(EntityRole.PHYSICAL)

    #
    # Write ascii file for post-processing
    filename = 'square{}.txt'.format(len(elist))
    myfile=open(filename,'w')
    for s in subds:
        s.write_to_txt(myfile)
    myfile.close()
    #

    logger.info(
        "Evaluating jacobians of %s elements..." % len(elist))
    for e in elist:
        e.eval_jacobian()

    logger.info("Evaluating mass matrices...")
    mass_op = ComputeEntityMassMatrix()
    for s in subds:            
        foreach_entity_in_subdomain(s, mass_op)

    logger.info("Evaluating physical elements' volumes...")
    vol_op = ComputeEntityVolume()
    for s in subds:            
        foreach_entity_in_subdomain(s, vol_op)

    # Model description
    logger.info("Setting up problem description...")
    model_desc = simple_model_01()

    # Periodic bc pairs definition: it is completely dependant to
    # mesh02 construction
    n = n_side

    # pair: FACE_0->FACE_1
    fset = model_desc.get_faceset('FACE_0') 
    phys = range(n)                         # Physical neighbors of FACE_0
    pair = np.arange((n - 1) * n, n * n, 1) # Physical neighbors of FACE_1
    for i in range(n):
        e = cm.get_entity(phys[i])  # physical neighbor, just to find 'ghost'
        ghost  = e.get_neighbour(0) # ghost whose ID is necessary
        source = cm.get_entity(pair[i]) # Periodic source
        fset.map[ghost.ID] = periodicbc_pair(source, 1)

    # pair: FACE_1->FACE_0
    fset = model_desc.get_faceset('FACE_1')
    phys = np.arange((n - 1) * n, n * n, 1) # Physical neighbors of FACE_1
    pair = range(n)                         # Physical neighbors of FACE_0
    for i in range(n):
        e = cm.get_entity(phys[i])  # physical neighbor, just to find 'ghost'
        ghost  = e.get_neighbour(1) # ghost whose ID is necessary
        source = cm.get_entity(pair[i]) # Periodic source
        fset.map[ghost.ID] = periodicbc_pair(source, 0)

    # pair: FACE_2->FACE_3
    fset = model_desc.get_faceset('FACE_2') 
    phys = np.arange(0, n * n, n)                # Physical neighbors of FACE_2
    pair = np.arange(n - 1, (n + 2) * (n - 1),n) # Physical neighbors of FACE_3
    for i in range(n):
        e = cm.get_entity(phys[i])  # physical neighbor, just to find 'ghost'
        ghost  = e.get_neighbour(2) # ghost whose ID is necessary
        source = cm.get_entity(pair[i]) # Periodic source
        fset.map[ghost.ID] = periodicbc_pair(source, 3)

    # pair: FACE_3->FACE_2
    fset = model_desc.get_faceset('FACE_3')
    phys = np.arange(n - 1, (n + 2) * (n - 1),n) # Physical neighbors of FACE_3 
    pair = np.arange(0, n * n, n)                # Physical neighbors of FACE_2
    for i in range(n):
        e = cm.get_entity(phys[i])  # physical neighbor, just to find 'ghost'
        ghost  = e.get_neighbour(3) # ghost whose ID is necessary
        source = cm.get_entity(pair[i]) # Periodic source
        fset.map[ghost.ID] = periodicbc_pair(source, 2)

    #
    logger.info('Initializing the Allen-Cahn equation...')
    eqs = EquationManager()
    eqs.set_equations(model_desc)

    # Initializing fields storage
    logger.info('Initializing fields storage...')

    subdomain_by_name = SubDomainNameIterator(cm)

    # Physical elements
    elset_nodes = model_desc.get_elementsets()
    for elset_node in elset_nodes:
        elset = elset_node.data
        state_vars    = eqs.state_variables_in_elset(elset)
        residual_vars = eqs.residual_variables_in_elset(elset)
        standard_vars = eqs.standard_variables_in_elset(elset)

        s = subdomain_by_name.find(elset.name)
            
        if not s:
            raise RuntimeError('Subdomain not found!!!')

        stt = state_vars.get()
        rsd = residual_vars.get()
        cte = standard_vars.get()

        init_fields = InitializeFields(stt, rsd, cte, auxiliary=rsd)
    
        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)


    # Ghost elements
    fset_nodes = model_desc.get_facesets()
    for fset_node in fset_nodes:
        fset = fset_node.data

        s = subdomain_by_name.find(fset.name)

        if not s:
            raise RuntimeError('Face set not found!!!')

        left_name = fset.left # name of the physical neighbour
        elset = model_desc.get_elementset(left_name)
        state_vars    = model_eqs.state_variables_in_elset(elset)
        residual_vars = model_eqs.residual_variables_in_elset(elset)

        stt = state_vars.get()
        rsd = residual_vars.get()

        init_fields = InitializeFields(stt, rsd)

        logger.debug('Initializing fields in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_fields)      

    # Initial condition
    time_settings = model_desc.get_time_settings()
    time_init     = time_settings.time_init
    time_glb      = ModelTime()
    time_glb.set(time_init)

    ######################
    #condicao_inicial = funcao_a_ser_definida()
    init_u_values = InitFieldValues(FieldVariable.PHASESTATE, condicao_inicial)
    ######################

    logger.info('Initializing fiels values from analytical expression...')
    for s in subds: 
        logger.debug('Initializing U values in subdomain %s...' % (s.name))
        foreach_entity_in_subdomain(s, init_u_values)

    logger.info("Evaluating characteristic lengths...")
    cl_op = CharacteristicLength()
    for s in subds:            
        foreach_entity_in_subdomain(s, cl_op)

    logger.info('Setting time setps...')
    cte_dt = ConstantTimeStep(model_desc.get_time_settings().deltat)
    for s in subds:
        foreach_entity_in_subdomain(s, cte_dt)

    t_now = time_settings.time_init
    dt    = time_settings.deltat
    t_max = time_settings.time_max
    t_fsv = time_settings.time_save

    tints = OrderedDict()
    for eq_name, eq in eqs:
        tints[eq_name] = SSP_5_4_RK(model_desc, eq)

    logger.info('MAIN TIME-INTEGRATION LOOP...')
    while (t_now < t_max):
        logger.info('INSTANT %s...' % (t_now))

        logger.debug('Computing the residual of the current state...')
        for eq_name, eq in eqs:
            eq.compute_residual(model_desc, cm)
        
        for eq_name in tints:
            logger.debug('Time step in equation %s...' % (eq_name))
            tints[eq_name].update_state(cm)

        t_now += dt
        time_glb.set(t_now)

    # logger.info('Checking error...')
    # func = example_warburton_book_6p1(x0=5.,y0=0.,beta=5.,gamma=g,t=t_now)
    # check_error = CheckError(func, stt)
    # for s in subds:            
    #     foreach_entity_in_subdomain(s, check_error)

    # logger.info('Discrete l2:')
    # for v in stt:
    #     logger.info('Error in %s = %s' % (v, sqrt(check_error.l2_norm[v])))

    # logger.info('Continuous l2:')
    # for v in stt:
    #     logger.info('Error in %s = %s' % (v, sqrt(check_error.l2_cont[v])))


    logger.info('Writing response fields...')
    # Write ascii file for post-processing
    e=cm.get_entity(0)
    order = e.key.order
    filename = 'square{}-p{}-results.txt'.format(len(elist),order)
    myfile=open(filename,'w')
    
    write_fields = WriteFields(myfile)
    for s in subds:            
        foreach_entity_in_subdomain(s, write_fields)
        
    myfile.close()
    #

    logger.info("Example ended!\n")

    # end of main

#-------------------------------------------------------------------------------


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Allen-Cahn example')
    parser.add_argument(
        '-ll',
        '--loglevel',
        type=str,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        default='CRITICAL',
        help='Set the logging level')

    args = parser.parse_args()

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format':
                '[%(asctime)s %(name)s::%(levelname)s::%(funcName)s at '
                '%(filename)s:%(lineno)d] %(message)s ',
                'datefmt':
                "%Y-%m-%d %H:%M:%S",
                }
            },
            'handlers': {
                'console': {
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                },
                'file':{
                    'level': args.loglevel,
                    'formatter': 'standard',
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': 'example_euler_00.log',
                    'mode': 'a',
                    'maxBytes': 1048576,
                    'backupCount': 3,
                },
            },
            'loggers': {
                '': {
                    'handlers': ['file', 'console'],
                    'level': args.loglevel,
                },
            }
        }

    # manticore.__init__.py imports logging and sets a project's
    # default configuration. This default is being overwritten here.
    manticore.logging.config.dictConfig(LOGGING)

    # Just executing main with the preceding logging setup
    main()

    manticore.logging.shutdown()
