﻿#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
from manticore.services.fieldvariables import FieldVariable
from manticore.mdf.enums import MapType
from manticore.models.phasefield.description.model import (
    ModelDescription, TransportData, EqType, FaceType )
#
#
from manticore.models.compressible.description.mdtypes import (
    EqParameter, EqSetting, TISetting)

def simple_model_01():
    """Model description for executing a sample phase field problem.
    """

    # File names
    meshfile = "platequad_o1.tdf"
    initfile = "platequad_o1-init.tdf"
    resufile = "platequad_o1-01-res.tdf"
    restfile = "platequad_o1-01-restart.tdf"
    # Material
    mat = { 'name':'Alloy-const-data', 'data': {'epsilon': 0.18, 'mu': 1.0} }
    # Equation
    eqname = ['reaction-diffusion']
    eqtype = [EqType.ALLENCAHN]
    eqs = dict(zip(eqname, eqtype))
    var = [ FieldVariable.PHASESTATE ]
    stdlim = {'min': 1.0e4, 'max': 1.0e15}
    conlim = {'min': 1.0e-12, 'max': 1.0e15}
    # Domain
    subd = ['FLUID']
    fname = ["FACE_0", "FACE_1", "FACE_2", "FACE_3"]
    ftype = [
        FaceType.PERIODIC, FaceType.PERIODIC,
        FaceType.PERIODIC, FaceType.PERIODIC
        ]
    fset = dict(zip(fname, ftype))
    fparam = dict.fromkeys(ftype)
    # Time integration
    time = {
        'tacc': TISetting.TACC_STEADYSTATE,
        'it_max': 20000,
        'it_save': 500,
        'stop': TISetting.STOPCRITERIA_MAX,
        'type': TISetting.TSTEP_CONSTANT,
        'cfl_type': TISetting.CFL_CONSTANT,
        'dt': 0.2,
        'tint': TISetting.INTEGRATOR_SSP54RK
    }

    model = ModelDescription()

    # Set input/output files through Tree's interface
    model.data.get_node('MESH').data    = meshfile
    model.data.get_node('INITIAL').data = initfile
    model.data.get_node('RESULTS').data = resufile
    model.data.get_node('RESTART').data = restfile
    model.data.get_node('INITIAL_TYPE').data = MapType.PER_INTP

    # Set material
    material = TransportData()
    material._epsilon = mat['data']['epsilon']
    material._mu      = mat['data']['mu']
    model.init_material(mat['name'], material)

    # Setting equation: (name, type)
    model.init_equation(eqname[0], eqtype[0])

    # Getting a reference to a specific equation description
    # stored as a Node object's data:
    eq = model.data.get_node(eqname[0]).data

    # Setting limits on state variables
    for v in var:
        eq.set_limits(v, stdlim['min'], stdlim['max'])

    eq.set_limits(var[0], conlim['min'], conlim['max'])

    # Setting the material associated to this equation
    eq.mat = mat['name']

    # Setting equation's parameters
    eq.set_data(EqParameter.PRINCIPAL_DGFORM, EqSetting.WEAKFORM)
    eq.set_data(EqParameter.DAUXILIAR_DGFORM, EqSetting.WEAKFORM)
    eq.set_data(EqParameter.DISSIPATIVE_FLUX, EqSetting.VISCOUS_BR1)

    #Set domain
    # Create sudomains
    for s in subd:
        model.init_elementset(s)

    # Set data of one subdomain
    sd = model.get_elementset(subd[0])

    sd.add_equation(eqname[0])

    for f in fset.keys():  # face sets
        sd.add_faceset(f)

    # Create boundaries
    for f in fset:
        model.init_faceset(f, fset[f])

    fs = model.get_facesets()

    for f_node in fs:
        # Acessing tree's node contents
        f = f_node.data
            
        # Standard bcs only required 'left' but periodic bcs also
        # have data on the right.
        f.set_neighbours(
            left=ModelTestCase.subd[0], right=ModelTestCase.subd[0]) 

            # Defining periodic pairs
            if f.name == 'FACE_0':
                f.data.pair = 'FACE_1'
            elif f.name == 'FACE_1':
                f.data.pair = 'FACE_0'
            elif f.name == 'FACE_2':
                f.data.pair = 'FACE_3'
            elif f.name == 'FACE_3':
                f.data.pair = 'FACE_2'
        

    model.init_time_settings(time['tacc'])
    time_settings = model.get_time_settings()
    time_settings.time_init = time['t_init']
    time_settings.time_max  = time['t_max']
    time_settings.time_save = time['t_save']
    time_settings.deltat    = time['dt']


    model.init_time_integration(eqname[0], time['tint'])

    return model

