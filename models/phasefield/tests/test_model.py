#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# DESCRIPTION tests
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#
# $ python3 -m unittest -v manticore/models/phasefield/tests/test_model.py

import unittest
from manticore.services.fieldvariables import FieldVariable
from manticore.mdf.enums import MapType
from manticore.models.phasefield.description.model import (
    ModelDescription, TransportData, EqType, FaceType )
#
#
from manticore.models.compressible.description.mdtypes import (
    EqParameter, EqSetting, TISetting)

class ModelTestCase(unittest.TestCase):
    # Data for the model

    # File names
    meshfile = "platequad_o1.tdf"
    initfile = "platequad_o1-init.tdf"
    resufile = "platequad_o1-01-res.tdf"
    restfile = "platequad_o1-01-restart.tdf"
    # Material
    mat = { 'name':'Alloy-const-data', 'data': {'epsilon': 0.18, 'mu': 1.0} }
    # Equation
    eqname = ['reaction-diffusion']
    eqtype = [EqType.ALLENCAHN]
    eqs = dict(zip(eqname, eqtype))
    var = [ FieldVariable.PHASESTATE ]
    stdlim = {'min': 1.0e4, 'max': 1.0e15}
    conlim = {'min': 1.0e-12, 'max': 1.0e15}
    eqparam = [
        EqParameter.PRINCIPAL_DGFORM,
        EqParameter.DAUXILIAR_DGFORM,
        EqParameter.DISSIPATIVE_FLUX
        ]
    eqsetting = [
        EqSetting.WEAKFORM,
        EqSetting.WEAKFORM,
        EqSetting.VISCOUS_BR1
        ]
    # Domain
    subd = ['metal']
    fname = ["FACE_0", "FACE_1", "FACE_2", "FACE_3"]
    ftype = [
        FaceType.PERIODIC, FaceType.PERIODIC,
        FaceType.PERIODIC, FaceType.PERIODIC
        ]
    fset = dict(zip(fname, ftype))
    fparam = dict.fromkeys(ftype)

    # Time integration
    time = {
        'tacc': TISetting.TACC_STEADYSTATE,
        'it_max': 20000,
        'it_save': 500,
        'stop': TISetting.STOPCRITERIA_MAX,
        'type': TISetting.TSTEP_CONSTANT,
        'cfl_type': TISetting.CFL_CONSTANT,
        'dt': 0.2,
        'tint': TISetting.INTEGRATOR_SSP54RK
    }

    def setUp(self):
        # Create empty model
        self.model = ModelDescription()

        # Set input/output files through Tree's interface
        self.model.data.get_node('MESH').data = ModelTestCase.meshfile
        self.model.data.get_node('INITIAL').data = ModelTestCase.initfile
        self.model.data.get_node('RESULTS').data = ModelTestCase.resufile
        self.model.data.get_node('RESTART').data = ModelTestCase.restfile
        self.model.data.get_node('INITIAL_TYPE').data = MapType.HIERARCHICAL


        # Set material

        # Insert Node with ID ModelTestCase.mat[0] as MATERIALS's
        # child using Tree's interface
        material = TransportData()
        material._epsilon = ModelTestCase.mat['data']['epsilon']
        material._mu      = ModelTestCase.mat['data']['mu']
        self.model.init_material(ModelTestCase.mat['name'], material)

        # Setting equation: (name, type)
        
        self.model.init_equation(ModelTestCase.eqname[0],
                                 ModelTestCase.eqtype[0])

        # Getting a reference to a specific equation description
        # stored as a Node object's data:
        eq = self.model.data.get_node(ModelTestCase.eqname[0]).data

        # Setting limits on state variables
        for v in ModelTestCase.var:
            eq.set_limits(v,
                          ModelTestCase.stdlim['min'],
                          ModelTestCase.stdlim['max'])

        eq.set_limits(ModelTestCase.var[0],
                      ModelTestCase.conlim['min'],
                      ModelTestCase.conlim['max'])

        # Setting the material associated to this equation
        eq.mat = ModelTestCase.mat['name']

        # Setting equation's parameters
        eq.set_data(ModelTestCase.eqparam[0], ModelTestCase.eqsetting[0])
        eq.set_data(ModelTestCase.eqparam[1], ModelTestCase.eqsetting[1])
        eq.set_data(ModelTestCase.eqparam[2], ModelTestCase.eqsetting[2])

        #Set domain
        # Create sudomains
        for s in ModelTestCase.subd:
            self.model.init_elementset(s)

        # Set data of one subdomain
        sd = self.model.data.get_node(ModelTestCase.subd[0]).data

        sd.add_equation(ModelTestCase.eqname[0])  # equations

        for f in ModelTestCase.fset.keys():  # face sets
            sd.add_faceset(f)

        # Create boundaries
        for f in ModelTestCase.fset:
            self.model.init_faceset(f, ModelTestCase.fset[f])

        fs = self.model.get_facesets()

        for f_node in fs:
            # Acessing tree's node contents
            f = f_node.data
            
            # Standard bcs only required 'left' but periodic bcs also
            # have data on the right.
            f.set_neighbours(
                left=ModelTestCase.subd[0], right=ModelTestCase.subd[0]) 

            # Defining periodic pairs
            if f.name == 'FACE_0':
                f.data.pair = 'FACE_1'
            elif f.name == 'FACE_1':
                f.data.pair = 'FACE_0'
            elif f.name == 'FACE_2':
                f.data.pair = 'FACE_3'
            elif f.name == 'FACE_3':
                f.data.pair = 'FACE_2'


        self.model.init_time_settings(ModelTestCase.time['tacc'])
        time_settings = self.model.get_time_settings()
        time_settings.it_max = ModelTestCase.time['it_max']
        time_settings.it_save = ModelTestCase.time['it_save']
        time_settings.stop_criteria = ModelTestCase.time['stop']
        time_settings.tstep.set_type(ModelTestCase.time['type'])
        time_settings.tstep.set_deltat(ModelTestCase.time['dt'])

        self.model.init_time_integration(
            ModelTestCase.eqname[0], ModelTestCase.time['tint'])


    def test_equations(self):
        eq = self.model.get_equations()

        # Test storage of equations
        self.assertEqual(len(eq), len(ModelTestCase.eqname))

        # Test types of equations
        for i in range(len(eq)):
            self.assertEqual(
                self.model.data.get_node(ModelTestCase.eqname[i]).data.type,
                ModelTestCase.eqtype[i])

        # Test limits
        limits = (ModelTestCase.conlim['min'], ModelTestCase.conlim['max'])
        e = self.model.data.get_node(ModelTestCase.eqname[0]).data
        self.assertEqual(e.get_limits(ModelTestCase.var[0]), limits)

        # Test Parameters
        self.assertTrue(ModelTestCase.eqparam[0] in e.data)
        self.assertTrue(ModelTestCase.eqparam[1] in e.data)
        self.assertTrue(ModelTestCase.eqparam[2] in e.data)

        # Test materials
        self.assertEqual(e.mat, ModelTestCase.mat['name'])

    # def tearDown(self):
    #     self.model.data.show()

    def test_subdomains(self):
        vs = self.model.get_elementsets()

        self.assertEqual(len(vs), len(ModelTestCase.subd))

        for s in ModelTestCase.subd:
            self.assertTrue(self.model.data.contains(s))

        for v in vs:
            self.assertTrue(v.data.name in ModelTestCase.subd)

        sd = self.model.data.get_node(ModelTestCase.subd[0]).data

        for f in ModelTestCase.fset.keys():
            self.assertTrue(f in sd.facesets)

        self.assertTrue(sd.equations[0] in ModelTestCase.eqname)

    def test_boundaries(self):
        fs = self.model.get_facesets()

        self.assertEqual(len(fs), len(ModelTestCase.ftype))

    def test_solution(self):
        time_settings = self.model.get_time_settings()
        self.assertEqual(time_settings.it_max, ModelTestCase.time['it_max'])
        self.assertEqual(time_settings.it_save, ModelTestCase.time['it_save'])
        self.assertEqual(time_settings.stop_criteria,
                         ModelTestCase.time['stop'])
        self.assertEqual(time_settings.tstep.type,
                         ModelTestCase.time['type'])
        self.assertAlmostEqual(time_settings.tstep.deltat,
                               ModelTestCase.time['dt'])

        self.assertEqual(
            self.model.get_time_integration(ModelTestCase.eqname[0]),
            ModelTestCase.time['tint'])


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_model.py ------------------------------------------------------------
