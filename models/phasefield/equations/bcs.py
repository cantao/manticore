#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Boundary conditions
#
# @addtogroup MODELS.PHASEFIELD.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore
import manticore.services.const as const
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import FieldType, FieldRole, EntityRole
from manticore.lops.modal_dg.engine import (
    EntityOperator, foreach_entity_in_subdomain)
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.computemesh import SubDomainRoleIterator
from manticore.lops.modal_dg.class_instances import (
    class_quad_physevaluator1d, class_tria_physevaluator1d)
from manticore.models.phasefield.description.model import (
    EqType, FaceType)
#
#
from manticore.models.compressible.services.predicatemixins import (
    model_description_mixin, equation_domain_signature)
from manticore.models.compressible.services.datatypes import (
    ModelTime )
# #
# #
# from manticore.models.compressible.directives import ExecDirective
# #
# # Execution accordingly to command line directives
# exec_dir = manticore.models.compressible.directive

# if exec_dir==ExecDirective.NULL_DIRICHLET:
#     from manticore.models.compressible.equations.dirichlet_bcs import (
#         null_dirichlet )

# elif exec_dir==ExecDirective.WARBURTON_EX_6_1:
#     from manticore.models.compressible.tests.test_helpers import (
#         example_warburton_book_6p1 )
    
# elif exec_dir==ExecDirective.CTE_EULER_FIELDS:
#     from manticore.models.compressible.tests.test_helpers import (
#         constant_euler_fields )

# elif exec_dir==ExecDirective.NS_VALIDATION:
#     from manticore.models.compressible.tests.test_helpers import (
#         ns_validation )
# #

class BC_Base(model_description_mixin, equation_domain_signature):

    def __init__(self, model_desc):
        model_description_mixin.__init__(self, model_desc)
        equation_domain_signature.__init__(self)

        self.uI = np.zeros(0)

    def setup(self, eqname, domain):
        
        equation_domain_signature.setup(self, eqname, domain)    

    def resize(self, nip):
        self.uI.resize((nip,), refcheck=False)

    def do_neighbour_reconstruction(self, ng, ng_face_id, mat):

        # B.C.s are applied on ghosts. Thus their neighbours are PHYSICAL.
        assert ng.role==EntityRole.PHYSICAL

        state = FieldRole.State

        # Computation of values on boundary face
        ng.get_face_field(state, FieldVariable.PHASESTATE, ng_face_id, self.uI)


# end of manticore.models.phasefield.equations.bcs.BC_Base


class dirichlet_bc(EntityOperator):
    def __init__(self, f):
        EntityOperator.__init__(self)
        self.f = f
        self.op = [None, None]

    def setup(self, eqname, domain):
        self.equation = eqname
        self.domain   = domain

    def container_operator(self, group, key, container):
        k = ExpansionKeyFactory.make(key.order, key.nip)
        
        self.op[0] = class_quad_physevaluator1d(k)
        self.op[1] = class_tria_physevaluator1d(k)

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)
        
        # Finally, the boundary condition imposition
        u = e.state(FieldType.ph, FieldVariable.PHASESTATE)

        np.copyto( u, self.op[ng.type](
            ng_face_id, self.f.u, ng.get_coeff(),  ng.geom_type, ng.fmap) )
        
# end of manticore.models.phasefield.equations.bcs.dirichlet_bc


class periodic_bc(EntityOperator, BC_Base):
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

    def setup(self, eqname, domain):
        BC_Base.setup(self, eqname, domain)

        # In the case of ghosts domainName corresponds to the name of
        # the original face set that gave birth to the ghost set.        
        fsdata = self.desc.get_faceset(domain).data

        self.periodic_map = fsdata.map  # e.ID->(e_ID, eface_ID)

    def container_operator(self, group, key, container): pass

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)

        # Periodic pair: "neighbour on the right"
        periodic_pair    = self.periodic_map.get(e.ID)
        periodic_ng      = periodic_pair.element # periodic_pair[0]
        periodic_face_id = periodic_pair.face    # periodic_pair[1]

        # Is the periodic pair a physical element?
        assert periodic_ng.role==EntityRole.PHYSICAL

        state = FieldRole.State

        # Accessing ghost storage
        u = e.state(FieldType.ph, FieldVariable.PHASESTATE)

        # Computation of values on boundary face: for a periodic
        # boundary condition, each ghost acts only as a temporary
        # storage for values reconstructed "on the right" of the face;
        # the element "on the left" is the physical element associated
        # to the ghost (active element); the element "on the right" is
        # the physical element of the periodic pair.
        periodic_ng.get_face_field_as_passive(
            state,
            FieldVariable.PHASESTATE, # field
            ng.type, ng_face_id, nip, # active element's data
            periodic_face_id,         # passive element's data
            u )                       


# end of manticore.models.phasefield.equations.bcs.periodic


class symmetry_bc(EntityOperator, BC_Base):
    def __init__(self, model_desc):
        BC_Base.__init__(self, model_desc)
        EntityOperator.__init__(self)

    def setup(self, eqname, domain):
        
        BC_Base.setup(self, eqname, domain)

        matname = self.desc.get_equation(eqname).mat

        self.mat = self.desc.get_material(matname)

    def container_operator(self, group, key, container): pass

    def resize(self, nip):
        BC_Base.resize(self, nip)

    def __call__(self, e):

        # Check setup was done
        assert self.domain

        # Am I a ghost?
        assert e.role == EntityRole.GHOST

        # Now, my neighbour (ghosts always have 0 index neighbour)
        ng = e.get_neighbour(0)

        # Is the neighbour a physical element?
        assert ng.role == EntityRole.PHYSICAL

        # From which face are we attached?
        ng_face_id = e.get_neighbour_face_id(0)
        nip        = ng.face_comm_size(ng_face_id)
        self.resize(nip)

        # Physical element (internal) reconstruction
        self.do_neighbour_reconstruction(ng, ng_face_id, self.mat)

        # Finally, the boundary condition imposition
        u = e.state(FieldType.ph, FieldVariable.PHASESTATE)

        np.copyto(u, self.uI)

# end of manticore.models.phasefield.equations.bcs.symmetry_bc


class UpdateBC:
    """Update values on ghost elements."""

    def __init__(self, model_desc):
        self._desc      = model_desc
        self._bc_sym    = symmetry_bc(model_desc)
        self._bc_perio  = periodic_bc(model_desc)

        self._bc_selector = {
            FaceType.SYMMETRY: self._bc_sym,
            FaceType.PERIODIC: self._bc_perio
            }

    def update(self, cm):

        # Access list of GHOST subdomains 
        subds = SubDomainRoleIterator(cm).find(EntityRole.GHOST)

        # The b.c's specified in self._bc_selector deal with
        # PHASESTATE for the equations listed below:
        allowed_types = [EqType.ALLENCAHN]

        # Additional state variable will require additional
        # b.c. classes as well as addition lists of "allowed_types"
        # and additional "elif" conditional executions below.

        for s in subds:
            
            # Each ghost subdmain inherits the name of the respective
            # face set on the boundary associated to a boundary
            # condition. From the face set's name we access the
            # element set 'on the left' of the face set which is the
            # internal, physical set of elements adjacent to the face
            # set.
            lvs = self._desc.get_faceset(s.name).left
            eqs = self._desc.get_data(lvs).equations

            for eq in eqs:
                eqtype = self._desc.get_data(eq).type

                if eqtype in allowed_types:
                    self.apply_bc(s, eq)
                else:
                    raise RuntimeError('Equation not implemented!')

    def apply_bc(self, sudb, eqname):

        bctype = self._desc.get_faceset(sudb.name).data.type

        if bctype in self._bc_selector:
            
            bc = self._bc_selector[bctype]
            bc.setup(eqname, sudb.name)
            foreach_entity_in_subdomain(sudb, bc)
            
        # elif bctype==FaceType.DIRICHLET:
        #     #
        #     # Specific procedures for generic Dirichlet must be put here.
        #     #
        #     if exec_dir==ExecDirective.STANDARD:
        #         raise RuntimeError('No functional Dirichlet BC is supported!')
            
        #     elif exec_dir==ExecDirective.NULL_DIRICHLET:
                
        #         bc = dirichlet_bc(null_dirichlet)
        #         bc.setup(eqname, sudb.name)
        #         foreach_entity_in_subdomain(sudb, bc)

        #     elif exec_dir==ExecDirective.WARBURTON_EX_6_1:

        #         # Get current global time
        #         time_glb = ModelTime()
        #         time     = time_glb.get()

        #         # Get material properties
        #         matname = self._desc.get_equation(eqname).mat
        #         fluid   = self._desc.get_material(matname)
        #         g       = fluid.thermo.gamma

        #         # Get analytical expression
        #         f = example_warburton_book_6p1(
        #             x0=5., y0=0., beta=5., gamma=g,t=time)

        #         # Apply boundary conditions
        #         bc = dirichlet_bc(f)
        #         bc.setup(eqname, sudb.name)
        #         foreach_entity_in_subdomain(sudb, bc)

        #     elif exec_dir==ExecDirective.CTE_EULER_FIELDS:

        #         bc = dirichlet_bc(constant_euler_fields)
        #         bc.setup(eqname, sudb.name)
        #         foreach_entity_in_subdomain(sudb, bc)

        #     elif exec_dir==ExecDirective.NS_VALIDATION:

        #         # Get current global time
        #         time_glb = ModelTime()
        #         time     = time_glb.get()

        #         # Get material properties
        #         matname = self._desc.get_equation(eqname).mat
        #         fluid   = self._desc.get_material(matname)
        #         g       = fluid.thermo.gamma
        #         cp      = fluid.thermo.Cp
        #         cv      = fluid.thermo.Cv
        #         mu      = fluid.transport.mu
        #         Pr      = fluid.transport.Pr
        #         k       = mu*cp/Pr

        #         # Get analytical expression
        #         f = ns_validation(g, mu, k, cv)

        #         # Apply boundary conditions
        #         bc = dirichlet_bc(f)
        #         bc.setup(eqname, sudb.name)
        #         foreach_entity_in_subdomain(sudb, bc)
                                
        #     else:
        #         raise RuntimeError('BC not implemented!')
            
        else:
            raise RuntimeError('BC not implemented!')

        
    def apply_bc_on_element(self, subd, eqname, e):

        bctype = self._desc.get_faceset(subd.name).data.type
        k = e.key
        c = subd(k)

        if bctype in self._bc_selector:
            
            bc = self._bc_selector[bctype]
            bc.setup(eqname, subd.name)
            bc.container_operator(subd, k.key, c)
            bc(e)
            
        # elif bctype==FaceType.DIRICHLET:
        #     #
        #     # Specific procedures for generic Dirichlet must be put here.
        #     #
        #     if exec_dir==ExecDirective.STANDARD:
        #         raise RuntimeError('No functional Dirichlet BC is supported!')
            
        #     elif exec_dir==ExecDirective.NULL_DIRICHLET:
                
        #         bc = dirichlet_bc(null_dirichlet)
        #         bc.setup(eqname, subd.name)
        #         bc.container_operator(subd, k.key, c)
        #         bc(e)

        #     elif exec_dir==ExecDirective.WARBURTON_EX_6_1:

        #         # Get current global time
        #         time_glb = ModelTime()
        #         time     = time_glb.get()

        #         # Get material properties
        #         matname = self._desc.get_equation(eqname).mat
        #         fluid   = self._desc.get_material(matname)
        #         g       = fluid.thermo.gamma

        #         # Get analytical expression
        #         f = example_warburton_book_6p1(
        #             x0=5., y0=0., beta=5., gamma=g,t=time)

        #         # Apply boundary conditions
        #         bc = dirichlet_bc(f)
        #         bc.setup(eqname, subd.name)
        #         bc.container_operator(subd, k.key, c)
        #         bc(e)

        #     elif exec_dir==ExecDirective.CTE_EULER_FIELDS:

        #         bc = dirichlet_bc(constant_euler_fields)
        #         bc.setup(eqname, subd.name)
        #         bc.container_operator(subd, k.key, c)
        #         bc(e)

        #     elif exec_dir==ExecDirective.NS_VALIDATION:

        #         # Get current global time
        #         time_glb = ModelTime()
        #         time     = time_glb.get()

        #         # Get material properties
        #         matname = self._desc.get_equation(eqname).mat
        #         fluid   = self._desc.get_material(matname)
        #         g       = fluid.thermo.gamma
        #         cp      = fluid.thermo.Cp
        #         cv      = fluid.thermo.Cv
        #         mu      = fluid.transport.mu
        #         Pr      = fluid.transport.Pr
        #         k       = mu*cp/Pr

        #         # Get analytical expression
        #         f = ns_validation(g, mu, k, cv)

        #         # Apply boundary conditions
        #         bc = dirichlet_bc(f)
        #         bc.setup(eqname, subd.name)
        #         bc.container_operator(subd, k.key, c)
        #         bc(e)
                                
        #     else:
        #         raise RuntimeError('BC not implemented!')
            
        else:
            raise RuntimeError('BC not implemented!')        

        
    def synchronize(self, cm): pass
        
