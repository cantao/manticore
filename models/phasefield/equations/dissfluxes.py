#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Dissipative fluxes
#
# @addtogroup MODELS.PHASEFIELD.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#
import numpy as np
import manticore
import manticore.services.const as const
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import FieldRole, EntityRole
from manticore.models.phasefield.equations.workspaces import(
    FaceReconstructWsp_f,
    DissipativeBR1Wsp_f, DissipativeBR1Wsp_v, DissipativeBR1Wsp_p)

logger = manticore.logging.getLogger('MDL_PHF_EQ')


class DissipativeBR1:
    """Dissipative Bassi-Rebay numerical flux, version 1."""

    Workspace_f = DissipativeBR1Wsp_f
    Workspace_v = DissipativeBR1Wsp_v
    Workspace_p = DissipativeBR1Wsp_p

    def __init__(self, model_desc, eqname):

        matname = model_desc.get_equation(eqname).mat
        self.mat = model_desc.get_material(matname)


    def eval(self, e, ff):

        # Number of integration points for computations on face ff
        # (max of the pair I/E)
        nip = e.face_comm_size(ff)

        # Workspace associated to this flux: room for all BR1 flux
        # temporaries on *one* face.
        Wsp_f = DissipativeBR1Wsp_f.get_instance(nip)

        # Neighbour element
        ng = e.get_neighbour(ff)
        ng_ff   = e.get_neighbour_face_id(ff)
        ng_role = ng.role
        e_type  = e.type

        state = FieldRole.State

        # Current element (internal) reconstruction                 
        e.get_face_field(state, FieldVariable.DPHASESTATEDX,  ff, Wsp_f.dudxI)
        e.get_face_field(state, FieldVariable.DPHASESTATEDY,  ff, Wsp_f.dudyI)


        # Neighbour element (external) reconstruction
        if ((ng_role==EntityRole.PHYSICAL) or (ng_role==EntityRole.COMM_GHOST)):
            
            ng.get_face_field_as_passive(
                state, FieldVariable.DPHASESTATEDX, e_type, ff, nip, ng_ff,
                Wsp_f.dudxE)
            ng.get_face_field_as_passive(
                state, FieldVariable.DPHASESTATEDY, e_type, ff, nip, ng_ff,
                Wsp_f.dudyE)


        else: # ng_role==EntityRole.GHOST
            
            # If we are on a ghost, the boundary condition for BR1 is
            # copying internal values
            np.copyto(Wsp_f.dudxE, Wsp_f.dudxI)
            np.copyto(Wsp_f.dudyE, Wsp_f.dudyI)

        ###################################################
        # You must implement the flux of the main problem:
        #
        # Return flux_x in Wsp_f.q_star[0][0]
        # Return flux_y in Wsp_f.q_star[0][1]
        #
        ###################################################

    def tilde(self, e, ff):
        
        # Number of integration points for computations on face ff
        # (max of the pair I/E)
        nip = e.face_comm_size(ff)

        # Workspace associated to this flux: room for all BR1 flux
        # temporaries on *one* face.
        Wsp_f = DissipativeBR1Wsp_f.get_instance(nip)

        # Accessing face reconstruction workspace: we are assuming
        # that Face_f.reconstruct was called just before entering in
        # the present context and then the ff-th face's data is stored
        # in the face reconstruction workspace.
        Face_f = FaceReconstructWsp_f.get_instance(*(e.face_comm_sizes) )

        ######################################################
        # You must implement the flux of the auxiliar problem:
        #
        # Return flux_x in Wsp_f.u_star
        #
        ######################################################

        
    
    
