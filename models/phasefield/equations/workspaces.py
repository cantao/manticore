#
# Copyright 2016, 2017, 2018 Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Workspaces
#
# @addtogroup MODELS.PHASEFIELD.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from enum import Enum
import manticore.services.const as const
from manticore.services.datatypes import FlyweightMixin
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.expantools import ExpansionSize
from manticore.lops.modal_dg.dgtypes import StdRegionType
from manticore.lops.modal_dg.dgtypes import FieldType, FieldRole, EntityRole
from manticore.lops.nodal_cg.interpol import InterpolationKey

class WorkspaceType(Enum):

    NoWorkspace = -1
    Facial     = 0
    Volumetric = 1
    Polynomial = 2

    def size():
        """ Enum size (valid itens). """
        return 4


def factory_facereconstruction_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class FaceReconstructionWsp:
            def __init__(self): pass

        return FaceReconstructionWsp

    elif wsp_type == WorkspaceType.Facial:

        class FaceReconstructionWsp(FlyweightMixin):

            def __init__(self, *size):
                """Constructor of FaceReconstructionWsp objects.

                Args:

                    size (tuple(int)): tuple of values
                        max(e.key.n1d, ng.key.n1d) for all faces of an
                        element e.
                """

                # Notation: E suffix stands for "external" (neighbour)
                # and I stands for "internal" (the current element)
                assert len(size) in (3,4)

                self.uI = [np.zeros(sz) for sz in size]
                self.uE = [np.zeros(sz) for sz in size]

                self.n =  [np.zeros((2,sz)) for sz in size]
                self.t =  [np.zeros((2,sz)) for sz in size]

            def reconstruct(self, ff, e, ng, mat):
                """

                Args:

                    ff (int): Local numbering of current face of entity e.

                    e (ExpansionEntity): active element (also called
                        "left" or "internal").

                    ng (ExpansionEntity): passive element (also called
                        "neighbour", "right" or "external").
                """
                nip = e.face_comm_size(ff)  # nip = max(e.n1d, ng.n1d)

                assert self.uI[ff].shape[0]==nip

                state = FieldRole.State
                u     = FieldVariable.PHASESTATE

                # Current element (internal) reconstruction
                e.get_face_field(state, u,  ff, self.uI[ff])

                # Neighbour element (external) reconstruction
                ng_ff   = e.get_neighbour_face_id(ff)
                ng_role = ng.role
                e_type  = e.type

                if( (ng_role == EntityRole.PHYSICAL) or
                    (ng_role == EntityRole.COMM_GHOST) ):
                    ng.get_face_field_as_passive(
                        state, u, e_type, ff, nip, ng_ff, self.uE[ff])
                    
                else: # If neighbour is a ghost, copying is enough for now
                    ng.get_face_field(
                        state, u, ng_ff, self.uE[ff])

                k = InterpolationKey.get_instance(e.geom_type, nip, ff)
                e.fmap.normals_tangents(k, e.get_coeff(),self.n[ff],self.t[ff])

            def reconstruct_internal(self, ff, e, mat):
                """

                Args:

                    ff (int): Local numbering of current face of entity e.

                    e (ExpansionEntity): active element (also called
                        "left" or "internal").

                    ng (ExpansionEntity): passive element (also called
                        "neighbour", "right" or "external").
                """
                nip = e.face_comm_size(ff)  # nip = max(e.n1d, ng.n1d)

                assert self.uI[ff].shape[0]==nip

                state = FieldRole.State
                u     = FieldVariable.PHASESTATE

                # Current element (internal) reconstruction
                e.get_face_field(state, u,  ff, self.uI[ff])

        return FaceReconstructionWsp

    elif wsp_type == WorkspaceType.Polynomial:

        class FaceReconstructionWsp:
            def __init__(self): pass

        return FaceReconstructionWsp

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
FaceReconstructWsp_f = factory_facereconstruction_wsp(WorkspaceType.Facial)

# end of manticore.models.phasefield.equations.factory_facereconstruction_wsp


def factory_residual_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class ResidualWsp:
            def __init__(self): pass

        return ResidualWsp

    elif wsp_type == WorkspaceType.Facial:

        class ResidualWsp:
            def __init__(self): pass

        return ResidualWsp

    elif wsp_type == WorkspaceType.Polynomial:

        class ResidualWsp(FlyweightMixin):

            def __init__(self, size):
                assert size > 0

                self.u  = np.zeros(size)
                self.temp = np.zeros(size)

            def zero(self):
                self.u.fill( 0.0)

        return ResidualWsp

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
ResidualWsp_p = factory_residual_wsp(WorkspaceType.Polynomial)

# end of manticore.models.phasefield.equations.factory_residual_wsp


def factory_dissipativebr1_wsp(wsp_type):

    if wsp_type == WorkspaceType.Volumetric:

        class DissipativeBR1Wsp_v(FlyweightMixin):
            def __init__(self, size):
                assert size > 0

                self.temp   = np.zeros(size)

        return DissipativeBR1Wsp_v

    elif wsp_type == WorkspaceType.Facial:

        class DissipativeBR1Wsp_f(FlyweightMixin):
            def __init__(self, size):
                assert size > 0

                self.u_star = np.zeros(size)                
                self.temp   = np.zeros(size)

                # Facial reconstructions
                # Notation: E suffix stands for "external" (neighbour)
                # and I stands for "internal" (the current element)

                self.dudxI  = np.zeros(size)
                self.dudxE  = np.zeros(size)
                self.dudyI  = np.zeros(size)
                self.dudyE  = np.zeros(size)

                self.muI    = np.zeros(size)
                self.muE    = np.zeros(size)

                # Numerical flux
                self.q_star=[[np.zeros(size),np.zeros(size)] for i in range(1)]

        return DissipativeBR1Wsp_f

    elif wsp_type == WorkspaceType.Polynomial:

        class DissipativeBR1Wsp_p(FlyweightMixin):

            def __init__(self, size):
                assert size > 0

                self.uxc  = np.zeros(size)
                self.uyc  = np.zeros(size)
                self.temp = np.zeros(size)

            def zero(self):
                self.uxc.fill(0.0)
                self.uyc.fill(0.0)

        return DissipativeBR1Wsp_p

    else:
        raise AssertionError("Incorrect workspace type!")

#
# Instantiation
#
DissipativeBR1Wsp_v = factory_dissipativebr1_wsp(WorkspaceType.Volumetric)
DissipativeBR1Wsp_f = factory_dissipativebr1_wsp(WorkspaceType.Facial)
DissipativeBR1Wsp_p = factory_dissipativebr1_wsp(WorkspaceType.Polynomial)

# end of manticore.models.phasefield.equations.factory_dissipativebr1_wsp
