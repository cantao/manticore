#
# Copyright 2016, 2017, 2018 Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Allen-Cahn residual
#
# @addtogroup MODELS.PHASEFIELD.EQUATIONS
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
import manticore
from manticore.services.fieldvariables import FieldVariable
from manticore.lops.modal_dg.dgtypes import StdRegionType, FieldType
from manticore.lops.modal_dg.engine import EntityOperator
from manticore.lops.modal_dg.expankeyfct import ExpansionKeyFactory
from manticore.lops.modal_dg.class_instances import (
    class_quad_physinnerproductgrad2d, class_tria_physinnerproductgrad2d,
    class_quad_physinnerproduct1d, class_tria_physinnerproduct1d,
    class_quad_physbackward2d, class_tria_physbackward2d,
    class_wadg_inverse_mas_op, class_rwadg_inverse_mas_op )
#
from manticore.models.phasefield.equations.workspaces import (
    FaceReconstructWsp_f, ResidualWsp_p )
#
# Importing generic entities temporalily placed under models/compressible
from manticore.models.compressible.services.predicatemixins import (
    model_description_mixin, equation_domain_signature, expansion_numbers)
#


logger = manticore.logging.getLogger('MDL_PHF_EQ')

def factory_weak_auxiliar_eq(diss_flux_t):
    """Factory for the auxiliar problem responsible by computin spatial
    derivatives of state variables for the BR1 dissipative flux.
    
    Args:
        diss_flux_t: A template parameter on the kind of dissipative
            flux we are using (BR1, BR2, etc).
    """

    Workspace_f = diss_flux_t.Workspace_f
    Workspace_v = diss_flux_t.Workspace_v
    Workspace_p = diss_flux_t.Workspace_p
    
    InverseMassOperator = [class_wadg_inverse_mas_op,class_rwadg_inverse_mas_op]

    class weak_auxiliar_eq(
            EntityOperator,
            model_description_mixin,
            equation_domain_signature,
            expansion_numbers):

        def __init__(self, model_desc):
            model_description_mixin.__init__(self, model_desc)
            equation_domain_signature.__init__(self)
            expansion_numbers.__init__(self)
            EntityOperator.__init__(self)

            self.innerpg  = [None, None] # InnerProductGradient[quad, tria]
            self.innerp_t = [None, None] # InnerProduct[quad, tria]
            self.bw       = [None, None] # Backward[quad, tria]
            
            self.temp = np.zeros(0)

        def setup(self, eqname, domain):
            equation_domain_signature.setup(self, eqname, domain)
            matname = self.desc.get_equation(eqname).mat
            self.mat = self.desc.get_material(matname)

        def container_operator(self, group, key, container):
            k = ExpansionKeyFactory.make(key.order, key.nip)

            regions =  [StdRegionType.DG_Quadrangle, StdRegionType.DG_Triangle]

            self.innerpg[regions[0]] = class_quad_physinnerproductgrad2d(k)
            self.innerpg[regions[1]] = class_tria_physinnerproductgrad2d(k)

            self.innerp_t[regions[0]] = class_quad_physinnerproduct1d
            self.innerp_t[regions[1]] = class_tria_physinnerproduct1d

            self.bw[regions[0]] = class_quad_physbackward2d(k)
            self.bw[regions[1]] = class_tria_physbackward2d(k)

            self.temp.resize( (key.nip+1,), refcheck=False)

        def __call__(self, e):

            # Check setup was done
            assert self.domain

            # Set nS and nQ for this element
            expansion_numbers.eval(self, e.type, e.key)

            #
            # STEP 0: Preparing workspaces
            #
            # Acessing temporary storage for gradient computation
            Grad_p = diss_flux_t.Workspace_p.get_instance(self.nS)
            Grad_p.zero()

            # Acessing temporary storage for gradient computation
            Aux_v = diss_flux_t.Workspace_v.get_instance(self.nQ)
            
            #
            # STEP 1: Evaluation of -( U, Grad phi )_e
            #

            # uxc --> result of the inner product of the u component of
            # velocity against the x derivative of basis functions. All other
            # variables follow the same convention, including internal energy.

            J  = e.get_jacobian()
            IM = e.get_inv_jacobian_matrix()
			
            u = e.state(FieldType.ph, FieldVariable.PHASESTATE)
            np.multiply(u, -1., Aux_v.temp)
            self.innerpg[e.type].eval(J, IM, Aux_v.temp, Grad_p.uxc, Grad_p.uyc)
          
            #
            # STEP 2: Face iteration
            # 
            Flux = diss_flux_t(self.desc, self.equation)

            num_faces  = len(e.neigh)

            # Access the workspace of face reconstructed data
            Face_f = FaceReconstructWsp_f.get_instance( *(e.face_comm_sizes) )

            # Guess a probable allocation for temporary arrays
            Temp = self.temp

            for ff in range(num_faces):                
                ng  = e.get_neighbour(ff)  # Get the ff-th neighbour
                nip = e.face_comm_size(ff) # Communication size on face ff

                #
                # Be careful when using ExpansionKey to get integration
                # points on faces: the second argument of the constructor
                # is the one-dimensional number of integration points for
                # numerical integration INSIDE the element. The number of
                # integration points generated on faces by ExpansionKey is
                # the second argument of the constructor PLUS ONE. Thus,
                # if you know the number of integration points on face (as
                # here) you must SUBTRACT ONE from it to get the correct
                # integration points on faces (see the assertion below).
                #
                k = ExpansionKeyFactory.make(e.key.order, nip-1)

                # In a mesh with non-constant p-order, the number of
                # integration points on each face can be different
                # from the others and then the inner product operator
                # my be set up when reaching each face.
                innerp = self.innerp_t[e.type](k)

                # Physical reconstruction on face ff
                Face_f.reconstruct(ff, e, ng, self.mat)

                # Evaluating the numerical flux f*(.)
                Flux.tilde(e, ff)

                # Access 
                Wsp_f = diss_flux_t.Workspace_f.get_instance(nip)

                # Normals to face ff at the integration points
                n = Face_f.n[ff]

                # Correct allocation
                Temp.resize( (nip,), refcheck=False)

                np.multiply(n[0,:], Wsp_f.uTilde, Temp)
                innerp(ff, Temp, e.get_face_jacobian(ff), Grad_p.temp)
                Grad_p.uxc += Grad_p.temp

                np.multiply(n[1,:], Wsp_f.uTilde, Temp)
                innerp(ff, Temp, e.get_face_jacobian(ff), Grad_p.temp)
                Grad_p.uyc += Grad_p.temp


            # M^{-1} product
            IM = e.get_inv_mass()
            tr = FieldType.tr

            dudx = e.state(tr,FieldVariable.DPHASESTATEDX)
            dudy = e.state(tr,FieldVariable.DPHASESTATEDY)
            
            InverseMassOperator[e.subtype].solve(IM, Grad_p.uxc, dudx)
            InverseMassOperator[e.subtype].solve(IM, Grad_p.uyc, dudy)         

            # Computing physical values
            bw = self.bw[e.type]
            ph = FieldType.ph

            bw(dudx, e.state(ph, FieldVariable.DPHASESTATEDX))
            bw(dudy, e.state(ph, FieldVariable.DPHASESTATEDY))

    # end of factory_weak_auxiliar_eq.weak_auxiliar_eq
    
    return weak_auxiliar_eq

# end of
# manticore.models.phasefield.equations.allencahnres.factory_weak_auxiliar_eq


def factory_weak_dg_allencahn_res(diss_flux_t):

    """Factory for the residual of the weak DG form of the Allen-Cahn
    equation for phase field.
    
    Args:
        diss_flux_t: A template parameter on the kind of disspative
            flux we are using
    """

    DissFluxWorkspace_f = diss_flux_t.Workspace_f
    DissFluxWorkspace_v = diss_flux_t.Workspace_v
    DissFluxWorkspace_p = diss_flux_t.Workspace_p
    
    InverseMassOperator = [class_wadg_inverse_mas_op,class_rwadg_inverse_mas_op]

    class weak_dg_allencahn_residual(
            EntityOperator,
            model_description_mixin,
            equation_domain_signature,
            expansion_numbers):

        def __init__(self, model_desc):
            model_description_mixin.__init__(self, model_desc)
            equation_domain_signature.__init__(self)
            expansion_numbers.__init__(self)
            EntityOperator.__init__(self)

            self.innerpg  = [None, None] # InnerProductGradient[quad, tria]
            self.innerp_t = [None, None] # InnerProduct1d[quad, tria]

            self.temp = np.zeros(0)
            self.aux  = np.zeros(0)

            # For the source term
            self.innerpSrc = [None, None] # InnerProduct2d[quad, tria]

        def setup(self, eqname, domain):
            equation_domain_signature.setup(self, eqname, domain)
            matname = self.desc.get_equation(eqname).mat
            self.mat = self.desc.get_material(matname)

        def container_operator(self, group, key, container):
            k = ExpansionKeyFactory.make(key.order, key.nip)

            regions =  [StdRegionType.DG_Quadrangle, StdRegionType.DG_Triangle]

            self.innerpg[regions[0]] = class_quad_physinnerproductgrad2d(k)
            self.innerpg[regions[1]] = class_tria_physinnerproductgrad2d(k)

            self.innerp_t[regions[0]] = class_quad_physinnerproduct1d
            self.innerp_t[regions[1]] = class_tria_physinnerproduct1d

            self.temp.resize( (key.nip+1,), refcheck=False)
            self.aux.resize(  (key.nip+1,), refcheck=False)

            # For the source term
            self.innerpSrc[regions[0]] = class_quad_physinnerproduct2d(k)
            self.innerpSrc[regions[1]] = class_tria_physinnerproduct2d(k) 

        def __call__(self, e):
            """Evaluates the convective residual of a single element.
            """

            #logger.debug('Evaluating residual on element %4d' % (e.ID))
            
            # Check setup was done
            assert self.domain

            # Set nS and nQ for this element
            expansion_numbers.eval(self, e.type, e.key)

            # Zeroing residuals
            Residual_p = ResidualWsp_p.get_instance(self.nS)
            Residual_p.zero()

            # Volumetric operations
            self.volumeContribution(e)

            # Facial operations
            self.faceContribution(e)

            # M^{-1} product
            IM = e.get_inv_mass()
            tr = FieldType.tr
            
            InverseMassOperator[e.subtype].solve(
                IM, Residual_p.u, e.residual(tr,FieldVariable.PHASESTATE) )
            

        def volumeContribution(self, e):

            Residual_p = ResidualWsp_p.get_instance(self.nS)
            J  = e.get_jacobian()
            IM = e.get_inv_jacobian_matrix()

            u    = e.state(ph, FieldVariable.PHASESTATE)
            dudx = e.state(ph, FieldVariable.DPHASESTATEDX)
            dudy = e.state(ph, FieldVariable.DPHASESTATEDY)

            eps = self.mat.epsilon
            mu  = self.mat.mu() # return a scalar

            # Stiffness operator
            self.innerpg[e.type](J, IM, dudx, dudy, Residual_p.temp)
            Residual_p.temp *= mu*eps**2
            Residual_p.u -= Residual_p.temp

            # Source term
            fTilde = self.mat.mu(u)*self.mat.free_energy_functional(u)
            self.innerpSrc[e.type](fTilde, J, Residual_p.temp)
            Residual_p.u -= Residual_p.temp

            
        def faceContribution(self, e):

            eps = self.mat.epsilon
            mu  = self.mat.mu() # return a scalar

            # Dissipative flux
            Flux = diss_flux_t(self.desc, self.equation)

            # Access the residual workspace
            Residual_p = ResidualWsp_p.get_instance(self.nS)

            num_faces  = len(e.neigh)

            # Access the workspace of face reconstructed data
            Face_f = FaceReconstructWsp_f.get_instance( *(e.face_comm_sizes) )

            # 
            Temp = self.temp
            Aux  = self.aux
            inner_temp = Residual_p.temp

            for ff in range(num_faces):                
                ng  = e.get_neighbour(ff)  # Get the ff-th neighbour
                nip = e.face_comm_size(ff) # Communication size on face ff

                #
                # Be careful when using ExpansionKey to get integration
                # points on faces: the second argument of the constructor
                # is the one-dimensional number of integration points for
                # numerical integration INSIDE the element. The number of
                # integration points generated on faces by ExpansionKey is
                # the second argument of the constructor PLUS ONE. Thus,
                # if you know the number of integration points on face (as
                # here) you must SUBTRACT ONE from it to get the correct
                # integration points on faces (see the assertion below).
                #
                k = ExpansionKeyFactory.make(e.key.order, nip-1)

                # In a mesh with non-constant p-order, the number of
                # integration points on each face can be different
                # from the others and then the inner product operator
                # my be set up when reaching each face.
                innerp = self.innerp_t[e.type](k)

                # Physical reconstruction on face ff
                Face_f.reconstruct(ff, e, ng, self.mat)

                # Evaluating the numerical flux f*(.)
                Flux.eval(e, ff)

                # Access 
                Wsp_f = diss_flux_t.Workspace_f.get_instance(nip)

                # Normals to face ff at the integration points
                n = Face_f.n[ff]

                # Correct allocation
                Temp.resize( (nip,), refcheck=False)
                Aux.resize( (nip,), refcheck=False)
                
                # Facial contribution
                np.multiply(n[0,:], Wsp_f.F_star[0][0], Temp)
                np.multiply(n[1,:], Wsp_f.F_star[0][1], Aux)
                Temp += Aux
                innerp(ff, Temp , e.get_face_jacobian(ff), inner_temp)
                inner_temp *= mu*eps**2
                Residual_p.u += inner_temp
                
    # end of factory_weak_dg_allencahn_res.weak_dg_allencahn_residual
    
    return weak_dg_allencahn_residual


# end of
# manticore.models.phasefield.equations.allencahnres.factory_weak_dg_allencahn_res
