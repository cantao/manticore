#
# Copyright 2016, 2017, 2018 Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Model description
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy as np
from enum import Enum
from treelib import Tree, Node
from collections import namedtuple
from manticore.mdf.enums import MapType
from manticore.services.datatypes import AutoNumberEnum
from manticore.services.fieldvariables import FieldVariable
#
# Importing generic entities temporalily placed under models/compressible
from manticore.models.compressible.description.mdtypes import (
    EqParameter )
from manticore.models.compressible.description.elsets import (
    ElementSet, FaceSet, FaceSetData, FaceSetNoData )
from manticore.models.compressible.description.equations import (
    EquationDescription )
from manticore.models.compressible.description.timint import init_time_acc

# @ARTICLE{B_Karasozen2017,
#   author = 	 {B. Karas�zen and M. Uzunca and A. S. Filibelioglu
#                   and H. Y�cel},
#   title = 	 {Energy Stable Discontinuous {G}alerkin Finite Element
#                   Method for the {A}llen-{C}ahn Equation},
#   journal = {International Journal of Computational Methods},
#   volume = {16},
#   number = {0},
#   pages = {1-26},
#   year = {2017},
#   month = {July},
#   doi = {10.1142/S0219876218500135}
# }

class EqType(Enum):
    EQTYPE_NONE = 0
    ALLENCAHN   = 1


class FaceType(AutoNumberEnum):
    FACETYPE_NONE   = ()  #
    SIMPLE_INTERNAL = ()  # Internal faces: no property
    SYMMETRY        = ()  #
    DIRICHLET       = ()  # Dirichlet used only for validation tests
    PERIODIC        = ()

periodicbc_pair = namedtuple('periodicbc_pair', ['element', 'face'])

class Periodic(FaceSetNoData):

    def __init__(self, ftype):
        FaceSetNoData.__init__(self, ftype)

        self.pair = None # name of the boundary pair
        self.map  = {}   # e.ID->(ExpansionEntity, face)


def init_face_data(ftype):
    """Allocation of FaceSetData objects according to a given 'FaceType'."""
    switcher = {
        FaceType.FACETYPE_NONE: FaceSetNoData(FaceType.FACETYPE_NONE),
        FaceType.SIMPLE_INTERNAL: FaceSetNoData(FaceType.SIMPLE_INTERNAL),
        FaceType.SYMMETRY: FaceSetNoData(FaceType.SYMMETRY),
        FaceType.DIRICHLET: FaceSetNoData(FaceType.DIRICHLET),
        FaceType.PERIODIC: Periodic(FaceType.PERIODIC)
    }
    return switcher.get(ftype, FaceSetData())      


class AllenCahn(EquationDescription):
    """Parameters for the Allen-Cahn equation."""

    def __init__(self):
        """Defines solution setting slots and state variables associated to
           this equation."""
        EquationDescription.__init__(self, EqType.ALLENCAHN)

        self.data = dict.fromkeys([
            EqParameter.PRINCIPAL_DGFORM,
            EqParameter.DAUXILIAR_DGFORM,
            EqParameter.DISSIPATIVE_FLUX
        ])

        var = [ FieldVariable.PHASESTATE ]

        for v in var:
            self.set_limit_key(v)


def init_equation_data(eqtype):
    """Allocation of EquationDescription objects according to a given
    'EqType'.
    """
    switcher = {
        EqType.EQTYPE_NONE: EquationDescription(),
        EqType.ALLENCAHN: AllenCahn()
    }
    return switcher.get(eqtype, EquationDescription())    


class TransportData:
    """Material data."""
    def __init__(self):
        self._epsilon = 0.0 # interaction length, capturing the
                            # dominating effect of the reaction
                            # kinetics and represents the effective
                            # diffusivity.
                            
        self._mu = 0.0      # non-negative mobility function that describes
                            # the physics of phase separation.
        
    @property
    def epsilon(self):
        # Safe access
        return self._epsilon

    def mu(self, u=0.0):
        # NOTE: if mu is non-constant, this method must be changed
        if isinstance(u, np.ndarray):
            return self._mu*np.ones(u.shape[0])
        else:
            return self._mu

    def free_energy_functional(self, u): pass
    ###################################################
    # You must implement your free energy function f(u)
    ###################################################

    def init_material_model(self): pass

      
class ModelDescription:

    def __init__(self):
        """Model objects are made of a Tree object with a fixed
           structure of master nodes describing the main components of
           a simulation model. Specific model's data must be supplied
           as Node's data or new sub-nodes.

        """
        self.data = Tree()
        self.data.create_node(None, 'MODEL')  #root node
        self.data.create_node(None, 'FILES', 'MODEL')
        self.data.create_node(None, 'MATERIALS', 'MODEL')
        self.data.create_node(None, 'EQUATIONS', 'MODEL')
        self.data.create_node(None, 'DOMAIN', 'MODEL')
        self.data.create_node(None, 'SOLUTION', 'MODEL')
        self.data.create_node(None, 'OUTPUT', 'MODEL')

        # FILE branch
        #                       tag        ID       parent   data
        self.data.create_node('MshFile' , 'MESH'   , 'FILES', '')
        self.data.create_node('InitFile', 'INITIAL', 'FILES', '')
        self.data.create_node('ResFile' , 'RESULTS', 'FILES', '')
        self.data.create_node('RstFile' , 'RESTART', 'FILES', '')
        #
        self.data.create_node(None, 'INITIAL_TYPE', 'INITIAL', MapType.PER_ELEM)

        # DOMAIN branch
        self.data.create_node(None, 'ELEMENTSETS', 'DOMAIN')
        self.data.create_node(None, 'FACESETS', 'DOMAIN')

        # SOLUTION branch
        self.data.create_node(None, 'TIME_PARAMETERS', 'SOLUTION')
        self.data.create_node(None, 'TIME_INTEGRATION', 'SOLUTION')


    def get_data(self, name):
        """Access any object stored in the model."""
        return self.data.get_node(name).data

    def get_equations(self):
        """Returns list of Node objects."""
        return self.data.children('EQUATIONS')

    def get_equation(self, name):
        return self.data.get_node(name).data

    def get_materials(self):
        """Returns list of Node objects."""
        return self.data.children('MATERIALS')

    def get_material(self, name):
        return self.data.get_node(name).data

    def get_elementsets(self):
        """Returns list of Node objects."""
        return self.data.children('ELEMENTSETS')

    def get_elementset(self, name):
        return self.data.get_node(name).data

    def get_facesets(self):
        """Returns list of Node objects."""
        return self.data.children('FACESETS')

    def get_faceset(self, name):
        return self.data.get_node(name).data

    def find_elsets_by_equation(self, equation):
        """Returns list of Node objects."""

        vsets = self.data.children('ELEMENTSETS')
        vfound = list()

        for v in vsets:
            if equation in v.data.equations:
                vfound.append(v.data.name)

        return vfound

    def get_time_settings(self):
        return self.data.get_node('GENERIC_TIME_SETTINGS').data

    def get_time_integration(self, eq_name):
        name = 'TIME_INTEGRATOR_EQ_' + eq_name
        return self.data.get_node(name).data

    def init_equation(self, name, eqtype):
        self.data.create_node(None, name, 'EQUATIONS',
                              init_equation_data(eqtype))

        self.data.get_node(name).data.name = name

    def init_elementset(self, name):
        self.data.create_node(None, name, 'ELEMENTSETS', ElementSet(name))

    def init_faceset(self, name, ftype):
        self.data.create_node(None, name, 'FACESETS', FaceSet(name))
        self.data.get_node(name).data.set_face_type(ftype,
                                                    init_face=init_face_data)

    def init_material(self, name, matdata):
        self.data.create_node(None, name, 'MATERIALS', matdata)
        self.data.get_node(name).data.init_material_model()  # matdata's method

    def init_time_settings(self, tacc):
        self.data.create_node(None, 'TIME_ACCURACY', 'TIME_PARAMETERS', tacc)
        self.data.create_node(None, 'GENERIC_TIME_SETTINGS', 'TIME_PARAMETERS',
                              init_time_acc(tacc))

    def init_time_integration(self, eq_name, tint):
        name = 'TIME_INTEGRATOR_EQ_' + eq_name
        self.data.create_node(None, name, 'TIME_INTEGRATION', tint)


# -- model.py -----------------------------------------------------------------
