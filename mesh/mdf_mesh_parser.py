#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Builds a mesh from an MDF file
#
# @addtogroup mesh
# @author Cantão! (rfcantao@gmail.com)
#

import manticore
from manticore.mdf.file import File
from manticore.geom.standard_geometries import (
    StandardGeometryGenerator, order, number_of_nodes
    )
from manticore.mesh.umesh import ConceptualGroup, UnstructuredMesh

logger = manticore.logging.getLogger('___MESH___')

class MdfMeshParser(File):
    """Creates an unstructured mesh from a MDF file."""

    def __init__(self, mdffile):
        super().__init__(mdffile, mode='r')

    def parse(self):
        """Main parser method.

        Args:
            mdffile (str): MDF file name.
        """
        logger.debug('Opening MDF file.')

        logger.debug('Creating empty mesh.')
        M = UnstructuredMesh()

        self.create()

        for p in range(0, self.model_data.n_parts):
            self.__process_nodes(M, p)
            self.__process_areas(M, p)
            self.__process_edges(M, p)

        self.close()

        return M

    def __process_nodes(self, M, part):
        logger.debug('Processing nodes.')

        xy, indices = self.read_nodes(part)

        for i, idx in enumerate(indices):
            M.create_vertex(idx, *xy[i])

    def __process_areas(self, M, part):
        logger.debug('Processing 2D elements.')

        model_data = self.model_data

        # Process elements from each subdomain
        for subd in range(0, model_data.n_subdomains):
            logger.debug('Processing domain {}'.format(subd))

            shapes = self.query_element_shapes(partition_id=0,
                                               subdomain_id=subd)

            subdomain_name = self.query_subdomain_name(partition_id=0,
                                                       subdomain_id=subd)

            logger.debug('Subdomain: {}'.format(subdomain_name))

            # Running through all geometries
            for sh in shapes:
                n_elements = self.query_number_elements(
                    partition_id=0, domain_id=subd, shape=sh
                )

                std_geom = StandardGeometryGenerator.gen_from_string(sh)

                for o, nels in n_elements.items():
                    logger.debug(
                        'Processing {} elements of order {}'.format(nels, o)
                    )

                    gid = M.create_group(subdomain_name,
                                         ConceptualGroup.Type.SUPERFICIAL,
                                         2, o)

                    conn, ids = self.read_elements(partition_id=part,
                                                   subdomain_id=subd,
                                                   p_order=o,
                                                   element=std_geom)

                    for i in range(0, nels):
                        M.create_area(gid, ids[i], std_geom, conn[i, :])

    def __process_edges(self, M, part):
        logger.debug('Processing 1D elements.')

        partition_data = self.partition_data[part]

        for bdr in partition_data.boundary_names:
            logger.debug('Partition {}: boundary {}'.format(part, bdr))

            # We must check if the name is a partitioner one. The name in this
            # case follows the standard:
            #
            # MANTICORE_P_A_B_C_D_EROCITNAM
            #
            # where A and B are the first conceptual group/partition and C, D
            # are the second ones.
            tokens = bdr.split('_')

            is_inter_partition = tokens[0] == 'MANTICORE' and \
                                 tokens[1] == 'P' and \
                                 tokens[:-1] == 'EROCITNAM'

            if is_inter_partition:
                group_type = ConceptualGroup.Type.PARALLEL
            else:
                group_type = ConceptualGroup.Type.BOUNDARY

            shapes = self.query_boundary_element_shapes(part, bdr)
            bdr_id = self.find_boundary(part, bdr)

            for sh in shapes:
                std_geom = StandardGeometryGenerator.gen_from_string(sh)

                gid = M.create_group(bdr, group_type, 1, order(std_geom))

                conn, ids = self.read_boundary_elements(partition_id=part,
                                                        boundary_id=bdr_id,
                                                        element=std_geom)

                nels = self.query_number_boundary_elements(part, bdr_id, sh)

                for i in range(0, nels):
                    M.create_edge(gid, ids[i], std_geom, conn[i, :])


# -- mdf_mesh_parser.py -------------------------------------------------------
