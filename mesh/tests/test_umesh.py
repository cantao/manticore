#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MESH tests
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#

import unittest
import numpy
from manticore.mesh.entity import Vertex, Edge
from manticore.geom.standard_geometries import (StandardGeometry,
                                                number_of_edges)
from manticore.mesh.umesh import (ConceptualGroup, UnstructuredMesh,
                                  double_link_region_to_edge)


class UnstructuredMeshTestCase(unittest.TestCase):
    def test_4tri3(self):
        # Mesh
        M = UnstructuredMesh()

        # Nodes
        vertices = numpy.array([
            (0.0, 0.0),  # 0
            (1.0, 0.0),  # 1
            (1.0, 1.0),  # 2
            (0.0, 1.0),  # 3
            (0.5, 0.5)  # 4
        ])

        n_vertices = vertices.shape[0]

        for ii in range(n_vertices):
            M.create_vertex(ii, vertices[ii, 0], vertices[ii, 1])

        # Elements
        gName = "MAIN"
        gId = M.create_group(gName, ConceptualGroup.Type.SUPERFICIAL, 2, 1)

        trias = [
            [0, 1, 4],  # 0
            [1, 2, 4],  # 1
            [2, 3, 4],  # 2
            [3, 0, 4]
        ]  # 3

        Idx = 0  # Index generator

        n_elements = len(trias)

        for ii in range(n_elements):
            M.create_area(gId, Idx, StandardGeometry.TRI3, trias[ii])
            Idx += 1

        # Boundary

        Idx = 0

        boundary = [
            [2, 3],  # Front
            [0, 1],  # Back
            [3, 0],  # Left
            [1, 2]  # Right
        ]

        n_boundaries = len(boundary)

        # Front
        gId = M.create_group("FRONT", ConceptualGroup.Type.BOUNDARY, 1, 1)
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[0])
        Idx += 1

        # Back
        gId = M.create_group("BACK", ConceptualGroup.Type.BOUNDARY, 1, 1)
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[1])
        Idx += 1

        # Left
        gId = M.create_group("LEFT", ConceptualGroup.Type.BOUNDARY, 1, 1)
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[2])
        Idx += 1

        # Right
        gId = M.create_group("RIGHT", ConceptualGroup.Type.BOUNDARY, 1, 1)
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[3])
        Idx += 1

        # Testing mesh dimensions
        self.assertEqual(M.size_by_dimension(0), n_vertices)
        self.assertEqual(M.size_by_dimension(1), n_boundaries)
        self.assertEqual(M.size_by_dimension(2), n_elements)

        group = M.find_by_name(gName)
        gId = group.get_id()

        self.assertEqual(M.get_group_name(gId), gName)

        # Create internal edges
        double_link_region_to_edge(M)

        # Testing the generation of the internal edges group
        internal_name = '_' + gName + '_MANTICORE_GENERATED_INTERNAL_'
        internal = M.find_by_name(internal_name)
        n_internal = internal.size(StandardGeometry.EDGE2)
        self.assertIsNotNone(internal)
        self.assertEqual(M.size_by_dimension(1), n_boundaries + n_internal)

        # Testing the existence of internal edges (using the same
        # vertices' IDs for generating the same hash IDs on vertices
        # and then on all entities built from them. Hash IDs are the
        # criteria of search.)
        v0 = Vertex(0, StandardGeometry.VERTEX1, vertices[0, 0],
                    vertices[0, 1])
        v1 = Vertex(1, StandardGeometry.VERTEX1, vertices[1, 0],
                    vertices[1, 1])
        v2 = Vertex(2, StandardGeometry.VERTEX1, vertices[2, 0],
                    vertices[2, 1])
        v3 = Vertex(3, StandardGeometry.VERTEX1, vertices[3, 0],
                    vertices[3, 1])
        v4 = Vertex(4, StandardGeometry.VERTEX1, vertices[4, 0],
                    vertices[4, 1])

        # Internal Edge 0
        edge0 = Edge(0, StandardGeometry.EDGE2)
        edge0.push_back(v0)
        edge0.push_back(v4)
        edge0.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge0)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have three neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 3)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 3)

        # Back test: do those three neibouring edges contain 'found'?
        test = False
        for e in range(3):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(3):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 1
        edge1 = Edge(1, StandardGeometry.EDGE2)
        edge1.push_back(v1)
        edge1.push_back(v4)
        edge1.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge1)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have three neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 3)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 3)

        # Back test: do those three neibouring edges contain 'found'?
        test = False
        for e in range(3):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(3):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 2
        edge2 = Edge(2, StandardGeometry.EDGE2)
        edge2.push_back(v2)
        edge2.push_back(v4)
        edge2.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge2)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have three neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 3)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 3)

        # Back test: do those three neibouring edges contain 'found'?
        test = False
        for e in range(3):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(3):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 3
        edge3 = Edge(3, StandardGeometry.EDGE2)
        edge3.push_back(v3)
        edge3.push_back(v4)
        edge3.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge3)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have three neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 3)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 3)

        # Back test: do those three neibouring edges contain 'found'?
        test = False
        for e in range(3):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(3):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

    def test_4tri6(self):
        # Mesh
        M = UnstructuredMesh()

        # Nodes
        vertices = numpy.array([
            (0.0, 0.0),  # 0
            (1.0, 0.0),  # 1
            (1.0, 1.0),  # 2
            (0.0, 1.0),  # 3
            (0.5, 0.5),  # 4
            (0.5, 0.0),  # 5
            (1.0, 0.5),  # 6
            (0.5, 1.0),  # 7
            (0.0, 0.5),  # 8
            (0.75, 0.25),  # 9
            (0.75, 0.75),  # 10
            (0.25, 0.75),  # 11
            (0.25, 0.25)  # 12
        ])

        n_vertices = vertices.shape[0]

        for ii in range(n_vertices):
            M.create_vertex(ii, vertices[ii, 0], vertices[ii, 1])

        # Elements
        gName = "MAIN"
        gId = M.create_group(gName, ConceptualGroup.Type.SUPERFICIAL, 2, 2)

        trias = [
            [0, 1, 4, 5, 9, 12],  # 0
            [1, 2, 4, 6, 10, 9],  # 1
            [2, 3, 4, 7, 11, 10],  # 2
            [3, 0, 4, 8, 12, 11]
        ]  # 3

        Idx = 0  # Index generator

        n_elements = len(trias)

        for ii in range(n_elements):
            M.create_area(gId, Idx, StandardGeometry.TRI6, trias[ii])
            Idx += 1

        # Boundary

        Idx = 0

        boundary = [
            [2, 7, 3],  # Front
            [0, 5, 1],  # Back
            [3, 8, 0],  # Left
            [1, 6, 2]
        ]  # Right

        n_boundaries = len(boundary)

        # Front
        gId = M.create_group("FRONT", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[0])
        Idx += 1

        # Back
        gId = M.create_group("BACK", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[1])
        Idx += 1

        # Left
        gId = M.create_group("LEFT", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[2])
        Idx += 1

        # Right
        gId = M.create_group("RIGHT", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[3])
        Idx += 1

        # Testing mesh dimensions
        self.assertEqual(M.size_by_dimension(0), n_vertices)
        self.assertEqual(M.size_by_dimension(1), n_boundaries)
        self.assertEqual(M.size_by_dimension(2), n_elements)

        group = M.find_by_name(gName)
        gId = group.get_id()

        self.assertEqual(M.get_group_name(gId), gName)

        # Create internal edges
        double_link_region_to_edge(M)

        # Testing the generation of the internal edges group
        internal_name = '_' + gName + '_MANTICORE_GENERATED_INTERNAL_'
        internal = M.find_by_name(internal_name)
        n_internal = internal.size(StandardGeometry.EDGE3)
        self.assertIsNotNone(internal)
        self.assertEqual(M.size_by_dimension(1), n_boundaries + n_internal)

        # Testing the existence of internal edges (using the same
        # vertices' IDs for generating the same hash IDs on vertices
        # and then on all entities built from them. Hash IDs are the
        # criteria of search.)
        v0 = Vertex(0, StandardGeometry.VERTEX1, vertices[0, 0],
                    vertices[0, 1])
        v1 = Vertex(1, StandardGeometry.VERTEX1, vertices[1, 0],
                    vertices[1, 1])
        v2 = Vertex(2, StandardGeometry.VERTEX1, vertices[2, 0],
                    vertices[2, 1])
        v3 = Vertex(3, StandardGeometry.VERTEX1, vertices[3, 0],
                    vertices[3, 1])
        v4 = Vertex(4, StandardGeometry.VERTEX1, vertices[4, 0],
                    vertices[4, 1])
        v9 = Vertex(9, StandardGeometry.VERTEX1, vertices[9, 0],
                    vertices[9, 1])
        v10 = Vertex(10, StandardGeometry.VERTEX1, vertices[10, 0],
                     vertices[10, 1])
        v11 = Vertex(11, StandardGeometry.VERTEX1, vertices[11, 0],
                     vertices[11, 1])
        v12 = Vertex(12, StandardGeometry.VERTEX1, vertices[12, 0],
                     vertices[12, 1])

        # Internal Edge 0
        edge0 = Edge(0, StandardGeometry.EDGE3)
        edge0.push_back(v0)
        edge0.push_back(v12)
        edge0.push_back(v4)
        edge0.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge0)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have three neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 3)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 3)

        # Back test: do those three neibouring edges contain 'found'?
        test = False
        for e in range(3):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(3):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 1
        edge1 = Edge(1, StandardGeometry.EDGE3)
        edge1.push_back(v1)
        edge1.push_back(v9)
        edge1.push_back(v4)
        edge1.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge1)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have three neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 3)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 3)

        # Back test: do those three neibouring edges contain 'found'?
        test = False
        for e in range(3):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(3):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 2
        edge2 = Edge(2, StandardGeometry.EDGE3)
        edge2.push_back(v2)
        edge2.push_back(v10)
        edge2.push_back(v4)
        edge2.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge2)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have three neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 3)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 3)

        # Back test: do those three neibouring edges contain 'found'?
        test = False
        for e in range(3):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(3):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 3
        edge3 = Edge(3, StandardGeometry.EDGE3)
        edge3.push_back(v3)
        edge3.push_back(v11)
        edge3.push_back(v4)
        edge3.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge3)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have three neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 3)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 3)

        # Back test: do those three neibouring edges contain 'found'?
        test = False
        for e in range(3):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(3):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

    def test_4quad4(self):
        # Mesh
        M = UnstructuredMesh()

        # Nodes
        vertices = numpy.array([
            (0.0, 0.0),  # 0
            (1.0, 0.0),  # 1
            (1.0, 1.0),  # 2
            (0.0, 1.0),  # 3
            (0.5, 0.5),  # 4
            (0.5, 0.0),  # 5
            (1.0, 0.5),  # 6
            (0.5, 1.0),  # 7
            (0.0, 0.5)
        ])  # 8

        n_vertices = vertices.shape[0]

        for ii in range(n_vertices):
            M.create_vertex(ii, vertices[ii, 0], vertices[ii, 1])

        # Elements
        gName = "MAIN"
        gId = M.create_group(gName, ConceptualGroup.Type.SUPERFICIAL, 2, 1)

        quads = [
            [0, 5, 4, 8],  # 0
            [5, 1, 6, 4],  # 1
            [4, 6, 2, 7],  # 2
            [8, 4, 7, 3]
        ]  # 3

        Idx = 0  # Index generator

        n_elements = len(quads)

        for ii in range(n_elements):
            M.create_area(gId, Idx, StandardGeometry.QUAD4, quads[ii])
            Idx += 1

        # Boundary

        Idx = 0

        boundary = [
            [2, 7],
            [7, 3],  # Front
            [0, 5],
            [5, 1],  # Back
            [3, 8],
            [8, 0],  # Left
            [1, 6],
            [6, 2]
        ]  # Right

        n_boundaries = len(boundary)

        # Front
        gId = M.create_group("FRONT", ConceptualGroup.Type.BOUNDARY, 1, 1)
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[0])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[1])
        Idx += 1

        # Back
        gId = M.create_group("BACK", ConceptualGroup.Type.BOUNDARY, 1, 1)
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[2])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[3])
        Idx += 1

        # Left
        gId = M.create_group("LEFT", ConceptualGroup.Type.BOUNDARY, 1, 1)
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[4])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[5])
        Idx += 1

        # Right
        gId = M.create_group("RIGHT", ConceptualGroup.Type.BOUNDARY, 1, 1)
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[6])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE2, boundary[7])
        Idx += 1

        # Testing mesh dimensions
        self.assertEqual(M.size_by_dimension(0), n_vertices)
        self.assertEqual(M.size_by_dimension(1), n_boundaries)
        self.assertEqual(M.size_by_dimension(2), n_elements)

        group = M.find_by_name(gName)
        gId = group.get_id()

        self.assertEqual(M.get_group_name(gId), gName)

        # Create internal edges
        double_link_region_to_edge(M)

        # Testing the generation of the internal edges group
        internal_name = '_' + gName + '_MANTICORE_GENERATED_INTERNAL_'
        internal = M.find_by_name(internal_name)
        n_internal = internal.size(StandardGeometry.EDGE2)
        self.assertIsNotNone(internal)
        self.assertEqual(M.size_by_dimension(1), n_boundaries + n_internal)

        # Testing the existence of internal edges (using the same
        # vertices' IDs for generating the same hash IDs on vertices
        # and then on all entities built from them. Hash IDs are the
        # criteria of search.)
        v4 = Vertex(4, StandardGeometry.VERTEX1, vertices[4, 0],
                    vertices[4, 1])
        v5 = Vertex(5, StandardGeometry.VERTEX1, vertices[5, 0],
                    vertices[5, 1])
        v6 = Vertex(6, StandardGeometry.VERTEX1, vertices[6, 0],
                    vertices[6, 1])
        v7 = Vertex(7, StandardGeometry.VERTEX1, vertices[7, 0],
                    vertices[7, 1])
        v8 = Vertex(8, StandardGeometry.VERTEX1, vertices[8, 0],
                    vertices[8, 1])

        # Internal Edge 0
        edge0 = Edge(0, StandardGeometry.EDGE2)
        edge0.push_back(v5)
        edge0.push_back(v4)
        edge0.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge0)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 1
        edge1 = Edge(1, StandardGeometry.EDGE2)
        edge1.push_back(v6)
        edge1.push_back(v4)
        edge1.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge1)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 2
        edge2 = Edge(2, StandardGeometry.EDGE2)
        edge2.push_back(v7)
        edge2.push_back(v4)
        edge2.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge2)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 3
        edge3 = Edge(3, StandardGeometry.EDGE2)
        edge3.push_back(v8)
        edge3.push_back(v4)
        edge3.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge3)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

    def test_4quad9(self):
        # Mesh
        M = UnstructuredMesh()

        # Nodes
        vertices = numpy.array([
            (0.0, 0.0),  # 0
            (1.0, 0.0),  # 1
            (1.0, 1.0),  # 2
            (0.0, 1.0),  # 3
            (0.5, 0.5),  # 4
            (0.5, 0.0),  # 5
            (1.0, 0.5),  # 6
            (0.5, 1.0),  # 7
            (0.0, 0.5),  # 8
            (0.25, 0.00),  # 9
            (0.75, 0.00),  # 10
            (1.00, 0.25),  # 11
            (1.00, 0.75),  # 12
            (0.75, 1.00),  # 13
            (0.25, 1.00),  # 14
            (0.00, 0.75),  # 15
            (0.00, 0.25),  # 16
            (0.25, 0.25),  # 17
            (0.50, 0.25),  # 18
            (0.75, 0.25),  # 19
            (0.75, 0.50),  # 20
            (0.75, 0.75),  # 21
            (0.50, 0.75),  # 22
            (0.25, 0.75),  # 23
            (0.25, 0.50)
        ])  # 24

        n_vertices = vertices.shape[0]

        for ii in range(n_vertices):
            M.create_vertex(ii, vertices[ii, 0], vertices[ii, 1])

        # Elements
        gName = "MAIN"
        gId = M.create_group(gName, ConceptualGroup.Type.SUPERFICIAL, 2, 2)

        quads = [
            [0, 5, 4, 8, 9, 18, 24, 16, 17],  # 0
            [5, 1, 6, 4, 10, 11, 20, 18, 19],  # 1
            [4, 6, 2, 7, 20, 12, 13, 22, 21],  # 2
            [8, 4, 7, 3, 24, 22, 14, 15, 23]
        ]  # 3

        Idx = 0  # Index generator

        n_elements = len(quads)

        for ii in range(n_elements):
            M.create_area(gId, Idx, StandardGeometry.QUAD9, quads[ii])
            Idx += 1

        # Boundary

        Idx = 0

        boundary = [
            [2, 13, 7],
            [7, 14, 3],  # Front
            [0, 9, 5],
            [5, 10, 1],  # Back
            [3, 15, 8],
            [8, 16, 0],  # Left
            [1, 11, 6],
            [6, 12, 2]
        ]  # Right

        n_boundaries = len(boundary)

        # Front
        gId = M.create_group("FRONT", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[0])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[1])
        Idx += 1

        # Back
        gId = M.create_group("BACK", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[2])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[3])
        Idx += 1

        # Left
        gId = M.create_group("LEFT", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[4])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[5])
        Idx += 1

        # Right
        gId = M.create_group("RIGHT", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[6])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[7])
        Idx += 1

        # Testing mesh dimensions
        self.assertEqual(M.size_by_dimension(0), n_vertices)
        self.assertEqual(M.size_by_dimension(1), n_boundaries)
        self.assertEqual(M.size_by_dimension(2), n_elements)

        group = M.find_by_name(gName)
        gId = group.get_id()

        self.assertEqual(M.get_group_name(gId), gName)

        # Create internal edges
        double_link_region_to_edge(M)

        # Testing the generation of the internal edges group
        internal_name = '_' + gName + '_MANTICORE_GENERATED_INTERNAL_'
        internal = M.find_by_name(internal_name)
        n_internal = internal.size(StandardGeometry.EDGE3)
        self.assertIsNotNone(internal)
        self.assertEqual(M.size_by_dimension(1), n_boundaries + n_internal)

        # Testing the existence of internal edges (using the same
        # vertices' IDs for generating the same hash IDs on vertices
        # and then on all entities built from them. Hash IDs are the
        # criteria of search.)
        v4 = Vertex(4, StandardGeometry.VERTEX1, vertices[4, 0],
                    vertices[4, 1])
        v5 = Vertex(5, StandardGeometry.VERTEX1, vertices[5, 0],
                    vertices[5, 1])
        v6 = Vertex(6, StandardGeometry.VERTEX1, vertices[6, 0],
                    vertices[6, 1])
        v7 = Vertex(7, StandardGeometry.VERTEX1, vertices[7, 0],
                    vertices[7, 1])
        v8 = Vertex(8, StandardGeometry.VERTEX1, vertices[8, 0],
                    vertices[8, 1])
        v18 = Vertex(18, StandardGeometry.VERTEX1, vertices[18, 0],
                     vertices[18, 1])
        v20 = Vertex(20, StandardGeometry.VERTEX1, vertices[20, 0],
                     vertices[20, 1])
        v22 = Vertex(22, StandardGeometry.VERTEX1, vertices[22, 0],
                     vertices[22, 1])
        v24 = Vertex(24, StandardGeometry.VERTEX1, vertices[24, 0],
                     vertices[24, 1])

        # Internal Edge 0
        edge0 = Edge(0, StandardGeometry.EDGE3)
        edge0.push_back(v5)
        edge0.push_back(v18)
        edge0.push_back(v4)
        edge0.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge0)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 1
        edge1 = Edge(1, StandardGeometry.EDGE3)
        edge1.push_back(v6)
        edge1.push_back(v20)
        edge1.push_back(v4)
        edge1.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge1)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 2
        edge2 = Edge(2, StandardGeometry.EDGE3)
        edge2.push_back(v7)
        edge2.push_back(v22)
        edge2.push_back(v4)
        edge2.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge2)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 3
        edge3 = Edge(3, StandardGeometry.EDGE3)
        edge3.push_back(v8)
        edge3.push_back(v24)
        edge3.push_back(v4)
        edge3.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge3)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

    def test_mixed(self):
        # Mesh
        M = UnstructuredMesh()

        # Nodes
        vertices = numpy.array([
            (0.0, 0.0),  # 0
            (1.0, 0.0),  # 1
            (1.0, 1.0),  # 2
            (0.0, 1.0),  # 3
            (0.5, 0.5),  # 4
            (0.5, 0.0),  # 5
            (1.0, 0.5),  # 6
            (0.5, 1.0),  # 7
            (0.0, 0.5),  # 8
            (0.25, 0.00),  # 9
            (0.75, 0.00),  # 10
            (1.00, 0.25),  # 11
            (1.00, 0.75),  # 12
            (0.75, 1.00),  # 13
            (0.25, 1.00),  # 14
            (0.00, 0.75),  # 15
            (0.00, 0.25),  # 16
            (0.25, 0.25),  # 17
            (0.50, 0.25),  # 18
            (0.75, 0.25),  # 19
            (0.75, 0.50),  # 20
            (0.75, 0.75),  # 21
            (0.50, 0.75),  # 22
            (0.25, 0.75),  # 23
            (0.25, 0.50)
        ])  # 24

        n_vertices = vertices.shape[0]

        for ii in range(n_vertices):
            M.create_vertex(ii, vertices[ii, 0], vertices[ii, 1])

        # Elements
        gName = "MAIN"
        gId = M.create_group(gName, ConceptualGroup.Type.SUPERFICIAL, 2, 2)

        quads = [
            [0, 5, 4, 8, 9, 18, 24, 16, 17],  # 0
            [5, 1, 6, 4, 10, 11, 20, 18, 19],  # 1
            [4, 6, 2, 7, 20, 12, 13, 22, 21]
        ]  # 2

        trias = [
            [8, 4, 7, 24, 22, 23],  # 3
            [8, 7, 3, 23, 14, 15]
        ]  # 4

        Idx = 0  # Index generator

        n_quads = len(quads)
        n_trias = len(trias)
        n_elements = n_quads + n_trias

        for ii in range(n_quads):
            M.create_area(gId, Idx, StandardGeometry.QUAD9, quads[ii])
            Idx += 1

        for ii in range(n_trias):
            M.create_area(gId, Idx, StandardGeometry.TRI6, trias[ii])
            Idx += 1

        # Boundary

        Idx = 0

        boundary = [
            [2, 13, 7],
            [7, 14, 3],  # Front
            [0, 9, 5],
            [5, 10, 1],  # Back
            [3, 15, 8],
            [8, 16, 0],  # Left
            [1, 11, 6],
            [6, 12, 2]
        ]  # Right

        n_boundaries = len(boundary)

        # Front
        gId = M.create_group("FRONT", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[0])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[1])
        Idx += 1

        # Back
        gId = M.create_group("BACK", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[2])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[3])
        Idx += 1

        # Left
        gId = M.create_group("LEFT", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[4])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[5])
        Idx += 1

        # Right
        gId = M.create_group("RIGHT", ConceptualGroup.Type.BOUNDARY, 1, 2)
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[6])
        Idx += 1
        M.create_edge(gId, Idx, StandardGeometry.EDGE3, boundary[7])
        Idx += 1

        # Testing mesh dimensions
        self.assertEqual(M.size_by_dimension(0), n_vertices)
        self.assertEqual(M.size_by_dimension(1), n_boundaries)
        self.assertEqual(M.size_by_dimension(2), n_elements)

        group = M.find_by_name(gName)
        gId = group.get_id()

        self.assertEqual(M.get_group_name(gId), gName)

        # Create internal edges
        double_link_region_to_edge(M)

        # Testing the generation of the internal edges group
        internal_name = '_' + gName + '_MANTICORE_GENERATED_INTERNAL_'
        internal = M.find_by_name(internal_name)
        n_internal = internal.size(StandardGeometry.EDGE3)
        self.assertIsNotNone(internal)
        self.assertEqual(M.size_by_dimension(1), n_boundaries + n_internal)

        # Testing the existence of internal edges (using the same
        # vertices' IDs for generating the same hash IDs on vertices
        # and then on all entities built from them. Hash IDs are the
        # criteria of search.)
        v4 = Vertex(4, StandardGeometry.VERTEX1, vertices[4, 0],
                    vertices[4, 1])
        v5 = Vertex(5, StandardGeometry.VERTEX1, vertices[5, 0],
                    vertices[5, 1])
        v6 = Vertex(6, StandardGeometry.VERTEX1, vertices[6, 0],
                    vertices[6, 1])
        v7 = Vertex(7, StandardGeometry.VERTEX1, vertices[7, 0],
                    vertices[7, 1])
        v8 = Vertex(8, StandardGeometry.VERTEX1, vertices[8, 0],
                    vertices[8, 1])
        v18 = Vertex(18, StandardGeometry.VERTEX1, vertices[18, 0],
                     vertices[18, 1])
        v20 = Vertex(20, StandardGeometry.VERTEX1, vertices[20, 0],
                     vertices[20, 1])
        v22 = Vertex(22, StandardGeometry.VERTEX1, vertices[22, 0],
                     vertices[22, 1])
        v23 = Vertex(23, StandardGeometry.VERTEX1, vertices[23, 0],
                     vertices[23, 1])
        v24 = Vertex(24, StandardGeometry.VERTEX1, vertices[24, 0],
                     vertices[24, 1])

        # Internal Edge 0
        edge0 = Edge(0, StandardGeometry.EDGE3)
        edge0.push_back(v5)
        edge0.push_back(v18)
        edge0.push_back(v4)
        edge0.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge0)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 1
        edge1 = Edge(1, StandardGeometry.EDGE3)
        edge1.push_back(v6)
        edge1.push_back(v20)
        edge1.push_back(v4)
        edge1.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge1)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have four neibouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_0), 4)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        self.assertEqual(len(back_found_1), 4)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(4):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(4):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 2
        edge2 = Edge(2, StandardGeometry.EDGE3)
        edge2.push_back(v7)
        edge2.push_back(v22)
        edge2.push_back(v4)
        edge2.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge2)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have the correct
        # number of neighbouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        num_edges_0 = number_of_edges(found_2d_adj[0].neighbour.get_type())
        self.assertEqual(len(back_found_0), num_edges_0)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        num_edges_1 = number_of_edges(found_2d_adj[1].neighbour.get_type())
        self.assertEqual(len(back_found_1), num_edges_1)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(num_edges_0):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(num_edges_1):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 3
        edge3 = Edge(3, StandardGeometry.EDGE3)
        edge3.push_back(v8)
        edge3.push_back(v24)
        edge3.push_back(v4)
        edge3.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge3)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have the correct
        # number of neighbouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        num_edges_0 = number_of_edges(found_2d_adj[0].neighbour.get_type())
        self.assertEqual(len(back_found_0), num_edges_0)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        num_edges_1 = number_of_edges(found_2d_adj[1].neighbour.get_type())
        self.assertEqual(len(back_found_1), num_edges_1)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(num_edges_0):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(num_edges_1):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)

        # Internal Edge 4
        edge4 = Edge(4, StandardGeometry.EDGE3)
        edge4.push_back(v8)
        edge4.push_back(v23)
        edge4.push_back(v7)
        edge4.generate_hash()

        # Does this edge exist in the mesh?
        found = M.find(edge4)
        self.assertIsNotNone(found)

        # Is this edge internal (that is, does it have two neighbouring areas)?
        found_2d_adj = found.get_adjacency(2)
        self.assertEqual(len(found_2d_adj), 2)

        # Back test: do those neighbouring areas have the correct
        # number of neighbouring edges?
        back_found_0 = found_2d_adj[0].neighbour.get_adjacency(1)
        num_edges_0 = number_of_edges(found_2d_adj[0].neighbour.get_type())
        self.assertEqual(len(back_found_0), num_edges_0)
        back_found_1 = found_2d_adj[1].neighbour.get_adjacency(1)
        num_edges_1 = number_of_edges(found_2d_adj[1].neighbour.get_type())
        self.assertEqual(len(back_found_1), num_edges_1)

        # Back test: do those four neibouring edges contain 'found'?
        test = False
        for e in range(num_edges_0):
            if back_found_0[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)
        test = False
        for e in range(num_edges_1):
            if back_found_1[e].neighbour.get_hash_id() == found.get_hash_id():
                test = True
        self.assertTrue(test)


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_umesh.py ------------------------------------------------------------
