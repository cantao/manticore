#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Geometric mesh integration tests
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#

from manticore.geom.standard_geometries import StandardGeometry
from manticore.mesh.entity import Vertex, Area, AreaToEdgeTemplate
from manticore.mesh.mecontainer import (MeshEntityContainer,
                                        SequentialIterator, IdentifierIterator,
                                        HashIdIterator)

print('\nG E O M E T R I C   M E S H   T E S T\n')
print(' ')
print('Types of geometric entities:')
print(StandardGeometry())
print(' ')

print('Creating Vertex objects:')
v = Vertex(10, StandardGeometry.VERTEX2, 56.7, 34.689)
w = Vertex(20, StandardGeometry.VERTEX2, 1456.89542, 998.6789)
y = Vertex(56, StandardGeometry.VERTEX2, 0.89542, -998.6789)

print(v)
print(' ')
print(w)
print(' ')

seq = 0
c = MeshEntityContainer()
v.set_seq_id(seq)
seq += 1
c.insert(v)
w.set_seq_id(seq)
seq += 1
c.insert(w)

it1 = IdentifierIterator(c)

for x in it1:
    print("[%s]\n%s\n" % (x[0], x[1]))

it2 = SequentialIterator(c)

for x in it2:
    print("%s\n" % x)

it3 = HashIdIterator(c)

for x in it3:
    print("[%s]\n%s\n" % (x[0], x[1]))

quad4 = Area(0, StandardGeometry.QUAD4)

# Incidence
quad4.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # A
quad4.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
quad4.push_back(Vertex(2, StandardGeometry.VERTEX1, 1.0, 1.0))  # C
quad4.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0))  # D

autoEdges = AreaToEdgeTemplate.area_to_edge(quad4)

# -- test_create_mesh_00.py ---------------------------------------------------
