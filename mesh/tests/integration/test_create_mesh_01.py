#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Geometric mesh integration tests
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy
from manticore.geom.standard_geometries import StandardGeometry
from manticore.mesh.umesh import (ConceptualGroup, UnstructuredMesh,
                                  double_link_region_to_edge)

print('\nG E O M E T R I C   M E S H   T E S T\n')

# Mesh
M = UnstructuredMesh()

# Nodes
vertices = numpy.array([
    (0.0, 0.0),  # 0
    (1.0, 0.0),  # 1
    (1.0, 1.0),  # 2
    (0.0, 1.0),  # 3
    (0.5, 0.5)
])  # 4

for v, i in enumerate(vertices):
    M.create_vertex(i, v[0], v[1])

# Elements
gId = M.create_group("MAIN", ConceptualGroup.Type.SUPERFICIAL, 2)

trias = [
    [0, 1, 4],  # 0
    [1, 2, 4],  # 1
    [2, 3, 4],  # 2
    [3, 0, 4]
]  # 3

n_elements = len(trias)

for e, i in enumerate(trias):
    M.create_area(gId, i, StandardGeometry.TRI3, e)

# Boundaries
boundary = {
    'FRONT': [2, 3],  # Front
    'BACK': [0, 1],  # Back
    'LEFT': [3, 0],  # Left
    'RIGHT': [1, 2]
}  # Right

for k, i in boundary:
    gId = M.create_group(k, ConceptualGroup.Type.BOUNDARY, 1)
    M.create_edge(gId, i, StandardGeometry.EDGE2, boundary[k])

group = M.find_by_name('MAIN')
gId = group.get_id()

double_link_region_to_edge(M)

print(M)
print(' ')
print(M.group_info(-1))
print(' ')
print(M.group_info(0))
print(' ')
print(M.group_info(5))
print(' ')

# -- test_create_mesh_01.py ---------------------------------------------------
