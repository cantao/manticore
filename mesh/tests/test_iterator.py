#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MESH tests
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#

import unittest
import numpy
from manticore.geom.standard_geometries import StandardGeometry
from manticore.mesh.entity import Vertex, Area


class IteratorTestCase(unittest.TestCase):
    def test_vertex_on_area(self):
        quad4 = Area(0, StandardGeometry.QUAD4)

        # Incidence
        quad4.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # A
        quad4.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        quad4.push_back(Vertex(2, StandardGeometry.VERTEX1, 1.0, 1.0))  # C
        quad4.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0))  # D

        # Template for testing incidence
        v = numpy.array([[0.0, 0.0], [1.0, 0.0], [1.0, 1.0], [0.0, 1.0]])

        ii = 0  # Vertex counter
        idx = 0

        for adj0 in quad4:
            # adj0 == 0-dimension AdjacencyInfo object ==>
            # adj0.neighbour == Vertex
            coords = adj0.neighbour.get_coordinates()
            check = coords == v[ii, :]
            self.assertTrue(check.all())
            ii += 1

            self.assertEqual(adj0.neighbour.get_id(), idx)
            idx += 1


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_iterator.py ---------------------------------------------------------
