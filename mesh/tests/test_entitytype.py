#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MESH tests
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#

import unittest
from manticore.geom.standard_geometries import (
    BaseGeometry, StandardGeometry, geom, order, dim, number_of_nodes,
    number_of_edges)


class StandardGeometryTestCase(unittest.TestCase):
    def test_quads(self):
        quads = [
            StandardGeometry.QUAD4, StandardGeometry.QUAD9,
            StandardGeometry.QUAD16, StandardGeometry.QUAD25
        ]

        for i, e in enumerate(quads):
            self.assertEqual(geom(e), BaseGeometry.QUAD)
            self.assertEqual(order(e), i)
            self.assertEqual(dim(e), 2)

        nnodes = [4, 9, 16, 25]

        for e, n in zip(quads, nnodes):
            self.assertEqual(number_of_nodes(e), n)
            self.assertEqual(number_of_edges(e), 4)

    def test_trias(self):
        trias = [
            StandardGeometry.TRI3, StandardGeometry.TRI6,
            StandardGeometry.TRI10, StandardGeometry.TRI15
        ]

        for i, e in enumerate(trias):
            self.assertEqual(geom(e), BaseGeometry.TRI)
            self.assertEqual(order(e), i)
            self.assertEqual(dim(e), 2)

        nnodes = [3, 6, 10, 15]

        for e, n in zip(trias, nnodes):
            self.assertEqual(number_of_nodes(e), n)
            self.assertEqual(number_of_edges(e), 3)

    def test_edges(self):
        edges = [
            StandardGeometry.EDGE2, StandardGeometry.EDGE3,
            StandardGeometry.EDGE4, StandardGeometry.EDGE5
        ]

        for i, e in enumerate(edges):
            self.assertEqual(geom(e), BaseGeometry.EDGE)
            self.assertEqual(order(e), i)
            self.assertEqual(dim(e), 1)

        nnodes = [2, 3, 4, 5]

        for e, n in zip(edges, nnodes):
            self.assertEqual(number_of_nodes(e), n)

    def test_verts(self):
        verts = [
            StandardGeometry.VERTEX1, StandardGeometry.VERTEX2,
            StandardGeometry.VERTEX3, StandardGeometry.VERTEX4
        ]

        for i, e in enumerate(verts):
            self.assertEqual(geom(e), BaseGeometry.VERTEX)
            self.assertEqual(order(e), i)
            self.assertEqual(dim(e), 0)

        for e in verts:
            self.assertEqual(number_of_nodes(e), 1)


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_entitytype.py -------------------------------------------------------
