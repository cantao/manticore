#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MESH tests
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#

import unittest
import numpy
import random
from manticore.mesh.entity import Vertex, Edge, Area
from manticore.geom.standard_geometries import StandardGeometry


class MeshEntityTestCase(unittest.TestCase):
    def test_vertex(self):
        v1 = Vertex(0, StandardGeometry.VERTEX1, 0.0, 1.0)

        self.assertEqual(v1.get_id(), 0)
        self.assertEqual(v1.get_type(), StandardGeometry.VERTEX1)

        info_uint64 = numpy.iinfo(numpy.uint64)

        random.seed(1)
        rnd = random.randrange(info_uint64.max)

        self.assertEqual(v1.get_hash_id(), rnd)

        a = v1.get_coordinates()
        b = numpy.array([0.0, 1.0])
        # Item by item comparison: c==array([True, True], dtype=bool)
        c = (a == b)
        self.assertTrue(c.all())

    def test_edge(self):
        e = Edge()

        self.assertTrue(e.empty(0))
        self.assertTrue(e.empty(1))
        self.assertTrue(e.empty(2))

        self.assertEqual(len(e.get_adjacency(0)), 0)
        self.assertEqual(len(e.get_adjacency(1)), 0)
        self.assertEqual(len(e.get_adjacency(2)), 0)

        self.assertEqual(e.get_id(), 0)
        self.assertEqual(e.get_hash_id(), 0)
        self.assertEqual(e.get_seq_id(), 0)

        v1 = Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0)
        v2 = Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0)

        e.set_id(5)
        e.set_type(StandardGeometry.EDGE2)
        e.push_back(v1)
        e.push_back(v2)

        e.generate_hash()

        self.assertTrue(e.get_hash_id() != 0)

        self.assertFalse(e.empty(0))
        self.assertTrue(e.empty(1))
        self.assertTrue(e.empty(2))

        self.assertEqual(len(e.get_adjacency(0)), 2)
        self.assertEqual(len(e.get_adjacency(1)), 0)
        self.assertEqual(len(e.get_adjacency(2)), 0)

        self.assertEqual(e.get_id(), 5)
        self.assertEqual(e.get_type(), StandardGeometry.EDGE2)

    def test_area(self):
        e = Area()

        self.assertTrue(e.empty(0))
        self.assertTrue(e.empty(1))
        self.assertTrue(e.empty(2))

        self.assertEqual(len(e.get_adjacency(0)), 0)
        self.assertEqual(len(e.get_adjacency(1)), 0)
        self.assertEqual(len(e.get_adjacency(2)), 0)

        self.assertEqual(e.get_id(), 0)
        self.assertEqual(e.get_hash_id(), 0)
        self.assertEqual(e.get_seq_id(), 0)

        v1 = Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0)
        v2 = Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0)
        v3 = Vertex(2, StandardGeometry.VERTEX1, 1.0, 1.0)
        v4 = Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0)

        e.set_id(15)
        e.set_type(StandardGeometry.QUAD4)
        e.push_back(v1)
        e.push_back(v2)
        e.push_back(v3)
        e.push_back(v4)

        e.generate_hash()

        self.assertTrue(e.get_hash_id() != 0)

        self.assertFalse(e.empty(0))
        self.assertTrue(e.empty(1))
        self.assertTrue(e.empty(2))

        self.assertEqual(len(e.get_adjacency(0)), 4)
        self.assertEqual(len(e.get_adjacency(1)), 0)
        self.assertEqual(len(e.get_adjacency(2)), 0)

        self.assertEqual(e.get_id(), 15)
        self.assertEqual(e.get_type(), StandardGeometry.QUAD4)


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_entity.py -----------------------------------------------------------
