#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MESH tests
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
# $ python3 -m unittest -v manticore/mesh/tests/test_container.py

import unittest
from manticore.geom.standard_geometries import StandardGeometry
from manticore.mesh.entity import Vertex, Edge, Area
from manticore.mesh.mecontainer import (MeshEntityContainer,
                                        IdentifierIterator, HashIdIterator)


class ContainerTestCase(unittest.TestCase):
    def setUp(self):
        self.C = MeshEntityContainer()
        self.C.insert(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))
        self.C.insert(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))
        self.C.insert(Vertex(2, StandardGeometry.VERTEX1, 1.0, 1.0))
        self.C.insert(Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0))

        self.C.insert(Edge(4, StandardGeometry.EDGE2))
        self.C.insert(Edge(5, StandardGeometry.EDGE2))
        self.C.insert(Edge(6, StandardGeometry.EDGE2))
        self.C.insert(Edge(7, StandardGeometry.EDGE2))

        self.C.insert(Area(8, StandardGeometry.QUAD4))
        self.C.insert(Area(9, StandardGeometry.TRI3))

    def test_insertion(self):
        self.assertEqual(self.C.size(), 10)

    def test_find_by_id(self):
        found = self.C.find_by_id(0)
        e = Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0)
        self.assertTrue(found == e)

        found = self.C.find_by_id(1)
        e = Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0)
        self.assertTrue(found == e)

        found = self.C.find_by_id(2)
        e = Vertex(2, StandardGeometry.VERTEX1, 1.0, 1.0)
        self.assertTrue(found == e)

        found = self.C.find_by_id(3)
        e = Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0)
        self.assertTrue(found == e)

        found = self.C.find_by_id(4)
        e = Edge(4, StandardGeometry.EDGE2)
        self.assertTrue(found == e)

        found = self.C.find_by_id(5)
        e = Edge(5, StandardGeometry.EDGE2)
        self.assertTrue(found == e)

        found = self.C.find_by_id(6)
        e = Edge(6, StandardGeometry.EDGE2)
        self.assertTrue(found == e)

        found = self.C.find_by_id(7)
        e = Edge(7, StandardGeometry.EDGE2)
        self.assertTrue(found == e)

    def test_identifier_iterator(self):
        it = IdentifierIterator(self.C)

        for x in it:
            self.assertEqual(x[0], x[1].get_id())

    def test_hashid_iterator(self):
        it = HashIdIterator(self.C)

        for x in it:
            self.assertEqual(x[0], x[1][0].get_hash_id())


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_container.py --------------------------------------------------------
