#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MESH tests
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#

import unittest
from manticore.geom.standard_geometries import StandardGeometry
from manticore.mesh.entity import Vertex, Area
from manticore.mesh.umesh import ConceptualGroup


class GroupTestCase(unittest.TestCase):
    def setUp(self):
        self.group = ConceptualGroup(0, 4, ConceptualGroup.Type.SUPERFICIAL, 2)
        self.numels = 6
        self.etype = StandardGeometry.TRI6

        for ii in range(self.numels):
            self.group.insert(Area(ii, self.etype))

    def test_insertion(self):
        self.assertEqual(self.group.size(self.etype), self.numels)

    def test_find_by_id(self):
        for ii in range(self.numels):
            self.assertTrue(self.group.find_by_id(self.etype, ii))

    def test_find(self):
        tri6 = Area(10, StandardGeometry.TRI6)

        # Incidence
        # Note that VERTEX1 can be used without any problem!!!!
        tri6.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # A
        tri6.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        tri6.push_back(Vertex(2, StandardGeometry.VERTEX1, 0.0, 1.0))  # C
        tri6.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.5, 0.0))  # D
        tri6.push_back(Vertex(4, StandardGeometry.VERTEX1, 0.5, 0.5))  # E
        tri6.push_back(Vertex(5, StandardGeometry.VERTEX1, 0.0, 0.5))  # F

        tri6.generate_hash()

        group = ConceptualGroup(0, 4, ConceptualGroup.Type.SUPERFICIAL, 2)
        group.insert(tri6)

        found = group.find(tri6)

        self.assertTrue(found)
        self.assertTrue(found[0] == tri6)
        self.assertFalse(group.find_by_id(StandardGeometry.TRI6, 234))

        # Testing with a different object with identical data
        tri6_b = Area(10, StandardGeometry.TRI6)
        tri6_b.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # A
        tri6_b.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        tri6_b.push_back(Vertex(2, StandardGeometry.VERTEX1, 0.0, 1.0))  # C
        tri6_b.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.5, 0.0))  # D
        tri6_b.push_back(Vertex(4, StandardGeometry.VERTEX1, 0.5, 0.5))  # E
        tri6_b.push_back(Vertex(5, StandardGeometry.VERTEX1, 0.0, 0.5))  # F

        tri6_b.generate_hash()

        found = group.find(tri6_b)

        self.assertTrue(found)
        self.assertTrue(found[0] == tri6_b)


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_group.py ------------------------------------------------------------
