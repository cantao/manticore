#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# MESH tests
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#
# @refs https://docs.python.org/3/library/unittest.html
#

import unittest
import numpy
from manticore.geom.standard_geometries import StandardGeometry
from manticore.mesh.entity import Vertex, Edge, Area, AreaToEdgeTemplate


class DecompositionTestCase(unittest.TestCase):
    def test_quad4(self):
        quad4 = Area(0, StandardGeometry.QUAD4)
        nunNodes = 4

        # Incidence
        quad4.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # A
        quad4.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        quad4.push_back(Vertex(2, StandardGeometry.VERTEX1, 1.0, 1.0))  # C
        quad4.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0))  # D

        # Template for testing incidence
        v = numpy.array([[0.0, 0.0], [1.0, 0.0], [1.0, 1.0], [0.0, 1.0]])

        a = quad4.get_adjacency(0)

        # Testing incidence
        for ii in range(nunNodes):
            coord = a[ii].neighbour.get_coordinates()
            check = coord == v[ii, :]
            self.assertTrue(check.all())

        handEdges = []

        edge = Edge(0, StandardGeometry.EDGE2)
        edge.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # A
        edge.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        handEdges.append(edge)

        edge = Edge(1, StandardGeometry.EDGE2)
        edge.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        edge.push_back(Vertex(2, StandardGeometry.VERTEX1, 1.0, 1.0))  # C
        handEdges.append(edge)

        edge = Edge(2, StandardGeometry.EDGE2)
        edge.push_back(Vertex(2, StandardGeometry.VERTEX1, 1.0, 1.0))  # C
        edge.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0))  # D
        handEdges.append(edge)

        edge = Edge(3, StandardGeometry.EDGE2)
        edge.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0))  # C
        edge.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # D
        handEdges.append(edge)

        autoEdges = AreaToEdgeTemplate.area_to_edge(quad4)

        # Testing decomposition and equality operator
        for ii in range(nunNodes):
            self.assertTrue(handEdges[ii] == autoEdges[ii])

    def test_tri3(self):
        tri3 = Area(0, StandardGeometry.TRI3)
        nunNodes = 3

        # Incidence
        tri3.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # A
        tri3.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        tri3.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0))  # D

        # Template for testing incidence
        v = numpy.array([[0.0, 0.0], [1.0, 0.0], [0.0, 1.0]])

        a = tri3.get_adjacency(0)

        # Testing incidence
        for ii in range(nunNodes):
            coord = a[ii].neighbour.get_coordinates()
            check = coord == v[ii, :]
            self.assertTrue(check.all())

        handEdges = []

        edge = Edge(0, StandardGeometry.EDGE2)
        edge.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # A
        edge.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        handEdges.append(edge)

        edge = Edge(1, StandardGeometry.EDGE2)
        edge.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        edge.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0))  # D
        handEdges.append(edge)

        edge = Edge(3, StandardGeometry.EDGE2)
        edge.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.0, 1.0))  # C
        edge.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # D
        handEdges.append(edge)

        autoEdges = AreaToEdgeTemplate.area_to_edge(tri3)

        # Testing decomposition and equality operator
        for ii in range(nunNodes):
            self.assertTrue(handEdges[ii] == autoEdges[ii])

    def test_quad9(self):
        quad9 = Area(0, StandardGeometry.QUAD9)
        numNodes = 9

        # Incidence
        quad9.push_back(Vertex(0, StandardGeometry.VERTEX2, 0.0, 0.0))  # A
        quad9.push_back(Vertex(1, StandardGeometry.VERTEX2, 1.0, 0.0))  # B
        quad9.push_back(Vertex(2, StandardGeometry.VERTEX2, 1.0, 1.0))  # C
        quad9.push_back(Vertex(3, StandardGeometry.VERTEX2, 0.0, 1.0))  # D
        quad9.push_back(Vertex(4, StandardGeometry.VERTEX2, 0.5, 0.0))  # E
        quad9.push_back(Vertex(5, StandardGeometry.VERTEX2, 1.0, 0.5))  # F
        quad9.push_back(Vertex(6, StandardGeometry.VERTEX2, 0.5, 1.0))  # G
        quad9.push_back(Vertex(7, StandardGeometry.VERTEX2, 0.0, 0.5))  # H
        quad9.push_back(Vertex(8, StandardGeometry.VERTEX2, 0.5, 0.5))  # I

        # Template for testing incidence
        v = numpy.array([[0.0, 0.0], [1.0, 0.0], [1.0, 1.0], [0.0,
                                                              1.0], [0.5, 0.0],
                         [1.0, 0.5], [0.5, 1.0], [0.0, 0.5], [0.5, 0.5]])

        a = quad9.get_adjacency(0)

        # Testing incidence
        for ii in range(numNodes):
            coord = a[ii].neighbour.get_coordinates()
            check = coord == v[ii, :]
            self.assertTrue(check.all())

        handEdges = []

        edge = Edge(0, StandardGeometry.EDGE3)
        edge.push_back(Vertex(0, StandardGeometry.VERTEX2, 0.0, 0.0))  # A
        edge.push_back(Vertex(4, StandardGeometry.VERTEX2, 0.5, 0.0))  # E
        edge.push_back(Vertex(1, StandardGeometry.VERTEX2, 1.0, 0.0))  # B
        handEdges.append(edge)

        edge = Edge(1, StandardGeometry.EDGE3)
        edge.push_back(Vertex(1, StandardGeometry.VERTEX2, 1.0, 0.0))  # B
        edge.push_back(Vertex(5, StandardGeometry.VERTEX2, 1.0, 0.5))  # F
        edge.push_back(Vertex(2, StandardGeometry.VERTEX2, 1.0, 1.0))  # C
        handEdges.append(edge)

        edge = Edge(2, StandardGeometry.EDGE3)
        edge.push_back(Vertex(2, StandardGeometry.VERTEX2, 1.0, 1.0))  # C
        edge.push_back(Vertex(6, StandardGeometry.VERTEX2, 0.5, 1.0))  # G
        edge.push_back(Vertex(3, StandardGeometry.VERTEX2, 0.0, 1.0))  # D
        handEdges.append(edge)

        edge = Edge(3, StandardGeometry.EDGE3)
        edge.push_back(Vertex(3, StandardGeometry.VERTEX2, 0.0, 1.0))  # C
        edge.push_back(Vertex(7, StandardGeometry.VERTEX2, 0.0, 0.5))  # H
        edge.push_back(Vertex(0, StandardGeometry.VERTEX2, 0.0, 0.0))  # D
        handEdges.append(edge)

        autoEdges = AreaToEdgeTemplate.area_to_edge(quad9)

        # Testing decomposition and equality operator
        for ii in range(4):
            self.assertTrue(handEdges[ii] == autoEdges[ii])

    def test_tri6(self):
        tri6 = Area(0, StandardGeometry.TRI6)
        numNodes = 6
        numEdges = 3

        # Incidence
        # Note that VERTEX1 can be used without any problem!!!!
        tri6.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))  # A
        tri6.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))  # B
        tri6.push_back(Vertex(2, StandardGeometry.VERTEX1, 0.0, 1.0))  # C
        tri6.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.5, 0.0))  # D
        tri6.push_back(Vertex(4, StandardGeometry.VERTEX1, 0.5, 0.5))  # E
        tri6.push_back(Vertex(5, StandardGeometry.VERTEX1, 0.0, 0.5))  # F

        # Template for testing incidence
        v = numpy.array([[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [0.5, 0.0],
                         [0.5, 0.5], [0.0, 0.5]])

        a = tri6.get_adjacency(0)

        # Testing incidence
        for ii in range(numNodes):
            coord = a[ii].neighbour.get_coordinates()
            check = coord == v[ii, :]
            self.assertTrue(check.all())

        handEdges = []

        edge = Edge(0, StandardGeometry.EDGE3)
        edge.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))
        edge.push_back(Vertex(3, StandardGeometry.VERTEX1, 0.5, 0.0))
        edge.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))
        handEdges.append(edge)

        edge = Edge(1, StandardGeometry.EDGE3)
        edge.push_back(Vertex(1, StandardGeometry.VERTEX1, 1.0, 0.0))
        edge.push_back(Vertex(4, StandardGeometry.VERTEX1, 0.5, 0.5))
        edge.push_back(Vertex(2, StandardGeometry.VERTEX1, 0.0, 1.0))
        handEdges.append(edge)

        edge = Edge(2, StandardGeometry.EDGE3)
        edge.push_back(Vertex(2, StandardGeometry.VERTEX1, 0.0, 1.0))
        edge.push_back(Vertex(5, StandardGeometry.VERTEX1, 0.0, 0.5))
        edge.push_back(Vertex(0, StandardGeometry.VERTEX1, 0.0, 0.0))
        handEdges.append(edge)

        autoEdges = AreaToEdgeTemplate.area_to_edge(tri6)

        # Testing decomposition and equality operator
        for ii in range(numEdges):
            self.assertTrue(handEdges[ii] == autoEdges[ii])


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_decomposition.py ----------------------------------------------------
