import numpy as np
import sys, glob
sys.path.insert(0, '.')
import manticore
from manticore.mesh.mesh_utils import MSH, mdf_factory
from manticore.visualization.PlotUtils import factory_plotter
from matplotlib import pyplot as plt
from manticore.mdf.enums import MapType, FieldType
from manticore.geom.standard_geometries import StandardGeometry, BaseGeometry

def main():

    #Path to the directory containing the files for Conversion
    folder_name = sys.argv[1]
    #Read the file names
    file_names = glob.glob(folder_name+'/*.msh')
    partitioning = False
    mdf_database = folder_name+'/mdf_database.json'

    p_order = {0: 4}

    for filename in file_names:

        MDF = mdf_factory('gmsh')
        mdf = MDF.get_instance(partitioning, mdf_database)
        mdf.LoadingMesh(filename)
        mdf.PreProcess()
        partition_id = 0
        mdf.WritingGeometry(partition_id, p_order)

if __name__ == "__main__":

    main()
