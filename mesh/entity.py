#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Geometric mesh abstractions
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy
import random
from abc import ABCMeta, abstractmethod
import manticore.services.smallops as smallops
from manticore.geom.standard_geometries import (StandardGeometry, dim,
                                                number_of_edges)


class AdjacencyInfo:
    """Adjacency information of a 2D geometric mesh entity.

    Adjacency here is a very simple concept. We have:

    - A reference to the adjacent entity (it should be the hash, but pointers
      are much more convenient here)

    - An id related to the local face (or edge, in 2D) number from the
      neighbour, numbered from 0 to the number of faces (or edges, in 2D) from
      the corresponding neighbour geometry minus one.
    """

    __slots__ = ['__neighbour', '__id']

    def __init__(self, neig=None, ID=-1):
        # Reference to the neighbouring entity
        self.__neighbour = neig
        # Local entity (e.g. face) id of the adjacent entity
        self.__id = ID

    def set_neighbour(self, e):
        self.__neighbour = e

    def get_neighbour(self):
        return self.__neighbour

    def set_id(self, ID):
        self.__id = ID

    def get_id(self):
        return self.__id

    @property
    def neighbour(self):
        return self.__neighbour

    @property
    def id(self):
        return self.__id

    def __str__(self):
        return 'Adjacent entity: LF = {}\n{}'.format(self.__id,
                                                     self.__neighbour)


class MeshEntity(metaclass=ABCMeta):
    """Base abstract class for a single geometric entity."""

    __slots__ = [
        'ID', 'hashID', 'seqID', 'partitions', 'type', 'adjacencies', 'it'
    ]

    def __init__(self, ID=0, metype=None):
        self.ID = ID  # Entity ID, from mesh generator (unique, nonsequential)
        self.hashID = numpy.uint64(0)  # Hash ID
        self.seqID = numpy.uint64(0)  # Sequential ID, used for fields
        self.partitions = []  # Partitions we live in
        self.type = metype  # Entity type

        # Container of adjacency information. This list will always have
        # exactly 3 positions, meaning the adjacencies to each possible
        # geometric dimension (0 to 2). Each adj in self.adjacencies is a
        # list of AdjacencyInfo objects.
        self.adjacencies = [[], [], []]

    @abstractmethod
    def get_coordinates(self):
        pass

    def empty(self, n):
        """Emptyness test: True if empty, False otherwise."""
        assert n < 3, 'Please, check your arguments!'
        return not self.adjacencies[n]

    def size(self, n):
        """Size per container"""
        assert n < 3, 'Please, check your arguments!'
        return len(self.adjacencies[n])

    def get_id(self):
        return self.ID

    def set_id(self, ID):
        self.ID = ID

    def get_hash_id(self):
        return self.hashID

    # def set_hash_id(self, hashID):
    #     self.hashID = hashID

    def get_seq_id(self):
        return self.seqID

    def set_seq_id(self, seqID):
        self.seqID = seqID

    def get_partition(self, idx):
        return self.partitions[idx]

    def set_partition(self, partition):
        """Specific for elements and their faces: only 1 or 2 partitions
           allowed."""
        if len(self.partitions) >= 2 and partition not in self.partitions:
            raise RuntimeError('Oops, more than 2 partitions not allowed!')
        else:
            self.partitions.append(partition)

    def number_of_partitons(self):
        return len(self.partitions)

    def get_type(self):
        return self.type

    def set_type(self, metype):
        self.type = metype

    def get_adjacency(self, dimension):
        assert dimension < 3, 'Please, check your arguments!'
        return self.adjacencies[dimension]

    def push_back(self, e, localId=-1):
        dimension = dim(e.get_type())
        self.adjacencies[dimension].append(AdjacencyInfo(e, localId))

    def generate_hash(self):
        """Hash generator"""
        N = len(self.adjacencies[0])
        assert N > 0

        # Note: numpy.nditer is required for generating an iterator
        # over a numpy.array
        gen = (k.get_neighbour().get_hash_id() for k in self.adjacencies[0])
        vHash = numpy.fromiter(gen, dtype=numpy.uint64, count=N)

        vHash.sort()  # Sort an array, in-place. Default: quicksort

        self.hashID = numpy.uint64(smallops.mean(vHash))

    @abstractmethod
    def show(self):
        pass

    def show_adjacency_data(self, dimension):
        adj = self.get_adjacency(dimension)
        msg = '('

        for a in adj:
            e = a.neighbour
            msg += ' [Local ID: {}, Neigh ID: {}, Neigh Hash: {}'.format(
                a.get_id(), e.get_id(), e.get_hash_id())
            msg += ' {}]'.format(e.show())

        msg += ' )'

        return msg

    def __str__(self):
        """User friendly output for using built in print()."""

        msg = 'ID: {}, Hash: {}, Partition(s): ['.format(self.ID, self.hashID)

        if self.partitions:
            msg += ', '.join(map(str, self.partitions))
        else:
            msg += "empty"

        msg += ']\nAdjacencies:\n'

        for ii, a in enumerate(self.adjacencies):
            msg += '[{}D] -> '.format(ii)
            msg += 'EMPTY' if not a else self.show_adjacency_data(ii)

            if ii < 2:
                msg += '\n'

        # Additional user friendly output that MUST BE implemented by
        # subclasses
        msg += '\n{}'.format(self.show())

        return msg

    def __repr__(self):
        msg = '<{} (ID: {}, Hash: {}, Partition(s): ['.format(
            self.__class__, self.ID, self.hashID)

        if self.partitions:
            msg += ', '.join(map(str, self.partitions))
        else:
            msg += "empty"

        msg += ']\nAdjacencies: ['

        for ii, a in enumerate(self.adjacencies):
            msg += '{}D: '.format(ii)
            msg += 'EMPTY' if not a else self.show_adjacency_data(ii)
            msg += '\n' if ii < 2 else ']'

        msg += ' {})>'.format(self.show())

        return msg

    def __eq__(self, other):
        """Comparison operator.

        Returns:
            bool: True if both entities have the same standard geometry and
                same vertices' IDs.
        """

        # Tests if MeshEntity
        if not isinstance(other, self.__class__):
            return False

        # Tests type
        if other.get_type() != self.type:
            return False

        # Tests adjacency dim 0
        a1 = other.get_adjacency(0)
        a2 = self.get_adjacency(0)

        # Tests number of vertices
        if len(a1) != len(a2):
            return False

        # Tests vertices' IDs
        return sorted(k.neighbour.get_id() for k in a1) == \
            sorted(k.neighbour.get_id() for k in a2)

    def __iter__(self):
        """Sets up an iterator over adjacency of 0 dimension (vertices)."""
        self.it = iter(self.get_adjacency(0))

        return self

    def __next__(self):
        """Maintains iteration over adjacency of 0 dimension (vertices)."""
        try:
            item = next(self.it)
        except StopIteration:
            raise StopIteration

        return item

    def __bool__(self):
        return self.type is not None


class Vertex(MeshEntity):
    """Single 2 coordinate vertex.

    The Vertex class is used to construct other entities' hash.
    """

    __slots__ = ['coordinates']

    def __init__(self, ID=0, metype=StandardGeometry.VERTEX1, x=0.0, y=0.0):
        super().__init__(ID, metype)

        self.coordinates = numpy.array([x, y])
        self.generate_hash()

    def set_id(self, ID):
        """Sets the ID for Vertex objects."""

        # Vertex is the root case of mesh entity types and its setup
        # is special.
        self.ID = ID
        self.generate_hash()

    def get_coordinates(self):
        return self.coordinates

    def generate_hash(self):
        """Hash generator for Vertex objects."""

        # Information about a numpy numeric type
        info_unit64 = numpy.iinfo(numpy.uint64)

        # Pseudo-random number generator
        random.seed(self.ID + 1)

        # Random integer in the maximum range allowed
        self.hashID = random.randrange(info_unit64.max)

    def show(self):
        return 'Coordinates: {}'.format(self.coordinates)

    def __eq__(self, other):
        """Comparison operator.

        Returns:
            bool: True if entites have the same StandardGeometry, same ID and
                same coordinates (this operator complements the equality test
                of MeshEntity which is mildly weak).
        """
        if not isinstance(other, self.__class__):
            return False

        if other.get_id() != self.ID:
            return False

        check = (other.get_coordinates() == self.coordinates)

        return (other.get_type() == self.type) and check.all()

    def __iter__(self):
        """Specialization for Vertex: a Vertex has no 0-dimension adjacency."""
        return self

    def __next__(self):
        """A Vertex has no 0-dimension adjacency."""
        raise StopIteration


class Edge(MeshEntity):
    """2D edge."""

    __slots__ = []

    def __init__(self, ID=0, metype=StandardGeometry.EDGE2):
        super().__init__(ID, metype)

    def show(self):
        pass

    def get_coordinates(self):
        assert False


class Area(MeshEntity):
    """Generic 2D area."""

    __slots__ = []

    def __init__(self, ID=0, metype=StandardGeometry.QUAD4):
        super().__init__(ID, metype)

    def show(self):
        pass

    def get_coordinates(self):
        assert False


class AreaToEdgeTemplate:
    """Template for decomposition of areas in edges."""

    # Geo X Edges X Vertices
    pattern = {
        StandardGeometry.QUAD4: [[0, 1], [2, 3], [3, 0], [1, 2]],
        StandardGeometry.TRI3: [[0, 1], [2, 0], [1, 2], [-1, -1]],

        # @todo Check this ordering with mesh generator standards
        # I adopted a simple ordering: sequentially from the
        # start to the end of a edge.

        # Quadratic
        StandardGeometry.QUAD9: [[0, 4, 1], [2, 6, 3], [3, 7, 0], [1, 5, 2]],
        StandardGeometry.TRI6: [[0, 3, 1], [2, 5, 0], [1, 4, 2], [-1, -1, -1]],

        # Cubic
        StandardGeometry.QUAD16: [[0, 4, 5, 1], [2, 8, 9, 3],
                                  [3, 10, 11, 0], [1, 6, 7, 2]],
        StandardGeometry.TRI10: [[0, 3, 4, 1], [2, 7, 8, 0], [1, 5, 6, 2],
                                 [-1, -1, -1, -1]],

        # Quartic
        StandardGeometry.QUAD25: [[0, 4, 5, 6, 1], [2, 10, 11, 12, 3],
                                  [3, 13, 14, 15, 0], [1, 7, 8, 9, 2]],
        StandardGeometry.TRI15: [[0, 3, 4, 5, 1], [2, 9, 10, 11, 0],
                                     [1, 6, 7, 8, 2], [-1, -1, -1, -1, -1]]
    }

    # Geo X Edges (number of nodes)
    nnodes = {
        StandardGeometry.QUAD4: [2, 2, 2, 2],
        StandardGeometry.TRI3: [2, 2, 2, None],

        # Quadratic
        StandardGeometry.QUAD9: [3, 3, 3, 3],
        StandardGeometry.TRI6: [3, 3, 3, None],

        # Cubic
        StandardGeometry.QUAD16: [4, 4, 4, 4],
        StandardGeometry.TRI10: [4, 4, 4, None],

        # Quartic
        StandardGeometry.QUAD25: [5, 5, 5, 5],
        StandardGeometry.TRI15: [5, 5, 5, None]
    }

    # Geo X Edges (types)
    types = {
        StandardGeometry.QUAD4: [
            StandardGeometry.EDGE2, StandardGeometry.EDGE2,
            StandardGeometry.EDGE2, StandardGeometry.EDGE2
        ],
        StandardGeometry.TRI3: [
            StandardGeometry.EDGE2, StandardGeometry.EDGE2,
            StandardGeometry.EDGE2, None
        ],

        # Quadratic
        StandardGeometry.QUAD9: [
            StandardGeometry.EDGE3, StandardGeometry.EDGE3,
            StandardGeometry.EDGE3, StandardGeometry.EDGE3
        ],
        StandardGeometry.TRI6: [
            StandardGeometry.EDGE3, StandardGeometry.EDGE3,
            StandardGeometry.EDGE3, None
        ],

        # Cubic
        StandardGeometry.QUAD16: [
            StandardGeometry.EDGE4, StandardGeometry.EDGE4,
            StandardGeometry.EDGE4, StandardGeometry.EDGE4
        ],
        StandardGeometry.TRI10: [
            StandardGeometry.EDGE4, StandardGeometry.EDGE4,
            StandardGeometry.EDGE4, None
        ],

        # Quartic
        StandardGeometry.QUAD25: [
            StandardGeometry.EDGE5, StandardGeometry.EDGE5,
            StandardGeometry.EDGE5, StandardGeometry.EDGE5
        ],
        StandardGeometry.TRI15: [
            StandardGeometry.EDGE5, StandardGeometry.EDGE5,
            StandardGeometry.EDGE5, None
        ]
    }

    def area_to_edge(a):
        """Creates a list of edges of a given area."""
        assert a
        Type = a.get_type()
        assert Type is not None
        assert not a.empty(0) and dim(Type) == 2

        adj = a.get_adjacency(0)
        N = number_of_edges(Type)
        e = list()

        for ii in range(N):
            nods = AreaToEdgeTemplate.nnodes[Type][ii]
            assert nods is not None
            edge = Edge(-1, AreaToEdgeTemplate.types[Type][ii])
            edge_push_back = edge.push_back

            for jj in range(nods):
                pattern = AreaToEdgeTemplate.pattern[Type][ii][jj]
                assert pattern is not None
                adjInfo = adj[pattern]  # testar se a referencia vai funcionar!
                edge_push_back(adjInfo.neighbour, adjInfo.id)

            # Note: the ID=-1 is meant to temporary (will be corrected
            # elsewhere) but edge.hashID is correct!
            edge.generate_hash()
            e.append(edge)

        return e


# -- entity.py ----------------------------------------------------------------
