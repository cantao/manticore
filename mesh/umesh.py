#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Structures for the representation of a unstructured mesh.
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy
from enum import Enum
from collections import defaultdict
from manticore.geom.standard_geometries import (
    StandardGeometry, dim, number_of_nodes, std_geometry_as_list)
from manticore.mesh.entity import Vertex, Edge, Area, AreaToEdgeTemplate
from manticore.mesh.mecontainer import MeshEntityContainer, SequentialIterator
from manticore.services.datatypes import bidict


class ConceptualGroup:
    """Group of MeshEntityContainers sharing some property.

    The ConceptualGroup carries a whole set of entities sharing something in
       common (material, for instance).
    """

    class Type(Enum):
        NONE = 1  # No group
        SUPERFICIAL = 2  # 2D elements
        BOUNDARY = 3  # Boundary faces
        VERTICES = 4  # Special vertex group
        INTERNAL = 5  # Faces created by Edros
        INTERFACE = 6  # Faces between different domains
        PARALLEL = 7  # Faces between partitions

    def __init__(self,
                 group_id=None,
                 order=0,
                 group_type=Type.NONE,
                 dimension=None):
        self.group_id = group_id
        self.order = order  # Field polynomial order
        self.group_type = group_type
        self.dimension = dimension  # Geometric dimension
        self.container = defaultdict(MeshEntityContainer)

        # Dictionary of containers indexed by StandardGeometry
        for etype in std_geometry_as_list():
            self.container[etype]

    def get_id(self):
        return self.group_id

    def get_order(self):
        return self.order

    def get_type(self):
        return self.group_type

    def get_dimension(self):
        return self.dimension

    def set_id(self, group_id):
        self.group_id = group_id

    def set_order(self, order):
        self.order = order

    def set_type(self, group_type):
        self.group_type = group_type

    def set_dimension(self, dimension):
        self.dimension = dimension

    def size(self, etype):
        assert etype is not None
        return self.container[etype].size()

    def insert(self, e):
        etype = e.get_type()
        assert etype is not None
        self.container[etype].insert(e)

    def find(self, e):
        """Returns list of mesh entities with the same hash. Disambiguation
           must be done elsewhere."""
        etype = e.get_type()
        assert etype is not None
        return self.container[etype].find(e)

    def find_by_id(self, etype, ID):
        assert etype is not None
        return self.container[etype].find_by_id(ID)

    def get_container(self, etype):
        assert etype is not None
        return self.container[etype]

    def __iter__(self):
        """Iterator over the dictionary of containers."""
        self.it = iter(self.container)
        return self

    def __next__(self):
        """Iterator over the dictionary of conatiners."""
        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.container[key]

    def __contains__(self, etype):  # Obvious by construction.
        return etype in self.container

    def __str__(self):
        return '[ '.join([
            '({} {})' % (v.size(), k) for k, v in self.container.items()
            if v.size()
        ]) + ']'


class GroupContainer:
    """Multi index list of groups. Items can be accessed by ID (unique),
       order, type or dimension.
    """

    def __init__(self):
        # Each dictionary reflects a different indexation. The real
        # storage is in self.groups_by_id.
        self.groups_by_id = defaultdict(ConceptualGroup)
        self.groups_by_order = defaultdict(list)
        self.groups_by_type = defaultdict(list)  # ConceptualGroup.Type
        self.groups_by_dim = defaultdict(list)

    def get_by_id(self, ID):
        return self.groups_by_id.get(ID)

    def get_by_order(self, order):
        return self.groups_by_order.get(order)

    def get_by_type(self, gtype):
        return self.groups_by_type.get(gtype)

    def get_by_dimension(self, dim):
        return self.groups_by_dim.get(dim)

    def number_groups(self):
        return len(self.groups_by_id)

    def insert(self, group):
        ID = group.get_id()

        order = group.get_order()
        gtype = group.get_type()
        dim = group.get_dimension()

        self.groups_by_id[ID] = group
        self.groups_by_order[order].append(self.groups_by_id[ID])
        self.groups_by_type[gtype].append(self.groups_by_id[ID])
        self.groups_by_dim[dim].append(self.groups_by_id[ID])


class GroupTypeIterator:
    """Iterator for GroupContainer objects. The iteration acts on groups
       of the same ConceptualGroup.Type value."""

    def __init__(self, container, gtype):
        self.wrapped = container
        self.gtype = gtype

    def __iter__(self):
        self.it = iter(self.wrapped.groups_by_type[self.gtype])
        return self

    def __next__(self):
        try:
            item = next(self.it)
        except StopIteration:
            raise StopIteration

        return item


class GroupDimensionIterator:
    """Iterator for GroupContainer objects. The iteration acts on groups
       of the same dimension value."""

    def __init__(self, container, dim):
        self.wrapped = container
        self.dim = dim

    def __iter__(self):
        self.it = iter(self.wrapped.groups_by_dim[self.dim])
        return self

    def __next__(self):
        try:
            item = next(self.it)
        except StopIteration:
            raise StopIteration

        return item


class GroupIDIterator:
    """Iterator for GroupContainer objects. The iteration acts on the ID
       map (not ordered sequentially by ID, though)."""

    def __init__(self, container):
        self.wrapped = container

    def __iter__(self):
        self.it = iter(self.wrapped.groups_by_id)
        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.wrapped.groups_by_id[key]

    def __contains__(self, ID):
        return ID in self.wrapped.groups_by_id


class UnstructuredMesh:
    """Storage of unstructured geometric mesh data."""
    available_id = -1  # Entity id generator
    sequential_id = {}  # Per group, element sequential numbering

    def __init__(self):
        self.num_partitions = 1
        self.group_id_generator = 0
        self.groups = GroupContainer()

        # Only one group for all mesh vertices. That group is created
        # and manipulated internally only. This a way if using the
        # same generic structure of "group of mesh entities" to store
        # nodes which, in general, are implemented in an exclusive
        # data structure. In fact, "group of mesh entities" is more
        # closely related to elements.
        # self.group_map = {"VERTICES": const.INVALID_RESULT} # {string: int}
        self.group_map = bidict({"VERTICES": None})
        self.groups.insert(
            ConceptualGroup(None, 0, ConceptualGroup.Type.VERTICES, 0))

        # Shortcut to the group just created above
        self.vertex_ptr = self.groups.get_by_type(
            ConceptualGroup.Type.VERTICES)[0]

    def size_by_dimension(self, dimension):
        # Iterator over list of all ConceptualGroup objects of
        # this dimension
        groups_this_dim = GroupDimensionIterator(self.groups, dimension)
        size = 0

        for group in groups_this_dim:
            # ConceptualGroup class defines an iterator over a dictionary of
            # MeshContainer objects indexed by StandardGeometry values. That
            # iterator returns a tuple t=(key, container[key]). t[1] access a
            # MeshContainer object. As the sizes of all types are added, this
            # procedure assumes that the group was correctly initialized and
            # populated (so types of different dimension will be empty and than
            # will add zero to size).
            for types in group:
                size += types[1].size()

        return size

    def create_group(self, group_name, group_type, group_dim, group_order=0):
        if group_name in self.group_map:  # group already exists
            gId = self.group_map[group_name]
        else:
            gId = self.group_id_generator

            self.group_map[group_name] = gId

            self.groups.insert(
                ConceptualGroup(gId, group_order, group_type, group_dim))

            self.group_id_generator += 1

            UnstructuredMesh.sequential_id[gId] = numpy.uint64(0)

        return gId

    def create_vertex(self, entity_id, x, y):
        """Creates vertex StandardGeometry.VERTEX1 with coordidates (x,y)."""
        etype = StandardGeometry.VERTEX1
        self.vertex_ptr.insert(Vertex(entity_id, etype, x, y))

    def create_edge(self, group_id, entity_id, entity_type, vertices_ids,
                    nc=0):
        assert entity_type is not None
        assert dim(entity_type) == 1

        self.finalize_entity(group_id, vertices_ids,
                             Edge(entity_id, entity_type), nc)

        UnstructuredMesh.available_id = max(UnstructuredMesh.available_id,
                                            entity_id)

    def create_area(self, group_id, entity_id, entity_type, vertices_ids,
                    nc=0):
        assert entity_type is not None
        assert dim(entity_type) == 2

        entity = self.finalize_entity(group_id, vertices_ids,
                                      Area(entity_id, entity_type), nc)

        UnstructuredMesh.sequential_id[group_id] += 1

        # Set internal, sequential number
        entity.set_seq_id(UnstructuredMesh.sequential_id[group_id])

        UnstructuredMesh.available_id = max(UnstructuredMesh.available_id,
                                            entity_id)

    def find_vertex(self, entity_id):
        """Finds a vertex object by its ID."""
        mec = self.vertex_ptr.get_container(StandardGeometry.VERTEX1)

        return mec.find_by_id(entity_id)

    def finalize_entity(self, group_id, vertices_ids, entity, nc):
        """Inserts vertices, generates hash ID and pushes this
           higher-than-zero-dimension mesh entity into a group of mesh
           entities.
        """
        g = self.groups.get_by_id(group_id)

        if g:
            N = number_of_nodes(entity.get_type())
            entity_push_back = entity.push_back
            self_find_vertex = self.find_vertex

            for ii in range(N):
                entity_push_back(self_find_vertex(vertices_ids[ii + nc]))

            entity.generate_hash()
            g.insert(entity)

            return entity
        else:
            raise RuntimeError('Inexistent group!')

    def find_by_id(self, group_id):
        """Find a group by its ID."""
        return self.groups.get_by_id(group_id)

    def find_by_name(self, group_name):
        """Find a group by its name."""
        return self.groups.get_by_id(self.group_map[group_name])

    def get_group_name(self, group_id):
        """Find the name of a group from its ID."""
        # Note that create_group forces a one-to-one bidirectional map.
        return self.group_map.inverse.get(group_id)[0]

    def find(self, entity):
        """Look for an mesh entity."""
        entity_type = entity.get_type()
        d = dim(entity_type)
        groups_this_dim = GroupDimensionIterator(self.groups, d)

        for group in groups_this_dim:
            list_entities = group.find(entity)

            if list_entities:
                if len(list_entities) == 1:
                    return list_entities[0]
                else:
                    for e in list_entities:
                        if e == entity:
                            return e

        return None

    def find_group(self, entity):
        """Given an entity, find its corresponding group."""
        entity_type = entity.get_type()
        d = dim(entity_type)
        groups_this_dim = GroupDimensionIterator(self.groups, d)

        for group in groups_this_dim:
            list_entities = group.find(entity)
            if list_entities:
                return group

        return None

    def get_number_of_partitions(self):
        return self.num_partitions

    def set_number_of_partitions(self, number_of_partitions):
        self.num_partitions = number_of_partitions

    def group_info(self, group_id):
        size = 0

        group = self.groups.get_by_id(group_id)

        if group:
            for types in group:
                size += types[1].size()

            name = "entities" if size > 1 else "entity"

            msg = "[%s, %sD (%s %s): %s] " % (self.get_group_name(group_id),
                                              group.get_dimension(), size,
                                              name, group)

        return msg

    def __str__(self):
        """User friendly output for using built in print()."""
        return self.__repr__()

    def __repr__(self):
        ngroups = self.groups.number_groups()

        name = "groups" if ngroups > 1 else "group"

        msg = "<%s object: [ %s %s: " % (self.__class__, ngroups, name)

        groups_by_id = GroupIDIterator(self.groups)

        for group in groups_by_id:
            size = 0
            for types in group[1]:
                size += types[1].size()

            name = "entities" if size > 1 else "entity"

            msg += "(%s, %sD [%s %s], ID: %s) " % (
                self.get_group_name(group[0]), group[1].get_dimension(), size,
                name, group[0])

        msg += "]>"

        return msg


def double_link_region_to_edge(mesh):
    """Creates the edge to element (and vice-versa) topology (2D <-> 1D)."""
    # Groups of areas (2d elements)
    groups_2d = GroupDimensionIterator(mesh.groups, 2)
    mesh_find = mesh.find
    func_area_to_edge = AreaToEdgeTemplate.area_to_edge

    # Creates an independent group of internal edges for each 2d group

    # import pdb
    # pdb.set_trace()


    # There will be no redudancy as UnstructuredMesh.find searches all
    # groups of a given dimension. If the same edge already exists
    # (same vertices leads to same hashID) it will be found.
    for group in groups_2d:
        g2d_name = mesh.get_group_name(group.get_id())
        name = '_' + g2d_name + '_MANTICORE_GENERATED_INTERNAL_'
        mesh.create_group(name, ConceptualGroup.Type.INTERNAL, 1)

        inner = mesh.find_by_name(name)
        innerId = 0 
        inner_insert = inner.insert

        for mesh_container in group:

            if not mesh_container[1].empty():
                entities = SequentialIterator(mesh_container[1])

                for entity in entities:

                    list_edges = func_area_to_edge(entity)
                    face_counter = 0
                    entity_push_back = entity.push_back

                    for edge in list_edges:
                        found = mesh_find(edge)  # search by edge.hashID

                        if not found:

                            # insert edge into the group of internal faces
                            edge.set_id(innerId)
                            innerId += 1
                            inner_insert(edge)

                            # double link
                            entity_push_back(edge, face_counter)
                            edge.push_back(entity, face_counter)  # same Id?
                            
                        else:
                            
                            # double link
                            entity_push_back(found, face_counter)
                            found.push_back(entity, face_counter)  # same Id?

                        face_counter += 1


def double_link_region_to_region(mesh):
    """Creates the element to element topology (2D <-> 2D). Must be
       preceded by double_link_region_to_edge.
    """
    # Groups of edges (1d elements)
    groups_1d = GroupDimensionIterator(mesh.groups, 1)

    for group in groups_1d:
        for mesh_container in group:
            if not mesh_container[1].empty():
                entities = SequentialIterator(mesh_container[1])

                # Iterate over 1D entities
                for entity in entities:
                    # Get adjacent 2D entities (region-to-edge
                    # topology must have already been computed)
                    alist = entity.get_adjacency(2)

                    if len(alist) > 1:  # Ok, this edge connects two areas.
                        # Linking areas (2D <-> 2D)
                        alist[0].get_neighbour().push_back(
                            alist[1].get_neighbour(), alist[1].get_id())
                        alist[1].get_neighbour().push_back(
                            alist[0].get_neighbour(), alist[0].get_id())


def double_link_vertex_to_region(mesh):
    """Creates the element to vertex topology (0D <-> 2D)."""
    # Groups of edges (2d elements)
    groups_2d = GroupDimensionIterator(mesh.groups, 2)

    for group in groups_2d:
        for mesh_container in group:
            if not mesh_container[1].empty():
                entities = SequentialIterator(mesh_container[1])

                # Iterate over 2D entities
                for entity in entities:
                    alist = entity.get_adjacency(0)  # Get incidence

                    for a in alist:
                        a.get_neighbour().push_back(entity)  # link 0D <-> 2D


# -- umesh.py -----------------------------------------------------------------
