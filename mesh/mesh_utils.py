#Conversion MSH to MDF

#bin/bash/python3
#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
#Simple script to recover data from msh and mdf format in order
#to enable the visualization via ParaView

#This script is embiased to the unstructured grid vtk format
#because this the single extension used is the vtu

import numpy as np
import os,sys, bitstring
from itertools import groupby
from operator import itemgetter
import json
import logging

from manticore.services.datatypes import FlyweightMixin
from manticore.mdf.fielddata import FieldData
from manticore.mdf.file import File
from manticore.geom.standard_geometries import StandardGeometry, BaseGeometry
from manticore.mdf.enums import MapType, FieldType, Interpolation


#MSH (GMSH standard format) format interpretation
class MSH(FlyweightMixin):

    def __init__(self):

        #Gmsh format tags
        self.tags = { 'format':{'$MeshFormat':None, '$EndMeshFormat':None},\
                     'names':{'$PhysicalNames':None, '$EndPhysicalNames':None},\
                     'nodes':{'$Nodes':None, '$EndNodes':None},\
                     'elements':{'$Elements':None, '$EndElements':None} }

        #Physical groups types
        self.physicalTypes = {1:'face', 2:'domain'}
        #Mesh entities types (the most important at least)
        self.elementTypes = {1:{'type':'edge', 'id':0}, 8:{'type':'2o-egde','id':0},
                            26:{'type': '3o-edge','id':0},27:{'type':'4o-edge','id':0},\
                             2:{'type':'triangle', 'id':1}, 9:{'type':'2o-triangle', 'id':1},\
                             21:{'type':'3o-triangle', 'id':1}, 23:{'type':'4o-triangle', 'id':1},
                             3:{'type':'quadrangle', 'id':0}, 10:{'type':'2o-quadrangle','id':0},\
                             36:{'type':'3o-quadrangle','id':0}, 37:{'type':'4o-quadrangle','id':0} }

        self.fieldsPositions = {'element_type':0, 'physical_group':3}

        self.MeshFormat = None
        self.PhysicalGroups = None
        self.PhysicalGroups_dict = None
        self.physicalDict = None
        self.NodesList = None
        self.NodesIndices = None
        self.EdgesList = None
        self.ElementsList = None
        self.BoundaryElIndices = None
        self.BoundaryElList = None
        self.DomainElList = None
        self.DomainElIndices = None

        self.NumberOfPhysical = None
        self.NumberOfNodes = None
        self.NumberOfBoundaryEl = None
        self.NumberOfDomainEl = None
        self.NumberOfElements = None

        #number standard gmsh columns
        self.NumberOfSatandard = 4
        self.NumberOfNodes_egde = None
        self.NumberOfNodes_element = None

        #Import geometric attributes
        self.gOrder = None
        self.element = None

    def _physicalGroup_template(self, physical_group):

        template = {'elements_list': None,'indices_list':None, 'group_name':physical_group[-1],\
                    'element_type':None, 'element_type_id':None, 'element': None,\
                    'element_type_rep':None, 'group_type':None, 'g_order':None}

        return template

    def _physicalGroup_filling(self,physicalGroup):

        position = self.fieldsPositions['element_type']
        element_type_id = self.physicalDict[physicalGroup][1][0,:][position]
        element_type_dict = self.elementTypes.get(element_type_id, None)
        element_type = element_type_dict.get('type', None)
        element_type_rep = element_type_dict.get('id', None)
        elements_list = self.physicalDict[physicalGroup][1]
        elements_list_conn = elements_list[:,self.NumberOfSatandard:]

        self.PhysicalGroups_dict[physicalGroup]['elements_list'] = elements_list_conn
        self.PhysicalGroups_dict[physicalGroup]['indices_list'] = self.physicalDict[physicalGroup][0]
        self.PhysicalGroups_dict[physicalGroup]['element_type'] = element_type
        self.PhysicalGroups_dict[physicalGroup]['element_type_id'] = element_type_id
        self.PhysicalGroups_dict[physicalGroup]['element_type_rep'] = element_type_rep

        identific = int(self._PhysicalGroups[physicalGroup][0])
        self.PhysicalGroups_dict[physicalGroup]['group_type'] = identific

        #Geometrical order to the elements in the mesh
        gOrder = self.NumberOfNodes_edge - 1
        self.PhysicalGroups_dict[physicalGroup]['g_order'] = gOrder

    def _removeInvalid(self, item):

        new_item = [subitem for subitem in item if subitem]
        name = new_item[-1].split(' ')
        name = '_'.join(name)
        new_item[-1] = name
        newest_item = ''.join(new_item)

        return newest_item

    def _correctString(self,string):

        string_n = string.replace('\n','')
        string_nr = string_n.replace('\r','')

        return string_nr

    def _localizeBlocks(self, stringList):

        for keyMother in self.tags.keys():
            Keys = self.tags[keyMother].keys()

            for keyChild in Keys:
                if keyChild in stringList:
                    self.tags[keyMother][keyChild] = stringList.index(keyChild)

    #It reads all the information available in the msh file
    def Reading(self,inputfile):

        UnitFile = open(inputfile,"rb")

        allFileLines = UnitFile.readlines()
        allFileLines_decoded = [item.decode('utf-8') for item in allFileLines]
        allFileLines_treated = [self._correctString(item) for item in allFileLines_decoded]
        self._localizeBlocks(allFileLines_treated)

        #Mesh format
        meshFormatString_index = self.tags['format']['$MeshFormat']+1
        self.MeshFormat = allFileLines_treated[meshFormatString_index]
        logging.debug('Mesh format {}'.format(self.MeshFormat))

        #Physical groups
        if self.tags['names']['$PhysicalNames']:

            physicalGroupsBegin_index = self.tags['names']['$PhysicalNames']+1
            self.NumberOfPhysical = int(allFileLines_treated[physicalGroupsBegin_index])
            physicalGroupsEnd_index = physicalGroupsBegin_index+self.NumberOfPhysical+1
            physicalNames = allFileLines_treated[physicalGroupsBegin_index+1:physicalGroupsEnd_index]
            _PhysicalNames = [item.split('"') for item in physicalNames]
            PhysicalNames = [ self._removeInvalid(item) for item in _PhysicalNames ]
            self.PhysicalGroups = [ item.split(' ') for item in PhysicalNames ]
            self._PhysicalGroups = {int(item[1]):item for item in self.PhysicalGroups}
            Names = [item[-1] for item in self.PhysicalGroups]
            logging.debug('Physical Groups: {}'.format(', '.join(Names)))

        #Nodes
        nodesBegin_index = self.tags['nodes']['$Nodes']+1
        self.NumberOfNodes = int(allFileLines_treated[nodesBegin_index])
        nodesEnd_index = nodesBegin_index+self.NumberOfNodes+1
        nodesList = allFileLines_treated[nodesBegin_index+1:nodesEnd_index]
        NodesList = [np.fromstring(item, sep=' ') for item in nodesList]
        NodesList = np.array(NodesList)
        self.NodesList = NodesList[:,1:]
        self.NodesIndices = NodesList[:,0]
        logging.debug('Nodes list read. {} nodes'.format(self.NumberOfNodes))

        #Elements (in general)
        elementsBegin_index = self.tags['elements']['$Elements']+1
        self.NumberOfElements = int(allFileLines_treated[elementsBegin_index])
        elementsEnd_index = elementsBegin_index+self.NumberOfElements+1
        elementsList = allFileLines_treated[elementsBegin_index+1:elementsEnd_index]
        ElementsList = [np.fromstring(item,dtype=np.int64, sep=' ') for item in elementsList]
        self.ElementsList = np.array(ElementsList)

        #Boundary elements
        self.NumberOfBoundaryEl = self.ElementsList[0].shape[0]
        self.NumberOfNodes_edge = self.NumberOfBoundaryEl - self.NumberOfSatandard -1

        #Domain elements
        self.NumberOfDomainEl = self.ElementsList[-1].shape[0]
        self.NumberOfNodes_elements = self.NumberOfDomainEl - self.NumberOfSatandard -1

        groupIter_element = groupby(self.ElementsList, key=lambda x:len(x))
        groupDict = dict()

        for key, group in groupIter_element:
            Group = list(group)
            groupDict.update({key:np.array(Group)})

        BoundaryElList = groupDict[self.NumberOfBoundaryEl]
        self.BoundaryElList = BoundaryElList[:,1:]
        self.BoundaryElIndices = BoundaryElList[:,0]
        logging.debug('Boundary elements list read. {} elements'.format(self.BoundaryElList.shape[0]))

        DomainElList = groupDict[self.NumberOfDomainEl]
        self.DomainElList = DomainElList[:,1:]
        self.DomainElIndices = DomainElList[:,0]
        logging.debug('Domain elements list read. {} elements'.format(self.DomainElList.shape[0]))

    #It parses and organizes the information
    def Parsing(self):

        self.physicalDict = dict()
        #It subdivides physical groups
        self.PhysicalGroups_dict = {int(physical_group[1]):self._physicalGroup_template(physical_group)\
        for physical_group in self.PhysicalGroups}

        group_key = self.fieldsPositions['physical_group']
        groupIter_physicalgroups = groupby(self.ElementsList, key=itemgetter(group_key))

        for key, group in groupIter_physicalgroups:

            Group = list(group)
            elementList = np.array(Group)

            indices = elementList[:,0]
            connectivityMatrix = elementList[:,1:]
            self.physicalDict.update( { int(key):[ indices, connectivityMatrix ] } )

        #It fills all the fields of each physical group item
        for physicalGroup in self.PhysicalGroups_dict:

            self._physicalGroup_filling(physicalGroup)


#There exist two ways for using the MDF class. For reading from gmsh and writing
#mdf storing information in a mdf_database ('gmsh') and without connection with a specif mesh format
#('independent')

def mdf_factory(meshFormat):

    if meshFormat == 'gmsh':

        class MDF(FlyweightMixin):

            def __init__(self, partitioning, mdf_database):


                self.partitioning = partitioning
                self.workdir = os.getcwd()

                self.mdfDatabase_file = mdf_database
                self._loadDatabase(self.mdfDatabase_file)

                self.partitions_list = dict()

            def _getDimension(self):

                third_dimension = self.mesh.NodesList[:,-1]
                check = np.sum(third_dimension)
                if check:
                    self.dim = 3
                else:
                    self.dim = 2
                    self.mesh.NodesList = self.mesh.NodesList[:,:-1]

            #It creates the set of partitions and models
            def _partitioning(self):

                #Subdivides the physical groups in domains and boundaries
                DomainGroups_keys = [item for item in self.mesh.PhysicalGroups_dict\
                if self.mesh.PhysicalGroups_dict[item]['group_type']>1]

                BoundaryGroups_keys = [item for item in self.mesh.PhysicalGroups_dict\
                if self.mesh.PhysicalGroups_dict[item]['group_type']==1]

                self.DomainGroups = {item:self.mesh.PhysicalGroups_dict[item] for item in DomainGroups_keys}
                self.BoundaryGroups = {item:self.mesh.PhysicalGroups_dict[item] for item in BoundaryGroups_keys}

                DomainNames = [self.DomainGroups[item]['group_name'] for item in self.DomainGroups]
                BoundaryNames = [self.BoundaryGroups[item]['group_name'] for item in self.BoundaryGroups]

                #Partitioned mode
                if self.partitioning:
                    pass
                #Serial mode
                else:
                    name = 'MainPartition'
                    self.description = 'Mesh for DG-FEM simulation'
                    n_nodes = self.mesh.NodesIndices.shape[0]
                    index = 0

                    self.partition_id = index
                    self.mdfObject.set_model_attr(self.description, interpolation=Interpolation.P_INTERPOL)

                    self.mdfObject.set_partition_attr(index,name,n_nodes=n_nodes, boundary_names=BoundaryNames,\
                    domain_names=DomainNames)

                    self.partitions_list.update({index:name})

            def _identifyElementType(self, physicalGroup):

                order = (bin(physicalGroup['g_order']-1)[2:]).zfill(4)
                dim = (bin(physicalGroup['group_type'])[2:]).zfill(2)
                geom =  (bin(physicalGroup['element_type_rep'])[2:]).zfill(2)

                element_bitstring = '0b' + ''.join(order) + ''.join(dim) + ''.join(geom)

                element = bitstring.Bits(bin=element_bitstring)

                return element, element_bitstring

            def _loadDatabase(self, filename):

                with open(filename, 'r') as db:
                    self.mdfdatabase_dict = json.load(db)
                db.close()

            def _saveDatabase(self, filename):

                with open(filename, 'w+') as db:
                    json.dump(self.mdfdatabase_dict, db,indent=4)
                db.close()

            def PreProcess(self):

                self.mdfObject = File(self.mdfFilename,'w')
                self._partitioning()
                self.mdfObject.create()

            def LoadingMesh(self, meshFilename):

                #It creates the mesh object and parses its data
                #preparing them for being used
                self.meshFilename = meshFilename
                self.mesh = MSH.get_instance()

                self.mesh.Reading(meshFilename)
                self.mesh.Parsing()

                self.mdfFilename = self.meshFilename.replace('.msh','.mdf')
                self.mdfdatabase_subdict = {self.mdfFilename:{'domains':{},'boundaries':{},'fields':{}}}
                self._getDimension()

            #Loads information from a mdf file previously stored
            def LoadingMDF(self,mdffilename):

                self.mdfFilename = mdffilename
                #It loads the mdf database from mdf_database.json
                self._loadDatabase(self.mdfDatabase_file)

                if self.mdfdatabase_dict['root'].get(self.mdfFilename, None):

                    self.mdfDomainsDict = self.mdfdatabase_dict['root'][self.mdfFilename]['domains']
                    self.mdfBoundariesDict = self.mdfdatabase_dict['root'][self.mdfFilename]['boundaries']
                    self.mdfFieldsDict = self.mdfdatabase_dict['root'][self.mdfFilename]['fields']

                    #Open a mdf file for reading
                    self.mdfObject = File(self.mdfFilename,mode='r')
                    self.mdfObject.read_model_attr()
                    self.mdfObject.read_partition_attr()

                else:
                    raise AssertionError("This file is not in the database!")


            def WritingGeometry(self, partition_id, p_order):

                self.SubDomains = dict()
                self.SubBoundaries = dict()

                #Write the nodes coordinates and indices
                self.mdfObject.write_nodes(self.dim, partition_id, self.mesh.NodesList, self.mesh.NodesIndices)

                #Write the elements connection matrix and indices for each subdomain and sub-boundary
                for idx, subboundary_id in enumerate(self.BoundaryGroups):

                    physicalGroup = self.mesh.PhysicalGroups_dict[subboundary_id]
                    conn = physicalGroup['elements_list']
                    indices = physicalGroup['indices_list']
                    element_type_id = int(physicalGroup['element_type_id'])
                    element, element_bitstring = self._identifyElementType(physicalGroup)
                    physicalGroup['element'] = element

                    subboundary_name = self.BoundaryGroups[subboundary_id]['group_name']
                    subboundary_dict = {subboundary_name:{'id':idx, 'element':element_bitstring,\
                    'element_type_id':element_type_id}}

                    self.mdfdatabase_subdict[self.mdfFilename]['boundaries'].update(subboundary_dict)
                    self.mdfObject.write_boundary_elements(partition_id, idx, element, conn, indices)

                    self.BoundaryGroups[subboundary_id]['id'] = idx
                    self.SubBoundaries.update({subboundary_name:self.BoundaryGroups[subboundary_id]})

                    logging.debug('{} sub-boundary data saved on the mdf file'.format(subboundary_name))

                for idx,subdomain_id in enumerate(self.DomainGroups):

                    physicalGroup = self.mesh.PhysicalGroups_dict[subdomain_id]
                    gOrder = p_order[subdomain_id]
                    conn = physicalGroup['elements_list']
                    indices = physicalGroup['indices_list']
                    element_type_id = int(physicalGroup['element_type_id'])
                    element, element_bitstring = self._identifyElementType(physicalGroup)
                    physicalGroup['element'] = element

                    subdomain_name = self.DomainGroups[subdomain_id]['group_name']
                    subdomain_dict = {subdomain_name:{'id':idx,'element':element_bitstring,'g_order':gOrder,\
                    'element_type_id':element_type_id}}

                    self.mdfdatabase_subdict[self.mdfFilename]['domains'].update(subdomain_dict)
                    self.mdfObject.write_elements(partition_id, idx, gOrder, element, conn, indices)

                    self.DomainGroups[subdomain_id]['id'] = idx
                    self.SubDomains.update({subdomain_name:self.DomainGroups[subdomain_id]})

                    logging.debug('{} subdomain data saved on the mdf file'.format(subdomain_name))

                self.mdfdatabase_dict['root'].update(self.mdfdatabase_subdict)
                self._saveDatabase(self.mdfDatabase_file)

                logging.debug('Sub-groups access information saved on mdf_database.json')

            def WritingFields(self,partition_id,subdomain_name,fieldDict):

                for idx,field in enumerate(fieldDict):

                    map_type = fieldDict[field]['map_type']
                    field_type = fieldDict[field]['field_type']
                    field_array = fieldDict[field]['data']
                    description = fieldDict[field]['description']

                    self.mdfObject.set_field_attr(idx, field,\
                    description=description, field_type=field_type, map_type=map_type)

                    subdomain_id = self.SubDomains[subdomain_name]['id']
                    g_order = self.SubDomains[subdomain_name]['g_order']
                    element = self.SubDomains[subdomain_name]['element']

                    field_dict = {field:{'id':idx}}

                    self.mdfObject.write_field(idx, partition_id, subdomain_id, g_order, element, field_array)
                    self.mdfdatabase_subdict[self.mdfFilename]['fields'].update(field_dict)

                self.mdfdatabase_dict['root'].update(self.mdfdatabase_subdict)
                self._saveDatabase(self.mdfDatabase_file)

            def ReadingGeometry(self, partition_id, subdomain_name):

                #Read Nodes
                Nodes, nodeIndices = self.mdfObject.read_nodes(partition_id)

                #Read elements
                if subdomain_name in self.mdfDomainsDict:

                    subdomain_id = self.mdfDomainsDict[subdomain_name]['id']
                    element = self.mdfDomainsDict[subdomain_name]['element']
                    gOrder = self.mdfDomainsDict[subdomain_name]['g_order']

                    Elements, elementIndices = self.mdfObject.read_elements(partition_id, subdomain_id,\
                    gOrder, element)

                else:

                    subboundary_id = self.mdfBoundariesDict[subboundary_id]['id']
                    element = self.mdfBoundariesDict[subboundary_id]['element']

                    Elements, elementIndices = self.mdfObject.read_boundary_elements(self, partition_id,\
                    subboundary_id, element)

                logging.debug('Read Nodes and Elements data from the mdf file')

                return Nodes, nodeIndices, Elements, elementIndices

            def ReadingFields(self, partition_id, subdomain_name, field_name):

                subdomain_id = self.mdfDomainsDict[subdomain_name]['id']
                element = self.mdfDomainsDict[subdomain_name]['element']
                gOrder = self.mdfDomainsDict[subdomain_name]['g_order']
                field_id = self.mdfFieldsDict[field_name]['id']

                Field = self.mdfObject.read_field(field_id, partition_id,\
                subdomain_id, gOrder, element)

                logging.debug('Read Field data from mdf file')


                return Field

    elif meshFormat == "independent":

        class MDF(FlyweightMixin):

            def __init__(self, mdf_filename_model, mdf_filename_geom):

                self.mdfFilename_model = mdf_filename_model
                self.mdfFilename_geom = mdf_filename_geom

                self.element = {BaseGeometry.EDGE:0, BaseGeometry.QUAD:0, BaseGeometry.TRI:1}
                #Position of each data storing type
                self.maptype = {MapType.HIERARCHICAL:0, MapType.PER_INTP:4}

            def _identifyElementType(self, p_order, dim, element_type):

                _geom = self.element.get(element_type, None)

                Order = (bin(p_order-1)[2:]).zfill(4)
                Dim = (bin(dim)[2:]).zfill(2)
                Geom =  (bin(_geom)[2:]).zfill(2)
         
                element_bitstring = '0b' + ''.join(Order) + ''.join(Dim) + ''.join(Geom)

                element = bitstring.Bits(bin=element_bitstring)
               
                return element

            def parse_mdf(self):

                if self.mdfFilename_model:
                    self.mdfObject_model = File(self.mdfFilename_model, mode='r')
                    self._res_model_att = self.mdfObject_model.read_model_attr()
                    
                self.mdfObject_geom = File(self.mdfFilename_geom, mode='r')
            
                self._geom_model_att = self.mdfObject_geom.read_model_attr()
                self._geom_partition_att = self.mdfObject_geom.read_partition_attr()

            def query_boundary_names(self, partition_id):
           
                return self._geom_partition_att[partition_id].boundary_names
            
            def read_nodes(self, partition_id):

                Nodes, Indices = self.mdfObject_geom.read_nodes(partition_id)

                return Indices, Nodes

            def read_boundary_elements(self, element_type, partition_id, boundary_id, g_order, dim):

                element = self._identifyElementType(g_order, dim, element_type)
                BoundaryElements = self.mdfObject_geom.read_boundary_elements(partition_id, boundary_id, element)

                return BoundaryElements

            def read_domain_elements(self, element_type, partition_id, subdomain_id, p_order, dim, g_order):

                element = self._identifyElementType(g_order, dim, element_type)
                DomainElements = self.mdfObject_geom.read_elements(partition_id, subdomain_id,
                                                                   p_order, element)

                return DomainElements

            def read_field(self, idx_fieldname, element_type, partition_id, subdomain_id, p_order, dim):

                #For each field in the mdf file, read the data content
                idx = idx_fieldname[0]
                field_data = idx_fieldname[1]
                field_name = idx_fieldname[2]

                field_type = field_data['fieldType']
                map_type = field_data['mapType']

                pos = self.maptype.get(map_type)
                idx+=pos

                fieldData = self.mdfObject_model.set_field_attr(idx, field_name,\
                field_type=field_type, map_type=map_type)

                element = self._identifyElementType(p_order, dim, element_type)

                field = self.mdfObject_model.read_field(idx, partition_id, subdomain_id, p_order,
                               element)

                return field

    else:
        raise AssertionError("Mesh Format is not supported!")

    return MDF

def ascii_factory(mesh_type):
    
    if mesh_type==BaseGeometry.QUAD:
        
        class ASCII(FlyweightMixin):
            
            def __init__(self, ascii_filename):
                
                self.ascii_filename = ascii_filename
                self._domain_order = 8
                self._face_order = 9
                
            def parse_ascii(self):
                
                data = np.loadtxt(self.ascii_filename, delimiter=',')
                nPoints, nFields = data.shape
                
                Points = data[:,:2]
                rho = data[:,2]
                rhou = data[:,3]
                rhov = data[:,4]
                rhoe = data[:,5]
               
                fields = {'Density':rho, 'Momentum x-axis':rhou, 'Momentum y-axis': rhov, 'Energy':rhoe}
                        
                case, ref, element_type_, g_order, p_order = self.parse_filename(self.ascii_filename)
                
                #ASCII format convention
                order_v = self._domain_order + 2*p_order
                order_f = order_v+1
                
                npoints = 4*order_f+order_v**2
                
                nelements = int(nPoints/npoints)
                
                elements = np.split(data,nelements)
                
                return elements, order_v, order_f


            def parse_filename(self,filename):
            
                if os.name == 'nt':
                    sep = '\\'
                elif os.name == 'Linux' or 'posix':
                    sep = '/'
                
                filename = filename.replace('-res', '')
               
                filename = filename.split('.')
                filename = filename[0]
                
                segmented_path = filename.split(sep)
                filename = segmented_path[-1]
                
                segmented_filename = filename.split('_')
                
                case = segmented_filename[0]
                element_type_ = segmented_filename[1]
                ref = segmented_filename[0]
                g_order = int( segmented_filename[3].replace('Q','') )
                p_order = int(segmented_filename[4].replace('p','') )
                
                return case, ref, element_type_, g_order, p_order
                
                
    return ASCII
