#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Container of geometric mesh abstractions
#
# @addtogroup MESH
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy
from collections import defaultdict

class MeshEntityContainer:
    """Multi index list of mesh entities.

    Items can be access sequentially (according to insertion order), by their
    externaly defined ID or by a hash ID. Suitable iterator wrappers are
    defined in the following classes.
    """

    def __init__(self):
        self.data = []  # [MeshEntity] <- sequential
        self.unique = {}  # {Id: MeshEntity}
        self.non_unique = defaultdict(list)  # {HashId: [MeshEntity]}
        self.seqId = numpy.uint64(0)  # Counter

    def size(self):
        return len(self.data)

    def __len__(self):
        return len(self.data)

    def empty(self):
        return bool(not len(self.data))

    def insert(self, e):
        assert e
        assert e.get_type() is not None

        e.set_seq_id(self.seqId)
        self.seqId += 1

        self.data.append(e)

        self.unique[e.get_id()] = self.data[-1]

        self.non_unique[e.get_hash_id()].append(self.data[-1])

    def find_by_id(self, ID):
        """Returns mesh entity by its unique ID."""
        return self.unique.get(ID)

    def find(self, e):
        """Returns list of mesh entities with the same
           hash. Disambiguation must be done elsewhere.

        Notes:
            * What is the point of this method?

        """

        # l   = self.non_unique.get( e.get_hash_id() )
        # ID  = e.get_id()

        # if len(l)==1:
        #     return l[0]
        # else:
        #     for el in l:
        #         if el.get_id() == ID:
        #             return el

        # return None

        return self.non_unique.get(e.get_hash_id())


class SequentialIterator:
    """Iterator for MeshEntityContainer objects following the insertion
       order of the entity in the container."""

    def __init__(self, container):
        self.wrapped = container

    def __iter__(self):

        self.it = iter(self.wrapped.data)

        return self

    def __next__(self):

        try:
            item = next(self.it)
        except StopIteration:
            raise StopIteration

        return item

    def find(self, e):  # This method seems very slow!
        if e in self.wrapped.data:
            return self.wrapped.data[self.wrapped.data.index(e)]
        else:
            return None


class IdentifierIterator:
    """Iterator for MeshEntityContainer objects following an internally
       hashed ordering based on the externally defined entities' IDs.
    """

    def __init__(self, container):
        self.wrapped = container

    def __iter__(self):

        self.it = iter(self.wrapped.unique)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.wrapped.unique[key]

    def __contains__(self, e):

        return e.get_id() in self.wrapped.unique

    def find(self, e):
        return self.wrapped.unique.get(e.get_id())


class HashIdIterator:
    """Iterator for MeshEntityContainer objects following an internally
       hashed ordering based on the entities' hash IDs.

    """

    def __init__(self, container):
        self.wrapped = container

    def __iter__(self):

        self.it = iter(self.wrapped.non_unique)

        return self

    def __next__(self):

        try:
            key = next(self.it)
        except StopIteration:
            raise StopIteration

        return key, self.wrapped.non_unique[key]

    def __contains__(self, e):

        return e.get_hash_id() in self.wrapped.non_unique

    def find(self, e):
        return self.wrapped.non_unique.get(e.get_hash_id())


# -- mecontainer.py -----------------------------------------------------------
