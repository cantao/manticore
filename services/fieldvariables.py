#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Enum with names of variables
#
# @addtogroup SERVICES
# @author cacs (claudio.acsilva@gmail.com)
#

from manticore.services.datatypes import AutoNumberEnum


class FieldVariable(AutoNumberEnum):
    # Conserved variables
    RHO = ()  # Density
    RHOU = ()  # Momentum component 1
    RHOV = ()  # Momentum component 2
    RHOW = ()  # Momentum component 3
    RHOE = ()  # Energy

    # Primitive variables
    U = ()
    V = ()
    W = ()
    P = ()
    T = ()
    MU = ()
    MUT = ()

    # Spatial derivatives of the primitive variables
    DUDX = ()
    DUDY = ()
    DUDZ = ()
    DVDX = ()
    DVDY = ()
    DVDZ = ()
    DWDX = ()
    DWDY = ()
    DWDZ = ()
    DEIDX = ()
    DEIDY = ()
    DEIDZ = ()
    DPDX = ()
    DPDY = ()
    DPDZ = ()
    DTDX = ()
    DTDY = ()
    DTDZ = ()

    # Spatial derivatives of the conservative variables
    DRHODX = ()
    DRHODY = ()
    DRHODZ = ()

    # Divergent of the equations 2, 3, 4 and 5 of the NS viscous flux
    DFV2 = ()
    DFV3 = ()
    DFV4 = ()
    DFV5 = ()

    # Related to time integration
    DELTAT = ()  # Time step: constant or variable
    DELTATAU = ()  # Pseudo time step (for dual time step algorithm):
    # cte or not
    CHAR_LENGTH = ()  # Characteristic length of an element

    # Auxiliars for flux computation
    DELTAF = ()  # Auxiliar variable for BR2 without auxiliar problem
    EPSHAT = ()  # Artificial dissipation
    PSENSOR = ()
	
    # Phase field, Allen-Cahn
    PHASESTATE    = ()
    DPHASESTATEDX = ()
    DPHASESTATEDY = ()

