#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Class for constant objects
#
# @addtogroup SERVICES
# @author cacs (claudio.acsilva@gmail.com)
#


class _const:
    """Class for the definition of constant objects.

    Inspired in:
    http://code.activestate.com/recipes/65207-constants-in-python/?in=user-97991
    """

    class ConstError(TypeError):
        pass

    def __setattr__(self, name, value):
        if name in self.__dict__:
            raise self.ConstError("Can't redefine const \'%s\'!!!" % name)
        self.__dict__[name] = value

    def __delattr__(self, name):
        if name in self.__dict__:
            raise self.ConstError("Can't delete const \'%s\'!!!" % name)


import sys
sys.modules[__name__] = _const()

# -- const.py -----------------------------------------------------------------
