#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Manticore constants
#
# @addtogroup DESCRIPTION
# @author cacs (claudio.acsilva@gmail.com)
#
import manticore.services.const as const


class CTEs:
    const.INF = 1e-15
    const.INF_VAR = 1e-12
    const.INVALID_RESULT = -1


# -- mconsts.py ---------------------------------------------------------------
