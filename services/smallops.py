#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Meta mean
#
# @addtogroup SERVICES
# @author cacs (claudio.acsilva@gmail.com)
#

import numpy


def mean(v):
    """The smart algorithm used here prevents overflow on integer mean
       calculation.

    """
    m = 0.0
    s = len(v)  # Get the number of elements of a numpy.array

    # for ii in range(s):
    #     d = v[ii] - m
    #     m += d / (ii + 1)

    for (ii, x) in zip(range(s), numpy.nditer(v)):
        m += (x - m) / (ii + 1)

    return m
