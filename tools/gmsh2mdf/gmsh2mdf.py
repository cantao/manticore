# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Execute at the directory above manticore:
# $ python3 manticore/tools/gmsh2mdf/gmsh2mdf.py -f <gmsh_file> -p <p-order>
#

# The following two lines are required to import manticore when
# running this program from the system's command line. This is not
# required for running interactively (using import or exec for
# instance).
#
import sys
sys.path.insert(0, '.')
#
#
import logging
import argparse
from os.path import splitext
import numpy as np

from manticore.mdf.file import File
from manticore.mdf.enums import Interpolation
from manticore.geom.standard_geometries import StandardGeometry


class UnsuportedElementInMesh(Exception):
    pass


class MSH:
    #
    # ELM-TYPE of interest:
    #    1: 2-node line
    #    2: 3-node triangle
    #    3: 4-node quadrangle
    #    8: 3-node second order line (2 nodes associated with the vertices
    #       and 1 with the edge)
    #    9: 6-node second order triangle (3 nodes associated with the
    #       vertices and 3 with the edges)
    #   10: 9-node second order quadrangle (4 nodes associated with the
    #       vertices, 4 with the edges and 1 with the face)
    #
    # Check http://gmsh.info/doc/texinfo/gmsh.html#MSH-ASCII-file-format
    # for more.
    VALID_ELEMENTS = {1:  (1, StandardGeometry.EDGE2),
                      2:  (1, StandardGeometry.TRI3),
                      3:  (1, StandardGeometry.QUAD4),
                      8:  (2, StandardGeometry.EDGE3),
                      9:  (2, StandardGeometry.TRI6),
                      10: (2, StandardGeometry.QUAD9),
                      26: (3, StandardGeometry.EDGE4),
                      21: (3, StandardGeometry.TRI10),
                      36: (3, StandardGeometry.QUAD16),
                      27: (4, StandardGeometry.EDGE5),
                      23: (4, StandardGeometry.TRI15),
                      37: (4, StandardGeometry.QUAD25)}

    def __init__(self):
        self.PhysicalNames = None
        self.NumberOfNodes = 0
        self.Nodes = None

    def read_float_array(self, line):
        return np.fromstring(line, dtype=float, sep=' ')

    def read_int_array(self, line):
        return np.fromstring(line, dtype=int, sep=' ')

    def read_physical_domains(self, UnitFile):
        # GMSH docs: NUMBER_OF_NAMES
        UnitFile.readline()  # Skip $PhysicalNames
        n_physical_names = int(UnitFile.readline())

        logging.info('Physical names ({}):'.format(n_physical_names))

        self.PhysicalNames = {}

        for i in range(0, n_physical_names):
            # GMSH docs: PHYSICAL_DIMENSION PHYSICAL_NUMBER "PHYSICAL_NAME"
            # ---------- 0                  1                2
            line = UnitFile.readline().split()

            dim = int(line[0])
            phys_number = int(line[1])
            name = line[2].decode('UTF-8')
            name = name[1:-1]  # erasing quotes from the string

            logging.debug('-> {} ({}D)'.format(name, dim))

            self.PhysicalNames[phys_number] = (name, dim)

        UnitFile.readline()  # Skip $EndPhysicalNames

    def read_nodes(self, UnitFile):
        UnitFile.readline()  # Skip $Nodes

        self.NumberOfNodes = int(UnitFile.readline())

        logging.info('Nodes: {}'.format(self.NumberOfNodes))

        self.Nodes = np.genfromtxt(UnitFile, dtype=float,
                                   max_rows=self.NumberOfNodes)

        UnitFile.readline()  # Skip $EndNodes

    def read_elements(self, UnitFile):
        UnitFile.readline()  # Skip $Elements

        NumberOfElements = int(UnitFile.readline())

        logging.info('{} total elements to read'.format(NumberOfElements))

        # GMSH docs: ELM-NUMBER ELM-TYPE NUMBER-OF-TAGS < TAG > ...
        #            NODE-NUMBER-LIST

        # We need to make some room to organize all the data
        self.Elements = {i: {j: ([], [])
                             for j in MSH.VALID_ELEMENTS.keys()}
                         for i in self.PhysicalNames.keys()}

        for e in range(0, NumberOfElements):
            element = self.read_int_array(UnitFile.readline())

            el_number = element[0]
            el_type = element[1]
            el_phys_domain = element[3]  # TODO: always???
            el_incid = element[element[2]+3:]

            if el_type != 15:
                if el_type not in MSH.VALID_ELEMENTS.keys():
                    raise UnsuportedElementInMesh(
                        'Element code = {}'.format(el_type)
                    )

                self.Elements[el_phys_domain][el_type][0].append(el_number)
                self.Elements[el_phys_domain][el_type][1].append(
                    el_incid.tolist())

    def read(self, inputfile):
        with open(inputfile, 'rb') as UnitFile:
            # Step 0: reading the header

            # GMSH docs: VERSION FILE_TYPE DATA_SIZE
            UnitFile.readline()  # Skip $MeshFormat
            MeshFormat = self.read_float_array(UnitFile.readline())
            UnitFile.readline()  # Skip $EndMeshFormat

            logging.info('GMSH format {}'.format(MeshFormat[0]))

            # Step 1: physical domains info
            self.read_physical_domains(UnitFile)

            # Step 2: reading nodes
            self.read_nodes(UnitFile)

            # Step 3: read elements
            self.read_elements(UnitFile)


# Used an msh object to write a mdf file
class MDF:
    def __init__(self, mshObject, p_order):
        self.mshObject = mshObject
        self.prepare_domain_names()
        self.p_order = p_order

    def prepare_domain_names(self):
        """Prepare the data structures necessary for PartitionData."""
        physical_names = self.mshObject.PhysicalNames

        self.domain_names = {1: [], 2: []}

        for k, v in physical_names.items():
            name, dim = v
            self.domain_names[dim].append(name)

        logging.debug('Extracted domain names = {}'.format(self.domain_names))

    def write(self, mdf_file):
        h = File(mdf_file, 'w')

        h.set_model_attr(
            description='GMSH converted mesh.',
            interpolation=Interpolation.P_INTERPOL)
        h.set_partition_attr(0, name='GMSH',
                             n_nodes=self.mshObject.NumberOfNodes,
                             boundary_names=self.domain_names[1],
                             domain_names=self.domain_names[2])

        physical_names = self.mshObject.PhysicalNames

        dom2d = {k: v for k, v in physical_names.items() if v[1] == 2}

        for i, (k, v) in enumerate(dom2d.items()):
            name, dim = v
            logging.debug('Subdomain {} = {}'.format(i, name))

        # Make the right holes
        logging.info('Creating MDF structure')
        h.create()

        # Nodes
        logging.info('Writing nodes')
        h.write_nodes(2, 0, coords=self.mshObject.Nodes[:, 1:3],
                      indices=self.mshObject.Nodes[:, 0])

        # Elements, both 1D and 2D
        logging.info('Writing elements')
        i2d, i1d = 0, 0

        for k, elst in self.mshObject.Elements.items():
            name, dim = physical_names[k]

            logging.debug(
                'Writing {}D elements from domain {}'.format(dim, name)
            )

            # 2D domains
            for el_type, (idx, conn) in elst.items():
                h_order, geom = MSH.VALID_ELEMENTS[el_type]

                if idx and conn:
                    conn = np.array(conn)

                    # Not so bad to have an "if" here. The structures are
                    # not tipically that big.
                    if dim == 2:
                        h.write_elements(0, i2d, self.p_order, geom, conn, idx)
                        i2d += 1
                    else:
                        h.write_boundary_elements(0, i1d, geom, conn, idx)
                        i1d += 1

        h.close()


if __name__ == '__main__':
    # Command line parsing
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-f', '--gmsh_file', type=str, help='GMSH file to convert')

    parser.add_argument(
        '-p', '--p_order', type=int, help='p-order for all mesh')

    args = parser.parse_args()

    try:
        logging.info("GMSH2MDF file converter")

        msh = MSH()
        msh.read(args.gmsh_file)

        mdf_file = splitext(args.gmsh_file)[0] + '_p{}'.format(args.p_order) + '.mdf'

        mdf = MDF(msh, args.p_order)
        mdf.write(mdf_file)

        logging.info("%s written to disk.\n" % mdf_file)

    except Exception as e:
        logging.critical(e)
        logging.critical("Failed to convert a MSH-file to MDF!")
        raise e


# -- Msh2HDF_5.py -------------------------------------------------------------
