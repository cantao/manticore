# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Script that automatizes the geo file generation
# Afterwards it should be include more options to generetes geometries
# and meshes

import numpy as np

inputfile = open('naca2012.txt', 'r')

A = np.genfromtxt(inputfile)
xc = np.sum(A[:, 0]) / len(A[:, 0])
yc = np.sum(A[:, 1]) / len(A[:, 0])
A = A[:-1, :]
outputfile = open('naca2012.geo', 'w')

outputfile.write('L = 2;\n')
outputfile.write('H = 3;\n')
outputfile.write('lc = 0.005;\n')
outputfile.write('LC = 25*lc;\n')
outputfile.write('Mesh.Algorithm=5;\n')
# Reads the points data from the txt file (by the moment, the most convenient
# way)
for i in range(0, len(A[:, 1])):

    [x, y, z] = A[i, :]
    line = 'Point(%i) = {%f,%f,%f,lc};\n' % (i, x, y, z)
    outputfile.write(line)

cont = 0

# Option to perform a splne with almost the all the points
line = 'Spline(%i) = {%i:%i};\n' % (len(A[:, 1]), 0, len(A[:, 1]) - 2)
outputfile.write(line)
cont = cont + 1

line = 'Spline(%i) = {%i,%i,%i};\n' % (len(A[:, 1]) + cont, len(A[:, 1]) - 2,
                                       len(A[:, 1]) - 1, 0)
outputfile.write(line)

# Creates the boundary around the airfoil, in the present case, a simple
# rectangle
outputfile.write('Point(%i) = {-H,-L,0,LC};\n' % (len(A[:, 1]) + cont))
outputfile.write('Point(%i) = {-H,L,0,LC};\n' % (len(A[:, 1]) + cont + 1))
outputfile.write('Point(%i) = {H,L,0,LC};\n' % (len(A[:, 1]) + cont + 2))
outputfile.write('Point(%i) = {H,-L,0,LC};\n' % (len(A[:, 1]) + cont + 3))

outputfile.write('Line(%i) = {%i,%i};\n' %
                 (len(A[:, 1]) + cont + 4, len(A[:, 1]) + cont,
                  len(A[:, 1]) + cont + 1))
outputfile.write('Line(%i) = {%i,%i};\n' %
                 (len(A[:, 1]) + cont + 5, len(A[:, 1]) + cont + 1,
                  len(A[:, 1]) + cont + 2))
outputfile.write('Line(%i) = {%i,%i};\n' %
                 (len(A[:, 1]) + cont + 6, len(A[:, 1]) + cont + 2,
                  len(A[:, 1]) + cont + 3))
outputfile.write('Line(%i) = {%i,%i};\n' %
                 (len(A[:, 1]) + cont + 7, len(A[:, 1]) + cont + 3,
                  len(A[:, 1]) + cont))

line = 'Line Loop(%i) = {%i:%i};\n' % (len(A[:, 1]) + cont + 8, len(A[:, 1]),
                                       len(A[:, 1]) + cont)
outputfile.write(line)

line = 'Line Loop(%i) = {%i:%i};\n' % (len(A[:, 1]) + cont + 9,
                                       len(A[:, 1]) + cont + 4,
                                       len(A[:, 1]) + cont + 7)
outputfile.write(line)

line = 'Plane Surface(%i) = {%i,%i};\n' % (len(A[:, 1]) + cont + 10,
                                           len(A[:, 1]) + cont + 8,
                                           len(A[:, 1]) + cont + 9)
outputfile.write(line)

line = 'Field[1] = Attractor;\n'
outputfile.write(line)

# Nodes close to the trailing edge
line = 'Field[1].NodesList = {%i,%i,%i};\n' % (0, len(A[:, 1]) - 1,
                                               len(A[:, 1]))
outputfile.write(line)
line = 'Field[1].NNodesByEdge = 100;\n'
outputfile.write(line)

line = 'Field[2] = Threshold;\n'
outputfile.write(line)
line = 'Field[2].IField = 1;\n'
outputfile.write(line)
line = 'Field[2].LcMin = lc/2;\n'
outputfile.write(line)
line = 'Field[2].LcMax = lc;\n'
outputfile.write(line)
line = 'Field[2].DistMin = lc;\n'
outputfile.write(line)
line = 'Field[2].DistMax = 10*lc;\n'
outputfile.write(line)

outputfile.write('Physical Line("farfield") = {50, 49, 52, 51};\n')

outputfile.write('Physical Line("wing") = {44};\n')

outputfile.write('Physical Surface("air") = {55};\n')

outputfile.close()
