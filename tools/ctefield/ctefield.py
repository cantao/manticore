#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#

import logging
import argparse
import numpy as np
from collections import OrderedDict
from manticore.mdf.file import File
from manticore.mdf.enums import FieldType, MapType
from manticore.geom.standard_geometries import StandardGeometryGenerator


"""Constant Field Generator for Manticore.

Given a MDF mesh file, generates an accompaining initial condition file with
constant per element values for the five conserved variables.

Example:
    You must give `ctefield.py` a job name. It will look for a mesh file named
    **job name.mdf** and it will generate a corresponding **job
    name-init.mdf**.

    % python ctefield.py plate
"""

fields = {'Density': 'CTE PER ELEMENT Density',
          'MomentumX': 'CTE PER ELEMENT Momentum X',
          'MomentumY': 'CTE PER ELEMENT Momentum Y',
          'MomentumZ': 'CTE PER ELEMENT Momentum Z',
          'SpecificTotalEnergy': 'CTE PER ELEMENT Energy'}

values = OrderedDict()


def mdf_field_skeleton(msh, fds):
    logging.info('Acessing original mdf mesh file...')
    msh.create()

    logging.info('Setting model attributes of fields file...')

    fds.set_model_attr(
        description='Constant Fields for Mesh ' + msh.model_data.description,
        n_subdomains=msh.model_data.n_subdomains,
        n_parts=msh.model_data.n_parts,
        n_fields=msh.model_data.n_fields,
        writing_conn=True,
        last_geom_file=mshfile
    )

    logging.info('Setting partition attributes for fields file...')

    for i, p in msh.partition_data.items():
        fds.set_partition_attr(i, p.name,
                               description=p.description,
                               n_nodes=p.n_nodes,
                               boundary_names=p.boundary_names)

    logging.info('Setting fields attributes...')

    for i, (name, desc) in enumerate(fields.items()):
        fds.set_field_attr(i, name, description=desc,
                           field_type=FieldType.SCALAR_FIELD,
                           map_type=MapType.PER_ELEM)

    logging.info('Creating field file...')

    fds.create()


def write_fields(msh, fds):
    logging.info('Writing fields...')

    for p in range(msh.model_data.n_parts):
        for s in range(msh.model_data.n_subdomains):
            shapes = msh.query_element_shapes(p, s)

            for sh in shapes:
                numels = msh.query_number_elements(p, s, sh)
                geometry = StandardGeometryGenerator.gen_from_string(sh)

                for order, nels in numels.items():
                    logging.debug(
                        'sh: %s, ord: %d, nels: %d' % (sh, order, nels)
                    )

                    for f, (name, value) in enumerate(values.items()):
                        buffer = np.full(nels, value)
                        fds.write_field(f, p, s, order, geometry, buffer)


if __name__ == '__main__':
    # Command line parsing
    parser = argparse.ArgumentParser()

    parser.add_argument('jobname', type=str, help='Job name.')

    args = parser.parse_args()

    logging.info('Constant MDF field writer')

    # Asking for the necessary fields.
    for i in fields.keys():
        values[i] = float(input('%s = ' % i))

    # Lets open the files.
    mshfile = args.jobname + '.mdf'
    fdsfile = args.jobname + '-init.mdf'

    try:
        with File(mshfile, 'r') as msh, File(fdsfile, 'w-') as fds:
            mdf_field_skeleton(msh, fds)
            write_fields(msh, fds)
    except Exception as e:
        logging.debug(e)

# -- ctefield.py -------------------------------------------------------------
