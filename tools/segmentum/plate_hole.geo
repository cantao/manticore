//+
lc = DefineNumber[ 3.0, Name "Parameters/corner" ];
//+
Point(1) = {0, 0, 0, lc};
//+
Point(2) = {10, 0, 0, lc};
//+
Point(3) = {10, 10, 0, lc};
//+
Point(4) = {0, 10, 0, lc};
//+
Line(1) = {4, 1};
//+
Line(2) = {1, 2};
//+
Line(3) = {2, 3};
//+
Line(4) = {3, 4};
//+
lc2 = DefineNumber[ 1.0, Name "Parameters/round" ];
//+
Point(5) = {5.5, 5, 0, lc2};
//+
Point(6) = {5, 5.5, 0, lc2};
//+
Point(7) = {4.5, 5, 0, lc2};
//+
Point(8) = {5, 4.5, 0, lc2};
//+
Point(9) = {5, 5, 0, lc2};
//+
Circle(5) = {6, 9, 5};
//+
Circle(6) = {5, 9, 8};
//+
Circle(7) = {8, 9, 7};
//+
Circle(8) = {7, 9, 6};
//+
Line Loop(9) = {1, 2, 3, 4};
//+
Line Loop(10) = {8, 5, 6, 7};
//+
Physical Line("top") = {4};
//+
Physical Line("right") = {3};
//+
Physical Line("left_bottom") = {1, 2};
//+
Physical Line("wall") = {8, 5, 6, 7};
//+
Plane Surface(11) = {9, 10};
//+
Physical Surface("plate") = {11};
