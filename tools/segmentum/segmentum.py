#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Segmentum mesh partitioner, based on Metis
#
# @addtogroup tools
# @author Cantão! (rfcantao@gmail.com)
#

import logging
import argparse
import pymetis
from manticore.mesh.mdf_mesh_parser import MdfMeshParser
from manticore.mesh.umesh import (
    double_link_region_to_edge, double_link_region_to_region
    )


class NumberOfPartitionsException(Exception):
    def __init__(self, partitions):
        self.message = f'Number of partitions [{partitions}] not allowed!'

    def __str__(self):
        return self.message


if __name__ == '__main__':
    # Command line parsing
    parser = argparse.ArgumentParser()

    parser.add_argument('mdffile', type=str, help='MDF file to partition')
    parser.add_argument('-p', '--partitions', type=int,
                        help='Number of partitions', required=True)

    args = parser.parse_args()

    try:
        if args.partitions < 2:
            raise NumberOfPartitionsException(args.partitions)

        parser = MdfMeshParser(args.mdffile)

        M = parser.parse()

        double_link_region_to_edge(M)
        double_link_region_to_region(M)

        print(M)

    except Exception as e:
        logging.critical(type(e))
        logging.critical(e)


# -- segmentum.py -------------------------------------------------------------
