#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Standard geometries abstraction. Useful for both MDF and MESH.
#
# @addtogroup GEOM
# @author Cantão! (rfcantao@gmail.com)
#

import unittest
from bitstring import BitArray

from manticore.geom import BaseGeometry
from manticore.geom import StandardGeometry


class TestStandardGeometries(unittest.TestCase):
    def setUp(self):
        # A dictionary with all possible combinations of elements
        self.elements = {}

        for g in BaseGeometry:
            self.elements[g] = []

            # All possible geometric orders
            for i in range(0, 16):
                gid = StandardGeometry.generate(g, order=i)
                self.elements[g].append(gid)

    # GENERATE tests
    def __generate(self, base_geometry, expected):
        self.assertEqual(len(expected), len(self.elements[base_geometry]))

        for e in zip(expected, self.elements[base_geometry]):
            with self.subTest():
                self.assertEqual(e[0], e[1].uint)
                self.assertEqual(BitArray(uint=e[0], length=8), e[1])

    def test_quad_generate(self):
        expected = [
            8, 24, 40, 56, 72, 88, 104, 120, 136, 152, 168, 184, 200, 216, 232,
            248
        ]
        self.__generate(BaseGeometry.QUAD, expected)

    def test_tri_generate(self):
        expected = [
            9, 25, 41, 57, 73, 89, 105, 121, 137, 153, 169, 185, 201, 217, 233,
            249
        ]
        self.__generate(BaseGeometry.TRI, expected)

    def test_edge_generate(self):
        expected = [
            4, 20, 36, 52, 68, 84, 100, 116, 132, 148, 164, 180, 196, 212, 228,
            244
        ]
        self.__generate(BaseGeometry.EDGE, expected)

    def test_vertex_generate(self):
        expected = [
            0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224,
            240
        ]
        self.__generate(BaseGeometry.VERTEX, expected)

    # DIMENSION tests
    def __dimension(self, base_geometry):
        expected = StandardGeometry.GEOM_TO_DIM[base_geometry]

        for e in self.elements[base_geometry]:
            with self.subTest():
                order = StandardGeometry.dim(e)
                self.assertEqual(order, expected)

    def test_quad_dimension(self):
        self.__dimension(BaseGeometry.QUAD)

    def test_tri_dimension(self):
        self.__dimension(BaseGeometry.TRI)

    def test_edge_dimension(self):
        self.__dimension(BaseGeometry.EDGE)

    def test_vertex_dimension(self):
        self.__dimension(BaseGeometry.VERTEX)

    # GEOMETRY tests
    def __geometry(self, base_geometry):
        for e in self.elements[base_geometry]:
            with self.subTest():
                geom = StandardGeometry.geom(e)
                self.assertEqual(geom, base_geometry)

    def test_quad_geom(self):
        self.__geometry(BaseGeometry.QUAD)

    def test_tri_geom(self):
        self.__geometry(BaseGeometry.TRI)

    def test_edge_geom(self):
        self.__geometry(BaseGeometry.EDGE)

    def test_vertex_geom(self):
        self.__geometry(BaseGeometry.VERTEX)

    # ORDER tests
    def __order(self, base_geometry):
        for i, e in enumerate(self.elements[base_geometry]):
            with self.subTest():
                self.assertEqual(StandardGeometry.order(e), i)

    def test_quad_order(self):
        self.__order(BaseGeometry.QUAD)

    def test_tri_order(self):
        self.__order(BaseGeometry.TRI)

    def test_edge_order(self):
        self.__order(BaseGeometry.EDGE)

    def test_vertex_order(self):
        self.__order(BaseGeometry.VERTEX)


if __name__ == '__main__':
    unittest.main(verbosity=2)

# -- test_standard_geometries.py ----------------------------------------------
