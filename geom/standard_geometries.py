#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# Standard geometries abstraction. Useful for both MDF and MESH.
#
# @addtogroup GEOM
# @author Cantão! (rfcantao@gmail.com)
#

from enum import Enum
import bitstring


class BaseGeometry(Enum):
    QUAD = 0,
    TRI = 1,
    EDGE = 2,
    VERTEX = 3


class StandardGeometryGenerator:
    """Geometric entities standardization for all modules.

    Eight bits (a byte) are used in this representation:

    +---+---+---+---+---+---+---+---+
    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
    +---+---+---+---+---+---+---+---+

    Description of each bitfield:

    - Element type

        +---+---+
        | 1 | 0 |
        +---+---+

    - Spatial dimension

        +---+---+
        | 3 | 2 |
        +---+---+

    - Geometry order

        +---+---+---+---+
        | 7 | 6 | 5 | 4 |
        +---+---+---+---+

    Note:
        In the geometry order field we are considering the *maximal* order of
        the element. So, transition elements (for instance, curved second order
        on a face e straight side on the order) are considered second order.

        Order starts from *one* (standard straigth sided elements).

    Todo:
        - Discover how to represent transitioning elements!
        - Check NUMBER_OF_VERTICES! Extend to higher orders!

    Example:
        We are going to represent a second order triangles and quadrilaterals.

        >>> trias = gen(BaseGeometry.TRIA, order=2)
        >>> quads = gen(BaseGeometry.QUADS, order=2)
    """

    # Geometry -> dimension association.
    GEOM_TO_DIM = {BaseGeometry.QUAD: 2, BaseGeometry.TRI: 2,
                   BaseGeometry.EDGE: 1, BaseGeometry.VERTEX: 0}

    # Geometry -> binary type association
    #
    # DIM = 2       DIM = 1       DIM = 0
    # +----------+  +------+---+  +--------+---+
    # | QUAD | 0 |  | EDGE | 0 |  | VERTEX | 0 |
    # | TRI  | 1 |  +------+---+  +--------+---+
    # +------+---+
    GEOM_TO_TYPE = {BaseGeometry.QUAD: 0, BaseGeometry.TRI: 1,
                    BaseGeometry.EDGE: 0, BaseGeometry.VERTEX: 0}

    # Associates dimension + type with geometry
    TYPE_TO_GEOM = {0: {0: BaseGeometry.VERTEX},
                    1: {0: BaseGeometry.EDGE},
                    2: {0: BaseGeometry.QUAD, 1: BaseGeometry.TRI}}

    # Number of nodes, based on the geometry order (only up until order 4)
    NUMBER_OF_NODES = {BaseGeometry.QUAD: [4,  9, 16, 25],
                       BaseGeometry.TRI: [3,  6,  10, 15],
                       BaseGeometry.EDGE: [2,  3,  4,  5],
                       BaseGeometry.VERTEX: [1,  1,  1,  1]}

    # Number of edges, per geometry
    NUMBER_OF_EDGES = {BaseGeometry.QUAD: 4, BaseGeometry.TRI: 3,
                       BaseGeometry.EDGE: 1, BaseGeometry.VERTEX: 0}

    # Abbreviated name
    ABBREV_NAME = {BaseGeometry.QUAD: 'quad', BaseGeometry.TRI: 'tri',
                   BaseGeometry.EDGE: 'edge', BaseGeometry.VERTEX: 'vrt'}

    NAME_ABBREV = {'quad': BaseGeometry.QUAD, 'tri': BaseGeometry.TRI,
                   'edge': BaseGeometry.EDGE, 'vrt': BaseGeometry.VERTEX}

    # Bitmasks
    GEO_ORDER_BITMASK = bitstring.Bits(bin='0b11110000')
    DIMENSION_BITMASK = bitstring.Bits(bin='0b00001100')
    TYPE_BITMASK = bitstring.Bits(bin='0b00000011')

    def gen(base_geometry, order):
        """Generate a binary representation of the object.

        The main objective here is to save memory. Eventually an object must
        know some information about itself. This is accomplished through a
        single byte that stores all needed information.

        Args:
            base_geometry (:class:`BaseGeometry`): the initial geometry.

            order (int): order of the geometry.
        """
        # As per http://pythonhosted.org/bitstring/walkthrough.html
        fmt = 'uint:4=order, uint:2=dim, uint:2=type'
        data = {'order': order,
                'dim': StandardGeometryGenerator.GEOM_TO_DIM[base_geometry],
                'type': StandardGeometryGenerator.GEOM_TO_TYPE[base_geometry]}

        return bitstring.Bits(bitstring.pack(fmt, **data))

    def gen_from_string(txt):
        """Create a StandardGeometry object from its textual description."""
        txt = txt.lower()

        for g in StandardGeometryGenerator.ABBREV_NAME.values():
            if g in txt:
                geom = StandardGeometryGenerator.NAME_ABBREV[txt[:len(g)]]
                n_vert = int(txt[len(g):])
                order = StandardGeometryGenerator.NUMBER_OF_NODES[geom].index(n_vert)

                return StandardGeometryGenerator.gen(geom, order)

        return None


class StandardGeometry:
    # First order elements
    VERTEX1 = StandardGeometryGenerator.gen(BaseGeometry.VERTEX, 0)
    EDGE2 = StandardGeometryGenerator.gen(BaseGeometry.EDGE, 0)
    TRI3 = StandardGeometryGenerator.gen(BaseGeometry.TRI, 0)
    QUAD4 = StandardGeometryGenerator.gen(BaseGeometry.QUAD, 0)

    # Second order elements
    VERTEX2 = StandardGeometryGenerator.gen(BaseGeometry.VERTEX, 1)
    EDGE3 = StandardGeometryGenerator.gen(BaseGeometry.EDGE, 1)
    TRI6 = StandardGeometryGenerator.gen(BaseGeometry.TRI, 1)
    QUAD9 = StandardGeometryGenerator.gen(BaseGeometry.QUAD, 1)

    # Third order elements
    VERTEX3 = StandardGeometryGenerator.gen(BaseGeometry.VERTEX, 2)
    EDGE4 = StandardGeometryGenerator.gen(BaseGeometry.EDGE, 2)
    TRI10 = StandardGeometryGenerator.gen(BaseGeometry.TRI, 2)
    QUAD16 = StandardGeometryGenerator.gen(BaseGeometry.QUAD, 2)

    # Fourth order elements
    VERTEX4 = StandardGeometryGenerator.gen(BaseGeometry.VERTEX, 3)
    EDGE5 = StandardGeometryGenerator.gen(BaseGeometry.EDGE, 3)
    TRI15 = StandardGeometryGenerator.gen(BaseGeometry.TRI, 3)
    QUAD25 = StandardGeometryGenerator.gen(BaseGeometry.QUAD, 3)

    def __str__(self):
        members = [getattr(self, attr) for attr in dir(self)
                   if not callable(getattr(self, attr)) and
                   not attr.startswith('__')]
        return '\n'.join([str(m) for m in members])


# The StandardGeometry class is not an Enum, so it is not iterable. In order to
# simplify things, we build a list with its elements. Not the best solution,
# but it serves to solve a corner case anyway.
def std_geometry_as_list():
    sg = StandardGeometry()

    return [getattr(sg, attr) for attr in dir(sg)
            if not callable(getattr(sg, attr)) and not attr.startswith('__')]


# Mask bits 2 and 3, shifts 2 times.
def dim(element):
    """Returns the spatial dimension. """
    return ((element &
             StandardGeometryGenerator.DIMENSION_BITMASK) >> 2).uint


# Mask bits 0 and 1.
def geom(element):
    """
    Returns the base geometry of the element (QUAD, TRI, EDGE or VERTEX).
    """
    g = (element & StandardGeometryGenerator.TYPE_BITMASK).uint
    d = dim(element)

    return StandardGeometryGenerator.TYPE_TO_GEOM[d][g]


# Mask bits 4 to 7, shifts 4 times.
def order(element):
    """Geometrical nodal order, minus 1."""
    return ((element &
             StandardGeometryGenerator.GEO_ORDER_BITMASK) >> 4).uint


def unpack(element):
    """
    Returns:
        Tuple with spatial dimension, geometry and order.
    """
    return (dim(element), geom(element), order(element))


def number_of_nodes(element):
    """ Return the number of nodes."""
    d, g, o = unpack(element)
    return StandardGeometryGenerator.NUMBER_OF_NODES[g][o]


def number_of_edges(element):
    """Return the number of edges."""
    g = geom(element)
    return StandardGeometryGenerator.NUMBER_OF_EDGES[g]


def geometry_name(element):
    """ Return the common geometry name, as a string."""
    g = geom(element)
    n = number_of_nodes(element)
    return '{}{}'.format(StandardGeometryGenerator.ABBREV_NAME[g], n)


def str(element):
    d, g, o = unpack(element)
    return '{} (order: {})'.format(g, o)

# -- standard_geometries.py ---------------------------------------------------
