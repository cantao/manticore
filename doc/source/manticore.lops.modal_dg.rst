manticore\.lops\.modal\_dg package
==================================

Submodules
----------

manticore\.lops\.modal\_dg\.class\_instances module
---------------------------------------------------

.. automodule:: manticore.lops.modal_dg.class_instances
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.computemesh module
----------------------------------------------

.. automodule:: manticore.lops.modal_dg.computemesh
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.create\_computemesh module
------------------------------------------------------

.. automodule:: manticore.lops.modal_dg.create_computemesh
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.dgtypes module
------------------------------------------

.. automodule:: manticore.lops.modal_dg.dgtypes
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.engine module
-----------------------------------------

.. automodule:: manticore.lops.modal_dg.engine
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.entity module
-----------------------------------------

.. automodule:: manticore.lops.modal_dg.entity
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.entityops module
--------------------------------------------

.. automodule:: manticore.lops.modal_dg.entityops
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.expankeyfct module
----------------------------------------------

.. automodule:: manticore.lops.modal_dg.expankeyfct
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.expansion module
--------------------------------------------

.. automodule:: manticore.lops.modal_dg.expansion
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.expantools module
---------------------------------------------

.. automodule:: manticore.lops.modal_dg.expantools
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.faceprinfunc module
-----------------------------------------------

.. automodule:: manticore.lops.modal_dg.faceprinfunc
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.fields module
-----------------------------------------

.. automodule:: manticore.lops.modal_dg.fields
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.gauss module
----------------------------------------

.. automodule:: manticore.lops.modal_dg.gauss
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.gauss\_strm module
----------------------------------------------

.. automodule:: manticore.lops.modal_dg.gauss_strm
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.geomfactors module
----------------------------------------------

.. automodule:: manticore.lops.modal_dg.geomfactors
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.jacobi module
-----------------------------------------

.. automodule:: manticore.lops.modal_dg.jacobi
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.phyops1d module
-------------------------------------------

.. automodule:: manticore.lops.modal_dg.phyops1d
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.phyops2d module
-------------------------------------------

.. automodule:: manticore.lops.modal_dg.phyops2d
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.prinfunc module
-------------------------------------------

.. automodule:: manticore.lops.modal_dg.prinfunc
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.stdmixins1d module
----------------------------------------------

.. automodule:: manticore.lops.modal_dg.stdmixins1d
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.stdmixins2d module
----------------------------------------------

.. automodule:: manticore.lops.modal_dg.stdmixins2d
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.stdops1d module
-------------------------------------------

.. automodule:: manticore.lops.modal_dg.stdops1d
    :members:
    :undoc-members:
    :show-inheritance:

manticore\.lops\.modal\_dg\.stdops2d module
-------------------------------------------

.. automodule:: manticore.lops.modal_dg.stdops2d
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: manticore.lops.modal_dg
    :members:
    :undoc-members:
    :show-inheritance:
