.. Manticore documentation master file, created by
   sphinx-quickstart on Wed Sep 21 15:30:25 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Manticore's documentation!
##########################

Manticore is a high-order Discontinuous Galerking solver.

MDF: Mesh Data Format
*********************

The MDF is the standard for storing partitioned finite element meshes of any
spatial dimension. A MDF file can hold meshes and fields, offering the
following capabilities:

* Each mesh can be composed of several subdomains;
* Fields can be scalar, vectorial or tensorial;
* Values can be stored per node, integration point or element;
* In the case of high order solutions, polynomial coefficients can also be stored.

Indices and tables
##################

.. toctree::
   :maxdepth: 2

   modules

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

