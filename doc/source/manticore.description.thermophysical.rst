manticore.description.thermophysical module
===========================================

.. automodule:: manticore.description.thermophysical
    :members:
    :undoc-members:
    :show-inheritance:
