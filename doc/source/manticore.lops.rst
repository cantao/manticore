manticore\.lops package
=======================

Subpackages
-----------

.. toctree::

    manticore.lops.modal_dg
    manticore.lops.nodal_cg

Module contents
---------------

.. automodule:: manticore.lops
    :members:
    :undoc-members:
    :show-inheritance:
