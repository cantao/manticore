manticore.description package
=============================

Submodules
----------

.. toctree::

   manticore.description.elsets
   manticore.description.equations
   manticore.description.materials
   manticore.description.mconsts
   manticore.description.model
   manticore.description.mtypes
   manticore.description.reference
   manticore.description.thermophysical
   manticore.description.timint

Module contents
---------------

.. automodule:: manticore.description
    :members:
    :undoc-members:
    :show-inheritance:
