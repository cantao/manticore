#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
# __init__ for Manticore
# @author Cantão! (rfcantao@gmail.com)
#

"""Manticore High-Order DG solver.

    .. moduleauthor:: Renato Cantão <rfcantao@gmail.com>,
    Claudio Silva <claudio.acsilva@gmail.com>
"""

__title__ = 'manticore'
__version__ = '0.1.1'
__author__ = 'R. F. Cantão, C. Silva'
__license__ = ''

import logging
import logging.config

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '[%(asctime)s: %(levelname)s::%(name)s %(funcName)s at '
            '%(filename)s:%(lineno)d] %(message)s ',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    }
}

logging.config.dictConfig(LOGGING)

# -- __init__.py --------------------------------------------------------------
