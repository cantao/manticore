import numpy as np
import sys, os
sys.path.insert(0, '.')
import manticore
from manticore.mesh.mesh_utils import MSH, mdf_factory
from manticore.visualization.PlotUtils import factory_plotter
from matplotlib import pyplot as plt
from manticore.mdf.enums import MapType, FieldType
from manticore.geom.standard_geometries import BaseGeometry

def parse_filename(mdf_filename):
    
    if os.name == 'nt':
        sep = '\\'
    elif os.name == 'Linux' or 'posix':
        sep = '/'

    mdf_filename = mdf_filename.split('.')
    mdf_filename = mdf_filename[0]
    
    segmented_path = mdf_filename.split(sep)
    filename = segmented_path[-1]
    
    segmented_filename = filename.split('_')
    
    case = segmented_filename[0]
    element_type_ = segmented_filename[1]
    ref = segmented_filename[2]
    g_order = int( segmented_filename[3].replace('Q','') )
    p_order = int(segmented_filename[4].replace('p','') )
    
    return case, ref, element_type_, g_order, p_order

def main():

    txt_filename = sys.argv[1]
    mdf_filename_geom = sys.argv[2]
    mdf_filename_model = None
    
    Element_Types = {'quad':BaseGeometry.QUAD, 'tri':BaseGeometry.TRI}
    
    data = np.loadtxt(txt_filename, delimiter=',')

    Points = data[:,:2]
    
    rho = data[:,2]
    rhou = data[:,3]
    rhov = data[:,4]
    rhoe = data[:,5]
  
    case, ref, element_type_, g_order, p_order = parse_filename( mdf_filename_geom )
    element_type_edge = BaseGeometry.EDGE 
    
    element_type = Element_Types.get(element_type_)
    partitioning = False
    dim = 2
    dim_edge=1
    partition_id = 0
    subdomain_id = 0
    
    MDF = mdf_factory('independent')
    mdf = MDF.get_instance(mdf_filename_model, mdf_filename_geom)
    mdf.parse_mdf()

    #Choose a subdomain name
    subdomain_name = 'FLUID'
    Indices, Nodes = mdf.read_nodes(partition_id)
    Elements, Indices = mdf.read_domain_elements(element_type, partition_id, subdomain_id, p_order, dim, g_order)
    
    boundary_names = mdf.query_boundary_names(partition_id)
    Boundaries = dict()
    
    for boundary_id, boundary_name in enumerate(boundary_names):
        BoundaryElements, BoundaryIndices  = mdf.read_boundary_elements(element_type_edge, partition_id, boundary_id, g_order, dim_edge)
        Boundaries[boundary_id] = BoundaryElements
    
    Plotter = factory_plotter('scatter_quad')
    plotter = Plotter(Nodes, Elements, Boundaries, Points, 'solid')
    fields = {'Density':rho, 'Momentum x-axis':rhou, 'Momentum y-axis':rhov, 'Energy':rhoe}
    
    
    for field_name, field  in fields.items():
        
        image_namefile = txt_filename.replace('.txt', '_'+field_name+'.png')
        filename = txt_filename.replace('res.txt', field_name+'.png')
        plotter.plot(field, field_name, filename)
        
   
if __name__=='__main__':

    main()
