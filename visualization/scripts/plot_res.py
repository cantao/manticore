import numpy as np
import sys, os
sys.path.insert(0, '.')
import manticore
from manticore.mesh.mesh_utils import MSH, mdf_factory,  ascii_factory
from manticore.visualization.VTKutils import conversor_manager
from manticore.visualization.PlotUtils import factory_plotter
from matplotlib import pyplot as plt
from manticore.mdf.enums import MapType, FieldType
from manticore.geom.standard_geometries import StandardGeometry, BaseGeometry
import matplotlib.pyplot as plt

def parse_filename(mdf_filename):
    
    if os.name == 'nt':
        sep = '\\'
    elif os.name == 'Linux' or 'posix':
        sep = '/'

    mdf_filename = mdf_filename.split('.')
    mdf_filename = mdf_filename[0]
    
    segmented_path = mdf_filename.split(sep)
    filename = segmented_path[-1]
    
    segmented_filename = filename.split('_')
    
    case = segmented_filename[0]
    element_type_ = segmented_filename[1]
    ref = segmented_filename[0]
    g_order = int( segmented_filename[3].replace('Q','') )
    p_order = int(segmented_filename[4].replace('p','') )
    
    return case, ref, element_type_, g_order, p_order

def main():

    txt_filename = sys.argv[1]
    mdf_filename_geom = sys.argv[2]
    mdf_filename_model = None
    
    Element_Types = {'quad':BaseGeometry.QUAD, 'tri':BaseGeometry.TRI}

    case, ref, element_type_, g_order, p_order = parse_filename(mdf_filename_geom)
    
    element_type = Element_Types.get(element_type_)
    element_type_edge = BaseGeometry.EDGE

    dim = 2
    dim_edge=1
    partition_id = 0
    subdomain_id = 0
    
    ASCII = ascii_factory(element_type)
    ascii_ = ASCII.get_instance(txt_filename)
    elements, order_v, order_f = ascii_.parse_ascii()
    
   
    partitioning = False
    MDF = mdf_factory('independent')
    mdf = MDF.get_instance(mdf_filename_model, mdf_filename_geom)
    mdf.parse_mdf()

    #Choose a subdomain name
    subdomain_name = 'FLUID'
    Indices, Nodes = mdf.read_nodes(partition_id)
    Elements, Indices = mdf.read_domain_elements(element_type, partition_id, subdomain_id, p_order, dim, g_order)
    
    boundary_names = mdf.query_boundary_names(partition_id)
    Boundaries = dict()
    
    Plotter = factory_plotter('quad')
    
    fields = ['Density', 'Momentum x-axis', 'Momentum y-axis','Energy']
    for field_name  in fields:
        
        plotter = Plotter(elements,field_name, order_v, order_f, 'solid')
        image_namefile = txt_filename.replace('.txt', '_'+field_name+'.png')
        filename = txt_filename.replace('res.txt', field_name+'.png')
        plotter.plot(filename)
   
if __name__=='__main__':

    main()
