## Plotting script

Script for plotting geometry and fields using global smoothing by means a triangulation

* Plot factory options:
    * solid
    * flat
    * contour
    
For running the plotting script type:

```
	python manticore/visualization/scripts/plot_res.py <results.txt> <mesh.mdf> 
```
