from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
import matplotlib.tri as tri
from scipy.spatial import Delaunay
from manticore.services.datatypes import FlyweightMixin
from manticore.services.datatypes import bidict

from shapely.geometry import MultiPoint, Polygon, LinearRing, Point

import matplotlib.pyplot as plt
import numpy as np
import sys
import logging
from numba import jit
from collections import defaultdict


def factory_plotter(element_type):

    if element_type == 'quad':
        return PlotterQuad
    if element_type =='tri':
        return PlotterTri
    if element_type == "scatter_quad":
        return PlotterScatterQuad
    if element_type == "scatter_tri":
        return PlotterScatteredTri

def refiner(trimesh, field):
   
    refiner = tri.UniformTriRefiner(trimesh)
    triang_ref, Field_ref = refiner.refine_field(field)

    return triang_ref, Field_ref

#Methods for plottling quadrilateral 2D meshes using Matplotlib
class PlotterQuad(FlyweightMixin):

    def __init__(self,elements_fields, field_name, order_v, order_f, plot_type):

        self.elements_fields = elements_fields
        self.elements_indices = bidict( { i:element.tostring() for i,element in enumerate(self.elements_fields) } )
        self.number_of_elements = len(elements_fields)
        self.order_v = order_v
        self.order_f = order_f
        self.field_name = field_name
        self.field_IDs = {'Density':0, 'Momentum x-axis':1, 'Momentum y-axis': 2, 'Energy':3}
        self.field_id = self.field_IDs.get(field_name)
        self.plot_type = plot_type
        self.elements_fields_mono = np.concatenate((self.elements_fields), axis=0)
        self.points = self.elements_fields_mono[:,:2]
        self.field = self.elements_fields_mono[:,2:][:,self.field_id]
        self.points_indices = bidict( { i:tuple(item) for i,item in enumerate(self.points) } )
        self.points_fields = bidict( { tuple(item):self.field[i] for i,item in enumerate(self.points) } )
        self.points_connections = defaultdict(list)
        
    def _triangNodes(self,triangulation):
        
        nodes = triangulation.points
        triangle_tuples_list = list()
        
        for triangle in triangulation.simplices:
            triangle_tuples = [tuple(nodes[i,:]) for i in triangle]
            triangle_tuples_list.append(triangle_tuples)
            
        return triangle_tuples_list
    
    #It constructs the quadrilateral element and maps the gaps between them
    def _DrawQuad(self,element_fields):
        
         element = element_fields[:,:2]
         abciss = element[:,0]
         ordinate = element[:,1]
         
         fields = element_fields[:,2:]
        
         field = fields[:,self.field_id]
        
         triangulation = Delaunay(element)
        
         boundary = element[self.order_v**2:, :]
         
         #Adjust for following the counterclockwise way
         face_0 = boundary[:self.order_f, :]
         face_1 = boundary[3*self.order_f:, :]
         face_2 = np.flipud(boundary[self.order_f:2*self.order_f,:])
         face_3 = np.flipud(boundary[2*self.order_f:3*self.order_f, :])
         
         faces=(face_0, face_1, face_2, face_3)
         
    
         corners_list = [[face[0], face[-1]] for face in faces]
         corners = sum(corners_list,[])
         corners_ = corners[1:]+[corners[0]]
         
         corners_pairs = [tuple(corners_[i:i+2]) for i in range(0,8,2)]
         
         imag_corners_tuples = [[(this[0], that[1]), (that[0], this[1])] for this,that in corners_pairs]
    
         for this,that in corners_pairs:
             self.points_connections[tuple(that)].append(tuple(this))
             self.points_connections[tuple(this)].append(tuple(that))
             
         boundary = np.concatenate(faces, axis=0)
         element_hull = Polygon(boundary)
        
         triangles = triangulation.simplices
         triangle_tuples_list = self._triangNodes(triangulation)

         sub_polygons_list = [Polygon(item).centroid for item in  triangle_tuples_list]
         checks_containing = [element_hull.contains(item) for item in sub_polygons_list]

         triangles_ = [triangles[i] for i,item in enumerate(checks_containing) if item]
         triangles_ = np.array(triangles_)

         triang = tri.Triangulation(abciss,ordinate,triangles_)

         imag_corners_tuples = sum(imag_corners_tuples, [])
         imag_corners_list = [ point for point in imag_corners_tuples if not element_hull.contains(Point(point)) ]
         
         corner_1, corner_2, corner_3, corner_4 = imag_corners_list
         #Insert the corners in the element polygon
         faces = ([corner_4], face_0, [corner_1], face_1,[ corner_2], face_2, [corner_3], face_3)
         
         boundary = np.concatenate(faces, axis=0)
         element_hull = Polygon(boundary)
         
         return triang, boundary, field
     
    #It fills the gaps originated by the missing element corners by means a local triangularization
    def _FillBlank(self):
        
        blanks_ = list()
        blanks = list()
        blanks_fields = list()
        
        for point,conn in self.points_connections.items():
    
            group = [point]+conn
            
            for sconn in conn:
                
                self.points_connections[sconn].remove(point)
                group += self.points_connections.get(sconn)
                blanks_.append(group)
            
        blanks_ = filter(lambda item: len(item)>=3,blanks_)
        blanks = list(blanks_)
     
        for group in blanks:
            
            group_field = [self.points_fields.get(point) for point in group]
            blanks_fields.append(group_field)
                
        blanks_triang = list()
        for group in blanks:
            
            blank_array=np.array(group)
            abciss = blank_array[:,0]
            ordinate = blank_array[:,1]
            
            triangulation =  Delaunay(blank_array)
            triangles = triangulation.simplices
            triang = tri.Triangulation(abciss,ordinate,triangles)
            blanks_triang.append(triang)
        
        triang_field = [(blanks_triang[i],blanks_fields[i]) for i,group in enumerate(blanks)]
        
        return triang_field
        
    def _plot_factory(self):

        if self.plot_type == 'solid':

            def aux_plot(triang, boundary, field, norm, ax0):

                im = ax0.tripcolor(triang,field,\
                cmap=plt.cm.jet,norm=norm,shading='gouraud')
                ax0.plot(boundary[:,0],boundary[:,1], lw=0.5, color='black')

                return im

            def blank_plot(triang, field, norm, ax0):

                im = ax0.tripcolor(triang,field,\
                cmap=plt.cm.jet,norm=norm,shading='gouraud')
            
                return im
            
        if self.plot_type == 'contour':

            def aux_plot(quad_domain, quad_boundary, field, norm, ax0):

               im = ax0.tricontourf(triang,field,\
               cmap=plt.cm.jet,norm=norm)
               ax0.plot(boundary[:,0],boundary[:,1], lw=0.5, color='black')

               return im
           
            def blank_plot(triang, field, norm, ax0):

                im = ax0.tripcolor(triang,field,\
                cmap=plt.cm.jet,norm=norm,shading='gouraud')
            
                return im

        return aux_plot, blank_plot


    def plot(self, filename):

        _plotIt = map(self._DrawQuad,self.elements_fields)
        Fields =  self.elements_fields_mono[:,2:]
        Field = Fields[:,self.field_id]
        
        hor_lenght = np.abs(self.elements_fields_mono[:,0].max()-self.elements_fields_mono[:,0].min())
        vert_lenght = np.abs(self.elements_fields_mono[:,1].max()-self.elements_fields_mono[:,1].min())
        
        if int(hor_lenght/vert_lenght):
            orient = 'horizontal'
        else:
            orient = 'vertical'
            
        cmap = plt.get_cmap('gist_rainbow')
        levels = MaxNLocator(nbins=20).tick_values(Field.min(),Field.max())
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
        
        logging.debug('Plotting pre-processing concluded')
        counter = 1

        logging.debug('Elementwise Plotting for field %s starting ...' % (self.field_name))
        aux_plot, blank_plot = self._plot_factory()
        fig = plt.figure(1)
        ax0 = plt.gca()
        ax0.set_aspect('equal')


        for triang, boundary, field in _plotIt:

            #Plot the quadrilateral elements
            im = aux_plot(triang, boundary, field, norm, ax0)
            sys.stdout.write("\rPlot Element %i of %i" % (counter,self.number_of_elements))
            sys.stdout.flush()
            
            counter+=1
            
        triang_field =  self._FillBlank()
        
        for triang, field in triang_field:
            im = blank_plot(triang, field, norm, ax0)
            
        plt.title(self.field_name)
        fig.colorbar(im, ax=ax0, orientation=orient)
        plt.savefig(filename)
        plt.close(fig)
        logging.debug('Plotting for  %s concluded' % (self.field_name))
        
#TO BE FIXED    
class PlotterTri(FlyweightMixin):

    def __init__(self,Nodes,Elements,Field,Points,field_name, plot_type):

        self.Nodes = Nodes
        self.Elements = Elements
        self.Field = Field
        self.Points = Points
        self.field_name =  field_name
        self.plot_type = plot_type
        self._vmapIndex = np.vectorize(self._mapIndex)


    def _preprocess(self):

        self.number_of_nodes, self.dimension = self.Points.shape

        self.number_of_elements = self.Elements.shape[0]

        self.NodesDict = {i+1:list(self.Nodes[i,:]) for i in np.arange(self.number_of_nodes)}

        self.PointsDict = {i+1:list(self.Points[i,:]) for i in np.arange(self.number_of_nodes)}

        self.search_coord = lambda item: [self.NodesDict[index] for index in item]

        self.Nodes2Field = {tuple(self.NodesDict[item]):self.Field[item-1] for item in self.PointsDict}


    #Based on the gmsh nodes distribution for quadrilateral elements
    def _boundaryDetection(self,element):

        order = int(((-1+np.sqrt(1+8*(len(element))).astype(np.int64)))/2)

        lenght = 3*(order-2)+3
        pos = 0
        pre_element_resorted = list()
        i=0

        layer = element[pos:pos+lenght]

        corners = list(layer[:3])
        middle_nodes = list(layer[3:])

        begins = [int(j*(order-2*(i+1))) for j in range(len(corners))]
        ends = [int((j+1)*(order-2*(i+1))) for j in range(len(corners))]

        layer = [[corner]+middle_nodes[begins[j]:ends[j]]\
        for j,corner in enumerate(corners)]

        boundary  = sum(layer,[])

        return boundary, order

    def _mapIndex(self,index, indexing_map):
        return indexing_map[index]

    def _fillField(self,x,y):
        coord = (x,y)
        return self.Nodes2Field[coord]

    def _triangNodes(self,triangulation):
        nodes = triangulation.points
        triangle_tuples_list = list()
        for triangle in triangulation.simplices:
            triangle_tuples = [tuple(nodes[i,:]) for i in triangle]
            triangle_tuples_list.append(triangle_tuples)
        return triangle_tuples_list

    def _plot_factory(self):

        if self.plot_type == 'solid':

            def aux_plot(triang, tri_boundary, field, norm, ax0):

                im = ax0.tripcolor(triang, field,\
                cmap=plt.cm.jet,norm=norm,shading='gouraud')
                ax0.plot(tri_boundary[:,0],tri_boundary[:,1], lw=0.5, color='black')

                return im

        if self.plot_type == 'flat':

            def aux_plot(triang, tri_boundary, field, norm, ax0):

                im = ax0.tripcolor(triang, field,\
                cmap=plt.cm.jet,norm=norm,shading='flat')
                ax0.plot(tri_boundary[:,0],tri_boundary[:,1], lw=0.5, color='black')

                return im

        if self.plot_type == 'contour':

            def aux_plot(quad_domain, quad_boundary, field, norm, ax0):

               im = ax0.tricontourf(triang, field,\
               cmap=plt.cm.jet,norm=norm)
               ax0.plot(tri_boundary[:,0],tri_boundary[:,1], lw=0.5, color='black')

               return im

        return aux_plot

    def _DrawTri(self,element_index):

         element = self.Elements[element_index]

         boundary, order = self._boundaryDetection(element)

         tri_element_tuples = self.search_coord(element)
         tri_element = np.array(tri_element_tuples)

         abciss = tri_element[:,0]
         ordinate = tri_element[:,1]

         tri_boundary_tuples = self.search_coord(boundary)
         tri_boundary = np.array(tri_boundary_tuples)

         cota = np.array([i+1 for i in range(len(abciss))]).astype(np.int64)
         Cota = {i+1:(abciss[i],ordinate[i]) for i,item in enumerate(abciss) }
         uCota = {(abciss[i],ordinate[i]):i for i,item in enumerate(abciss) }

         triangulation = Delaunay(tri_element)

         triangle_hull = Polygon(tri_boundary_tuples)
         triangles = triangulation.simplices
         triangle_tuples_list = self._triangNodes(triangulation)

         sub_polygons_list = [Polygon(item).centroid for item in  triangle_tuples_list]
         checks_containing = [triangle_hull.contains(item) for item in sub_polygons_list]

         triangles_ = [triangles[i] for i,item in enumerate(checks_containing) if item]
         triangles_ = np.array(triangles_)

         triang = tri.Triangulation(abciss,ordinate,triangles_)

         vfillField = np.vectorize(self._fillField)
         field = vfillField(abciss,ordinate)

         return triang,tri_boundary,field

    def plot(self):

        self._preprocess()
        element_indices = list(np.arange(0,len(self.Elements),1).astype(np.int64))
        _plotIt = map(self._DrawTri,element_indices)

        cmap = plt.get_cmap('gist_rainbow')
        levels = MaxNLocator(nbins=20).tick_values(self.Field.min(),self.Field.max())
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
        fig = plt.figure(1)
        ax0 = plt.gca()
        logging.debug('Plotting pre-processing concluded')
        counter = 1

        logging.debug('Elementwise Plotting for {} field starting ...').format(field_name)
        aux_plot = self._plot_factory()

        for triang, tri_boundary, field in _plotIt:

            im = aux_plot(triang, tri_boundary, field, norm, ax0)
            sys.stdout.write("\rPlot Element %i of %i" % (counter,self.number_of_elements))
            sys.stdout.flush()

            counter+=1

        plt.title(self.field_name)
        fig.colorbar(im, ax=ax0)
        plt.show()

#Plot from a scattered set of points
class PlotterScatterQuad(FlyweightMixin):

    def __init__(self,Nodes,Elements,Boundaries, Points, plot_type):

        self.Nodes = Nodes #Geometric mesh nodes reference
        self.Elements = Elements
        self.Points = Points
        self.Boundaries = Boundaries
        self.plot_type = plot_type

        self._Points = Points
        self.count = 0
        
    def _preprocess(self):

        self.number_of_points, self.dimension = self.Points.shape

        self.number_of_nodes = self.Nodes.shape[0]

        self.number_of_elements = self.Elements.shape[0]

        self.NodesDict = {i+1:list(self.Nodes[i,:]) for i in np.arange(self.number_of_nodes)}

        self.PointsDict = {i+1:list(self.Points[i,:]) for i in np.arange(self.number_of_points)}

        self.search_coord = lambda item, Dict: [Dict[index] for index in item]

        self.Nodes2Field = {tuple(self.PointsDict[item]):self.Field[item-1] for item in self.PointsDict}

        self.geo_Points = [Point(point) for point in self.Points]

        self._external_boundaryDetection()
        
    def resort_edge(self, edge_element):
        
        #Gmsh edge element convention
        head = [edge_element[0]]
        tail = [edge_element[1]]
        body = edge_element[2:]
        element_resorted = sum([head,body,tail],[])
        
        return element_resorted
    
    def check_containing(self, triang_polygon):
        
        check=list()
        for boundary_shape in self.boundaries_shapes.values():
            
            check.append( boundary_shape.contains(triang_polygon) )
       
        return sum(check)
    
    def _external_boundaryDetection(self):
        
        self.boundaries_shapes = dict()
        for boundary_name, boundary in self.Boundaries.items():
    
            boundary_list = [list(item) for item in boundary]
            boundary_list_resorted = [self.resort_edge(element) for element in boundary_list]
            boundary_resorted = sum(boundary_list_resorted,[])
            boundary_resorted_simplified = list( set(boundary_resorted) )
           
            boundary_tuples = self.search_coord(boundary_resorted_simplified, self.NodesDict)
            boundary_polygon = Polygon(boundary_tuples)
            if boundary_polygon.area:
                self.boundaries_shapes[boundary_name] = boundary_polygon
    
    #Based on the gmsh nodes distribution for quadrilateral elements
    def _boundaryDetection(self,element):

        order = np.sqrt(len(element)).astype(np.int64)

        lenght = 4*(order-2)+4
        pos = 0
        pre_element_resorted = list()
        i=0

        layer = element[pos:pos+lenght]

        corners = list(layer[:4])
        middle_nodes = list(layer[4:])

        begins = [int(j*(order-2*(i+1))) for j in range(len(corners))]
        ends = [int((j+1)*(order-2*(i+1))) for j in range(len(corners))]

        layer = [[corner]+middle_nodes[begins[j]:ends[j]]\
        for j,corner in enumerate(corners)]

        boundary  = sum(layer,[])

        return boundary

    def _triangNodes(self,triangulation):

        nodes = triangulation.points
        triangle_tuples_list = list()
        
        for triangle in triangulation.simplices:
            triangle_tuples = [tuple(nodes[i,:]) for i in triangle]
            
            triang_polygon = Polygon(triangle_tuples)
            check = self.check_containing(triang_polygon)
            if not check:
                triangle_tuples_list.append(triangle)

        return np.array(triangle_tuples_list)

    def _plot_factory(self):

        cmap = plt.get_cmap('gist_rainbow')
        levels = MaxNLocator(nbins=100).tick_values(self.Field.min(),self.Field.max())
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

        if self.plot_type == 'solid':

            def aux_plot(triang, _plotIt, image_namefile):
                fig = plt.figure(self.count)
               
                ax0 = plt.gca()
                ax0.set_aspect('auto')

                im = ax0.tripcolor(triang, self.Field,\
                cmap=plt.cm.jet,norm=norm,shading='gouraud')
                fig.colorbar(im, ax=ax0)

                for quad_boundary in _plotIt:

                    im = plt.plot(quad_boundary[:,0],quad_boundary[:,1], lw=0.5, color='black')
                  
                plt.title(self.field_name)
                plt.savefig(image_namefile)
                self.count += 1
            
        if self.plot_type == 'flat':

            def aux_plot(triang, _plotIt, image_namefile):
                fig = plt.figure(self.count)
                ax0 = plt.gca()


                im = ax0.tripcolor(triang, self.Field,\
                cmap=plt.cm.jet,norm=norm,shading='flat')
                fig.colorbar(im, ax=ax0)
                self.count += 1
                for quad_boundary in _plotIt:

                    im = ax0.plot(quad_boundary[:,0],quad_boundary[:,1], lw=0.5, color='black')
                    sys.stdout.write("\rPlot Element %i of %i" % (counter,self.number_of_elements))
                    sys.stdout.flush()

                    counter+=1

                plt.title(self.field_name)
                plt.savefig(image_namefile)
    
            
        if self.plot_type == 'contour':

            def aux_plot(triang, _plotIt, image_namefile):

                fig = plt.figure(self.count)
                ax0 = plt.gca().set_aspect('equal')
                cmap = plt.get_cmap('gist_rainbow')


                levels = MaxNLocator(nbins=100).tick_values(self.Field.min(),self.Field.max())
                norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

                im = plt.tricontourf(triang, self.Field,\
                100,cmap=plt.cm.jet,norm=norm)

                fig.colorbar(im, ax=ax0)
                self.count += 1

                for quad_boundary in _plotIt:

                    im = plt.plot(quad_boundary[:,0],quad_boundary[:,1], lw=0.5, color='black')
                    sys.stdout.write("\rPlot Element %i of %i" % (counter,self.number_of_elements))
                    sys.stdout.flush()

                    counter+=1

                plt.title(self.field_name)
                plt.savefig(image_namefile)

        return aux_plot

    def _DrawDomain(self):

         triangulation = Delaunay(self.Points)
         abciss = self.Points[:,0]
         ordinate = self.Points[:,1]
         triangles = triangulation.simplices
         triangles = self._triangNodes(triangulation)
        
         triang = tri.Triangulation(abciss,ordinate,triangles)

         return triang

    def _DrawQuad(self,element_index):

         element = self.Elements[element_index]

         boundary = self._boundaryDetection(element)

         quad_boundary_tuples = self.search_coord(boundary, self.NodesDict)

         quad_boundary = np.array(quad_boundary_tuples)

         return quad_boundary

    def plot(self, field, field_name, image_namefile):

        self.field_name =  field_name
        self.Field = field
        self._preprocess()
        element_indices = list(np.arange(0,len(self.Elements),1).astype(np.int64))
        _plotIt = map(self._DrawQuad,element_indices)

        logging.debug('Plotting pre-processing concluded')
        counter = 1

        logging.debug('Elementwise Plotting for field %s starting ...' % self.field_name)
        aux_plot = self._plot_factory()

        triang = self._DrawDomain()
        aux_plot(triang, _plotIt, image_namefile)
        logging.debug('Plotting concluded')
        
#Plotting scattered data point for triangular mesh
class PlotterSacatteredTri(FlyweightMixin):

    def __init__(self,Nodes,Elements,Field,Points,field_name, plot_type):

        self.Nodes = Nodes
        self.Elements = Elements
        self.Field = Field
        self.Points = Points
        self.field_name =  field_name
        self.plot_type = plot_type
        self._vmapIndex = np.vectorize(self._mapIndex)


    def _preprocess(self):

        self.number_of_points, self.dimension = self.Points.shape

        self.number_of_nodes = self.Nodes.shape[0]

        self.number_of_elements = self.Elements.shape[0]

        self.NodesDict = {i+1:list(self.Nodes[i,:]) for i in np.arange(self.number_of_nodes)}

        self.PointsDict = {i+1:list(self.Points[i,:]) for i in np.arange(self.number_of_points)}

        self.search_coord = lambda item, Dict: [Dict[index] for index in item]

        self.Nodes2Field = {tuple(self.PointsDict[item]):self.Field[item-1] for item in self.PointsDict}

        self.geo_Points = [Point(point) for point in self.Points]


    #Based on the gmsh nodes distribution for quadrilateral elements
    def _boundaryDetection(self,element):

        order = int(((-1+np.sqrt(1+8*(len(element))).astype(np.int64)))/2)

        lenght = 3*(order-2)+3
        pos = 0
        pre_element_resorted = list()
        i=0

        layer = element[pos:pos+lenght]

        corners = list(layer[:3])
        middle_nodes = list(layer[3:])

        begins = [int(j*(order-2*(i+1))) for j in range(len(corners))]
        ends = [int((j+1)*(order-2*(i+1))) for j in range(len(corners))]

        layer = [[corner]+middle_nodes[begins[j]:ends[j]]\
        for j,corner in enumerate(corners)]

        boundary  = sum(layer,[])

        return boundary, order

    def _mapIndex(self,index, indexing_map):
        return indexing_map[index]

    def _fillField(self,x,y):
        coord = (x,y)
        return self.Nodes2Field[coord]

    def _triangNodes(self,triangulation):
        nodes = triangulation.points
        triangle_tuples_list = list()
        for triangle in triangulation.simplices:
            triangle_tuples = [tuple(nodes[i,:]) for i in triangle]
            triangle_tuples_list.append(triangle_tuples)
        return triangle_tuples_list

    def _plot_factory(self):

        cmap = plt.get_cmap('gist_rainbow')
        levels = MaxNLocator(nbins=100).tick_values(self.Field.min(),self.Field.max())
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

        if self.plot_type == 'solid':

            def aux_plot(triang, _plotIt):
                fig = plt.figure(1)
                ax0 = plt.gca().set_aspect('equal')

                triang_ref, Field_ref = refiner(triang, self.Field)

                im = plt.tripcolor(triang_ref, Field_ref,\
                cmap=plt.cm.jet,norm=norm,shading='gouraud')
                fig.colorbar(im, ax=ax0)
                counter = 1
                for tri_boundary in _plotIt:

                    im = plt.plot(tri_boundary[:,0],tri_boundary[:,1], lw=0.5, color='black')
                    sys.stdout.write("\rPlot Element %i of %i" % (counter,self.number_of_elements))
                    sys.stdout.flush()

                    counter+=1

                plt.title(self.field_name)
                plt.show()

        if self.plot_type == 'flat':

            def aux_plot(triang, _plotIt):
                fig = plt.figure(1)
                ax0 = plt.gca()


                im = ax0.tripcolor(triang, self.Field,\
                cmap=plt.cm.jet,norm=norm,shading='flat')
                fig.colorbar(im, ax=ax0)
                counter = 1
                for quad_boundary in _plotIt:

                    im = ax0.plot(quad_boundary[:,0],quad_boundary[:,1], lw=0.5, color='black')
                    sys.stdout.write("\rPlot Element %i of %i" % (counter,self.number_of_elements))
                    sys.stdout.flush()

                    counter+=1

                plt.title(self.field_name)
                plt.show()

        if self.plot_type == 'contour':

            def aux_plot(triang, _plotIt):

                fig = plt.figure(1)
                ax0 = plt.gca().set_aspect('equal')
                cmap = plt.get_cmap('gist_rainbow')


                levels = MaxNLocator(nbins=100).tick_values(self.Field.min(),self.Field.max())
                norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

                im = plt.tricontourf(triang, self.Field,\
                100,cmap=plt.cm.jet,norm=norm)

                fig.colorbar(im, ax=ax0)
                counter = 1

                for quad_boundary in _plotIt:

                    im = plt.plot(quad_boundary[:,0],quad_boundary[:,1], lw=0.5, color='black')
                    sys.stdout.write("\rPlot Element %i of %i" % (counter,self.number_of_elements))
                    sys.stdout.flush()

                    counter+=1

                plt.title(self.field_name)
                plt.show()

        return aux_plot

    def _DrawDomain(self):

         triangulation = Delaunay(self.Points)
         abciss = self.Points[:,0]
         ordinate = self.Points[:,1]
         triangles = triangulation.simplices
         triangle_tuples_list = self._triangNodes(triangulation)

         triang = tri.Triangulation(abciss,ordinate,triangles)

         return triang

    def plot(self, image_namefile):

        self._preprocess()
        element_indices = list(np.arange(0,len(self.Elements),1).astype(np.int64))
        _plotIt = map(self._DrawTri,element_indices)

        cmap = plt.get_cmap('gist_rainbow')
        levels = MaxNLocator(nbins=20).tick_values(self.Field.min(),self.Field.max())
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
        fig = plt.figure(1)
        ax0 = plt.gca()
        logging.debug('Plotting pre-processing concluded')
        counter = 1

        logging.debug('Elementwise Plotting starting ...')
        aux_plot = self._plot_factory()

        for triang, tri_boundary, field in _plotIt:

            im = aux_plot(triang, tri_boundary, field, norm, ax0)
            sys.stdout.write("\rPlot Element %i of %i" % (counter,self.number_of_elements))
            sys.stdout.flush()

            counter+=1

        plt.title(self.field_name)
        fig.colorbar(im, ax=ax0)
        plt.savefig(image_namefile)
        plt.show()
