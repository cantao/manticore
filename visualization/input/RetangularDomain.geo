
Lx = 10.;
Ly = 5.;
lc = 2.5;
LC = 2*lc;

Point(0) = {0,0,0,lc};
Point(1) = {Lx,0,0,lc};

Point(2) = {Lx,Ly,0,lc};
Point(3) = {0,Ly,0,lc};

Line(4) = {0,1};
Line(5) = {1,2};
Line(6) = {2,3};
Line(7) = {3,0};

Line Loop(8) = {4:7};
Plane Surface(9) = {8};

Physical Line("Inlet") = {7};
Physical Line("Outlet") = {5};
Physical Line("Superior Wall") = {6};
Physical Line("Inferior Wall") = {4};

Physical Surface("Medium") = {9};
//Transfinite Surface{9} = {0,1,2,3};
//Recombine Surface{9};
