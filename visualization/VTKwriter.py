#bin/bash/python3
#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
#Simple script to recover data from msh and mdf format in order
#to enable the visualization via ParaView

#This script is embiased to the unstructured grid vtk format
#because this the single extension used is the vtu

import numpy as np
import os,sys

from mdf.fielddata import FieldData
from mdf.file import File

#MSH format interpretation
class MSH:

    def __init__(self):

        self.MeshFormat = None
        self.PhysicalNames = None
        self.NodesList = None
        self.EdgesList = None
        self.ElementsList = None

        self.NumberOfPhysical = None
        self.NumberOfNodes = None
        self.NumberOfEdges = None
        self.NumberOfElements = None

        self.NOfStdColNodes = 4
        self.NOfStdColElements = None

        self.StdDataCols = 5

    def Reading(self,inputfile):

        UnitFile = open(inputfile,"rb")

        #Step Zero: reading the header
        #Mesh Format

        UnitFile.readline()
        self.MeshFormat = np.array(UnitFile.readline())
        UnitFile.readline()
        UnitFile.readline()

        #Physical Elements

        self.NumberOfPhysical = int(UnitFile.readline())

        self.PhysicalNames = list()

        for i in range(0,self.NumberOfPhysical):

            self.PhysicalNames.append(UnitFile.readline())

        UnitFile.readline()
        UnitFile.readline()

        #First Part: Reading Nodes

        self. NumberOfNodes = int(UnitFile.readline())

        self.NodesList=np.zeros((self.NumberOfNodes,self.NOfStdColNodes))

        counter = 0
        for counter in range(0,self.NumberOfNodes):

            self.NodesList[counter,:] = \
            np.fromstring(UnitFile.readline(),sep = " ")

        UnitFile.readline()
        UnitFile.readline()

        #Second Part: Reading Face/Edge Elements

        self.NumberOfEdges = int(UnitFile.readline())

        ref = np.fromstring(UnitFile.readline(),sep = " ")

        counter = 0

        self.NOfStdColEdges = len(ref)

        self.EdgesList=np.zeros((self.NumberOfEdges,self.NOfStdColEdges))

        self.EdgesList[0,:] = ref

        while len(ref) == self.NOfStdColEdges:

            self.EdgesList[counter,:] = ref
            ref = np.fromstring(UnitFile.readline(),sep = " ")
            counter +=1

        #Third Part: Reading Domain Elements

        self.NumberOfElements = self.NumberOfEdges-counter
        self.NOfStdColElements = len(ref)
        self.ElementsList=np.zeros((self.NumberOfElements,self.NOfStdColElements))

        self.ElementsList[0,:] = ref

        for counter in range(1,self.NumberOfElements):

            self.ElementsList[counter,:] = \
            np.fromstring(UnitFile.readline(),sep = " ")

        UnitFile.close()

#global standard variables
class standard(object):
    #default
    def __init__(self):

        self.version = "1.0"
        self.unstructured_format_version = "0.1"
        self.byte_order = "BigEndian" #BigEndian or LittleEndian
        self.compressor = "vtkZLibDataCompressor"
        self.int32 = 4 #bytes
        self.int64 = 8 #bytes

    def non_default(self):
        pass

#Generic XML DataArray template
class DataArray(standard):

    def __init__(self,Name,_type,_format,NumberOfComponents,data_array,tab):
        standard.__init__(self)
        self.type = _type
        self.Name = Name
        self.NumberOfComponents = NumberOfComponents
        self.data_array = data_array
        self.tab = tab
        self.format = _format

    def write(self,unitfile,array_list):

        range_max = np.amax(array_list.astype(np.float32))
        range_min = np.amin(array_list.astype(np.float32))

        if self.NumberOfComponents:
            line1 = '%s<DataArray type="%s" Name="%s" NumberOfComponents="%i" format="%s" RangeMin="%i" RangeMax="%i">\n'\
             % (self.tab,self.type,self.Name, self.NumberOfComponents,self.format, range_min, range_max)
        else:
            line1 = '%s<DataArray type="%s" Name="%s" format="%s" RangeMin="%i" RangeMax="%i">\n'\
             % (self.tab,self.type,self.Name,self.format, range_min, range_max)

        line2 = '%s\n' %(self.tab+'\t'+self.data_array)
        line3 = self.tab+'</DataArray>\n'

        unitfile.writelines(line1)
        unitfile.writelines(line2)
        unitfile.writelines(line3)

#VTK XML format
class XMLVtk(standard):

        def __init__(self, msh, _struct_type,parallel,fieldVar,_write_format):
            standard.__init__(self)
            #Gmsh msh object
            self.msh = msh

            # Is it a partitioned structure, False or True
            self.parallelization = parallel

            self.struct_type = _struct_type
            #There is variables values associated,False or True
            self.FieldVariables = fieldVar
            self._write_format = _write_format #ascii or binary

            self.number_of_points = self.msh.NodesList.shape[0]
            self.number_of_cells = self.msh.ElementsList.shape[0]

            self.element_nvertices = self.msh.ElementsList.shape[1] - self.msh.StdDataCols
            self.Nodes_data_array = None
            self.Cells_data_array = None

            self.Nodes_list = None
            self.Elements_list_offsets = None
            self.Offsets = None
            self.offsets = None

            self.cell_types = np.zeros(self.msh.NumberOfElements)

            self.pieces = 1 #This should be modified

            self.identation = lambda n: n*'\t'
            self.tags = list()
            self.headers = dict()

            self.vtk_type_dict = {'3':[5,'VTK_triangle'], '4':[8,'VTK_quad']}

            #It prepares the dato for the writing process
            self.PreProcessing()

        #Basic features preparation
        def PreProcessing(self):

            if self.FieldVariables:
                self.tags = ['PointData','CellData','Points','Cells']
            else:
                self.tags = ['Points','Cells']

            if self.parallelization:

                for i,tag in enumerate(self.tags):
                    self.tag[i] = 'P'+tag

            header_main = '<VTKFile type="%s" version="%s" byte_order="%s" compressor="%s">\n'\
            % (self.struct_type,self.unstructured_format_version,self.byte_order,self.compressor)
            baseboard_main = '</VTKFile>\n'

            header_structure = '%s<%s>\n'\
            % (self.identation(1),self.struct_type)
            baseboard_structure = '%s</%s>\n'\
            % (self.identation(1),self.struct_type)

            header_piece = '%s<Piece NumberOfPoints="%i" NumberOfCells="%i">\n'\
            % (self.identation(2),self.number_of_points,self.number_of_cells)
            baseboard_piece = '%s</Piece>\n' % (self.identation(2))

            self.headers.update({'main':[header_main,baseboard_main]})
            self.headers.update({'structure':[header_structure,baseboard_structure]})
            self.headers.update({'piece':[header_piece,baseboard_piece]})

            for tag in self.tags:

                tab = self.identation(3)
                self.headers.update({tag:[tab+'<'+tag+'>\n',tab+'</'+tag+'>\n']})

            #It concatenates all the nodes in a single string
            self.Nodes_list = self.msh.NodesList[:,1:]
            all_nodes = self.Nodes_list.reshape(3*self.number_of_points).astype(str)
            all_nodes_str = ' '.join(list(all_nodes))

            #It corrects the accounting criterium, VTK starts in 0, GMSH mesh format (msh) in 1.
            self.Elements_list_offsets = self.msh.ElementsList[:,self.msh.StdDataCols:]\
            -np.ones((self.number_of_cells,self.element_nvertices))

            self.Elements_list_offsets = (self.Elements_list_offsets.astype(np.int64))
            self.Elements_list_offsets = (self.Elements_list_offsets.astype(np.int64)).astype(str)

            all_cells = self.Elements_list_offsets.reshape((self.element_nvertices)*self.number_of_cells).astype(str)
            all_cells_str = ' '.join(list(all_cells))

            self.Offsets = np.arange(self.element_nvertices,((self.number_of_cells+1)*self.element_nvertices),self.element_nvertices)
            self.offsets = ' '.join((self.Offsets).astype(np.int64).astype(str))

            cell_lengths = map(lambda cell: len(cell[self.msh.StdDataCols:]), self.msh.ElementsList)
            for i,element_len in enumerate(cell_lengths):
                self.cell_types[i] = self.vtk_type_dict[str(element_len)][0]

            all_cell_types_str = ' '.join(list((self.cell_types.astype(np.int64)).astype(str)))

            tab = self.identation(4)
            self.Nodes_data_array = DataArray("Points","Float32",self._write_format,3,all_nodes_str,tab)
            self.Connectivity_data_array = DataArray("connectivity","Int32",self._write_format,None,all_cells_str,tab)
            self.Offsets_data_array = DataArray("offsets","Int32",self._write_format,None,self.offsets,tab)
            self.Types_data_array = DataArray("types","UInt8",self._write_format,None,all_cell_types_str,tab)

        def Writing(self,filename):

            Unitfile = open(filename,'w')
            Unitfile.writelines('<?xml version="%s"?>\n' % (self.version))

            self.file_identification = filename.split('/')[-1][:-4]

            #It writes the headers
            Unitfile.writelines(self.headers['main'][0])
            Unitfile.writelines(self.headers['structure'][0])
            Unitfile.writelines(self.headers['piece'][0])

            #It writes the scope
            ###Nodes
            Unitfile.writelines(self.headers['Points'][0])
            tab = self.identation(4)
            self.Nodes_data_array.write(Unitfile,self.Nodes_list)
            Unitfile.writelines(self.headers['Points'][1])

            ##Elements
            Unitfile.writelines(self.headers['Cells'][0])
            self.Connectivity_data_array.write(Unitfile,self.Elements_list_offsets)
            self.Offsets_data_array.write(Unitfile,self.Offsets)
            self.Types_data_array.write(Unitfile,self.cell_types)
            Unitfile.writelines(self.headers['Cells'][1])

            #It writes the baseborads
            Unitfile.writelines(self.headers['piece'][1])
            Unitfile.writelines(self.headers['structure'][1])
            Unitfile.writelines(self.headers['main'][1])

            Unitfile.close()
def main():

    if len(sys.argv) < 3:
        print('\n')
        print('Syntax: python %s <format flag> <input file>' % sys.argv[0])
        print ('format flag --> --ascii or --binary')
        print('\n')
    else:
        #MSH instance
        mesh = MSH()
        #msh file reading
        mesh.Reading(sys.argv[2])
        outupt_filename = '%s.vtu' % (sys.argv[2][:-4])

        parallel = False #It will be modified
        fieldVar = False #It will be modified

        _struct_type = 'UnstructuredGrid' #It can be modified

        if sys.argv[1] == '--ascii':
            _format = 'ascii'

        if sys.argv[1] == '--binary':
            _format = 'binary'

        vtk = XMLVtk(mesh,_struct_type,parallel,fieldVar,_format)
        vtk.Writing(outupt_filename)

if __name__ == "__main__":

    main()
