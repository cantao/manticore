#bin/bash/python3
#
# Copyright 2016, 2017, Cláudio Alessandro de Carvalho Silva and
# Renato Fernandes Cantão.
#
# This file is part of Manticore.
#
# Manticore is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Manticore is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with Manticore. If not, see <http://www.gnu.org/licenses/>.
#
#Simple script to recover data from msh and mdf format in order
#to enable the visualization via ParaView

#This script is embiased to the unstructured grid vtk format
#because this the single extension used is the vtu

import numpy as np
import os,sys
import logging

from manticore.mdf.fielddata import FieldData
from manticore.mdf.file import File
from manticore.mdf.enums import MapType, FieldType


#global standard variables
class standard_vtkxml(object):
    #default
    def __init__(self):

        self.version = "1.0"
        self.unstructured_format_version = "0.1"
        self.byte_order = "LittleEndian" #BigEndian or LittleEndian
        self.compressor = "vtkZLibDataCompressor"
        self.int32 = 4 #bytes
        self.int64 = 8 #bytes

    def non_default(self):
        pass

#Generic XML DataArray template
class DataArray(standard_vtkxml):

    def __init__(self,Name,_type,_format,NumberOfComponents,data_array,tab):
        standard_vtkxml.__init__(self)
        self.type = _type
        self.Name = Name
        self.NumberOfComponents = NumberOfComponents
        self.data_array = data_array
        self.tab = tab
        self.format = _format

    def write(self,unitfile,array_list,ranging):

        if ranging:
            range_max = np.amax(array_list.astype(np.float32))
            range_min = np.amin(array_list.astype(np.float32))
            line1 = '%s<DataArray type="%s" Name="%s" NumberOfComponents="%i" format="%s" RangeMin="%i" RangeMax="%i">\n'\
             % (self.tab,self.type,self.Name, self.NumberOfComponents,self.format, range_min, range_max)
        else:
            line1 = '%s<DataArray type="%s" Name="%s" format="%s">\n'\
             % (self.tab,self.type,self.Name,self.format)

        line2 = '%s\n' %(self.tab+'\t'+self.data_array)
        line3 = self.tab+'</DataArray>\n'

        unitfile.writelines(line1)
        unitfile.writelines(line2)
        unitfile.writelines(line3)

#VTK XML format
class XMLVtk(standard_vtkxml):

        def __init__(self, struct_mdf):

            standard_vtkxml.__init__(self)

            self.element_type = struct_mdf['element_type']

            self.field_type = struct_mdf['field_type']

            # Is it a partitioned structure, False or True
            self.parallelization = struct_mdf['parallel']
            # VTK structure (structured grid,unstructured grid, pointset, etc)
            self.struct_type = struct_mdf['struct_type']
            #enconding type, ascii or binary
            self._write_format = struct_mdf['write_format']

            #Mesh data
            self.partition_id = struct_mdf['partition_id']
            self.subdomain_id = struct_mdf['subdomain_id']
            self.field_name = struct_mdf['field_name']

            #mdf instantiation
            self.mdf_filename = struct_mdf['filename']

            #recovering information of the mesh
            #field values
            self.field_values = struct_mdf['field']

            #nodes information
            #A pair of arrays containing the nodes, coordinates and indices
            self.Nodes_list = struct_mdf['nodes']
            self.Nodes_indices =  struct_mdf['nodes_indices']

            #elements to nodes information
            self.Elements_list = struct_mdf['elements']
            self.Elements_indices = struct_mdf['elements_indices']

            #Spatial dimension and number of nodes obtained from the previous arrays
            self.number_of_points, self.SpatialDimension = self.Nodes_list.shape
            self.number_of_cells, self.element_nvertices = self.Elements_list.shape

            self.Nodes_data_array = None
            self.Cells_data_array = None

            self.Elements_list_offsets = None
            self.Offsets = None
            self.offsets = None

            self.identation = lambda n: n*'\t'
            self.tags = list()
            self.headers = dict()

            self.vtk_type_dict = {2:[5,'VTK_triangle'],3:[9,'VTK_quad'],8:[22,'VTK_quadratic_tri'],\
            10:[23,'VTK_quadratic_quad']}

        def _fieldSetup(self):

            if self.field_type == FieldType.SCALAR_FIELD:
                header = 'Scalars="%s"' % (self.field_name)

            if self.field_type in [FieldType.VECTOR_FIELD_2D, FieldType.VECTOR_FIELD_3D]:
                header = 'Vectors="%s"' % (self.field_name)

            number_of_components = [FieldType.SCALAR_FIELD, FieldType.VECTOR_FIELD_2D,\
            FieldType.VECTOR_FIELD_3D].index(self.field_type)+1

            return header, number_of_components

        def _connSetup(self):

             elementType_couple = self.vtk_type_dict.get(self.element_type,[1,'VTK_vertex'])
             elementType = elementType_couple[0]

             if elementType != 1:

                    self.Elements_list_offsets = (self.Elements_list) -\
                    np.ones((self.number_of_cells,self.element_nvertices))
                    self.Elements_list_offsets = np.array(self.Elements_list_offsets).astype(np.int64)

                    all_cells = self.Elements_list_offsets.reshape((self.element_nvertices)*self.number_of_cells)\
                    .astype(str)

                    all_cells_str = ' '.join(list(all_cells))

                    self.Offsets = np.arange(self.element_nvertices,((self.number_of_cells+1)*\
                    self.element_nvertices),self.element_nvertices)
                    self.offsets = ' '.join((self.Offsets).astype(np.int64).astype(str))

                    cell_lengths = map(lambda cell: str(len(cell)), self.Elements_list)
                    cell_lengths = list(cell_lengths)
                    cell_lengths_str = ' '.join(cell_lengths)

                    self.cell_types = map(lambda cell: elementType, cell_lengths)
                    self.cell_types = list(self.cell_types)
                    self.cell_types = np.array(self.cell_types).astype(str)

                    all_cell_types_str = ' '.join(list(self.cell_types))

             else:

                     self.Elements_list_offsets = (self.Nodes_indices) -\
                     np.ones(self.number_of_points)
                     self.Elements_list_offsets = np.array(self.Elements_list_offsets).astype(np.int64)

                     all_cells = self.Elements_list_offsets.astype(str)
                     all_cells_str = ' '.join(list(all_cells))

                     self.Offsets = np.arange(1,(self.number_of_points+1),1)
                     self.offsets = ' '.join((self.Offsets).astype(np.int64).astype(str))

                     cell_lenghts = map(lambda cell: '1', self.Nodes_list)
                     cell_lenghts = list(cell_lenghts)
                     cell_lengths_str = ''.join(cell_lenghts)

                     self.cell_types = map(lambda item: elementType, cell_lenghts)
                     self.cell_types = list(self.cell_types)

                     all_cell_types_str = ' '.join(self.cell_types)
                     self.cell_types = np.array(self.cell_types)

             return all_cells_str, all_cell_types_str, cell_lengths_str

        #Basic features preparation
        def PreProcessing(self):

            if self.field_name:
                self.tags = ['Points','Cells']
                self.datatags = ['PointData','CellData']

            else:
                self.tags = ['Points','Cells']
                self.datatags = []

            if self.parallelization:

                for i,tag in enumerate(self.tags):
                    self.tag[i] = 'P'+tag

            if self.SpatialDimension<3:
                extraDim = np.zeros((self.number_of_points,1))
                self.Nodes_list = np.concatenate((self.Nodes_list,extraDim),axis=1)
                self.SpatialDimension+=1

            extra_header, number_of_components = self._fieldSetup()

            header_main = '<VTKFile type="%s" version="%s" byte_order="%s" compressor="%s">\n'\
            % (self.struct_type,self.unstructured_format_version,self.byte_order,self.compressor)
            baseboard_main = '</VTKFile>\n'

            header_structure = '%s<%s>\n'\
            % (self.identation(1),self.struct_type)
            baseboard_structure = '%s</%s>\n'\
            % (self.identation(1),self.struct_type)

            header_piece = '%s<Piece NumberOfPoints="%i" NumberOfCells="%i">\n'\
            % (self.identation(2),self.number_of_points,self.number_of_cells)
            baseboard_piece = '%s</Piece>\n' % (self.identation(2))

            self.headers.update({'main':[header_main,baseboard_main]})
            self.headers.update({'structure':[header_structure,baseboard_structure]})
            self.headers.update({'piece':[header_piece,baseboard_piece]})

            for tag in self.tags:

                tab = self.identation(3)
                self.headers.update({tag:[tab+'<'+tag+'>\n',tab+'</'+tag+'>\n']})

            for datatag in self.datatags:

                tab = self.identation(3)
                self.headers.update({datatag:[tab+'<'+datatag+' '+extra_header+'>\n',tab+'</'+datatag+'>\n']})

            #It concatenates all the nodes in a single string
            all_nodes = self.Nodes_list.reshape(self.SpatialDimension*self.number_of_points).astype(str)
            all_nodes_str = ' '.join(list(all_nodes))

            #It concatenates all the nodes field values in a single string
            all_nodes_values = self.field_values.astype(str)
            all_nodes_values_str = ' '.join(list(all_nodes_values))

            all_cells_str, all_cell_types_str, cell_legths_str = self._connSetup()
            tab = self.identation(4)

            #Nodes filed data array
            self.NodesField_data_array = DataArray(self.field_name,"Float32",self._write_format,\
            3,all_nodes_values_str,tab)
            #Nodes coordinates data array
            self.Nodes_data_array = DataArray("Points","Float32",self._write_format,\
            self.SpatialDimension,all_nodes_str,tab)
            #Connectivity between cells data array
            all_cells_str, all_cell_types_str, cell_lengths_str = self._connSetup()
            #Cells datasets
            self.Connectivity_data_array = DataArray("connectivity","Int32",self._write_format,None,all_cells_str,tab)
            self.Offsets_data_array = DataArray("offsets","Int32",self._write_format,None,self.offsets,tab)
            self.Types_data_array = DataArray("types","UInt8",self._write_format,None,all_cell_types_str,tab)

        def Writing(self,filename):

            Unitfile = open(filename,'w')
            Unitfile.writelines('<?xml version="%s"?>\n' % (self.version))

            self.file_identification = filename.split('/')[-1][:-4]

            #It writes the headers
            Unitfile.writelines(self.headers['main'][0])
            Unitfile.writelines(self.headers['structure'][0])
            Unitfile.writelines(self.headers['piece'][0])

            ###Nodes Data
            Unitfile.writelines(self.headers['Points'][0])
            tab = self.identation(4)
            self.Nodes_data_array.write(Unitfile,self.Nodes_list,True)
            Unitfile.writelines(self.headers['Points'][1])
            logging.debug('Wrote Nodes into VTK file')

            ##Elements
            Unitfile.writelines(self.headers['Cells'][0])
            self.Connectivity_data_array.write(Unitfile,self.Elements_list_offsets,False)
            self.Offsets_data_array.write(Unitfile,self.Offsets,False)
            self.Types_data_array.write(Unitfile,self.cell_types,False)
            Unitfile.writelines(self.headers['Cells'][1])
            logging.debug('Wrote Elements into VTK file')

            ##Fields_Data
            Unitfile.writelines(self.headers['PointData'][0])
            tab = self.identation(4)
            self.NodesField_data_array.write(Unitfile,self.field_values,False)
            Unitfile.writelines(self.headers['PointData'][1])
            logging.debug('Wrote Nodal Data into VTK file')

            #It writes the baseborads
            Unitfile.writelines(self.headers['piece'][1])
            Unitfile.writelines(self.headers['structure'][1])
            Unitfile.writelines(self.headers['main'][1])

            Unitfile.close()
            logging.debug('Visualization file is ready')

def conversor_manager(mdf_struct):

    if mdf_struct['visualization_format'] == 'vtk_unstructured':

        conversor = XMLVtk(mdf_struct)

    return conversor
